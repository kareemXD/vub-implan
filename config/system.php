<?php

return [
    'settings' => [
        'municipality' => 1,
    ],

    'google' => [
        'maps' => [
            'apiKey' => 'AIzaSyCtezdDD1-OGd9BRJyoe6MR-rXWFlEz7OA',
            // 'apiKey' => 'AIzaSyA9-VHqTEdbnNe4SBieg9AgUwiX4zYYINU',
        ],
    ],

    'du' => [
        'requests' => [
            'statuses' => [
                'created' => 0,
                'verification' => 1,
                'inspection' => 2,
                'dictamination' => 3,
                'confirmation' => 4,
                'payValidation' => 5,
                'paid' => 6,
                'formats' => 7,
                'accepted' => 8,
                'rejected' => 9,

                'labels' => [
                    '0' => 'Creada',
                    '1' => 'Asignación',
                    '2' => 'Inspección',
                    '3' => 'Dictaminación',
                    '4' => 'Folio de pago para validación',
                    '5' => 'Pendiente de pago',
                    '6' => 'Trámite pagado',
                    '7' => 'Trámites para liberación',
                    '8' => 'Aceptado',
                    '9' => 'Rechazado',
                ]
            ],
        ],

        'licenses' => [
            'initialFolio' => 1913,
        ],

        'roles' => [
            'dictaminator' => 'Desarrollo Urbano Dictaminador',
            'inspector' => 'Desarrollo Urbano Inspector',
        ],
    ],

    'departments' => [
        'padronYLicencias' => '02',
        'desarrollorUrbano' => '18',
    ],
    
    'padron' => [
        'garbage' => [
            'concepts' => [
                '02057' => 'DESCARGA DE BASURA EN RELLENO SANITARIO M3',
                '02058' => 'RECOLECCION Y TRASLADO RESIDUOS SOLIDOS M3'
            ]
        ]
    ],

    'pc' => [
        'requests' => [
            'statuses' => [
                'pending' => 1,
                'process' => 2,
                'verification' => 3,
                'verified' => 4,
                'approved' => 5,
                'cancelled' => 6,
                'rejected' => 7,
            ],

            'statuses_labels' => [
                1 => 'PENDIENTE',
                2 => 'EN REVISIÓN',
                3 => 'EN VERIFICACIÓN',
                4 => 'VERIFICADO',
                5 => 'APROVADO',
                6 => 'CANCELADO',
                7 => 'RECHAZADO',
            ],

            'turnTypes' => [
                'highRisk' => 1,
                'ordinary' => 2,

                'labels' => [
                    1 => 'Alto Riesgo',
                    2 => 'Ordinario',
                ],

                'classes' => [
                    1 => 'badge badge-danger',
                    2 => 'badge badge-success',
                ],
            ],

            'initialFolio' => 1,
        ],
        'requirements' => [
            'statuses' => [
                'pending' => 'Pendiente',
                'uploaded' => 'Subido'
            ]
        ],
        'sources' => [
            'padron' => 1,
            'du' => 2,
            'proteccionCivil' => 3,
        ]
    ],

    'directions' => [
        'implan' => 1,
        'padron' => 2,
        'du' => 3,
        'pc' => 4,
    ],

    'months' => [
        1 => 'Ene',
        2 => 'Feb',
        3 => 'Mar', 
        4 => 'Abr', 
        5 => 'May', 
        6 => 'Jun', 
        7 => 'Jul', 
        8 => 'Ago', 
        9 => 'Sep', 
        10 => 'Oct', 
        11 => 'Nov', 
        12 => 'Dic', 
    ],
];