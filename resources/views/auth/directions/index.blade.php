@extends('layouts.index')

@section('title') Direcciones @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Direcciones</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary text-center" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <table class="table table-striped">
                <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Abreviación</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($directions as $direction)
                    <tr>
                        <td><a href="{{ route('auth.directions.edit', $direction->id) }}">{{ $direction->id }}</a></td>
                        <td>{{ $direction->nombre }}</td>
                        <td>{{ $direction->abreviacion }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>   
@endsection

@section('js')
<script>
    $(function(){

    });
</script>
@endsection