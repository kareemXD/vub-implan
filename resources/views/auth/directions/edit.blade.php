@extends('layouts.index')

@section('title') Editar Permiso @endsection

@section('css')
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Actualizar permiso</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary text-center" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <form action="{{ route('auth.directions.update') }}" method="POST">
                @csrf
                @method('PUT')
                <input type="hidden" name="directionId" value="{{ $direction->id }}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Nombre</label>
                            <input type="text" name="nombre" class="form-control" value="{{ old('nombre', $direction->nombre) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Abreviación</label>
                            <input type="text" name="abreviacion" class="form-control" value="{{ old('abreviacion', $direction->abreviacion) }}">
                        </div>
                    </div>
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
        </div>
    </div>   
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
<script>
    $(function(){

    });
</script>
@endsection