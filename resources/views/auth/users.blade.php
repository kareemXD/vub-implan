@extends('layouts.index')

@section('title') Usuarios @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Usuarios</h3>
        </div>
    </div>
    @can('write_users')
        <div class="row justify-content-end panel-buttoms">
            <button type="button" class="btn btn-primary mr-3" data-toggle="modal" data-target="#modal-new-user"><i class="fas fa-user-plus mr-2"></i> Crear Usuario</button>
        </div>
    @endcan
    <div class="panel-body with-buttons">
        <div class="row panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive">
                <table class="table dataTable">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Correo</th>
                            <th scope="col">Puesto</th>
                            <th scope="col">Dirección</th>
                            <th scope="col">Rol</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($users) > 0)
                            @foreach ($users as $user)
                                <tr class="edit-user" data-id="{{ $user->id }}">
                                    <td scope="row">{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->profile->position->nombre }}</td>
                                    <td>{{ $user->profile->position->direction->abreviacion }}</td>
                                    <td><a href="{{ route('edit.role', $user->roles[0]->id) }}" class="btn-link">{{ $user->getRoleNames()[0] }}</a></td>
                                    <td class="center"><button type="button" class="btn btn-danger delete-user" data-user="{{ $user->id }}"><i class="fas fa-trash-alt"></i></button></td>
                                </tr>
                            @endforeach
                        @endif
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @can('write_users')
        @include('partials.modals.modal_new_user')
    @endcan
    @include('partials.modals.modal_update_user')
@endsection

@section('js')
    <script>
        $(function(){
            $("#auth").addClass('active');

            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'pdfHtml5'
                ]
            });
            
            $(document).on('dblclick', '.edit-user', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.user.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    var modal = $("#modal-update-user");
                    modal.find('#id').val(result.id);
                    modal.find('.name').val(result.profile.nombre);
                    modal.find('.ape_pat').val(result.profile.apellido_pat);
                    modal.find('.ape_mat').val(result.profile.apellido_mat);
                    modal.find('.email').val(result.email);
                    modal.find('.position').val(result.profile.puesto_id).trigger('change');
                    modal.find('.role').val(result.role).trigger('change');

                    $(".select2").select2();
                    modal.modal("show");
                });
            });

            $(document).on("click", ".delete-user", function(){
                var id = $(this).data("user"),
                    token = "{{ csrf_token() }}";

                var _this = $(this);
                $.confirm({
                    title: '¿ Estas Seguro (a) ?',
                    content: 'Se eliminara el usuario.',
                    buttons: {
                        Confirmar: {
                            text: 'Confirmar',
                            btnClass: 'btn btn-primary btn-confirm',
                            action: function(){
                                $.ajax({
                                    url: "{{ route('deactivate.user') }}",
                                    type: "POST",
                                    data: {
                                        _token: token,
                                        id: id,
                                    },
                                }).done(function(result){
                                    _this.parents("tr").fadeOut("slow");
                                });
                            }
                        },
                        Cancelar: {
                            text: 'Cancelar',
                            btnClass: 'btn btn-secondary btn-confirm',
                        }
                    }
                });
            });
        });
    </script>
@endsection