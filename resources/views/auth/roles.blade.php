@extends('layouts.index')

@section('title') Roles @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Roles</h3>
        </div>
    </div>
    @can('write_roles')
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('new.role') }}" class="btn btn-primary mr-3" ><i class="fas fa-user-plus mr-2"></i> Crear Rol</a>
        </div>
    @endcan
    <div class="panel-body with-buttons">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary text-center" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="row margin-10 justify-content-center">
                @foreach ($roles as $role)
                    <div class="col-md-3 col-md-offset-3">
                        <div class="card">
                            <div class="card-image">
                                <i class="fas fa-user-tie"></i>
                            </div><!-- card image -->
                            <div class="card-content">
                                <span class="card-title">{{ $role->name }}</span>                    
                            </div><!-- card content -->
                            <div class="card-action">
                                <a href="{{ route('edit.role', $role->id) }}" class="btn-link">Editar</a>
                            </div><!-- card actions -->
                        </div>
                    </div>
                @endforeach   
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            $("#auth").addClass('active');
            
            $(document).on('dblclick', '.edit-role', function(){
                var id = $(this).data("id");
                window.location.href = "/autenticacion/editar/rol/"+id;
            });
        });
    </script>
@endsection