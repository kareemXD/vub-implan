<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Iniciar Sessión</title>

    {{-- Fonts --}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/all.min.css') }}">
    {{-- End Fonts --}}
    
    {{-- Styles --}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ mix('assets/css/login.css') }}">
    {{-- End Styles --}}
</head>
<body>
    <div class="wrapper d-flex">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-2 wrapper-login">
                    <div class="row">
                        <div class="login-header">
                            <img src="{{ asset('assets/images/logo_morena.png') }}" alt="Logo IMPLAN">
                        </div>
                    </div>
                    <div class="row">
                        <form method="POST" class="col-sm-12 login-body" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                            @csrf
    
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input id="email" type="email"  placeholder="Ingrese Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
    
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        Recuperar Password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    {{-- Scripts --}}
    <script src="{{ asset('assets/plugins/jquery/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
    {{-- End Scripts --}}
</body>
</html>