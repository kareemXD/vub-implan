@extends('layouts.index')

@section('title') Editar Permiso @endsection

@section('css')
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Actualizar permiso</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary text-center" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <form action="{{ route('auth.permissions.update') }}" method="POST">
                @csrf
                @method('PUT')
                <input type="hidden" name="permissionId" value="{{ $permission->id }}">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control" value="{{ old('name', $permission->name) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Guard</label>
                            <input type="text" name="guard" class="form-control" value="{{ old('guard', $permission->guard_name) }}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Label</label>
                            <input type="text" name="name" class="form-control" value="{{ old('label', $permission->label) }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group mt-1 mb-1 col-md-2">
                        <label for="status">Custom</label>
                        <input autocomplete="off" type="checkbox" data-toggle="toggle" data-on="Si" data-off="No" class="form-control " id="custom" name="custom" {{ $permission->custom == 1? 'checked' : '' }}>
                    </div>
                </div>
                <br>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </form>
        </div>
    </div>   
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
<script>
    $(function(){

    });
</script>
@endsection