@extends('layouts.index')

@section('title') Editar Role @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Permisos</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary text-center" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <table class="table table-striped">
                <thead class="thead-light">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Guard name</th>
                        <th>Custom</th>
                        <th>Label</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($permissions as $permission)
                    <tr>
                        <td><a href="{{ route('auth.permissions.edit', $permission->id) }}">{{ $permission->id }}</a></td>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->guard_name }}</td>
                        <td>{{ $permission->custom }}</td>
                        <td>{{ $permission->label }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>   
@endsection

@section('js')
<script>
    $(function(){

    });
</script>
@endsection