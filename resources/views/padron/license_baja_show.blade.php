@extends('layouts.index')

@section('title') Licencia   ( {{ $bajaLicencia->NumeroLicencia }} ) @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Baja: Licencia ( {{ $bajaLicencia->NumeroLicencia }} )</h3>
    </div>
</div>
@php
    $turndetails = $bajaLicencia->getTurnsDetails();
@endphp
<div class="panel-body @can('write_requests') with-buttons @endcan">
    <div class="margin-fix panel-row-fluid">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-link active" id="period-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Ubicación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="data-tab" data-toggle="tab" href="#data" role="tab" aria-controls="data" aria-selected="true">Solicitud</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link" id="period-tab" data-toggle="tab" href="#turn" role="tab" aria-controls="turn" aria-selected="false">Giro</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link" id="period-tab" data-toggle="tab" href="#requests" role="tab" aria-controls="requests" aria-selected="false">Tramites</a>
            </li>
        </ul>
        <form id="form-data" action="#" method="POST" class="tab-content small-form" style="height:calc(100% - 60px)">
            @csrf
            <input type="hidden" name="id" value="{{ $bajaLicencia->NumeroLicencia }}">
            <input type="hidden" name="operation" value="update">
            <div class="tab-pane fade show active" id="location" role="tabpanel" aria-labelledby="location-tab">
                @if(session()->has('alert'))
                    <div class="alert alert-primary" role="alert">
                        {{ session("alert") }}
                    </div>
                @endif
                <div class="form-group row margin-10 mt-2">
                    <label for="city" class="col-sm-2 col-form-label" >Poblacion</label>
                    <div class="col-sm-10">
                        <select disabled name="city" id="city" class="select2 form-control  {{ $errors->has('city') ? ' is-invalid' : '' }}">
                            <option value="0">Seleccione una Población</option>
                            @foreach ($poblaciones as $poblacion)
                                @if ($poblacion->IdPoblacion == $bajaLicencia->IdPoblacion)
                                    <option selected value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @else
                                    <option value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10 mt-2">
                    <label for="colony" class="col-sm-2 col-form-label">Colonia</label>
                    <div class="col-sm-4">
                        <select disabled name="colony" id="colony" class="select2 form-control {{ $errors->has('colony') ? ' is-invalid' : '' }}">
                            <option value="0">Seleccione una Colonia</option>
                        </select>
                        @if ($errors->has('colony'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('colony') }}</strong>
                            </span>
                        @endif
                    </div>
                    <label for="zone" class="col-sm-2 col-form-label">Zona</label>
                    <div class="col-sm-4">
                        <select disabled name="zone" id="zone" class="select2 form-control {{ $errors->has('zone') ? ' is-invalid' : '' }}">
                            <option value="0">Seleccione una Zona</option>
                            @foreach ($zonas as $zona)
                                @if ($zona->IdZona == $bajaLicencia->IdZona)
                                    <option selected value="{{ $zona->IdZona }}">{{ $zona->Nombre }}</option>
                                @else
                                    <option value="{{ $zona->IdZona }}">{{ $zona->Nombre }}</option>
                                @endif
                                <option value="{{ $zona->IdZona }}">{{ $zona->Nombre }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('zone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('zone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10 ">
                    <label for="domicilio2" class="col-sm-2 col-form-label">Calle</label>
                    <div class="col-sm-10">
                        <select disabled name="domicilio2" id="domicilio2" class="select2 form-control {{ $errors->has('domicilio2') ? ' is-invalid' : '' }}" >
                            <option value="">Seleccione una Calle</option>
                        </select>
                        @if ($errors->has('domicilio2'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('domicilio2') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="calle1_u" class="col-sm-2 col-form-label">Entre Calle</label>
                    <div class="col-sm-6">
                        <select disabled name="calle1_u" id="calle1_u" class="select2 form-control {{ $errors->has('calle1_u') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Entre Calle</option>
                        </select>
                        @if ($errors->has('calle1_u'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle1_u') }}</strong>
                            </span>
                        @endif
                    </div>
                    <label for="ext" class="col-sm-2 col-form-label">Exterior</label>
                    <div class="col-sm-2">
                        <input disabled type="number" class="form-control {{ $errors->has('ext') ? ' is-invalid' : '' }}" id="ext" name="ext" value="{{ $bajaLicencia->Exterior }}">
                        @if ($errors->has('ext'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ext') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="calle2_u" class="col-sm-2 col-form-label">Y Calle</label>
                    <div class="col-sm-6">
                        <select disabled name="calle2_u" id="calle2_u" class="select2 form-control {{ $errors->has('calle2_u') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Entre Calle</option>
                        </select>
                        @if ($errors->has('calle2_u'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle2_u') }}</strong>
                            </span>
                        @endif
                    </div>
                    <label for="int" class="col-sm-2 col-form-label">Interior</label>
                    <div class="col-sm-2">
                        <input disabled type="number" class="form-control {{ $errors->has('int') ? ' is-invalid' : '' }}" id="int" name="int" value="{{ $bajaLicencia->Interior }}">
                        @if ($errors->has('int'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('int') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="este" class="col-sm-2 col-form-label">Coordenada Este</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="form-control" id="este" name="este" value="{{ $bajaLicencia->CoordenadaEste }}">
                    </div>
                    <label for="norte" class="col-sm-2 col-form-label">Coordenada Norte</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="form-control" id="norte" name="norte" value="{{ $bajaLicencia->CoordenadaNorte }}">
                    </div>
                </div>
                <div id="map" class="map" style="width:100%; height:500px;"></div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="button" class="btn btn-primary mr-2 mb-2 mt-2 btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
            
            <div class="tab-pane fade " id="data" role="tabpanel" aria-labelledby="data-tab">
                
                <div class="form-row mt-3 d-none">
                    <div class="form-group col-4">
                        <label for="idtaxpayer">Contribuyente</label>
                        <input disabled maxlength="100" type="hidden" class="form-control {{ $errors->has('idtaxpayer') ? ' is-invalid' : '' }}" id="idtaxpayer" name="idtaxpayer" value="{{ $bajaLicencia->IdContribuyente }}" >
                        @if ($errors->has('idtaxpayer'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('idtaxpayer') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-2" >
                        <label for=""> </label>
                        <button type="button" class="btn btn-primary btn-block btn-search-taxpayer d-none"><i class="fas fa-search"></i></button>
                    </div>
                </div>
                <br>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="nombre">Nombre</label>
                        <input disabled required maxlength="100" type="text" class="form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre') }}" >
                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="apellido_pat">Apellido Paterno</label>
                        <input disabled required maxlength="50" type="text" class="form-control {{ $errors->has('apellido_pat') ? ' is-invalid' : '' }}" id="apellido_pat" name="apellido_pat" value="{{ old('apellido_pat') }}" >
                        @if ($errors->has('apellido_pat'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellido_pat') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="apellido_mat">Apellido Materno</label>
                        <input disabled required maxlength="50" type="text" class="form-control {{ $errors->has('apellido_mat') ? ' is-invalid' : '' }}" id="apellido_mat" name="apellido_mat" value="{{ old('apellido_mat') }}">
                        @if ($errors->has('apellido_mat'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellido_mat') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="rfc">R.F.C</label>
                        <input disabled required maxlength="15" type="text" class="form-control {{ $errors->has('rfc') ? ' is-invalid' : '' }}" id="rfc" name="rfc" value="{{ old('rfc') }}" placeholder="XAXX010101000">
                        @if ($errors->has('rfc'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('rfc') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="sexo">Sexo</label>
                        <select disabled name="sexo" id="sexo" class="form-control {{ $errors->has('sexo') ? ' is-invalid' : '' }}">
                            <option value="I">Seleccione Sexo</option>
                            <option value="I">Indefinido</option>
                            <option value="M">Hombre</option>
                            <option value="F">Mujer</option>
                        </select>
                        @if ($errors->has('sexo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('sexo') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="persona">Persona Moral ?</label>
                        <input disabled type="checkbox" data-toggle="toggle"  data-on="Si" data-off="No" class="form-control " id="persona" name="persona">
                        @if ($errors->has('persona'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('persona') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="email">Correo</label>
                        <input disabled type="email" maxlength="50" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" >
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="telefono_casa">Telefono Casa</label>
                        <input disabled type="number" maxlength="15" class="form-control {{ $errors->has('telefono_casa') ? ' is-invalid' : '' }}" id="telefono_casa" name="telefono_casa" value="{{ old('telefono_casa') }}" >
                        @if ($errors->has('telefono_casa'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telefono_casa') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="telefono_trab">Telefono Trabajo</label>
                        <input disabled type="number" maxlength="15" class="form-control {{ $errors->has('telefono_trab') ? ' is-invalid' : '' }}" id="telefono_trab" name="telefono_trab" value="{{ old('telefono_trab') }}" >
                        @if ($errors->has('telefono_trab'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telefono_trab') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="domicilio">Domicilio</label>
                        <input disabled required maxlength="60" type="text" class="form-control {{ $errors->has('domicilio') ? ' is-invalid' : '' }}" id="domicilio" name="domicilio" value="{{ old('domicilio') }}" >
                        @if ($errors->has('domicilio'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="calle1">Entre la Calle</label>
                        <input disabled type="text" maxlength="60" class="form-control {{ $errors->has('calle1') ? ' is-invalid' : '' }}" id="calle1" name="calle1" value="{{ old('calle1') }}" >
                        @if ($errors->has('calle1'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle1') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="calle2">y la Calle</label>
                        <input disabled type="text" maxlength="60" class="form-control {{ $errors->has('calle2') ? ' is-invalid' : '' }}" id="calle2" name="calle2" value="{{ old('calle2') }}" >
                        @if ($errors->has('calle2'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle2') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="exterior">Numero Exterior</label>
                        <input disabled required type="text" maxlength="50" class="form-control {{ $errors->has('exterior') ? ' is-invalid' : '' }}" id="exterior" name="exterior" value="{{ old('exterior') }}" >
                        @if ($errors->has('exterior'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('exterior') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="interior">Numero Interior</label>
                        <input disabled type="text" maxlength="50" class="form-control {{ $errors->has('interior') ? ' is-invalid' : '' }}" id="interior" name="interior" value="{{ old('interior') }}" >
                        @if ($errors->has('interior'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('interior') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="cp">C.P.</label>
                        <input disabled required type="number" maxlength="5" class="form-control {{ $errors->has('cp') ? ' is-invalid' : '' }}" id="cp" name="cp" value="{{ old('cp') }}" >
                        @if ($errors->has('cp'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cp') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="localidad">Localidad</label>
                        <input disabled required maxlength="40" type="text" class="form-control {{ $errors->has('localidad') ? ' is-invalid' : '' }}" id="localidad" name="localidad" value="{{ old('localidad') }}" >
                        @if ($errors->has('localidad'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('localidad') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="colonia">Colonia</label>
                        <input disabled required maxlength="40" type="text" class="form-control {{ $errors->has('colonia') ? ' is-invalid' : '' }}" id="colonia" name="colonia" value="{{ old('colonia') }}" >
                        @if ($errors->has('colonia'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('colonia') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="button" class="btn btn-primary mr-2 mb-2 mt-2 btn-next" data-target="#turn">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

            <div class="tab-pane fade" id="turn" role="tabpanel" aria-labelledby="turn-tab">
                <div class="form-group row mt-2 margin-10">
                    <label for="name_b" class="col-sm-2 col-form-label">Nombre del Negocio</label>
                    <div class="col-sm-10">
                        <input disabled type="text" class="form-control {{ $errors->has('name_b') ? ' is-invalid' : '' }}" id="name_b" name="name_b" value="{{ $bajaLicencia->NombreNegocio }}">
                        @if ($errors->has('name_b'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name_b') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="linderos" class="col-sm-2 col-form-label">Linderos</label>
                    <div class="col-sm-10">
                        <textarea disabled class="form-control {{ $errors->has('linderos') ? ' is-invalid' : '' }}" id="linderos"  name="linderos" rows="2">{{ $bajaLicencia->Linderos }}</textarea>
                        @if ($errors->has('linderos'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('linderos') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="observations" class="col-sm-2 col-form-label">Observaciones</label>
                    <div class="col-sm-10">
                        <input disabled type="text" class="form-control {{ $errors->has('observations') ? ' is-invalid' : '' }}" id="observations" name="observations" value="{{ $bajaLicencia->Observaciones }}">
                        @if ($errors->has('observations'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('observations') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label data-legends="{{ json_encode($bajaLicencia->legends()->pluck("Leyendas.IdLeyenda")->toArray()) }}" for="legends" class="col-sm-2 col-form-label">Leyendas</label>
                    <div class="col-sm-10">
                        <select name="legends[]" id="legends" class="required select2 form-control {{ $errors->has('legends') ? ' is-invalid' : '' }}" multiple>
                            <option value="">Seleccione las Leyendas</option>
                            @foreach ($legends as $legend)
                                <option value="{{ $legend->IdLeyenda }}">{{ $legend->NombreLeyenda }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('legends'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('legends') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                @if (!empty($bajaLicencia->PermisoAlcohol))
                    <div class="form-group mt-1 mb-1 row margin-10" id="alcohol-div">
                        <label for="alcohol" class="col-sm-2 col-form-label">Permiso de Alcohol</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control {{ $errors->has('alcohol') ? ' is-invalid' : '' }}" id="alcohol" name="alcohol" value="{{ $bajaLicencia->PermisoAlcohol }}">
                            @if ($errors->has('alcohol'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('alcohol') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                @endif
                <div class="form-group row margin-10">
                    <label for="surface" class="col-sm-2 col-form-label">Superficie</label>
                    <div class="col-sm-4">
                        <input disabled type="number" step="0.00" class="form-control {{ $errors->has('surface') ? ' is-invalid' : '' }}" id="surface" name="surface" value="{{ $bajaLicencia->Superficie }}">
                        @if ($errors->has('surface'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('surface') }}</strong>
                            </span>
                        @endif
                    </div>
                    <label for="employees" class="col-sm-2 col-form-label">Empleados</label>
                    <div class="col-sm-4">
                        <input disabled type="number" class="form-control {{ $errors->has('employees') ? ' is-invalid' : '' }}" id="employees" name="employees" value="{{ $bajaLicencia->EmpleosCreados }}">
                        @if ($errors->has('employees'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('employees') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                    <thead>
                        <tr>
                            <th width="10%">IdGiro</th>
                            <th>Descripción</th>
                            <th>Observaciones</th>
                            <th width="10%">Fecha</th>
                            <th width="10%">Cuota</th>
                            <th width="10%">Cantidad</th>
                        </tr>
                    </thead>
                    <tbody id="turns-content">
                        
                    </tbody>
                </table>
                <div class="row margin-10 mt-2">
                    <div class="col-8">
                        <span>El costo de los anexos depende del porcentaje de pago para altas y refrendos registrados en preferencias y que en este momento es de <b>{{ $trimester["percentage"] }} %</b> del costo anual.</span>
                    </div>
                    <div class="col-4">
                        <div class="form-group row margin-10">
                            <label for="costo_f" class="col-sm-4 col-form-label">Costo Forma</label>
                            <div class="col-sm-8">
                                <input disabled type="text" class="form-control {{ $errors->has('costo_f') ? ' is-invalid' : '' }}" id="costo_f" value="$ 0.00">
                            </div>
                        </div>
                        <div class="form-group row mt-2 margin-10">
                            <label for="costo_t" class="col-sm-4 col-form-label">Costo Total</label>
                            <div class="col-sm-8">
                                <input disabled type="text" class="form-control {{ $errors->has('costo_t') ? ' is-invalid' : '' }}" id="costo_t" value="$ 0.00">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
            
            <div class="tab-pane fade" id="requests" role="tabpanel" aria-labelledby="requests-tab" style="height:100%">
                @if ($bajaLicencia->alerts()->where("PermitirTramites", 0)->count() > 0)
                    <div class="alert alert-primary" role="alert">
                        Esta licencia tiene una Alerta que no le permite realizar Trámites.
                    </div>
                @endif
                <div class="row row-top">
                    <div class="col-6 col-lft">
                        <div class="card">
                            <div class="card-header">
                                Trámites
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                                    <thead>
                                        <tr>
                                            <th>No. Solicitud</th>
                                            <th>Tipo Tramite</th>
                                            <th width="10%">Estatus</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="turns-content">
                                        @foreach ($bajaLicencia->requests as $request)
                                            <tr>
                                                <td width="30%">{{ $request->NumeroSolicitud }}</td>
                                                <td width="30%">
                                                @foreach ($request->process as $process)
                                                    {{ $process->Nombre . ", " }}
                                                @endforeach
                                                </td>
                                                <td width="10%" class="{{ ($request->Aplicado == 1)? 'bg-green' : 'bg-yellow' }}">{{ ($request->Aplicado == 1)? 'Aplicado' : 'En Tramite' }}</td>
                                                <td width="5%">
                                                    <a href="{{ route('update.request', [$request->NumeroSolicitud, "update"]) }}" class="btn btn-primary btn-action-table" data-toggle="tooltip" data-placement="top" title="Ver Solicitud"><i class="fas fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-rgt">
                        <div class="card">
                            <div class="card-header">
                                <span>Alertas</span>
                                <button type="button" class="btn btn-primary btn-action-table float-rigth" data-toggle="modal" data-target="#modal-add-alert"><i class="fas fa-plus"></i></button>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>De</th>
                                            <th>Alerta</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($bajaLicencia->alerts() as $alert)
                                            <tr>
                                                <td>{{ $alert->FechaAlta }}</td>
                                                <td>{{ $alert->De }}</td>
                                                <td>{{ $alert->Alerta }}</td>
                                                <td><button type="button" class="btn btn-primary btn-action-table btn-update-alert"
                                                            data-id="{{ $alert->IdAlerta }}"
                                                            data-date="{{ \Carbon\Carbon::parse($alert->FechaAlta)->toDateString() }}"
                                                            data-from="{{ $alert->De }}"
                                                            data-resquests="{{ $alert->PermitirTramites }}"
                                                            data-description="{{ $alert->Alerta }}"><i class="fas fa-edit"></i></button></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row row-bot">
                    {{-- <div class="col-6 col-lft">
                        <div class="card">
                            <div class="card-header">
                                Multas
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Usuario</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-12 col-rgt">
                        <div class="card">
                            <div class="card-header">
                                Bitacora
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Usuario</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($bajaLicencia->bitacora() as $item)
                                            <tr>
                                                <td>{{ $item->FechaAlta }}</td>
                                                <td>{{ $item->Usuario }}</td>
                                                <td>{{ $item->DescripcionCorta }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@can('write_requests')
    <div class="row justify-content-end panel-buttoms">
        <button type="button" class="btn btn-primary mr-3 btn-next " id="btn-previous" style="display:none;" data-target="#data"><i class="fas fa-arrow-left mr-2"></i> Anterior</button>
        <button type="button" class="btn btn-primary mr-3 btn-next" id="btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
    <a target="_blank" href="{{ route('print.license.baja', $bajaLicencia->NumeroLicencia) }}" style="display:none" type="button" class="btn btn-primary mr-3 btn-next" id="btn-print-baja" data-target="#btn-print-baja">Imprimir Baja <i class="fas fa-file ml-2"></i></a>
        
    </div>
@endcan
@endsection


@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>

    <script>
        window.addEventListener('load', function () {
            OnChangeCity("{{ $bajaLicencia->IdColonia }}");
            OnChangeColony("{{ $bajaLicencia->IdCalle }}", "{{ $bajaLicencia->IdCruce1 }}", "{{ $bajaLicencia->IdCruce2 }}");
            @if(session()->has('document_updated'))
                $('#myTab a[href="#documents"]').tab('show');
            @endif
            setTimeout(function(){ 
                $(".btn-search-taxpayer")[0].click();
                var cost = 0;
                $(".cost_search").each(function() {
                    cost += parseFloat($(this).val());
                });
                cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                $("#costo_t").val("$ " + cost.toFixed(2));
            }, 100);
        });
        $(function(){
            $("#padron").addClass('active');
            
            // $('#update_persona').bootstrapToggle('on')
            var created = false;
            var map = L.map('map',{scrollWheelZoom:true}).setView([20.74689, -105.39425], 12);
            // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            //     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap Saul Moncivais</a> contributors'
            // }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:Vuelo_Bajo_Ortofoto@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 12,
                maxZoom: 23,
                attribution: '&copy; IMPLAN - Ortofoto Bahia de Banderas 2019'
            }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:vialidad_BB@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);

            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:lim_loc_final@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);
            var theMarker = {};
            var item = L.utm({x: "{{ $bajaLicencia->CoordenadaEste }}", y: "{{ $bajaLicencia->CoordenadaNorte }}", zone: 13, band: 'N'});
            var coord = item.latLng();
            theMarker = L.marker(coord).addTo(map);
            map.on('click',function(e){
                if (theMarker != undefined) {
                    map.removeLayer(theMarker);
                };
                theMarker = L.marker(e.latlng).addTo(map);
                $("#este").val(theMarker.getLatLng().utm().x)
                $("#norte").val(theMarker.getLatLng().utm().y)
            });
            
            $(document).on("click", "#btn-save-taxpayer", function(){
                $("input[name=operation]").val('change_contributor');
                $("#form-data")[0].submit();
            });
            
            $(document).on("click", "#btn-applay-request", function(){
                $("input[name=operation]").val('apply_changes');
                $("#form-data")[0].submit();
            });

            $(document).on("click", "#btn-save-location", function(){
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción?',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            $("input[name=operation]").val('update_location');
                            $("#form-data")[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });
            
            $(document).on("click", ".btn-update-document", function(){
                var _this = $(this);
                $.ajax({
                    url: "{{ route('get.document.by.request') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        requisito: _this.data("requisito"),
                        solicitud: $("input[name=id]").val()
                    },
                }).done(function(result){
                    console.log(result);
                    var modal = $("#modal-update-document");
                    modal.find("#requisito_id").val(result.Requisito);
                    modal.find("#nombre_r").val(result.Nombre);
                    modal.find("#comments").val(result.pivot_Comentario);
                    if(result.pivot_Archivo)
                    {
                        modal.find("#documents-content").prepend('<ul><li class="download-file" data-requisito="'+result.Requisito+'" data-solicitud="'+$("input[name=id]").val()+'">\
                            <img src="{{ asset("assets/plugins/preview/preview_pdf.png") }}" >\
                        </li></ul>');
                        $('.old_document').simpleFilePreview();
                    }
                    modal.modal("show");
                });
            });
            
            $(document).on('click', '.btn-search-taxpayer', function(){

                var id = $("#idtaxpayer").val(),
                    token = "{{ csrf_token() }}";
                    legends = $("[data-legends]").data("legends");
                    if($("[data-tramite]").data("tramite") == "A"){
                        $("#legends").prop("disabled", false);
                    }else{
                        $("#legends").prop("disabled", true);
                    }
                    $("#legends").select2("val", legends);
                    $('#update_persona').bootstrapToggle('off')
                $.ajax({
                    url: "{{ route('get.taxpayer.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $('#taxpayer').val(result.IdContribuyente);
                    $('#cuenta').val(result.IdContribuyente);
                    $('#rfc').val(result.RFC);
                    $('#nombre').val(result.Nombre);
                    $('#apellido_pat').val(result.ApellidoPaterno);
                    $('#apellido_mat').val(result.ApellidoMaterno);
                    if(result.Sexo)
                    {
                        $('#sexo').val(result.Sexo);
                    }else
                    {
                        $('#sexo').val("I");
                    }
                    if(result.PersonaMoral == 1)
                    {
                        $('#persona').bootstrapToggle('on')
                    }
                    $('#email').val(result.Email);
                    $('#exterior').val(result.Exterior);
                    $('#interior').val(result.Interior);
                    $('#cp').val(result.CP);
                    $('#localidad').val(result.Ciudad);
                    $('#colonia').val(result.Colonia);
                    $('#domicilio').val(result.Domicilio);
                    $('#calle1').val(result.Cruce1);
                    $('#calle2').val(result.Cruce2);
                    $('#telefono_casa').val(result.TelefonoCasa);
                    $('#telefono_trab').val(result.TelefonoTrabajo);
                });
            });
            
            $(document).on('focusout', '#cuenta_serach', function(){
                var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.turn.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $('#description_search').val(result.Nombre);
                    $('#cost_search').val(result.CuotaFija);
                    $('#quantity_search').val(1);
                });
            });

            $(document).on("click", ".btn-add-turn", function(){
                var flag = true;
                $(this).parents("table").find(".custom_input").each(function() {
                    if(!$(this).val()){
                        flag = false;
                        $(this).addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                        }
                    }
                });
                if(flag){
                    var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                    $.ajax({
                        url: "{{ route('get.license.cost') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: $("#cuenta_serach").val(),
                            date: $("#date_search").val()
                        },
                    }).done(function(result){

                        $("#turns-content").append('\
                            <tr>\
                                <td><input type="hidden" class="cuenta_serach" name="cuenta_serach[]" value="'+$("#cuenta_serach").val()+'"/>'+$("#cuenta_serach").val()+'</td>\
                                <td><input type="hidden" class="description_search" name="description_search[]" value="'+$("#description_search").val()+'"/>'+$("#description_search").val()+'</td>\
                                <td><input type="hidden" class="observations_search" name="observations_search[]" value="'+$("#observations_search").val()+'"/>'+$("#observations_search").val()+'</td>\
                                <td><input type="hidden" class="date_search" name="date_search[]" value="'+$("#date_search").val()+'"/>'+$("#date_search").val()+'</td>\
                                <td><input type="hidden" class="cost_search" name="cost_search[]" value="'+parseFloat(result)+'"/>$ '+parseFloat(result).toFixed(2)+'</td>\
                                <td><input type="hidden" class="quantity_search" name="quantity_search[]" value="'+$("#quantity_search").val()+'"/>'+$("#quantity_search").val()+'</td>\
                                <td><button type="button" class="btn btn-danger btn-action-table btn-delete-turn"><i class="fas fa-trash"></i></button></td>\
                            </tr>');
                        $("#cuenta_serach").val("");
                        $("#description_search").val("");
                        $("#observations_search").val("");
                        $("#date_search").val("");
                        $("#cost_search").val("");
                        $("#quantity_search").val("");
                        var cost = 0;
                        $(".cost_search").each(function() {
                            cost += parseFloat($(this).val());
                        });
                        cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                        $("#costo_t").val("$ " + cost.toFixed(2));
                    });
                }
            });
            
            $(document).on("click", ".btn-delete-turn", function(){
                var _this = $(this);
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción?',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            _this.parents("tr").fadeOut("slow", function(){
                                _this.parents("tr").remove();
                                var cost = 0;
                                $(".cost_search").each(function() {
                                    cost += parseFloat($(this).val());
                                });
                                cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                                $("#costo_t").val("$ " + cost.toFixed(2));
                            });
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });

            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var target = e.target.hash;
                switch (e.relatedTarget.hash) {
                    case "#data":
                        if(!$("#idtaxpayer").val())
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione un contribuyente para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        break;
                
                    case "#location":
                        if(!$("#norte").val() || !$("#este").val())
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione la ubicación para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        break;
                    case "#requests":
                        // $("#btn-save-changes").fadeOut("fast");
                        // $("#btn-applay-request").fadeOut("fast");
                        $("#btn-next").fadeIn("fast");
                        $("#btn-print").fadeOut("fast");
                        $("#btn-print-cost").fadeOut("fast");
                        $("#btn-ampliacion").fadeOut("fast");
                        $("#btn-domicilio").fadeOut("fast");
                        $("#btn-repocision").fadeOut("fast");
                        $("#btn-cambio").fadeOut("fast");
                        $("#btn-traspaso").fadeOut("fast");
                        $("#btn-anexo").fadeOut("fast");
                        break;
                }

                switch (e.target.hash) {
                    case "#location":
                        $("#btn-previous").fadeOut("fast")
                        $("#btn-next").data("target", "#data")
                        break;
                    case "#data":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#location")
                        });
                        $("#btn-next").data("target", "#turn")
                        break;
                    case "#turn":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#data")
                        });
                        $("#btn-next").data("target", "#requests")
                        break;
                    case "#requests":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#turn")
                        });
                        $("#btn-next").fadeOut("fast");
                        $("#btn-print").fadeIn("fast");
                        $("#btn-print-cost").fadeIn("fast");
                        $("#btn-ampliacion").fadeIn("fast");
                        $("#btn-domicilio").fadeIn("fast");
                        $("#btn-repocision").fadeIn("fast");
                        $("#btn-cambio").fadeIn("fast");
                        $("#btn-traspaso").fadeIn("fast");
                        $("#btn-anexo").fadeIn("fast");
                        break;
                }
            });
            $("[href='#requests']").on("show.bs.tab", function(event){
                $("#btn-print-baja").fadeIn(500);
            });
            $("[href='#requests']").on("hide.bs.tab", function(event){
                $("#btn-print-baja").fadeOut(500);
            });

            $(document).on("click", ".btn-next", function(){
                $('#myTab a[href="'+$(this).data("target")+'"]').tab('show');
            });

            $(document).on("change", "#city", function(){
                OnChangeCity();
            });
            $(document).on("change", "#colony", function(){
                OnChangeColony();
            });

            //agregar taxapyer

            $("[href='#data']").on("show.bs.tab", function(event){
                $("#btn-save-taxpayer").fadeIn(500);
                $("#btn-save")
            });
            $("[href='#data']").on("hide.bs.tab", function(event){
                $("#btn-save-taxpayer").fadeOut(500);
            });
        });

        function OnChangeCity(colony = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.colonies.by.city') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#colony").empty();
                $("#colony").append("<option value='0'>Seleccione una Colonia</option>");
                result.forEach(element => {
                    $("#colony").append($('<option>', {
                        value: element.IdColonia,
                        text: element.NombreColonia
                    }));
                });
                if(colony)
                {
                    $("#colony").val(colony);
                }
            });

            $(document).on("click", ".download-file", function(){
                var requisito = $(this).data("requisito");
                var solicitud = $(this).data("solicitud");
                window.open('/padron/mostrar/documento/'+requisito+'/'+solicitud, '_blank');
            });
        }

        function OnChangeColony(calle = null, calle1 = null, calle2 = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.calles.by.colony') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#domicilio2").empty();
                $("#domicilio2").append("<option value='0'>Seleccione una Calle</option>");

                $("#calle1_u").empty();
                $("#calle1_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                $("#calle2_u").empty();
                $("#calle2_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                result.forEach(element => {
                    $("#domicilio2").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle1_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle2_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));
                });
                if(calle || calle1 || calle2)
                {
                    $("#domicilio2").val(calle);
                    $("#calle1_u").val(calle1);
                    $("#calle2_u").val(calle2);
                }
            });

            /******************************************************************
            *
            * Funtions for click Events
            *
            *******************************************************************/
            $(document).on("click", ".btn-update-alert", function(){
                var modal = $("#modal-add-alert");
                var alert = $(this).data('id');
                var date = $(this).data('date');
                var from = $(this).data('from');
                var resquests = $(this).data('resquests');
                var description = $(this).data('description');
                // alert(alert);
                modal.find('input[name=operation]').val("update");
                modal.find('input[name=alert]').val(alert);
                modal.find('#date').val(date);
                modal.find('#from').val(from);
                if(resquests == 1){
                    modal.find('#resquests').prop('checked', true); 
                }else{
                    modal.find('#resquests').prop('checked', false); 
                }
                modal.find('#description').val(description);
                modal.modal("show");
            });
        }
    </script>
@endsection