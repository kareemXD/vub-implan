@extends('layouts.index')

@section('title') Editar Giro @endsection

@section('css')
    
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Editar Giro</h3>
    </div>
</div>
<div class="panel-body">
    <div class="margin-fix panel-row-fluid">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-link active" id="data-tab" data-toggle="tab" href="#data" role="tab" aria-controls="data" aria-selected="true">Informacion</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link" id="period-tab" data-toggle="tab" href="#period" role="tab" aria-controls="period" aria-selected="false">Historico</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="data" role="tabpanel" aria-labelledby="data-tab">
                @can('write_turns')
                <form action="{{ route('store.update.turn') }}" method="POST">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                @endcan
                    @csrf
                    <input type="hidden" name="id" value="{{ $turn->IdGiro }}">
                    <div class="row margin-10 justify-content-center padding-15">
                        <fieldset class="col-xl-12 col-sm-12 fieldset pb-3">
                            <legend class="legend">Información General</legend>
                            <div class="form-row">
                                <div class="col-6">
                                    <label for="name">Nombre</label>
                                    <input  @can('read_turns') disabled @endcan type="text"  class="form-control" id="name" name="name" value="{{ $turn->Nombre }}" required>
                                </div>
                                @php
                                    $now = Carbon\Carbon::now("America/Mexico_City")->format("Y");
                                    $actualPeriod = $turn->periods()->where("Ano", $now)->first();
                                    $giroCuota = ($turn->periods()->where("Ano", \Carbon\Carbon::now('America/Mexico_City')->format('Y'))->first());
                                    if(is_null($giroCuota)){
                                        $giroCuota = ($turn->periods()->where("Ano", (\Carbon\Carbon::now('America/Mexico_City')->format('Y')- 1))->first());
                                    }

                                    $cuotaMinima = 0;
                                    // $cuotaMaxima = 0;
                                    if (!empty($giroCuota))
                                    {
                                        $cuotaMinima = $giroCuota->CuotaMinima;
                                        // $cuotaMaxima = $giroCuota->CuotaMaxima;
                                    }
                                @endphp
                                <div class="col-3">
                                    <label for="cuota_fija">Cuota Fija</label>
                                    <input  @can('read_turns') disabled @endcan type="number" step="0.01" class="form-control" id="cuota_fija" name="cuota_fija" value="{{ $turn->CuotaFija }}" required>
                                </div>
                                <div class="col-3">
                                    <label for="cuota_minima">Refrendo</label>
                                    <input  @can('read_turns') disabled @endcan type="number" step="0.01" class="form-control" id="cuota_minima" name="cuota_minima" value="{{ $cuotaMinima }}" required>
                                </div>
                                {{-- <div class="col-2">
                                    <label for="cuota_maxima">Cuota Maxima</label>
                                    <input  @can('read_turns') disabled @endcan type="number" step="0.01" class="form-control" id="cuota_maxima" name="cuota_maxima" value="{{ $cuotaMaxima }}" required>
                                </div> --}}
                            </div>
                            <div class="form-row">
                                <div class="col">
                                    <label for="tipo">Tipo Giro</label>
                                    <select @can('read_turns') disabled @endcan required class="form-control select2" name="tipo" id="tipo">
                                        <option value="">Selecciones Tipo de Giro</option>
                                        @foreach ($tiposGiro as $tipoGiro)
                                            @if ($tipoGiro->TipoGiro == $turn->TipoGiro)
                                                <option selected value="{{ $tipoGiro->TipoGiro }}">{{ $tipoGiro->Nombre }}</option>
                                            @else
                                                <option value="{{ $tipoGiro->TipoGiro }}">{{ $tipoGiro->Nombre }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <label for="subtipo">SubTipo Giro</label>
                                    <select @can('read_turns') disabled @endcan required class="form-control select2" name="subtipo" id="subtipo">
                                        <option value="">Selecciones SubTipo de Giro</option>
                                        @foreach ($subTiposGiro as $subTipoGiro)
                                            @if ($subTipoGiro->SubTipoGiro == $turn->SubTipoGiro)
                                                <option selected value="{{ $subTipoGiro->SubTipoGiro }}">{{ $subTipoGiro->Nombre }}</option>
                                            @else
                                                <option value="{{ $subTipoGiro->SubTipoGiro }}">{{ $subTipoGiro->Nombre }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <label for="riesgo">Riesgo</label>
                                    <select @can('read_turns') disabled @endcan required class="form-control select2" name="riesgo" id="riesgo">
                                        <option value="">Selecciones Riesgo</option>
                                        @foreach ($riesgosGiros as $riesgoGiros)
                                            @if ($riesgoGiros->Riesgo == $turn->Riesgo)
                                                <option selected value="{{ $riesgoGiros->Riesgo }}">{{ $riesgoGiros->Nombre }}</option>
                                            @else
                                                <option value="{{ $riesgoGiros->Riesgo }}">{{ $riesgoGiros->Nombre }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col">
                                    <label for="clase">Clase</label>
                                    <select @can('read_turns') disabled @endcan required class="form-control select2" name="clase" id="clase">
                                        <option value="">Selecciones Clase</option>
                                        @foreach ($clasesGiros as $claseGiros)
                                            @if ($claseGiros->IdClase == $turn->IdClase)
                                                <option selected value="{{ $claseGiros->IdClase }}">{{ $claseGiros->Nombre }}</option>
                                            @else
                                                <option value="{{ $claseGiros->IdClase }}">{{ $claseGiros->Nombre }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-4">
                                    <label for="concepto">Concepto</label>
                                    <select @can('read_turns') disabled @endcan required class="form-control select2" name="concepto" id="concepto">
                                        <option value="">Selecciones el concepto</option>
                                        @foreach ($conceptos as $concepto)
                                            @if ($concepto->Concepto == $turn->Concepto)
                                                <option selected value="{{ $concepto->Concepto }}">({{ $concepto->Concepto }}) {{ $concepto->Descripcion }}</option>
                                            @else
                                                <option value="{{ $concepto->Concepto }}">({{ $concepto->Concepto }}) {{ $concepto->Descripcion }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4">
                                    <label for="claveHacienda">Clave Hacienda</label>
                                    <select @can('read_turns') disabled @endcan class="form-control select2" name="claveHacienda" id="claveHacienda">
                                        <option value="">Selecciones la clave</option>
                                        @if ($turn->CveHacienda == "SARE")
                                            <option selected value="SARE">SARE</option>
                                        @else
                                            <option value="SARE">SARE</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="col-4">
                                    <label for="alcohol">¿Alcohol?</label>
                                    <select @can('read_turns') disabled @endcan class="form-control select2" name="alcohol" id="alcohol">
                                        <option @if($turn->Alcohol != true) selected @endif value="0">No</option>
                                        <option @if($turn->Alcohol == true) selected @endif value="1" >Sí</option>
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="row margin-10 justify-content-center padding-15">
                        <fieldset class="col-xl-12 col-sm-12 fieldset">
                            <legend class="legend">Requisitos</legend>
                            <table class="table table-sm table-striped">
                                <thead>
                                    <tr>
                                        <th>Documento</th>
                                        <th class="text-center">Alta</th>
                                        <th class="text-center">Cambio Domicilio</th>
                                        <th class="text-center">Cambio Giro</th>
                                        <th class="text-center">Traspaso</th>
                                        <th class="text-center">Repocision Licencia</th>
                                        <th class="text-center">Baja</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($documents as $document)
                                        <tr>
                                            <td>{{ $document->Nombre }}</td>
                                            <td align="center" ><input @can('read_turns') disabled @endcan type="checkbox" value="1" name="check_{{ $document->Requisito }}_al" {{ (array_key_exists("check_".$document->Requisito."_al", $turnConfig)) ? "Checked" : ""}}></td>
                                            <td align="center" ><input @can('read_turns') disabled @endcan type="checkbox" value="1" name="check_{{ $document->Requisito }}_cd" {{ (array_key_exists("check_".$document->Requisito."_cd", $turnConfig)) ? "Checked" : ""}}></td>
                                            <td align="center" ><input @can('read_turns') disabled @endcan type="checkbox" value="1" name="check_{{ $document->Requisito }}_cg" {{ (array_key_exists("check_".$document->Requisito."_cg", $turnConfig)) ? "Checked" : ""}}></td>
                                            <td align="center" ><input @can('read_turns') disabled @endcan type="checkbox" value="1" name="check_{{ $document->Requisito }}_tr" {{ (array_key_exists("check_".$document->Requisito."_tr", $turnConfig)) ? "Checked" : ""}}></td>
                                            <td align="center" ><input @can('read_turns') disabled @endcan type="checkbox" value="1" name="check_{{ $document->Requisito }}_rl" {{ (array_key_exists("check_".$document->Requisito."_rl", $turnConfig)) ? "Checked" : ""}}></td>
                                            <td align="center" ><input @can('read_turns') disabled @endcan type="checkbox" value="1" name="check_{{ $document->Requisito }}_ba" {{ (array_key_exists("check_".$document->Requisito."_ba", $turnConfig)) ? "Checked" : ""}}></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                @can('write_turns')
                    <div class="row margin-10 justify-content-center ">
                        <button type="submit" class="btn btn-primary mr-2 mb-2 mt-2"><i class="fas fa-save mr-2"></i> Guardar</button>
                    </div>
                </form>
                @endcan
            </div>
            <div class="tab-pane fade" id="period" role="tabpanel" aria-labelledby="period-tab">
                <div class="row justify-content-center padding-15">
                    @can('write_turns')
                    <a class="btn btn-primary" href="#" data-toggle="modal" data-target="#modal_add_turn"><i class="fa fa-plus"></i></a>
                    @endcan
                    <fieldset class="col-xl-12 col-sm-12 fieldset pb-3">
                        <legend class="legend">Información de Cuotas por Periodo</legend>
                        <table class="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Periodo</th>
                                    <th width="10%" class="text-center" scope="col">Cuota Fija</th>
                                    <th width="10%" class="text-center" scope="col">Refrendo</th>
                                    {{-- <th width="10%" class="text-center" scope="col">Cuota Maxima</th> --}}
                                    @can('write_turns')
                                        <th width="10%" class="text-center" scope="col">Acciones</th>
                                    @endcan
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($turn->periods() as $period)
                                    <tr>
                                        <td><input type="hidden" class="period" name="period" value="{{ $period->Ano }}">{{ $period->Ano }}</td>
                                        <td align="center" class="cuota_fija">$ {{ $period->CuotaFija }}</td>
                                        <td align="center" class="cuota_minima">$ {{ $period->CuotaMinima }}</td>
                                        {{-- <td align="center" class="cuota_maxima">$ {{ $period->CuotaMaxima }}</td> --}}
                                        @can('write_turns')
                                            <td align="center" class="actions"><button type="button" data-id="{{ $period->IdGiro }}" data-period="{{ $period->Ano }}" data-toggle="tooltip" data-placement="top" title="Editar periodo" class="btn btn-primary update-period"><i class="fas fa-edit"></i></button></td>
                                        @endcan
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modals')
@include('partials.modals.padron.modal_add_turn')
@endsection

@section('js')
    <script>
        $(function(){
            @if(count($errors) > 0)
                $("#period-tab").click();
                $("#modal_add_turn").modal("show");
                $("#modal_add_turn").find(".invalid-feedback").attr("style:display:block");
            @endif

            $("#padron").addClass('active');
            $(document).on("click", ".update-period", function(){
                var id = $(this).data("id");
                var period = $(this).data("period");
                $('[data-toggle="tooltip"]').tooltip("hide");

                var cuota_fija = $(this).parents("tr").find(".cuota_fija");
                var cuota_fija_val = $(this).parents("tr").find(".cuota_fija").html();
                cuota_fija.html("<input maxlength='11' type='number' class='form-control cuota_fija-input' value='"+cuota_fija_val.split("$ ")[1]+"'/>");

                var cuota_minima = $(this).parents("tr").find(".cuota_minima");
                var cuota_minima_val = $(this).parents("tr").find(".cuota_minima").html();
                cuota_minima.html("<input maxlength='11' type='number' class='form-control cuota_minima-input' value='"+cuota_minima_val.split("$ ")[1]+"'/>");

                // var cuota_maxima = $(this).parents("tr").find(".cuota_maxima");
                // var cuota_maxima_val = $(this).parents("tr").find(".cuota_maxima").html();
                // cuota_maxima.html("<input maxlength='11' type='number' class='form-control cuota_maxima-input' value='"+cuota_maxima_val.split("$ ")[1]+"'/>");

                var actions = $(this).parents("tr").find(".actions");
                actions.html('<button type="button" class="btn btn-success save-period" data-toggle="tooltip" data-placement="top" title="Guardar" data-period="'+period+'" data-id="'+id+'"><i class="fas fa-save"></i></button>\
                            <button type="button" class="btn btn-danger cancel-period" data-toggle="tooltip" data-placement="top" title="Cancelar" data-period="'+period+'" data-id="'+id+'"><i class="fas fa-ban"></i></button>');
                $('[data-toggle="tooltip"]').tooltip();
            });
            
            $(document).on("click", ".cancel-period", function(){
                var id = $(this).data("id");
                var period = $(this).data("period");
                $('[data-toggle="tooltip"]').tooltip("hide");

                var cuota_fija = $(this).parents("tr").find(".cuota_fija");
                var cuota_fija_val = $(this).parents("tr").find(".cuota_fija").find("input").val();
                cuota_fija.html("$ "+cuota_fija_val);

                var cuota_minima = $(this).parents("tr").find(".cuota_minima");
                var cuota_minima_val = $(this).parents("tr").find(".cuota_minima").find("input").val();
                cuota_minima.html("$ "+cuota_minima_val);

                // var cuota_maxima = $(this).parents("tr").find(".cuota_maxima");
                // var cuota_maxima_val = $(this).parents("tr").find(".cuota_maxima").find("input").val();
                // cuota_maxima.html("$ "+cuota_maxima_val);

                var actions = $(this).parents("tr").find(".actions");
                actions.html('<button type="button" class="btn btn-primary update-period" data-toggle="tooltip" data-placement="top" title="Editar periodo" data-period="'+period+'" data-id="'+id+'"><i class="fas fa-edit"></i></button>');
                $('[data-toggle="tooltip"]').tooltip();
            });

            $(document).on("click", ".save-period", function(){
                var _this = $(this);
                var id = $(this).data("id");
                var period = $(this).data("period");
                var cuota_fija_val = $(this).parents("tr").find(".cuota_fija").find("input").val();
                var cuota_minima_val = $(this).parents("tr").find(".cuota_minima").find("input").val();
                // var cuota_maxima_val = $(this).parents("tr").find(".cuota_maxima").find("input").val();

                $.confirm({
                    title: '¿ Estas Seguro (a) ?',
                    content: 'Se actualizara el producto de la lista.',
                    buttons: {
                        Confirmar: {
                            text: 'Confirmar',
                            btnClass: 'btn btn-primary btn-confirm',
                            action: function(){
                                $.ajax({
                                    url: "{{ route('update.period') }}",
                                    type: "POST",
                                    data: {
                                        _token: "{{ csrf_token() }}",
                                        cuota_fija: cuota_fija_val,
                                        cuota_minima: cuota_minima_val,
                                        // cuota_maxima: cuota_maxima_val,
                                        id: id,
                                        period: period
                                    },
                                }).done(function(result){
                                    $('[data-toggle="tooltip"]').tooltip("hide");

                                    var cuota_fija = _this.parents("tr").find(".cuota_fija");
                                    cuota_fija.html("$ "+cuota_fija_val);

                                    var cuota_minima = _this.parents("tr").find(".cuota_minima");
                                    cuota_minima.html("$ "+cuota_minima_val);

                                    // var cuota_maxima = _this.parents("tr").find(".cuota_maxima");
                                    // cuota_maxima.html("$ "+cuota_maxima_val);

                                    $("#cuota_fija").val(cuota_fija_val);
                                    $("#cuota_minima").val(cuota_minima_val);
                                    // $("#cuota_maxima").val(cuota_maxima_val);

                                    var actions = _this.parents("tr").find(".actions");
                                    actions.html('<button type="button" class="btn btn-primary update-period" data-toggle="tooltip" data-placement="top" title="Editar periodo" data-id="'+id+'"><i class="fas fa-edit"></i></button>');
                                    $('[data-toggle="tooltip"]').tooltip();
                                });
                            }
                        },
                        Cancelar: {
                            text: 'Cancelar',
                            btnClass: 'btn btn-secondary btn-confirm',
                        }
                    }
                });
            });
        });
    </script>
@endsection