@extends('layouts.index')

@section('title') Colonias @endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.css') }}">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Colonias</h3>
        </div>
    </div>
    @can('write_location')
        <div class="row justify-content-end panel-buttoms">
            <button href="#modal_create_colony" data-target="#modal_create_colony"  data-toggle="modal" class="btn btn-primary mr-3"><i class="fas fa-plus mr-2"></i> Registrar Colonia</button>
        </div>
    @endcan
    <div class="panel-body @can('write_location') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive custam-table">
                <div id="table-filter" style="display:none; text-align:left;">
                    <div class="row margin-10">
                        <div class="col">
                            <button type="button" class="btn btn-success"><i class="fas fa-pdf mr-2"></i> Exportar a PDF</button>
                        </div>
                    </div>
                </div>
                <table id="table_colonies" class="table table-sm table-hover dataTable">
                    <thead>
                        <tr>
                            <form id="form-data" action="" method="POST">
                                @csrf
                                <th scope="col"><input autocomplete="off"  type="text" name="id_colonia" value="{{ (isset(session('inputs')['id_colonia'])) ? session('inputs')['id_colonia'] : "" }}" placeholder="ID" class="form-control custom_input" /></th>
                                <th scope="col"><input autocomplete="off"  type="text" name="nombre_colonia" value="{{ (isset(session('inputs')['nombre_colonia'])) ? session('inputs')['nombre_colonia'] : "" }}" placeholder="Nombre" class="form-control custom_input" /></th>
                                <th><input autocomplete="off"  type="text" name="nombre_comun" value="{{ (isset(session('inputs')['nombre_comun'])) ? session('inputs')['nombre_comun'] : "" }}" placeholder="Nombre Común" class="form-control custom_input" /></th>
                                <th><input disabled autocomplete="off"  type="text" name="nombre_corto" placeholder="Nombre Colonia" class="form-control custom_input" /></th>
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-submit"><i class="fas fa-search"></i></button></th>
                            </form>
                        </tr>
                        <tr>
                            <th width="5%" scope="col">ID</th>
                            <th scope="col">Nombre Colonia</th>
                            <th width="col" scope="col">Nombre Común</th>
                            <th width="col" scope="col">Nombre Población</th>
                            <th width="15%" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($colonies as $colony)
                            <tr>
                                <td>{{ $colony->IdColonia }}</td>
                                <td>{{ $colony->NombreColonia }}</td>
                                <td>{{ $colony->NombreComun }}</td>
                                <td>{{ $colony->location->NombrePoblacion }}</td>
                                <td align="center">
                                    <button data-target="#modal_update_colony" data-toggle="modal" data-route="{{ route('colony.update', $colony->IdColonia) }}" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="table-footer">
                    <div class="pagination">
                        <span>Mostrando {{ (($colonies->currentPage() * 15) - 14) }} a {{( $colonies->currentPage() * 15) }} de {{ $colonies->total() }} Filas</span>
                        {{ $colonies->appends(Request::only('IdPoblacion', 'IdColonia'))->links() }} 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
@include('partials.modals.padron.modal_update_colony')
@include('partials.modals.padron.modal_create_colony')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(function(){
            $("#padron").addClass('active');
            $(document).on("click", ".btn-submit", function(){
                $( ".custom_input" ).each(function( index, element ) {
                    $("#form-data").append($(this));
                });
                $("#form-data")[0].submit();
            });
            
            $(document).on("keypress", ".custom_input", function(e){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $( ".custom_input" ).each(function( index, element ) {
                        $("#form-data").append($(this));
                    });
                    $("#form-data")[0].submit();
                }
            });
/*****************************************actualizar colonia********************************/
            //modal para modificar poblacion
            $("#modal_update_colony").on("show.bs.modal", function(event){
                $button = $(event.relatedTarget);
                $route = $button.data("route");
                $this = $(this);
                $("#form-update-colony").attr("action", $route);
                $.ajax({
                    type: "get",
                    url: $route,
                    success: function(response){
                        $("#actualizar_nombre_colonia").val(response.NombreColonia);
                        $("#actualizar_nombre_comun").val(response.NombreComun);
                        $("#actualizar_id_poblacion").val(response.IdPoblacion);
                        $("#actualizar_id_poblacion").select2().trigger('change');
                    }, error(error){
                        $("#modal_update_colony").modal("hide");
                        swal({title:"Ups!", text:"Error al cargar colonia", type: "error"})
                    }
                });
            });
            //fin modal
            //evento cuando se cierra
            $("#modal_update_colony").on("hide.bs.modal", function(e){
                $("#actualizar_nombre_colonia").val("");
                $("#actualizar_nombre_comun").val("");
                $("#form-update-location").attr("action", "#");
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
            });
            // fin de cierre
            $("#form-update-colony").on("submit", function(event){
                event.preventDefault();
                $url = $(this).attr("action");
                $method = $(this).attr("method");
                $data = $(this).serializeArray();
                if(validateUpdateColony()){
                    $.ajax({
                        url: $url,
                        type: $method,
                        data: $data,
                        success: function(response){
                            $("#modal_update_colony").modal("hide");

                            if(response.alert == "success"){
                                data = response.data;
                                swal({title:"Correcto", text:"Se actualizó correctamente la Colonia", type: "success"});
                                $(document).find("[data-route='"+$url+"']").parents('tr').html(`<td>`+data.IdColonia+`</td>
                                <td>`+data.NombreColonia+`</td>
                                <td>`+data.NombreComun+`</td>
                                <td>`+data.location.NombrePoblacion+`</td>
                                <td align="center">
                                    <button data-target="#modal_update_colony" data-toggle="modal" data-route="`+response.url_update+`" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></button>
                                    </td>`);
                            }else{
                                swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                            }
                        },error: function(error){
                            $("#modal_update_colony").modal("hide");
                            swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                        }
                    });
                }
            });

            //validar actualizacion de localidades
            function validateUpdateColony(){
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
                let errors = 0;

                if($("#actualizar_nombre_colonia").val() == "" || $("#actualizar_nombre_colonia").val().length < 2 ){
                    errors++;
                    $("#actualizar_nombre_colonia").addClass("is-invalid");
                    $("#actualizar_nombre_colonia").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre de la colonia debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#actualizar_nombre_comun").val() == "" || $("#actualizar_nombre_comun").val().length < 2 ){
                    errors ++;
                    $("#actualizar_nombre_comun").addClass("is-invalid");
                    $("#actualizar_nombre_comun").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre común debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#actualizar_id_poblacion").val() == ""){
                    errors++;
                    $("#actualizar_id_poblacion").addClass("is-invalid");
                    $("#actualizar_id_poblacion").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>Seleccione una población.</strong>
                                </span>`);
                }

                if(errors === 0){
                    return true;
                }

                return false;
            }
        /************************************************************fin actualizar localidad ***********/
        /******************************************agregar nueva colonia**********************/
            //evento cuando se cierra
            $("#modal_create_colony").on("hide.bs.modal", function(e){
                $("#crear_nombre_colonia").val("");
                $("#crear_nombre_comun").val("");
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
            });
            // fin de cierre
            $("#form-create-colony").on("submit", function(event){
                event.preventDefault();
                $url = $(this).attr("action");
                $method = $(this).attr("method");
                $data = $(this).serializeArray();
                if(validateStoreColony()){
                    $.ajax({
                        url: $url,
                        type: $method,
                        data: $data,
                        success: function(response){
                            $("#modal_create_colony").modal("hide");

                            if(response.alert == "success"){
                                data = response.data;
                                swal({title:"Correcto", text:"Se Registró correctamente la colonia", type: "success"});
                                $("#table_colonies").find('tbody').prepend(`<tr><td>`+data.IdColonia+`</td>
                                <td>`+data.NombreColonia+`</td>
                                <td>`+data.NombreComun+`</td>
                                <td>`+data.location.NombrePoblacion+`</td>
                                <td align="center">
                                    <button data-target="#modal_update_colony" data-toggle="modal" data-route="`+response.url_update+`" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></button>
                                    </td></tr>`);
                            }else{
                                swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                            }
                        },error: function(error){
                            $("#modal_create_colony").modal("hide");
                            swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                        }
                    });
                }
            });

            //validar actualizacion de localidades
            function validateStoreColony(){
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
                let errors = 0;

                if($("#crear_nombre_colonia").val() == "" || $("#crear_nombre_colonia").val().length < 2 ){
                    errors++;
                    $("#crear_nombre_colonia").addClass("is-invalid");
                    $("#crear_nombre_colonia").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre de la colonia debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#crear_nombre_comun").val() == "" || $("#crear_nombre_comun").val().length < 2 ){
                    errors ++;
                    $("#crear_nombre_comun").addClass("is-invalid");
                    $("#crear_nombre_comun").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre común debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#crear_id_poblacion").val() == ""){
                    errors++;
                    $("#crear_id_poblacion").addClass("is-invalid");
                    $("#crear_id_poblacion").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>Seleccione una población.</strong>
                                </span>`);
                }

                if(errors === 0){
                    return true;
                }

                return false;
            }
            /***********************fin agregar nueva localidad **************/
        });
    </script>
@endsection