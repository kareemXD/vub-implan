@extends('layouts.index')

@section('title') {{ $process->Nombre }} @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>{{ $process->Nombre }} ( {{ $license->NumeroLicencia }} )</h3>
    </div>
</div> 
<div class="panel-body @can('write_requests') with-buttons @endcan">
    <div class="margin-fix panel-row-fluid">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-link active" id="period-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Ubicación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="data-tab" data-toggle="tab" href="#data" role="tab" aria-controls="data" aria-selected="true">Solicitud</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link" id="period-tab" data-toggle="tab" href="#turn" role="tab" aria-controls="turn" aria-selected="false">Giro</a>
            </li>
        </ul>
        <form id="form-data" action="{{ route('store.update.license') }}" method="POST" class="tab-content small-form">
            @csrf
            <input type="hidden" name="id" value="{{ $license->NumeroLicencia }}">
            <input type="hidden" name="operation" value="update">
            <div class="tab-pane fade show active" id="location" role="tabpanel" aria-labelledby="location-tab">
                @if(session()->has('alert'))
                    <div class="alert alert-primary" role="alert">
                        {{ session("alert") }}
                    </div>
                @endif
                @php
                    $turndetails = $license->getTurnsDetails();
                @endphp
                <div class="form-group row margin-10 mt-2">
                    <label for="city" class="col-sm-2 col-form-label">Poblacion</label>
                    <div class="col-sm-10">
                        <select {{ ($process->TipoTramite != "D") ? "Disabled" : "" }} name="city" id="city" class="select2 form-control {{ $errors->has('city') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Población</option>
                            @foreach ($poblaciones as $poblacion)
                                @if ($poblacion->IdPoblacion == $license->IdPoblacion)
                                    <option selected value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @else
                                    <option value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10 mt-2">
                    <label for="colony" class="col-sm-2 col-form-label">Colonia</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            @if($process->TipoTramite == "D")
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="colony" data-class="required" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir una Colonia"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            @endif
                            <select {{ ($process->TipoTramite != "D") ? "Disabled" : "" }} name="colony" id="colony" class="select2 form-control {{ $errors->has('colony') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Colonia</option>
                            </select>
                            @if ($errors->has('colony'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('colony') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                    </div>
                    <label for="zone" class="col-sm-2 col-form-label">Zona</label>
                    <div class="col-sm-4">
                        <select {{ ($process->TipoTramite != "D") ? "Disabled" : "" }} name="zone" id="zone" class="select2 form-control {{ $errors->has('zone') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Zona</option>
                            @foreach ($zonas as $zona)
                                @if ($zona->IdZona == $license->IdZona)
                                    <option selected value="{{ $zona->IdZona }}">{{ $zona->Nombre }}</option>
                                @else
                                    <option value="{{ $zona->IdZona }}">{{ $zona->Nombre }}</option>
                                @endif
                                <option value="{{ $zona->IdZona }}">{{ $zona->Nombre }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('zone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('zone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10 ">
                    <label for="domicilio2" class="col-sm-2 col-form-label">Calle</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            @if($process->TipoTramite == "D")
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="domicilio2" data-class="required" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir una Calle"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            @endif
                            <select {{ ($process->TipoTramite != "D") ? "Disabled" : "" }} name="domicilio2" id="domicilio2" class="select2 form-control {{ $errors->has('domicilio2') ? ' is-invalid' : '' }}" >
                                <option value="">Seleccione una Calle</option>
                            </select>
                            @if ($errors->has('domicilio2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('domicilio2') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="calle1_u" class="col-sm-2 col-form-label">Entre Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            @if($process->TipoTramite == "D")
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-primary btn-other" data-name="calle1_u" data-class="required" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir una Entre Calle"><i class="fas fa-plus-circle"></i></button>
                                </div>
                            @endif
                            <select {{ ($process->TipoTramite != "D") ? "Disabled" : "" }} name="calle1_u" id="calle1_u" class="select2 form-control {{ $errors->has('calle1_u') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle1_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle1_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="ext" class="col-sm-2 col-form-label">Exterior</label>
                    <div class="col-sm-2">
                        <input {{ ($process->TipoTramite != "D") ? "Disabled" : "" }} type="number" class="form-control {{ $errors->has('ext') ? ' is-invalid' : '' }}" id="ext" name="ext" value="{{ $license->Exterior }}">
                        @if ($errors->has('ext'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ext') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="calle2_u" class="col-sm-2 col-form-label">Y Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group">
                            @if($process->TipoTramite == "D")
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-primary btn-other" data-name="calle2_u" data-class="required" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir otra Entre Calle"><i class="fas fa-plus-circle"></i></button>
                                </div>
                            @endif
                            <select {{ ($process->TipoTramite != "D") ? "Disabled" : "" }} name="calle2_u" id="calle2_u" class="select2 form-control {{ $errors->has('calle2_u') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle2_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle2_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="int" class="col-sm-2 col-form-label">Interior</label>
                    <div class="col-sm-2">
                        <input {{ ($process->TipoTramite != "D") ? "Disabled" : "" }} type="number" class="form-control {{ $errors->has('int') ? ' is-invalid' : '' }}" id="int" name="int" value="{{ $license->Interior }}">
                        @if ($errors->has('int'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('int') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="este" class="col-sm-2 col-form-label">Coordenada Este</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="form-control" id="este" name="este" value="{{ $license->CoordenadaEste }}">
                    </div>
                    <label for="norte" class="col-sm-2 col-form-label">Coordenada Norte</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="form-control" id="norte" name="norte" value="{{ $license->CoordenadaNorte }}">
                    </div>
                </div>
                <div id="map" class="map" style="width:100%; height:500px;"></div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="button" class="btn btn-primary mr-2 mb-2 mt-2 btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
            
            <div class="tab-pane fade " id="data" role="tabpanel" aria-labelledby="data-tab">
                
                <div class="form-row mt-3">
                    <div class="form-group col-4">
                        <label for="idtaxpayer" >Contribuyente</label>
                        <input {{ ($process->TipoTramite != "T") ? "Disabled" : "" }} required maxlength="100" type="text" class="form-control {{ $errors->has('idtaxpayer') ? ' is-invalid' : '' }}" id="idtaxpayer" name="idtaxpayer" value="{{ $license->IdContribuyente }}" >
                        @if ($errors->has('idtaxpayer'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('idtaxpayer') }}</strong>
                            </span>
                        @endif
                    </div>
                        <div class="form-group col-2">
                            <label for=""> </label>
                            <button @if($process->TipoTramite != "T") style="display:none;" @endif  type="button" class="btn btn-primary btn-block btn-search-taxpayer"><i class="fas fa-search"></i></button>
                        </div>
                    
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="nombre">Nombre</label>
                        <input disabled required maxlength="100" type="text" class="form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre') }}" >
                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="apellido_pat">Apellido Paterno</label>
                        <input disabled required maxlength="50" type="text" class="form-control {{ $errors->has('apellido_pat') ? ' is-invalid' : '' }}" id="apellido_pat" name="apellido_pat" value="{{ old('apellido_pat') }}" >
                        @if ($errors->has('apellido_pat'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellido_pat') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="apellido_mat">Apellido Materno</label>
                        <input disabled required maxlength="50" type="text" class="form-control {{ $errors->has('apellido_mat') ? ' is-invalid' : '' }}" id="apellido_mat" name="apellido_mat" value="{{ old('apellido_mat') }}">
                        @if ($errors->has('apellido_mat'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellido_mat') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="rfc">R.F.C</label>
                        <input disabled required maxlength="15" type="text" class="form-control {{ $errors->has('rfc') ? ' is-invalid' : '' }}" id="rfc" name="rfc" value="{{ old('rfc') }}" placeholder="XAXX010101000">
                        @if ($errors->has('rfc'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('rfc') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="sexo">Sexo</label>
                        <select disabled name="sexo" id="sexo" class="form-control {{ $errors->has('sexo') ? ' is-invalid' : '' }}">
                            <option value="I">Seleccione Sexo</option>
                            <option value="I">Indefinido</option>
                            <option value="M">Hombre</option>
                            <option value="F">Mujer</option>
                        </select>
                        @if ($errors->has('sexo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('sexo') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="persona">Persona Moral ?</label>
                        <input disabled type="checkbox" data-toggle="toggle"  data-on="Si" data-off="No" class="form-control " id="persona" name="persona">
                        @if ($errors->has('persona'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('persona') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="email">Correo</label>
                        <input disabled type="email" maxlength="50" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" >
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="telefono_casa">Telefono Casa</label>
                        <input disabled type="number" maxlength="15" class="form-control {{ $errors->has('telefono_casa') ? ' is-invalid' : '' }}" id="telefono_casa" name="telefono_casa" value="{{ old('telefono_casa') }}" >
                        @if ($errors->has('telefono_casa'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telefono_casa') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="telefono_trab">Telefono Trabajo</label>
                        <input disabled type="number" maxlength="15" class="form-control {{ $errors->has('telefono_trab') ? ' is-invalid' : '' }}" id="telefono_trab" name="telefono_trab" value="{{ old('telefono_trab') }}" >
                        @if ($errors->has('telefono_trab'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telefono_trab') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="domicilio">Domicilio</label>
                        <input disabled required maxlength="60" type="text" class="form-control {{ $errors->has('domicilio') ? ' is-invalid' : '' }}" id="domicilio" name="domicilio" value="{{ old('domicilio') }}" >
                        @if ($errors->has('domicilio'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="calle1">Entre la Calle</label>
                        <input disabled type="text" maxlength="60" class="form-control {{ $errors->has('calle1') ? ' is-invalid' : '' }}" id="calle1" name="calle1" value="{{ old('calle1') }}" >
                        @if ($errors->has('calle1'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle1') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="calle2">y la Calle</label>
                        <input disabled type="text" maxlength="60" class="form-control {{ $errors->has('calle2') ? ' is-invalid' : '' }}" id="calle2" name="calle2" value="{{ old('calle2') }}" >
                        @if ($errors->has('calle2'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle2') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="exterior">Numero Exterior</label>
                        <input disabled required type="text" maxlength="50" class="form-control {{ $errors->has('exterior') ? ' is-invalid' : '' }}" id="exterior" name="exterior" value="{{ old('exterior') }}" >
                        @if ($errors->has('exterior'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('exterior') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="interior">Numero Interior</label>
                        <input disabled type="text" maxlength="50" class="form-control {{ $errors->has('interior') ? ' is-invalid' : '' }}" id="interior" name="interior" value="{{ old('interior') }}" >
                        @if ($errors->has('interior'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('interior') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="cp">C.P.</label>
                        <input disabled required type="number" maxlength="5" class="form-control {{ $errors->has('cp') ? ' is-invalid' : '' }}" id="cp" name="cp" value="{{ old('cp') }}" >
                        @if ($errors->has('cp'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cp') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="localidad">Localidad</label>
                        <input disabled required maxlength="40" type="text" class="form-control {{ $errors->has('localidad') ? ' is-invalid' : '' }}" id="localidad" name="localidad" value="{{ old('localidad') }}" >
                        @if ($errors->has('localidad'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('localidad') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="colonia">Colonia</label>
                        <input disabled required maxlength="40" type="text" class="form-control {{ $errors->has('colonia') ? ' is-invalid' : '' }}" id="colonia" name="colonia" value="{{ old('colonia') }}" >
                        @if ($errors->has('colonia'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('colonia') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="button" class="btn btn-primary mr-2 mb-2 mt-2 btn-next" data-target="#turn">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

            <div class="tab-pane fade" id="turn" role="tabpanel" aria-labelledby="turn-tab">
                <div class="form-group row mt-2 margin-10">
                    <label for="name_b" class="col-sm-2 col-form-label">Nombre del Negocio</label>
                    <div class="col-sm-10">
                        <input disabled type="text" class="form-control {{ $errors->has('name_b') ? ' is-invalid' : '' }}" id="name_b" name="name_b" value="{{ $license->NombreNegocio }}">
                        @if ($errors->has('name_b'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name_b') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="linderos" class="col-sm-2 col-form-label">Linderos</label>
                    <div class="col-sm-10">
                        <textarea  @if($process->TipoTramite != "A") disabled @endif class="form-control {{ $errors->has('linderos') ? ' is-invalid' : '' }}" id="linderos"  name="linderos" rows="2">{{ $license->Linderos }}</textarea>
                        @if ($errors->has('linderos'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('linderos') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="observations" class="col-sm-2 col-form-label">Observaciones</label>
                    <div class="col-sm-10">
                        <input @if($process->TipoTramite != "A") disabled @endif type="text" class="form-control {{ $errors->has('observations') ? ' is-invalid' : '' }}" id="observations" name="observations" value="{{ $license->Observaciones }}">
                        @if ($errors->has('observations'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('observations') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label data-legends="{{ json_encode($license->legends()->pluck("Leyendas.IdLeyenda")->toArray()) }}" for="legends" class="col-sm-2 col-form-label">Leyendas</label>
                    <div class="col-sm-10">
                        <select name="legends[]" id="legends" class="required select2 form-control {{ $errors->has('legends') ? ' is-invalid' : '' }}" multiple>
                            <option value="">Seleccione las Leyendas</option>
                            @foreach ($legends as $legend)
                                <option value="{{ $legend->IdLeyenda }}">{{ $legend->NombreLeyenda }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('legends'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('legends') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="surface" class="col-sm-2 col-form-label">Superficie</label>
                    <div class="col-sm-4">
                        <input  @if($process->TipoTramite != "A") disabled @endif  type="number" step="0.00" class="form-control {{ $errors->has('surface') ? ' is-invalid' : '' }}" id="surface" name="surface" value="{{ $license->Superficie }}">
                        @if ($errors->has('surface'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('surface') }}</strong>
                            </span>
                        @endif
                    </div>
                    <label for="employees" class="col-sm-2 col-form-label">Empleados</label>
                    <div class="col-sm-4">
                        <input  @if($process->TipoTramite != "A") disabled @endif  type="number" class="form-control {{ $errors->has('employees') ? ' is-invalid' : '' }}" id="employees" name="employees" value="{{ $license->EmpleosCreados }}">
                        @if ($errors->has('employees'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('employees') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <br>
                <h5 class="mb-0">Giros</h5>
                <table data-tramite="{{ $process->TipoTramite }}" class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                    <thead>
                        @if ($process->TipoTramite == "A")
                            <tr>
                                <th colspan="4">
                                    {{-- <input type="text" id="cuenta_serach" placeholder="ID" class="form-control custom_input" /> --}}
                                    <select id="cuenta_serach" name="cuenta_serach" class="js-data-turns-ajax form-control"></select>
                                    <input type="hidden" disabled id="description_search" placeholder="Descripción" class="form-control custom_input" />
                                </th>
                                <th><input type="date" id="date_search" placeholder="Fecha" class="form-control custom_input" /></th>
                                <th><input type="number" id="quantity_search" placeholder="Cantidad" class="form-control custom_input" /></th>
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-add-turn"><i class="fas fa-plus"></i></button></th>
                            </tr>
                        @endif
                        <tr>
                            <th width="10%">IdGiro</th>
                            <th>Descripción</th>
                            <th>Observaciones</th>
                            <th width="10%">Fecha</th>
                            <th width="10%">Cuota</th>
                            <th width="10%">Cantidad</th>
                            @if ($process->TipoTramite == "G")
                                <th width="10%">Acciones</th>
                            @else
                                <th></th>
                            @endif
                        </tr>
                    </thead>
                    <tbody id="turns-content">
                        @if(isset($turndetails["data"]))
                            @foreach ($turndetails["data"] as $turn)
                                @if ($process->TipoTramite == "G")
                                    <tr>
                                        <input type="hidden" class="type" name="type[]" value="{{ $turn['type'] }}"/>
                                        <td><input type="hidden" class="cuenta_serach" name="cuenta_serach[]" value="{{ $turn['id'] }}"/>{{ $turn['id'] }}</td>
                                        <td><input type="hidden" class="description_search" name="description_search[]" value="{{ $turn['nombre'] }}"/> {{ $turn['nombre'] }}</td>
                                        <td><input type="hidden" class="observations_search" name="observations_search[]" value="{{ $turn['observation'] }}"/> {{ $turn['observation'] }}</td>
                                        <td><input type="hidden" class="date_search" name="date_search[]" value="{{ $turn['date'] }}"/>{{ $turn['date'] }}</td>
                                        <td><input type="hidden" class="cost_search" name="cost_search[]" value="{{ $turn['cost'] }}"/>$ {{ $turn['cost'] }}</td>
                                        <td><input type="hidden" class="quantity_search" name="quantity_search[]" value="{{ $turn['quantity'] }}"/> {{ $turn['quantity'] }}</td>
                                        <td><button type="button" class="btn btn-danger btn-action-table btn-delete-turn"><i class="fas fa-trash"></i></button></td>
                                    </tr>
                                @else
                                    <tr>
                                        <td>{{ $turn['id'] }}</td>
                                        <td>{{ $turn['nombre'] }}</td>
                                        <td>{{ $turn['observation'] }}</td>
                                        <td>{{ $turn['date'] }}</td>
                                        <td>$ {{ $turn['cost'] }}</td>
                                        <td>{{ $turn['quantity'] }}</td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <div class="row margin-10 mt-2">
                    <div class="col-8">
                        <span>El costo de los anexos depende del porcentaje de pago para altas y refrendos registrados en preferencias y que en este momento es de <b>{{ $trimester["percentage"] }} %</b> del costo anual.</span>
                    </div>
                    <div class="col-4">
                        <div class="form-group row margin-10">
                            <label for="costo_f" class="col-sm-4 col-form-label">Costo Forma</label>
                            <div class="col-sm-8">
                                <input disabled type="text" class="form-control {{ $errors->has('costo_f') ? ' is-invalid' : '' }}" id="costo_f" value="$ 0.00">
                            </div>
                        </div>
                        <div class="form-group row mt-2 margin-10">
                            <label for="costo_t" class="col-sm-4 col-form-label">Costo Total</label>
                            <div class="col-sm-8">
                                <input disabled type="text" class="form-control {{ $errors->has('costo_t') ? ' is-invalid' : '' }}" id="costo_t" value="$ 0.00">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@can('write_requests')
    <div class="row justify-content-end panel-buttoms" id="buttons">
        <button type="button" class="btn btn-primary mr-3 btn-next " id="btn-previous" style="display:none;" data-target="#data">Anterior <i class="fas fa-arrow-left ml-2"></i></button>
        <button type="button" class="btn btn-primary mr-3 btn-next" id="btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
        @if(($license->CoordenadaEste == 0 && $license->CoordenadaNorte == 0)  || $process->TipoTramite == "D")
            <button type="submit" class="btn btn-primary mr-3" id="btn-save-location" ><i class="fas fa-save mr-2"></i> Guardar Ubicación</button>
        @endif
        
        <button type="submit" class="btn btn-primary mr-3" id="btn-save-changes" style="display:none;"><i class="fas fa-save mr-2"></i> Guardar Cambios</button>
    </div>
@endcan
@endsection

@section('modals')
    {{-- @include('partials.modals.padron.modal_update_document') --}}
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('system.google.maps.apiKey') }}&libraries=places&v=weekly"
        defer
    ></script>
    <script>
        window.addEventListener('load', function () {
            OnChangeCity("{{ $license->IdColonia }}");
            OnChangeColony("{{ $license->IdCalle }}", "{{ $license->IdCruce1 }}", "{{ $license->IdCruce2 }}");
            @if(session()->has('document_updated'))
                $('#myTab a[href="#documents"]').tab('show');
            @endif
            setTimeout(function(){ 
                if($(".btn-search-taxpayer").length > 0){
                    $(".btn-search-taxpayer")[0].click();
                }
            }, 100);
        });
        $(function(){
            setTimeout(function() {
                updateTotalGiros();
            }, 1000);

            $("#padron").addClass('active');
            
            var created = false;
            var map = L.map('map',{scrollWheelZoom:true}).setView([20.74689, -105.39425], 12);
            // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            //     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap Saul Moncivais</a> contributors'
            // }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:Vuelo_Bajo_Ortofoto@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 12,
                maxZoom: 23,
                attribution: '&copy; IMPLAN - Ortofoto Bahia de Banderas 2019'
            }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:vialidad_BB@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);

            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:lim_loc_final@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);
            var theMarker = {};
            var item = L.utm({x: "{{ $license->CoordenadaEste }}", y: "{{ $license->CoordenadaNorte }}", zone: 13, band: 'N'});
            var coord = item.latLng();
            theMarker = L.marker(coord).addTo(map);
            
            /*
                Validaciones para los campos de dirección
            */
            $(document).on('change', '#colony', function() {
                $('#newcolony').val("");
            });

            $(document).on('change', '#newcolony', function() {
                $('#colony').val("").trigger('change.select2');
            });

            $(document).on('change', '#domicilio2', function() {
                $('#newdomicilio2').val("");
            });

            $(document).on('change', '#newdomicilio2', function() {
                $('#domicilio2').val("").trigger('change.select2');
            });

            $(document).on('change', '#calle1_u', function() {
                $('#newcalle1_u').val("");
            });

            $(document).on('change', '#newcalle1_u', function() {
                $('#calle1_u').val("").trigger('change.select2');
            });

            $(document).on('change', '#calle2_u', function() {
                $('#newcalle2_u').val("");
            });

            $(document).on('change', '#newcalle2_u', function() {
                $('#calle2_u').val("").trigger('change.select2');
            });

            /*
                Actualizar marcador del mapa
            */
            $(document).on("keyup", '#ext', function(event){
                if(event.keyCode == 13){
                    getLocationFromAddress()
                }
            });

            $(document).on('change', '#city, #colony, #domicilio2, #newdomicilio2, #ext', function() {
                getLocationFromAddress();
            });

            function getLocationFromAddress(){
                var address = "";

                if($("#domicilio2 option:selected").text().length > 2 && $("#domicilio2").val() != "" && $("#domicilio2").val() != 0){
                    address = address + $("#domicilio2 option:selected").text();
                }else if ($("#newdomicilio2").val() != "")
                {
                    address = address +" "+ $("#newdomicilio2").val();
                }

                if($("#ext").val() != ""){
                    address = address + " "+ $("#ext").val();
                }

                if($("#colony option:selected").text().length > 2 && $("#colony").val() != "" && $("#colony option:selected").text() != "BAHIA DE BANDERAS" ){
                    address = address + ", "+ $("#colony option:selected").text();
                }else if ($("#newcolony").val() != "")
                {
                    address = address +" "+ $("#newcolony").val();
                }   

                if($("#city option:selected").text().length > 2 && $("#city").val() != "" && $("#city option:selected").text() != "BAHIA DE BANDERAS"){
                    address = address + ", "+ $("#city option:selected").text();
                }

                console.log(address);         
                if(address != ""){
                    var address = address + ", Bahía de banderas";

                    var geocoder = new google.maps.Geocoder();
                    var geocoderRequest = { address: address };
                    geocoder.geocode(geocoderRequest, function(results, status){
                        window.geome = results;
                        if(results.length > 0){
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            };

                            theMarker = L.marker({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}).addTo(map);
                            $("#este").val(theMarker.getLatLng().utm().x);
                            $("#norte").val(theMarker.getLatLng().utm().y);

                            map.setView({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}, 16);
                        }else
                        {
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            }; 

                            $("#este").val(0);
                            $("#norte").val(0);

                            map.setView([20.74689, -105.39425], 12);
                        }
                    });
                }
                
            }
            map.on('click',function(e){
                if (theMarker != undefined) {
                    map.removeLayer(theMarker);
                };
                theMarker = L.marker(e.latlng).addTo(map);
                $("#este").val(theMarker.getLatLng().utm().x)
                $("#norte").val(theMarker.getLatLng().utm().y)
            });
            
            $(document).on("click", "#btn-save-changes", function(){
                $.confirm({
                    title: 'Confirmación!',
                    content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción realizara cambios en la licencia.</small>',
                    type: "blue",
                    buttons: {
                        Aceptar: function () {
                            
                            $("input[name=operation]").val($("[data-tramite]").data("tramite"));
                            $('#form-data')[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });
            
            $(document).on("click", "#btn-applay-request", function(){
                $("input[name=operation]").val('apply_changes');
                $("#form-data")[0].submit();
            });

            $(document).on("click", "#btn-save-location", function(){
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción?',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            $("input[name=operation]").val('update_location');
                            $("#form-data")[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });
            
            $(document).on("click", ".btn-update-document", function(){
                var _this = $(this);
                $.ajax({
                    url: "{{ route('get.document.by.request') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        requisito: _this.data("requisito"),
                        solicitud: $("input[name=id]").val()
                    },
                }).done(function(result){
                    console.log(result);
                    var modal = $("#modal-update-document");
                    modal.find("#requisito_id").val(result.Requisito);
                    modal.find("#nombre_r").val(result.Nombre);
                    modal.find("#comments").val(result.pivot_Comentario);
                    if(result.pivot_Archivo)
                    {
                        modal.find("#documents-content").prepend('<ul><li class="download-file" data-requisito="'+result.Requisito+'" data-solicitud="'+$("input[name=id]").val()+'">\
                            <img src="{{ asset("assets/plugins/preview/preview_pdf.png") }}" >\
                        </li></ul>');
                        $('.old_document').simpleFilePreview();
                    }
                    modal.modal("show");
                });
            });
            
            $(document).on('click', '.btn-search-taxpayer', function(){
                var id = $("#idtaxpayer").val(),
                    token = "{{ csrf_token() }}",
                    legends = $("[data-legends]").data("legends");
                    if($("[data-tramite]").data("tramite") == "A"){
                        $("#legends").prop("disabled", false);
                    }else{
                        $("#legends").prop("disabled", true);
                    }

                    $("#legends").select2("val", legends);

                    $('#update_persona').bootstrapToggle('off')
                $.ajax({
                    url: "{{ route('get.taxpayer.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $('#taxpayer').val(result.IdContribuyente);
                    $('#cuenta').val(result.IdContribuyente);
                    $('#rfc').val(result.RFC);
                    $('#nombre').val(result.Nombre);
                    $('#apellido_pat').val(result.ApellidoPaterno);
                    $('#apellido_mat').val(result.ApellidoMaterno);
                    if(result.Sexo)
                    {
                        $('#sexo').val(result.Sexo);
                    }else
                    {
                        $('#sexo').val("I");
                    }
                    if(result.PersonaMoral == 1)
                    {
                        $('#persona').bootstrapToggle('on')
                    }
                    $('#email').val(result.Email);
                    $('#exterior').val(result.Exterior);
                    $('#interior').val(result.Interior);
                    $('#cp').val(result.CP);
                    $('#localidad').val(result.Ciudad);
                    $('#colonia').val(result.Colonia);
                    $('#domicilio').val(result.Domicilio);
                    $('#calle1').val(result.Cruce1);
                    $('#calle2').val(result.Cruce2);
                    $('#telefono_casa').val(result.TelefonoCasa);
                    $('#telefono_trab').val(result.TelefonoTrabajo);
                });
            });

            $('.js-data-turns-ajax').select2({
                minimumInputLength: 1,
                placeholder: "Giro",
                allowClear: true,
                theme: "bootstrap",
                language: "es",
                ajax: {
                    url: "{{ route('ajax.turns') }}",
                    type: 'post',
                    dataType: 'json',
                    delay: 400,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _token: "{{ csrf_token() }}"
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data.data.turns
                        };
                    }
                }
            });
            
            $(document).on('select2:select', '#cuenta_serach', function (e) {
                var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.turn.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $('#description_search').val(result.Nombre);
                    $('#quantity_search').val(1);
                });
            });
            
            $(document).on("click", ".btn-add-turn", function(){
                var flag = true;
                $(this).parents("table").find(".custom_input").each(function() {
                    if(!$(this).val()){
                        flag = false;
                        $(this).addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                        }
                    }
                });
                if(flag){
                    var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                    $.ajax({
                        url: "{{ route('get.license.cost.details') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: $("#cuenta_serach").val(),
                            date: $("#date_search").val(),
                            location: $("#city").val(),
                        },
                    }).done(function(result){
                        if(result.alcohol){
                            $("#alcohol-div").fadeIn();
                            $("#alcohol-div").find("#alcohol").addClass("required");
                        }
                        result.data.forEach(function(element){ 
                            $("#turns-content").append('\
                                <tr>\
                                    <input type="hidden" class="type" name="type[]" value="'+element.type+'"/>\
                                    <td><input type="hidden" class="cuenta_serach" name="cuenta_serach[]" value="'+element.id+'"/>'+element.id+'</td>\
                                    <td><input type="hidden" class="description_search" name="description_search[]" value="'+element.nombre+'"/>'+element.nombre+'</td>\
                                    <td><input type="hidden" class="observations_search" name="observations_search[]" value="'+element.observation+'"/>'+element.observation+'</td>\
                                    <td><input type="hidden" class="date_search" name="date_search[]" value="'+element.date+'"/>'+element.date+'</td>\
                                    <td><input type="hidden" class="cost_search" name="cost_search[]" value="'+parseFloat(element.cost)+'"/>$ '+parseFloat(element.cost).toFixed(2)+'</td>\
                                    <td><input type="hidden" class="quantity_search" name="quantity_search[]" value="'+$("#quantity_search").val()+'"/>'+$("#quantity_search").val()+'</td>\
                                    <td><button type="button" class="btn btn-danger btn-action-table btn-delete-turn"><i class="fas fa-trash"></i></button></td>\
                                </tr>');
                        });

                        updateTotalGiros();

                        $("#cuenta_serach").val("").trigger("change");
                        $("#date_search").val("");
                        $("#cost_search").val("");
                        $("#quantity_search").val("");
                    });
                }
            });
            
            function updateTotalGiros()
            {
                var cost = 0;
                $('[name="cost_search[]"]').each(function() {
                    cost += parseFloat($(this).val()) * parseFloat($(this).parent().parent().find(".quantity_search").val());
                    
                });
                // cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                $("#costo_t").val("$ " + cost.toFixed(2));
            }
            
            $(document).on("click", ".btn-delete-turn", function(){
                var _this = $(this);
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción?',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            _this.parents("tr").fadeOut("slow", function(){
                                _this.parents("tr").remove();
                                
                                updateTotalGiros();
                            });
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });

            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var target = e.target.hash;
                switch (e.relatedTarget.hash) {
                    case "#data":
                        if(!$("#idtaxpayer").val())
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione un contribuyente para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        break;
                
                    case "#location":
                        if(!$("#norte").val() || !$("#este").val())
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione la ubicación para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        break;
                    case "#documents":
                        $("#btn-save-changes").fadeOut("fast");
                        $("#btn-applay-request").fadeOut("fast");
                        $("#btn-next").fadeIn("fast");
                        break;
                }

                switch (e.target.hash) {
                    case "#location":
                        $("#btn-previous").fadeOut("fast")
                        $("#btn-next").data("target", "#data")
                        break;
                    case "#data":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#location")
                        });
                        $("#btn-next").data("target", "#turn")
                        break;
                    case "#turn":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#data")
                        });
                        $("#btn-next").data("target", "#documents")
                        break;
                    case "#documents":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#turn")
                        });
                        $("#btn-next").fadeOut("fast", function(){
                            $("#btn-save-changes").fadeIn("fast");
                            $("#btn-applay-request").fadeIn("fast");
                        });
                        break;
                }
            });

            $(document).on("click", ".btn-next", function(){
                $('#myTab a[href="'+$(this).data("target")+'"]').tab('show');
            });

            $(document).on("change", "#city", function(){
                OnChangeCity();
            });
            $(document).on("change", "#colony", function(){
                OnChangeColony();
            });
            
            /*tramites */
            //ampliacion de giro
            if($("[data-tramite]").data("tramite") == "A") {
                $('a#period-tab').on("show.bs.tab", function(event){
                    $("#btn-save-changes").fadeIn(500);
                });
                $('a#period-tab').on("hide.bs.tab", function(event){
                    $("#btn-save-changes").fadeOut(500);
                });

            }
            
            //AGREGAR OTRO
            $(document).on("click", ".btn-other", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);

                parent.find("span.select2").fadeOut("slow", function(){
                    parent.append('<input type="text" class="form-control inputInserted '+_this.data("class")+'" id="new'+_this.data("name")+'" name="new'+_this.data("name")+'" placeholder="Escriba el nombre de la Calle"> ').show('slow');
                    _this.html('<i class="fas fa-minus-circle"></i>');
                    _this.removeClass("btn-other");
                    _this.addClass("remove-calle");
                    _this.attr("data-original-title", "Seleccionar").tooltip('show');
                });

                parent.find('select[data-required="true"]').removeClass('required');
            });

            $(document).on("click", ".remove-calle", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);

                parent.find(".inputInserted").fadeOut("slow", function(){
                    $(this).remove();
                    parent.find("span.select2").fadeIn("slow")
                    _this.html('<i class="fas fa-plus-circle"></i>')
                    _this.removeClass("remove-calle");
                    _this.addClass("btn-other");
                    _this.attr("data-original-title", "Añadir otro").tooltip('show');
                });

                parent.find('select[data-required="true"]').addClass('required');
            });
        });

        function OnChangeCity(colony = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.colonies.by.city') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#colony").empty();
                $("#colony").append("<option value=''>Seleccione una Colonia</option>");
                result.forEach(element => {
                    $("#colony").append($('<option>', {
                        value: element.IdColonia,
                        text: element.NombreColonia
                    }));
                });
                if(colony)
                {
                    $("#colony").val(colony);
                }
                
            });

            $(document).on("click", ".download-file", function(){
                var requisito = $(this).data("requisito");
                var solicitud = $(this).data("solicitud");
                window.open('/padron/mostrar/documento/'+requisito+'/'+solicitud, '_blank');
            });
        }

        function OnChangeColony(calle = null, calle1 = null, calle2 = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.calles.by.colony') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#domicilio2").empty();
                $("#domicilio2").append("<option value=''>Seleccione una Calle</option>");

                $("#calle1_u").empty();
                $("#calle1_u").append("<option value=''>Seleccione una Entre Calle</option>");

                $("#calle2_u").empty();
                $("#calle2_u").append("<option value=''>Seleccione una Entre Calle</option>");

                result.forEach(element => {
                    $("#domicilio2").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle1_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle2_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));
                });
                if(calle || calle1 || calle2)
                {
                    if($("#domicilio2 option[value='"+calle+"']").length > 0){
                        $("#domicilio2").val(calle);
                    }else{
                        $.get("{{ route('api.get-street-by-id') }}/"+calle, function(response){
                            $("#domicilio2").append("<option selected value='"+calle+"'>"+response.NombreCalle+"</option>");
                        });
                    }

                    if($("#calle1_u option[value='"+calle1+"']").length > 0){
                        $("#calle1_u").val(calle1);
                    }else{
                        $.get("{{ route('api.get-street-by-id') }}/"+calle1, function(response){
                            $("#calle1_u").append("<option selected value='"+calle1+"'>"+response.NombreCalle+"</option>");
                        });
                    }

                    if($("#calle2_u option[value='"+calle2+"']").length > 0){
                        $("#calle2_u").val(calle2);
                    }else{
                        $.get("{{ route('api.get-street-by-id') }}/"+calle2, function(response){
                            $("#calle2_u").append("<option selected value='"+calle2+"'>"+response.NombreCalle+"</option>");
                        });
                    }
                }
            });
        }
    </script>
@endsection