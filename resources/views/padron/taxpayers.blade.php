@extends('layouts.index')

@section('title') Contribuyentes @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Contribuyentes</h3>
        </div>
    </div>
    
    @can('write_contribuyentes')
        <div class="row justify-content-end panel-buttoms">
            <button type="button" class="btn btn-primary mr-3" data-toggle="modal" data-target="#modal-add-taxpayer"><i class="fas fa-user-plus mr-2"></i> Nuevo Contribuyente</button>
        </div>
    @endcan
    <div class="panel-body @can('write_contribuyentes') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-sm table-hover laravel-pagination">
                    <thead>
                        <tr>
                            <form id="form-data" action="{{ route('contribuyentes') }}" method="POST">
                                @csrf
                                <th scope="col"><input type="text" name="cuenta" value="{{ (isset(session('inputs')['cuenta'])) ? session('inputs')['cuenta'] : "" }}" placeholder="Cuenta" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="nombre" value="{{ (isset(session('inputs')['nombre'])) ? session('inputs')['nombre'] : "" }}" placeholder="Nombre" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="ape_paterno" value="{{ (isset(session('inputs')['ape_paterno'])) ? session('inputs')['ape_paterno'] : "" }}" placeholder="Apellido Paterno" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="ape_materno" value="{{ (isset(session('inputs')['ape_materno'])) ? session('inputs')['ape_materno'] : "" }}" placeholder="Apellido Materno" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="rfc" value="{{ (isset(session('inputs')['rfc'])) ? session('inputs')['rfc'] : "" }}" placeholder="R.F.C" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="colonia" value="{{ (isset(session('inputs')['colonia'])) ? session('inputs')['colonia'] : "" }}" placeholder="Colonia" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="fecha_alta" value="{{ (isset(session('inputs')['fecha_alta'])) ? session('inputs')['fecha_alta'] : "" }}" placeholder="Fecha de Alta" class="form-control custom_input" /></th>
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-submit"><i class="fas fa-search"></i></button></th>
                            </form>
                        </tr>
                        <tr>
                            <th scope="col" style="width:5%;">Cuenta</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Apellido Paterno</th>
                            <th scope="col">Apellido Materno</th>
                            <th scope="col">R.F.C</th>
                            <th scope="col">Colonia</th>
                            <th scope="col">Fecha de Alta</th>
                            <th scope="col" style="width:5%;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="small-font">
                        @foreach ($taxpayers as $taxpayer)
                            <tr>
                                <td>{{ $taxpayer->IdContribuyente }}</td>
                                <td>{{ $taxpayer->Nombre }}</td>
                                <td>{{ $taxpayer->ApellidoPaterno }}</td>
                                <td>{{ $taxpayer->ApellidoMaterno }}</td>
                                <td>{{ $taxpayer->RFC }}</td>
                                <td>{{ $taxpayer->Colonia }}</td>
                                <td>{{ $taxpayer->FechaAlta }}</td>
                                <td align="center"><button type="button" class="btn btn-primary edit-taxpayer btn-action-table" data-id="{{ $taxpayer->IdContribuyente }}"><i class="fas fa-edit"></i></button></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="table-footer">
                    <div class="pagination">
                        <span>Mostrando {{ (($taxpayers->currentPage() * 15) - 14) }} a {{( $taxpayers->currentPage() * 15) }} de {{ $taxpayers->total() }} Filas</span>
                        {{ $taxpayers->appends(Request::only('IdContribuyente'))->links() }} 
                    </div>
                    <div class="button-exports">
                        <a href="{{ route('print.taxpayers') }}" target="_BLANK" class="btn btn-success edit-taxpayer"><i class="fas fa-pdf"></i> Exportar a PDF</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('modals')
    @can('write_contribuyentes')
        @include('partials.modals.padron.modal_add_taxpayer')
    @endcan
    @include('partials.modals.padron.modal_update_taxpayer')
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script>
        $(function(){
            $("#padron").addClass('active');
            // $("[name='persona']").bootstrapSwitch();
            // $("[name='update_persona']").bootstrapSwitch();
            $('input[name="fecha_alta"]').daterangepicker({
                autoUpdateInput: false,
                opens: 'left',
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aceptar",
                    "cancelLabel": "Limpiar",
                    "fromLabel": "De",
                    "toLabel": "A",
                    "customRangeLabel": "Personalizado",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agusto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                }
            });
            $('input[name="fecha_alta"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('input[name="fecha_alta"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            $(document).on("click", ".btn-submit", function(){
                $( ".custom_input" ).each(function( index, element ) {
                    $("#form-data").append($(this));
                });
                $("#form-data")[0].submit();
            });
            
            $(document).on("keypress", ".custom_input", function(e){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $( ".custom_input" ).each(function( index, element ) {
                        $("#form-data").append($(this));
                    });
                    $("#form-data")[0].submit();
                }
            });

            $(document).on('click', '.edit-taxpayer', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                    $('#update_persona').bootstrapToggle('off')
                $.ajax({
                    url: "{{ route('get.taxpayer.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    console.log(result);
                    var modal = $("#modal-update-taxpayer");
                    modal.find('#taxpayer').val(result.IdContribuyente);
                    modal.find('#update_cuenta').val(result.IdContribuyente);
                    modal.find('#update_rfc').val(result.RFC);
                    modal.find('#update_nombre').val(result.Nombre);
                    modal.find('#update_apellido_pat').val(result.ApellidoPaterno);
                    modal.find('#update_apellido_mat').val(result.ApellidoMaterno);
                    if(result.Sexo)
                    {
                        modal.find('#update_sexo').val(result.Sexo);
                    }else
                    {
                        modal.find('#update_sexo').val("I");
                    }
                    if(result.PersonaMoral == 1)
                    {
                        $('#update_persona').bootstrapToggle('on')
                    }
                    modal.find('#update_email').val(result.Email);
                    modal.find('#update_exterior').val(result.Exterior);
                    modal.find('#update_interior').val(result.Interior);
                    modal.find('#update_cp').val(result.CP);
                    modal.find('#update_localidad').val(result.Ciudad);
                    modal.find('#update_colonia').val(result.Colonia);
                    modal.find('#update_domicilio').val(result.Domicilio);
                    modal.find('#update_calle1').val(result.Cruce1);
                    modal.find('#update_calle2').val(result.Cruce2);
                    modal.find('#update_telefono_casa').val(result.TelefonoCasa);
                    modal.find('#update_telefono_trab').val(result.TelefonoTrabajo);
                    modal.find('#fecha_alta').val(result.FechaAlta);
                    modal.find('#update_user').val(result.ClaveUsuario);

                    modal.modal("show");
                });
            });
        });
    </script>
@endsection