@extends('layouts.index')

@section('title') Nuevo Giro @endsection

@section('css')
    
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Nuevo Giro</h3>
    </div>
</div>
<div class="panel-body">
    <div class="margin-fix panel-row-fluid">
        <form action="{{ route('store.turn') }}" method="POST">
            @csrf
            <div class="row margin-10 justify-content-center padding-15">
                <fieldset class="col-xl-12 col-sm-12 fieldset pb-3">
                    <legend class="legend">Información General</legend>
                    <div class="form-row">
                        <div class="col-6">
                            <label for="name">Nombre</label>
                            <input type="text"  class="form-control" id="name" name="name" value="{{ old("name") }}" required>
                        </div>
                        <div class="col-2">
                            <label for="cuota_fija">Cuota Fija</label>
                            <input type="number" step="0.01" class="form-control" id="cuota_fija" name="cuota_fija" value="{{ old("cuota_fija") }}" required>
                        </div>
                        <div class="col-2">
                            <label for="cuota_minima">Cuota Minima</label>
                            <input type="number" step="0.01" class="form-control" id="cuota_minima" name="cuota_minima" value="{{ old("cuota_minima") }}" required>
                        </div>
                        <div class="col-2">
                            <label for="cuota_maxima">Cuota Maxima</label>
                            <input type="number" step="0.01" class="form-control" id="cuota_maxima" name="cuota_maxima" value="{{ old("cuota_maxima") }}" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col">
                            <label for="tipo">Tipo Giro</label>
                            <select required class="form-control select2" name="tipo" id="tipo">
                                <option value="">Selecciones Tipo de Giro</option>
                                @foreach ($tiposGiro as $tipoGiro)
                                    <option value="{{ $tipoGiro->TipoGiro }}">{{ $tipoGiro->Nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label for="subtipo">SubTipo Giro</label>
                            <select required class="form-control select2" name="subtipo" id="subtipo">
                                <option value="">Selecciones SubTipo de Giro</option>
                                @foreach ($subTiposGiro as $subTipoGiro)
                                    <option value="{{ $subTipoGiro->SubTipoGiro }}">{{ $subTipoGiro->Nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label for="riesgo">Riesgo</label>
                            <select required class="form-control select2" name="riesgo" id="riesgo">
                                <option value="">Selecciones Riesgo</option>
                                @foreach ($riesgosGiros as $riesgoGiros)
                                    <option value="{{ $riesgoGiros->Riesgo }}">{{ $riesgoGiros->Nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label for="clase">Clase</label>
                            <select required class="form-control select2" name="clase" id="clase">
                                <option value="">Selecciones Clase</option>
                                @foreach ($clasesGiros as $claseGiros)
                                    <option value="{{ $claseGiros->IdClase }}">{{ $claseGiros->Nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-4">
                            <label for="concepto">Concepto</label>
                            <select required class="form-control select2" name="concepto" id="concepto">
                                <option value="">Selecciones el concepto</option>
                                @foreach ($conceptos as $concepto)
                                    <option value="{{ $concepto->Concepto }}">({{ $concepto->Concepto }}) {{ $concepto->Descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4">
                            <label for="claveHacienda">Clave Hacienda</label>
                            <select class="form-control select2" name="claveHacienda" id="claveHacienda">
                                <option value="">Selecciones la clave</option>
                                <option value="SARE">SARE</option>
                            </select>
                        </div>
                        <div class="col-4">
                            <label for="alcohol">¿Alcohol?</label>
                            <select @can('read_turns') disabled @endcan class="form-control select2" name="alcohol" id="alcohol">
                                <option value="0">No</option>
                                <option value="1">Sí</option>
                            </select>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="row margin-10 justify-content-center padding-15">
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend class="legend">Requisitos</legend>
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th class="text-center">Alta</th>
                                <th class="text-center">Cambio Domicilio</th>
                                <th class="text-center">Cambio Giro</th>
                                <th class="text-center">Traspaso</th>
                                <th class="text-center">Repocision Licencia</th>
                                <th class="text-center">Baja</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($documents as $document)
                                <tr>
                                    <td>{{ $document->Nombre }}</td>
                                    <td align="center" ><input type="checkbox" value="1" name="check_{{ $document->Requisito }}_al"></td>
                                    <td align="center" ><input type="checkbox" value="1" name="check_{{ $document->Requisito }}_cd"></td>
                                    <td align="center" ><input type="checkbox" value="1" name="check_{{ $document->Requisito }}_cg"></td>
                                    <td align="center" ><input type="checkbox" value="1" name="check_{{ $document->Requisito }}_tr"></td>
                                    <td align="center" ><input type="checkbox" value="1" name="check_{{ $document->Requisito }}_rl"></td>
                                    <td align="center" ><input type="checkbox" value="1" name="check_{{ $document->Requisito }}_ba"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </fieldset>
            </div>
            <div class="row margin-10 justify-content-center ">
                <button type="submit" class="btn btn-primary mr-2 mb-2 mt-2"><i class="fas fa-save mr-2"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $("#padron").addClass('active');
        });
    </script>
@endsection