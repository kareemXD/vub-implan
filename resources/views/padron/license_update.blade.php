@extends('layouts.index')

@section('title') Licencia @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Licencia ( {{ $license->NumeroLicencia }} )</h3>
    </div>
</div>
@php
    $turndetails = $license->getTurnsDetails();
@endphp
<div class="panel-body @can('write_requests') with-buttons @endcan">
    <div class="margin-fix panel-row-fluid">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-link active" id="location-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Ubicación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="data-tab" data-toggle="tab" href="#data" role="tab" aria-controls="data" aria-selected="true">Solicitud</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link" id="period-tab" data-toggle="tab" href="#turn" role="tab" aria-controls="turn" aria-selected="false">Giro</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link" id="period-tab" data-toggle="tab" href="#requests" role="tab" aria-controls="requests" aria-selected="false">Tramites</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link" id="payments-tab" data-toggle="tab" href="#payments" role="tab" aria-controls="payments" aria-selected="false">Historial de Pagos</a>
            </li>
        </ul>
        <form id="form-data" action="{{ route('store.update.license') }}" method="POST" class="tab-content small-form" style="height:calc(100% - 60px)">
            @csrf
            <input type="hidden" name="id" value="{{ $license->NumeroLicencia }}">
            <input type="hidden" name="operation" value="update">
            <div class="tab-pane fade show active" id="location" role="tabpanel" aria-labelledby="location-tab">
                @if(session()->has('alert'))
                    <div class="alert alert-primary" role="alert">
                        {{ session("alert") }}
                    </div>
                @endif
                <div class="form-group row margin-10 mt-2">
                    <label for="city" class="col-sm-2 col-form-label" >Poblacion</label>
                    <div class="col-sm-10">
                        <select name="city" id="city" class="select2 form-control  {{ $errors->has('city') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Población</option>
                            @foreach ($poblaciones as $poblacion)
                                @if ($poblacion->IdPoblacion == $license->IdPoblacion)
                                    <option selected value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @else
                                    <option value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10 mt-2">
                    <label for="colony" class="col-sm-2 col-form-label">Colonia</label>
                    <div class="col-sm-4">
                        <select name="colony" id="colony" class="select2 form-control {{ $errors->has('colony') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Colonia</option>
                        </select>
                        @if ($errors->has('colony'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('colony') }}</strong>
                            </span>
                        @endif
                    </div>
                    <label for="zone" class="col-sm-2 col-form-label">Zona</label>
                    <div class="col-sm-4">
                        <select name="zone" id="zone" class="select2 form-control {{ $errors->has('zone') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Zona</option>
                            @foreach ($zonas as $zona)
                                @if ($zona->IdZona == $license->IdZona)
                                    <option selected value="{{ $zona->IdZona }}">{{ $zona->Nombre }}</option>
                                @else
                                    <option value="{{ $zona->IdZona }}">{{ $zona->Nombre }}</option>
                                @endif
                                <option value="{{ $zona->IdZona }}">{{ $zona->Nombre }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('zone'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('zone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10 ">
                    <label for="domicilio2" class="col-sm-2 col-form-label">Calle</label>
                    <div class="col-sm-10">
                        <select name="domicilio2" id="domicilio2" class="select2 form-control {{ $errors->has('domicilio2') ? ' is-invalid' : '' }}" >
                            <option value="">Seleccione una Calle</option>
                        </select>
                        @if ($errors->has('domicilio2'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('domicilio2') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="calle1_u" class="col-sm-2 col-form-label">Entre Calle</label>
                    <div class="col-sm-6">
                        <select name="calle1_u" id="calle1_u" class="select2 form-control {{ $errors->has('calle1_u') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Entre Calle</option>
                        </select>
                        @if ($errors->has('calle1_u'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle1_u') }}</strong>
                            </span>
                        @endif
                    </div>
                    <label for="ext" class="col-sm-2 col-form-label">Exterior</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control {{ $errors->has('ext') ? ' is-invalid' : '' }}" id="ext" name="ext" value="{{ $license->Exterior }}">
                        @if ($errors->has('ext'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ext') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="calle2_u" class="col-sm-2 col-form-label">Y Calle</label>
                    <div class="col-sm-6">
                        <select name="calle2_u" id="calle2_u" class="select2 form-control {{ $errors->has('calle2_u') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Entre Calle</option>
                        </select>
                        @if ($errors->has('calle2_u'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle2_u') }}</strong>
                            </span>
                        @endif
                    </div>
                    <label for="int" class="col-sm-2 col-form-label">Interior</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control {{ $errors->has('int') ? ' is-invalid' : '' }}" id="int" name="int" value="{{ $license->Interior }}">
                        @if ($errors->has('int'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('int') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="este" class="col-sm-2 col-form-label">Coordenada Este</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="form-control" id="este" name="este" value="{{ $license->CoordenadaEste }}">
                    </div>
                    <label for="norte" class="col-sm-2 col-form-label">Coordenada Norte</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="form-control" id="norte" name="norte" value="{{ $license->CoordenadaNorte }}">
                    </div>
                </div>
                <div id="map" class="map" style="width:100%; height:500px;"></div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="button" class="btn btn-primary mr-2 mb-2 mt-2 btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
            
            <div class="tab-pane fade " id="data" role="tabpanel" aria-labelledby="data-tab">
                <div class="form-row mt-3">
                    <div class="form-group mt-1 mb-1 col-4"> 
                        <label for="idtaxpayer">Contribuyente</label>
                        <select id="idtaxpayer" name="idtaxpayer" class="js-data-taxpayers-ajax form-control {{ $errors->has('idtaxpayer') ? ' is-invalid' : '' }}">
                            <option value="{{ $license->IdContribuyente }}">{{ $license->taxpayer->NombreCompleto.' - #'.$license->IdContribuyente }}</option>
                        </select>
                        {{-- <input required maxlength="100" type="text" class="required form-control {{ $errors->has('idtaxpayer') ? ' is-invalid' : '' }}" id="idtaxpayer" name="idtaxpayer" value="{{ old('idtaxpayer') }}" > --}}
                        @if ($errors->has('idtaxpayer'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('idtaxpayer') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group my-0 mb-1 col-2 d-flex align-items-end">
                        <a target="_blank" href="{{ route('contribuyentes') }}" class="btn btn-secondary ml-2"><i class="fas fa-external-link-alt"></i> Todos los contribuyentes</a>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="nombre">Nombre</label>
                        <input disabled required maxlength="100" type="text" class="form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre') }}" >
                        @if ($errors->has('nombre'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="apellido_pat">Apellido Paterno</label>
                        <input disabled required maxlength="50" type="text" class="form-control {{ $errors->has('apellido_pat') ? ' is-invalid' : '' }}" id="apellido_pat" name="apellido_pat" value="{{ old('apellido_pat') }}" >
                        @if ($errors->has('apellido_pat'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellido_pat') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="apellido_mat">Apellido Materno</label>
                        <input disabled required maxlength="50" type="text" class="form-control {{ $errors->has('apellido_mat') ? ' is-invalid' : '' }}" id="apellido_mat" name="apellido_mat" value="{{ old('apellido_mat') }}">
                        @if ($errors->has('apellido_mat'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('apellido_mat') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="rfc">R.F.C</label>
                        <input disabled required maxlength="15" type="text" class="form-control {{ $errors->has('rfc') ? ' is-invalid' : '' }}" id="rfc" name="rfc" value="{{ old('rfc') }}" placeholder="XAXX010101000">
                        @if ($errors->has('rfc'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('rfc') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="sexo">Sexo</label>
                        <select disabled name="sexo" id="sexo" class="form-control {{ $errors->has('sexo') ? ' is-invalid' : '' }}">
                            <option value="I">Seleccione Sexo</option>
                            <option value="I">Indefinido</option>
                            <option value="M">Hombre</option>
                            <option value="F">Mujer</option>
                        </select>
                        @if ($errors->has('sexo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('sexo') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="persona">Persona Moral ?</label>
                        <input disabled type="checkbox" data-toggle="toggle"  data-on="Si" data-off="No" class="form-control " id="persona" name="persona">
                        @if ($errors->has('persona'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('persona') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="domicilio">Domicilio</label>
                        <input disabled required maxlength="60" type="text" class="form-control {{ $errors->has('domicilio') ? ' is-invalid' : '' }}" id="domicilio" name="domicilio" value="{{ old('domicilio') }}" >
                        @if ($errors->has('domicilio'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="calle1">Entre la Calle</label>
                        <input disabled type="text" maxlength="60" class="form-control {{ $errors->has('calle1') ? ' is-invalid' : '' }}" id="calle1" name="calle1" value="{{ old('calle1') }}" >
                        @if ($errors->has('calle1'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle1') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="calle2">y la Calle</label>
                        <input disabled type="text" maxlength="60" class="form-control {{ $errors->has('calle2') ? ' is-invalid' : '' }}" id="calle2" name="calle2" value="{{ old('calle2') }}" >
                        @if ($errors->has('calle2'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('calle2') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="exterior">Numero Exterior</label>
                        <input disabled required type="text" maxlength="50" class="form-control {{ $errors->has('exterior') ? ' is-invalid' : '' }}" id="exterior" name="exterior" value="{{ old('exterior') }}" >
                        @if ($errors->has('exterior'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('exterior') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="interior">Numero Interior</label>
                        <input disabled type="text" maxlength="50" class="form-control {{ $errors->has('interior') ? ' is-invalid' : '' }}" id="interior" name="interior" value="{{ old('interior') }}" >
                        @if ($errors->has('interior'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('interior') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="cp">C.P.</label>
                        <input disabled required type="number" maxlength="5" class="form-control {{ $errors->has('cp') ? ' is-invalid' : '' }}" id="cp" name="cp" value="{{ old('cp') }}" >
                        @if ($errors->has('cp'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('cp') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="localidad">Localidad</label>
                        <input disabled required maxlength="40" type="text" class="form-control {{ $errors->has('localidad') ? ' is-invalid' : '' }}" id="localidad" name="localidad" value="{{ old('localidad') }}" >
                        @if ($errors->has('localidad'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('localidad') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="colonia">Colonia</label>
                        <input disabled required maxlength="40" type="text" class="form-control {{ $errors->has('colonia') ? ' is-invalid' : '' }}" id="colonia" name="colonia" value="{{ old('colonia') }}" >
                        @if ($errors->has('colonia'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('colonia') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="licenses-by-taxpayer"></div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="button" class="btn btn-primary mr-2 mb-2 mt-2 btn-next" data-target="#turn">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

            <div class="tab-pane fade" id="turn" role="tabpanel" aria-labelledby="turn-tab">
                <div class="row">
                    <div class="col-md-7">
                        <legend>Datos del negocio</legend>
                        <div class="form-group row mt-2 margin-10">
                            <label for="name_b" class="col-sm-2 col-form-label">Nombre del Negocio</label>
                            <div class="col-sm-10">
                                <input  type="text" class="form-control {{ $errors->has('name_b') ? ' is-invalid' : '' }}" id="name_b" name="name_b" value="{{ $license->NombreNegocio }}">
                                @if ($errors->has('name_b'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name_b') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- <div class="form-group row margin-10">
                            <label for="linderos" class="col-sm-2 col-form-label">Linderos</label>
                            <div class="col-sm-10">
                                <textarea class="form-control {{ $errors->has('linderos') ? ' is-invalid' : '' }}" id="linderos"  name="linderos" rows="2">{{ $license->Linderos }}</textarea>
                                @if ($errors->has('linderos'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('linderos') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> --}}
                        <div class="form-group row margin-10">
                            <label for="observations" class="col-sm-2 col-form-label">Observaciones</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control {{ $errors->has('observations') ? ' is-invalid' : '' }}" id="observations" name="observations" value="{{ $license->Observaciones }}">
                                @if ($errors->has('observations'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('observations') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @if (!empty($license->PermisoAlcohol))
                            <div class="form-group mt-1 mb-1 row margin-10" id="alcohol-div">
                                <label for="alcohol" class="col-sm-2 col-form-label">Permiso de Alcohol</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control {{ $errors->has('alcohol') ? ' is-invalid' : '' }}" id="alcohol" name="alcohol" value="{{ $license->PermisoAlcohol }}">
                                    @if ($errors->has('alcohol'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('alcohol') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif
                        <div class="form-group row margin-10">
                            <label data-legends="{{ json_encode($license->legends()->pluck("Leyendas.IdLeyenda")->toArray()) }}" for="legends" class="col-sm-2 col-form-label">Leyendas</label>
                            <div class="col-sm-4">
                                <select name="legends[]" id="legends" class="required select2 form-control {{ $errors->has('legends') ? ' is-invalid' : '' }}" multiple>
                                    <option value="">Seleccione las Leyendas</option>
                                    @foreach ($legends as $legend)
                                        <option value="{{ $legend->IdLeyenda }}">{{ $legend->NombreLeyenda }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('legends'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('legends') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <label for="surface" class="col-sm-1 col-form-label">Superficie</label>
                            <div class="col-sm-2">
                                <input  type="number" step="0.00" class="form-control {{ $errors->has('surface') ? ' is-invalid' : '' }}" id="surface" name="surface" value="{{ $license->Superficie }}">
                                @if ($errors->has('surface'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surface') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <label for="employees" class="col-sm-1 col-form-label">Empleados</label>
                            <div class="col-sm-2">
                                <input  type="number" class="form-control {{ $errors->has('employees') ? ' is-invalid' : '' }}" id="employees" name="employees" value="{{ $license->EmpleosCreados }}">
                                @if ($errors->has('employees'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('employees') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group mt-1 mb-1 row margin-10">
                            <label for="alta" class="col-sm-2 col-form-label">Fecha Alta</label>
                            <div class="col-sm-3">
                                <input type="date" class="required form-control {{ $errors->has('alta') ? ' is-invalid' : '' }}" id="alta" name="alta" value="{{ substr($license->FechaAlta, 0, 10) }}">
                                @if ($errors->has('alta'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('alta') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <label for="tarjeton_alcohol" class="col-sm-3 col-form-label">Num. Tarjeton de Alcoholes</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="tarjeton_alcohol" name="tarjeton_alcohol" value="{{ $license->NumeroTarjetonAlcoholes }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="mb-2">
                            <legend>Basura</legend>
                            <button type="button" class="btn btn-sm btn-primary btn-add-garbage"><i class="fas fa-plus"></i> Agregar</button>
                        </div>
                        <div class="garbage-wrapper">
                            <table class="table table-striped table-sm table-garbage">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Año</th>
                                        <th width="8%">Concepto</th>
                                        <th>Descripcion</th>
                                        <th>Importe</th>
                                        <th width="5%">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($license->garbages()->orderBy('Year', 'desc')->get() as $key => $garbage)
                                        <tr class="garbage-row" data-element="{{ $garbage->Year }}">
                                            <td><input type="number" placeholder="Año" name="garbage_year[]" value="{{ $garbage->Year }}" class="form-control form-control-sm" required></td>
                                            <td> {{ $garbage->Concepto }} 
                                                <input type="hidden" placeholder="Concepto" name="garbage_concept[]" value="{{ $garbage->Concepto }}" class="form-control form-control-sm">
                                            </td>
                                            <td>{{ $garbage->Descripcion }}</td>
                                            <td><input type="number" placeholder="Importe" name="garbage_amount[]" value="{{ $garbage->Importe }}" class="form-control form-control-sm" required></td>
                                            <td>
                                                @if (($key % 2) == 0)
                                                    <button class="btn btn-sm btn-danger btn-remove-garbage" type="button" data-element="{{ $garbage->Year }}"><i class="fas fa-trash"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <legend>Giros</legend>
                <table class="table table-sm table-hover dataTable" style="">
                    <thead>
                        <tr>
                            <th colspan="4">
                                {{-- <input type="text" id="cuenta_serach" placeholder="ID" class="form-control custom_input" /> --}}
                                <select id="cuenta_serach" name="cuenta_serach" class="js-data-turns-ajax form-control"></select>
                                <input type="hidden" disabled id="description_search" placeholder="Descripción" class="form-control custom_input" />
                            </th>
                            <th><input type="date" id="date_search" placeholder="Fecha" class="form-control custom_input" /></th>
                            <th>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="giro_refrendo" value="1"> &nbsp; Refrendo
                                    </label>
                                </div>
                            </th>
                            <th><input type="number" id="quantity_search" placeholder="Cantidad" class="form-control custom_input" /></th>
                            <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-add-turn"><i class="fas fa-plus"></i></button></th>
                        </tr>
                        <tr>
                            <th width="10%">IdGiro</th>
                            <th>Descripción</th>
                            <th>Observaciones</th>
                            <th width="10%">Fecha</th>
                            <th width="10%">Cuota</th>
                            <th width="10%">Cantidad</th>
                            <th width="10%">Subtotal</th>
                            <th width="10%">Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="turns-content">
                        
                        @if(isset($turndetails["data"]))
                        @foreach ($turndetails["data"] as $turn)
                            <tr>
                                <input type="hidden" class="type" name="type[]" value="{{ $turn['type'] }}"/>
                                <td><input type="hidden" class="cuenta_serach" name="cuenta_serach[]" value="{{ $turn['id'] }}"/>{{ $turn['id'] }}</td>
                                <td><input type="hidden" class="description_search" name="description_search[]" value="{{ $turn['nombre'] }}"/> {{ $turn['nombre'] }}</td>
                                <td><input type="hidden" class="observations_search" name="observations_search[]" value="{{ $turn['observation'] }}"/> {{ $turn['observation'] }}</td>
                                <td><input type="hidden" class="date_search" name="date_search[]" value="{{ $turn['date'] }}"/>{{ $turn['date'] }}</td>
                                <td><input type="hidden" class="cost_search" name="cost_search[]" value="{{ $turn['cost'] }}"/>$ {{ $turn['cost'] }}</td>
                                <td><input type="hidden" class="quantity_search" name="quantity_search[]" value="{{ $turn['quantity'] }}"/> {{ $turn['quantity'] }}</td>
                                <td>$ {{ $turn['subtotal'] }}</td>
                                <td><button type="button" class="btn btn-danger btn-action-table btn-delete-turn"><i class="fas fa-trash"></i></button></td>
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
                <div class="row margin-10 mt-2">
                    <div class="col-8">
                        <span>El costo de los anexos depende del porcentaje de pago para altas y refrendos registrados en preferencias y que en este momento es de <b>{{ $trimester["percentage"] }} %</b> del costo anual.</span>
                    </div>
                    <div class="col-4">
                        <div class="form-group row margin-10">
                            <label for="costo_f" class="col-sm-4 col-form-label">Costo Forma</label>
                            <div class="col-sm-8">
                                <input disabled type="text" class="form-control {{ $errors->has('costo_f') ? ' is-invalid' : '' }}" id="costo_f" value="$ 0.00">
                            </div>
                        </div>
                        <div class="form-group row mt-2 margin-10">
                            <label for="costo_t" class="col-sm-4 col-form-label">Costo Total</label>
                            <div class="col-sm-8">
                                <input disabled type="text" class="form-control {{ $errors->has('costo_t') ? ' is-invalid' : '' }}" id="costo_t" value="$ 0.00">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
            
            <div class="tab-pane fade" id="requests" role="tabpanel" aria-labelledby="requests-tab" style="height:100%">
                @if ($license->alerts()->where("PermitirTramites", 0)->count() > 0)
                    <div class="alert alert-primary" role="alert">
                        Esta licencia tiene una Alerta que no le permite realizar Trámites.
                    </div>
                @endif
                <div class="row row-top">
                    <div class="col-6 col-lft">
                        <div class="card">
                            <div class="card-header">
                                Trámites
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                                    <thead>
                                        <tr>
                                            <th>No. Solicitud</th>
                                            <th>Tipo Tramite</th>
                                            <th width="10%">Estatus</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="turns-content">
                                        @foreach ($license->requests as $request)
                                            <tr>
                                                <td width="30%">{{ $request->NumeroSolicitud }}</td>
                                                <td width="30%">
                                                @foreach ($request->process as $process)
                                                    {{ $process->Nombre . ", " }}
                                                @endforeach
                                                </td>
                                                <td width="10%" class="{{ ($request->Aplicado == 1)? 'bg-green' : 'bg-yellow' }}">{{ ($request->Aplicado == 1)? 'Aplicado' : 'En Tramite' }}</td>
                                                <td width="5%">
                                                    <a href="{{ route('update.request', [$request->NumeroSolicitud, "update"]) }}" class="btn btn-primary btn-action-table" data-toggle="tooltip" data-placement="top" title="Ver Solicitud"><i class="fas fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-rgt">
                        <div class="card">
                            <div class="card-header">
                                <span>Alertas</span>
                                <button type="button" class="btn btn-primary btn-action-table float-rigth" data-toggle="modal" data-target="#modal-add-alert" data-date="{{ now()->format('Y-m-d') }}" data-operation="store"><i class="fas fa-plus"></i></button>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>De</th>
                                            <th>Alerta</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($license->alerts() as $alert)
                                            <tr>
                                                <td>{{ $alert->FechaAlta }}</td>
                                                <td>{{ $alert->De }}</td>
                                                <td>{{ $alert->Alerta }}</td>
                                                <td>
                                                    <button data-toggle="modal" data-target="#modal-add-alert" type="button" class="btn btn-primary btn-action-table btn-update-alert"
                                                            data-id="{{ $alert->IdAlerta }}"
                                                            data-date="{{ \Carbon\Carbon::parse($alert->FechaAlta)->toDateString() }}"
                                                            data-from="{{ $alert->De }}"
                                                            data-resquests="{{ $alert->PermitirTramites }}"
                                                            data-description="{{ $alert->Alerta }}"
                                                            data-operation="update"><i class="fas fa-edit"></i></button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Proteccion Civil</legend>
                    <div class="row">
                        <div class="col-7">
                            <div class="d-flex justify-content-between">
                                <h4>Tramites</h4>
                                @if ($license->pcRequirements()->count() > 0)
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#newFormalityLicenseModal"><i class="fas fa-plus"></i> Nuevo tramite</button>
                                @else
                                    <div class="bg-warning text-dark px-3 py-2">Suba los requisitos necesarios para poder solicitar algún tramite de Protección civil.</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-5">
                            <h4>Requisitos</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <table id="tableFormalities" class="table table-sm table-hover" style="margin: 1.5em 0px !important;">
                                <thead>
                                    <tr>
                                        <th width="8%">#</th>
                                        <th>Tramite</th>
                                        <th>Comentarios</th>
                                        <th width="10%">Estatus</th>
                                        <th width="8%">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($license->pcRequests as $pcRequest)
                                        <tr>
                                            <td>{{ $pcRequest->pcRequest->requestNumberText() }}</td>
                                            <td>Visto Bueno</td>
                                            <td>{{ $pcRequest->pcRequest->resultado_dictaminacion }}</td>
                                            <td class="{{ $pcRequest->pcRequest->statusClass() }}">{{ $pcRequest->pcRequest->status() }}</td>
                                            <td>
                                                @if ($pcRequest->pcRequest->status() == 'PENDIENTE')
                                                    <a href="{{ route('pc.requests.edit', $pcRequest->pcRequest->id) }}?redirect=padron_license" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                                                @else
                                                    <a href="{{ route('pc.requests.detail', $pcRequest->pcRequest->id) }}" class="btn btn-secondary btn-sm"><i class="fas fa-eye"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-5">
                            <table class="table table-sm table-hover" style="margin: 1.5em 0px !important;">
                                <thead>
                                    <tr>
                                        <th>Requisito</th>
                                        <th width="10%">Estatus</th>
                                        <th width="10%">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($requirements as $requirement)
                                        <tr>
                                            <td>{{ $requirement->requisito }}</td>
                                            <td class="{{ $license->pcRequirementStatusClass($requirement->id) }}">{{ $license->pcRequirementStatus($requirement->id) }}</td>
                                            <td class="d-flex">
                                                <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#updatePcRequirementLicenseModal" data-requirement="{{ $requirement->requisito }}" data-requirementid="{{ $requirement->id }}" title="Subir / Actualizar documento"><i class="fas fa-edit"></i></button>
                                                @if ($license->pcRequirementStatus($requirement->id) != 'Pendiente')
                                                    <a href="{{ asset('storage/'.$license->pcRequirements()->where('id_requisito', $requirement->id)->first()->documento) }}" class="btn btn-secondary btn-sm ml-1" target="_blank" title="Descargar documento"><i class="fas fa-download"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </fieldset>
                <br>
                <div class="row row-bot">
                    {{-- <div class="col-6 col-lft">
                        <div class="card">
                            <div class="card-header">
                                Multas
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Usuario</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-12 col-rgt">
                        <div class="card">
                            <div class="card-header">
                                Bitacora
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Usuario</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($license->bitacora() as $item)
                                            <tr>
                                                <td>{{ $item->FechaAlta }}</td>
                                                <td>{{ $item->Usuario }}</td>
                                                <td>{{ $item->DescripcionCompleta }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
            
            <div class="tab-pane fade" id="payments" role="tabpanel" aria-labelledby="payments-tab" style="height:100%">
                <legend class="text-center">Pagos</legend>
                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                    <thead>
                        <tr>
                            <th>No. Recibo</th>
                            <th>Año</th>
                            <th>Fecha Pago</th>
                            <th>Descripción</th>
                            <th>Importe</th>
                            <th>Estatus</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($license->pagos()->orderBy('Recibo', 'ASC')->get() as $item)
                            <tr>
                                <td>{{ $item->Recibo }}</td>
                                <td>{{ substr($item->Referencia, 0, 4) }}</td>
                                <td>{{ substr($item->FechaPago, 0, 10) }}</td>
                                <td>{{ $item->Descripcion }}</td>
                                <td>{{ $item->Importe }}</td>
                                <td>
                                    @if ($item->Estado == 'B')
                                        Pagado
                                    @elseif ($item->Estado == 'C')
                                        Cancelado
                                    @else
                                        {{ $item->Estado }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
            </div>
        </form>
    </div>
</div>
@can('write_requests')
    <div class="row justify-content-end panel-buttoms">
        <button type="button" class="btn btn-primary mr-3 btn-next " id="btn-previous" style="display:none;" data-target="#data"><i class="fas fa-arrow-left mr-2"></i> Anterior</button>
        <button type="button" class="btn btn-primary mr-3 btn-next" id="btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
        @if ($license->alerts()->where("PermitirTramites", 0)->count() == 0)
            @if (auth()->user()->can('padron_licencias_print_license'))
                <a style="display:none;" id="btn-print" href="{{ route('print.license', [$license->NumeroLicencia]) }}" target="_BLANK" class="btn btn-primary btn-next ml-2">Imprimir Licencia</a>
            @endif
            <a style="display:none;" id="btn-print-cost" href="{{ route('print.cost.license', [$license->NumeroLicencia]) }}" target="_BLANK" class="btn btn-primary btn-next ml-2">Orden de Pago</a>
            <a style="display:none;" id="btn-print-account" href="{{ route('print.account.license', [$license->NumeroLicencia]) }}" target="_BLANK" class="btn btn-primary btn-next ml-2">Estado de cuenta</a>
            {{-- <a style="display:none;" id="btn-ampliacion" href="{{ route('add.request.license', [$license->NumeroLicencia, 'A']) }}" class="btn btn-primary btn-next ml-2">Ampliación de Giro</a> --}}
            {{-- <a style="display:none;" id="btn-domicilio" href="{{ route('add.request.license', [$license->NumeroLicencia, 'D']) }}" class="btn btn-primary btn-next ml-2">Cambio Domicilio</a> --}}
            {{-- <a style="display:none;" id="btn-repocision" href="{{ route('add.request.license', [$license->NumeroLicencia, 'E']) }}" class="btn btn-primary btn-next ml-2">Repocisión Licencia</a> --}}
            {{-- <a style="display:none;" id="btn-cambio" href="{{ route('add.request.license', [$license->NumeroLicencia, 'G']) }}" class="btn btn-primary btn-next ml-2">Cambio Giro</a> --}}
            {{--<a style="display:none;" id="btn-traspaso" href="{{ route('add.request.license', [$license->NumeroLicencia, 'T']) }}" class="btn btn-primary btn-next ml-2">Traspaso</a> --}}
            {{--<a style="display:none;" id="btn-anexo" href="{{ route('add.request.license', [$license->NumeroLicencia, 'Y']) }}" class="btn btn-primary btn-next ml-2 mr-3">Anexo</a> --}}
            <a style="display:none;" id="btn-baja" href="#modal-baja-licencia" data-toggle="modal" data-target="#modal-baja-licencia" class="btn btn-primary btn-next ml-2 mr-3">Baja de Licencia</a>
            <a style="display:none;" id="btn-baja" href="{{ route('print.taxpayer.detail', [$license->NumeroLicencia]) }}#modal-baja-licencia" data-toggle="modal" data-target="#modal-baja-licencia" class="btn btn-primary btn-next ml-2 mr-3">Baja de Licencia</a>
            <button type="submit" class="btn btn-primary mr-3" id="btn-save-taxpayer" style="display:none;"><i class="fas fa-save mr-2"></i> Modificar Contribuyente</button>
            @endif
            <button type="submit" class="btn btn-primary mr-3" id="btn-save-location" ><i class="fas fa-save mr-2"></i> Guardar Ubicación</button>
            <button  style="display:none" type="submit" class="btn btn-primary mr-3" id="btn-save-turn" ><i class="fas fa-save mr-2"></i> Guardar Giro</button>
            <button style="display:none" type="submit" class="btn btn-primary mr-3" id="btn-save-turn-data" ><i class="fas fa-save mr-2"></i> Guardar Datos</button>
        
    </div>
@endcan
@endsection

@section('modals')
    @include('partials.modals.padron.modal_add_alert')
    @include('partials.modals.padron.modal-baja-licencia')
    @include('partials.modals.padron.modal_update_pc_requirement_license')
    @include('partials.modals.padron.modal_new_formality_license')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('system.google.maps.apiKey') }}&libraries=places&v=weekly"
        defer
    ></script>
    <script>
        window.addEventListener('load', function () {
            OnChangeCity("{{ $license->IdColonia }}");
            OnChangeColony("{{ $license->IdCalle }}", "{{ $license->IdCruce1 }}", "{{ $license->IdCruce2 }}");
            
            @if(session()->has('document_updated'))
                $('#myTab a[href="#documents"]').tab('show');
            @endif

            @if(session()->has('show_formalities'))
                $('#myTab a[href="#requests"]').tab('show');
            @endif
        });
        $(function(){
            setTimeout(function(){ 
                $('#idtaxpayer').trigger('select2:select');

               updateTotalGiros();
               showTaxpayerLicenses();
            }, 100);

            $("#padron").addClass('active');

            $('.js-data-taxpayers-ajax').select2({
                minimumInputLength: 1,
                placeholder: "Contribuyente",
                allowClear: true,
                theme: "bootstrap",
                language: "es",
                ajax: {
                    url: "{{ route('ajax.taxpayers') }}",
                    type: 'post',
                    dataType: 'json',
                    delay: 400,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _token: "{{ csrf_token() }}"
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data.data.taxpayers
                        };
                    }
                }
            });

            $('.js-data-turns-ajax').select2({
                minimumInputLength: 1,
                placeholder: "Giro",
                allowClear: true,
                theme: "bootstrap",
                language: "es",
                ajax: {
                    url: "{{ route('ajax.turns') }}",
                    type: 'post',
                    dataType: 'json',
                    delay: 400,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _token: "{{ csrf_token() }}"
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data.data.turns
                        };
                    }
                }
            });
            
            // $('#update_persona').bootstrapToggle('on')
            var created = false;
            var map = L.map('map',{scrollWheelZoom:true}).setView([20.74689, -105.39425], 12);
            // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            //     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap Saul Moncivais</a> contributors'
            // }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:Vuelo_Bajo_Ortofoto@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 12,
                maxZoom: 23,
                attribution: '&copy; IMPLAN - Ortofoto Bahia de Banderas 2019'
            }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:vialidad_BB@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);

            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:lim_loc_final@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);
            var theMarker = {};
            var item = L.utm({x: "{{ $license->CoordenadaEste }}", y: "{{ $license->CoordenadaNorte }}", zone: 13, band: 'N'});
            var coord = item.latLng();
            theMarker = L.marker(coord).addTo(map);
            
            /*
                Actualizar marcador del mapa
            */
            $(document).on("keyup", '#ext', function(event){
                if(event.keyCode == 13){
                    getLocationFromAddress()
                }
            });

            $(document).on('change', '#city, #colony, #domicilio2, #ext', function() {
                getLocationFromAddress();
            });

            function getLocationFromAddress(){
                var address = "";

                if($("#domicilio2 option:selected").text().length > 2 && $("#domicilio2").val() != "" && $("#domicilio2").val() != 0){
                    address = address +  $("#domicilio2 option:selected").text();
                }

                if($("#ext").val() != ""){
                    address = address + " "+ $("#ext").val();
                }

                if($("#colony option:selected").text().length > 2 && $("#colony").val() != "" && $("#colony option:selected").text() != "BAHIA DE BANDERAS" && $("#city option:selected").text() !="Seleccione una Colonia" ){
                    address = address + ", "+ $("#colony option:selected").text();
                }

                if($("#city option:selected").text().length > 2 && $("#city").val() != "" && $("#city option:selected").text() != "BAHIA DE BANDERAS" && $("#city option:selected").text() != "Seleccione una Colonia"){
                    address = address + ", "+ $("#city option:selected").text();
                }

                if(address != ""){
                    var address = address + ", Bahía de banderas";

                    var geocoder = new google.maps.Geocoder();
                    var geocoderRequest = { address: address, componentRestrictions: {
                            country: "MX",
                            } };
                    geocoder.geocode(geocoderRequest, function(results, status){
                        window.geome = results;
                        if(results.length > 0){
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            };    

                            theMarker = L.marker({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}).addTo(map);
                            $("#este").val(theMarker.getLatLng().utm().x);
                            $("#norte").val(theMarker.getLatLng().utm().y);

                            map.setView({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}, 16);
                            
                        }else
                        {
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            }; 

                            $("#este").val(0);
                            $("#norte").val(0);

                            map.setView([20.74689, -105.39425], 12);
                        }
                    });
                }
                
            }
            
            map.on('click',function(e){
                if (theMarker != undefined) {
                    map.removeLayer(theMarker);
                };
                theMarker = L.marker(e.latlng).addTo(map);
                $("#este").val(theMarker.getLatLng().utm().x)
                $("#norte").val(theMarker.getLatLng().utm().y)
            });
            
            $(document).on("click", "#btn-save-taxpayer", function(){
                $("input[name=operation]").val('change_contributor');
                $("#form-data")[0].submit();
            });
            
            $(document).on("click", "#btn-applay-request", function(){
                $("input[name=operation]").val('apply_changes');
                $("#form-data")[0].submit();
            });

            $(document).on("click", "#btn-save-location", function(){
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción?',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            $("input[name=operation]").val('update_location');
                            $("#form-data")[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });

            $(document).on("click", "#btn-save-turn", function(){
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción?',
                    type: "green",
                    buttons: {
                        Aceptar: function () {
                            $("input[name=operation]").val('update_turn');
                            $("#form-data")[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });
            $(document).on("click", "#btn-save-turn-data", function(){
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Solo se guardaran los datos del Giro, Desea continuar?',
                    type: "green",
                    buttons: {
                        Aceptar: function () {
                            $("input[name=operation]").val('update_turn_data');
                            $("#form-data")[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });
            
            $(document).on("click", ".btn-update-document", function(){
                var _this = $(this);
                $.ajax({
                    url: "{{ route('get.document.by.request') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        requisito: _this.data("requisito"),
                        solicitud: $("input[name=id]").val()
                    },
                }).done(function(result){
                    console.log(result);
                    var modal = $("#modal-update-document");
                    modal.find("#requisito_id").val(result.Requisito);
                    modal.find("#nombre_r").val(result.Nombre);
                    modal.find("#comments").val(result.pivot_Comentario);
                    if(result.pivot_Archivo)
                    {
                        modal.find("#documents-content").prepend('<ul><li class="download-file" data-requisito="'+result.Requisito+'" data-solicitud="'+$("input[name=id]").val()+'">\
                            <img src="{{ asset("assets/plugins/preview/preview_pdf.png") }}" >\
                        </li></ul>');
                        $('.old_document').simpleFilePreview();
                    }
                    modal.modal("show");
                });
            });
            
            $(document).on('select2:select', '#idtaxpayer', function() {
                var id = $("#idtaxpayer").val(),
                    token = "{{ csrf_token() }}",
                    legends = $("[data-legends]").data("legends");
                    
                    $("#legends").prop("disabled", false);
                    
                    $("#legends").select2("val", legends);
                    $('#update_persona').bootstrapToggle('off');
                    showTaxpayerLicenses();
                $.ajax({
                    url: "{{ route('get.taxpayer.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $('#taxpayer').val(result.IdContribuyente);
                    $('#cuenta').val(result.IdContribuyente);
                    $('#rfc').val(result.RFC);
                    $('#nombre').val(result.Nombre);
                    $('#apellido_pat').val(result.ApellidoPaterno);
                    $('#apellido_mat').val(result.ApellidoMaterno);
                    if(result.Sexo)
                    {
                        $('#sexo').val(result.Sexo);
                    }else
                    {
                        $('#sexo').val("I");
                    }
                    if(result.PersonaMoral == 1)
                    {
                        $('#persona').bootstrapToggle('on')
                    }
                    $('#email').val(result.Email);
                    $('#exterior').val(result.Exterior);
                    $('#interior').val(result.Interior);
                    $('#cp').val(result.CP);
                    $('#localidad').val(result.Ciudad);
                    $('#colonia').val(result.Colonia);
                    $('#domicilio').val(result.Domicilio);
                    $('#calle1').val(result.Cruce1);
                    $('#calle2').val(result.Cruce2);
                    $('#telefono_casa').val(result.TelefonoCasa);
                    $('#telefono_trab').val(result.TelefonoTrabajo);
                });
            });
            
            $(document).on('select2:select', '#cuenta_serach', function (e) {
                var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.turn.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $('#description_search').val(result.Nombre);
                    $('#quantity_search').val(1);
                });
            });

            function showTaxpayerLicenses()
            {
                var taxpayerId = $('#idtaxpayer').val(),
                    token = "{{ csrf_token() }}";

                $('.licenses-by-taxpayer').load("{{ route('load.taxpayer.licences') }}", { taxpayerId: taxpayerId, _token: token });
            }
/*
            $(document).on("click", ".btn-add-turn", function(){
                var flag = true;
                $(this).parents("table").find(".custom_input").each(function() {
                    if(!$(this).val()){
                        flag = false;
                        $(this).addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                        }
                    }
                });
                if(flag){
                    var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                    $.ajax({
                        url: "{{ route('get.license.cost') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: $("#cuenta_serach").val(),
                            date: $("#date_search").val()
                        },
                    }).done(function(result){

                        $("#turns-content").append('\
                            <tr>\
                                <td><input type="hidden" class="cuenta_serach" name="cuenta_serach[]" value="'+$("#cuenta_serach").val()+'"/>'+$("#cuenta_serach").val()+'</td>\
                                <td><input type="hidden" class="description_search" name="description_search[]" value="'+$("#description_search").val()+'"/>'+$("#description_search").val()+'</td>\
                                <td><input type="hidden" class="observations_search" name="observations_search[]" value="'+$("#observations_search").val()+'"/>'+$("#observations_search").val()+'</td>\
                                <td><input type="hidden" class="date_search" name="date_search[]" value="'+$("#date_search").val()+'"/>'+$("#date_search").val()+'</td>\
                                <td><input type="hidden" class="cost_search" name="cost_search[]" value="'+parseFloat(result)+'"/>$ '+parseFloat(result).toFixed(2)+'</td>\
                                <td><input type="hidden" class="quantity_search" name="quantity_search[]" value="'+$("#quantity_search").val()+'"/>'+$("#quantity_search").val()+'</td>\
                                <td><button type="button" class="btn btn-danger btn-action-table btn-delete-turn"><i class="fas fa-trash"></i></button></td>\
                            </tr>');
                        $("#cuenta_serach").val("");
                        $("#description_search").val("");
                        $("#observations_search").val("");
                        $("#date_search").val("");
                        quantity = parseFloat($("#quantity_search").val());
                        $("#cost_search").val("");
                        $("#quantity_search").val("");
                        var cost = 0;
                        $(".cost_search").each(function() {
                            cost += parseFloat($(this).val()) * parseFloat($(this).parent().next().find(".quantity_search").val());
                        });
                        cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                        $("#costo_t").val("$ " + cost.toFixed(2));
                        
                    });
                }
            }); */
            $(document).on("click", ".btn-add-turn", function(){
                var flag = true;
                $(this).parents("table").find(".custom_input").each(function() {
                    if(!$(this).val()){
                        flag = false;
                        $(this).addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                        }
                    }
                });
                if(flag){
                    var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                    $.ajax({
                        url: "{{ route('get.license.cost.details') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: $("#cuenta_serach").val(),
                            date: $("#date_search").val(),
                            location: $("#city").val(),
                            quantity: $('#quantity_search').val(),
                            refrendo: $('[name="giro_refrendo"]:checked').val(),
                        },
                    }).done(function(result){
                        if(result.alcohol){
                            $("#alcohol-div").fadeIn();
                            $("#alcohol-div").find("#alcohol").addClass("required");
                        }
                        result.data.forEach(function(element){ 
                            $("#turns-content").append('\
                                <tr>\
                                    <input type="hidden" class="type" name="type[]" value="'+element.type+'"/>\
                                    <td><input type="hidden" class="cuenta_serach" name="cuenta_serach[]" value="'+element.id+'"/>'+element.id+'</td>\
                                    <td><input type="hidden" class="description_search" name="description_search[]" value="'+element.nombre+'"/>'+element.nombre+'</td>\
                                    <td><input type="hidden" class="observations_search" name="observations_search[]" value="'+element.observation+'"/>'+element.observation+'</td>\
                                    <td><input type="hidden" class="date_search" name="date_search[]" value="'+element.date+'"/>'+element.date+'</td>\
                                    <td><input type="hidden" class="cost_search" name="cost_search[]" value="'+parseFloat(element.cost)+'"/>$ '+parseFloat(element.cost).toFixed(2)+'</td>\
                                    <td><input type="hidden" class="quantity_search" name="quantity_search[]" value="'+element.quantity+'"/>'+element.quantity+'</td>\
                                    <td>$ '+element.subtotal+'</td>\
                                    <td><button type="button" class="btn btn-danger btn-action-table btn-delete-turn"><i class="fas fa-trash"></i></button></td>\
                                </tr>');
                        });

                        updateTotalGiros();

                        $("#cuenta_serach").val("").trigger("change");
                        $("#date_search").val("");
                        $("#cost_search").val("");
                        $("#quantity_search").val("");
                        $('[name="giro_refrendo"]').prop('checked', false);
                    });
                }
            });

            function updateTotalGiros()
            {
                var cost = 0;
                $('[name="cost_search[]"]').each(function() {
                    cost += parseFloat($(this).val()) * parseFloat($(this).parent().parent().find(".quantity_search").val());
                    
                });
                // cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                $("#costo_t").val("$ " + cost.toFixed(2));
            }
            
            $(document).on("click", ".btn-delete-turn", function(){
                var _this = $(this);
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción?',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            _this.parents("tr").fadeOut("slow", function(){
                                _this.parents("tr").remove();
                                
                                updateTotalGiros();
                            });
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });


            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var target = e.target.hash;
                switch (e.relatedTarget.hash) {
                    case "#data":
                        if(!$("#idtaxpayer").val())
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione un contribuyente para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        break;
                
                    case "#location":
                        if(!$("#norte").val() || !$("#este").val())
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione la ubicación para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        break;
                }

                switch (e.target.hash) {
                    case "#location":
                        $("#btn-previous").fadeOut("fast")
                        $("#btn-next").data("target", "#data")
                        break;
                    case "#data":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#location")
                        });
                        $("#btn-next").data("target", "#turn")
                        break;
                    case "#turn":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#data")
                        });
                        $("#btn-next").data("target", "#requests")
                        $("#btn-print").fadeOut("fast");
                        $("#btn-print-cost").fadeOut("fast");
                        $("#btn-print-account").fadeOut("fast");
                        break;
                    case "#requests":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#turn")
                        });
                        $("#btn-next").fadeIn("fast", function() {
                            $("#btn-next").data("target", "#payments")
                        });
                        $("#btn-print").fadeIn("fast");
                        $("#btn-print-cost").fadeIn("fast");
                        $("#btn-print-account").fadeIn("fast");
                        // $("#btn-ampliacion").fadeIn("fast");
                        // $("#btn-domicilio").fadeIn("fast");
                        // $("#btn-repocision").fadeIn("fast");
                        // $("#btn-cambio").fadeIn("fast");
                        // $("#btn-traspaso").fadeIn("fast");
                        // $("#btn-anexo").fadeIn("fast");
                        break;
                    case "#payments":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#requests")
                        });
                        $("#btn-next").fadeOut("fast");
                        $("#btn-print").fadeOut("fast");
                        $("#btn-print-cost").fadeOut("fast");
                        $("#btn-print-account").fadeOut("fast");
                        break;
                }
            });
            $("[href='#requests']").on("show.bs.tab", function(event){
                $("#btn-baja").fadeIn(500);
            });
            $("[href='#requests']").on("hide.bs.tab", function(event){
                $("#btn-baja").fadeOut(500);
            });

            $(document).on("click", ".btn-next", function(){
                $('#myTab a[href="'+$(this).data("target")+'"]').tab('show');
            });

            $(document).on("change", "#city", function(){
                OnChangeCity();
            });
            $(document).on("change", "#colony", function(){
                OnChangeColony();
            });

            //agregar taxapyer

            $("[href='#data']").on("show.bs.tab", function(event){
                $("#btn-save-taxpayer").fadeIn(500);
                $("#btn-save")
            });
            $("[href='#data']").on("hide.bs.tab", function(event){
                $("#btn-save-taxpayer").fadeOut(500);
            });

            $("[href='#location']").on("show.bs.tab", function(event){
                $("#btn-save-location").fadeIn(500);
            });
            $("[href='#location']").on("hide.bs.tab", function(event){
                $("#btn-save-location").fadeOut(500);
            });

            $("[href='#turn']").on("show.bs.tab", function(event){
                $("#btn-save-turn").fadeIn(500);
                $("#btn-save-turn-data").fadeIn(500);
            });
            $("[href='#turn']").on("hide.bs.tab", function(event){
                $("#btn-save-turn").fadeOut(500);
                $("#btn-save-turn-data").fadeOut(500);
            });

            var garbageCounter = 0;
            $(document).on('click', '.btn-add-garbage', function() {
                garbageCounter++;
                var year = "{{ now()->year }}";
                
                var rows = '<tr class="garbage-row" data-element="'+garbageCounter+'"> \
                            <td><input type="number" placeholder="Año" name="garbage_year[]" value="'+year+'" class="form-control form-control-sm" required></td> \
                            <td> 02057 \
                                <input type="hidden" placeholder="Concepto" name="garbage_concept[]" value="02057" class="form-control form-control-sm"> \
                            </td> \
                            <td>DESCARGA DE BASURA EN RELLENO SANITARIO M3</td> \
                            <td><input type="number" placeholder="Importe" name="garbage_amount[]" value="426.57" class="form-control form-control-sm" required></td> \
                            <td><button class="btn btn-sm btn-danger btn-remove-garbage" type="button" data-element="'+garbageCounter+'"><i class="fas fa-trash"></i></button></td> \
                        </tr> \
                        <tr class="garbage-row" data-element="'+garbageCounter+'"> \
                            <td><input type="number" placeholder="Año" name="garbage_year[]" value="'+year+'" class="form-control form-control-sm" required></td> \
                            <td> 02058 \
                                <input type="hidden" placeholder="Concepto" name="garbage_concept[]" value="02058" class="form-control form-control-sm"> \
                            </td> \
                            <td>RECOLECCION Y TRASLADO RESIDUOS SOLIDOS M3</td> \
                            <td><input type="number" placeholder="Importe" name="garbage_amount[]" value="230.42" class="form-control form-control-sm" required></td> \
                            <td></td> \
                        </tr>';

                $('.table-garbage').find('tbody').append(rows);
            });

            $(document).on('click', '.btn-remove-garbage', function() {
                var elementId = $(this).data('element');

                $('.table-garbage').find('.garbage-row[data-element="'+elementId+'"]').remove();
            });

            $('#updatePcRequirementLicenseModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    requirementId = button.data('requirementid'),
                    requirement = button.data('requirement'),
                    modal = $(this);

                modal.find('#nombre_r').val(requirement);
                modal.find('#requisito_id').val(requirementId);
            });
        });

        function OnChangeCity(colony = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.colonies.by.city') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#colony").empty();
                $("#colony").append("<option value=''>Seleccione una Colonia</option>");
                result.forEach(element => {
                    $("#colony").append($('<option>', {
                        value: element.IdColonia,
                        text: element.NombreColonia
                    }));
                });
                if(colony)
                {
                    $("#colony").val(colony);
                }
            });

            $(document).on("click", ".download-file", function(){
                var requisito = $(this).data("requisito");
                var solicitud = $(this).data("solicitud");
                window.open('/padron/mostrar/documento/'+requisito+'/'+solicitud, '_blank');
            });
        }

        function OnChangeColony(calle = null, calle1 = null, calle2 = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.calles.by.colony') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#domicilio2").empty();
                $("#domicilio2").append("<option value=''>Seleccione una Calle</option>");

                $("#calle1_u").empty();
                $("#calle1_u").append("<option value=''>Seleccione una Entre Calle</option>");

                $("#calle2_u").empty();
                $("#calle2_u").append("<option value=''>Seleccione una Entre Calle</option>");

                result.forEach(element => {
                    $("#domicilio2").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle1_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle2_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));
                });
                if(calle || calle1 || calle2)
                {
                    if($("#domicilio2 option[value='"+calle+"']").length > 0){
                        $("#domicilio2").val(calle);
                    }else{
                        $.get("{{ route('api.get-street-by-id') }}/"+calle, function(response){
                            $("#domicilio2").append("<option selected value='"+calle+"'>"+response.NombreCalle+"</option>");
                        });
                    }

                    if($("#calle1_u option[value='"+calle1+"']").length > 0){
                        $("#calle1_u").val(calle1);
                    }else{
                        $.get("{{ route('api.get-street-by-id') }}/"+calle1, function(response){
                            $("#calle1_u").append("<option selected value='"+calle1+"'>"+response.NombreCalle+"</option>");
                        });
                    }

                    if($("#calle2_u option[value='"+calle2+"']").length > 0){
                        $("#calle2_u").val(calle2);
                    }else{
                        $.get("{{ route('api.get-street-by-id') }}/"+calle2, function(response){
                            $("#calle2_u").append("<option selected value='"+calle2+"'>"+response.NombreCalle+"</option>");
                        });
                    }
                }
            });

            /******************************************************************
            *
            * Funtions for click Events
            *
            *******************************************************************/
            $('#modal-add-alert').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget), // Button that triggered the modal
                    alert = button.data('id'),
                    date = button.data('date'),
                    from = button.data('from'),
                    operation = button.data('operation'),
                    resquests = button.data('resquests'),
                    description = button.data('description'),
                    modal = $(this);
                    
                modal.find('input[name=operation]').val(operation);
                modal.find('.save-alert').attr('operation', operation);
                modal.find('input[name=alert]').val(alert);
                modal.find('#date').val(date);
                modal.find('#from').val(from);

                if(resquests == 1){
                    modal.find('#resquests').prop('checked', true); 
                }else{
                    modal.find('#resquests').prop('checked', false); 
                }
                modal.find('#description').val(description);
            });
        }
    </script>
@endsection