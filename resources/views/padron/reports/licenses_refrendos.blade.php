@extends('layouts.index')

@section('title') Reportes @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Reporte - Pagos Licencias y Refrendos</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <form id="reportForm" action="{{ route('detail.report.licensesRefrendos') }}" method="GET" target="_blank" class="row justify-content-center">
                <input type="hidden" name="type" value="view">
                <div class="col-6 ">
                    <div class="form-row mt-3">
                        <div class="form-group col">
                            <label for="start">Fecha de Inicio</label>
                            <input type="date" class="form-control required {{ $errors->has('start') ? ' is-invalid' : '' }}" id="start" name="start" required>
                            @if ($errors->has('start'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="end">Fecha de Termino</label>
                            <input type="date" class="form-control required {{ $errors->has('end') ? ' is-invalid' : '' }}" id="end" name="end" value="" required>
                            @if ($errors->has('end'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('end') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row mt-3">
                        <div class="form-group col" style="text-align: center">
                            <button type="button" class="btn btn-primary" id="btn-generate"><i class="fas fa-check-circle mr-2"></i> Generar Reporte</button> <br>
                            <button type="button" class="btn btn-success mt-3" id="btn-generate-all">Reporte Completo</button>
                            {{-- <button type="button" class="btn btn-success mr-3" id="btn-export"><i class="fas fa-file-excel"></i> Exportar Reporte</button> --}}
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $(document).on('click', '#btn-generate-all', function() {
                var firstDate = "{{ $firstDate }}",
                    today = "{{ now()->format('Y-m-d') }}";

                $('[name="start"]').val(firstDate);
                $('[name="end"]').val(today);

                $('#reportForm').submit();
            });

            $("#reports").addClass('active');

            $(document).on('click', '#btn-generate', function() {
                var form = $('#reportForm');

                if (validateForm())
                {
                    form.find('[name="type"]').val('view');
                    form.submit();
                }
            });

            $(document).on('click', '#btn-export', function() {
                var form = $('#reportForm');

                if (validateForm())
                {
                    form.find('[name="type"]').val('export');
                    form.submit();
                }
            });

            function validateForm()
            {
                console.log('Entro');
                var flag = true;
                $("#reportForm").find(".required").each(function() {
                    if(!$(this).val()){
                        flag = false;
                        $(this).addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                        }
                    }
                });

                return flag;
            }
            
        });
    </script>
@endsection