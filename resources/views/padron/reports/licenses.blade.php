@extends('layouts.index')

@section('title') Reportes @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Reportes</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <form id="form-data" action="{{ route('print.report.licenses') }}" method="POST" class="row justify-content-center">
                @csrf
                <div class="col-6 ">
                    <div class="form-row mt-3">
                        <div class="form-group col">
                            <label for="start">Fecha de Inicio</label>
                            <input type="date" class="form-control {{ $errors->has('start') ? ' is-invalid' : '' }}" id="start" name="start">
                            @if ($errors->has('start'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('start') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="end">Fecha de Termino</label>
                            <input type="date" class="form-control {{ $errors->has('end') ? ' is-invalid' : '' }}" id="end" name="end">
                            @if ($errors->has('end'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('end') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row mt-3">
                        <div class="form-group col">
                            <label for="locations">Poblaciones</label>
                            <select name="locations[]" multiple id="locations" class="required select2 form-control {{ $errors->has('locations') ? ' is-invalid' : '' }}">
                                <option value="">Todas</option>
                                @foreach ($poblaciones as $poblacion)
                                    <option value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('locations'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('locations') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row mt-3">
                        <div class="form-group col">
                            <label for="turns">Giros</label>
                            <select name="turns[]" multiple id="turns" class="required select2 form-control {{ $errors->has('turns') ? ' is-invalid' : '' }}">
                                <option value="">Todos</option>
                                @foreach ($turns as $turn)
                                    <option value="{{ $turn->IdGiro }}">( {{ $turn->IdGiro }} ){{ $turn->Nombre }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('turns'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('turns') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row mt-3">
                        <div class="form-group col" style="text-align: center">
                            <button type="submit" class="btn btn-primary mr-3" id="btn-applay-process" target="_BLANK" ><i class="fas fa-check-circle mr-2"></i> Generar Reporte</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $("#process").addClass('active');
        });
    </script>
@endsection