@extends('layouts.index')

@section('title', 'Reportes - Presupuesto de Licencias') 

@section('css')
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Reporte - Presupuesto de Licencias</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <form id="reportForm" action="{{ route('report.licensingBudgetReport.detail') }}" method="GET" target="_blank" class="">
                        <input type="hidden" name="type" value="taxpayer">

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="taxpayer-tab" data-toggle="tab" href="#taxpayer" role="tab" aria-controls="taxpayer" aria-selected="true">Contrubuyente</a>
                            </li>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="licenses-tab" data-toggle="tab" href="#licenses" role="tab" aria-controls="licenses" aria-selected="false">Licencias</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="taxpayer" role="tabpanel" aria-labelledby="taxpayer-tab">
                                <div class="form-group mt-3">
                                    <div class="form-group mt-1 mb-1"> 
                                        <label for="idtaxpayer">Contribuyente</label>
                                        <select id="idtaxpayer" name="idtaxpayer" class="js-data-taxpayers-ajax form-control {{ $errors->has('idtaxpayer') ? ' is-invalid' : '' }}"></select>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-success btn-taxpayer"><i class="fas fa-file-excel"></i> Generar reporte</button>
                            </div>
                            <div class="tab-pane fade" id="licenses" role="tabpanel" aria-labelledby="licenses-tab">
                                <br>
                                <p><b>Nota:</b> Por favor separar los numeros de licencias por medio de una coma <code>,</code></p>
                                <div class="form-group mt-3">
                                    <div class="form-group mt-1 mb-1">
                                        <label for="licences">Licencias</label>
                                        <input type="text" name="licenses" class="form-control" id="licenses" placeholder="">
                                        <small class="text-helper">Ej. <code>0000105841, 0000001580, 0000005812, ...</code></small>
                                    </div>
                                </div>
                                <div class="bg-warning text-dark p-2 mb-3"><b>IMPORTANTE</b>: Si alguna licencia no se encuentra, no se mostrará información alguna acerca de dicha licencia.</div>
                                <button type="button" class="btn btn-success btn-licenses"><i class="fas fa-file-excel"></i> Generar reporte</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $('.js-data-taxpayers-ajax').select2({
                minimumInputLength: 1,
                placeholder: "Contribuyente",
                allowClear: true,
                theme: "bootstrap",
                language: "es",
                ajax: {
                    url: "{{ route('ajax.taxpayers') }}",
                    type: 'post',
                    dataType: 'json',
                    delay: 400,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _token: "{{ csrf_token() }}"
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data.data.taxpayers
                        };
                    }
                }
            });

            $(document).on('click', '.btn-taxpayer', function() {
                $('#reportForm').find('[name="type"]').val('taxpayer');
                $('#reportForm').submit();
            }); 

            $(document).on('click', '.btn-licenses', function() {
                $('#reportForm').find('[name="type"]').val('licenses');
                $('#reportForm').submit();
            }); 
        });
    </script>
@endsection