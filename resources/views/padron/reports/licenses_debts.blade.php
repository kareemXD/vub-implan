@extends('layouts.index')

@section('title') Reportes - Licencias con adeudos @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Reporte - Licencias con Adeudos</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <form id="reportForm" action="{{ route('report.licenses.debts.details') }}" method="GET" target="_blank" class="row justify-content-center">
                <input type="hidden" name="type" value="view">
                <div class="col-6">
                    <div class="form-row">
                        <div class="form-group col">
                            <legend>Población</legend>
                            <select id="poblacion" name="poblacion" class="form-control select2" id="" >
                                @foreach ($poblaciones as $poblacion)
                                    <option value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row mt-3">
                        <div class="form-group col" style="text-align: center">
                            <button type="button" class="btn btn-primary" id="btn-generate"><i class="fas fa-check-circle mr-2"></i> Generar Reporte</button> <br>
                        </div>
                    </div>
                </div>
            </form>
            <br>
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <legend style="d-flex justify-content-between">Total de deuda: ${{ number_format($total, 2) }}</legend>
                    <table class="table dataTable table-sm table-bordered table-striped table-hover" cellpadding="4">
                        <thead>
                            <tr>
                                <th>Población</th>
                                <th width="12%">Licencias</th>
                                <th width="15%">Adeudo total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($poblaciones as $poblacion)
                                <tr>
                                    <td>{{ $poblacion->NombrePoblacion }}</td>
                                    <td>{{ number_format($poblacion->padronLicencesWithDebts(), 0) }}</td>
                                    <td>${{ number_format($poblacion->padronDebts(), 2) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'Bfrtip',
                // bFilter: false,
                "lengthMenu": [[15, 30, 50, -1], [15, 30, 50, "All"]],
                buttons: [
                    // 'excelHtml5',
                    // 'pdfHtml5'
                ],
                "order": [[ 2, "desc" ]]
            });

            $("#reports").addClass('active');

            $(document).on('click', '#btn-generate', function() {
                var form = $('#reportForm');

                if (validateForm())
                {
                    form.find('[name="type"]').val('view');
                    form.submit();
                }
            });

            function validateForm()
            {
                console.log('Entro');
                var flag = true;
                $("#reportForm").find(".required").each(function() {
                    if(!$(this).val()){
                        flag = false;
                        $(this).addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                        }
                    }
                });

                return flag;
            }
            
        });
    </script>
@endsection