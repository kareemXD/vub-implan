@extends('layouts.index')

@section('title') Alertas @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Alertas</h3>
        </div>
    </div>
    <div class="panel-body @can('write_turns') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive custam-table">
                <table class="table table-sm table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="5%" scope="col">#</th>
                            <th scope="col">Alerta</th>
                            <th width="10%" scope="col"># Licencia</th>
                            <th width="10%" scope="col">Desde </th>
                            <th width="10%" scope="col">Para</th>
                            <th width="10%" scope="col">Fecha Alta</th>
                            <th width="10%" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                      {{--aqui se supone que deben estar las alertas--}} 
                      @foreach($alerts as $index => $alerta)
                      <tr>
                        <td>{{ $alerta->IdAlerta }}</td>
                        <td>{{ $alerta->Alerta }}</td>
                        <td>{{ $alerta->NumeroLicencia }}</td>
                        <td>{{ $alerta->De }}</td>
                        <td>{{ $alerta->Para }}</td>
                        <td>{{ $alerta->FechaAlta }}</td>
                        <td align="center"><a target="_blank" href="{{ route('update.license', $alerta->NumeroLicencia) }}" title="Abrir Licencias" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></a></td>
                      </tr>
                      @endforeach
                    </tbody>
                </table>
                <div class="table-footer">
                    <div class="pagination">
                        <span>Mostrando {{ (($alerts->currentPage() * 15) - 14) }} a {{( $alerts->currentPage() * 15) }} de {{ $alerts->total() }} Filas</span>
                        {{ $alerts->links() }} 
                    </div>
                    <div class="button-exports">
                        <a href="{{ route('print.alerts') }}" target="_BLANK" class="btn btn-success edit-taxpayer"><i class="fas fa-pdf"></i> Exportar a PDF</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $("#padron").addClass('active');
            $(document).on("click", ".btn-submit", function(){
                $( ".custom_input" ).each(function( index, element ) {
                    $("#form-data").append($(this));
                });
                $("#form-data")[0].submit();
            });
            
            $(document).on("keypress", ".custom_input", function(e){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $( ".custom_input" ).each(function( index, element ) {
                        $("#form-data").append($(this));
                    });
                    $("#form-data")[0].submit();
                }
            });

        });
    </script>
@endsection