@extends('layouts.index')

@section('title') {{ $process->Nombre }} @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>{{ $process->Nombre }} ( {{ $license->NumeroLicencia }} )</h3>
    </div>
</div>
<div class="row">
    <div class="col">
    <a href="{{ route('update.license', $license->NumeroLicencia) }}" class="btn btn-outline-light text-white mt-2"><i class="fa fa-arrow-left"></i>Regresar a la Licencia</a>
    </div>
</div>
@php
    $turndetails = $license->getTurnsDetails();
@endphp
<div class="panel-body @can('write_requests') with-buttons @endcan">
    <div class="margin-fix panel-row-fluid">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-link active" id="period-tab" data-toggle="tab" href="#turn" role="tab" aria-controls="turn" aria-selected="false">Giro</a>
            </li>
        </ul>
        <form id="form-data" action="{{ route('store.update.license') }}" method="POST" class="tab-content small-form">
            @csrf
            <input type="hidden" name="id" value="{{ $license->NumeroLicencia }}">
                
            <input type="hidden" name="operation" value="update">
            <div class="tab-pane fade tab-pane fade active show" id="turn" role="tabpanel" aria-labelledby="turn-tab">
                <div class="form-group row mt-2 margin-10">
                    <label for="name_b" class="col-sm-2 col-form-label">Nombre del Negocio</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control {{ $errors->has('name_b') ? ' is-invalid' : '' }}" id="name_b" name="name_b" value="{{ $license->NombreNegocio }}">
                        @if ($errors->has('name_b'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name_b') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="linderos" class="col-sm-2 col-form-label">Linderos</label>
                    <div class="col-sm-10">
                        <textarea class="form-control {{ $errors->has('linderos') ? ' is-invalid' : '' }}" id="linderos"  name="linderos" rows="2">{{ $license->Linderos }}</textarea>
                        @if ($errors->has('linderos'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('linderos') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="observations" class="col-sm-2 col-form-label">Observaciones</label>
                    <div class="col-sm-10">
                        <input  type="text" class="form-control {{ $errors->has('observations') ? ' is-invalid' : '' }}" id="observations" name="observations" value="{{ $license->Observaciones }}">
                        @if ($errors->has('observations'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('observations') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label data-legends="{{ json_encode($license->legends()->pluck("Leyendas.IdLeyenda")->toArray()) }}" for="legends" class="col-sm-2 col-form-label">Leyendas</label>
                    <div class="col-sm-10">
                        <select name="legends[]" id="legends" class="required select2 form-control {{ $errors->has('legends') ? ' is-invalid' : '' }}" multiple>
                            <option value="">Seleccione las Leyendas</option>
                            @foreach ($legends as $legend)
                                <option value="{{ $legend->IdLeyenda }}">{{ $legend->NombreLeyenda }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('legends'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('legends') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="surface" class="col-sm-2 col-form-label">Superficie</label>
                    <div class="col-sm-4">
                        <input  type="number" step="0.00" class="form-control {{ $errors->has('surface') ? ' is-invalid' : '' }}" id="surface" name="surface" value="{{ $license->Superficie }}">
                        @if ($errors->has('surface'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('surface') }}</strong>
                            </span>
                        @endif
                    </div>
                    <label for="employees" class="col-sm-2 col-form-label">Empleados</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control {{ $errors->has('employees') ? ' is-invalid' : '' }}" id="employees" name="employees" value="{{ $license->EmpleosCreados }}">
                        @if ($errors->has('employees'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('employees') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            <table data-tramite="{{ $process->TipoTramite }}" class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                    <thead>
                        @if ($process->TipoTramite == "G")
                            <tr>
                                <th><input type="text" id="cuenta_serach" placeholder="ID" class="form-control custom_input" /></th>
                                <th><input type="text" disabled id="description_search" placeholder="Descripción" class="form-control custom_input" /></th>
                                <th><input type="text" id="observations_search" placeholder="Observaciones" class="form-control custom_input" /></th>
                                <th><input type="date" id="date_search" placeholder="Fecha" class="form-control custom_input" /></th>
                                <th><input type="text"disabled id="cost_search" placeholder="Cuota" class="form-control custom_input" /></th>
                                <th><input type="number" id="quantity_search" placeholder="Cantidad" class="form-control custom_input" /></th>
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-add-turn"><i class="fas fa-plus"></i></button></th>
                            </tr>
                        @endif
                        <tr>
                            <th width="10%">IdGiro</th>
                            <th>Descripción</th>
                            <th>Observaciones</th>
                            <th width="10%">Fecha</th>
                            <th width="10%">Cuota</th>
                            <th width="10%">Cantidad</th>
                            <th width="10%">Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="turns-content">
                        @foreach ($turndetails["data"] as $turn)
                            <tr>
                                <input type="hidden" class="type" name="type[]" value="{{ $turn['type'] }}"/>
                                <td><input type="hidden" class="cuenta_serach" name="cuenta_serach[]" value="{{ $turn['id'] }}"/>{{ $turn['id'] }}</td>
                                <td><input type="hidden" class="description_search" name="description_search[]" value="{{ $turn['nombre'] }}"/> {{ $turn['nombre'] }}</td>
                                <td><input type="hidden" class="observations_search" name="observations_search[]" value="{{ $turn['observation'] }}"/> {{ $turn['observation'] }}</td>
                                <td><input type="hidden" class="date_search" name="date_search[]" value="{{ $turn['date'] }}"/> {{ $turn['date'] }}</td>
                                <td><input type="hidden" class="cost_search" name="cost_search[]" value="{{ $turn['cost'] }}"/>$ {{ $turn['cost'] }}</td>
                                <td><input type="hidden" class="quantity_search" name="quantity_search[]" value="{{ $turn['quantity'] }}"/> {{ $turn['quantity'] }}</td>
                                <td><button type="button" class="btn btn-danger btn-action-table btn-delete-turn"><i class="fas fa-trash"></i></button></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="row margin-10 mt-2">
                    <div class="col-8">
                        <span>El costo de los anexos depende del porcentaje de pago para altas y refrendos registrados en preferencias y que en este momento es de <b>{{ $trimester["percentage"] }} %</b> del costo anual.</span>
                    </div>
                    <div class="col-4">
                        <div class="form-group row margin-10">
                            <label for="costo_f" class="col-sm-4 col-form-label">Costo Forma</label>
                            <div class="col-sm-8">
                                <input disabled type="text" class="form-control {{ $errors->has('costo_f') ? ' is-invalid' : '' }}" id="costo_f" value="$ 0.00">
                            </div>
                        </div>
                        <div class="form-group row mt-2 margin-10">
                            <label for="costo_t" class="col-sm-4 col-form-label">Costo Total</label>
                            <div class="col-sm-8">
                                <input disabled type="text" class="form-control {{ $errors->has('costo_t') ? ' is-invalid' : '' }}" id="costo_t" value="$ 0.00">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@can('write_requests')
    <div class="row justify-content-end panel-buttoms" id="buttons">
        <button type="submit" class="btn btn-primary mr-3" id="btn-save-changes"><i class="fas fa-save mr-2"></i> Guardar Cambios</button>
    </div>
@endcan
@endsection

@section('modals')
    {{-- @include('partials.modals.padron.modal_update_document') --}}
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('system.google.maps.apiKey') }}&libraries=places&v=weekly"
        defer
    ></script>
    <script>
        window.addEventListener('load', function () {
            OnChangeCity("{{ $license->IdColonia }}");
            OnChangeColony("{{ $license->IdCalle }}", "{{ $license->IdCruce1 }}", "{{ $license->IdCruce2 }}");
            @if(session()->has('document_updated'))
                $('#myTab a[href="#documents"]').tab('show');
            @endif
            setTimeout(function(){ 
                if($(".btn-search-taxpayer").length > 0){
                    $(".btn-search-taxpayer")[0].click();

                }
                var cost = 0;
                $(".cost_search").each(function() {
                    cost += parseFloat($(this).val()) * parseFloat($(this).parent().next().find(".quantity_search").val());
                });
                cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                $("#costo_t").val("$ " + cost.toFixed(2));
            }, 100);
        });
        $(function(){
            $("#padron").addClass('active');
            
            var created = false;



            
            $(document).on("click", "#btn-save-changes", function(){
                $.confirm({
                    title: 'Confirmación!',
                    content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción realizara cambios en la licencia.</small>',
                    type: "blue",
                    buttons: {
                        Aceptar: function () {
                            
                            $("input[name=operation]").val($("[data-tramite]").data("tramite"));
                            $('#form-data')[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });
            
            $(document).on("click", "#btn-applay-request", function(){
                $("input[name=operation]").val('apply_changes');
                $("#form-data")[0].submit();
            });

            $(document).on("click", "#btn-save-location", function(){
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción?',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            $("input[name=operation]").val('update_location');
                            $("#form-data")[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });
            
            $(document).on("click", ".btn-update-document", function(){
                var _this = $(this);
                $.ajax({
                    url: "{{ route('get.document.by.request') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        requisito: _this.data("requisito"),
                        solicitud: $("input[name=id]").val()
                    },
                }).done(function(result){
                    console.log(result);
                    var modal = $("#modal-update-document");
                    modal.find("#requisito_id").val(result.Requisito);
                    modal.find("#nombre_r").val(result.Nombre);
                    modal.find("#comments").val(result.pivot_Comentario);
                    if(result.pivot_Archivo)
                    {
                        modal.find("#documents-content").prepend('<ul><li class="download-file" data-requisito="'+result.Requisito+'" data-solicitud="'+$("input[name=id]").val()+'">\
                            <img src="{{ asset("assets/plugins/preview/preview_pdf.png") }}" >\
                        </li></ul>');
                        $('.old_document').simpleFilePreview();
                    }
                    modal.modal("show");
                });
            });
            
            $(document).on('click', '.btn-search-taxpayer', function(){
                var id = $("#idtaxpayer").val(),
                    token = "{{ csrf_token() }}",
                    legends = $("[data-legends]").data("legends");
                    if($("[data-tramite]").data("tramite") == "A"){
                        $("#legends").prop("disabled", false);
                    }else{
                        $("#legends").prop("disabled", true);
                    }

                    $("#legends").select2("val", legends);

                    $('#update_persona').bootstrapToggle('off')
                $.ajax({
                    url: "{{ route('get.taxpayer.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $('#taxpayer').val(result.IdContribuyente);
                    $('#cuenta').val(result.IdContribuyente);
                    $('#rfc').val(result.RFC);
                    $('#nombre').val(result.Nombre);
                    $('#apellido_pat').val(result.ApellidoPaterno);
                    $('#apellido_mat').val(result.ApellidoMaterno);
                    if(result.Sexo)
                    {
                        $('#sexo').val(result.Sexo);
                    }else
                    {
                        $('#sexo').val("I");
                    }
                    if(result.PersonaMoral == 1)
                    {
                        $('#persona').bootstrapToggle('on')
                    }
                    $('#email').val(result.Email);
                    $('#exterior').val(result.Exterior);
                    $('#interior').val(result.Interior);
                    $('#cp').val(result.CP);
                    $('#localidad').val(result.Ciudad);
                    $('#colonia').val(result.Colonia);
                    $('#domicilio').val(result.Domicilio);
                    $('#calle1').val(result.Cruce1);
                    $('#calle2').val(result.Cruce2);
                    $('#telefono_casa').val(result.TelefonoCasa);
                    $('#telefono_trab').val(result.TelefonoTrabajo);
                });
            });
            
            $(document).on('focusout', '#cuenta_serach', function(){
                var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.turn.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $('#description_search').val(result.Nombre);
                    $('#cost_search').val(result.CuotaFija);
                    $('#quantity_search').val(1);
                });
            });
            /*
            $(document).on("click", ".btn-add-turn", function(){
                var flag = true;
                $(this).parents("table").find(".custom_input").each(function() {
                    if(!$(this).val()){
                        flag = false;
                        $(this).addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                        }
                    }
                });
                if(flag){
                    var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                    $.ajax({
                        url: "{{ route('get.license.cost') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: $("#cuenta_serach").val(),
                            date: $("#date_search").val()
                        },
                    }).done(function(result){

                        $("#turns-content").append('\
                            <tr>\
                                <td><input type="hidden" class="cuenta_serach" name="cuenta_serach[]" value="'+$("#cuenta_serach").val()+'"/>'+$("#cuenta_serach").val()+'</td>\
                                <td><input type="hidden" class="description_search" name="description_search[]" value="'+$("#description_search").val()+'"/>'+$("#description_search").val()+'</td>\
                                <td><input type="hidden" class="observations_search" name="observations_search[]" value="'+$("#observations_search").val()+'"/>'+$("#observations_search").val()+'</td>\
                                <td><input type="hidden" class="date_search" name="date_search[]" value="'+$("#date_search").val()+'"/>'+$("#date_search").val()+'</td>\
                                <td><input type="hidden" class="cost_search" name="cost_search[]" value="'+parseFloat(result)+'"/>$ '+parseFloat(result).toFixed(2)+'</td>\
                                <td><input type="hidden" class="quantity_search" name="quantity_search[]" value="'+$("#quantity_search").val()+'"/>'+$("#quantity_search").val()+'</td>\
                                <td><button type="button" class="btn btn-danger btn-action-table btn-delete-turn"><i class="fas fa-trash"></i></button></td>\
                            </tr>');
                        $("#cuenta_serach").val("");
                        $("#description_search").val("");
                        $("#observations_search").val("");
                        $("#date_search").val("");
                        $("#cost_search").val("");
                        $("#quantity_search").val("");
                        var cost = 0;
                        $(".cost_search").each(function() {
                            cost += parseFloat($(this).val()) * parseFloat($(this).parent().next().find(".quantity_search").val());
                        });
                        cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                        $("#costo_t").val("$ " + cost.toFixed(2));
                    });
                }
            });  */
            $(document).on("click", ".btn-add-turn", function(){
                var flag = true;
                $(this).parents("table").find(".custom_input").each(function() {
                    if(!$(this).val()){
                        flag = false;
                        $(this).addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                        }
                    }
                });
                if(flag){
                    var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                    $.ajax({
                        url: "{{ route('get.license.cost.details') }}",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            id: $("#cuenta_serach").val(),
                            date: $("#date_search").val(),
                            location: 0,
                        },
                    }).done(function(result){
                        if(result.alcohol){
                            $("#alcohol-div").fadeIn();
                            $("#alcohol-div").find("#alcohol").addClass("required");
                        }
                        result.data.forEach(function(element){ 
                            $("#turns-content").append('\
                                <tr>\
                                    <input type="hidden" class="type" name="type[]" value="'+element.type+'"/>\
                                    <td><input type="hidden" class="cuenta_serach" name="cuenta_serach[]" value="'+$("#cuenta_serach").val()+'"/>'+$("#cuenta_serach").val()+'</td>\
                                    <td><input type="hidden" class="description_search" name="description_search[]" value="'+$("#description_search").val()+'"/>'+$("#description_search").val()+'</td>\
                                    <td><input type="hidden" class="observations_search" name="observations_search[]" value="'+element.observation+'"/>'+element.observation+'</td>\
                                    <td><input type="hidden" class="date_search" name="date_search[]" value="'+element.date+'"/>'+element.date+'</td>\
                                    <td><input type="hidden" class="cost_search" name="cost_search[]" value="'+parseFloat(element.cost)+'"/>$ '+parseFloat(element.cost).toFixed(2)+'</td>\
                                    <td><input type="hidden" class="quantity_search" name="quantity_search[]" value="'+$("#quantity_search").val()+'"/>'+$("#quantity_search").val()+'</td>\
                                    <td><button type="button" class="btn btn-danger btn-action-table btn-delete-turn"><i class="fas fa-trash"></i></button></td>\
                                </tr>');
                        });
                        $("#cuenta_serach").val("");
                        $("#description_search").val("");
                        $("#observations_search").val("");
                        $("#date_search").val("");
                        $("#cost_search").val("");
                        $("#quantity_search").val("");
                        var cost = 0;

                        $(".cost_search").each(function() {
                            cost += parseFloat($(this).val()) * parseFloat($(this).parent().parent().find(".quantity_search").val());
                            
                        });
                        // cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                        $("#costo_t").val("$ " + cost.toFixed(2));
                    });
                }
            });

            $(document).on("click", ".btn-delete-turn", function(){
                var _this = $(this);
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción?',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            _this.parents("tr").fadeOut("slow", function(){
                                _this.parents("tr").remove();
                                var cost = 0;
                                $(".cost_search").each(function() {
                                    cost += parseFloat($(this).val()) * parseFloat($(this).parent().next().find(".quantity_search").val());
                                });
                                cost = cost * (parseFloat('{{ $trimester["percentage"] }}') / 100);
                                $("#costo_t").val("$ " + cost.toFixed(2));
                            });
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });

            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var target = e.target.hash;
                switch (e.relatedTarget.hash) {
                    case "#data":
                        if(!$("#idtaxpayer").val())
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione un contribuyente para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        break;
                
                    case "#location":
                        if(!$("#norte").val() || !$("#este").val())
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione la ubicación para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        break;
                    case "#documents":
                        $("#btn-save-changes").fadeOut("fast");
                        $("#btn-applay-request").fadeOut("fast");
                        $("#btn-next").fadeIn("fast");
                        break;
                }

                switch (e.target.hash) {
                    case "#location":
                        $("#btn-previous").fadeOut("fast")
                        $("#btn-next").data("target", "#data")
                        break;
                    case "#data":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#location")
                        });
                        $("#btn-next").data("target", "#turn")
                        break;
                    case "#turn":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#data")
                        });
                        $("#btn-next").data("target", "#documents")
                        break;
                    case "#documents":
                        $("#btn-previous").fadeIn("fast", function(){
                            $("#btn-previous").data("target", "#turn")
                        });
                        $("#btn-next").fadeOut("fast", function(){
                            $("#btn-save-changes").fadeIn("fast");
                            $("#btn-applay-request").fadeIn("fast");
                        });
                        break;
                }
            });

            $(document).on("click", ".btn-next", function(){
                $('#myTab a[href="'+$(this).data("target")+'"]').tab('show');
            });

            $(document).on("change", "#city", function(){
                OnChangeCity();
            });
            $(document).on("change", "#colony", function(){
                OnChangeColony();
            });
            
            /*tramites */
            //ampliacion de giro
            if($("[data-tramite]").data("tramite") == "A") {
                $('a#period-tab').on("show.bs.tab", function(event){
                    $("#btn-save-changes").fadeIn(500);
                });
                $('a#period-tab').on("hide.bs.tab", function(event){
                    $("#btn-save-changes").fadeOut(500);
                });

            }
             //AGREGAR CALLE
             $(document).on("click", ".btn-other", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);
                parent.find("span.select2").fadeOut("slow", function(){
                    parent.append('<input type="text" class="form-control inputInserted '+_this.data("class")+'" name="new'+_this.data("name")+'" placeholder="Escriba el nombre de la Calle"> ').show('slow');
                    _this.html('<i class="fas fa-minus-circle"></i>');
                    _this.removeClass("btn-other");
                    _this.addClass("remove-calle");
                    _this.attr("data-original-title", "Seleccionar Calle").tooltip('show');
                });
            });

            $(document).on("click", ".remove-calle", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);
                parent.find(".inputInserted").fadeOut("slow", function(){
                    $(this).remove();
                    parent.find("span.select2").fadeIn("slow")
                    _this.html('<i class="fas fa-plus-circle"></i>')
                    _this.removeClass("remove-calle");
                    _this.addClass("btn-other");
                    //_this.attr("data-original-title", "Añadir una Calle").tooltip('show');
                });
            });
            

        });

        function OnChangeCity(colony = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.colonies.by.city') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#colony").empty();
                $("#colony").append("<option value='0'>Seleccione una Colonia</option>");
                result.forEach(element => {
                    $("#colony").append($('<option>', {
                        value: element.IdColonia,
                        text: element.NombreColonia
                    }));
                });
                if(colony)
                {
                    $("#colony").val(colony);
                }
                
            });

            $(document).on("click", ".download-file", function(){
                var requisito = $(this).data("requisito");
                var solicitud = $(this).data("solicitud");
                window.open('/padron/mostrar/documento/'+requisito+'/'+solicitud, '_blank');
            });
        }

        function OnChangeColony(calle = null, calle1 = null, calle2 = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.calles.by.colony') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#domicilio2").empty();
                $("#domicilio2").append("<option value='0'>Seleccione una Calle</option>");

                $("#calle1_u").empty();
                $("#calle1_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                $("#calle2_u").empty();
                $("#calle2_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                result.forEach(element => {
                    $("#domicilio2").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle1_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle2_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));
                });
                if(calle || calle1 || calle2)
                {
                    if($("#domicilio2 option[value='"+calle+"']").length > 0){
                        $("#domicilio2").val(calle);
                    }else{
                        $.get("{{ route('api.get-street-by-id') }}/"+calle, function(response){
                            $("#domicilio2").append("<option selected value='"+calle+"'>"+response.NombreCalle+"</option>");
                        });
                    }

                    if($("#calle1_u option[value='"+calle1+"']").length > 0){
                        $("#calle1_u").val(calle1);
                    }else{
                        $.get("{{ route('api.get-street-by-id') }}/"+calle1, function(response){
                            $("#calle1_u").append("<option selected value='"+calle1+"'>"+response.NombreCalle+"</option>");
                        });
                    }

                    if($("#calle2_u option[value='"+calle2+"']").length > 0){
                        $("#calle2_u").val(calle2);
                    }else{
                        $.get("{{ route('api.get-street-by-id') }}/"+calle2, function(response){
                            $("#calle2_u").append("<option selected value='"+calle2+"'>"+response.NombreCalle+"</option>");
                        });
                    }
                }
            });
        }
    </script>
@endsection