@extends('layouts.index')

@section('title') Localidades @endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.css') }}">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Localidades</h3>
        </div>
    </div>
    @can('write_location')
       
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('colony.list') }}" class="btn btn-primary mr-3"><i class="fas fa-eye mr-2"></i> Lista de colonias</a>
            <a href="{{ route("street.list") }}" class="btn btn-primary mr-3"><i class="fas fa-eye mr-2"></i> Lista de Calles</a>
            <button href="#modal_create_location" data-target="#modal_create_location"  data-toggle="modal" class="btn btn-primary mr-3"><i class="fas fa-plus mr-2"></i> Registrar Localidad</button>
        </div>
    @endcan
    <div class="panel-body @can('write_location') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive custam-table">
                <div id="table-filter" style="display:none; text-align:left;">
                    <div class="row margin-10">
                        <div class="col">
                            <button type="button" class="btn btn-success"><i class="fas fa-pdf mr-2"></i> Exportar a PDF</button>
                        </div>
                    </div>
                </div>
                <table id="table_locations" class="table table-sm table-hover dataTable">
                    <thead>
                        <tr>
                            <form id="form-data" action="{{ route('locations.list') }}" method="POST">
                                @csrf
                                <th scope="col"><input autocomplete="off"  type="text" name="id_poblacion" value="{{ (isset(session('inputs')['id_poblacion'])) ? session('inputs')['id_poblacion'] : "" }}" placeholder="ID" class="form-control custom_input" /></th>
                                <th scope="col"><input autocomplete="off"  type="text" name="nombre_poblacion" value="{{ (isset(session('inputs')['nombre_poblacion'])) ? session('inputs')['nombre_poblacion'] : "" }}" placeholder="Nombre" class="form-control custom_input" /></th>
                            <th><input autocomplete="off"  type="text" name="nombre_corto" value="{{ (isset(session('inputs')['nombre_corto'])) ? session('inputs')['nombre_corto'] : "" }}" placeholder="Nombre Corto" class="form-control custom_input" /></th>
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-submit"><i class="fas fa-search"></i></button></th>
                            </form>
                        </tr>
                        <tr>
                            <th width="5%" scope="col">ID</th>
                            <th scope="col">Nombre Población</th>
                            <th width="col" scope="col">Nombre Corto</th>
                            <th width="15%" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($locations as $location)
                            <tr>
                                <td>{{ $location->IdPoblacion }}</td>
                                <td>{{ $location->NombrePoblacion }}</td>
                                <td>{{ $location->NombreCorto }}</td>
                                <td align="center">
                                    <button data-target="#modal_update_location" data-toggle="modal" data-route="{{ route('location.update', $location->IdPoblacion) }}" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></button>
                                    <a target="_blank" href="{{  route('colony.list', $location->IdPoblacion) }}"  class="btn btn-primary btn-action-table" ><i class="fas fa-eye"></i>Colonias</a>
                                    <a target="_blank" href="{{  route('street.list', $location->IdPoblacion) }}"  class="btn btn-primary btn-action-table" ><i class="fas fa-eye"></i>Calles</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="table-footer">
                    <div class="pagination">
                        <span>Mostrando {{ (($locations->currentPage() * 15) - 14) }} a {{( $locations->currentPage() * 15) }} de {{ $locations->total() }} Filas</span>
                        {{ $locations->appends(Request::only('IdPoblacion'))->links() }} 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
@include('partials.modals.padron.modal_update_location')
@include('partials.modals.padron.modal_create_location')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(function(){
            $("#padron").addClass('active');
            $(document).on("click", ".btn-submit", function(){
                $( ".custom_input" ).each(function( index, element ) {
                    $("#form-data").append($(this));
                });
                $("#form-data")[0].submit();
            });
            
            $(document).on("keypress", ".custom_input", function(e){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $( ".custom_input" ).each(function( index, element ) {
                        $("#form-data").append($(this));
                    });
                    $("#form-data")[0].submit();
                }
            });
/*****************************************actualizar localidad********************************/
            //modal para modificar poblacion
            $("#modal_update_location").on("show.bs.modal", function(event){
                $button = $(event.relatedTarget);
                $route = $button.data("route");
                $this = $(this);
                $("#form-update-location").attr("action", $route);
                $.ajax({
                    type: "get",
                    url: $route,
                    success: function(response){
                        $("#actualizar_nombre_poblacion").val(response.NombrePoblacion);
                        $("#actualizar_nombre_corto").val(response.NombreCorto);
                    }, error(error){
                        $("#modal_update_location").modal("hide");
                        swal({title:"Ups!", text:"Error al cargar localidad", type: "error"})
                    }
                });
            });
            //fin modal
            //evento cuando se cierra
            $("#modal_update_location").on("hide.bs.modal", function(e){
                $("#actualizar_nombre_poblacion").val("");
                $("#actualizar_nombre_corto").val("");
                $("#form-update-location").attr("action", "#");
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
            });
            // fin de cierre
            $("#form-update-location").on("submit", function(event){
                event.preventDefault();
                $url = $(this).attr("action");
                $method = $(this).attr("method");
                $data = $(this).serializeArray();
                if(validateUpdateLocation()){
                    $.ajax({
                        url: $url,
                        type: $method,
                        data: $data,
                        success: function(response){
                            $("#modal_update_location").modal("hide");

                            if(response.alert == "success"){
                                data = response.data;
                                swal({title:"Correcto", text:"Se actualizó correctamente la localidad", type: "success"});
                                $(document).find("[data-route='"+$url+"']").parents('tr').html(`<td>`+data.IdPoblacion+`</td>
                                <td>`+data.NombrePoblacion+`</td>
                                <td>`+data.NombreCorto+`</td>
                                <td align="center">
                                    <button data-target="#modal_update_location" data-toggle="modal" data-route="`+$url+`" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></button>
                                    <a target="_blank" href="`+response.url_colony+`"  class="btn btn-primary btn-action-table" ><i class="fas fa-eye"></i>Colonias</a>
                                    <a target="_blank" href="`+response.url_street+`"  class="btn btn-primary btn-action-table" ><i class="fas fa-eye"></i>Calles</a>
                                    </td>`);
                            }else{
                                swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                            }
                        },error: function(error){
                            $("#modal_update_location").modal("hide");
                            swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                        }
                    });
                }
            });

            //validar actualizacion de localidades
            function validateUpdateLocation(){
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
                let errors = 0;

                if($("#actualizar_nombre_poblacion").val() == "" || $("#actualizar_nombre_poblacion").val().length < 2 ){
                    errors++;
                    $("#actualizar_nombre_poblacion").addClass("is-invalid");
                    $("#actualizar_nombre_poblacion").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre de la poblacion debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#actualizar_nombre_corto").val() == "" || $("#actualizar_nombre_corto").val().length < 2 ){
                    errors ++;
                    $("#actualizar_nombre_corto").addClass("is-invalid");
                    $("#actualizar_nombre_corto").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre de la corto debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if(errors === 0){
                    return true;
                }

                return false;
            }
        /************************************************************fin actualizar localidad ***********/
        /******************************************agregar nueva localidad**********************/
            //evento cuando se cierra
            $("#modal_create_location").on("hide.bs.modal", function(e){
                $("#crear_nombre_poblacion").val("");
                $("#crear_nombre_corto").val("");
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
            });
            // fin de cierre
            $("#form-create-location").on("submit", function(event){
                event.preventDefault();
                $url = $(this).attr("action");
                $method = $(this).attr("method");
                $data = $(this).serializeArray();
                if(validateStoreLocation()){
                    $.ajax({
                        url: $url,
                        type: $method,
                        data: $data,
                        success: function(response){
                            $("#modal_create_location").modal("hide");

                            if(response.alert == "success"){
                                data = response.data;
                                swal({title:"Correcto", text:"Se Registró correctamente la localidad", type: "success"});
                                $("#table_locations").find('tbody').prepend(`<tr><td>`+data.IdPoblacion+`</td>
                                <td>`+data.NombrePoblacion+`</td>
                                <td>`+data.NombreCorto+`</td>
                                <td align="center">
                                    <button data-target="#modal_update_location" data-toggle="modal" data-route="`+response.url_update+`" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></button>
                                    <a target="_blank" href="`+response.url_colony+`"  class="btn btn-primary btn-action-table" ><i class="fas fa-eye"></i>Colonias</a>
                                    <a target="_blank" href="`+response.url_street+`"  class="btn btn-primary btn-action-table" ><i class="fas fa-eye"></i>Calles</a>
                                    </td></tr>`);
                            }else{
                                swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                            }
                        },error: function(error){
                            $("#modal_create_location").modal("hide");
                            swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                        }
                    });
                }
            });

            //validar actualizacion de localidades
            function validateStoreLocation(){
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
                let errors = 0;

                if($("#crear_nombre_poblacion").val() == "" || $("#crear_nombre_poblacion").val().length < 2 ){
                    errors++;
                    $("#crear_nombre_poblacion").addClass("is-invalid");
                    $("#crear_nombre_poblacion").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre de la poblacion debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#crear_nombre_corto").val() == "" || $("#crear_nombre_corto").val().length < 2 ){
                    errors ++;
                    $("#crear_nombre_corto").addClass("is-invalid");
                    $("#crear_nombre_corto").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre de la corto debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if(errors === 0){
                    return true;
                }

                return false;
            }
            /***********************fin agregar nueva localidad **************/
        });
    </script>
@endsection