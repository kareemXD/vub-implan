@extends('layouts.index')

@section('title') Licencias @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.css') }}">

    <style>
        .table, tr{
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Licencias</h3>
        </div>
    </div>
    @can('write_contribuyentes')
        <div class="row justify-content-end panel-buttoms">
            <button class="btn btn-secondary mr-2"  data-toggle="modal" data-target="#modalSearchLicense"><i class="fa fa-search"></i></button>
            <a href="{{ route('add.license') }}" class="btn btn-primary mr-3"><i class="fas fa-plus-circle mr-2"></i> Nueva Solicitud</a>
        </div>
    @endcan
    <div class="panel-body @can('write_contribuyentes') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-sm table-hover laravel-pagination">
                    <thead>
                        <tr>
                            <form id="form-data" action="{{ route('licenses') }}" method="POST" autocomplete="off">
                                @csrf
                                <th scope="col"><input type="text" name="cuenta" value="{{ (isset(session('inputs')['cuenta'])) ? session('inputs')['cuenta'] : "" }}" placeholder="Solicitud" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="id_contribuyente" value="{{ (isset(session('inputs')['id_contribuyente'])) ? session('inputs')['id_contribuyente'] : "" }}" placeholder="No. Contribuyente" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="contribuyente" value="{{ (isset(session('inputs')['contribuyente'])) ? session('inputs')['contribuyente'] : "" }}" placeholder="Nombre Contribuyente" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="nombre" value="{{ (isset(session('inputs')['nombre'])) ? session('inputs')['nombre'] : "" }}" placeholder="Nombre Negocio" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="fecha_alta" value="{{ (isset(session('inputs')['fecha_alta'])) ? session('inputs')['fecha_alta'] : "" }}" placeholder="Fecha de Alta" class="form-control custom_input custom-datetimepicker" /></th>
                                {{--nuevos encabezados--}}
                                <th scope="col"><input type="text" name="calle" value="{{ (isset(session('inputs')['calle'])) ? session('inputs')['calle'] : "" }}" placeholder="Calle" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="exterior" value="{{ (isset(session('inputs')['exterior'])) ? session('inputs')['exterior'] : "" }}" placeholder="Exterior" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="interior" value="{{ (isset(session('inputs')['interior'])) ? session('inputs')['interior'] : "" }}" placeholder="Interior" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="colonia" value="{{ (isset(session('inputs')['colonia'])) ? session('inputs')['colonia'] : "" }}" placeholder="Colonia" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="poblacion" value="{{ (isset(session('inputs')['poblacion'])) ? session('inputs')['poblacion'] : "" }}" placeholder="Población" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="fecha_tramite" value="{{ (isset(session('inputs')['fecha_tramite'])) ? session('inputs')['fecha_tramite'] : "" }}" placeholder="Fecha de Tramite" class="form-control custom_input custom-datetimepicker" /></th>
                                <th scope="col"><input type="text" name="giros_list" value="{{ (isset(session('inputs')['giros_list'])) ? session('inputs')['giros_list'] : "" }}" placeholder="Giro(s)" class="form-control custom_input" /></th>
                                {{--fin nuevos encabezados--}}
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-submit"><i class="fas fa-search"></i></button></th>
                            </form>
                        </tr>
                        <tr>
                            <th scope="col" style="width:8%;">#</th>
                            <th scope="col">No. Contribuyente</th>
                            <th scope="col">Nombre Contribuyente</th>
                            <th scope="col">Nombre Negocio</th>
                            <th scope="col">Fecha Alta</th>
                            {{--nuevos encabezados--}}
                            <th scope="col">Calle</th>
                            <th scope="col">Exterior</th>
                            <th scope="col">Interior</th>
                            <th scope="col">Colonia</th>
                            <th scope="col">Poblacion</th>
                            <th scope="col">Fecha Trámite</th>
                            <th scope="col">Giro(s)</th>
                            {{--fin nuevos encabezados--}}
                            <th scope="col" style="width:10%;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="small-font">
                        @foreach ($licenses as $license)
                            <tr data-route="{{ route('search.license.details', ["search-license-details" => $license->NumeroLicencia ]) }}" data-license="{{ $license->NumeroLicencia }}">
                                <td>{{ $license->NumeroLicencia }}</td>
                                <td>{{ $license->IdContribuyente }}</td>
                                <td>{{ $license->taxpayer->NombreCompleto }}</td>
                                <td>{{ $license->NombreNegocio }}</td>
                                <td>{{ $license->FechaAlta }}</td>
                                {{--nuevos encabezados--}}
                                <td scope="col">{{ is_null($license->street())? "": $license->street()->NombreCalle }}</td>
                                <td scope="col">{{ $license->Exterior }}</td>
                                <td scope="col">{{ $license->Interior }}</td>
                                <td scope="col">{{ is_null($license->colony())? "": $license->colony()->NombreColonia }}</td>
                                <td scope="col">{{ is_null($license->location())? "": $license->location()->NombrePoblacion }}</td>
                                <td scope="col">{{ ($license->FechaTramite < '2000-01-01')? "":  date("Y-m-d", strtotime($license->FechaTramite)) }}</td>
                                <td>{{ implode( ", ", $license->turns()->pluck("Nombre")->toArray() ) }}</td>
                                {{--fin nuevos encabezados--}}
                                <td align="center">
                                    <a href="{{ route('update.license', $license->NumeroLicencia) }}" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></a>
                                    @if ($license->alerts()->where("PermitirTramites", 0)->count() == 0 && auth()->user()->can('padron_licencias_print_license'))
                                    <a href="{{ route('print.license', [$license->NumeroLicencia]) }}" target="_BLANK" class="btn btn-primary btn-action-table"><i class="fa fa-print"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="table-footer">
                    <div class="pagination">
                        <span>Mostrando {{ (($licenses->currentPage() * 15) - 14) }} a {{( $licenses->currentPage() * 15) }} de {{ $licenses->total() }} Filas</span>
                        {{ $licenses->appends(Request::only('NumeroLicencia'))->links() }} 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="display:none" id="license_info_bottom">
        <nav class="navbar fixed-bottom navbar-light bg-light">
            <div style="height: 45vh;
            padding: 0;
            background-color: #0062a0;
            margin: 0;
            border-radius: 20px 20px 0 0;
            border: solid 2px #000;" class="container-fluid">
                <button onclick="closeData();" class="btn btn-primary" style="position: absolute;
                color: white;
                top: 23px;
                right: 32px;" id = "x">
                    X
                </button>
                <div style="background-color: #FFF;
                margin: 0 auto;
                padding: 20px;
                width: 100%;" class="row" id="license_data_content"></div>
            </div>
            
        </nav>
    </div>
    
    
@endsection

@section('modals')
    @include('partials.modals.padron.modal_search_license')
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>

    <script>
        $(function(){
            $("#padron").addClass('active');
            // $("[name='persona']").bootstrapSwitch();
            // $("[name='update_persona']").bootstrapSwitch();
            $('input.custom-datetimepicker ').daterangepicker({
                autoUpdateInput: false,
                opens: 'left',
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aceptar",
                    "cancelLabel": "Limpiar",
                    "fromLabel": "De",
                    "toLabel": "A",
                    "customRangeLabel": "Personalizado",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agusto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                }
            });
            $('input.custom-datetimepicker').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('input.custom-datetimepicker ').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            $(document).on("click", ".btn-submit", function(){
                $( ".custom_input" ).each(function( index, element ) {
                    $("#form-data").append($(this));
                });
                $("#form-data")[0].submit();
            });
            
            $(document).on("keypress", ".custom_input", function(e){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $( ".custom_input" ).each(function( index, element ) {
                        $("#form-data").append($(this));
                    });
                    $("#form-data")[0].submit();
                }
            });

            /**buscar licencia y obtener datos**/
            $(document).on("show.bs.modal", "#modalSearchLicense", function(event){
                $("#seaarch-license-content").html("");
            });
            $("#form-data-search-license").on("submit", function(event){
                event.preventDefault();
                if($("input[name='search-license-details']").val().length !== 10){
                    $("input[name='search-license-details']").addClass("is-invalid");
                    $("input[name='search-license-details']").parent().append(`
                        <div class="invalid-feedback is-invalid">
                            <strong>Revise este dato</strong>
                        </div>
                    `);

                    return;

                }else{
                    $(this).find(".is-invalid").removeClass("is-invalid");
                    $(this).find(".invalid-feedback").remove();
                }
                $button = $(this).find("button[type='submit']");
                $this = $(this);
                $url = $this.attr("action"), $type = $this.attr("method"), $data=$this.serializeArray();
                $html = $button.html();
                $button.html(`
                <div class="fa-2x">
                    <i class="fas fa-circle-notch fa-spin"></i>
                </div>
                `);
                $button.prop("disabled", true);
                $.ajax({
                    url: $url,
                    type: $type,
                    data: $data,
                    success(response){
                        
                        
                        console.log(response);
                        $button.html($html);
                        $button.prop("disabled", false);
                        if(!response){
                            swal({title:"Sin resultados", text:"Licencia no encontrada", type: "error"});
                        }else{
                            $("#license_info_bottom").slideDown(1000)
                            $("#license_data_content").html(response);
                            $("#modalSearchLicense").modal("hide");
                        }

                        
                    },
                    error(error){
                        swal({title:"Ups!", text:"Error al cargar licencia", type: "error"});
                        $button.html($html);
                        $button.prop("disabled", false);
                    }
                })

            });
            /**fin de buscar licencia y obtener datos**/
            /*obtener daros de la licencia con id*/
            $(document).on("dblclick", "[data-license]", function(event){
                console.log("hola");
                $this = $(this);
                $url = $this.data("route"), $type = "get";
                $('.wrapper-spinner-initial').fadeIn('fast');
                $.ajax({
                    url: $url,
                    type: $type,
                    success(response){
                        $("#license_data_content").html(response);
                        $("#license_info_bottom").slideDown(1000)
                        $('.wrapper-spinner-initial').fadeOut('fast');
                        if(response == false){
                            swal({title:"Sin resultados", text:"Licencia no encontrada", type: "error"});
                        }
                    },
                    error(error){
                        $('.wrapper-spinner-initial').fadeOut('fast');
                        swal({title:"Ups!", text:"Error al cargar licencia", type: "error"});
                    }
                });
            });
        });

        function closeData(){
            $("#license_info_bottom").slideUp(200)
        }
    </script>
@endsection