@extends('layouts.index')

@section('title') Basura @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Gestion de Cobro de Basura</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <ul class="nav nav-tabs" id="tabs" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="turns-tab" data-toggle="tab" href="#turns" role="tab" aria-controls="turns" aria-selected="true">Giros</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="locations-tab" data-toggle="tab" href="#locations" role="tab" aria-controls="locations" aria-selected="false">Localidades</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="turns" role="tabpanel" aria-labelledby="turns-tab">
                    <table class="table table-sm table-hover">
                        <thead>
                            <tr>
                                <form id="form-data" action="{{ route('pyl.trash') }}" method="POST">
                                    @csrf
                                    <th scope="col"><input type="text" name="cuenta" value="{{ (isset(session('inputs')['cuenta'])) ? session('inputs')['cuenta'] : "" }}" placeholder="ID" class="form-control custom_input" /></th>
                                    <th scope="col"><input type="text" name="nombre" value="{{ (isset(session('inputs')['nombre'])) ? session('inputs')['nombre'] : "" }}" placeholder="Nombre" class="form-control custom_input" /></th>
                                    <th><input type="text" name="concepto" value="{{ (isset(session('inputs')['concepto'])) ? session('inputs')['concepto'] : "" }}" placeholder="Concepto" class="form-control custom_input" /></th>
                                    <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-submit"><i class="fas fa-search"></i></button></th>
                                </form>
                            </tr>
                            <tr>
                                <th width="5%" scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th width="10%" scope="col">Concepto</th>
                                <th width="10%" scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($turns as $turn)
                                <tr>
                                    <td>{{ $turn->IdGiro }}</td>
                                    <td>{{ $turn->Nombre }}</td>
                                    <td>{{ $turn->Concepto }}</td>
                                    <td align="center">
                                        <a href="{{ route('pyl.edit.trash', $turn->IdGiro) }}" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></a>
                                        {{-- <button type="button" class="btn btn-primary btn-action-table update-trash"
                                                                data-id="{{ $turn->IdGiro }}"
                                                                data-trash="{{ $turn->Basura }}"
                                                                data-condescarga="{{ $turn->ConceptoDescarga }}"
                                                                data-cuotadescarga="{{ $turn->CostoDescarga }}"
                                                                data-conrecoleccion="{{ $turn->ConceptoRecoleccion }}"
                                                                data-cuotarecoleccion="{{ $turn->CostoRecoleccion }}"><i class="fas fa-edit"></i></button> --}}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="table-footer">
                        <div class="pagination">
                            <span>Mostrando {{ (($turns->currentPage() * 15) - 14) }} a {{( $turns->currentPage() * 15) }} de {{ $turns->total() }} Filas</span>
                            {{ $turns->appends(Request::only('IdGiro'))->links() }} 
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="locations" role="tabpanel" aria-labelledby="locations-tab">
                    <table class="table table-sm table-hover dataTable">
                        <thead>
                            <tr>
                                <th width="5%" scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th width="10%" scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($poblaciones as $poblacion)
                                <tr>
                                    <td>{{ $poblacion->IdPoblacion }}</td>
                                    <td>{{ $poblacion->NombrePoblacion }}</td>
                                    <td align="center"><input type="checkbox" @if($poblacion->Basura == 1) checked @endif  class="poblacion" data-id="{{ $poblacion->IdPoblacion }}" /></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('partials.modals.padron.modal_update_trash')
@endsection

@section('js')
    <script>
        $(function(){
            $("#process").addClass('active');
            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                pageLength: 15,
                dom: 'frtip',
                buttons: [
                    'excelHtml5',
                    'pdfHtml5'
                ]
            });
            $(document).on("click", ".btn-submit", function(){
                $( ".custom_input" ).each(function( index, element ) {
                    $("#form-data").append($(this));
                });
                $("#form-data")[0].submit();
            });
            $(document).on("click", ".update-trash", function(){
                var modal = $("#modal-update-trash");
                var trash = $(this).data("trash");
                    id = $(this).data("id");
                    condescarga = $(this).data("condescarga");
                    cuotadescarga = $(this).data("cuotadescarga");
                    conrecoleccion = $(this).data("conrecoleccion");
                    cuotarecoleccion = $(this).data("cuotarecoleccion");
                modal.find("input[name=turn]").val(id);
                if(trash == 1){
                    modal.find("#trash").prop("checked", true);
                }else{
                    modal.find("#trash").prop("checked", false);
                }
                modal.find("#condescarga").val(condescarga);
                modal.find("#condescarga").trigger('change');
                modal.find("#cuotadescarga").val(cuotadescarga);
                modal.find("#conrecoleccion").val(conrecoleccion);
                modal.find("#conrecoleccion").trigger('change');
                modal.find("#cuotarecoleccion").val(cuotarecoleccion);
                modal.modal("show");
            });
            $(document).on("click", ".poblacion", function(){
                var _this = $(this);
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción? <br /> Esta acción realizara cambios en el sistema',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            var value = 0;
                            if(_this.is(':checked')){
                                value = 1;
                            }
                            var id = _this.data("id"),
                                token = "{{ csrf_token() }}";
                            $.ajax({
                                url: "{{ route('pyl.update.trash') }}",
                                type: "POST",
                                data: {
                                    _token: token,
                                    id: id,
                                    value:value,
                                    type:'location'
                                },
                            }).done(function(result){
                                console.log(result);
                            });
                        },
                        Cancelar: function () {
                            if(_this.is(':checked')){
                                _this.prop('checked', false); 
                            }else{
                                _this.prop('checked', true); 
                            }
                        },
                    }
                });
            });
        });
    </script>
@endsection