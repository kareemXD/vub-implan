@extends('layouts.index')

@section('title') Basura @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Gestion de Cobro de Basura</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <fieldset>
                <legend class="text-center">{{ $turn->Nombre }}</legend>
                <div class="col">
                    <form id="form-trash" action="{{ route('pyl.update.trash') }}" method="POST">
                        @csrf
                        <input type="hidden" name="type" value="turn">
                        <input type="hidden" name="turn" value="{{ $turn->IdGiro }}">
                        <div class="form-row">
                            <div class="form-group">
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" name="trash" id="trash" value="1" {{ $turn->Basura == 1? 'checked' : '' }}>
                                  <label class="form-check-label" for="trash">
                                    ¿Habilitar cuota de basura?
                                  </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-6">
                                <label for="condescarga">Concepto por Descarga</label>
                                <select required class="form-control" name="condescarga" id="condescarga" readonly>
                                    <option value="02057">(02057) DESCARGA DE  BASURA EN RELLENO SANITARIO  M3</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <label for="cuotadescarga">Cuota Por Descarga</label>
                                <input type="number" step="0.01" class="form-control required" id="cuotadescarga" name="cuotadescarga" value="{{ $turn->CostoDescarga }}" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-6">
                                <label for="conrecoleccion">Concepto por Recolección</label>
                                <select required class="form-control" name="conrecoleccion" id="conrecoleccion" readonly>
                                    <option value="02058">(02058) RECOLECCION Y TRASLADO RESIDUOS SOLIDOS M3</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <label for="cuotarecoleccion">Cuota por Recolección</label>
                                <input type="number" step="0.01" class="form-control required" id="cuotarecoleccion" name="cuotarecoleccion" value="{{ $turn->CostoRecoleccion }}" required>
                            </div>
                        </div>
                        <br>
                        <button type="button" class="btn btn-primary btn-send"><i class="fas fa-save mr-2"></i> Guardar</button>
                    </form>
                    <br>
                    <h3 class="text-center">
                        Historial de costos
                    </h3>
                    <div class="text-center">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#modal-trash-add-year"><i class="far fa-plus-square fa-2x"></i></button>
                    </div>
                    <br>
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-6">
                            <table class="table table-hover table-striped table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <td>Año</td>
                                        <td>Descarga</td>
                                        <td>Recolección</td>
                                        <td>Acción</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($turn->trashYears as $year)
                                        <tr>
                                            <td>{{ $year->year }}</td>
                                            <td>{{ $year->costo_descarga }}</td>
                                            <td>{{ $year->costo_recoleccion }}</td>
                                            <td><button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-trash-edit-year" data-year="{{ $year->year }}" data-descarga="{{ $year->costo_descarga }}" data-recoleccion="{{ $year->costo_recoleccion }}"><i class="fas fa-edit"></i></button></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
@endsection

@section('modals')
    @include('partials.modals.padron.modal_update_trash_add_year')
    @include('partials.modals.padron.modal_update_trash_edit_year')
@endsection

@section('js')
    <script>
        $(function(){
            var conceptoDescarga = "{{ $turn->ConceptoDescarga }}",
                conceptoRecolección = "{{ $turn->ConceptoRecoleccion }}";

            if (conceptoDescarga != '')
            {
                $('[name="condescarga"]').val(conceptoDescarga);
            }
            if (conceptoRecolección != '')
            {
                $('[name="conrecoleccion"]').val(conceptoRecolección);
            }

            $(document).on("click", ".btn-send", function(){
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción? <br /> Esta acción realizara cambios en el sistema',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            $('#form-trash')[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });

            $('#modal-trash-edit-year').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    year = button.data('year'),
                    descarga = button.data('descarga'),
                    recoleccion = button.data('recoleccion'),
                    modal = $(this);

                modal.find('[name="year"]').val(year);
                modal.find('[name="cuotadescarga"]').val(descarga);
                modal.find('[name="cuotarecoleccion"]').val(recoleccion);
            });
        });
    </script>
@endsection