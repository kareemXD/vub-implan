@extends('layouts.index')

@section('title') Giros @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Actualizacion de Giros</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <form id="form-data" action="{{ route('aplay.turns.process') }}" method="POST" class="row justify-content-center">
                @csrf
                <div class="col-6 ">
                    <div class="form-row mt-3">
                        <div class="form-group col">
                            <label for="period">Periodo</label>
                            <input required type="number" class="form-control {{ $errors->has('period') ? ' is-invalid' : '' }}" id="period" name="period">
                            @if ($errors->has('period'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('period') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row mt-3">
                        <div class="form-group col">
                            <label for="percentage">% de Incremento</label>
                            <input required type="number" class="form-control {{ $errors->has('percentage') ? ' is-invalid' : '' }}" id="percentage" name="percentage">
                            @if ($errors->has('percentage'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('percentage') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row mt-3">
                        <div class="form-group col">
                            <button type="button" class="btn btn-primary mr-3" id="btn-applay-process" ><i class="fas fa-check-circle mr-2"></i> Aplicar Proceso</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $("#process").addClass('active');
            $(document).on("click", "#btn-applay-process", function(){
                $.confirm({
                    title: 'Alerta!',
                    content: '¿Estas seguro(a) de realizar esta acción? <br /> Esta acción realizara cambios en el sistema',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            $('#form-data')[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });
        });
    </script>
@endsection