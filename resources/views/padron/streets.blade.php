@extends('layouts.index')

@section('title') Calles @endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.css') }}">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Calles</h3>
        </div>
    </div>
    @can('write_location')
        <div class="row justify-content-end panel-buttoms">
            <button href="#modal_create_street" data-target="#modal_create_street"  data-toggle="modal" class="btn btn-primary mr-3"><i class="fas fa-plus mr-2"></i> Registrar Calle</button>
        </div>
    @endcan
    <div class="panel-body @can('write_location') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive custam-table">
                <div id="table-filter" style="display:none; text-align:left;">
                    <div class="row margin-10">
                        <div class="col">
                            <button type="button" class="btn btn-success"><i class="fas fa-pdf mr-2"></i> Exportar a PDF</button>
                        </div>
                    </div>
                </div>
                <table id="table_streets" class="table table-sm table-hover dataTable">
                    <thead>
                        <tr>
                            <form id="form-data" action="" method="POST">
                                @csrf
                                <th scope="col"><input autocomplete="off"  type="text" name="id_calle" value="{{ (isset(session('inputs')['id_calle'])) ? session('inputs')['id_calle'] : "" }}" placeholder="ID" class="form-control custom_input" /></th>
                                <th scope="col"><input autocomplete="off"  type="text" name="nombre_calle" value="{{ (isset(session('inputs')['nombre_calle'])) ? session('inputs')['nombre_calle'] : "" }}" placeholder="Nombre" class="form-control custom_input" /></th>
                                <th><input autocomplete="off"  type="text" name="alias_calle" value="{{ (isset(session('inputs')['alias_calle'])) ? session('inputs')['alias_calle'] : "" }}" placeholder="Alias Calle" class="form-control custom_input" /></th>
                                <th><input autocomplete="off"  type="text" name="nombre_oficial" value="{{ (isset(session('inputs')['nombre_oficial'])) ? session('inputs')['nombre_oficial'] : "" }}" placeholder="Nombre Oficial" class="form-control custom_input" /></th>
                                <th><input disabled autocomplete="off"  type="text" name="" placeholder="" class="form-control custom_input" /></th>
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-submit"><i class="fas fa-search"></i></button></th>
                            </form>
                        </tr>
                        <tr>
                            <th width="5%" scope="col">ID</th>
                            <th scope="col">Nombre Calle</th>
                            <th width="col" scope="col">Alias Calle</th>
                            <th width="col" scope="col">Nombre Oficial</th>
                            <th width="col" scope="col">Poblacion</th>
                            <th width="10%" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($streets as $street)
                            <tr>
                                <td>{{ $street->IdCalle }}</td>
                                <td>{{ $street->NombreCalle }}</td>
                                <td>{{ $street->AliasCalle }}</td>
                                <td>{{ $street->NombreOficial }}</td>
                                <td>{{ $street->location->NombrePoblacion }}</td>
                                <td align="center">
                                    <button data-target="#modal_update_street" data-toggle="modal" data-route="{{ route('street.update', $street->IdCalle) }}" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="table-footer">
                    <div class="pagination">
                        <span>Mostrando {{ (($streets->currentPage() * 15) - 14) }} a {{( $streets->currentPage() * 15) }} de {{ $streets->total() }} Filas</span>
                        {{ $streets->appends(Request::only('IdPoblacion', 'IdColonia'))->links() }} 
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
@include('partials.modals.padron.modal_update_street')
@include('partials.modals.padron.modal_create_street')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.min.js') }}"></script>
    <script>
        $(function(){
            $("#padron").addClass('active');
            $(document).on("click", ".btn-submit", function(){
                $( ".custom_input" ).each(function( index, element ) {
                    $("#form-data").append($(this));
                });
                $("#form-data")[0].submit();
            });
            
            $(document).on("keypress", ".custom_input", function(e){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $( ".custom_input" ).each(function( index, element ) {
                        $("#form-data").append($(this));
                    });
                    $("#form-data")[0].submit();
                }
            });
/*****************************************actualizar colonia********************************/
            //modal para modificar poblacion
            $("#modal_update_street").on("show.bs.modal", function(event){
                $button = $(event.relatedTarget);
                $route = $button.data("route");
                $this = $(this);
                $("#form-update-street").attr("action", $route);
                $.ajax({
                    type: "get",
                    url: $route,
                    success: function(response){
                        $("#actualizar_nombre_calle").val(response.NombreCalle);
                        $("#actualizar_alias_calle").val(response.AliasCalle);
                        $("#actualizar_nombre_oficial").val(response.NombreOficial);
                        $("#actualizar_id_poblacion").val(response.IdPoblacion);
                        $("#actualizar_id_poblacion").select2().trigger('change');
                    }, error(error){
                        $("#modal_update_street").modal("hide");
                        swal({title:"Ups!", text:"Error al cargar calle", type: "error"})
                    }
                });
            });
            //fin modal
            //evento cuando se cierra
            $("#modal_update_street").on("hide.bs.modal", function(e){
                $("#actualizar_nombre_calle").val("");
                $("#actualizar_alias_calle").val("");
                $("#actualizar_nombre_oficial").val("");
                $("#actualizar_id_poblacion").val("");
                $("#actualizar_id_poblacion").select2().trigger('change');
                $("#form-update-street").attr("action", "#");
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
            });
            // fin de cierre
            $("#form-update-street").on("submit", function(event){
                event.preventDefault();
                $url = $(this).attr("action");
                $method = $(this).attr("method");
                $data = $(this).serializeArray();
                if(validateUpdateStreet()){
                    $.ajax({
                        url: $url,
                        type: $method,
                        data: $data,
                        success: function(response){
                            $("#modal_update_street").modal("hide");

                            if(response.alert == "success"){
                                data = response.data;
                                swal({title:"Correcto", text:"Se actualizó correctamente la Calle", type: "success"});
                                $(document).find("[data-route='"+$url+"']").parents('tr').html(`<td>`+data.IdCalle+`</td>
                                <td>`+data.NombreCalle+`</td>
                                <td>`+data.AliasCalle+`</td>
                                <td>`+data.NombreOficial+`</td>
                                <td>`+data.location.NombrePoblacion+`</td>
                                <td align="center">
                                    <button data-target="#modal_update_street" data-toggle="modal" data-route="`+response.url_update+`" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></button>
                                    </td>`);
                            }else{
                                swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                            }
                        },error: function(error){
                            $("#modal_update_street").modal("hide");
                            swal({title:"Ups!", text:"Error al actualizar, comuniquese con el administrador", type: "error"})
                        }
                    });
                }
            });

            //validar actualizacion de localidades
            function validateUpdateStreet(){
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
                let errors = 0;

                if($("#actualizar_nombre_calle").val() == "" || $("#actualizar_nombre_calle").val().length < 2 ){
                    errors++;
                    $("#actualizar_nombre_calle").addClass("is-invalid");
                    $("#actualizar_nombre_calle").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre de la calle debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#actualizar_alias_calle").val() == "" || $("#actualizar_alias_calle").val().length < 2 ){
                    errors ++;
                    $("#actualizar_alias_calle").addClass("is-invalid");
                    $("#actualizar_alias_calle").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El alias de la calle debe contener al menos 3 caracteres</strong>
                                </span>`);
                }
                if($("#actualizar_nombre_oficial").val() == "" || $("#actualizar_nombre_oficial").val().length < 2 ){
                    errors ++;
                    $("#actualizar_nombre_oficial").addClass("is-invalid");
                    $("#actualizar_nombre_oficial").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El alias de la calle debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#actualizar_id_poblacion").val() == ""){
                    errors++;
                    $("#actualizar_id_poblacion").addClass("is-invalid");
                    $("#actualizar_id_poblacion").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>Seleccione una población.</strong>
                                </span>`);
                }

                if(errors === 0){
                    return true;
                }

                return false;
            }
        /************************************************************fin actualizar localidad ***********/
        /******************************************agregar nueva calle**********************/
            //evento cuando se cierra
            $("#modal_create_street").on("hide.bs.modal", function(e){
                $("#crear_nombre_calle").val("");
                $("#crear_alias_calle").val("");
                $("#crear_nombre_oficial").val("");
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
            });
            // fin de cierre
            $("#form-create-street").on("submit", function(event){
                event.preventDefault();
                $url = $(this).attr("action");
                $method = $(this).attr("method");
                $data = $(this).serializeArray();
                if(validateStoreStreet()){
                    $.ajax({
                        url: $url,
                        type: $method,
                        data: $data,
                        success: function(response){
                            $("#modal_create_street").modal("hide");

                            if(response.alert == "success"){
                                data = response.data;
                                swal({title:"Correcto", text:"Se Registró correctamente la Calle", type: "success"});
                                $("#table_streets").find('tbody').prepend(`<tr><td>`+data.IdCalle+`</td>
                                <td>`+data.NombreCalle+`</td>
                                <td>`+data.AliasCalle+`</td>
                                <td>`+data.NombreOficial+`</td>
                                <td>`+data.location.NombreOficial+`</td>
                                <td align="center">
                                    <button data-target="#modal_update_street" data-toggle="modal" data-route="`+response.url_update+`" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></button>
                                    </td></tr>`);
                            }else{
                                swal({title:"Ups!", text:"Error al crear, comuniquese con el administrador", type: "error"})
                            }
                        },error: function(error){
                            $("#modal_create_street").modal("hide");
                            swal({title:"Ups!", text:"Error al crear, comuniquese con el administrador", type: "error"})
                        }
                    });
                }
            });

            //validar actualizacion de localidades
            function validateStoreStreet(){
                $(".invalid-feedback").remove();
                $(".is-invalid").removeClass("is-invalid");
                let errors = 0;

                if($("#crear_nombre_calle").val() == "" || $("#crear_nombre_calle").val().length < 2 ){
                    errors++;
                    $("#crear_nombre_calle").addClass("is-invalid");
                    $("#crear_nombre_calle").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre de la calle debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#crear_alias_calle").val() == "" || $("#crear_alias_calle").val().length < 2 ){
                    errors ++;
                    $("#crear_alias_calle").addClass("is-invalid");
                    $("#crear_alias_calle").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El alias debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#crear_nombre_oficial").val() == "" || $("#crear_nombre_oficial").val().length < 2 ){
                    errors ++;
                    $("#crear_nombre_oficial").addClass("is-invalid");
                    $("#crear_nombre_oficial").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>El nombre oficial debe contener al menos 3 caracteres</strong>
                                </span>`);
                }

                if($("#crear_id_poblacion").val() == ""){
                    errors++;
                    $("#crear_id_poblacion").addClass("is-invalid");
                    $("#crear_id_poblacion").parent().append(`<span class="invalid-feedback" role="alert">
                                    <strong>Seleccione una población.</strong>
                                </span>`);
                }

                if(errors === 0){
                    return true;
                }

                return false;
            }
            /***********************fin agregar nueva localidad **************/
        });
    </script>
@endsection