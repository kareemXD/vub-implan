<table>
    <thead>
        <tr>
            <th colspan="9">GIROS</th>
        </tr>
    </thead>
</table>
<table>
    <thead>
        <tr>
            <th>NO.</th>
            <th>GIRO</th>
            <th>LICENCIA</th>
            <th>IMPORTE</th>
            <th>UAN</th>
            <th>BASURA</th>
            <th>TOTAL</th>
            <th>CONTRIBUYENTE</th>
            <th>DIRECCIÓN</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rows as $key => $row)
            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $row['turn'] }}</td>
                <td>{{ (string)$row['license'] }}</td>
                <td>{{ $row['import'] }}</td>
                <td>{{ $row['uan'] }}</td>
                <td>{{ $row['trash'] }}</td>
                <td>{{ $row['total'] }}</td>
                <td>{{ $row['taxpayer'] }}</td>
                <td>{{ $row['address'] }}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $totals['import'] }}</td>
            <td>{{ $totals['uan'] }}</td>
            <td>{{ $totals['trash'] }}</td>
            <td>{{ $totals['total'] }}</td>
            <td></td>
            <td></td>
        </tr>
    </tfoot>
</table>