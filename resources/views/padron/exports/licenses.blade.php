<table class="table table-sm table-striped table-hover">
    <thead class="table-light">
        <tr>
            <th>#</th>
            <th>No. Licencia</th>
            <th>Contribuyente</th>
            <th>Negocio</th>
            <th>Calle</th>
            <th>Exterior</th>
            <th>Interior</th>
            <th>Colonia</th>
            <th>Poblacion</th>
            <th>Giro</th>
            <th>Importe</th>
            <th>Fecha Pago</th>
            <th>Fecha Alta</th>
        </tr>
    </thead>
    <tbody>
        @php
            $rowCounter = 0;
        @endphp
        @foreach ($licenses as $key => $license)
            @php
                $turndetails = $license['license']->getTurnsDetails();
                $rowCounter++;
            @endphp
            <tr>
                <td>{{ $rowCounter }}</td>
                <td>{{ $license['license']->NumeroLicencia }}</td>
                <td>{{ $license['license']->taxpayer->NombreCompleto }}</td>
                <td>{{ $license['license']->NombreNegocio }}</td>
                <td>{{ is_null($license['license']->street())? "": $license['license']->street()->NombreCalle }}</td>
                <td>{{ $license['license']->Exterior }}</td>
                <td>{{ $license['license']->Interior }}</td>
                <td>{{ is_null($license['license']->colony())? "": $license['license']->colony()->NombreColonia }}</td>
                <td>{{ is_null($license['license']->location())? "": $license['license']->location()->NombrePoblacion }}</td>
                <td>{{ $turndetails['turn']['Nombre'] }}</td>
                <td>{{ $license['importe'] }}</td>
                <td>{{ substr($license['fechaPago'], 0, 10) }}</td>
                <td>{{ substr($license['license']->FechaAlta, 0, 10) }}</td>
            </tr>
        @endforeach
    </tbody>
</table>