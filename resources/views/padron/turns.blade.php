@extends('layouts.index')

@section('title') Giros @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Giros</h3>
        </div>
    </div>
    @can('write_turns')
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('add.turn') }}" class="btn btn-primary mr-3"><i class="fas fa-plus mr-2"></i> Nuevo Giro</a>
        </div>
    @endcan
    <div class="panel-body @can('write_turns') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive custam-table">
                <div id="table-filter" style="display:none; text-align:left;">
                    <div class="row margin-10">
                        <div class="col">
                            <button type="button" class="btn btn-success"><i class="fas fa-pdf mr-2"></i> Exportar a PDF</button>
                        </div>
                    </div>
                </div>
                <table class="table table-sm table-hover dataTable">
                    <thead>
                        <tr>
                            <form id="form-data" action="{{ route('turns') }}" method="POST">
                                @csrf
                                <th scope="col"><input type="text" name="cuenta" value="{{ (isset(session('inputs')['cuenta'])) ? session('inputs')['cuenta'] : "" }}" placeholder="ID" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="nombre" value="{{ (isset(session('inputs')['nombre'])) ? session('inputs')['nombre'] : "" }}" placeholder="Nombre" class="form-control custom_input" /></th>
                                <th></th>
                                <th><select name="clave" id="clave" class="form-control custom_input">
                                        <option value="">Clave</option>
                                        @if (isset(session('inputs')['clave']) && session('inputs')['clave'] == "SARE")
                                            <option selected value="SARE">SARE</option>
                                        @else
                                            <option value="SARE">SARE</option>
                                        @endif
                                    </select>
                                </th>   
                            <th><input type="text" name="concepto" value="{{ (isset(session('inputs')['concepto'])) ? session('inputs')['concepto'] : "" }}" placeholder="Concepto" class="form-control custom_input" /></th>
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-submit"><i class="fas fa-search"></i></button></th>
                            </form>
                        </tr>
                        <tr>
                            <th width="5%" scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th width="10%" scope="col">Cuota Fija</th>
                            <th width="10%" scope="col">Clave Hacienda</th>
                            <th width="10%" scope="col">Concepto</th>
                            <th width="10%" scope="col">¿Alcohol?</th>
                            <th width="10%" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($turns as $turn)
                            <tr>
                                <td>{{ $turn->IdGiro }}</td>
                                <td>{{ $turn->Nombre }}</td>
                                <td>$ {{ $turn->CuotaFija }}</td>
                                <td>{{ $turn->CveHacienda }}</td>
                                <td>{{ $turn->Concepto }}</td>
                                <td>{{ ($turn->Alcohol == 0)? 'No' : 'Sí' }}</td>
                                <td align="center"><a href="{{ route('update.turn', $turn->IdGiro) }}" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="table-footer">
                    <div class="pagination">
                        <span>Mostrando {{ (($turns->currentPage() * 15) - 14) }} a {{( $turns->currentPage() * 15) }} de {{ $turns->total() }} Filas</span>
                        {{ $turns->appends(Request::only('IdGiro'))->links() }} 
                    </div>
                    <div class="button-exports">
                        <a href="{{ route('print.turns') }}" target="_BLANK" class="btn btn-success edit-taxpayer"><i class="fas fa-pdf"></i> Exportar a PDF</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $("#padron").addClass('active');
            $(document).on("click", ".btn-submit", function(){
                $( ".custom_input" ).each(function( index, element ) {
                    $("#form-data").append($(this));
                });
                $("#form-data")[0].submit();
            });
            
            $(document).on("keypress", ".custom_input", function(e){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $( ".custom_input" ).each(function( index, element ) {
                        $("#form-data").append($(this));
                    });
                    $("#form-data")[0].submit();
                }
            });

        });
    </script>
@endsection