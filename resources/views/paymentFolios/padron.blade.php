@extends('layouts.index')

@section('title') Estado de cuenta @endsection

@section('css')
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Estado de cuenta {{ !empty($license)? '('.$license->NumeroLicencia.')' : '' }}</h3>
    </div>
</div>
<div class="panel-body">
    <div class="margin-fix panel-row-fluid">
        @if(session()->has('alert'))
            <div class="alert alert-primary" role="alert">
                {{ session("alert") }}
            </div>
        @endif

        @if (!empty($account) && empty($license))
            <div class="alert alert-danger">Licencia no encontrada.</div>
        @endif
        
        <div class="row">
            <div class="col-md-8">
                <form class="" action="{{ route('paymentFolios.padron') }}" method="GET">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="account" class="form-control" placeholder="Licencia" value="{{ $account }}" maxlength="10" required>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-success">Buscar </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <div class="text-right">
                    @if (!empty($license))
                    <a href="{{ route('update.license', $license->NumeroLicencia) }}" target="_blank" class="btn btn-primary btn-next mr-2">Detalle Licencia</a>
                        <a href="{{ route('print.cost.license', [$license->NumeroLicencia]) }}" target="_blank" class="btn btn-primary btn-next mr-2">Orden de Pago</a>
                        <a href="{{ route('print.account.license', [$license->NumeroLicencia]) }}" target="_blank" class="btn btn-primary btn-next">Estado de cuenta</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-hover laravel-pagination">
                <thead>
                    <tr>
                        <th scope="col">Concepto</th>
                        <th scope="col">Referencia</th>
                        <th scope="col">Fecha Movto</th>
                        <th scope="col">Descripción</th>
                        <th scope="col" style="width:8%;">Importe</th>
                        <th scope="col" style="width:8%;">Acciones</th>
                    </tr>
                </thead>
                <tbody class="small-font">
                    @php
                        $total = 0;
                    @endphp
                    @foreach ($debts as $debt)
                        @php
                            $total += (float)$debt->Importe;
                        @endphp
                        <tr>
                            <td>{{ $debt->concept->Descripcion }}</td>
                            <td>{{ $debt->Referencia }}</td>
                            <td>{{ $debt->FechaMovto }}</td>
                            <td>{{ $debt->Descripcion }}</td>
                            <td>${{ $debt->Importe }}</td>
                            <td>
                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editDebtModal" data-reference="{{ $debt->Referencia }}" data-concept="{{ $debt->Concepto }}" data-description="{{ $debt->Descripcion }}" data-amount="{{ $debt->Importe }}"><i class="fas fa-edit"></i></button>
                                <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteDebtModal" data-reference="{{ $debt->Referencia }}" data-originalconcept="{{ $debt->Concepto }}" data-concept="({{ $debt->Concepto }}) {{ $debt->concept->Descripcion }}" data-date="{{ $debt->FechaMovto }}" data-description="{{ $debt->Descripcion }}" data-amount="{{ $debt->Importe }}"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4"></td>
                        <td>${{ number_format($total, 2) }}</td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="editDebtModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="editDebtModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editDebtModalLabel">Editar adeudo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="editDebtForm" action="{{ route('paymentFolios.debts.update') }}" method="POST">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="cuentaDepto" value="{{ !empty($license)? $license->NumeroLicencia : '' }}">
                    <input type="hidden" name="department" value="02">
                    <input type="hidden" name="reference" value="">
                    <input type="hidden" name="originalConcept" value="">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="">Concepto</label>
                            <select name="concept" class="form-control select2" style="width: 100%" required id="">
                                <option value="">Seleccionar concepto</option>
                                @foreach ($concepts as $concept)
                                    <option value="{{ $concept->Concepto }}">({{ $concept->Concepto.') '.$concept->Descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Importe</label>
                        <input type="text" name="amount" class="form-control" required>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="">Motivo de actualización</label>
                        <textarea name="reason" id="" cols="" rows="3" class="form-control" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" form="editDebtForm" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteDebtModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="deleteDebtModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteDebtModalLabel">Eliminar adeudo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="deleteDebtForm" action="{{ route('paymentFolios.debts.delete') }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <input type="hidden" name="cuentaDepto" value="{{ !empty($license)? $license->NumeroLicencia : '' }}">
                    <input type="hidden" name="department" value="02">
                    <input type="hidden" name="reference" value="">
                    <input type="hidden" name="originalConcept" value="">
                    <div class="form-group">
                        <label for="">Concepto</label>
                        <input type="text" name="concept" class="form-control" readonly>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Referencia</label>
                                <input type="text" name="reference_text" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Fecha Movto</label>
                                <input type="text" name="date" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Descripción</label>
                        <input type="text" name="description" class="form-control" readonly>
                    </div>
                    <div class="form-group">
                        <label for="">Importe</label>
                        <input type="text" name="amount" class="form-control" readonly>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label for="">Motivo de eliminación</label>
                        <textarea name="reason" id="" cols="" rows="3" class="form-control" required></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer d-flex justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="submit" form="deleteDebtForm" class="btn btn-danger">Eliminar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(function(){
        $("#menu_paymentfolio").addClass('active');

        $('.select2').select2({
            dropdownParent: $("#editDebtModal")
        });

        $('#editDebtModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget),
                reference = button.data('reference'),
                concept = button.data('concept'),
                description = button.data('description'),
                amount = button.data('amount'),
                modal = $(this);

            modal.find('[name="reference"]').val(reference);
            modal.find('[name="originalConcept"]').val(concept);
            modal.find('[name="concept"]').val(concept).trigger('change');
            modal.find('[name="description"]').val(description);
            modal.find('[name="amount"]').val(amount);
        });

        $('#deleteDebtModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget),
                reference = button.data('reference'),
                concept = button.data('concept'),
                originalConcept = button.data('originalconcept'),
                description = button.data('description'),
                date = button.data('date'),
                amount = button.data('amount'),
                modal = $(this);

            modal.find('[name="reference"]').val(reference);
            modal.find('[name="originalConcept"]').val(originalConcept);
            modal.find('[name="concept"]').val(concept).trigger('change');
            modal.find('[name="reference_text"]').val(reference);
            modal.find('[name="date"]').val(date);
            modal.find('[name="description"]').val(description);
            modal.find('[name="amount"]').val(amount);
        });
    });
</script>
@endsection