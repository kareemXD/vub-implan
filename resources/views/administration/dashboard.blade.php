@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
@endsection

@section('content')
	<div id="dashboard-panel" class="panel-top">
		<div class="text-center">
			<h3>Panel de Control</h3>
		</div>
	</div>
	<div class="panel-body">
		<div class="row panel-row">
			<div class="col-md-12 panel-left">
				<div class="panel-left-content">
					<canvas id="char_license" height="50%"></canvas>
				</div>
			</div>
			{{-- <div class="col-md-6 panel-right">
				<div class="panel-right-content">
					
				</div>
			</div> --}}
		</div>
		<div class="row panel-row" style="margin-top:10px;">
			<div class="col-md-12 panel-left">
				<div class="panel-left-content">
					<canvas id="char_money" height="50%"></canvas>
				</div>
			</div>
			{{-- <div class="col-md-6 panel-right">
				<div class="panel-right-content">
					
				</div>
			</div> --}}
		</div>
	</div>
@endsection

@section('modals')
@endsection

@section('js')}
	<script src="{{ asset('assets/plugins/chart.js/Chart.js') }}"></script>
	<script>
		var labels = [];
		var licenses = [];
		var money = [];

		@forEach($data as $key => $axe)
			labels.push("{{ $axe['nombre'] }}");
			licenses.push("{{ $axe['licensias'] }}");
			money.push("{{ $axe['recaudacion'] }}");
		@endforeach

		var barChartData = {
			labels: labels,
			datasets: [{
				label: 'Número de Licencias',
				backgroundColor: "#66bb6a",
				stack: 'Stack 0',
				data: licenses
			}]
		};

		var barChartData2 = {
			labels: labels,
			datasets: [{
				label: 'Recaudación',
				backgroundColor: "#66bb6a",
				stack: 'Stack 0',
				data: money
			}]
		};
		
		window.onload = function() {
			var _this = $("#dashboard-panel").parents(".container-fluid");
			_this.addClass("panels");
			_this.removeClass("container-fluid");
			var char_license = document.getElementById('char_license').getContext('2d');
			var char_money = document.getElementById('char_money').getContext('2d');
			var current_year = "{{ $now->year }}";

			/*	Gráfica de Licencias Aprobadas por Mes	*/
			window.myBar = new Chart(char_license, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Gráfica de Licencias Aprobadas ' + current_year +  ' por Mes'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					},
					animation: {
						onComplete: function () {
							var chartInstance = this.chart,
							ctx = chartInstance.ctx;
							ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
							ctx.textAlign = 'center';
							ctx.textBaseline = 'bottom';

							this.data.datasets.forEach(function (dataset, i) {
								var meta = chartInstance.controller.getDatasetMeta(i);
								meta.data.forEach(function (bar, index) {
									var data = dataset.data[index];                            
									ctx.fillText(data, bar._model.x, bar._model.y - 5);
								});
							});
						}
					}
				}
			});

			/*	Gráfica de Recaudaciones por Mes   */
			window.myBar = new Chart(char_money, {
				type: 'bar',
				data: barChartData2,
				options: {
					title: {
						display: true,
						text: 'Gráfica de Recaudaciones ' + current_year + ' por Mes'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					},
					animation: {
						onComplete: function () {
							var chartInstance = this.chart,
							ctx = chartInstance.ctx;
							ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
							ctx.textAlign = 'center';
							ctx.textBaseline = 'bottom';

							this.data.datasets.forEach(function (dataset, i) {
								var meta = chartInstance.controller.getDatasetMeta(i);
								meta.data.forEach(function (bar, index) {
									var data = dataset.data[index];                            
									ctx.fillText(data, bar._model.x, bar._model.y - 5);
								});
							});
						}
					}
				}
			});
		};

		$(function(){

		});

	</script>
@endsection