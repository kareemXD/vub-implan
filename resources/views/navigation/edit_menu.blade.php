@extends('layouts.index')

@section('title') Nuevo Menu @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Nuevo Menu</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <form action="{{ route('update.menu') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $menu->id }}">
                <div class="row margin-10 justify-content-center">
                    <fieldset class="col-xl-8 col-sm-12 fieldset">
                        <legend class="legend">Informacion del Menu:</legend>
                        <div class="form-row">
                            <div class="form-group col">
                                <label for="name2">Nombre</label>
                                <input required type="text" class="form-control" id="name2" name="name" value="{{ $menu->nombre }}">
                            </div>
                            <div class="form-group col">
                                <label for="alias">Alias</label>
                                <input required type="text" class="form-control" id="alias" name="alias" readonly value="{{ $menu->alias }}">
                            </div>
                            <div class="form-group col">
                                <label for="url">Ruta</label>
                                <input type="text" class="form-control" id="url" name="url" value="{{ $menu->url }}">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-sm-6">
                                <label for="position">Posición</label>
                                <input required type="number" class="form-control" id="position" name="position" value="{{ $menu->posicion }}">
                            </div>
                            <div class="form-group col-sm-5">
                                <label for="icon">Icono</label>
                                <input required type="text" class="form-control" id="icon" name="icon" value="{{ $menu->icono }}">
                            </div>
                            <div class="col-sm-1 justify-content-center align-items-center" style="display:flex;">
                                <i class="fa-2x {{ $menu->icono }} icon-viewer"></i>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="col-xl-8 col-sm-12 fieldset">
                        <legend class="legend">Submenus:</legend>
                        <div class="row justify-content-end">
                            <button type="button" class="btn btn-primary mr-2 mb-2 add-submenu"><i class="fas fa-plus-circle mr-2"></i> Añadir Submenu</button>
                        </div>
                        <div class="row justify-content-center mb-4" id="submenus-container">
                            @if (count($menu->submenus) > 0)
                                @foreach ($menu->submenus()->orderBy("posicion")->get() as $submenu)
                                    <fieldset class="col-sm-11 fieldset submenu">
                                        <legend class="legend">Informacion del Submenu:</legend>
                                        <input type="hidden" name="submenus_id[]" value="{{ $submenu->id }}">
                                        <button type="button" class="btn btn-danger circle btn-delete"><i class="fas fa-times-circle"></i></button>
                                        <div class="form-row">
                                            <div class="form-group col">
                                                <label for="subname2">Nombre</label>
                                                <input required type="text" class="form-control" name="subname[]" value="{{ $submenu->nombre }}">
                                            </div>
                                            <div class="form-group col">
                                                <label for="suburl">Ruta</label>
                                                <input required type="text" class="form-control"  name="suburl[]" value="{{ $submenu->url }}">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-sm-6">
                                                <label for="subalias">Alias</label>
                                                <input required type="text" class="form-control"  name="subalias[]" value="{{ $submenu->alias }}" readonly>
                                            </div>
                                            <div class="form-group col-sm-6">
                                                <label for="subposition">Posición</label>
                                                <input required type="number" class="form-control" name="subposition[]" value="{{ $submenu->posicion }}">
                                            </div>
                                        </div>
                                    </fieldset>
                                @endforeach
                            @endif
                        </div>
                    </fieldset>
                </div>
                <div class="row margin-10 justify-content-center ">
                    <button type="submit" class="btn btn-primary mr-2 mb-2 mt-3"><i class="fas fa-save mr-2"></i> Guardar</button>
                </div>
            </form>
        </div>
    </div>
    
@endsection

@section('modals')

@endsection

@section('js')
    <script>
        $(function(){
            $("#users").addClass('active');
            $(document).on("focusout", "#icon", function(){
                $(".icon-viewer").addClass($(this).val());
            });

            $(document).on("click", ".add-submenu", function(){
                $("#url").attr("disabled", true);
                $("#submenus-container").append('<fieldset class="col-sm-11 fieldset submenu">\
                        <legend class="legend">Informacion del Submenu:</legend>\
                        <button type="button" class="btn btn-danger circle btn-delete"><i class="fas fa-times-circle"></i></button>\
                        <div class="form-row">\
                            <div class="form-group col">\
                                <label for="subname2">Nombre</label>\
                                <input required type="text" class="form-control" id="subname" name="subname[]">\
                            </div>\
                            <div class="form-group col">\
                                <label for="suburl">Ruta</label>\
                                <input required type="text" class="form-control" id="suburl" name="suburl[]">\
                            </div>\
                        </div>\
                        <div class="form-row">\
                            <div class="form-group col-sm-6">\
                                <label for="subalias">Alias</label>\
                                <input required type="text" class="form-control" id="subalias" name="subalias[]">\
                            </div>\
                            <div class="form-group col-sm-6">\
                                <label for="subposition">Posición</label>\
                                <input required type="number" class="form-control" id="subposition" name="subposition[]">\
                            </div>\
                        </div>\
                    </fieldset>');
                    
                    $("#submenus-container .fieldset").last().hide().animate({
                        opacity: 10,
                        height: "toggle"
                    }, 800, function() {
                        // Animation complete.
                    });

            });

            $(document).on("click", ".btn-delete", function(){
                var _this = $(this);
                _this.parent("fieldset").hide("slow", function(){
                     _this.parent("fieldset").remove();
                    if($(".submenu").length == 0)
                    {
                        $("#url").removeAttr("disabled");
                    }
                });
            });

            if($(".submenu").length > 0)
            {
                $("#url").attr("disabled", true);
            }
        });
    </script>
@endsection