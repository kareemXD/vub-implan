@extends('layouts.index')

@section('title') Menus @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Menus</h3>
        </div>
    </div>
    @can('write_menus')
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('new.menu') }}" class="btn btn-primary mr-3"><i class="fas fa-plus-circle mr-2"></i> Crear Menu</a>
        </div>
    @endcan
    <div class="panel-body with-buttons">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="row margin-10 justify-content-center">
                @foreach ($menus as $menu)
                    <div class="col-md-3 col-md-offset-3">
                        <div class="card">
                            <div class="card-image">
                                <i class="{{ $menu->icono }}"></i>
                            </div><!-- card image -->
                            <div class="card-content">
                                <span class="card-title">{{ $menu->nombre }}</span>                    
                                <button type="button" class="show btn btn-custom pull-right" aria-label="Left Align">
                                    <i class="fa fa-ellipsis-v"></i>
                                </button>
                            </div><!-- card content -->
                            <div class="card-action">
                                <a href="{{ route('edit.menu', $menu->id) }}" class="btn-link">Editar</a>
                            </div><!-- card actions -->
                            <div class="card-reveal">
                                <span class="card-title">Submenus</span> <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <table class="table table-sm">
                                    <thead>
                                        <tr>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Url</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if (count($menu->submenus) > 0)
                                            @foreach ($menu->submenus()->orderBy("posicion")->get() as $submenu)
                                                <tr>
                                                    <td>{{ $submenu->nombre }}</td>
                                                    <td>{{ $submenu->url }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="2">Sin submenus</td>
                                            </tr>
                                        @endif
                                        
                                    </tbody>
                                </table>
                            </div><!-- card reveal -->
                        </div>
                    </div>
                @endforeach   
            </div>
        </div>
    </div>
@endsection

@section('modals')

@endsection

@section('js')
    <script>
        $(function(){
            $("#auth").addClass('active');
            $(function(){
                $('.show').on('click',function(){        
                    $(this).parents(".card").find('.card-reveal').slideToggle('slow');
                });
                $('.card-reveal .close').on('click',function(){
                    $(this).parents(".card").find('.card-reveal').slideToggle('slow');
                });
            });
        });
    </script>
@endsection