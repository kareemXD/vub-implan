@extends('layouts.index')

@section('title') Solicitud: UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }} @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
<div class="panel-top">
    <div class="d-flex justify-content-between align-items-center">
        <h3>Solicitud: UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}</h3>
        <div class="d-flex align-items-center h3"><span>Estatus:</span> <span class="badge badge-secondary ml-3">{{ config('system.du.requests.statuses.labels.'.$request->estatus) }}</span></div>
    </div>
</div>
<div class="panel-body @can('write_du_requests') with-buttons @endcan">
    <div class="margin-fix panel-row-fluid">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-link active" id="period-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Ubicación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="data-tab" data-toggle="tab" href="#data" role="tab" aria-controls="data" aria-selected="true">Detalles</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="formalities-tab" data-toggle="tab" href="#formalities" role="tab" aria-controls="formalities" aria-selected="true">Protección Civil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link" id="period-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="documents" aria-selected="false">Requisitos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link" id="period-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments" aria-selected="false">Observaciones</a>
            </li>
        </ul>
        <form id="form-data" action="{{ route('du.store.inspect.request') }}" method="POST" class="tab-content">
            @csrf
            <input type="hidden" name="operation" value="observation">
            <input type="hidden" name="solicitud" value="{{ $request->id }}">
            <input type="hidden" name="contribuyente" value="{{ $request->id_contribuyente }}">
            <input type="hidden" name="user" id="user">
            
            <div class="tab-pane fade show active pt-3" id="location" role="tabpanel" aria-labelledby="location-tab">
                @if(session()->has('alert'))
                    <div class="alert alert-primary" role="alert">
                        {{ session("alert") }}
                    </div>
                @endif
                <div class="form-group mt-1 mb-1 row margin-10 ">
                    <label for="city" class="col-sm-2 col-form-label">Poblacion <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <select disabled name="city" id="city" class="select2 form-control {{ $errors->has('city') ? ' is-invalid' : '' }}">
                            <option value="0">Seleccione una Población</option>
                            @foreach ($poblaciones as $poblacion)
                                @if ($poblacion->IdPoblacion == $request->id_poblacion)
                                    <option selected value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @else
                                    <option value="{{ $poblacion->IdPoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10 ">
                    <label for="colony" class="col-sm-2 col-form-label">Colonia <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <div class="input-group ">
                            <select disabled name="colony" id="colony" class="required select2 form-control {{ $errors->has('colony') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Colonia</option>
                            </select>
                            @if ($errors->has('colony'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('colony') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="domicilio2" class="col-sm-2 col-form-label">Calle <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <div class="input-group ">
                            <select disabled name="domicilio2" id="domicilio2" class="required select2 form-control {{ $errors->has('domicilio2') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Calle</option>
                            </select>
                            @if ($errors->has('domicilio2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('domicilio2') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="form-group mt-1 mb-1 row margin-10">
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="calle1_u" class="col-sm-2 col-form-label">Entre Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group ">
                            <select disabled name="calle1_u" id="calle1_u" class="select2 form-control {{ $errors->has('calle1_u') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle1_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle1_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="ext" class="col-sm-2 col-form-label">Exterior <span class="text-danger">*</span></label>
                    <div class="col-sm-2">
                        <input disabled type="text" class="form-control {{ $errors->has('ext') ? ' is-invalid' : '' }}" id="ext" name="ext" value="{{ $request->no_exterior }}">
                        @if ($errors->has('ext'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ext') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="calle2_u" class="col-sm-2 col-form-label">Y Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group ">
                            <select disabled name="calle2_u" id="calle2_u" class="select2 form-control {{ $errors->has('calle2_u') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle2_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle2_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="int" class="col-sm-2 col-form-label">Interior</label>
                    <div class="col-sm-2">
                        <input disabled type="text" class="form-control {{ $errors->has('int') ? ' is-invalid' : '' }}" id="int" name="int" value="{{ $request->no_interior }}">
                        @if ($errors->has('int'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('int') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row margin-10">
                    <label for="este" class="col-sm-2 col-form-label">Coordenada Este</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="form-control" id="este" name="este" value="{{ $request->coordenada_este }}">
                    </div>
                    <label for="norte" class="col-sm-2 col-form-label">Coordenada Norte</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="form-control" id="norte" name="norte" value="{{ $request->coordenada_norte }}">
                    </div>
                </div>
                <div id="map" class="map" style="width:100%; height:500px;"></div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="submit" class="btn btn-primary mr-2   btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

            <div class="tab-pane fade pt-3" id="data" role="tabpanel" aria-labelledby="data-tab">
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Solicitante</legend>

                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="nombre">Nombre(s) <span class="text-danger">*</span></label>
                            <input disabled maxlength="100" type="text" class="required form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ $request->taxpayer->nombre }}" >
                            @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="apellido_pat">Apellido Paterno <span class="text-danger">*</span></label>
                            <input disabled maxlength="50" type="text" class="required form-control {{ $errors->has('apellido_pat') ? ' is-invalid' : '' }}" id="apellido_pat" name="apellido_pat" value="{{ $request->taxpayer->apellido_pat }}" >
                            @if ($errors->has('apellido_pat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellido_pat') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="apellido_mat">Apellido Materno <span class="text-danger">*</span></label>
                            <input disabled maxlength="50" type="text" class="required form-control {{ $errors->has('apellido_mat') ? ' is-invalid' : '' }}" id="apellido_mat" name="apellido_mat" value="{{ $request->taxpayer->apellido_mat }}">
                            @if ($errors->has('apellido_mat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellido_mat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="email">Correo</label>
                            <input disabled type="email" maxlength="50" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ $request->taxpayer->correo }}" >
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="telefono">Telefono <span class="text-danger">*</span></label>
                            <input disabled type="number" maxlength="15" class="form-control {{ $errors->has('telefono') ? ' is-invalid' : '' }}" id="telefono" name="telefono" value="{{ $request->taxpayer->telefono }}" >
                            @if ($errors->has('telefono'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="edad">Edad</label>
                            <input disabled type="number" maxlength="15" class="form-control {{ $errors->has('edad') ? ' is-invalid' : '' }}" id="edad" name="edad" value="{{ $request->taxpayer->edad }}" >
                            @if ($errors->has('edad'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('edad') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="sexo">Sexo <span class="text-danger">*</span></label>
                            <select disabled name="sexo" id="sexo" class="form-control {{ $errors->has('sexo') ? ' is-invalid' : '' }}">
                                <option value="I">Seleccione Sexo</option>
                                @php
                                    $sexos = [
                                        [
                                            'name' => 'Indefinido',
                                            'value' => 'I'
                                        ],
                                        [
                                            'name' => 'Hombre',
                                            'value' => 'M'
                                        ],
                                        [
                                            'name' => 'Mujer',
                                            'value' => 'F'
                                        ]
                                    ];
                                @endphp
                                @foreach ($sexos as $sexo)
                                    @if ($sexo['value'] == $request->taxpayer->sexo)
                                        <option selected value="{{ $sexo['value'] }}">{{ $sexo['name'] }}</option>
                                    @else
                                        <option value="{{ $sexo['value'] }}">{{ $sexo['name'] }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @if ($errors->has('sexo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('sexo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="rfc">R.F.C</label>
                            <input disabled maxlength="15" type="text" class="form-control {{ $errors->has('rfc') ? ' is-invalid' : '' }}" id="rfc" name="rfc" value="{{ $request->taxpayer->rfc }}" placeholder="XAXX010101000">
                            @if ($errors->has('rfc'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rfc') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="persona">¿ Persona Moral ?</label>
                            <input disabled type="checkbox" data-toggle="toggle"  data-on="Si" data-off="No" class="form-control " id="persona" name="persona" @if($request->taxpayer->persona == 'true') checked @endif>
                            @if ($errors->has('persona'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('persona') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row inputs-hidden" style="display: none">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="razon">Razon Social</label>
                            <input disabled type="text" class="form-control {{ $errors->has('razon') ? ' is-invalid' : '' }}" id="razon" name="razon" value="{{ $request->taxpayer->razon_social }}" >
                            @if ($errors->has('razon'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('razon') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="telefono_trab">Telefono Empresa</label>
                            <input disabled type="number" maxlength="15" class="form-control {{ $errors->has('telefono_trab') ? ' is-invalid' : '' }}" id="telefono_trab" name="telefono_trab" value="{{ $request->taxpayer->telefono_trab }}" >
                            @if ($errors->has('telefono_trab'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono_trab') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="rfc_trab">R.F.C Empresa</label>
                            <input disabled maxlength="15" type="text" class="form-control {{ $errors->has('rfc_trab') ? ' is-invalid' : '' }}" id="rfc_trab" name="rfc_trab" value="{{ $request->taxpayer->rfc_trab }}" placeholder="XAXX010101000">
                            @if ($errors->has('rfc_trab'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rfc_trab') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="gestor">Gestor</label>
                            <input maxlength="255" disabled type="text" class="form-control {{ $errors->has('gestor') ? ' is-invalid' : '' }}" id="gestor" name="gestor" value="{{ $request->gestor }}">
                        </div>
                    </div>
                    <br />
                </fieldset>
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Solicitud</legend>
                    <div class="form-group mt-1 mb-1 row margin-10">
                        <label for="clave" class="col-sm-2 col-form-label">Clave Catastral <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input disabled type="text" class="required form-control {{ $errors->has('clave') ? ' is-invalid' : '' }}" id="clave" name="clave" value="{{ $request->clave_catastral }}">
                            @if ($errors->has('clave'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('clave') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group mt-1 mb-1 row margin-10">
                        <label for="observations" class="col-sm-2 col-form-label">Observaciones</label>
                        <div class="col-sm-10">
                            <textarea disabled class="form-control {{ $errors->has('observations') ? ' is-invalid' : '' }}" id="observations" name="observations" rows="4">{{ $request->observaciones }}</textarea>
                            @if ($errors->has('observations'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('observations') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div><br />
                </fieldset>
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Tramites</legend>
                    <div class="d-flex justify-content-center">
                        <div class="col-sm-8">
                            <table class="table table-sm table-procedures">
                                <tbody>
                                    @foreach ($request->procedures as $process)
                                        <tr class="process-row" data-id="{{ $process->id }}">
                                            <td>
                                                <div>
                                                    <span class="process-name"><i class="far fa-file-alt"></i> {{ $process->nombre }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br />
                </fieldset>
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

            <div class="tab-pane fade" id="formalities" role="tabpanel" aria-labelledby="formalities-tab">
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Proteccion Civil</legend>
                    <div class="row">
                        <div class="col-7">
                            <div class="d-flex justify-content-between">
                                <h4>Tramites</h4>
                                @if ($request->pcRequirements()->count() > 0)
                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#newFormalityModal"><i class="fas fa-plus"></i> Nuevo tramite</button>
                                @else
                                    <div class="bg-warning text-dark px-3 py-2">Suba los requisitos necesarios para poder solicitar algún tramite de Protección civil.</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-5">
                            <h4>Requisitos</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <table id="tableFormalities" class="table table-sm table-hover" style="margin: 1.5em 0px !important;">
                                <thead>
                                    <tr>
                                        <th width="10%">#</th>
                                        <th>Tramite</th>
                                        <th>Comentarios</th>
                                        <th width="10%">Estatus</th>
                                        <th width="8%">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($request->pcRequests as $pcRequest)
                                        <tr>
                                            <td>{{ $pcRequest->pcRequest->requestNumberText() }}</td>
                                            <td>Visto Bueno</td>
                                            <td>{{ $pcRequest->pcRequest->resultado_dictaminacion }}</td>
                                            <td class="{{ $pcRequest->pcRequest->statusClass() }}">{{ $pcRequest->pcRequest->status() }}</td>
                                            <td>
                                                @if ($pcRequest->pcRequest->status() == 'PENDIENTE')
                                                    <a href="{{ route('pc.requests.edit', $pcRequest->pcRequest->id) }}?redirect=du_inspect" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                                                @else
                                                    <a href="{{ route('pc.requests.detail', $pcRequest->pcRequest->id) }}" class="btn btn-secondary btn-sm"><i class="fas fa-eye"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-5">
                            <table class="table table-sm table-hover" style="margin: 1.5em 0px !important;">
                                <thead>
                                    <tr>
                                        <th>Requisito</th>
                                        <th width="10%">Estatus</th>
                                        <th width="10%">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($requirements as $requirement)
                                        <tr>
                                            <td>{{ $requirement->requisito }}</td>
                                            <td class="{{ $request->pcRequirementStatusClass($requirement->id) }}">{{ $request->pcRequirementStatus($requirement->id) }}</td>
                                            <td class="d-flex">
                                                <button class="btn btn-primary btn-sm" type="button" data-toggle="modal" data-target="#updatePcRequirementModal" data-requirement="{{ $requirement->requisito }}" data-requirementid="{{ $requirement->id }}" title="Subir / Actualizar documento"><i class="fas fa-edit"></i></button>
                                                @if ($request->pcRequirementStatus($requirement->id) != 'Pendiente')
                                                    <a href="{{ asset('storage/'.$request->pcRequirements()->where('id_requisito', $requirement->id)->first()->documento) }}" class="btn btn-secondary btn-sm ml-1" target="_blank" title="Descargar documento"><i class="fas fa-download"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </fieldset>
            </div>
            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                        
            <div class="tab-pane fade pt-3" id="documents" role="tabpanel" aria-labelledby="documents-tab">
                <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                    <thead>
                        <tr>
                            <th>Nombre del Requisito</th>
                            <th>Comentarios</th>
                            <th width="10%">Estatus</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="turns-content">
                        @foreach ($request->documents as $document)
                            <tr>
                                <td width="30%">{{ $document->nombre }}</td>
                                <td width="30%">{{ $document->pivot->comentario }}</td>
                                <td width="10%" class="{{ ($document->pivot->estatus == 1)? 'bg-green' : 'bg-yellow' }}">{{ ($document->pivot->estatus == 1)? 'Terminado' : 'Proceso' }}</td>
                                <td width="5%" style="text-align: center;">
                                @if ($document->pivot->archivo)
                                    <button type="button" class="btn btn-primary btn-action-table download-file" data-toggle="tooltip" data-placement="top" title="Descargar Archivo" data-requisito="{{ $document->id }}" data-solicitud="{{ $request->id }}"><i class="fas fa-download"></i></button>
                                @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                        
            <div class="tab-pane fade pt-3" id="comments" role="tabpanel" aria-labelledby="comments-tab">
                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col">
                        <label for="comment">Agregar Observación</label>
                        <textarea name="comment" class="required form-control " id="comment" name="comment" cols="30" rows="2"></textarea>
                    </div>
                </div>
                <button type="button" class="btn btn-primary mr-3" id="btn-save-changes"><i class="fas fa-save"></i> Guardar Observación</button>
                @if (count($request->comments) > 0)
                    @foreach ($request->comments()->orderBy('id', 'desc')->get() as $comment)
                        <div class="comment {{ (Auth::user()->id == $comment->id_usuario)? 'response' : '' }}">
                            <h5>{{ config('system.du.requests.statuses.labels.'.$comment->estatus) }}</h5>
                            <span><i class="fas fa-user"></i> {{ $comment->user->name }}</span>
                            <p>{{ $comment->observaciones }}</p>  
                        </div>
                    @endforeach
                @else
                    <h4>Sin observaciones.</h4>
                @endif
            </div>
        </form>
    </div>
</div>
@can('write_du_requests')
    <div class="row justify-content-end panel-buttoms">
        <button type="button" class="btn btn-primary mr-3 btn-next " id="btn-previous" style="display:none;" data-target="#data">Anterior <i class="fas fa-arrow-left ml-2"></i></button>
        <button type="button" class="btn btn-primary mr-3 btn-next" id="btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
        <button type="button" class="btn btn-primary mr-3" id="btn-verify" style="display:none;"><i class="fas fa-check-circle"></i> Verificación</button>
    </div>
@endcan
@endsection

@section('modals')
    @include('partials.modals.du.modal_update_document')
    @include('partials.modals.du.modal_new_formality')
    @include('partials.modals.du.modal_update_pc_requirement')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>

    <script>
        window.addEventListener('load', function () {
            OnChangeCity("{{ $request->id_colonia }}");
            OnChangeColony("{{ $request->id_calle }}", "{{ $request->id_cruce1 }}", "{{ $request->id_cruce2 }}");
            OnPersona("{{ $request->taxpayer->persona }}");
            
            @if (session()->has('show_formalities'))
                setTimeout(function() {
                    $('#myTab a[href="#formalities"]').tab('show');
                }, 1000);
            @endif
        });

        function OnPersona(persona = null)
        {
            if(persona == '1'){
                $(".inputs-hidden").find("#razon").addClass("required");
                $(".inputs-hidden").find("#telefono_trab").addClass("required");
                $(".inputs-hidden").find("#rfc_trab").addClass("required");
                $(".inputs-hidden").fadeIn("slow");
            }
        }

        function OnChangeCity(colony = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.colonies.by.city') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#colony").empty();
                $("#colony").append("<option value='0'>Seleccione una Colonia</option>");
                result.forEach(element => {
                    $("#colony").append($('<option>', {
                        value: element.IdColonia,
                        text: element.NombreColonia
                    }));
                });
                if(colony)
                {
                    $("#colony").val(colony);
                }
            });
        }

        function OnChangeColony(calle = null, calle1 = null, calle2 = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.calles.by.colony') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#domicilio2").empty();
                $("#domicilio2").append("<option value='0'>Seleccione una Calle</option>");

                $("#calle1_u").empty();
                $("#calle1_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                $("#calle2_u").empty();
                $("#calle2_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                result.forEach(element => {
                    $("#domicilio2").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle1_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle2_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));
                });
                if(calle || calle1 || calle2)
                {
                    $("#domicilio2").val(calle);
                    $("#calle1_u").val(calle1);
                    $("#calle2_u").val(calle2);
                }
            });
        }
        
        $(function(){
            @if(session()->has('comment_added'))
                $('#myTab a[href="#comments"]').trigger('click');
                $("#btn-previous").fadeIn("fast", function(){
                    $("#btn-previous").data("target", "#documents")
                });
                $("#btn-next").fadeOut("fast", function(){
                    $("#btn-verify").fadeIn("fast");
                });
            @endif
            $("#du").addClass('active');
            // $('#update_persona').bootstrapToggle('on')
            var utm = "+proj=utm +zone=13";
            var wgs84 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
            var east = parseFloat($("#este").val().replace(",", "."));
            var nort = parseFloat($("#norte").val().replace(",", "."));

            var coordinates = proj4(utm,wgs84,[ east,nort]);
            var lat_and_long = {lat: coordinates[1], lng: coordinates[0]};
            var created = false;
            var map = L.map('map',{scrollWheelZoom:true}).setView([coordinates[1], coordinates[0]], 16);
            // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            //     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap Saul Moncivais</a> contributors'
            // }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:Vuelo_Bajo_Ortofoto@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 12,
                maxZoom: 23,
                attribution: '&copy; IMPLAN - Ortofoto Bahia de Banderas 2019'
            }).addTo(map);
            
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:vialidad_BB@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);

            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:lim_loc_final@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);
            var theMarker = {};
            theMarker = L.marker(lat_and_long).addTo(map);
            
            map.on('click',function(e){
                if (theMarker != undefined) {
                    map.removeLayer(theMarker);
                }; 
                theMarker = L.marker(e.latlng).addTo(map);
                $("#este").val(theMarker.getLatLng().utm().x)
                $("#norte").val(theMarker.getLatLng().utm().y)
            });
            
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var target = e.target.hash,
                    flag = true;
                switch (e.relatedTarget.hash) {
                    case "#data":
                        $("#data").find(".required").each(function() {
                            if(!$(this).val()){
                                flag = false;
                                $(this).addClass("is-invalid");
                                $(this).parent().find(".select2").addClass("is-invalid");
                            }else{
                                if($(this).hasClass("is-invalid")){
                                    $(this).removeClass("is-invalid");
                                    $(this).parent().find(".select2").removeClass("is-invalid");
                                }
                            }
                        });
                        if(!flag)
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor complete los datos del solicitante para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        break;
                
                    case "#location":
                        $("#location").find(".required").each(function() {
                            if(!$(this).val()){
                                flag = false;
                                $(this).addClass("is-invalid");
                                $(this).parent().find(".select2").addClass("is-invalid");
                            }else{
                                if($(this).hasClass("is-invalid")){
                                    $(this).removeClass("is-invalid");
                                    $(this).parent().find(".select2").removeClass("is-invalid");
                                }
                            }
                        });
                        
                        if(!flag)
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione la ubicación para continuar y/o Verifique que la dirección sea correcta.!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        
                        break;
                    case "#comments":
                        break;
                }
                if(flag)
                {
                    switch (e.target.hash) {
                        case "#location":
                            $("#btn-previous").fadeOut("fast")
                            $("#btn-next").data("target", "#data");
                            $("#btn-next").fadeIn("fast");

                            $("#btn-verify").fadeOut("fast");
                            break;
                        case "#data":
                            $("#btn-previous").fadeIn("fast", function(){
                                $("#btn-previous").data("target", "#location")
                            });
                            $("#btn-next").data("target", "#formalities");
                            $("#btn-next").fadeIn("fast");

                            $("#btn-verify").fadeOut("fast");
                            break;
                        case "#formalities":
                            $("#btn-previous").fadeIn("fast", function(){
                                $("#btn-previous").data("target", "#data")
                            });
                            $("#btn-next").data("target", "#documents");
                            $("#btn-next").fadeIn("fast");

                            $("#btn-verify").fadeOut("fast");
                            break;
                        case "#documents":
                            $("#btn-previous").fadeIn("fast", function(){
                                $("#btn-previous").data("target", "#formalities")
                            });
                            $("#btn-next").data("target", "#comments");
                            $("#btn-next").fadeIn("fast");

                            $("#btn-verify").fadeOut("fast");
                            break;
                        case "#comments":
                            $("#btn-previous").fadeIn("fast", function(){
                                $("#btn-previous").data("target", "#documents")
                            });
                            $("#btn-next").fadeOut("fast", function(){
                                $("#btn-verify").fadeIn("fast");
                            });
                            break;
                    }
                }
            });

 
            $(document).on("click", "#btn-verify", function(){
                $.confirm({
                    title: 'Aviso!',
                    content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción mandara el expediente a verificación.</small>',
                    type: "orange",
                    buttons: {
                        Aceptar: function () {
                            $("input[name=operation]").val('verify');
                            $("#form-data")[0].submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });
            $(document).on("click", "#btn-save-changes", function(){
                let flag = true;
                $("#comments").find(".required").each(function() {
                    if(!$(this).val() || $(this).val().length == 0){
                        flag = false;
                        $(this).addClass("is-invalid");
                        $(this).parent().find(".select2").addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                            $(this).parent().find(".select2").removeClass("is-invalid");
                        }
                    }
                });
                
                if(flag){
                    $.confirm({
                        title: 'Confirmación!',
                        content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción modificara la solicitud.</small>',
                        type: "blue",
                        buttons: {
                            Aceptar: function () {
                                $('#form-data')[0].submit();
                            },
                            Cancelar: function () {
                            },
                        }
                    });
                }else{
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor Verifique que la información sea correcta.!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                }
            });

            $(document).on("click", ".btn-next", function(){
                $('#myTab a[href="'+$(this).data("target")+'"]').tab('show');
            });

            $(document).on("change", "#city", function(){
                OnChangeCity();
            });
            $(document).on("change", "#colony", function(){
                OnChangeColony();
            });
            
            $(document).on("click", ".btn-other", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);
                parent.find("span.select2").fadeOut("slow", function(){
                    parent.append('<input type="text" class="form-control inputInserted '+_this.data("class")+'" name="new'+_this.data("name")+'" placeholder="Escriba el nombre de la Calle"> ').show('slow');
                    _this.html('<i class="fas fa-minus-circle"></i>');
                    _this.removeClass("btn-other");
                    _this.addClass("remove-calle");
                    _this.attr("data-original-title", "Seleccionar Calle").tooltip('show');
                });
            });

            $(document).on("click", ".remove-calle", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);
                parent.find(".inputInserted").fadeOut("slow", function(){
                    $(this).remove();
                    parent.find("span.select2").fadeIn("slow")
                    _this.html('<i class="fas fa-plus-circle"></i>')
                    _this.removeClass("remove-calle");
                    _this.addClass("btn-other");
                    _this.attr("data-original-title", "Añadir una Calle").tooltip('show');
                });
            });
            $(document).on("change", "#persona", function(){
                if($(this).is(":checked")){
                    $(".inputs-hidden").find("#razon").addClass("required");
                    $(".inputs-hidden").find("#telefono_trab").addClass("required");
                    $(".inputs-hidden").find("#rfc_trab").addClass("required");
                    $(".inputs-hidden").fadeIn("slow");
                }
                else{
                    $(".inputs-hidden").find("#razon").removeClass("required");
                    $(".inputs-hidden").find("#telefono_trab").removeClass("required");
                    $(".inputs-hidden").find("#rfc_trab").removeClass("required");
                    $(".inputs-hidden").fadeOut("slow");
                }
            });

            $(document).on("click", ".btn-update-document", function(){
                var _this = $(this);
                $.ajax({
                    url: "{{ route('get.document.du.by.request') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        requisito: _this.data("requisito"),
                        solicitud: _this.data("solicitud")
                    },
                }).done(function(result){
                    console.log(result);
                    var modal = $("#modal-update-document");
                    modal.find("#requisito_id").val(result.id);
                    modal.find("#nombre_r").val(result.nombre);
                    modal.find("#comments").val(result.pivot_comentario);
                    if(result.pivot_archivo)
                    {
                        modal.find("#documents-content").prepend('<ul><li class="download-file" data-requisito="'+result.Requisito+'" data-solicitud="'+$("input[name=id]").val()+'">\
                            <img src="{{ asset("assets/plugins/preview/preview_pdf.png") }}" >\
                        </li></ul>');
                        $('.old_document').simpleFilePreview();
                    }
                    modal.modal("show");
                });
            });

            $(document).on("click", ".download-file", function(){
                var requisito = $(this).data("requisito");
                var solicitud = $(this).data("solicitud");
                window.open('/du/mostrar/documento/'+requisito+'/'+solicitud, '_blank');
            });
            //despues ejecutar
            
            $('#updatePcRequirementModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    requirementId = button.data('requirementid'),
                    requirement = button.data('requirement'),
                    modal = $(this);

                modal.find('#nombre_r').val(requirement);
                modal.find('#requisito_id').val(requirementId);
            });
        });
    </script>
@endsection