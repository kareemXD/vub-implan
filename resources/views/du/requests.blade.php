@extends('layouts.index')

@section('title') Solicitudes @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Solicitudes</h3>
        </div>
    </div>
    
    <form id="searchForm" action="{{ route('du.requests') }}" method="GET">
        <input type="hidden" name="searching" value="0">
        <input type="hidden" name="section" value="">
    </form>
    
    @can('write_du_requests')
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('du.add.request') }}" class="btn btn-primary mr-3"><i class="fas fa-plus-circle mr-2"></i> Nueva Solicitud</a>
        </div>
    @endcan
    <div class="panel-body @can('write_contribuyentes') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @can('write_du_ventanilla')
                <li class="nav-item">
                    <a class="nav-link btn-link active" id="venta-tab" data-toggle="tab" href="#venta" role="tab" aria-controls="venta" aria-selected="false">Ventanilla</a>
                </li>
                @endcan
                @can('write_du_verificacion')
                <li class="nav-item">
                    <a class="nav-link btn-link" id="verif-tab" data-toggle="tab" href="#verif" role="tab" aria-controls="verif" aria-selected="false">Verificador</a>
                </li>
                @endcan
                @can('write_du_inspeccion')
                <li class="nav-item">
                    <a class="nav-link btn-link" id="inspec-tab" data-toggle="tab" href="#inspec" role="tab" aria-controls="inspec" aria-selected="false">Inspector</a>
                </li>
                @endcan
                @can('write_du_dictaminacion')
                <li class="nav-item">
                    <a class="nav-link btn-link " id="dicta-tab" data-toggle="tab" href="#dicta" role="tab" aria-controls="dicta" aria-selected="true">Dictaminador</a>
                </li>
                @endcan
                {{-- @can('write_du_confirmacion')
                <li class="nav-item">
                    <a class="nav-link btn-link" id="confir-tab" data-toggle="tab" href="#confir" role="tab" aria-controls="confir" aria-selected="false">Confirmación</a>
                </li>
                @endcan --}}
                {{-- @can('write_du_concluidas')
                <li class="nav-item">
                    <a class="nav-link btn-link" id="concluidas-tab" data-toggle="tab" href="#concluidas" role="tab" aria-controls="concluidas" aria-selected="false">Concluidas</a>
                </li>
                @endcan --}}
            </ul>
            <div class="tab-content">
                @can('write_du_ventanilla')
                <div class="tab-pane fade show active" id="venta" role="tabpanel" aria-labelledby="location-tab">
                    <br>
                    <table style="width: 100%;" class="table dataTable table-sm table-hover">
                        <thead>
                            <tr class="search-field" data-section="venta">
                                <th scope="col" style="width:10%;"><input type="text" name="cuenta" style="font-size: 13px" value="{{ $search['cuenta'] }}" placeholder="Solicitud" class="form-control input-search" /></th>
                                <th scope="col"><input type="text" name="contribuyente" style="font-size: 13px" value="{{ $search['contribuyente'] }}" placeholder="Nombre Contribuyente" class="form-control input-search" /></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th class="btn-search-table" style="width:5%;"><button type="button" class="btn btn-block btn-success btn-submit"><i class="fas fa-search"></i></button></th>
                            </tr>
                            <tr>
                                <th scope="col" style="width:10%;">#</th>
                                <th scope="col">Nombre Contribuyente</th>
                                <th scope="col">Tramite</th>
                                <th scope="col">Fecha Alta</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Última actualización</th>
                                <th scope="col" style="width:5%;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody class="small-font">
                            @if(count($requests) > 0)
                                @foreach ($requests as $request)
                                    <tr>
                                        <td>UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}</td>
                                        <td>{{ $request->taxpayer->nombreCompleto() }}</td>
                                        <td><a href="javascript:;" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#requestProceduresModal" data-id="{{ $request->id }}" data-number="UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}"><i class="far fa-file-alt"></i> Tramites</a></td>
                                        <td>{{ $request->created_at->format('Y-m-d') }}</td>
                                        <td>{{ config('system.du.requests.statuses.labels.'.$request->estatus) }}</td>
                                        <td>{{ $request->updated_at }}</td>
                                        <td>
                                            @if ($request->estatus == config('system.du.requests.statuses.created'))
                                                <a href="{{ route('du.update.request', $request->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                                            @else
                                                <a href="{{ route('du.detail.request', $request->id) }}" class="btn btn-sm btn-secondary"><i class="fas fa-eye"></i></a>
                                            @endif
                                            {{-- <a target="_blank" href="{{ route('du.print.caratula.request', $request->id) }}" class="btn btn-info"><i class="fa fa-print"></i></a> --}}
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                @endcan 
                @can('write_du_verificacion')
                <div class="tab-pane fade" id="verif" role="tabpanel" aria-labelledby="location-tab">
                    <br>
                    <table style="width: 100%;" class="table dataTable table-sm table-hover">
                        <thead>
                            <tr class="search-field" data-section="verif">
                                <th scope="col" style="width:10%;"><input type="text" name="cuenta" style="font-size: 13px" value="{{ $search['cuenta'] }}" placeholder="Solicitud" class="form-control input-search" /></th>
                                <th scope="col"><input type="text" name="contribuyente" style="font-size: 13px" value="{{ $search['contribuyente'] }}" placeholder="Nombre Contribuyente" class="form-control input-search" /></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th class="btn-search-table" style="width:5%;"><button type="button" class="btn btn-block btn-success btn-submit"><i class="fas fa-search"></i></button></th>
                            </tr>
                            <tr>
                                <th scope="col" style="width:10%;">#</th>
                                <th scope="col">Nombre Contribuyente</th>
                                <th scope="col">Tramite</th>
                                <th scope="col">Fecha Alta</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Última actualización</th>
                                <th scope="col" style="width:5%;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody class="small-font">
                            @if(count($requests) > 0)
                                @foreach ($requests->where("estatus", '>=', 1) as $request)
                                    <tr>
                                        <td>UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}</td>
                                        <td>{{ $request->taxpayer->nombreCompleto() }}</td>
                                        <td>
                                            <a href="javascript:;" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#requestProceduresModal" data-id="{{ $request->id }}" data-number="UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}"><i class="far fa-file-alt"></i> Tramites</a>
                                        </td>
                                        <td>{{ $request->created_at->format('Y-m-d') }}</td>
                                        <td>{{ config('system.du.requests.statuses.labels.'.$request->estatus) }}</td>
                                        <td>{{ $request->updated_at }}</td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="{{ route('du.verify.request', $request->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                                                @if ($request->estatus == config('system.du.requests.statuses.verification'))
                                                    <button type="button" class="btn btn-warning btn-sm ml-1" title="Asignar Inspector" data-toggle="modal" data-target="#requestAsignToInspectionModal" data-id="{{ $request->id }}" data-number="UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}"><i class="fas fa-binoculars"></i></button>
                                                    <button type="button" class="btn btn-info btn-sm ml-1" title="Asignar Dictaminador" data-toggle="modal" data-target="#requestAsignToDictaminationModal" data-id="{{ $request->id }}" data-number="UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}"><i class="fas fa-check-circle"></i></button>
                                                @endif
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                @endcan 
                @can('write_du_inspeccion')
                <div class="tab-pane fade" id="inspec" role="tabpanel" aria-labelledby="location-tab">
                    <br>
                    <table style="width: 100%;" class="table dataTable table-sm table-hover">
                        <thead>
                            <tr class="search-field" data-section="inspec">
                                <th scope="col" style="width:10%;"><input type="text" name="cuenta" style="font-size: 13px" value="{{ $search['cuenta'] }}" placeholder="Solicitud" class="form-control input-search" /></th>
                                <th scope="col"><input type="text" name="contribuyente" style="font-size: 13px" value="{{ $search['contribuyente'] }}" placeholder="Nombre Contribuyente" class="form-control input-search" /></th>
                                <th scope="col" colspan="5"></th>
                                <th class="btn-search-table" style="width:5%;"><button type="button" class="btn btn-block btn-success btn-submit"><i class="fas fa-search"></i></button></th>
                            </tr>
                            <tr>
                                <th scope="col" style="width:10%;">#</th>
                                <th scope="col">Nombre Contribuyente</th>
                                <th scope="col">Tramite</th>
                                <th scope="col">Fecha Alta</th>
                                <th scope="col">Asignado</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Última actualización</th>
                                <th scope="col" style="width:5%;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody class="small-font">
                            @if(count($requests) > 0)
                                @foreach ($requests->where("estatus", '>=', 2) as $request)
                                    <tr>
                                        <td>UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}</td>
                                        <td>{{ $request->taxpayer->nombreCompleto() }}</td>
                                        <td><a href="javascript:;" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#requestProceduresModal" data-id="{{ $request->id }}" data-number="UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}"><i class="far fa-file-alt"></i> Tramites</a></td>
                                        <td>{{ $request->created_at->format('Y-m-d') }}</td>
                                        <td>{{ !empty($request->currentInspector())? $request->currentInspector()->name : 'Sin inspector' }}</td>
                                        <td>{{ config('system.du.requests.statuses.labels.'.$request->estatus) }}</td>
                                        <td>{{ $request->updated_at }}</td>
                                        <td><a href="{{ route('du.inspect.request', $request->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a></td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                @endcan 
                @can('write_du_dictaminacion')
                <div class="tab-pane fade" id="dicta" role="tabpanel" aria-labelledby="location-tab">
                    <br>
                    <table style="width: 100%;" class="table dataTable table-sm table-hover">
                        <thead>
                            <tr class="search-field" data-section="dicta">
                                <th scope="col" style="width:10%;"><input type="text" name="cuenta" style="font-size: 13px" value="{{ $search['cuenta'] }}" placeholder="Solicitud" class="form-control input-search" /></th>
                                <th scope="col"><input type="text" name="contribuyente" style="font-size: 13px" value="{{ $search['contribuyente'] }}" placeholder="Nombre Contribuyente" class="form-control input-search" /></th>
                                <th scope="col" colspan="5"></th>
                                <th class="btn-search-table" style="width:5%;"><button type="button" class="btn btn-block btn-success btn-submit"><i class="fas fa-search"></i></button></th>
                            </tr>
                            <tr>
                                <th scope="col" style="width:10%;">#</th>
                                <th scope="col">Nombre Contribuyente</th>
                                <th scope="col">Tramite</th>
                                <th scope="col">Fecha Alta</th>
                                <th scope="col">Asignado</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Última actualización</th>
                                <th scope="col" style="width:5%;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody class="small-font">
                            @if(count($requests) > 0)
                                @foreach ($requests->where("estatus", '>=', 3) as $request)
                                    <tr>
                                        <td>UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}</td>
                                        <td>{{ $request->taxpayer->nombreCompleto() }}</td>
                                        <td><a href="javascript:;" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#requestProceduresModal" data-id="{{ $request->id }}" data-number="UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}"><i class="far fa-file-alt"></i> Tramites</a></td>
                                        <td>{{ $request->created_at->format('Y-m-d') }}</td>
                                        <td>{{ !empty($request->currentDictaminator())? $request->currentDictaminator()->name : 'Sin dictaminador' }}</td>
                                        <td>{{ config('system.du.requests.statuses.labels.'.$request->estatus) }}</td>
                                        <td>{{ $request->updated_at }}</td>
                                        <td><a href="{{ route('du.dictamination.request', $request->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a></td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                @endcan 
                {{-- @can('write_du_confirmacion')
                <div class="tab-pane fade" id="confir" role="tabpanel" aria-labelledby="location-tab">
                    <table style="width: 100%;" class="table dataTable table-sm table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><input type="text" name="cuenta" style="font-size: 13px" value="{{ (isset(session('inputs')['cuenta'])) ? session('inputs')['cuenta'] : "" }}" placeholder="Solicitud" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="contribuyente" style="font-size: 13px" value="{{ (isset(session('inputs')['contribuyente'])) ? session('inputs')['contribuyente'] : "" }}" placeholder="Nombre Contribuyente" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="nombre" value="{{ (isset(session('inputs')['nombre'])) ? session('inputs')['nombre'] : "" }}" placeholder="Nombre Negocio" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="fecha_alta" value="{{ (isset(session('inputs')['fecha_alta'])) ? session('inputs')['fecha_alta'] : "" }}" placeholder="Fecha de Alta" class="form-control custom_input" /></th>
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-submit"><i class="fas fa-search"></i></button></th>
                            </tr>
                            <tr>
                                <th scope="col" style="width:5%;">#</th>
                                <th scope="col">Nombre Contribuyente</th>
                                <th scope="col">Tramite</th>
                                <th scope="col">Fecha Alta</th>
                                <th scope="col" style="width:5%;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody class="small-font">
                            @if(count($requests) > 0)
                                @foreach ($requests->whereIn("estatus", [4, 5]) as $request)
                                    <tr>
                                        <td>UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}</td>
                                        <td>{{ $request->taxpayer->nombreCompleto() }}</td>
                                        <td><a href="javascript:;" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#requestProceduresModal" data-id="{{ $request->id }}" data-number="UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}"><i class="far fa-file-alt"></i> Tramites</a></td>
                                        <td>{{ $request->created_at }}</td>
                                        <td align="center"><a href="{{ route('du.confirmation.request', $request->id) }}" class="btn btn-primary "><i class="fas @if($request->estatus == 5) fa-check @else fa-edit @endif"></i></a></td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                @endcan  --}}
                {{-- @can('write_du_concluidas')
                <div class="tab-pane fade" id="concluidas" role="tabpanel" aria-labelledby="location-tab">
                    <br>
                    <table style="width: 100%;" class="table dataTable table-sm table-hover">
                        <thead>
                            <tr class="search-field" data-section="concluidas">
                                <th scope="col" style="width:10%;"><input type="text" name="cuenta" style="font-size: 13px" value="{{ $search['cuenta'] }}" placeholder="Solicitud" class="form-control input-search" /></th>
                                <th scope="col"><input type="text" name="contribuyente" style="font-size: 13px" value="{{ $search['contribuyente'] }}" placeholder="Nombre Contribuyente" class="form-control input-search" /></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                                <th class="btn-search-table" style="width:5%;"><button type="button" class="btn btn-block btn-success btn-submit"><i class="fas fa-search"></i></button></th>
                            </tr>
                            <tr>
                                <th scope="col" style="width:10%;">#</th>
                                <th scope="col">Nombre Contribuyente</th>
                                <th scope="col">Tramite</th>
                                <th scope="col">Fecha Alta</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Última actualización</th>
                                <th scope="col">Fecha confirmado/Rechazado</th>
                                <th scope="col" style="width:5%;">Acciones</th>
                            </tr>
                        </thead>
                        <tbody class="small-font">
                            @if(count($requests) > 0)
                                @foreach ($requests->whereIn("estatus", [8, 9]) as $request)
                                    <tr>
                                        <td>UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}</td>
                                        <td>{{ $request->taxpayer->nombreCompleto() }}</td>
                                        <td><a href="javascript:;" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#requestProceduresModal" data-id="{{ $request->id }}" data-number="UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}"><i class="far fa-file-alt"></i> Tramites</a></td>
                                        <td>{{ $request->created_at->format('Y-m-d') }}</td>
                                        <td>{{ config('system.du.requests.statuses.labels.'.$request->estatus) }}</td>
                                        <td>{{ $request->updated_at }}</td>
                                        <td>{{ $request->fecha_confirmacion }}</td>
                                        <td><a href="#" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a></td>
                                    </tr>
                                @endforeach
                            @endif 
                        </tbody>
                    </table>
                </div>
                @endcan --}}
            </div>
        </div>
    </div>
@endsection

@section('modals')
<!-- Request Procedures Modal -->
<div class="modal fade" id="requestProceduresModal" tabindex="-1" aria-labelledby="requestProceduresModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requestProceduresModalLabel">Solicitud: <span class="request-number"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"><i class="fas fa-spinner fa-pulse"></i></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="requestAsignToInspectionModal" tabindex="-1" aria-labelledby="requestAsignToInspectionModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requestAsignToInspectionModalLabel">Solicitud: <span class="request-number"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-load">
                <div class="modal-body"><i class="fas fa-spinner fa-pulse"></i></div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="requestAsignToDictaminationModal" tabindex="-1" aria-labelledby="requestAsignToDictaminationModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="requestAsignToDictaminationModalLabel">Solicitud: <span class="request-number"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-load">
                <div class="modal-body"><i class="fas fa-spinner fa-pulse"></i></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script>
        $(function(){
            var tabSection = "{{ $search['section'] }}";

            if (tabSection != '')
            {
                $('#myTab a[href="#'+tabSection+'"]').tab('show');
            }

            $("#du").addClass('active');
            // $("[name='persona']").bootstrapSwitch();
            // $("[name='update_persona']").bootstrapSwitch();
            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'Bfrtip',
                bFilter: false,
                "lengthMenu": [[15, 30, 50, -1], [15, 30, 50, "All"]],
                buttons: [
                    // 'excelHtml5',
                    // 'pdfHtml5'
                ],
                "order": [[ 0, "desc" ]]
            });

            $('input[name="fecha_alta"]').daterangepicker({
                autoUpdateInput: false,
                opens: 'left',
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aceptar",
                    "cancelLabel": "Limpiar",
                    "fromLabel": "De",
                    "toLabel": "A",
                    "customRangeLabel": "Personalizado",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agusto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                }
            });
            $('input[name="fecha_alta"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('input[name="fecha_alta"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            $(document).on("click", ".btn-submit", function(){
                var trSearchFields = $(this).parents('.search-field'),
                    section = trSearchFields.data('section'),
                    searchForm = $('#searchForm');

                searchForm.find('.input-search').each(function(index, element) {
                    $(this).remove();
                });

                trSearchFields.find('.input-search').each(function(index, element) {
                    searchForm.append($(this));
                });

                searchForm.find('.input-search').each(function(index, element) {
                    $(this).attr('type', 'hidden');
                });

                searchForm.find('[name="searching"]').val(1);
                searchForm.find('[name="section"]').val(section);
                searchForm.submit();
            });
            
            $(document).on("keypress", ".custom_input", function(e){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $( ".custom_input" ).each(function( index, element ) {
                        $("#form-data").append($(this));
                    });
                    $("#form-data")[0].submit();  
                }
            });

            $('#requestProceduresModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    requestId = button.data('id'),
                    requetNumber = button.data('number'),
                    modal = $(this),
                    token = "{{ csrf_token() }}";

                modal.find('.request-number').text(requetNumber);
                modal.find('.modal-body').load("{{ route('du.requests.procedures') }}", { _token: token, requestId: requestId});
            });

            $('#requestProceduresModal').on('hidden.bs.modal', function (event) {
                var modal = $(this);

                modal.find('.modal-body').html('<i class="fas fa-spinner fa-pulse"></i>');
            });

            $('#requestAsignToInspectionModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    requestId = button.data('id'),
                    requetNumber = button.data('number'),
                    modal = $(this),
                    token = "{{ csrf_token() }}";

                modal.find('.request-number').text(requetNumber);
                modal.find('.modal-load').load("{{ route('du.request.asign.inspectors') }}", { _token: token, requestId: requestId });
            });

            $('#requestAsignToInspectionModal').on('hidden.bs.modal', function (event) {
                var modal = $(this);

                modal.find('.modal-load').html('<div class="modal-body"><i class="fas fa-spinner fa-pulse"></i></div>');
            });

            $('#requestAsignToDictaminationModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    requestId = button.data('id'),
                    requetNumber = button.data('number'),
                    modal = $(this),
                    token = "{{ csrf_token() }}";

                modal.find('.request-number').text(requetNumber);
                modal.find('.modal-load').load("{{ route('du.request.asign.dictaminators') }}", { _token: token, requestId: requestId });
            });

            $('#requestAsignToDictaminationModal').on('hidden.bs.modal', function (event) {
                var modal = $(this);

                modal.find('.modal-load').html('<div class="modal-body"><i class="fas fa-spinner fa-pulse"></i></div>');
            });
        });
    </script>
@endsection