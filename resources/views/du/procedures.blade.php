@extends('layouts.index')

@section('title') Tramites @endsection

@section('css')
    
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Tramites</h3>
        </div>
    </div>
    @can('write_du_formalities')
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('du.add.process') }}" class="btn btn-primary mr-3"><i class="fas fa-plus mr-2"></i> Nuevo Tramite</a>
        </div>
    @endcan
    <div class="panel-body @can('write_du_formalities') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive custam-table">
                <table class="table table-sm table-hover dataTable">
                    <thead>
                        <tr>
                            <th width="5%" scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th width="10%" scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($procedures as $process)
                            <tr>
                                <td>{{ $process->id }}</td>
                                <td>{{ $process->nombre }}</td>
                                <td align="center"><a href="{{ route('du.update.process', $process->id) }}" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $("#du").addClass('active');
            var table = $('.dataTable').DataTable({
                pageLength: 15,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'Bfrtip',
                buttons: [
                {
                        extend: 'excel',
                        text: 'Excel',
                        className: 'btn btn-default',
                },
                {
                        extend: 'pdf',
                        text: 'PDF',
                        className: 'btn btn-default',
                }]
            });
        });
    </script>
@endsection