@extends('layouts.index')

@section('title') Nuevo Tramite @endsection

@section('css')
    
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Nuevo Tramite</h3>
    </div>
</div>
<div class="panel-body">
    <div class="margin-fix panel-row-fluid">
        <form action="{{ route('du.store.process') }}" method="POST">
            @csrf
            <div class="row margin-10 justify-content-center padding-15">
                <fieldset class="col-xl-12 col-sm-12 fieldset pb-3">
                    <legend class="legend">Información General</legend>
                    <div class="form-row">
                        <div class="col-12">
                            <label for="name">Nombre</label>
                            <input type="text"  class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" value="{{ old("name") }}" >
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="row margin-10 justify-content-center padding-15">
                <fieldset class="col-xl-12 col-sm-12 fieldset ">
                    @if ($errors->has('validate_requirement'))
                        <div class="alert alert-danger" role="alert">
                            Seleccione almenos un requisito para el tramite.
                        </div>
                    @endif
                    <legend class="legend">Requisitos</legend>
                    <table class="table table-sm table-striped @if ($errors->has('validate_requirement')) table-invalid @endif">
                        <thead>
                            <tr>
                                <th>Documento</th>
                                <th class="text-center">Alta</th>
                            </tr>
                        </thead>
                        <tbody id="requiremts-container"></tbody>
                    </table>
                    <div class="row justify-content-end" style="margin-bottom: 5px;">
                        <a href="#!" class="btn btn-primary mr-3 add-requirement"><i class="fas fa-plus mr-2"></i> Nuevo Requisito</a>
                    </div>
                </fieldset>
            </div>
            <div class="row margin-10 justify-content-center ">
                <button type="submit" class="btn btn-primary mr-2 mb-2 mt-2"><i class="fas fa-save mr-2"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $("#du").addClass('active');
            $(document).on("click", ".add-requirement", function(){
                $("#requiremts-container").append('<tr>\
                    <td colspan="2"><input type="text" class="form-control" name="requirement[]"/></td>\
                </tr>');
            });
        });
    </script>
@endsection