@extends('layouts.index')

@section('title') Nueva Solicitud @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />

@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Nueva Solicitud</h3>
    </div>
</div>
<div class="panel-body @can('write_du_requests') with-buttons @endcan">
    <div class="margin-fix panel-row-fluid">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-link active" id="period-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Ubicación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="data-tab" data-toggle="tab" href="#data" role="tab" aria-controls="data" aria-selected="true">Detalles</a>
            </li>
            {{-- <li class="nav-item">
                <a class="nav-link btn-link" id="period-tab" data-toggle="tab" href="#turn" role="tab" aria-controls="turn" aria-selected="false">Detalles</a>
            </li> --}}
        </ul>
        <form id="form-data" action="{{ route('du.store.request') }}" method="POST" class="tab-content">
            @csrf
            
            <div class="tab-pane fade show active" id="location" role="tabpanel" aria-labelledby="location-tab">
                <div class="form-group mt-1 mb-1 row margin-10 ">
                    <label for="city" class="col-sm-2 col-form-label">Poblacion <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <select name="city" id="city" class="required select2 form-control {{ $errors->has('city') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Población</option>
                            @foreach ($poblaciones as $poblacion)
                                <option value="{{ $poblacion->IdPoblacion }}" data-city="{{ $poblacion->NombrePoblacion }}">{{ $poblacion->NombrePoblacion }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10 ">
                    <label for="colony" class="col-sm-2 col-form-label">Colonia <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="colony" data-class="required" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir otro"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            <select name="colony" id="colony" data-required="true" class="required select2 form-control {{ $errors->has('colony') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Colonia</option>
                            </select>
                            @if ($errors->has('colony'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('colony') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="domicilio2" class="col-sm-2 col-form-label">Calle <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="domicilio2" data-class="required" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir otro"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            <select name="domicilio2" id="domicilio2" data-required="true" class="required select2 form-control {{ $errors->has('domicilio2') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Calle</option>
                            </select>
                            @if ($errors->has('domicilio2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('domicilio2') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="form-group mt-1 mb-1 row margin-10">
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="calle1_u" class="col-sm-2 col-form-label">Entre Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="calle1_u" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir otro"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            <select name="calle1_u" id="calle1_u" class="select2 form-control {{ $errors->has('calle1_u') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle1_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle1_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="ext" class="col-sm-2 col-form-label">Exterior <span class="text-danger">*</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="required form-control {{ $errors->has('ext') ? ' is-invalid' : '' }}" id="ext" name="ext">
                        @if ($errors->has('ext'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ext') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="calle2_u" class="col-sm-2 col-form-label">Y Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="calle2_u" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir otro"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            <select name="calle2_u" id="calle2_u" class="select2 form-control {{ $errors->has('calle2_u') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle2_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle2_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="int" class="col-sm-2 col-form-label">Interior</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control {{ $errors->has('int') ? ' is-invalid' : '' }}" id="int" name="int">
                        @if ($errors->has('int'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('int') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="este" class="col-sm-2 col-form-label">Coordenada Este</label>
                    <div class="col-sm-4">
                        <input readonly value="0" type="number" class="required form-control" id="este" name="este">
                    </div>
                    <label for="norte" class="col-sm-2 col-form-label">Coordenada Norte</label>
                    <div class="col-sm-4">
                        <input readonly value="0" type="number" class="required form-control" id="norte" name="norte">
                    </div>
                </div>
                <div id="map" class="map" style="width:100%; height:500px;"></div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="submit" class="btn btn-primary mr-2   btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

            <div class="tab-pane fade " id="data" role="tabpanel" aria-labelledby="data-tab">
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Solicitante</legend>

                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="nombre">Nombre(s) <span class="text-danger">*</span></label>
                            <input maxlength="100" type="text" class="required form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre') }}" >
                            @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="apellido_pat">Apellido Paterno <span class="text-danger">*</span></label>
                            <input maxlength="50" type="text" class="required form-control {{ $errors->has('apellido_pat') ? ' is-invalid' : '' }}" id="apellido_pat" name="apellido_pat" value="{{ old('apellido_pat') }}" >
                            @if ($errors->has('apellido_pat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellido_pat') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="apellido_mat">Apellido Materno <span class="text-danger">*</span></label>
                            <input maxlength="50" type="text" class="required form-control {{ $errors->has('apellido_mat') ? ' is-invalid' : '' }}" id="apellido_mat" name="apellido_mat" value="{{ old('apellido_mat') }}">
                            @if ($errors->has('apellido_mat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellido_mat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="email">Correo</label>
                            <input type="email" maxlength="50" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" >
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="telefono">Telefono <span class="text-danger">*</span></label>
                            <input type="number" maxlength="15" class="required form-control {{ $errors->has('telefono') ? ' is-invalid' : '' }}" id="telefono" name="telefono" value="{{ old('telefono') }}" >
                            @if ($errors->has('telefono'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="edad">Edad</label>
                            <input type="number" maxlength="15" class="form-control {{ $errors->has('edad') ? ' is-invalid' : '' }}" id="edad" name="edad" value="{{ old('edad') }}" >
                            @if ($errors->has('edad'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('edad') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="sexo">Sexo <span class="text-danger">*</span></label>
                            <select name="sexo" id="sexo" class="form-control {{ $errors->has('sexo') ? ' is-invalid' : '' }}">
                                <option value="I">Seleccione Sexo</option>
                                <option value="I">Indefinido</option>
                                <option value="M">Hombre</option>
                                <option value="F">Mujer</option>
                            </select>
                            @if ($errors->has('sexo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('sexo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="rfc">R.F.C</label>
                            <input maxlength="13" type="text" class="form-control {{ $errors->has('rfc') ? ' is-invalid' : '' }}" id="rfc" name="rfc" value="{{ old('rfc') }}" placeholder="XAXX010101000">
                            @if ($errors->has('rfc'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rfc') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="persona">¿ Persona Moral ?</label>
                            <input type="checkbox" data-toggle="toggle"  data-on="Si" data-off="No" class="form-control " id="persona" name="persona">
                            @if ($errors->has('persona'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('persona') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row inputs-hidden" style="display: none">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="razon">Razon Social</label>
                            <input type="text" class="form-control {{ $errors->has('razon') ? ' is-invalid' : '' }}" id="razon" name="razon" value="{{ old('razon') }}" >
                            @if ($errors->has('razon'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('razon') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="telefono_trab">Telefono Empresa</label>
                            <input type="number" maxlength="15" class="form-control {{ $errors->has('telefono_trab') ? ' is-invalid' : '' }}" id="telefono_trab" name="telefono_trab" value="{{ old('telefono_trab') }}" >
                            @if ($errors->has('telefono_trab'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono_trab') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="rfc_trab">R.F.C Empresa</label>
                            <input maxlength="15" type="text" class="form-control {{ $errors->has('rfc_trab') ? ' is-invalid' : '' }}" id="rfc_trab" name="rfc_trab" value="{{ old('rfc_trab') }}" placeholder="XAXX010101000">
                            @if ($errors->has('rfc_trab'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rfc_trab') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="gestor">Gestor</label>
                            <input maxlength="255" type="text" class="form-control {{ $errors->has('gestor') ? ' is-invalid' : '' }}" id="gestor" name="gestor" value="{{ old('gestor') }}" >
                            @if ($errors->has('gestor'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('gestor') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <br />
                </fieldset>
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Solicitud</legend>
                    <div class="form-group mt-1 mb-1 row margin-10">
                        <label for="clave" class="col-sm-2 col-form-label">Clave Catastral <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <input type="text" class="required form-control {{ $errors->has('clave') ? ' is-invalid' : '' }}" id="clave" name="clave">
                            @if ($errors->has('clave'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('clave') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group mt-1 mb-1 row margin-10">
                        <label for="observations" class="col-sm-2 col-form-label">Observaciones</label>
                        <div class="col-sm-10">
                            <textarea class="form-control {{ $errors->has('observations') ? ' is-invalid' : '' }}" id="observations" name="observations" rows="4">{{ old('observations') }}</textarea>
                            @if ($errors->has('observations'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('observations') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div><br />
                </fieldset>
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Tramites</legend>
                    <div class="form-group mt-1 mb-1 row margin-10">
                        <label for="" class="col-sm-2 col-form-label">Seleccione uno o más tramites <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="process_selector" id="process_selector" class="select2 form-control {{ $errors->has('process_selector') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione Trámite</option>
                                @foreach ($procedures as $process)
                                    <option value="{{ $process->id }}" data-process="{{ $process->nombre }}">{{ $process->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <button type="button" class="btn btn-outline-secondary add-process"><i class="fas fa-plus"></i> Agregar</button>
                        </div>
                        @if ($errors->has('process'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('process') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="col-sm-8">
                            <table class="table table-sm table-procedures">
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <br />
                </fieldset>
            </div>

            {{-- ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}

            {{-- <div class="tab-pane fade" id="turn" role="tabpanel" aria-labelledby="turn-tab">
                
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Requisitos</legend>
                    <div id="requiremets-container">
                        <table class="table table-sm table-hover dataTable" style="margin: 1.5em 0px !important;">
                            <thead>
                                <tr>
                                    <th>Nombre del Requisito</th>
                                    <th width="10%">Estatus</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody id="turns-content">
                                
                            </tbody>
                        </table>
                    </div>
                </fieldset>
            </div> --}}
        </form>
    </div>
</div>
@can('write_du_requests')
    <div class="row justify-content-end panel-buttoms">
        <button type="button" class="btn btn-primary mr-3 btn-next " id="btn-previous" style="display:none;" data-target="#data">Anterior <i class="fas fa-arrow-left ml-2"></i></button>
        <button type="button" class="btn btn-primary mr-3 btn-next" id="btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
        <button type="submit" class="btn btn-primary mr-3" id="btn-save-changes" style="display:none;"><i class="fas fa-save mr-2"></i> Guardar Cambios</button>
    </div>
@endcan
@endsection

@section('modals')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('system.google.maps.apiKey') }}&libraries=places&v=weekly"
        defer
    ></script>

    <script>
        var processSelected = 0;

        $(function(){
            $("#du").addClass('active');
            // $('#update_persona').bootstrapToggle('on')
            var created = false;
            var map = L.map('map',{scrollWheelZoom:true}).setView([20.74689, -105.39425], 12);
            // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            //     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap Saul Moncivais</a> contributors'
            // }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:Vuelo_Bajo_Ortofoto@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 12,
                maxZoom: 23,
                attribution: '&copy; IMPLAN - Ortofoto Bahia de Banderas 2019'
            }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:vialidad_BB@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);

            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:lim_loc_final@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);
            var theMarker = {};
            map.on('click',function(e){
                if (theMarker != undefined) {
                    map.removeLayer(theMarker);
                };
                theMarker = L.marker(e.latlng).addTo(map);
                $("#este").val(theMarker.getLatLng().utm().x)
                $("#norte").val(theMarker.getLatLng().utm().y)
            });
            
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var target = e.target.hash,
                    flag = true;
                switch (e.relatedTarget.hash) {
                    case "#data":
                        $("#data").find(".required").each(function() {
                            if(!$(this).val()){
                                flag = false;
                                $(this).addClass("is-invalid");
                                $(this).parent().find(".select2").addClass("is-invalid");
                            }else{
                                if($(this).hasClass("is-invalid")){
                                    $(this).removeClass("is-invalid");
                                    $(this).parent().find(".select2").removeClass("is-invalid");
                                }
                            }
                        });
                        if(!flag)
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor complete los datos del solicitante para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }else{
                            $("#btn-save-changes").fadeOut("fast");
                            $("#btn-next").fadeIn("fast");
                        }
                        break;
                
                    case "#location":
                        $("#location").find(".required").each(function() {
                            if(!$(this).val()){
                                flag = false;
                                $(this).addClass("is-invalid");
                                $(this).parent().find(".select2").addClass("is-invalid");
                            }else{
                                if($(this).hasClass("is-invalid")){
                                    $(this).removeClass("is-invalid");
                                    $(this).parent().find(".select2").removeClass("is-invalid");
                                }
                            }
                        });
                        
                        if(!flag)
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione la ubicación para continuar y/o Verifique que la dirección sea correcta.!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        
                        break;
                }
                if(flag)
                {
                    switch (e.target.hash) {
                        case "#location":
                            $("#btn-previous").fadeOut("fast")
                            $("#btn-next").data("target", "#data")
                            break;
                        case "#data":
                            $("#btn-previous").fadeIn("fast", function(){
                                $("#btn-previous").data("target", "#location")
                            });
                            $("#btn-next").fadeOut("fast", function(){
                                $("#btn-save-changes").fadeIn("fast");
                            });
                            break;
                    }
                }
            });

            $(document).on("click", "#btn-save-changes", function(){
                let flag = true;
                $("#data").find(".required").each(function() {
                    if(!$(this).val() || $(this).val().length == 0){
                        flag = false;
                        $(this).addClass("is-invalid");
                        $(this).parent().find(".select2").addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                            $(this).parent().find(".select2").removeClass("is-invalid");
                        }
                    }
                });
                
                if(flag){
                    if (processSelected > 0)
                    {
                        $.confirm({
                            title: 'Confirmación!',
                            content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción genera una nueva solicitud.</small>',
                            type: "blue",
                            buttons: {
                                Aceptar: function () {
                                    $('#form-data')[0].submit();
                                },
                                Cancelar: function () {
                                },
                            }
                        });
                    }else
                    {
                        $.alert({
                            title: 'Alerta!',
                            content: 'Por favor Verifique que haya agregado algún tramite a su solicitud!',
                            type: "red",
                            buttons: {
                                Aceptar:{
                                    text: "Aceptar",
                                }
                            }
                        });
                    }
                }else{
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor Verifique que la información sea correcta.!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                }
            });

            $(document).on("click", ".btn-next", function(){
                $('#myTab a[href="'+$(this).data("target")+'"]').tab('show');
            });

            $(document).on("change", "#city", function(){
                var id = $("#city").val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.colonies.by.city') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $("#colony").empty();
                    $("#colony").append("<option value=''>Seleccione una Colonia</option>");
                    result.forEach(element => {
                        $("#colony").append($('<option>', {
                            value: element.IdColonia,
                            text: element.NombreColonia
                        }));
                    });
                });
            });
            $(document).on("change", "#colony", function(){
                var id = $("#city").val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.calles.by.colony') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $("#domicilio2").empty();
                    $("#domicilio2").append("<option value=''>Seleccione una Calle</option>");

                    $("#calle1_u").empty();
                    $("#calle1_u").append("<option value=''>Seleccione una Entre Calle</option>");

                    $("#calle2_u").empty();
                    $("#calle2_u").append("<option value=''>Seleccione una Entre Calle</option>");

                    result.forEach(element => {
                        $("#domicilio2").append($('<option>', {
                            value: element.IdCalle,
                            text: element.NombreCalle
                        }));

                        $("#calle1_u").append($('<option>', {
                            value: element.IdCalle,
                            text: element.NombreCalle
                        }));

                        $("#calle2_u").append($('<option>', {
                            value: element.IdCalle,
                            text: element.NombreCalle
                        }));
                    });
                });
            });
            
            $(document).on("click", ".btn-other", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);

                parent.find("span.select2").fadeOut("slow", function(){
                    parent.append('<input type="text" class="form-control inputInserted '+_this.data("class")+'" id="new'+_this.data("name")+'" name="new'+_this.data("name")+'" placeholder="Escriba el nombre de la Calle"> ').show('slow');
                    _this.html('<i class="fas fa-minus-circle"></i>');
                    _this.removeClass("btn-other");
                    _this.addClass("remove-calle");
                    _this.attr("data-original-title", "Seleccionar").tooltip('show');
                });

                parent.find('select[data-required="true"]').removeClass('required');
            });

            $(document).on("click", ".remove-calle", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);

                parent.find(".inputInserted").fadeOut("slow", function(){
                    $(this).remove();
                    parent.find("span.select2").fadeIn("slow")
                    _this.html('<i class="fas fa-plus-circle"></i>')
                    _this.removeClass("remove-calle");
                    _this.addClass("btn-other");
                    _this.attr("data-original-title", "Añadir otro").tooltip('show');
                });

                parent.find('select[data-required="true"]').addClass('required');
            });

            $(document).on("change", "#persona", function(){
                if($(this).is(":checked")){
                    $(".inputs-hidden").find("#razon").addClass("required");
                    $(".inputs-hidden").find("#telefono_trab").addClass("required");
                    $(".inputs-hidden").find("#rfc_trab").addClass("required");
                    $(".inputs-hidden").fadeIn("slow");
                }
                else{
                    $(".inputs-hidden").find("#razon").removeClass("required");
                    $(".inputs-hidden").find("#telefono_trab").removeClass("required");
                    $(".inputs-hidden").find("#rfc_trab").removeClass("required");
                    $(".inputs-hidden").fadeOut("slow");
                }
            });
            
            $(document).on("change", "#process", function(){
                var id = $(this).val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.requeriments.by.process') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    console.log(result);
                });
            });

            /*
                Validaciones para los campos de dirección
            */
            $(document).on('change', '#colony', function() {
                $('#newcolony').val("");
            });

            $(document).on('change', '#newcolony', function() {
                $('#colony').val("").trigger('change.select2');
            });

            $(document).on('change', '#domicilio2', function() {
                $('#newdomicilio2').val("");
            });

            $(document).on('change', '#newdomicilio2', function() {
                $('#domicilio2').val("").trigger('change.select2');
            });

            $(document).on('change', '#calle1_u', function() {
                $('#newcalle1_u').val("");
            });

            $(document).on('change', '#newcalle1_u', function() {
                $('#calle1_u').val("").trigger('change.select2');
            });

            $(document).on('change', '#calle2_u', function() {
                $('#newcalle2_u').val("");
            });

            $(document).on('change', '#newcalle2_u', function() {
                $('#calle2_u').val("").trigger('change.select2');
            });

            /*
                Actualizar marcador del mapa
            */
            $(document).on("keyup", '#ext', function(event){
                if(event.keyCode == 13){
                    getLocationFromAddress()
                }
            });

            $(document).on('change', '#city, #colony, #newcolony, #domicilio2, #newdomicilio2, #ext', function() {
                getLocationFromAddress();
            });

            function getLocationFromAddress(){
                var address = "";

                if($("#domicilio2 option:selected").text().length > 2 && $("#domicilio2").val() != "" && $("#domicilio2").val() != 0){
                    address = address + $("#domicilio2 option:selected").text();
                }else if ($("#newdomicilio2").val() != "")
                {
                    address = address +" "+ $("#newdomicilio2").val();
                }

                if($("#ext").val() != ""){
                    address = address +" "+ $("#ext").val();
                }

                // if($("#colony option:selected").text().length > 2 && $("#colony").val() != "" && $("#colony option:selected").text() != "BAHIA DE BANDERAS" && $("#city option:selected").text() !="Seleccione una Colonia" ){
                //     address = address + ", "+ $("#colony option:selected").text();
                // }else if ($("#newcolony").val() != "")
                // {
                //     address = address +" "+ $("#newcolony").val();
                // } 

                if($("#city option:selected").text().length > 2 && $("#city").val() != "" && $("#city option:selected").text() != "BAHIA DE BANDERAS" && $("#city option:selected").text() != "Seleccione una Colonia"){
                    address = address + ", "+ $("#city option:selected").text();
                }

                if(address != ""){
                    var address = address + ", Bahía de banderas";
                    console.log('Geo: '+address);

                    var geocoder = new google.maps.Geocoder();
                    var geocoderRequest = { address: address, componentRestrictions: {
                            country: "MX",
                            } };
                    geocoder.geocode(geocoderRequest, function(results, status){
                        window.geome = results;
                        if(results.length > 0){
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            };
                            
                            theMarker = L.marker({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}).addTo(map);
                            $("#este").val(theMarker.getLatLng().utm().x);
                            $("#norte").val(theMarker.getLatLng().utm().y);

                            map.setView({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}, 16);
                            
                        }else
                        {
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            }; 

                            $("#este").val(0);
                            $("#norte").val(0);

                            map.setView([20.74689, -105.39425], 12);
                        }
                    });
                }
            }

            $(document).on('click', '.add-process', function() {
                var processSelector = $('[name="process_selector"]'),
                    processId = processSelector.val(),
                    processName = processSelector.find(':selected').data('process'),
                    tableProcedures = $('.table-procedures');

                if (processId != '')
                {
                    console.log(processId, processName);
                    var processRow = '<tr class="process-row" data-id="'+processId+'"> \
                                                <td> \
                                                    <div class="d-flex justify-content-between"> \
                                                        <span class="process-name"><i class="far fa-file-alt"></i> '+processName+'</span> <button type="button" class="btn btn-outline-danger btn-sm remove-process"><i class="fas fa-trash-alt"></i></button> \
                                                        <input type="hidden" name="process[]" value="'+processId+'"> \
                                                    </div> \
                                                </td> \
                                            </tr>';

                    tableProcedures.append(processRow);
                    processSelector.find(':selected').remove();
                    processSelector.val(null).trigger('change');

                    processSelected++;
                }
            });

            $(document).on('click', '.remove-process', function() {
                var processRow = $(this).parents('.process-row'),
                    processId = processRow.data('id'),
                    processName = processRow.find('.process-name').text();

                var option = '<option value="'+processId+'" data-process="'+processName+'">'+processName+'</option>';

                $('[name="process_selector"]').append(option);
                $('[name="process_selector"]').trigger('change');
                processRow.remove();

                processSelected--;
            });
        });
    </script>
@endsection