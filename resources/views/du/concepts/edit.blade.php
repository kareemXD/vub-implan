@extends('layouts.index')

@section('title') Conceptos y Artículos @endsection

@section('css')
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Editar Artículo</h3>
    </div>
</div>
<div class="panel-body with-buttons">
    <div class="margin-fix panel-row-fluid">
        <form id="conceptForm" action="{{ route('du.concepts.update') }}" method="POST">
            @csrf
            @method('PUT')
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul class="mb-0">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <input type="hidden" name="conceptId" value="{{ $concept->id }}">
            <div class="form-row">
                <div class="form-group mt-1 mb-1 col-md-2">
                    <label for="articulo">Artículo <span class="text-danger">*</span></label>
                    <input maxlength="100" type="text" class="required form-control {{ $errors->has('articulo') ? ' is-invalid' : '' }}" id="articulo" name="articulo" value="{{ old('articulo', $concept->Articulo) }}" >
                    @if ($errors->has('articulo'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('articulo') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group mt-1 mb-1 col-md-10">
                    <label for="uso">Concepto <span class="text-danger">*</span></label>
                    <input maxlength="50" type="text" class="required form-control {{ $errors->has('uso') ? ' is-invalid' : '' }}" id="uso" name="uso" value="{{ old('uso', $concept->Uso) }}">
                    @if ($errors->has('uso'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('uso') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group mt-1 mb-1 col">
                    <label for="descripcion">Descripción</label>
                    <textarea rows="3" class="form-control {{ $errors->has('descripcion') ? ' is-invalid' : '' }}" id="descripcion" name="descripcion">{{ old('descripcion', $concept->Descripcion) }}</textarea>
                    @if ($errors->has('descripcion'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('descripcion') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-row">
                <div class="form-group mt-1 mb-1 col-md-2">
                    <label for="status">Activo</label>
                    <input autocomplete="off" type="checkbox" data-toggle="toggle" data-on="Si" data-off="No" class="form-control " id="status" name="status" {{ $concept->Status == '1'? 'checked' : '' }}>
                </div>
            </div>
            <br>
            <h4>Folio de pago</h4>
            <hr>
            <div class="form-row">
                <div class="form-group mt-1 mb-1 col-md-6">
                    <label for="concepto">Concepto de Caja <span class="text-danger">*</span></label>
                    <select required name="concepto" id="concepto" class="select2 form-control required">
                        <option value="">Seleccione el concepto</option>
                        @foreach ($ingresosConceptos as $ingresosConcepto)
                            <option data-importe="{{ $ingresosConcepto->Importe }}" value="{{ $ingresosConcepto->Concepto }}">#{{ $ingresosConcepto->Concepto }} - {{ $ingresosConcepto->Descripcion }} - ${{ $ingresosConcepto->Importe }}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('concepto'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('concepto') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group mt-1 mb-1 col-md-2">
                    <label for="import">Importe</label>
                    <input disabled type="text" class="form-control" id="importe" value="{{ number_format(optional($concept->ingresosConcepto)->Importe, 2) }}">
                </div>
                <div class="form-group mt-1 mb-1 col-md-2">
                    <label for="uan">12% U.A.N</label>
                    <input autocomplete="off" type="checkbox" data-toggle="toggle" data-on="Si" data-off="No" class="form-control " id="uan" name="uan" {{ $concept->UAN == '1'? 'checked' : '' }}>
                </div>
                <div class="form-group mt-1 mb-1 col-md-2">
                    <label for="over_cost">0.5% Sobre Costo</label>
                    <input type="checkbox" data-toggle="toggle"  data-on="Si" data-off="No" class="form-control " id="over_cost" name="over_cost" {{ $concept->OverCost == '1'? 'checked' : '' }}>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="row justify-content-end panel-buttoms">
    <button type="button" class="btn btn-primary mr-3" id="btn-save-changes"><i class="fas fa-save mr-2"></i> Guardar Cambios</button>
</div>
@endsection

@section('modals')
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
<script>
    $(function(){
        var concepto = "{{ $concept->Concepto }}";
        $('[name="concepto"]').val(concepto).trigger('change');

        $(document).on("click", "#btn-save-changes", function(){
            let flag = true;
            $("#data").find(".required").each(function() {
                if(!$(this).val() || $(this).val().length == 0){
                    flag = false;
                    $(this).addClass("is-invalid");
                    $(this).parent().find(".select2").addClass("is-invalid");
                }else{
                    if($(this).hasClass("is-invalid")){
                        $(this).removeClass("is-invalid");
                        $(this).parent().find(".select2").removeClass("is-invalid");
                    }
                }
            });
            
            if(flag){
                $.confirm({
                    title: 'Confirmación!',
                    content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción generará un nuevo concepto.</small>',
                    type: "blue",
                    buttons: {
                        Aceptar: function () {
                            $('#conceptForm').submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            }else{
                $.alert({
                    title: 'Alerta!',
                    content: 'Por favor Verifique que la información sea correcta.!',
                    type: "red",
                    buttons: {
                        Aceptar:{
                            text: "Aceptar",
                        }
                    }
                });
            }
        });

        $(document).on('select2:select', '#concepto', function() {
            var importe = $(this).find(':selected').data('importe');

            $('#importe').val(importe);
        });
    });
</script>
@endsection