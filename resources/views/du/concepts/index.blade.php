@extends('layouts.index')

@section('title') Conceptos y Artículos @endsection

@section('css')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Conceptos y Artículos</h3>
        </div>
    </div>
    <div class="row justify-content-end panel-buttoms">
        <a href="{{ route('du.concepts.create') }}" class="btn btn-primary mr-3"><i class="fas fa-plus-circle mr-2"></i> Nuevo Concepto</a>
    </div>
    <div class="panel-body">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <table style="width: 100%;" class="table dataTable table-sm table-hover">
                <thead>
                    <tr>
                        <th scope="col">Artículo</th>
                        <th scope="col">Concepto</th>
                        <th scope="col">Importe</th>
                        <th scope="col">UAN</th>
                        <th scope="col">Sobre Costo</th>
                        <th scope="col" style="width:5%;">Acciones</th>
                    </tr>
                </thead>
                <tbody class="small-font">
                    @foreach ($concepts as $concept)
                        <tr>
                            <td class="{{ $concept->Status == 1? 'table-success' : 'table-danger' }}">{{ $concept->Articulo }}</td>
                            <td>{{ $concept->Uso }}</td>
                            <td>{{ number_format(optional($concept->ingresosConcepto)->Importe, 2) }}</td>
                            <td>
                                @if ($concept->UAN == 1)
                                    <span class="text-success h5">
                                        <i class="far fa-check-circle"></i>
                                    </span>
                                @endif
                            </td>
                            <td>
                                @if ($concept->OverCost == 1)
                                    <span class="text-success h5">
                                        <i class="far fa-check-circle"></i>
                                    </span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ route('du.concepts.edit', $concept->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
<script>
    $(function(){
        $(".dataTable").DataTable({
            language: {
                "decimal": "",
                "emptyTable": "No hay información",
                "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                "infoEmpty": "Mostrando 0 de 0 Entradas",
                "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                "infoPostFix": "",
                "thousands": ",",
                "lengthMenu": "Mostrar _MENU_ Entradas",
                "loadingRecords": "Cargando...",
                "processing": "Procesando...",
                "search": "Buscar:",
                "zeroRecords": "Sin resultados encontrados",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            dom: 'Bfrtip',
            buttons: [
                'excelHtml5',
                'pdfHtml5'
            ],
            "order": [[0, "asc"]],
            "pageLength": 100
        });
    });
</script>
@endsection