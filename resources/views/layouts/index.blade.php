<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('assets/images/logo_morena_icon.png')}}" type="image/x-icon">
    {{-- Fonts --}}
    <link href="https://fonts.googleapis.com/css?family=Bitter" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/all.min.css') }}">
    {{-- End Fonts --}}

    {{-- Styles --}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2-bootstrap/dist/select2-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/jconfirm/jquery-confirm.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/spinner.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/flexs.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dashboard.min.css') }}">
    <link rel="stylesheet" href="{{ mix('assets/css/style.css') }}">
    @yield('css')
    {{-- End Styles --}}
    @include('partials.analytics')
</head>
<body id="page-top">
    {{-- Navbar --}}
    <nav class="navbar navbar-expand static-top">
        <button class="btn btn-link btn-sm btn-responsive order-1 order-sm-0 mr-10" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>
        <a class="navbar-brand mr-1" href="{{ route('dashboard') }}"><img src="{{ asset('assets/images/logo_morena.png') }}" alt=""></a>

        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"></form>
        
        <ul class="navbar-nav ml-auto ml-md-0">
            <?php 
                $user = Auth::user();
                $notifications = $user->getNewsNotifications();
            ?>
            @if(count($notifications) > 0)
                <li class="nav-item dropdown no-arrow mx-1 br-50">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell fa-fw"></i>
                        <span class="badge badge-danger notifications_number">{{ count($notifications) }}+</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                        @foreach($notifications as $notification)
                            <a class="dropdown-item view-notification" href="javascript:;" data-id="{{ $notification->id }}">
                                <div class="dropdown-message small">{{ $notification->contenido }}</div>
                            </a>
                        @endforeach
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item small text-center" href="javascript:;" data-toggle="modal" data-target="#modal-all-notifications">Ver Todas...</a>
                    </div>
                </li>
            @else
                <li class="nav-item dropdown no-arrow mx-1 br-50">
                    <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell fa-fw"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
                        <a class="dropdown-item" href="javascript:;">
                            <div class="dropdown-message small">Ninguna notificación nueva.</div>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item small text-center" href="javascript:;" data-toggle="modal" data-target="#modal-all-notifications">Ver Todas...</a>
                    </div>
                </li>
            @endif
            
            <li class="nav-item dropdown br-50">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Auth::user()->name }} <i class="fas fa-user-circle fa-fw"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#modal-update-profile">Perfil</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Cerrar Sesión</a>
                </div>
            </li>
        </ul>
    </nav>
    {{-- End Navbar --}}

    <div id="wrapper">

        {{-- Sidebar --}}
        <?php
            $user = Auth::user();
            $navs = $user->getMenu();
        ?>
        <ul class="sidebar navbar-nav toggled">
            @foreach ($navs as $nav)
                @if (count($nav["submenu"]) > 0)
                    @if ($nav["name"] == "Reportes")
                        <li class="nav-item dropdown" id="{{ $nav['alias'] }}">
                            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa-fw {{ $nav['icon'] }}"></i>
                                <span>{{ $nav["name"] }}</span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                                @foreach ($nav["submenu"] as $submenu)
                                    @if ($submenu["name"] == "Licencias")
                                        <a class="dropdown-item" href="{{ route($submenu['url']) }}">{{ $submenu["name"] }}</a>
                                    @else
                                        <a class="dropdown-item" target="_BLANK" href="{{ route($submenu['url']) }}">{{ $submenu["name"] }}</a>
                                    @endif
                                @endforeach
                            </div>
                        </li>
                    @else
                        <li class="nav-item dropdown" id="{{ $nav['alias'] }}">
                            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa-fw {{ $nav['icon'] }}"></i>
                                <span>{{ $nav["name"] }}</span>
                            </a>
                            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                                @foreach ($nav["submenu"] as $submenu)
                                    @if(Route::has($submenu['url']))
                                    <a class="dropdown-item" href="{{ route($submenu['url']) }}">{{ $submenu["name"] }}</a>
                                    @endif
                                @endforeach
                            </div>
                        </li>
                    @endif
                @else
                    <li class="nav-item" id="{{ $nav['alias'] }}">
                        <a class="nav-link" href="{{ route($nav['url']) }}">
                            <i class="fa-fw {{ $nav['icon'] }}"></i>
                            <span>{{ $nav["name"] }}</span>
                        </a>
                    </li>
                @endif
            @endforeach       
        </ul>
        {{-- End Sidebar --}}

        <div id="content-wrapper">
            <div class="panels">
                {{-- Content --}}
                @yield('content')
                {{-- End Content --}}
            </div>
            {{-- Footer --}}
            <footer class="sticky-footer">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright © IMPLAN {{ Carbon\Carbon::now('America/Mexico_City')->format('Y') }}</span>
                    </div>
                </div>
            </footer>
            {{-- End Footer --}}
        </div>
    </div>

    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    



    {{-- Scripts --}}
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/dist/js/i18n/es.js') }}"></script>
    <script src="{{ asset('assets/plugins/jconfirm/jquery-confirm.min.js') }}"></script>
    <script src="{{ asset('assets/js/dashboard.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/proj4.js') }}"></script>
    {{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
    <script>
        $(function(){
            /********************* Initialization of Libraries JS  ***********************/
            $(".select2").select2({
                theme: "bootstrap",
                language: "es"
            });
            $('[data-toggle="tooltip"]').tooltip()

            /********************* End of Initialization of Libraries JS  ***********************/
            $(document).on('show.bs.modal', '.modal', function () {
                var zIndex = 1040 + (10 * $('.modal:visible').length);
                $(this).css('z-index', zIndex);
                setTimeout(function() {
                    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                }, 0);
            });
            
            if($(".alert"))
            {
                setTimeout(() => {
                    $(".alert").fadeOut("slow"); 
                }, 10000);
            }

            $(document).on("click", ".view-notification", function(e){
                e.preventDefault();
                var _this = $(this),
                    id = _this.data("id"),
                    token = "{{ csrf_token() }}";

                var arr_not = ($(".notifications_number").text()).split("");
                var notifications_number = parseInt(arr_not[0]) - 1;
                $.ajax({
                    url: "{{ route('update.notification') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    }
                }).done(function(result){
                    var modal = $("#modal-notification");
                    
                    modal.find('#date').val(result.created_at);
                    modal.find('#notification').val(result.contenido);
                    modal.modal("show");
                    if(notifications_number == 0)
                    {
                        _this.text("Ninguna notificación nueva.");
                        _this.removeClass("view-notification");
                        $(".notifications_number").remove();
                    }else
                    {
                        _this.remove();
                        $(".notifications_number").text(notifications_number);
                    }
                });
            });
        })
    </script>
    @yield('js')
    {{-- End Scripts --}}

    {{-- Partials And Modals --}}
    @include('partials.modals.modal_logout')
    @include('partials.modals.modal_notification')
    @include('partials.modals.modal_all_notifications')
    @include('partials.modals.modal_update_profile')
    @yield('modals')
    
    @include('partials.spinner')
    {{-- End Paritals And Modals --}}
    </body>
</html>
