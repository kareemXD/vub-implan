@php setlocale(LC_TIME, 'es'); @endphp

<style>
.black{
    font-weight: bold;
}
.small{
    font-size: 8px;
}

.bg-gray{
    background-color: #bdbdbd;
}

</style> 

<table border="1px" cellpadding="2px">
    <tbody>
        <tr style="font-size:10px;">
            <td class="bg-gray" style="width:25%;">FOLIO</td>
            <td style="width:25%;">UAM-{{ $request->numero_solicitud }}/{{ \Carbon\Carbon::parse($request->created_at)->format('y') }}</td>
            <td class="bg-gray" style="width:20%;">NOMBRE</td>
            <td style="width:30%;" >{{ $request->taxpayer->nombreCompleto() }}</td>
        </tr>
    </tbody>
</table><br><br>

<table border="1px" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray">TRAMITES</td>
        </tr>
        @if ($request->procedures->count() > 0)
            @foreach ($request->procedures as $process)
                <tr>
                    <td>{{ strtoupper($process->nombre) }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td>NO CONTIENE TRAMITES</td>
            </tr>
        @endif
    </tbody>
</table><br><br>

<table border="1px" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray">DOMICILIO</td>
        </tr>
        <tr>
            <td>{{ strtoupper($request->fullAddress()) }}</td>
        </tr>
    </tbody>
</table><br><br>

<table border="1px" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray">FUNDAMENTO(S)</td>
        </tr>
        @if(count($request->payments) > 0)
            @foreach ($request->payments as $payment)
                @php
                    $concept = $payment->concept;
                @endphp
                <tr>
                    <td>({{ $concept->Articulo }}) {{ strtoupper(($concept->Uso == $concept->Descripcion)? $concept->Descripcion :  $concept->Uso." ". $concept->Descripcion) }}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table><br><br>

<table border="1px" cellpadding="2px">
    <tbody>
        <tr style="font-size:9px;border-bottom:1px solid #000;">
            <td class="bg-gray" style="width:58%;">CONCEPTO</td>
            <td class="bg-gray" style="width:12%;">SUBTOTAL</td>
            <td class="bg-gray" style="width:9%;">12% U.A.N</td>
            <td class="bg-gray" style="width:9%;">0.5% SOBRE COSTO</td>
            <td class="bg-gray" style="width:12%;">TOTAL</td>
        </tr>
        @php
            $balance = 0;   
        @endphp
        @if(count($request->payments) > 0)
            @foreach ($request->payments as $payment)
                @if (!empty($payment->ingresosConcepto))
                    @php 
                        $balance += $payment->total;
                        $ingresosConcepto = $payment->ingresosConcepto;
                    @endphp
                    <tr>
                        <td style="width:58%;">{{ $ingresosConcepto->Descripcion }}</td>
                        <td style="width:12%;">{{ "$ ".number_format($payment->subtotal, 2) }}</td>
                        <td style="width:9%;">{{ ($payment->uan == 1)? "$ ".number_format(($payment->subtotal * 0.12), 2): "$ 0.00" }} </td>
                        <td style="width:9%;">{{ ($payment->sobre_costo == 1)? "$ ".number_format(($payment->subtotal * 0.005), 2): "$ 0.00" }} </td>
                        <td style="width:12%;">{{ "$ ".number_format($payment->total, 2) }} </td>
                    </tr>
                @endif
            @endforeach
        @endif
        <tr style="font-size:9px;border-bottom:1px solid #000;">
            <td class="bg-gray" colspan="4">Total</td>
            <td class="bg-gray">${{ number_format($balance, 2) }}</td>
        </tr>
    </tbody>
</table>
