<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>CONSTANCIA DE RECEPCIÓN DE SOLICITUD DE TRAMITE</title>
    <style>
        body {
            font-size: 20px;
            font-family: Arial, Helvetica, sans-serif;
            margin-bottom: 0;
        }
        .logo {
            text-align: center;
        }
        .logo img {
            height: 150px;
        }
        .header-title {
            padding: 1rem;
        }
        .bordered {
            border: 1px solid #000;
        }
        .font-regular {
            font-weight: 400;
        }
        .content {
            margin: 2rem 0;
        }
        table tr td {
            padding: 1rem 0;
        }

        .sign {
            width: 350px;
            border: 1px solid #000;
        }
        .sign .sign-box {
            height: 120px;
        }
        .sign .sign-person {
            border-top: 1px solid #000;
            text-align: center;
        }

        @media print {
            @page { margin: 0; }
            body { margin: 1.6cm; }
        }
    </style>
    <script>
        try{
            window.print();
            window.onafterprint = function(){
                window.close();
            }
        }catch(e){}
    </script>
</head>
<body>
    <div class="container">
        <header class="d-flex justify-content-center mb-3">
            <div class="logo">
                <img src="{{ asset('assets\images\logos\logo-ayuntamineto.png') }}" alt="">
            </div>
            <div class="header-title text-center">
                <b class="h4">XI. AYUNTAMIENTO DE BAHIA DE BANDERAS <br> DIRECCIÓN DE DESARROLLO URBANO Y ECOLOGÍA</b> <br>
                <p class="mt-5 h5 font-regular">CONSTANCIA DE RECEPCIÓN DE SOLICITUD DE TRAMITE</p>
            </div>
        </header>

        <div class="d-flex justify-content-between align-items-start my-5">
            <div class="text-center">
                <b>Nuevo Vallarta, NAY. A</b> <br>
                <p>{{ $duRequest->created_at->format('d / m / Y') }}</p>
            </div>
            <div class="d-flex align-items-center">
                <div class="bordered px-2 py-1">Folio</div>
                <div class="bordered px-2 py-1">UAM-{{ $duRequest->numero_solicitud }}/{{ \Carbon\Carbon::parse($duRequest->created_at)->format('y') }}</div>
            </div>
        </div>

        <div class="content">
            <table class="w-100 mb-5">
                <tr>
                    <td width="30%"><b>TIPO DE TRAMITE(S):</b></td>
                    <td width="70%">{{ $procedures }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>CLAVE CATASTRAL:</b></td>
                    <td width="70%">{{ $duRequest->clave_catastral }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>DOMICILIO:</b></td>
                    <td width="70%">{{ strtoupper($duRequest->fullAddress()) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>TELÉFONO:</b></td>
                    <td width="70%">{{ $duRequest->taxpayer->telefono }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>NOMBRE O RAZÓN SOCIAL:</b></td>
                    <td width="70%">{{ strtoupper($duRequest->taxpayer->nombreCompleto()) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>GESTOR:</b></td>
                    <td width="70%">{{ strtoupper($duRequest->gestor) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>OBSERVACIONES:</b></td>
                    <td width="70%">{{ strtoupper($duRequest->observaciones) }}</td>
                </tr>
            </table>
            <div class="pt-3 pb-5">
                <table class="w-100">
                    <tr>
                        <td>
                            <div class="sign">
                                <div class="sign-box"></div>
                                <div class="sign-person">SOLICITANTE</div>
                            </div>
                        </td>
                        <td width="40%"></td>
                        <td>
                            <div class="sign">
                                <div class="sign-box"></div>
                                <div class="sign-person">RECEPCIONO</div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="text-center pt-2" style="font-size: 16px;">
                <p>Se recibe solo para ser turnado a la autoridad competente, a efecto que de conformidad con la normativa aplicable, se emita el documento solicitado en caso de que corresponda.</p>
                <p><b>Paseo de Los Cocoteros, Centro Empresarial Paradise Village, Loc. 2224, Nvo. Vta., Nay. Tel. 322-297-0306</b></p>
            </div>
        </div>
    </div>


    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
  </body>
</html>