@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .black{
        font-weight: bold;
    }
    .small{
        font-size: 8px;
    }

    .bg-gray{
        background-color: #bdbdbd;
    }
    .text-center {
        text-align: center;
    }
</style> 

<table width="100%">
    <tr>
        <td style="text-align: right; font-size: 9px;">
            N° Oficio: CC/{{ $request->cc }}/{{ $request->created_at->format('Y') }} <br>
            Número de Expediente: {{ !empty($request->expedient)? $request->expedient->no_expediente : '' }} <br>
            Asunto: Carta Compromiso <br>
            Bahía de Banderas, Nayarit. A {{ now()->day.'/'.config('system.months.'.now()->format('n')).'/'.now()->year }}
        </td>
    </tr>
</table>
<table>
    <tbody>
        <tr>
            <td>
                <h4>{{ strtoupper($request->fullName()) }}<br>{{ strtoupper($request->fullAddress()) }}</h4>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </tbody>
</table>
<div class="text-center">
    <b>CARTA COMPROMISO</b>
</div>
<p style="font-size: 9px;">En la localidad de La Jarretadera, Municipio de Bahía de Banderas, Nayarit, en fecha arriba mencionada, en mi carácter de representante legal y/o propietario del establecimiento con giro de</p>
<h4>{{ strtoupper($request->turnsText()) }}</h4>
<p style="font-size: 9px;">a nombre de <b>{{ strtoupper($request->fullName()) }}</b> con domicilio arriba citado, manifiesto que estoy de acuerdo en celebrar la presente <b>CARTA COMPROMISO</b> con el H.X Ayuntamiento de Bahía de Banderas, Nayarit; a través de la Unidad Municipal de ProtecciónCivil; y me comprometo a cumplir con los requisitos de seguridad recomendados por la misma,para dar cumplimiento al artículo 1, 100, 101, 102, 106, 110, 111, 113, 114, 115, del Reglamentode Protección Civil para el Municipio de Bahía de Banderas Nayarit, en un lapso no mayor a 30 treinta días naturales a partir de la fecha de expedición de la presente, ya que dentro de mis obligaciones se encuentra el prevenir y así minimizar riesgos que puedan presentarse en el establecimiento.</p>
<p style="font-size: 9px;">Teniendo en cuenta que, de no cumplir con lo establecido, me hago acreedor a la sanción correspondiente o en su caso, a la cancelación o revocación del permiso otorgado.</p>
<p style="font-size: 9px;">Así mismo entiendo que en la fecha que indiquen se realizará una visita de verificación en el lugar, debiendo cumplir con todo lo que se observe en el Acta de Visita de Verificación de Medidas de Seguridad a Establecimientos Comerciales y de Servicios, para dar cumplimiento en medidas de seguridad.</p>
<p style="font-size: 9px;">Reconozco y acepto los alcances de esta carta compromiso, y en caso de no dar cumplimiento a lo establecido, deslindo de toda responsabilidad al H. X Ayuntamiento y/o cualquier otro departamento involucrado.</p>
<p></p><p></p>
<table>
    <tbody>
        <tr>
            <td class="text-center" style="font-size: 9px;">
                <b>{{ strtoupper($request->fullName()) }}</b> <br>
                Propietario y/o Representante Legal
            </td>
            <td class="text-center" style="font-size: 9px;">
                <b>C. VÍCTOR MANUEL MARTÍNEZ SAMANIEGO</b><br>
                DIRECTOR DE LA UNIDAD MUNICIPAL DE PROTECCIÓN CIVIL
            </td>
        </tr>
    </tbody>
</table>
<br><br><br><br>
<table>
    <tbody>
        <tr>
            <td class="text-center" style="font-size: 9px;">
                <b>{{ strtoupper($request->testigo_1) }}</b><br>
                Testigo
            </td>
            <td class="text-center" style="font-size: 9px;">
                <b>{{ strtoupper($request->testigo_2) }}</b><br>
                Testigo
            </td>
        </tr>
    </tbody>
</table>