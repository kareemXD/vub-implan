@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .black{
        font-weight: bold;
    }
    .small{
        font-size: 8px;
    }

    .bg-gray{
        background-color: #bdbdbd;
    }
    .text-center {
        text-align: center;
    }
</style> 

<table width="100%">
    <tr>
        <td style="text-align: right; font-size: 9px;">
            N° Oficio: UMPC/02/{{ $request->umpc }}/{{ $request->created_at->format('Y') }} <br>
            Número de Expediente: {{ !empty($request->expedient)? $request->expedient->no_expediente : '' }} <br>
            Asunto: Carta de Conformidad de Visto Bueno <br>
            Bahía de Banderas, Nayarit. A {{ now()->day.'/'.config('system.months.'.now()->format('n')).'/'.now()->year }}
        </td>
    </tr>
</table>
<table>
    <tbody>
        <tr>
            <td>
                <h4>{{ strtoupper($request->fullName()) }}<br>{{ strtoupper($request->fullAddress()) }}<br>BAHÍA DE BANDERAS, NAYARIT.</h4>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </tbody>
</table>
<h4>P R E S E N T E.</h4>
<p style="font-size: 9px;">Por medio de la presente, me dirijo a Usted para informarle, que con fundamento en el Artículo 1, 74, 76, 78, 100, 101, 102, 103, 104, 105, 106, 110 , 113 , 114 y 115 del Reglamento de Protección Civil para el Municipio de Bahía de Banderas Nayarit; Articulo 29, de la Reforma y adiciona varias disposiciones al reglamento para establecimientos mercantiles, comerciales, tianguis y prestadores de servicios turístico en el municipio de bahía de banderas, Nayarit; Artículo 53 del Reglamento para establecimientos mercantiles, comerciantes, tianguis y prestadores de servicio turístico en el municipio de bahía de banderas, Nayarit presentado el comprobante de pago de derechos, como lo establece el Artículo 59 de la Ley de Ingresos para la Municipalidad de Bahía de Banderas, Nayarit para el Ejercicio Fiscal {{ now()->year }}, esta autoridad le otorga la <b>CARTA DE CONFORMIDAD DE VISTO BUENO</b> en función del <b>cumplimiento total de medidas de seguridad en materia de Protección Civil y gestión integral de riesgos</b>, de acuerdo a lo establecido en el reglamento de protección civil para el Municipio de Bahía de Banderas Nayarit, así como en la normatividad vigente aplicable en la materia.</p>
<p style="font-size: 9px;">Las modificaciones o adecuaciones a sus instalaciones que no sean notificadas y autorizadas a esta autoridad, invalidarán el presente documento, que se extiende con carácter de inalienable e intransferible, con validez hasta el 31 de diciembre del {{ now()->year }}, y lo ampara única y exclusivamente para el giro comercial de:</p>
<h4>{{ strtoupper($request->turnsText()) }}</h4>
<p style="font-size: 9px;">no eximiéndole de los ordenamientos o restricciones que le sean señalados por otras dependencias que legalmente les corresponda. Los riesgos no evaluados, como consecuencia de omisiones voluntarias o involuntarias durante la operación o funcionamiento del giro, son de su completa responsabilidad. En caso de anomalías o faltas a las medidas de seguridad establecidas para la función de la actividad económica arriba descrita, la CARTA DE CONFORMIDAD DE VISTO BUENO PUEDE SER REVOCADA O SUSPENDIDA.</p>
<p style="font-size: 9px;">Durante la vigencia del presente, el personal adscrito a esta institución podrá realizar visitas de supervisión a sus instalaciones, por lo que les deberá garantizar el acceso para que constaten el cumplimiento de las medidas de seguridad.</p>
<p style="font-size: 9px;">Sin más por el momento me despido de usted remitiendo un cordial saludo, quedando a sus órdenes para cualquier aclaración al respecto.</p>
<div class="text-center">
    <b>ATENTAMENTE</b>
</div>
<br>
<br>
<div class="text-center" style="font-size: 9px;">
    <b>C. VÍCTOR MANUEL MARTÍNEZ SAMANIEGO</b><br>
    DIRECTOR DE LA UNIDAD MUNICIPAL DE PROTECCIÓN CIVIL
</div>