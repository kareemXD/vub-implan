<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <title>CONSTANCIA DE RECEPCIÓN DE SOLICITUD DE TRAMITE</title>
    <style>
        body {
            font-size: 20px;
            font-family: Arial, Helvetica, sans-serif;
            margin-bottom: 0;
        }
        .logo {
            text-align: center;
        }
        .logo img {
            height: 150px;
        }
        .header-title {
            padding: 1rem;
        }
        .bordered {
            border: 1px solid #000;
        }
        .font-regular {
            font-weight: 400;
        }
        .content {
            margin: 2rem 0;
        }
        table tr td {
            padding: 1rem 0;
        }

        .sign {
            width: 350px;
            border: 1px solid #000;
        }
        .sign .sign-box {
            height: 120px;
        }
        .sign .sign-person {
            border-top: 1px solid #000;
            text-align: center;
        }

        @media print {
            @page { margin: 0; }
            body { margin: 1.6cm; }
        }
    </style>
    <script>
        try{
            window.print();
            window.onafterprint = function(){
                window.close();
            }
        }catch(e){}
    </script>
</head>
<body>
    <div class="container">
        <header class="d-flex justify-content-center mb-3">
            <div class="logo">
                <img src="{{ asset('assets\images\logos\logo-ayuntamineto.png') }}" alt="">
            </div>
            <div class="header-title text-center">
                <b class="h4">XI. AYUNTAMIENTO DE BAHIA DE BANDERAS <br> DIRECCIÓN DE PROTECCIÓN CIVIL</b> <br>
                <p class="mt-5 h5 font-regular">CONSTANCIA DE RECEPCIÓN DE SOLICITUD DE TRAMITE</p>
            </div>
        </header>

        <div class="d-flex justify-content-between align-items-start my-5">
            <div class="text-center">
                <b>Jarretaderas, NAY. A</b> <br>
                <p>{{ $request->created_at->format('d / m / Y') }}</p>
            </div>
            <div class="d-flex align-items-center">
                <div class="bordered px-2 py-1">Folio</div>
                <div class="bordered px-2 py-1">{{ $request->requestNumberText() }}</div>
            </div>
        </div>

        <div class="content">
            <table class="w-100 mb-5">
                <tr>
                    <td width="30%"><b>NOMBRE / RAZÓN SOCIAL:</b></td>
                    <td width="70%">{{ strtoupper($request->nombre) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>TIPO DE TRAMITE(S):</b></td>
                    <td width="70%">{{ strtoupper($formalities) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>DOMICILIO:</b></td>
                    <td width="70%">{{ strtoupper($request->fullAddress()) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>TELÉFONO(S):</b></td>
                    <td width="70%">{{ $request->telefono_celular }} {{ !empty($request->telefono_fijo)? ' | '.$request->telefono_fijo : '' }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>NOMBRE COMERCIAL:</b></td>
                    <td width="70%">{{ strtoupper($request->nombre_comercial) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>RFC:</b></td>
                    <td width="70%">{{ strtoupper($request->rfc) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>GIRO:</b></td>
                    <td width="70%">{{ strtoupper($request->turn->Nombre) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>NO. EMPLEADOS:</b></td>
                    <td width="70%">{{ $request->no_empleados }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>SOLICITANTE:</b></td>
                    <td width="70%">{{ strtoupper($request->nombre_solicitante) }}</td>
                </tr>
                <tr>
                    <td width="30%"><b>CARACTER JURIDICO DEL SOLICITANTE:</b></td>
                    <td width="70%">{{ strtoupper($request->caracter_juridico_solicitante) }}</td>
                </tr>
            </table>
            <div class="text-center pt-2" style="font-size: 16px;">
                <p>Se recibe solo para ser turnado a la autoridad competente, a efecto que de conformidad con la normativa aplicable, se emita el documento solicitado en caso de que corresponda.</p>
                <p><b>Carretera federal 200 S/N, Jarretaderas, Nayarit. Cp. 63735  Tel. (322)-113-3255 / 911</b></p>
            </div>
        </div>
    </div>


    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
  </body>
</html>