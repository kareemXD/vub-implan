@php setlocale(LC_TIME, 'es'); @endphp

<style>
.black{
    font-weight: bold;
}
.small{
    font-size: 8px;
}

.bg-blue{
    background-color: #56242A;
    color: #fff;
}

</style> 

<table border="1px" cellpadding="2px">
    <thead>
        <tr class="bg-blue">
            <td class="black" style="width:7%;">#</td>
            <td class="black" style="width:63%;">Nombre</td>
            <td class="black" style="width:10%;">Cuota Fija</td>
            <td class="black" style="width:10%;">Clave Hacienda</td>
            <td class="black" style="width:10%">Concepto</td>
        </tr>
    </thead>
    <tbody>
        @foreach ($turns as $turn)
            <tr>
                <td class="small" style="width:7%;">{{ $turn->IdGiro }}</td>
                <td class="small" style="width:63%;">{{ $turn->Nombre }}</td>
                <td class="small" style="width:10%;">$ {{ $turn->CuotaFija }}</td>
                <td class="small" style="width:10%;">{{ $turn->CveHacienda }}</td>
                <td class="small" style="width:10%;">{{ $turn->Concepto }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

