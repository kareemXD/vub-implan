<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Reporte Licencias y Refrendos - Periodo: {{ $dateStart.' al '.$dateEnd }}</title>
    <style>
        .black{
            font-weight: bold;
        }
        .small{
            font-size: 11px;
        }
        .bg-blue{
            background-color: #56242A;
            color: #fff;
        }
        .gray{
            background-color: #bdbdbd;
        }
    </style> 
</head>
<body>
    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <h2 class="h3">Reporte Licencias y Refrendos</h2>
            <h3 class="h5">Periodo: {{ $dateStart.' al '.$dateEnd }}</h3>
        </div>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active" id="licenses-tab" data-bs-toggle="tab" data-bs-target="#licenses" type="button" role="tab" aria-controls="licenses" aria-selected="true">Licencias</button>
            </li>
            <li class="nav-item" role="presentation">
              <button class="nav-link" id="refrendos-tab" data-bs-toggle="tab" data-bs-target="#refrendos" type="button" role="tab" aria-controls="refrendos" aria-selected="false">Refrendos</button>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="licenses" role="tabpanel" aria-labelledby="licenses-tab">
                <table class="table table-sm table-striped table-hover">
                    <thead class="table-light">
                        <tr>
                            <th class="small gray">#</th>
                            <th class="small gray">No. Licencia</th>
                            <th class="small gray">Contribuyente</th>
                            <th class="small gray">Negocio</th>
                            <th class="small gray">Calle</th>
                            <th class="small gray">Exterior</th>
                            <th class="small gray">Interior</th>
                            <th class="small gray">Colonia</th>
                            <th class="small gray">Poblacion</th>
                            <th class="small gray">Giro</th>
                            <th class="small gray">Importe</th>
                            <th class="small gray">Fecha Pago</th>
                            <th class="small gray">Fecha Alta</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $rowCounter = 0;
                        @endphp
                        @foreach ($licenses['licenses'] as $key => $license)
                            @php
                                $turndetails = $license['license']->getTurnsDetails();
                                $rowCounter++;
                            @endphp
                            <tr>
                                <td class="small">{{ $rowCounter }}</td>
                                <td class="small">{{ $license['license']->NumeroLicencia }}</td>
                                <td class="small">{{ $license['license']->taxpayer->NombreCompleto }}</td>
                                <td class="small">{{ $license['license']->NombreNegocio }}</td>
                                <td class="small">{{ is_null($license['license']->street())? "": $license['license']->street()->NombreCalle }}</td>
                                <td class="small">{{ $license['license']->Exterior }}</td>
                                <td class="small">{{ $license['license']->Interior }}</td>
                                <td class="small">{{ is_null($license['license']->colony())? "": $license['license']->colony()->NombreColonia }}</td>
                                <td class="small">{{ is_null($license['license']->location())? "": $license['license']->location()->NombrePoblacion }}</td>
                                <td class="small">{{ $turndetails['turn']['Nombre'] }}</td>
                                <td class="small">{{ $license['importe'] }}</td>
                                <td class="small">{{ substr($license['fechaPago'], 0, 10) }}</td>
                                <td class="small">{{ substr($license['license']->FechaAlta, 0, 10) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="refrendos" role="tabpanel" aria-labelledby="refrendos-tab">
                <table class="table table-sm table-striped table-hover">
                    <thead class="table-light">
                        <tr>
                            <th class="small gray">#</th>
                            <th class="small gray">No. Licencia</th>
                            <th class="small gray">Contribuyente</th>
                            <th class="small gray">Negocio</th>
                            <th class="small gray">Calle</th>
                            <th class="small gray">Exterior</th>
                            <th class="small gray">Interior</th>
                            <th class="small gray">Colonia</th>
                            <th class="small gray">Poblacion</th>
                            <th class="small gray">Giro</th>
                            <th class="small gray">Importe</th>
                            <th class="small gray">Fecha Pago</th>
                            <th class="small gray">Fecha Alta</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $rowCounter = 0;
                        @endphp
                        @foreach ($licenses['refrendos'] as $key => $license)
                            @php
                                $turndetails = $license['license']->getTurnsDetails();
                                $rowCounter++;
                            @endphp
                            <tr>
                                <td class="small">{{ $rowCounter }}</td>
                                <td class="small">{{ $license['license']->NumeroLicencia }}</td>
                                <td class="small">{{ $license['license']->taxpayer->NombreCompleto }}</td>
                                <td class="small">{{ $license['license']->NombreNegocio }}</td>
                                <td class="small">{{ is_null($license['license']->street())? "": $license['license']->street()->NombreCalle }}</td>
                                <td class="small">{{ $license['license']->Exterior }}</td>
                                <td class="small">{{ $license['license']->Interior }}</td>
                                <td class="small">{{ is_null($license['license']->colony())? "": $license['license']->colony()->NombreColonia }}</td>
                                <td class="small">{{ is_null($license['license']->location())? "": $license['license']->location()->NombrePoblacion }}</td>
                                <td class="small">{{ $turndetails['turn']['Nombre'] }}</td>
                                <td class="small">{{ $license['importe'] }}</td>
                                <td class="small">{{ substr($license['fechaPago'], 0, 10) }}</td>
                                <td class="small">{{ substr($license['license']->FechaAlta, 0, 10) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
  </body>
</html>

