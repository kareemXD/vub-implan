@php setlocale(LC_TIME, 'es'); @endphp
<style>
    .black{
        font-weight: bold;
    }
    .small{
        font-size: 8px;
    }
    
    .bg-blue{
        background-color: #56242A;
        color: #fff;
    }
    
    </style> 
<table  border="1px" cellpadding="2px">
    <thead class="table-light">
        <tr class="bg-blue">
            <th class="black">No. Licencia</th>
            <th class="black">Contribuyente</th>
            <th class="black">Negocio</th>
            <th class="black">Calle</th>
            <th class="black">Exterior</th>
            <th class="black">Interior</th>
            <th class="black">Colonia</th>
            <th class="black">Poblacion</th>
            <th class="black">Giro</th>
            <th class="black">Importe</th>
            <th class="black">Fecha Alta</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($licenses['licencias'] as $license)
            <tr>
                <td class="small">{{ $license['NumeroLicencia'] }}</td>
                <td class="small">{{ $license['Contribuyente'] }}</td>
                <td class="small">{{ $license['NombreNegocio'] }}</td>
                <td class="small">{{ $license['Calle'] }}</td>
                <td class="small">{{ $license['Exterior'] }}</td>
                <td class="small">{{ $license['Interior'] }}</td>
                <td class="small">{{ $license['Colonia'] }}</td>
                <td class="small">{{ $license['Poblacion'] }}</td>
                <td class="small">{{ $license['Giro'] }}</td>
                <td class="small">${{ number_format($license['Importe'], 2) }}</td>
                <td class="small">{{ $license['FechaAlta'] }}</td>
            </tr>
        @endforeach
    </tbody>
</table>