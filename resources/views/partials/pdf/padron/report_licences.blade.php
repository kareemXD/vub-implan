@php setlocale(LC_TIME, 'es'); @endphp

<style>
.black{
    font-weight: bold;
}
.small{
    font-size: 8px;
}

.bg-blue{
    background-color: #56242A;
    color: #fff;
}

</style> 

<table border="1px" cellpadding="2px">
    <tbody>
        @foreach ($data as $mounth)
            <tr>
                <td class="small" rowspan="2">{{ $mounth["nombre"] }}</td>
                <td class="small">Licencias Otorgadas: </td>
                <td class="small">{{ $mounth["licensias"] }}</td>
            </tr>
            <tr>
                <td class="small">Recaudación: </td>
                <td class="small">{{ $mounth["recaudacion"] }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

