@php setlocale(LC_TIME, 'es'); @endphp

<style>
.black{
    font-weight: bold;
}
.small{
    font-size: 8px;
}

.bg-gray{
    background-color: #bdbdbd;
}

</style> 

<table border="1px" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray" style="width:25%;">LICENCIA</td>
            <td style="width:25%;">{{ $license->NumeroLicencia }}</td>
            <td class="bg-gray" style="width:50%;">NOMBRE</td>
        </tr>
        <tr>
            <td class="bg-gray">HORARIO</td>
            @php
                $legends = '';
                foreach ($license->legends as $key => $legend) {
                    $legends .= $legend->NombreLeyenda.", ";
                }
                $legends = substr($legends, 0, -2);
            @endphp
            <td>{{ $legends }}</td>
            <td>{{ $license->taxpayer->Nombre." ".$license->taxpayer->ApellidoPaterno." ".$license->taxpayer->ApellidoMaterno }}</td>
        </tr>
    </tbody>
</table><br><br>

<table border="1px" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray">DESCRIPCIÓN DEL GIRO</td>
            
        </tr>
        <tr>
            @php
                $giros = '';
                $arrayNames = [];
                foreach ($license->turns as $key => $turn) {
                    if(!in_array($turn->Nombre, $arrayNames)){
                        $giros .= $turn->Nombre.", ";
                    }
                    $arrayNames[] = $turn->Nombre;

                }
                $giros = substr($giros, 0, -2);
            @endphp
            <td>{{ $giros }}</td>
        </tr>
    </tbody>
</table><br><br>

<table border="1px" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray">DOMICILIO</td>
            
        </tr>
        <tr>
            <td>{{ $license->getAddress() }}</td>
        </tr>
    </tbody>
</table><br><br>

<table border="1px" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray">OBSERVACIONES</td>
            
        </tr>
        <tr>
            <td>{{ $license->Observaciones }}</td>
        </tr>
    </tbody>
</table><br><br>

<table border="1px" cellpadding="2px">
    <tbody>
        <tr style="font-size:9px;">
            <td class="bg-gray" style="width:6%;">Año</td>
            <td class="bg-gray" style="width:56%;">Descripción</td>
            <td class="bg-gray" style="width:10%;">Importe</td>
            <td class="bg-gray" style="width:9%;">% UAN</td>
            <td class="bg-gray" style="width:9%;">% Basura</td>
            <td class="bg-gray" style="width:10%;">Subtotal</td>
        </tr>
        @php
            $importes = 0;
            $alcohol = 0;
            $totalBasura = 0;
            $totalAlcohol = 0;
            $basura = 0;
            $total = 0;
        @endphp
        @foreach ($license->debtsN() as $debt)
            @php
                $reference = substr($debt->Referencia, 0, 4);

                //en caso de que sea nulo no da el giro
                if((strpos(strtoupper($debt->Descripcion), "BASURA") !== false) || strpos(strtoupper($debt->Descripcion), "RECOLECCION Y TRASLADO") !== false)
                {
                    if ((int)$reference >= 2022)
                    {
                        $basura = $debt->Importe * 0.15;
                    }else
                    {
                        $basura = $debt->Importe * 0.12;
                    }
                }else
                {
                    $turn = \DB::connection("sqlsrv")->table("Giros")->where("Nombre", $debt->Descripcion)->first();
                    $concept = \DB::connection('sqlsrv')->table('Conceptos')->where('Concepto', $debt->Concepto)->first();

                    $addUAN = false;
                    $wordsToSearch = ['ANUNC', 'PENDON', 'DIFUSION', 'PERIFONEO', 'VOLANTEO'];
                    if (!empty($concept))
                    {
                        foreach ($wordsToSearch as $word)
                        {
                            if ((strpos(strtoupper($concept->Descripcion), $word) !== false))
                            {
                                $addUAN = true;
                            }
                        }
                    }

                    if($debt->Referencia2 == "Alcohol" || @$turn->Alcohol==true || $addUAN === true)
                    {
                        // Cobrar UAN a todo los giros que tengan alcoholes
                        if ((int)$reference >= 2022)
                        {
                            $alcohol = $debt->Importe * 0.15;
                        }else
                        {
                            $alcohol = $debt->Importe * 0.12;
                        }
                    }
                }

                $importes += $debt->Importe;
                $totalBasura += $basura;
                $totalAlcohol += $alcohol;
                $subtotal = $debt->Importe + $alcohol + $basura;
                $total += $subtotal;
            @endphp
            <tr style="font-size: 9px;">
                <td>{{ $reference }}</td>
                <td>{{ $debt->Descripcion }}</td>
                <td>{{ number_format($debt->Importe,2) }}</td>
                <td>{{ number_format($alcohol, 2) }}</td>
                <td>{{ number_format($basura, 2) }}</td>
                <td>{{ number_format($subtotal, 2) }}</td>
            </tr>
            @php
                $basura = 0;
                $alcohol = 0;
            @endphp
        @endforeach
        <tr style="font-size: 9px;">
            <td class="bg-gray" colspan="2">Totales</td>
            <td class="bg-gray">{{ number_format($importes, 2) }}</td>
            <td class="bg-gray">{{ number_format($totalAlcohol, 2) }}</td>
            <td class="bg-gray">{{ number_format($totalBasura, 2) }}</td>
            <td class="bg-gray">{{ number_format($total, 2) }}</td>
        </tr>
    </tbody>
</table>

