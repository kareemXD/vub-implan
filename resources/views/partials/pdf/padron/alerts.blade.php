@php setlocale(LC_TIME, 'es'); @endphp

<style>
.black{
    font-weight: bold;
}
.small{
    font-size: 8px;
}

.bg-blue{
    background-color: #56242A;
    color: #fff;
}

</style> 

<table width="100%" border="1px" cellpadding="2px">
    <thead>
        <tr class="bg-blue">
            <th scope="col">#</th>
            <th scope="col">Alerta</th>
            <th scope="col"># Licencia</th>
            <th scope="col">Desde </th>
            <th scope="col">Para</th>
            <th scope="col">Fecha Alta</th>
        </tr>
    </thead>
    <tbody>
        @foreach($alerts as $index => $alerta)
        <tr>
            <td>{{ $alerta->IdAlerta }}</td>
            <td>{{ $alerta->Alerta }}</td>
            <td>{{ $alerta->NumeroLicencia }}</td>
            <td>{{ $alerta->De }}</td>
            <td>{{ $alerta->Para }}</td>
            <td>{{ $alerta->FechaAlta }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

