@php setlocale(LC_TIME, 'es'); @endphp

<style>
.black{
    font-weight: bold;
}
.small{
    font-size: 8px;
}

.bg-blue{
    background-color: #56242A;
    color: #fff;
}

</style> 

<table border="1px" cellpadding="2px">
    <thead>
        <tr class="bg-blue">
            <td class="black" style="width:7%;">#</td>
            <td class="black" style="width:10%;">No. Contribuyente</td>
            <td class="black" style="width:63%;">Nombre Contribuyente</td>
            <td class="black" style="width:10%;">Nombre Negocio</td>
            <td class="black" style="width:10%">Fecha Alta</td>
        </tr>
    </thead>
    <tbody>
        
        @foreach ($licenses as $license)
            <tr>
                <td class="small" style="width:7%;">{{ $license->NumeroLicencia }}</td>
                <td class="small" style="width:10%;">{{ $license->IdContribuyente }}</td>
                <td class="small" style="width:63%;">{{ $license->taxpayer->NombreCompleto }}</td>
                <td class="small" style="width:10%;">{{ $license->NombreNegocio }}</td>
                <td class="small" style="width:10%;">{{ $license->FechaAlta }}</td>
            </tr>
            
        @endforeach
    </tbody>
</table>

