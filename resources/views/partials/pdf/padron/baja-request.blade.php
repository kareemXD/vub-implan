@php setlocale(LC_TIME, 'es-MX'); @endphp

<style>
.black{
    font-weight: bold;
}
.small{
    font-size: 8px;
}

.bg-gray{
    background-color: #bdbdbd;
}

</style> 
<br>
<br>
<table border="none" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray" style="width:25%;">LICENCIA</td>
            <td style="width:25%;">{{ $license->NumeroLicencia }}</td>
            <td class="bg-gray" style="width:50%;">Contribuyente</td>
        </tr>
        <tr>
            <td class="bg-gray">CAUSA BAJA</td>
            <td>{{ $license->Comentario }}</td>
            <td>{{ $license->taxpayer->NombreCompleto }}</td>
        </tr>
    </tbody>
</table><br><br>

<table border="none" cellpadding="2px">
    <tbody>
        <tr>
            <td style="width:25%" class="bg-gray">FECHA SOLICITUD</td>
            <td style="width:25%" class="">{{ date("Y-m-d", strtotime($license->FechaSolicitud)) }}</td>
            <td style="width:25%" class="bg-gray">FECHA TRAMITE</td>
            <td style="width:25%" class="">{{ date("Y-m-d", strtotime($license->FechaTramite))  }}</td>
        </tr>
        <tr>
            <td style="width:25%" class="bg-gray">FECHA ALTA</td>
            <td style="width:25%" class="">{{ date("Y-m-d", strtotime($license->FechaAlta))  }}</td>
            <td style="width:25%" class="bg-gray">FECHA BAJA</td>
            <td style="width:25%" class="">{{ date("Y-m-d", strtotime($license->FechaBaja))  }}</td>
        </tr>
    </tbody>
</table><br><br>

<table>
    
</table>
<br><br>

<table border="none" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray">DESCRIPCIÓN DEL GIRO</td>
            
        </tr>
        <tr>
            @php
                $giros = '';
                $arrayNames = [];
                if (!empty($request))
                {
                    foreach ($request->turns as $key => $turn) {
                        if(!in_array($turn->Nombre, $arrayNames)){
                            $giros .= $turn->Nombre.", ";
                        }
                        $arrayNames[] = $turn->Nombre;
    
                    }
                }
                $giros = substr($giros, 0, -2);
            @endphp
            <td>{{ $giros }}</td>
        </tr>
        
    </tbody>
</table><br><br>

<table border="none" cellpadding="2px">
    <tbody>
        <tr>
            <td class="bg-gray">DOMICILIO</td>
            
        </tr>
        <tr>
            <td>{{ $license->getAddress() }}</td>
        </tr>
    </tbody>
</table>
<div style="margin-top:50%;width:100%;display:block;">
    <p style="text-align: center;font-size:13px;">Baja de licencia</p>
    
    <p style="text-align: center;font-size:12px;">Se  solicita con fecha de hoy sea dada de baja del Padrón de Giros, la Licencia arriba mencionada con su respectiva descripción tales como Contribuyente, Domicilio, Giro y Causas de Baja.</p>
    <br>
    <br>
    @php
        Carbon\Carbon::setLocale('es_MX');
        $fecha = Carbon\Carbon::now("America/Mexico_city")->formatLocalized(" %d de %B de %Y") ;
        $fecha = mb_strtoupper(utf8_encode($fecha), "utf-8");
    @endphp
    <p style="text-align: rigth;font-size:12px;">BAHIA DE BANDERAS, NAYARIT A  {{ $fecha }} </p></p>
    <br>
    <br>
    <br>
    <p>________________________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________________________________________</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SOLICITANTE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; POR EL AYUNTAMIENTO  </p>
    <br>
    <p style="text-align:justify;font-size:12px;form-weight:bold"><b>NOTA: Esta solicitud no procederá mientras no se demuestre que el giro en cuestión se encuentre libre de todo gravamen municipal. </b></p>
</div>

