@php setlocale(LC_TIME, 'es'); @endphp

<style>
.black{
    font-weight: bold;
}
.small{
    font-size: 8px;
}

.bg-blue{
    background-color: #56242A;
    color: #fff;
}

.gray{
    background-color: #bdbdbd;
}

</style> 

@foreach ($licenses as $license)
    @if(count($license->turns) > 0)
        <table border="1px" cellpadding="2px">
            <tbody>
                <tr>
                    <td class="small gray" style="width:12%;">No. Licencia</td>
                    <td class="small gray" style="width:12%;">No. Contribuyente</td>
                    <td class="small gray" style="width:50%;">Nombre Contribuyente</td>
                    <td class="small gray" style="width:13%;">Nombre Negocio</td>
                    <td class="small gray" style="width:13%;">Fecha Alta</td>
                </tr>
                <tr>
                    <td class="small" >{{ $license->NumeroLicencia }}</td>
                    <td class="small" >{{ $license->IdContribuyente }}</td>
                    <td class="small" >{{ $license->taxpayer->NombreCompleto }}</td>
                    <td class="small" >{{ $license->NombreNegocio }}</td>
                    <td class="small" >{{ $license->FechaAlta }}</td>
                </tr>
            </tbody>
        </table>
        <table border="1px" cellpadding="2px">
            <tbody>
                <tr>
                    <th class="small gray" width="10%">IdGiro</th>
                    <th class="small gray" width="30%">Descripción</th>
                    <th class="small gray" width="30%">Observaciones</th>
                    <th class="small gray" width="10%">Fecha</th>
                    <th class="small gray" width="10%">Cuota</th>
                    <th class="small gray" width="10%">Cantidad</th>
                </tr>
                @php
                    $turndetails = $license->getTurnsDetails();
                @endphp
                @foreach ($turndetails["data"] as $turn)
                    <tr>
                        <td class="small">{{ $turn['id'] }}</td>
                        <td class="small">{{ $turn['nombre'] }}</td>
                        <td class="small">{{ $turn['observation'] }}</td>
                        <td class="small">{{ $turn['date'] }}</td>
                        <td class="small">$ {{ $turn['cost'] }}</td>
                        <td class="small">{{ $turn['quantity'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table><br /><br />
    @endif
@endforeach

