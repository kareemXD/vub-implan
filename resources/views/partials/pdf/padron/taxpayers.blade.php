@php setlocale(LC_TIME, 'es'); @endphp

<style>
.black{
    font-weight: bold;
}
.small{
    font-size: 8px;
}

.bg-blue{
    background-color: #56242A;
    color: #fff;
}

</style> 

<table border="1px" cellpadding="2px">
    <thead>
        <tr class="bg-blue">
            <td class="black" style="width:7%;">Cuenta</td>
            <td class="black" style="width:19%;">Nombre</td>
            <td class="black" style="width:19%;">Apellido Paterno</td>
            <td class="black" style="width:19%;">Apellido Materno</td>
            <td class="black" style="width:10%;">R.F.C</td>
            <td class="black" style="width:14%;">Colonia</td>
            <td class="black" style="width:12%;">Fecha de Alta</td>
        </tr>
    </thead>
    <tbody>
        @php
            $chunk = $taxpayers->chunk(500);
        @endphp
        @foreach ($chunk as $taxpayers)
            @foreach ($taxpayers as $taxpayer)
                <tr>
                    <td class="small" style="width:7%;">{{ $taxpayer->IdContribuyente }}</td>
                    <td class="small" style="width:19%;">{{ $taxpayer->Nombre }}</td>
                    <td class="small" style="width:19%;">{{ $taxpayer->ApellidoPaterno }}</td>
                    <td class="small" style="width:19%;">{{ $taxpayer->ApellidoMaterno }}</td>
                    <td class="small" style="width:10%;">{{ $taxpayer->RFC }}</td>
                    <td class="small" style="width:14%;">{{ $taxpayer->Colonia }}</td>
                    <td class="small" style="width:12%;">{{ $taxpayer->FechaAlta }}</td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>

