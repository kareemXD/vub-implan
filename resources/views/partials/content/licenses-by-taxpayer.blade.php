<br>
<h2>Otras Licencias <small>({{ count($licenses) }})</small></h2>

@if (count($licenses) > 0)
<div class="col">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col" style="width:8%;">#</th>
                <th scope="col">No. Contribuyente</th>
                <th scope="col">Nombre Contribuyente</th>
                <th scope="col">Nombre Negocio</th>
                <th scope="col">Fecha Alta</th>
                {{--nuevos encabezados--}}
                <th scope="col">Calle</th>
                <th scope="col">Exterior</th>
                <th scope="col">Colonia</th>
                <th scope="col">Poblacion</th>
                <th scope="col">Fecha Trámite</th>
                <th scope="col">Giro(s)</th>
                {{--fin nuevos encabezados--}}
                <th scope="col">Acciones</th>
            </tr>
        </thead>
        <tbody class="small-font">
            @foreach ($licenses as $license)
                <tr>
                    <td>{{ $license->NumeroLicencia }}</td>
                    <td>{{ $license->IdContribuyente }}</td>
                    <td>{{ $license->taxpayer->NombreCompleto }}</td>
                    <td>{{ $license->NombreNegocio }}</td>
                    <td>{{ $license->FechaAlta }}</td>
                    {{--nuevos encabezados--}}
                    <td scope="col">{{ is_null($license->street())? "": $license->street()->NombreCalle }}</td>
                    <td scope="col">{{ $license->Exterior }}</td>
                    <td scope="col">{{ is_null($license->colony())? "": $license->colony()->NombreColonia }}</td>
                    <td scope="col">{{ is_null($license->location())? "": $license->location()->NombrePoblacion }}</td>
                    <td scope="col">{{ ($license->FechaTramite < '2000-01-01')? "":  date("Y-m-d", strtotime($license->FechaTramite)) }}</td>
                    <td>{{ implode( ", ", $license->turns()->pluck("Nombre")->toArray()) }}</td>
                    {{--fin nuevos encabezados--}}
                    <td align="center">
                        <a href="{{ route('update.license', $license->NumeroLicencia) }}" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@else
<hr>
<p>Este contribuyente no cuenta con otras licencias.</p>
@endif