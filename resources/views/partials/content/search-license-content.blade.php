<!-- tabs -->
<div class="col">
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
        <a class="nav-link active" id="adeudos-tab" data-toggle="tab" href="#adeudos-content" role="tab" aria-controls="adeudos-content" aria-selected="true">Adeudos</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" id="pagos-tab" data-toggle="tab" href="#pagos-content" role="tab" aria-controls="pagos-content" aria-selected="false">Pagos</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" id="giros-tab" data-toggle="tab" href="#giros-content" role="tab" aria-controls="giros-content" aria-selected="false">Giros</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" id="solicitudes-tab" data-toggle="tab" href="#solicitudes-content" role="tab" aria-controls="solicitudes-content" aria-selected="false">Solicitudes</a>
        </li>
    </ul>
    
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="adeudos-content" role="tabpanel" aria-labelledby="adeudos-tab">
            <table class="table">
                <thead>
                    <tr>
                        <th>Acción</th>
                        <th>Departamento</th>
                        <th>Concepto</th>
                        <th>Cuenta Depto</th>
                        <th>Referencia</th>
                        <th>Fecha Movto</th>
                        <th>Licencia</th>
                        <th>Fecha Vence</th>
                        <th>Descripcion</th>
                        <th>Importe</th>
                        <th>Estado</th>
                        <th>Cargo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($license->adeudos as $adeudo)
                    <tr>
                        <td><button class="btn btn-primary"><i class="fa fa-edit"></i></button></td>
                        <td>{{ $adeudo->Departamento }}</td>
                        <td>{{ $adeudo->Concepto }}</td>
                        <td>{{ $adeudo->CuentaDepto }}</td>
                        <td>{{ $adeudo->Referencia }}</td>
                        <td>{{ date("Y-m-d", strtotime($adeudo->FechaMovto)) }}</td>
                        <td>{{ $license->NumeroLicencia }}</td>
                        <td>{{ date("Y-m-d", strtotime($adeudo->FechaVence)) }}</td>
                        <td>{{ $adeudo->Descripcion }}</td>
                        <td>${{ number_format($adeudo->Importe, 2) }}</td>
                        <td>{{ $adeudo->Estado }}</td>
                        <td>{{ $adeudo->Cargo }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="tab-pane" id="pagos-content" role="tabpanel" aria-labelledby="pagos-tab">
            <table class="table">
                <thead>
                    <tr>
                        <th>Departamento</th>
                        <th>Concepto</th>
                        <th>Cuenta Depto</th>
                        <th>Referencia</th>
                        <th>Fecha Movto</th>
                        <th>Licencia</th>
                        <th>Fecha Vence</th>
                        <th>Fecha Pago</th>
                        <th>Descripcion</th>
                        <th>Importe</th>
                        <th>Estado</th>
                        <th>Cargo</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($license->Pagos as $pago)
                    <tr>
                        <td>{{ $pago->Departamento }}</td>
                        <td>{{ $pago->Concepto }}</td>
                        <td>{{ $pago->CuentaDepto }}</td>
                        <td>{{ $pago->Referencia }}</td>
                        <td>{{ date("Y-m-d", strtotime($pago->FechaMovto)) }}</td>
                        <td>{{ $license->NumeroLicencia }}</td>
                        <td>{{ date("Y-m-d", strtotime($pago->FechaVence)) }}</td>
                        <td>{{ date("Y-m-d", strtotime($pago->FechaPago)) }}</td>
                        <td>{{ $pago->Descripcion }}</td>
                        <td>${{ number_format($pago->Importe, 2) }}</td>
                        <td>{{ $pago->Estado }}</td>
                        <td>{{ $pago->Cargo }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="tab-pane" id="giros-content" role="tabpanel" aria-labelledby="giros-tab">
            <table class="table">
                <thead>
                    <tr>
                        <th>IdGiro</th>
                        <th>SubTipo Giro</th>
                        <th>Tipo Giro</th>
                        <th>Nombre</th>
                        <th>Cantidad</th>
                        <th>Cve Hacienda</th>
                        <th>Cuota Fija</th>
                        <th>Concepto</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($license->turns as $turn)
                        <tr>
                            <td>{{ $turn->IdGiro }}</td>
                            <td>{{ $turn->SubTipoGiro }}</td>
                            <td>{{ $turn->TipoGiro }}</td>
                            <td>{{ $turn->Nombre }}</td>
                            <td>{{ floatval($turn->pivot->Cantidad) }}</td>
                            <td>{{ $turn->CveHacienda }}</td>
                            <td>{{ $turn->CuotaFija }}</td>
                            <td>{{ $turn->Concepto }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="tab-pane" id="solicitudes-content" role="tabpanel" aria-labelledby="solicitudes-tab">
            <table class="table">
                <thead>
                    <tr>
                        <th>No. Solicitud</th>
                        <th>Tipo Tramite</th>
                        <th width="10%">Estatus</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody id="turns-content">
                    @foreach ($license->requests as $request)
                        <tr>
                            <td width="30%">{{ $request->NumeroSolicitud }}</td>
                            <td width="30%">
                            @foreach ($request->process as $process)
                                {{ $process->Nombre . ", " }}
                            @endforeach
                            </td>
                            <td width="10%" class="{{ ($request->Aplicado == 1)? 'bg-green' : 'bg-yellow' }}">{{ ($request->Aplicado == 1)? 'Aplicado' : 'En Tramite' }}</td>
                            <td width="5%">
                                <a href="{{ route('update.request', [$request->NumeroSolicitud, "update"]) }}" class="btn btn-primary btn-action-table" data-toggle="tooltip" data-placement="top" title="Ver Solicitud"><i class="fas fa-eye"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        {{--bottom panel---------------------------}}
        {{--fin bottom panel-----------------------}}
    </div>
</div>
