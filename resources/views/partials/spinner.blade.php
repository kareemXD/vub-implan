
<div class="wrapper-spinner-initial">
    <div class="wrapper-spinner">
        <div class='triangles'>
            <div class='tri invert'></div>
            <div class='tri invert'></div>
            <div class='tri'></div>
            <div class='tri invert'></div>
            <div class='tri invert'></div>
            <div class='tri'></div>
            <div class='tri invert'></div>
            <div class='tri'></div>
            <div class='tri invert'></div>
        </div>
        <div class="spinner-text">
            <h4>Cargando...</h4>
        </div>
    </div>
</div>

<script type="text/javascript">
	window.addEventListener("beforeunload", function (event) {
		$('.wrapper-spinner-initial').fadeIn('fast');
	});
    
    window.addEventListener('load', function () {
        $('.wrapper-spinner-initial').fadeOut('fast');
    });
</script>