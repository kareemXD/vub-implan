{{-- Modal Logout --}}
<div class="modal fade" id="modal_update_location" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form id="form-update-location" method="POST" action="#" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="operation" value="update">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Localidad</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="actualizar_nombre_poblacion">Nombre Población</label>
                            <input  autocomplete="off" value="{{ old('actualizar_nombre_poblacion') }}" name="actualizar_nombre_poblacion" maxlength="100" type="text" class="form-control @error('actualizar_nombre_poblacion') is-invalid @enderror" id="actualizar_nombre_poblacion">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="actualizar_nombre_corto">Nombre Corto</label>
                            <input  autocomplete="off" value="{{ old('actualizar_nombre_corto') }}" name="actualizar_nombre_corto" maxlength="100" type="text" class="form-control @error('actualizar_nombre_corto') is-invalid @enderror" id="actualizar_nombre_corto">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Actualizar</button>
                </div>
            </div>
        </form>
    </div>
</div>
