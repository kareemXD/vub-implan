{{-- Modal Logout --}}
<div class="modal fade" id="modal-baja-licencia" role="dialog" >
    <div class="modal-dialog" role="document">
        <form id="form-data-document" method="POST" action="{{ route('store.license.baja', $license->NumeroLicencia) }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="solicitud_id" id="solicitud_id" value="{{ !empty($request->NumeroSolicitud)? $request->NumeroSolicitud : '' }}">
            <input type="hidden" name="operation" value="store">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dar de baja Licencia</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="baja-comments">Motivo de Baja</label>
                            <textarea class="form-control" id="baja-comments" name="baja-comments" rows="4"></textarea>
                            @if ($errors->has('baja-comments'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('baja-comments') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cancelar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-times mr-2"></i> Aplicar Baja</button>
                </div>
            </div>
        </form>
    </div>
</div>
