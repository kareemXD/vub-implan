<div class="modal fade" id="modal_create_street" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form id="form-create-street" method="POST" action="{{ route('street.add') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="operation" value="store">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registrar Calle</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="crear_nombre_calle">Nombre Calle</label>
                            <input  autocomplete="off" value="{{ old('crear_nombre_calle') }}" name="crear_nombre_calle" maxlength="100" type="text" class="form-control @error('crear_nombre_calle') is-invalid @enderror" id="crear_nombre_calle">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="crear_alias_calle">Alias </label>
                            <input  autocomplete="off" value="{{ old('crear_alias_calle') }}" name="crear_alias_calle" maxlength="100" type="text" class="form-control @error('crear_alias_calle') is-invalid @enderror" id="crear_alias_calle">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="crear_nombre_oficial">Nombre Oficial</label>
                            <input  autocomplete="off" value="{{ old('crear_nombre_oficial') }}" name="crear_nombre_oficial" maxlength="100" type="text" class="form-control @error('crear_nombre_oficial') is-invalid @enderror" id="crear_nombre_oficial">
                        </div>
                    </div>
                    <div class="form-row">
                        <label for="crear_id_poblacion" >Población</label>
                        <select name="crear_id_poblacion" id="crear_id_poblacion" class="select2 form-control">
                            <option value="">Seleccione una colonia</option>
                            @foreach($locations as $location)
                            <option @if($location->IdPoblacion == $idPoblacion) selected @endif value="{{ $location->IdPoblacion }}">{{ $location->NombrePoblacion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Registrar</button>
                </div>
            </div>
        </form>
    </div>
</div>
