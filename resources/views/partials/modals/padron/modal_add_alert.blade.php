{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-alert" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" action="{{ route('pyl.store.alert') }}">
            @csrf
            <input type="hidden" name="operation" value="store">
            <input type="hidden" name="license" value="{{ $license->NumeroLicencia }}">
            <input type="hidden" name="alert" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nueva Alerta</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="date" class="col-sm-2 col-form-label">Fecha Alta:</label>
                        <div class="col-sm-10">
                            <input type="date"class="form-control" name="date" id="date" value="{{ \Carbon\Carbon::now()->toDateString() }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="from" class="col-sm-2 col-form-label">De:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="from" id="from" maxlength="50">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="to" class="col-sm-2 col-form-label">Para:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly  id="to" value="{{ $license->NombreNegocio }}">
                        </div>
                    </div>
                    @php
                        $giros = '';
                        foreach ($license->turns as $key => $turn) {
                            $giros .= $turn->Nombre.", ";
                        }
                        $giros = substr($giros, 0, -2);
                    @endphp
                    <div class="form-group row">
                        <label for="turns" class="col-sm-2 col-form-label">Tipo Giro:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" readonly id="turns" value="{{ $giros }}">
                        </div>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="tramites" value="1" id="resquests">
                        <label class="form-check-label" for="resquests">
                          ¿ Permitir tramites ?
                        </label>
                    </div>
                    <div class="form-group row mt-2">
                        <label for="description" class="col-sm-2 col-form-label">Alerta:</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" name="description" id="description" maxlength="250"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-between">
                    <div>
                        <button type="button" class="btn btn-danger delete-alert"><i class="fas fa-trash-alt"></i> Elimiar</button>
                    </div>
                    <div>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                        <button type="button" class="btn btn-primary save-alert"><i class="fas fa-user-plus mr-2"></i> Registrar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on('click', '.delete-alert', function() {
            $('#modal-add-alert').find('form [name="operation"]').val('delete');
            $('#modal-add-alert').find('form').submit();
        });

        $(document).on('click', '.save-alert', function() {
            var operation = $(this).attr('operation');
            $('#modal-add-alert').find('form [name="operation"]').val(operation);
            $('#modal-add-alert').find('form').submit();
        });
    });
</script>