{{-- Modal Logout --}}
<div class="modal fade" id="modal-update-taxpayer" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" action="{{ route('store.update.taxpayer') }}">
            @csrf
            <input type="hidden" name="id" value="{{ old('id') }}" id="taxpayer">
            <input type="hidden" name="operation" value="update">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Actualizar Contribuyente</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="cuenta">Cuenta</label>
                            <input @can('read_contribuyentes') disabled @endcan readonly type="number" class="form-control {{ $errors->has('update_cuenta') ? ' is-invalid' : '' }}" id="update_cuenta" name="update_cuenta" value="{{ old('update_cuenta') }}">
                            @if ($errors->has('update_cuenta'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_cuenta') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="fecha_alta">Fecha de Alta</label>
                            <input @can('read_contribuyentes') disabled @endcan readonly type="text" class="form-control {{ $errors->has('fecha_alta') ? ' is-invalid' : '' }}" id="fecha_alta" name="fecha_alta" value="{{ old('fecha_alta') }}">
                            @if ($errors->has('fecha_alta'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('fecha_alta') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="user">Usuario</label>
                            <input @can('read_contribuyentes') disabled @endcan readonly type="text" class="form-control {{ $errors->has('update_user') ? ' is-invalid' : '' }}" id="update_user" name="update_user" value="{{ old('update_user') }}">
                            @if ($errors->has('update_user'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_user') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="nombre">Nombre</label>
                            <input @can('read_contribuyentes') disabled @endcan required type="text" class="form-control {{ $errors->has('update_nombre') ? ' is-invalid' : '' }}" id="update_nombre" name="update_nombre" value="{{ old('update_nombre') }}" >
                            @if ($errors->has('update_nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="apellido_pat">Apellido Paterno</label>
                            <input @can('read_contribuyentes') disabled @endcan  type="text" class="form-control {{ $errors->has('update_apellido_pat') ? ' is-invalid' : '' }}" id="update_apellido_pat" name="update_apellido_pat" value="{{ old('update_apellido_pat') }}" >
                            @if ($errors->has('update_apellido_pat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_apellido_pat') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="apellido_mat">Apellido Materno</label>
                            <input @can('read_contribuyentes') disabled @endcan type="text" class="form-control {{ $errors->has('update_apellido_mat') ? ' is-invalid' : '' }}" id="update_apellido_mat" name="update_apellido_mat" value="{{ old('update_apellido_mat') }}">
                            @if ($errors->has('update_apellido_mat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_apellido_mat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="rfc">R.F.C</label>
                            <input @can('read_contribuyentes') disabled @endcan  type="text" class="form-control {{ $errors->has('update_rfc') ? ' is-invalid' : '' }}" id="update_rfc" name="update_rfc" value="{{ old('update_rfc') }}" placeholder="XAXX010101000">
                            @if ($errors->has('update_rfc'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_rfc') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="update_sexo">Sexo</label>
                            <select @can('read_contribuyentes') disabled @endcan name="update_sexo" id="update_sexo" class="form-control {{ $errors->has('update_sexo') ? ' is-invalid' : '' }}">
                                <option value="I">Seleccione Sexo</option>
                                <option value="I">Indefinido</option>
                                <option value="M">Hombre</option>
                                <option value="F">Mujer</option>
                            </select>
                            @if ($errors->has('update_sexo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('sexo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="update_persona">Persona Moral ?</label>
                            <input @can('read_contribuyentes') disabled @endcan type="checkbox" data-toggle="toggle" data-on="Si" data-off="No" {{ $errors->has('update_persona') ? ' checked' : '' }} class="form-control " id="update_persona" name="update_persona">
                            @if ($errors->has('update_persona'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_persona') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="update_email">Correo</label>
                            <input @can('read_contribuyentes') disabled @endcan type="update_email" class="form-control {{ $errors->has('update_email') ? ' is-invalid' : '' }}" id="update_email" name="update_email" value="{{ old('update_email') }}" >
                            @if ($errors->has('update_email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="telefono_casa">Telefono Casa</label>
                            <input @can('read_contribuyentes') disabled @endcan type="number" class="form-control {{ $errors->has('update_telefono_casa') ? ' is-invalid' : '' }}" id="update_telefono_casa" name="update_telefono_casa" value="{{ old('update_telefono_casa') }}" >
                            @if ($errors->has('update_telefono_casa'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_telefono_casa') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="telefono_trab">Telefono Trabajo</label>
                            <input @can('read_contribuyentes') disabled @endcan type="number" class="form-control {{ $errors->has('update_telefono_trab') ? ' is-invalid' : '' }}" id="update_telefono_trab" name="update_telefono_trab" value="{{ old('update_telefono_trab') }}" >
                            @if ($errors->has('update_telefono_trab'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_telefono_trab') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="domicilio">Domicilio</label>
                            <input @can('read_contribuyentes') disabled @endcan required type="text" class="form-control {{ $errors->has('update_domicilio') ? ' is-invalid' : '' }}" id="update_domicilio" name="update_domicilio" value="{{ old('update_domicilio') }}" >
                            @if ($errors->has('update_domicilio'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_domicilio') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="calle1">Entre la Calle</label>
                            <input @can('read_contribuyentes') disabled @endcan type="text" class="form-control {{ $errors->has('update_calle1') ? ' is-invalid' : '' }}" id="update_calle1" name="update_calle1" value="{{ old('update_calle1') }}" >
                            @if ($errors->has('update_calle1'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_calle1') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="calle2">y la Calle</label>
                            <input @can('read_contribuyentes') disabled @endcan type="text" class="form-control {{ $errors->has('update_calle2') ? ' is-invalid' : '' }}" id="update_calle2" name="update_calle2" value="{{ old('update_calle2') }}" >
                            @if ($errors->has('update_calle2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_calle2') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="update_exterior">Numero Exterior</label>
                            <input @can('read_contribuyentes') disabled @endcan required  type="text" class="form-control {{ $errors->has('update_exterior') ? ' is-invalid' : '' }}" id="update_exterior" name="update_exterior" value="{{ old('update_exterior') }}" >
                            @if ($errors->has('update_exterior'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_exterior') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="update_interior">Numero Interior</label>
                            <input @can('read_contribuyentes') disabled @endcan type="text" class="form-control {{ $errors->has('update_interior') ? ' is-invalid' : '' }}" id="update_interior" name="update_interior" value="{{ old('update_interior') }}" >
                            @if ($errors->has('update_interior'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('interior') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="cp">C.P.</label>
                            <input @can('read_contribuyentes') disabled @endcan required type="number" class="form-control {{ $errors->has('update_cp') ? ' is-invalid' : '' }}" id="update_cp" name="update_cp" value="{{ old('update_cp') }}" >
                            @if ($errors->has('update_cp'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_cp') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="localidad">Localidad</label>
                            <input @can('read_contribuyentes') disabled @endcan required type="text" class="form-control {{ $errors->has('update_localidad') ? ' is-invalid' : '' }}" id="update_localidad" name="update_localidad" value="{{ old('update_localidad') }}" >
                            @if ($errors->has('update_localidad'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_localidad') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="colonia">Colonia</label>
                            <input @can('read_contribuyentes') disabled @endcan required type="text" class="form-control {{ $errors->has('update_colonia') ? ' is-invalid' : '' }}" id="update_colonia" name="update_colonia" value="{{ old('update_colonia') }}" >
                            @if ($errors->has('update_colonia'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('update_colonia') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    @can('write_contribuyentes')
                        <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus mr-2"></i> Registrar</button>
                    @endcan
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        var operation = "{{ old('operation') }}";
        if(operation == "update")
        {
            $("#modal-update-taxpayer").modal("show");
        }
        $('#modal-update-taxpayer').on('hidden.bs.modal', function (e) {
            $('#modal-update-taxpayer').find(".is-invalid").removeClass("is-invalid");
        });
    });
</script>