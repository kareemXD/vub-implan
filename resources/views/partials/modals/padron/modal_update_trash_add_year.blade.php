{{-- Modal Logout --}}
<div class="modal fade" id="modal-trash-add-year" role="dialog" >
    <div class="modal-dialog" role="document">
        <form method="POST" id="form-trash-add-year" action="{{ route('pyl.trash.years.add') }}">
            @csrf
            <input type="hidden" name="turn" value="{{ $turn->IdGiro }}">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar pago de basura</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row mb-2">
                        <div class="col-12">
                            <label for="year">Año</label>
                            <input type="number" class="form-control" id="year" name="year" required>
                        </div>
                    </div>
                    <div class="form-row mb-2">
                        <div class="col-12">
                            <label for="cuotadescarga">Cuota Por Descarga</label>
                            <input type="number" step="0.01" class="form-control" id="cuotadescarga" name="cuotadescarga" required>
                        </div>
                    </div>
                    <div class="form-row mb-2">
                        <div class="col-12">
                            <label for="cuotarecoleccion">Cuota por Recolección</label>
                            <input type="number" step="0.01" class="form-control" id="cuotarecoleccion" name="cuotarecoleccion" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" form="form-trash-add-year" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
    });
</script>