{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-taxpayer" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" action="{{ route('store.taxpayer') }}">
            @csrf
            <input type="hidden" name="operation" value="store">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Contribuyente</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="nombre">Nombre</label>
                            <input required maxlength="100" type="text" class="form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre') }}" >
                            @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="apellido_pat">Apellido Paterno</label>
                            <input  maxlength="50" type="text" class="form-control {{ $errors->has('apellido_pat') ? ' is-invalid' : '' }}" id="apellido_pat" name="apellido_pat" value="{{ old('apellido_pat') }}" >
                            @if ($errors->has('apellido_pat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellido_pat') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="apellido_mat">Apellido Materno</label>
                            <input  maxlength="50" type="text" class="form-control {{ $errors->has('apellido_mat') ? ' is-invalid' : '' }}" id="apellido_mat" name="apellido_mat" value="{{ old('apellido_mat') }}">
                            @if ($errors->has('apellido_mat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellido_mat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="rfc">R.F.C</label>
                            <input maxlength="15" type="text" class="form-control {{ $errors->has('rfc') ? ' is-invalid' : '' }}" id="rfc" name="rfc" value="{{ old('rfc') }}" placeholder="XAXX010101000">
                            @if ($errors->has('rfc'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rfc') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="sexo">Sexo</label>
                            <select name="sexo" id="sexo" class="form-control {{ $errors->has('sexo') ? ' is-invalid' : '' }}">
                                <option value="I">Seleccione Sexo</option>
                                <option value="I">Indefinido</option>
                                <option value="M">Hombre</option>
                                <option value="F">Mujer</option>
                            </select>
                            @if ($errors->has('sexo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('sexo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="persona">Persona Moral ?</label>
                            <input type="checkbox" data-toggle="toggle"  data-on="Si" data-off="No" class="form-control " id="persona" name="persona">
                            @if ($errors->has('persona'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('persona') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="email">Correo</label>
                            <input type="email" maxlength="50" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" >
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="telefono_casa">Telefono Casa</label>
                            <input type="number" maxlength="15" class="form-control {{ $errors->has('telefono_casa') ? ' is-invalid' : '' }}" id="telefono_casa" name="telefono_casa" value="{{ old('telefono_casa') }}" >
                            @if ($errors->has('telefono_casa'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono_casa') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="telefono_trab">Telefono Trabajo</label>
                            <input type="number" maxlength="15" class="form-control {{ $errors->has('telefono_trab') ? ' is-invalid' : '' }}" id="telefono_trab" name="telefono_trab" value="{{ old('telefono_trab') }}" >
                            @if ($errors->has('telefono_trab'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono_trab') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="domicilio">Domicilio</label>
                            <input required maxlength="60" type="text" class="form-control {{ $errors->has('domicilio') ? ' is-invalid' : '' }}" id="domicilio" name="domicilio" value="{{ old('domicilio') }}" >
                            @if ($errors->has('domicilio'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('domicilio') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="calle1">Entre la Calle</label>
                            <input type="text" maxlength="60" class="form-control {{ $errors->has('calle1') ? ' is-invalid' : '' }}" id="calle1" name="calle1" value="{{ old('calle1') }}" >
                            @if ($errors->has('calle1'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle1') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="calle2">y la Calle</label>
                            <input type="text" maxlength="60" class="form-control {{ $errors->has('calle2') ? ' is-invalid' : '' }}" id="calle2" name="calle2" value="{{ old('calle2') }}" >
                            @if ($errors->has('calle2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle2') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="exterior">Numero Exterior</label>
                            <input required type="text" maxlength="50" class="form-control {{ $errors->has('exterior') ? ' is-invalid' : '' }}" id="exterior" name="exterior" value="{{ old('exterior') }}" >
                            @if ($errors->has('exterior'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('exterior') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="interior">Numero Interior</label>
                            <input type="text" maxlength="50" class="form-control {{ $errors->has('interior') ? ' is-invalid' : '' }}" id="interior" name="interior" value="{{ old('interior') }}" >
                            @if ($errors->has('interior'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('interior') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="cp">C.P.</label>
                            <input required type="number" maxlength="5" class="form-control {{ $errors->has('cp') ? ' is-invalid' : '' }}" id="cp" name="cp" value="{{ old('cp') }}" >
                            @if ($errors->has('cp'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('cp') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="localidad">Localidad</label>
                            <input required maxlength="40" type="text" class="form-control {{ $errors->has('localidad') ? ' is-invalid' : '' }}" id="localidad" name="localidad" value="{{ old('localidad') }}" >
                            @if ($errors->has('localidad'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('localidad') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="colonia">Colonia</label>
                            <input required maxlength="40" type="text" class="form-control {{ $errors->has('colonia') ? ' is-invalid' : '' }}" id="colonia" name="colonia" value="{{ old('colonia') }}" >
                            @if ($errors->has('colonia'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('colonia') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus mr-2"></i> Registrar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        var operation = "{{ old('operation') }}";
        if(operation == "store")
        {
            $("#modal-add-taxpayer").modal("show");
        }
    });
</script>