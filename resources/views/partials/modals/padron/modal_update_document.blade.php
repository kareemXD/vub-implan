{{-- Modal Logout --}}
<div class="modal fade" id="modal-update-document" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form id="form-data-document" method="POST" action="{{ route('update.request.document') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="requisito_id" id="requisito_id">
            <input type="hidden" name="solicitud_id" id="solicitud_id" value="{{ $request->NumeroSolicitud }}">
            <input type="hidden" name="operation" value="store">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Requisito</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="nombre_r">Nombre del Requisito</label>
                            <input disabled maxlength="100" type="text" class="form-control" id="nombre_r">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="comments">Comentarios</label>
                            <textarea class="form-control" id="comments" name="comments"></textarea>
                            @if ($errors->has('comments'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('comments') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="document">Documento</label>
                            <div id="documents-content">
                                <input type='file' class='document' name='document' accept="application/pdf"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $('.document').simpleFilePreview();
    });
</script>