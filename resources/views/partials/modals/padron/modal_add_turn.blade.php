{{-- Modal Logout --}}
<div class="modal fade" id="modal_add_turn" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form id="form-data-document" method="POST" action="{{ route('add.period', $turn->IdGiro) }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="operation" value="store">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar Histórico</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="periodo_ano">Año</label>
                            <input value="{{ old('periodo_ano') }}" name="periodo_ano" maxlength="100" type="number" class="form-control @error('periodo_ano') is-invalid @enderror" id="periodo_ano">
                            @error('periodo_ano')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('periodo_ano') }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="periodo_cuota_minima">Cuota Minima</label>
                            <input value="{{ is_null(old('periodo_cuota_minima'))? 0 : old('periodo_cuota_minima') }}" name="periodo_cuota_minima" maxlength="100" type="number"
                             class="form-control" id="periodo_cuota_minima">
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col">
                            <label for="periodo_cuota_maxima">Cuota Máxima</label>
                            <input value="{{ is_null(old('periodo_cuota_maxima'))? 0 : old('periodo_cuota_maxima') }}" name="periodo_cuota_maxima" maxlength="100" type="number" class="form-control" id="periodo_cuota_maxima">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="periodo_cuota_fija">Cuota Fija</label>
                            <input value="{{  is_null(old('periodo_cuota_fija'))? $turn->CuotaFija : old('periodo_cuota_fija') }}" name="periodo_cuota_fija" 
                            maxlength="100" type="number" class="form-control  @error('periodo_cuota_fija') is-invalid @enderror" id="periodo_cuota_fija">
                            @error('periodo_cuota_fija')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('periodo_cuota_fija') }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-money-check mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
