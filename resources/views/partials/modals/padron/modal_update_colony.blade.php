{{-- Modal Logout --}}
<div class="modal fade" id="modal_update_colony" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form id="form-update-colony" method="POST" action="#" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="operation" value="store">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Colonia</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="actualizar_nombre_colonia">Nombre Colonia</label>
                            <input  autocomplete="off" value="{{ old('actualizar_nombre_colonia') }}" name="actualizar_nombre_colonia" maxlength="100" type="text" class="form-control @error('actualizar_nombre_colonia') is-invalid @enderror" id="actualizar_nombre_colonia">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="actualizar_nombre_comun">Nombre Común</label>
                            <input  autocomplete="off" value="{{ old('actualizar_nombre_comun') }}" name="actualizar_nombre_comun" maxlength="100" type="text" class="form-control @error('actualizar_nombre_comun') is-invalid @enderror" id="actualizar_nombre_comun">
                        </div>
                    </div>
                    <div class="form-row">
                        <label for="actualizar_id_poblacion" >Población</label>
                        <select name="actualizar_id_poblacion" id="actualizar_id_poblacion" class="select2 form-control">
                            <option value="">Seleccione una colonia</option>
                            @foreach($locations as $location)
                            <option @if($location->IdPoblacion == $idPoblacion) selected @endif value="{{ $location->IdPoblacion }}">{{ $location->NombrePoblacion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Actualizar</button>
                </div>
            </div>
        </form>
    </div>
</div>
