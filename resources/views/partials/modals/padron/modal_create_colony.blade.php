{{-- Modal Logout --}}
<div class="modal fade" id="modal_create_colony" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form id="form-create-colony" method="POST" action="{{ route('colony.add') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="operation" value="store">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registrar Colonia</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="crear_nombre_colonia">Nombre Colonia</label>
                            <input  autocomplete="off" value="{{ old('crear_nombre_colonia') }}" name="crear_nombre_colonia" maxlength="100" type="text" class="form-control @error('crear_nombre_colonia') is-invalid @enderror" id="crear_nombre_colonia">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="crear_nombre_comun">Nombre Común</label>
                            <input  autocomplete="off" value="{{ old('crear_nombre_comun') }}" name="crear_nombre_comun" maxlength="100" type="text" class="form-control @error('crear_nombre_comun') is-invalid @enderror" id="crear_nombre_comun">
                        </div>
                    </div>
                    <div class="form-row">
                        <label for="crear_id_poblacion" >Población</label>
                        <select name="crear_id_poblacion" id="crear_id_poblacion" class="select2 form-control">
                            <option value="">Seleccione una colonia</option>
                            @foreach($locations as $location)
                            <option @if($location->IdPoblacion == $idPoblacion) selected @endif value="{{ $location->IdPoblacion }}">{{ $location->NombrePoblacion }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Registrar</button>
                </div>
            </div>
        </form>
    </div>
</div>
