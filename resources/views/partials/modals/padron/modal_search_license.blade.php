{{-- Modal Logout --}}
<div class="modal fade" id="modalSearchLicense" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
    <form id="form-data-search-license" method="get" action="{{ route('search.license.details') }}" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="searchLicenseLabel">Consultar Licencia</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <input type="text" class="form-control" name="search-license-details" id="search-license-details" placeholder="Ingrese Número de Licencia">
                        </div>
                    </div>
                    <div class="form-row" id="search-license-content">
                        
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cancelar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-search-plus mr-2"></i> Buscar</button>
                </div>
            </div>
        </form>
    </div>
</div>
