{{-- Modal Logout --}}
<div class="modal fade" id="modal-update-trash" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" id="form-trash" action="{{ route('pyl.update.trash') }}">
            @csrf
            <input type="hidden" name="type" value="turn">
            <input type="hidden" name="turn">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar pago de basura</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group">
                            <div class="form-check">
                              <input class="form-check-input" type="checkbox" name="trash" id="trash" value="1">
                              <label class="form-check-label" for="trash">
                                ¿ Habilitar cuota de basura ?
                              </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-6">
                            <label for="condescarga">Concepto por Descarga</label>
                            <select required class="form-control select2" name="condescarga" id="condescarga">
                                <option value="">Selecciones el concepto</option>
                                @foreach ($conceptos as $concepto)
                                    <option value="{{ $concepto->Concepto }}">({{ $concepto->Concepto }}) {{ $concepto->Descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-6">
                            <label for="cuotadescarga">Cuota Por Descarga</label>
                            <input type="number" step="0.01" class="form-control" id="cuotadescarga" name="cuotadescarga">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-6">
                            <label for="conrecoleccion">Concepto por Recolección</label>
                            <select required class="form-control select2" name="conrecoleccion" id="conrecoleccion">
                                <option value="">Selecciones el concepto</option>
                                @foreach ($conceptos as $concepto)
                                    <option value="{{ $concepto->Concepto }}">({{ $concepto->Concepto }}) {{ $concepto->Descripcion }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-6">
                            <label for="cuotarecoleccion">Cuota por Recolección</label>
                            <input type="number" step="0.01" class="form-control" id="cuotarecoleccion" name="cuotarecoleccion">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="button" class="btn btn-primary btn-send"><i class="fas fa-save mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", ".btn-send", function(){
            $.confirm({
                title: 'Alerta!',
                content: '¿Estas seguro(a) de realizar esta acción? <br /> Esta acción realizara cambios en el sistema',
                type: "red",
                buttons: {
                    Aceptar: function () {
                        $('#form-trash')[0].submit();
                    },
                    Cancelar: function () {
                    },
                }
            });
        });
    });
</script>