{{-- Modal Logout --}}
<div class="modal fade" id="newFormalityLicenseModal" role="dialog">
    <div class="modal-dialog" role="document">
        <form id="form-data-document" method="POST" action="{{ route('update.license.formalities.store') }}">
            @csrf
            <input type="hidden" name="licencia_id" id="licencia_id" value="{{ $license->NumeroLicencia }}">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo tramite</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="nombre_r">Protección Civil</label>
                            <input readonly type="text" class="form-control" name="pc_formality" value="Visto Bueno">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus mr-2"></i> Generar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $('.document').simpleFilePreview();
    });
</script>