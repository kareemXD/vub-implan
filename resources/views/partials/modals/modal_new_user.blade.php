{{-- Modal Logout --}}
<div class="modal fade" id="modal-new-user" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" action="{{ route('new.register') }}" aria-label="{{ __('Register') }}">
            @csrf
            <input type="hidden" name="operation" value="store">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Usuario</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-5 col-form-label text-md-right">Nombre</label>

                        <div class="col-md-6">
                            <input  type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ape_pat" class="col-md-5 col-form-label text-md-right">Apellido Paterno</label>

                        <div class="col-md-6">
                            <input  type="text" class="form-control{{ $errors->has('ape_pat') ? ' is-invalid' : '' }}" name="ape_pat" value="{{ old('ape_pat') }}" required >

                            @if ($errors->has('ape_pat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ape_pat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ape_mat" class="col-md-5 col-form-label text-md-right">Apellido Materno</label>

                        <div class="col-md-6">
                            <input  type="text" class="form-control{{ $errors->has('ape_mat') ? ' is-invalid' : '' }}" name="ape_mat" value="{{ old('ape_mat') }}" required >

                            @if ($errors->has('ape_mat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ape_mat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-5 col-form-label text-md-right">Correo Electronico</label>

                        <div class="col-md-6">
                            <input  type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-5 col-form-label text-md-right">Contraseña</label>

                        <div class="col-md-6">
                            <input  type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-5 col-form-label text-md-right">Confirmar Contraseña</label>

                        <div class="col-md-6">
                            <input  type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="position" class="col-md-5 col-form-label text-md-right">Seleccione un Puesto</label>
                        <div class="col-md-5 content">
                            <select class="form-control select2" name="position" required >
                                <option value="" selected>Seleccione un rol</option>
                                @foreach ($positions as $position)
                                    <option value="{{ $position->id }}">{{ $position->nombre }} ( {{ $position->direction->abreviacion }} )</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1">
                            <button type="button" class="btn-add-puesto btn btn-primary"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="role" class="col-md-5 col-form-label text-md-right">Seleccione un Rol</label>
                        <div class="col-md-6 ">
                            <select class="form-control select2" id="role" name="role" required>
                                <option value="" selected >Seleccione un rol</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->name }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus mr-2"></i> Registrar</button>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
    $(function(){
        var operation = "{{ old('operation') }}";
        if(operation == "store")
        {
            $("#modal-new-user").modal("show");
        }

        $(document).on("click", ".btn-add-puesto", function(){
            var _this = $(this).parents(".form-group");
            _this.find(".content").animate({ height: "toggle" }, 1000, function(){
                _this.find(".content").empty();
                _this.find(".content").append('<div class="form-group" style="width:100%;"><input  type="text" class="form-control" name="puesto_name" placeholder="Nombre del Puesto" required></div>');
                _this.find(".content").append('<div class="form-group" style="width:100%;"><input  type="text" class="form-control" name="puesto_desc" placeholder="Descripción del Puesto" required></div>');
                var direcciones = "";
                @foreach ($direcciones as $direccion)
                    direcciones += '<option value="{{ $direccion->id }}">{{ $direccion->abreviacion }}</option>';
                @endforeach
                _this.find(".content").append('<select class="form-control select2" name="puesto_direccion" required >\
                                <option value="" selected>Seleccione una Dirección</option>\
                                '+direcciones+'\
                            </select>');
                $(".select2").select2({
                    theme: "bootstrap"
                });
                _this.find("button").removeClass("btn-add-puesto");
                _this.find("button").addClass("btn-delete-puesto");
                _this.find("button").removeClass("btn-primary");
                _this.find("button").addClass("btn-danger");
                _this.find("button").html('<i class="fas fa-trash"></i>');
                _this.find(".content").animate({ height: "toggle" }, 1000);
            });
        });
        
        $(document).on("click", ".btn-delete-puesto", function(){
            var _this = $(this).parents(".form-group");
            _this.find(".content").animate({ height: "toggle" }, 1000, function(){
                _this.find(".content").empty();
                var puesto = "";
                @foreach ($positions as $position)
                    puesto += '<option value="{{ $position->id }}">{{ $position->nombre }} ( {{ $position->direction->abreviacion }} )</option>';
                @endforeach
                _this.find(".content").append('<select class="form-control select2" name="position" required>\
                                <option value="" selected>Seleccione un Puesto</option>\
                                '+puesto+'\
                            </select>');
                $(".select2").select2({
                    theme: "bootstrap"
                });
                _this.find("button").removeClass("btn-delete-puesto");
                _this.find("button").addClass("btn-add-puesto");
                _this.find("button").removeClass("btn-danger");
                _this.find("button").addClass("btn-primary");
                _this.find("button").html('<i class="fas fa-plus"></i>');
                _this.find(".content").animate({ height: "toggle" }, 1000);
            });
        });
    });
</script>