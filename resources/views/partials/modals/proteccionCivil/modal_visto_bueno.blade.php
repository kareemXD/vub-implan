<div class="modal fade" id="vistoBuenoModal" role="dialog" >
    <div class="modal-dialog" role="document">
        <form id="vistoBuenoForm" method="GET" action="{{ route('pc.requests.vistoBueno', $request->id) }}" target="_blank">
            @csrf
            <input type="hidden" name="requestId" id="requestId" value="{{ $request->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Generar Visto Bueno</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="umpc">UMPC</label>
                            <input name="umpc" type="text" class="required form-control" id="umpc" value="{{ $request->umpc }}" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="document">VMMS</label>
                            <select name="ldrr" class="required form-control" id="">
                                <option value="">Seleccionar</option>
                                <option value="JANC">(JANC) - JOSÉ ALBERTO NOLASCO CORONA</option>
                                <option value="ASCM">(ASCM) - ADILENE SARAHÍ CASTAÑEDA MARTÍNEZ</option>
                                <option value="HOCD">(HOCD) - HÉCTOR ORLANDO CASTILLON DÍAZ</option>
                                <option value="VMMS">(VMMS) - VICTOR MANUEL MARTÍNEZ SAMANIEGO</option>
                                <option value="JKGM">(JKGM) - JUDITH KARINA GONZÁLEZ MEDINA</option>
                                <option value="MASR">(MASR) - MIGUEL ANTONIO SALAS RAMIREZ</option>
                                <option value="OEBR">(OEBR) - OSVALDO EMMANUEL BANDERAS RUELAS</option>
                                <option value="OEBR">(LDAC) - LUIS DAVID ALVARADO CASTAÑEDA</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" form="vistoBuenoForm" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Generar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        var ldrr = "{{ $request->ldrr }}";
        $('[name="ldrr"]').val(ldrr);
    });
</script>