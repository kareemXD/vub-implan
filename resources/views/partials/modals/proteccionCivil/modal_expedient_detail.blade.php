<div class="modal fade" id="expedientDetailModal" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Detalle de Expediente</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="#">
                    @csrf
                    <input type="hidden" name="expedientId" value="">
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="no_expediente">No. Expediente</label>
                            <input type="text" class="form-control " id="no_expediente" name="no_expediente" value="" readonly>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="grupo">Grupo</label>
                            <input type="text" class="form-control" id="grupo" name="grupo" value="" readonly>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-sm-4">
                            <label for="domicilio">Calle</label>
                            <input type="text" class="form-control" id="domicilio" name="domicilio" value="">
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="numero">Número</label>
                            <input type="text" class="form-control" id="numero" name="numero" value="">
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="colonia">Colonia</label>
                            <input type="text" class="form-control" id="colonia" name="colonia" value="">
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="localidad">Localidad</label>
                            <input type="text" class="form-control" id="localidad" name="localidad" value="">
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-sm-4">
                            <label for="razon_social">Razón Social</label>
                            <input type="text" class="form-control" id="razon_social" name="razon_social" value="">
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="rfc">RFC</label>
                            <input type="text" class="form-control" id="rfc" name="rfc" value="">
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="nombre_comercial">Nombre Comercial</label>
                            <input type="text" class="form-control" id="nombre_comercial" name="nombre_comercial" value="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="telefono">Teléfono</label>
                            <input type="text" class="form-control" id="telefono" name="telefono" value="">
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="correo">Correo</label>
                            <input type="text" class="form-control" id="correo" name="correo" value="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="estado">Estado</label>
                            <input type="text" class="form-control" id="estado" name="estado" value="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="observaciones">Observaciones</label>
                            <textarea class="form-control" id="observaciones" name="observaciones" rows="5"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                <button class="btn btn-primary expedientDetailSave" type="button"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        $(document).on('click', '.expedientDetailSave', function() {
            var form = $('#expedientDetailModal form'),
                data = form.serialize();

            $.ajax({
                type: 'POST',
                url: "{{ route('pc.ajax.expedients.update') }}",
                data: data,
                success: function(response) {
                    console.log(response);

                    $('#expedientDetailModal').modal('hide');
                }
            });
        });
    });
</script>