<div class="modal fade" id="cartaCompromisoModal" role="dialog" >
    <div class="modal-dialog" role="document">
        <form id="cartaCompromisoForm" method="GET" action="{{ route('pc.requests.cartaCompromiso', $request->id) }}" target="_blank">
            @csrf
            <input type="hidden" name="requestId" id="requestId" value="{{ $request->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Generar Carta Compromiso</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="cc">Folio CC</label>
                            <input name="cc" type="text" class="required form-control" id="cc" value="{{ $request->cc }}" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="testigo_1">Testigo 1</label>
                            <select name="testigo_1" class="required form-control" id="" required>
                                <option value="">Seleccionar</option>
                                <option value="JOSÉ ALBERTO NOLASCO CORONA">(JANC) - JOSÉ ALBERTO NOLASCO CORONA</option>
                                <option value="ADILENE SARAHÍ CASTAÑEDA MARTÍNEZ">(ASCM) - ADILENE SARAHÍ CASTAÑEDA MARTÍNEZ</option>
                                <option value="HÉCTOR ORLANDO CASTILLON DÍAZ">(HOCD) - HÉCTOR ORLANDO CASTILLON DÍAZ</option>
                                <option value="VICTOR MANUEL MARTÍNEZ SAMANIEGO">(VMMS) - VICTOR MANUEL MARTÍNEZ SAMANIEGO</option>
                                <option value="JUDITH KARINA GONZÁLEZ MEDINA">(JKGM) - JUDITH KARINA GONZÁLEZ MEDINA</option>
                                <option value="MIGUEL ANTONIO SALAS RAMIREZ">(MASR) - MIGUEL ANTONIO SALAS RAMIREZ</option>
                                <option value="OSVALDO EMMANUEL BANDERAS RUELAS">(OEBR) - OSVALDO EMMANUEL BANDERAS RUELAS</option>
                                <option value="LUIS DAVID ALVARADO CASTAÑEDA">(LDAC) - LUIS DAVID ALVARADO CASTAÑEDA</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="testigo_2">Testigo 2</label>
                            <select name="testigo_2" class="required form-control" id="" required>
                                <option value="">Seleccionar</option>
                                <option value="JOSÉ ALBERTO NOLASCO CORONA">(JANC) - JOSÉ ALBERTO NOLASCO CORONA</option>
                                <option value="ADILENE SARAHÍ CASTAÑEDA MARTÍNEZ">(ASCM) - ADILENE SARAHÍ CASTAÑEDA MARTÍNEZ</option>
                                <option value="HÉCTOR ORLANDO CASTILLON DÍAZ">(HOCD) - HÉCTOR ORLANDO CASTILLON DÍAZ</option>
                                <option value="VICTOR MANUEL MARTÍNEZ SAMANIEGO">(VMMS) - VICTOR MANUEL MARTÍNEZ SAMANIEGO</option>
                                <option value="JUDITH KARINA GONZÁLEZ MEDINA">(JKGM) - JUDITH KARINA GONZÁLEZ MEDINA</option>
                                <option value="MIGUEL ANTONIO SALAS RAMIREZ">(MASR) - MIGUEL ANTONIO SALAS RAMIREZ</option>
                                <option value="OSVALDO EMMANUEL BANDERAS RUELAS">(OEBR) - OSVALDO EMMANUEL BANDERAS RUELAS</option>
                                <option value="LUIS DAVID ALVARADO CASTAÑEDA">(LDAC) - LUIS DAVID ALVARADO CASTAÑEDA</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" form="cartaCompromisoForm" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Generar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        var testigo_1 = "{{ $request->testigo_1 }}",
            testigo_2 = "{{ $request->testigo_2 }}";

        $('[name="testigo_1"]').val(testigo_1);
        $('[name="testigo_2"]').val(testigo_2);
    });
</script>