<h5 class="text-info"><b>Tramites</b></h5>
@if ($request->procedures()->count() > 0)
<table class="table table-sm table-procedures">
    <tbody>
        @foreach ($request->procedures as $process)
            <tr class="process-row" data-id="{{ $process->id }}">
                <td>
                    <div class="">
                        <span class="process-name"><i class="far fa-file-alt"></i> {{ $process->nombre }}</span> 
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@else
    <p>No cuenta con tramites esta solicitud!</p>
@endif