<div class="modal-body">
    <form id="requestAsignToDictaminationForm" action="{{ route('du.request.asign.dictaminators.update') }}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="requestId" value="{{ $request->id }}">
        <div class="form-group">
            <label for="">Asignar Dictaminador</label>
            <select name="dictaminator" class="form-control" id="" required>
                <option value="">Seleccionar</option>
                @foreach ($dictaminators as $dictaminator)
                    <option value="{{ $dictaminator['id'] }}">{{ $dictaminator['name'] }} - Solicitudes: {{ $dictaminator['countActiveRequests'] }}</option>
                @endforeach
            </select>
        </div>
    </form>
</div>
<div class="modal-footer d-flex justify-content-between">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    <button type="submit" form="requestAsignToDictaminationForm" class="btn btn-primary">Asignar</button>
</div>