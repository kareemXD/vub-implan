<div class="modal-body">
    <form id="requestAsignToInspectionForm" action="{{ route('du.request.asign.inspectors.update') }}" method="POST">
        @csrf
        @method('PUT')
        <input type="hidden" name="requestId" value="{{ $request->id }}">
        <div class="form-group">
            <label for="">Asignar Inspector</label>
            <select name="inspector" class="form-control" id="" required>
                <option value="">Seleccionar</option>
                @foreach ($inspectors as $inspector)
                    <option value="{{ $inspector['id'] }}">{{ $inspector['name'] }} - Solicitudes: {{ $inspector['countActiveRequests'] }}</option>
                @endforeach
            </select>
        </div>
    </form>
</div>
<div class="modal-footer d-flex justify-content-between">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
    <button type="submit" form="requestAsignToInspectionForm" class="btn btn-primary">Asignar</button>
</div>