{{-- Modal Logout --}}
<div class="modal fade" id="updatePcRequirementModal" role="dialog" >
    <div class="modal-dialog" role="document">
        <form id="form-data-document" method="POST" action="{{ route('update.du.request.pcRequirement') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="requisito_id" id="requisito_id">
            <input type="hidden" name="solicitud_id" id="solicitud_id" value="{{ $request->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Requisito</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="nombre_r">Nombre del Requisito</label>
                            <input disabled maxlength="100" type="text" class="form-control" id="nombre_r">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="document">Documento</label>
                            <div id="documents-content">
                                <input type='file' class='document' name='document' accept="application/pdf, image/png, image/gif, image/jpeg"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $('.document').simpleFilePreview();
    });
</script>