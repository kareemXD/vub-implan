@extends('layouts.index')

@section('title') Solicitudes @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Solicitudes</h3>
        </div>
    </div>    
    {{-- @can('write_du_requests') --}}
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('pc.requests.create') }}" class="btn btn-primary mr-3"><i class="fas fa-plus-circle mr-2"></i> Nueva Solicitud</a>
        </div>
    {{-- @endcan --}}
    <div class="panel-body with-buttons">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif

            <form id="searchForm" action="{{ route('pc.requests') }}" method="GET" autocomplete="off">
            </form>
            
            <table style="width: 100%;" class="table dataTable table-sm table-hover">
                <thead>
                    <tr class="search-field">
                        <th scope="col" style="width:5%;"><input type="text" name="numero_solicitud" form="searchForm" style="font-size: 13px" value="{{ !empty(session('inputs')['numero_solicitud'])? session('inputs')['numero_solicitud'] : '' }}" placeholder="Solicitud" class="form-control input-search" /></th>
                        <th scope="col" style="width:5%;"><input type="text" name="no_expediente" form="searchForm" style="font-size: 13px" value="{{ !empty(session('inputs')['no_expediente'])? session('inputs')['no_expediente'] : '' }}" placeholder="Expediente" class="form-control input-search" /></th>
                        <th scope="col"><input type="text" name="nombre_comercial" form="searchForm" style="font-size: 13px" value="{{ !empty(session('inputs')['nombre_comercial'])? session('inputs')['nombre_comercial'] : '' }}" placeholder="Nombre Comercial" class="form-control input-search" /></th>
                        <th scope="col"><input type="text" name="nombre_solicitante" form="searchForm" style="font-size: 13px" value="{{ !empty(session('inputs')['nombre_solicitante'])? session('inputs')['nombre_solicitante'] : '' }}" placeholder="Solicitante" class="form-control input-search" /></th>
                        <th scope="col"><input type="text" name="giro" form="searchForm" style="font-size: 13px" value="{{ !empty(session('inputs')['giro'])? session('inputs')['giro'] : '' }}" placeholder="Giro" class="form-control input-search" /></th>
                        <th scope="col">
                            <select name="origen" class="form-control" form="searchForm" id="">
                                <option value="">Todos</option>
                                <option value="Protección Civil">Protección Civil</option>
                                <option value="Padrón y Licencias">Padrón y Licencias</option>
                                <option value="Desarrollo Urbano">Desarrollo Urbano</option>
                            </select>
                        </th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th class="btn-search-table" style="width:5%;"><button type="submit" form="searchForm" class="btn btn-block btn-success"><i class="fas fa-search"></i></button></th>
                    </tr>
                    <tr>
                        <th scope="col" style="width:5%;">#</th>
                        <th scope="col" style="width:5%;"># Exp</th>
                        <th scope="col">Nombre Comercial</th>
                        <th scope="col">Solicitante</th>
                        <th scope="col">Giro</th>
                        <th scope="col">Origen</th>
                        <th scope="col">Estatus</th>
                        <th scope="col">Última actualización</th>
                        <th scope="col" style="width:5%;">Acciones</th>
                    </tr>
                </thead>
                <tbody class="small-font">
                    @foreach ($requests as $request)
                        <tr>
                            <td>{{ $request->requestNumberText() }}</td>
                            <td>
                                @if (!empty($request->expedient))
                                    {{ $request->expedient->no_expediente }}
                                @endif
                            </td>
                            <td>{{ $request->nombre_comercial }}</td>
                            <td>{{ $request->nombre_solicitante }}</td>
                            <td>
                                @if (!empty($request->turn->Nombre))
                                    {{ $request->turn->Nombre }}</td>
                                @endif
                            <td><b>{{ $request->origen }}</b></td>
                            <td class="{{ $request->statusClass() }}">{{ $request->status() }}</td>
                            <td>{{ $request->updated_at }}</td>
                            <td>
                                <a href="{{ route('pc.requests.edit', $request->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="table-footer">
                <div class="pagination">
                    <span>Mostrando {{ (($requests->currentPage() * 15) - 14) }} a {{( $requests->currentPage() * 15) }} de {{ $requests->total() }} Filas</span>
                    {{ $requests->appends(Request::only('numero_solicitud'))->links() }} 
                </div>
                {{-- <div class="button-exports">
                    <a href="{{ route('print.requests') }}" target="_BLANK" class="btn btn-success edit-taxpayer"><i class="fas fa-pdf"></i> Exportar a PDF</a>
                </div> --}}
            </div>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        $(function(){
            $("#menu_proteccion_civil").addClass('active');

            var searchSource = "{{ !empty(session('inputs')['origen'])? session('inputs')['origen'] : '' }}";
            $('[name="origen"]').val(searchSource);
        });
    </script>
@endsection