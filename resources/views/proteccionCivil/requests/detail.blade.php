@extends('layouts.index')

@section('title') Solicitud: {{ $request->requestNumberText() }} ({{ $request->status() }}) @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
<div class="panel-top">
    <div class="d-flex justify-content-between">
        <h3>Solicitud: {{ $request->requestNumberText() }}</h3>
        <h3>Estatus: <span class="badge badge-secondary ml-3">{{ $request->status() }}</span></h3>
    </div>
</div>
<div class="panel-body with-buttons">
    <div class="margin-fix panel-row-fluid">
        @if(session()->has('alert'))
            <div class="alert alert-primary" role="alert">
                {{ session("alert") }}
            </div>
        @endif
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-link active" id="period-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Ubicación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="data-tab" data-toggle="tab" href="#data" role="tab" aria-controls="data" aria-selected="true">Solicitud</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="requirements-tab" data-toggle="tab" href="#requirements" role="tab" aria-controls="requirements" aria-selected="true">Requisitos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="inspection-tab" data-toggle="tab" href="#inspection" role="tab" aria-controls="inspection" aria-selected="true">Inspección</a>
            </li>
        </ul>
        <form id="form-data" action="#" method="POST" class="tab-content">
            @csrf
            @method('PUT')
            <input type="hidden" name="requestId" value="{{ $request->id }}">
            <input type="hidden" name="request_action" value="">
            <div class="tab-pane fade show active" id="location" role="tabpanel" aria-labelledby="location-tab">
                <div class="form-group mt-1 mb-1 row margin-10 ">
                    <label for="city" class="col-sm-2 col-form-label">Poblacion <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <select name="city" id="city" class="required select2 form-control {{ $errors->has('city') ? ' is-invalid' : '' }}" disabled>
                            <option value="">Seleccione una Población</option>
                            @foreach ($poblaciones as $poblacion)
                                <option value="{{ $poblacion->IdPoblacion }}" data-city="{{ $poblacion->NombrePoblacion }}" {{ $request->id_poblacion == $poblacion->IdPoblacion? 'selected' : '' }}>{{ $poblacion->NombrePoblacion }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10 ">
                    <label for="colony" class="col-sm-2 col-form-label">Colonia <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <div class="input-group ">
                            <select name="colony" id="colony" data-required="true" class="required select2 form-control {{ $errors->has('colony') ? ' is-invalid' : '' }}" disabled>
                                <option value="">Seleccione una Colonia</option>
                            </select>
                            @if ($errors->has('colony'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('colony') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="domicilio2" class="col-sm-2 col-form-label">Calle <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <div class="input-group ">
                            <select name="domicilio2" id="domicilio2" data-required="true" class="required select2 form-control {{ $errors->has('domicilio2') ? ' is-invalid' : '' }}" disabled>
                                <option value="">Seleccione una Calle</option>
                            </select>
                            @if ($errors->has('domicilio2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('domicilio2') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="form-group mt-1 mb-1 row margin-10">
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="calle1_u" class="col-sm-2 col-form-label">Entre Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group ">
                            <select name="calle1_u" id="calle1_u" class="select2 form-control {{ $errors->has('calle1_u') ? ' is-invalid' : '' }}" disabled>
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle1_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle1_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="ext" class="col-sm-2 col-form-label">Exterior <span class="text-danger">*</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="required form-control {{ $errors->has('ext') ? ' is-invalid' : '' }}" id="ext" name="ext" value="{{ $request->no_exterior }}" disabled>
                        @if ($errors->has('ext'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ext') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="calle2_u" class="col-sm-2 col-form-label">Y Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group ">
                            <select name="calle2_u" id="calle2_u" class="select2 form-control {{ $errors->has('calle2_u') ? ' is-invalid' : '' }}" disabled>
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle2_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle2_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="int" class="col-sm-2 col-form-label">Interior</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control {{ $errors->has('int') ? ' is-invalid' : '' }}" id="int" name="int" value="{{ $request->no_interior }}" disabled>
                        @if ($errors->has('int'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('int') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="este" class="col-sm-2 col-form-label">Coordenada Este</label>
                    <div class="col-sm-4">
                        <input disabled type="number" class="required form-control" id="este" name="este" value="{{ $request->coordenada_este }}">
                    </div>
                    <label for="norte" class="col-sm-2 col-form-label">Coordenada Norte</label>
                    <div class="col-sm-4">
                        <input disabled type="number" class="required form-control" id="norte" name="norte" value="{{ $request->coordenada_norte }}">
                    </div>
                </div>
                <div id="map" class="map" style="width:100%; height:500px;"></div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="submit" class="btn btn-primary mr-2   btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            <div class="tab-pane fade " id="data" role="tabpanel" aria-labelledby="data-tab">
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Solicitud</legend>

                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="origen">Origen </label>
                            <input type="text" class="required form-control " id="origen" name="origen" value="{{ $request->origen }}" readonly>
                        </div>
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="fecha_solicitud">Fecha <span class="text-danger">*</span></label>
                            <div class="form-row">
                                <div class="col-3">
                                    <select name="fecha_solicitud_day" class="form-control required {{ $errors->has('fecha_solicitud_day') ? ' is-invalid' : '' }}" id="" disabled>
                                        <option value="">Seleccionar</option>
                                        @for ($i = 1; $i <= 31; $i++)
                                            <option value="{{ $i < 10? '0'.$i : $i }}">{{ $i < 10? '0'.$i : $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-3">
                                    <select name="fecha_solicitud_month" class="form-control required {{ $errors->has('fecha_solicitud_month') ? ' is-invalid' : '' }}" id="" disabled>
                                        <option value="">Seleccionar</option>
                                        <option value="01">Enero</option>
                                        <option value="02">Febrero</option>
                                        <option value="03">Marzo</option>
                                        <option value="04">Abril</option>
                                        <option value="05">Mayo</option>
                                        <option value="06">Junio</option>
                                        <option value="07">Julio</option>
                                        <option value="08">Agosto</option>
                                        <option value="09">Septiembre</option>
                                        <option value="10">Octubre</option>
                                        <option value="11">Noviembre</option>
                                        <option value="12">Diciembre</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <select name="fecha_solicitud_year" class="form-control required {{ $errors->has('fecha_solicitud_year') ? ' is-invalid' : '' }}" id="" disabled>
                                        <option value="">Seleccionar</option>
                                        <option value="{{ now()->subYear()->year }}">{{ now()->subYear()->year }}</option>
                                        <option value="{{ now()->year }}" selected>{{ now()->year }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="estatus_licencia">Estatus de la Licencia <span class="text-danger">*</span></label>
                            <select name="estatus_licencia" class="form-control required {{ $errors->has('estatus_licencia') ? ' is-invalid' : '' }}" id="" disabled>
                                <option value="Nueva Licencia">Nueva Licencia</option>
                                <option value="Refrendo">Refrendo</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </div>
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="no_licencia">No. de Licencia Municipal</label>
                            <input type="text" maxlength="50" class="form-control {{ $errors->has('no_licencia') ? ' is-invalid' : '' }}" id="no_licencia" name="no_licencia" value="{{ old('no_licencia', $request->no_licencia) }}" disabled>
                            @if ($errors->has('no_licencia'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('no_licencia') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="nombre">Nombre o Razón Social <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre', $request->nombre) }}" disabled>
                            @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="rfc">R.F.C.</label>
                            <input maxlength="50" type="text" class="form-control {{ $errors->has('rfc') ? ' is-invalid' : '' }}" id="rfc" name="rfc" value="{{ old('rfc', $request->rfc) }}" disabled>
                            @if ($errors->has('rfc'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rfc') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="curp">CURP <span class="text-danger">*</span></label>
                            <input maxlength="50" type="text" class="form-control {{ $errors->has('curp') ? ' is-invalid' : '' }}" id="curp" name="curp" value="{{ old('curp', $request->curp) }}" disabled>
                            @if ($errors->has('curp'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('curp') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="nombre_comercial">Nombre comercial <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('nombre_comercial') ? ' is-invalid' : '' }}" id="nombre_comercial" name="nombre_comercial" value="{{ old('nombre_comercial', $request->nombre_comercial) }}" disabled>
                            @if ($errors->has('nombre_comercial'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre_comercial') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="horario_laboral">Horario y días laborales <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('horario_laboral') ? ' is-invalid' : '' }}" id="horario_laboral" name="horario_laboral" value="{{ old('horario_laboral', $request->horario_laboral) }}" disabled>
                            @if ($errors->has('horario_laboral'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('horario_laboral') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="no_empleados">No. de Empleados que laboran en el Inmueble <span class="text-danger">*</span></label>
                            <input type="number" class="required form-control {{ $errors->has('no_empleados') ? ' is-invalid' : '' }}" id="no_empleados" name="no_empleados" value="{{ old('no_empleados', $request->no_empleados) }}" disabled>
                            @if ($errors->has('no_empleados'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('no_empleados') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="no_bombas_gasolinera">No. de Bombas en Gasolinera</label>
                            <input type="number" class="required form-control {{ $errors->has('no_bombas_gasolinera') ? ' is-invalid' : '' }}" id="no_bombas_gasolinera" name="no_bombas_gasolinera" value="{{ old('no_bombas_gasolinera', $request->no_bombas_gasolinera) }}" disabled>
                            @if ($errors->has('no_bombas_gasolinera'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('no_bombas_gasolinera') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="nombre_solicitante">Nombre del Solicitante <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('nombre_solicitante') ? ' is-invalid' : '' }}" id="nombre_solicitante" name="nombre_solicitante" value="{{ old('nombre_solicitante', $request->nombre_solicitante) }}" disabled>
                            @if ($errors->has('nombre_solicitante'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre_solicitante') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="caracter_juridico_solicitante">Caracter Juridico del Solicitante <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('caracter_juridico_solicitante') ? ' is-invalid' : '' }}" id="caracter_juridico_solicitante" name="caracter_juridico_solicitante" value="{{ old('caracter_juridico_solicitante', $request->caracter_juridico_solicitante) }}" disabled>
                            @if ($errors->has('caracter_juridico_solicitante'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('caracter_juridico_solicitante') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="telefono_celular">Telefono celular <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('telefono_celular') ? ' is-invalid' : '' }}" id="telefono_celular" name="telefono_celular" value="{{ old('telefono_celular', $request->telefono_celular) }}" disabled>
                            @if ($errors->has('telefono_celular'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono_celular') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="telefono_fijo">Telefono fijo</label>
                            <input maxlength="50" type="text" class="form-control {{ $errors->has('telefono_fijo') ? ' is-invalid' : '' }}" id="telefono_fijo" name="telefono_fijo" value="{{ old('telefono_fijo', $request->telefono_fijo) }}" disabled>
                            @if ($errors->has('telefono_fijo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono_fijo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="email">Email <span class="text-danger">*</span></label>
                            <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email', $request->email) }}" disabled>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <br>
                </fieldset>
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Giros</legend>
                    <table class="table table-sm turns-table">
                        <tbody>
                            @foreach ($request->turns as $turn)
                            <tr class="turn-row">
                                <td>
                                    {{ $turn->giro }} - #{{ $turn->id_giro }}
                                </td>    
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                </fieldset>
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Tramites</legend>
                    <div class="form-row">
                        @foreach ($request->formalities as $formality)
                            <div class="form-group mt-1 mb-1 col-md-6">
                                <input type="checkbox" class="" id="formality_{{ $formality->id }}" name="formalities[]" value="{{ $formality->id }}" checked disabled>
                                <label for="formality_{{ $formality->id }}">{{ $formality->formality->tramite }}</label>
                            </div>
                        @endforeach
                    </div>
                    @if (!empty($request->otro_tramite))
                        <div class="form-row">
                            <div class="form-group mt-1 mb-1 col">
                                <input type="checkbox" class="" id="other_formality" name="formalities[]" value="otro" {{ !empty($request->otro_tramite)? 'checked' : '' }} disabled>
                                <label for="other_formality">Otro tramite</label>
                                <input type="text" class="form-control {{ $errors->has('other_formality') ? ' is-invalid' : '' }}" name="other_formality" value="{{ old('other_formality', $request->otro_tramite) }}" disabled>
                            </div>
                        </div>
                    @endif
                    <br>
                </fieldset>
            </div>

            <div class="tab-pane fade " id="requirements" role="tabpanel" aria-labelledby="requirements-tab">
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Requisitos</legend>

                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th width="80%">Requisito</th>
                                <th width="10%" class="text-center">Estatus</th>
                                <th width="10%" class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($requirements as $requirement)
                                <tr>
                                    <td>{{ $requirement->requisito }}</td>
                                    <td class="text-center text-white {{ $request->requirementStatus($requirement->id) == 'Pendiente' ? 'bg-warning' : 'bg-success' }}">{{ $request->requirementStatus($requirement->id) }}</td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary btn-sms" data-toggle="modal" data-target="#modal-update-requirement" data-requirementid="{{ $requirement->id }}" data-requirement="{{ $requirement->requisito }}"><i class="fas fa-edit"></i></button>
                                        @if ($request->requirementStatus($requirement->id) != config('system.pc.requirements.statuses.pending'))
                                            <a href="{{ asset('storage/'.$request->requirements()->where('id_requisito', $requirement->id)->first()->documento) }}" target="_blank" class="btn btn-secondary"><i class="fas fa-download"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                </fieldset>
            </div>

            <div class="tab-pane fade " id="inspection" role="tabpanel" aria-labelledby="inspection-tab">
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Inspección</legend>
                    <div>
                        <h4>Expediente</h4>
                        <div class="form-row">
                            <div class="form-group mt-1 mb-1 col-sm-10">
                                <select id="no_expediente" name="no_expediente" class="js-data-no-expediente-ajax form-control required" disabled></select>
                            </div>
                            <div class="form-group mt-1 mb-1 col-sm-2">
                                <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#expedientDetailModal">Detalles de Expediente</button>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group my-1 col">
                                <label for="inmueble_sector">Inmueble Perteneciente al Sector:</label>
                                <select name="inmueble_sector" id="inmueble_sector" class="required form-control" id="" disabled>
                                    <option value="">Seleccionar</option>
                                    @foreach ($inmuebleSectores as $inmuebleSector)
                                        <option value="{{ $inmuebleSector->id }}">{{ $inmuebleSector->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group my-1 col">
                                <label for="tipo_establecimiento">Tipo de Establecimiento:</label>
                                <select name="tipo_establecimiento" id="tipo_establecimiento" class="required form-control" id="" disabled>
                                    <option value="">Seleccionar</option>
                                    @foreach ($tipoEstablecimientos as $tipoEstablecimiento)
                                        <option value="{{ $tipoEstablecimiento->id }}">{{ $tipoEstablecimiento->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div>
                        @if (empty($request->tipo_giro))
                            <h4>Tipo de Giro</h4>
                            <button type="button" class="btn btn-danger mr-3" id="high-risk-turn">Giro de Alto Riesgo</button> 
                            <button type="button" class="btn btn-success mr-3" id="ordinary-turn">Giro Ordinario</button>
                            <br>
                        @else
                            <h4>Tipo de Giro: <span class="{{ config('system.pc.requests.turnTypes.classes.'.$request->tipo_giro) }}">{{ config('system.pc.requests.turnTypes.labels.'.$request->tipo_giro) }}</span></h4>
                        @endif

                        @if (!empty($request->tipo_giro) && $request->tipo_giro == config('system.pc.requests.turnTypes.highRisk'))
                            <br>
                            <h5>Dictamen de Verificación</h5>
                            <textarea name="resultado_verificacion" id="" class="form-control" rows="4" disabled>{{ $request->resultado_verificacion }}</textarea>
                        @endif
                    </div>
                    <hr>
                    <div class="text-right mb-3 d-flex justify-content-between">
                        <h4>Documentación Oficial</h4>
                        @if (empty($_GET['redirect']))
                            <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modal-update-document"><i class="fas fa-plus"></i> Subir documento</button>
                        @endif
                    </div>
                    <table class="table table-hover table-sm">
                        <thead class="thead-light">
                            <tr>
                                <th width="80%">Documento</th>
                                <th width="80%" class="text-center">Fecha</th>
                                <th width="10%" class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($request->documents as $document)
                                <tr>
                                    <td>{{ $document->descripcion }}</td>
                                    <td class="text-center">{{ $document->created_at }}</td>
                                    <td class="text-center">
                                        <a href="{{ asset('storage/'.$document->documento) }}" target="_blank" class="btn btn-secondary"><i class="fas fa-download"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br />
                </fieldset>
            </div>
        </form>
    </div>
</div>
<div class="row justify-content-end panel-buttoms">
    <button type="button" class="btn btn-primary mr-3 btn-next " id="btn-previous" style="display:none;" data-target="#data">Anterior <i class="fas fa-arrow-left ml-2"></i></button>
    <button type="button" class="btn btn-primary mr-3 btn-next" id="btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
    {{-- <button type="button" class="btn btn-danger mr-3" id="btn-reject-request" style="display:none;"><i class="fas fa-times-circle mr-2"></i> Rechazar Solicitud</button>
    <button type="button" class="btn btn-primary mr-3" id="btn-approve-request" style="display:none;"><i class="fas fa-check-double mr-2"></i> Aprobar Solicitud</button> --}}
</div>
@endsection

@section('modals')
    @include('partials.modals.proteccionCivil.modal_update_document')
    @include('partials.modals.proteccionCivil.modal_update_requirement')
    @include('partials.modals.proteccionCivil.modal_expedient_detail')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('system.google.maps.apiKey') }}&libraries=places&v=weekly"
        defer
    ></script>

    <script>
        window.addEventListener('load', function () {
            OnChangeCity("{{ $request->id_colonia }}");
            OnChangeColony("{{ $request->id_calle }}", "{{ $request->id_cruce1 }}", "{{ $request->id_cruce2 }}");

            var request_date_day = "{{ explode('-', $request->fecha_solicitud)[2] }}",
                request_date_month = "{{ explode('-', $request->fecha_solicitud)[1] }}",
                request_date_year = "{{ explode('-', $request->fecha_solicitud)[0] }}",
                status_licencia = "{{ $request->estatus_licencia }}",
                inmueble_sector = "{{ $request->pc_inmueble_sector_id }}",
                tipo_establecimiento = "{{ $request->pc_tipo_establecimiento_id }}";

            $('[name="fecha_solicitud_day"]').val(request_date_day);
            $('[name="fecha_solicitud_month"]').val(request_date_month);
            $('[name="fecha_solicitud_year"]').val(request_date_year);
            $('[name="estatus_licencia"]').val(status_licencia);
            $('[name="inmueble_sector"]').val(inmueble_sector);
            $('[name="tipo_establecimiento"]').val(tipo_establecimiento);

            @if (session()->has('document_uploaded'))
                $('#myTab li a#inspection-tab').tab('show');
            @endif

            @if (!empty($request->expedient))
                var expedienteId = "{{ $request->pc_expediente_id }}",
                    expediente = "{{ $request->expedient->no_expediente }} - {{ $request->expedient->razon_social }} - {{ $request->expedient->nombre_comercial }}"
                    expedienteOption = '<option value="'+expedienteId+'">'+expediente+'</option>';

                $('[name="no_expediente"]').append(expedienteOption);

                readEpedientDetail();
            @endif
        });

        function OnChangeCity(colony = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.colonies.by.city') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#colony").empty();
                $("#colony").append("<option value='0'>Seleccione una Colonia</option>");
                result.forEach(element => {
                    $("#colony").append($('<option>', {
                        value: element.IdColonia,
                        text: element.NombreColonia
                    }));
                });
                if(colony)
                {
                    $("#colony").val(colony).trigger('change.select2');
                }
            });
        }

        function OnChangeColony(calle = null, calle1 = null, calle2 = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.calles.by.colony') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#domicilio2").empty();
                $("#domicilio2").append("<option value='0'>Seleccione una Calle</option>");

                $("#calle1_u").empty();
                $("#calle1_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                $("#calle2_u").empty();
                $("#calle2_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                result.forEach(element => {
                    $("#domicilio2").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle1_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle2_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));
                });
                if(calle || calle1 || calle2)
                {
                    $("#domicilio2").val(calle).trigger('change.select2');
                    $("#calle1_u").val(calle1).trigger('change.select2');
                    $("#calle2_u").val(calle2).trigger('change.select2');
                }
            });
        }

        function readEpedientDetail()
        {
            var field = $('[name="no_expediente"]'),
                expedientId = field.val(),
                token = "{{ csrf_token() }}";

            if (expedientId != '')
            {
                $.ajax({
                    type: 'POST',
                    url: "{{ route('pc.ajax.expedient.find') }}",
                    data: {
                        expedientId: expedientId,
                        _token: token
                    },
                    success: function(result) {
                        console.log(result.data.expedient);

                        var modal = $('#expedientDetailModal');

                        modal.find('[name="expedientId"]').val(result.data.expedient.id);
                        modal.find('[name="no_expediente"]').val(result.data.expedient.no_expediente);
                        modal.find('[name="grupo"]').val(result.data.expedient.grupo);
                        modal.find('[name="domicilio"]').val(result.data.expedient.domicilio);
                        modal.find('[name="numero"]').val(result.data.expedient.numero);
                        modal.find('[name="colonia"]').val(result.data.expedient.colonia);
                        modal.find('[name="localidad"]').val(result.data.expedient.localidad);
                        modal.find('[name="razon_social"]').val(result.data.expedient.razon_social);
                        modal.find('[name="rfc"]').val(result.data.expedient.rfc);
                        modal.find('[name="nombre_comercial"]').val(result.data.expedient.nombre_comercial);
                        modal.find('[name="telefono"]').val(result.data.expedient.telefono);
                        modal.find('[name="correo"]').val(result.data.expedient.correo);
                        modal.find('[name="estado"]').val(result.data.expedient.estado);
                        modal.find('[name="observaciones"]').val(result.data.expedient.observaciones);
                    }
                })
            }
        }

        $(function(){
            $("#menu_proteccion_civil").addClass('active');
            // $('#update_persona').bootstrapToggle('on')
            var utm = "+proj=utm +zone=13";
            var wgs84 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
            var east = parseFloat($("#este").val().replace(",", "."));
            var nort = parseFloat($("#norte").val().replace(",", "."));

            var coordinates = proj4(utm,wgs84,[ east,nort]);
            var lat_and_long = {lat: coordinates[1], lng: coordinates[0]};
            var created = false;
            var map = L.map('map',{scrollWheelZoom:true}).setView([coordinates[1], coordinates[0]], 16);
            // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            //     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap Saul Moncivais</a> contributors'
            // }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:Vuelo_Bajo_Ortofoto@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 12,
                maxZoom: 23,
                attribution: '&copy; IMPLAN - Ortofoto Bahia de Banderas 2019'
            }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:vialidad_BB@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);

            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:lim_loc_final@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);
            var theMarker = {};
            theMarker = L.marker(lat_and_long).addTo(map);

            map.on('click',function(e){
                if (theMarker != undefined) {
                    map.removeLayer(theMarker);
                };
                theMarker = L.marker(e.latlng).addTo(map);
                $("#este").val(theMarker.getLatLng().utm().x)
                $("#norte").val(theMarker.getLatLng().utm().y)
            });
            
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var target = e.target.hash,
                    flag = true;

                switch (e.relatedTarget.hash) {
                    case "#inspection":
                        // $("#inspection").find(".required").each(function() {
                        //     if(!$(this).val()){
                        //         flag = false;
                        //         $(this).addClass("is-invalid");
                        //         $(this).parent().find(".select2").addClass("is-invalid");
                        //     }else{
                        //         if($(this).hasClass("is-invalid")){
                        //             $(this).removeClass("is-invalid");
                        //             $(this).parent().find(".select2").removeClass("is-invalid");
                        //         }
                        //     }
                        // });
                        // if(!flag)
                        // {
                        //     e.preventDefault();
                        //     $.alert({
                        //         title: 'Alerta!',
                        //         content: 'Por favor complete los datos del solicitante para continuar!',
                        //         type: "red",
                        //         buttons: {
                        //             Aceptar:{
                        //                 text: "Aceptar",
                        //             }
                        //         }
                        //     });
                        // }
                        break;

                    case "#requirements":
                        break;

                    case "#data":
                        break;
                
                    case "#location":
                        break;
                }

                if(flag)
                {
                    switch (e.target.hash) {
                        case "#location":
                            $("#btn-previous").fadeOut("fast");
                            
                            $("#btn-next").fadeIn("fast", function(){
                                $("#btn-next").data("target", "#data");
                            });

                            $("#btn-reject-request").fadeOut("fast");
                            $("#btn-approve-request").fadeOut("fast");
                            break;
                        case "#data":
                            $("#btn-previous").fadeIn("fast", function(){
                                $("#btn-previous").data("target", "#location")
                            });
                            $("#btn-next").fadeIn("fast", function(){
                                $("#btn-next").data("target", "#requirements");

                                $("#btn-reject-request").fadeOut("fast");
                                $("#btn-approve-request").fadeOut("fast");
                            });

                            break;
                        case "#requirements":
                            $("#btn-previous").fadeIn("fast", function(){
                                $("#btn-previous").data("target", "#data")
                            });

                            $("#btn-next").fadeIn("fast", function(){
                                $("#btn-next").data("target", "#inspection");

                                $("#btn-reject-request").fadeOut("fast");
                                $("#btn-approve-request").fadeOut("fast");
                            });

                            break;
                        case "#inspection":
                            $("#btn-previous").data("target", "#requirements");

                            $("#btn-next").fadeOut("fast", function() {
                                $("#btn-reject-request").fadeIn("fast");
                                $("#btn-approve-request").fadeIn("fast");
                            });

                            break;
                    }
                }
            });

            $(document).on("click", "#btn-reject-request", function(){
                let flag = true;
                $("#inspection").find(".required").each(function() {
                    if(!$(this).val() || $(this).val().length == 0){
                        flag = false;
                        $(this).addClass("is-invalid");
                        $(this).parent().find(".select2").addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                            $(this).parent().find(".select2").removeClass("is-invalid");
                        }
                    }
                });
                
                if(flag){
                    $.confirm({
                        title: 'Rechazar tramite!',
                        content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción rechazará la solicitud.</small>',
                        type: "red",
                        buttons: {
                            Aceptar: function () {
                                $('#form-data').find('[name="request_action"]').val('reject');
                                $('#form-data').submit();
                            },
                            Cancelar: function () {
                            },
                        }
                    });
                }else{
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor Verifique que la información sea correcta.!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                }
            });

            $(document).on("click", "#btn-approve-request", function(){
                let flag = true;
                $("#inspection").find(".required").each(function() {
                    if(!$(this).val() || $(this).val().length == 0){
                        flag = false;
                        $(this).addClass("is-invalid");
                        $(this).parent().find(".select2").addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                            $(this).parent().find(".select2").removeClass("is-invalid");
                        }
                    }
                });
                
                if(flag){
                    $.confirm({
                        title: 'Aprobar tramite!',
                        content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción aprobará el tramite.</small>',
                        type: "green",
                        buttons: {
                            Aceptar: function () {
                                $('#form-data').find('[name="request_action"]').val('approve');
                                $('#form-data').submit();
                            },
                            Cancelar: function () {
                            },
                        }
                    });
                }else{
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor Verifique que la información sea correcta.!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                }
            });

            $(document).on("click", ".btn-next", function(){
                $('#myTab a[href="'+$(this).data("target")+'"]').tab('show');
            });

            $(document).on("change", "#city", function(){
                var id = $("#city").val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.colonies.by.city') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $("#colony").empty();
                    $("#colony").append("<option value=''>Seleccione una Colonia</option>");
                    result.forEach(element => {
                        $("#colony").append($('<option>', {
                            value: element.IdColonia,
                            text: element.NombreColonia
                        }));
                    });
                });
            });
            $(document).on("change", "#colony", function(){
                var id = $("#city").val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.calles.by.colony') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $("#domicilio2").empty();
                    $("#domicilio2").append("<option value=''>Seleccione una Calle</option>");

                    $("#calle1_u").empty();
                    $("#calle1_u").append("<option value=''>Seleccione una Entre Calle</option>");

                    $("#calle2_u").empty();
                    $("#calle2_u").append("<option value=''>Seleccione una Entre Calle</option>");

                    result.forEach(element => {
                        $("#domicilio2").append($('<option>', {
                            value: element.IdCalle,
                            text: element.NombreCalle
                        }));

                        $("#calle1_u").append($('<option>', {
                            value: element.IdCalle,
                            text: element.NombreCalle
                        }));

                        $("#calle2_u").append($('<option>', {
                            value: element.IdCalle,
                            text: element.NombreCalle
                        }));
                    });
                });
            });
            
            $(document).on("click", ".btn-other", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);

                parent.find("span.select2").fadeOut("slow", function(){
                    parent.append('<input type="text" class="form-control inputInserted '+_this.data("class")+'" id="new'+_this.data("name")+'" name="new'+_this.data("name")+'" placeholder="Escriba el nombre de la Calle"> ').show('slow');
                    _this.html('<i class="fas fa-minus-circle"></i>');
                    _this.removeClass("btn-other");
                    _this.addClass("remove-calle");
                    _this.attr("data-original-title", "Seleccionar").tooltip('show');
                });

                parent.find('select[data-required="true"]').removeClass('required');
            });

            $(document).on("click", ".remove-calle", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);

                parent.find(".inputInserted").fadeOut("slow", function(){
                    $(this).remove();
                    parent.find("span.select2").fadeIn("slow")
                    _this.html('<i class="fas fa-plus-circle"></i>')
                    _this.removeClass("remove-calle");
                    _this.addClass("btn-other");
                    _this.attr("data-original-title", "Añadir otro").tooltip('show');
                });

                parent.find('select[data-required="true"]').addClass('required');
            });

            /*
                Validaciones para los campos de dirección
            */
            $(document).on('change', '#colony', function() {
                $('#newcolony').val("");
            });

            $(document).on('change', '#newcolony', function() {
                $('#colony').val("").trigger('change.select2');
            });

            $(document).on('change', '#domicilio2', function() {
                $('#newdomicilio2').val("");
            });

            $(document).on('change', '#newdomicilio2', function() {
                $('#domicilio2').val("").trigger('change.select2');
            });

            $(document).on('change', '#calle1_u', function() {
                $('#newcalle1_u').val("");
            });

            $(document).on('change', '#newcalle1_u', function() {
                $('#calle1_u').val("").trigger('change.select2');
            });

            $(document).on('change', '#calle2_u', function() {
                $('#newcalle2_u').val("");
            });

            $(document).on('change', '#newcalle2_u', function() {
                $('#calle2_u').val("").trigger('change.select2');
            });

            /*
                Actualizar marcador del mapa
            */
            $(document).on("keyup", '#ext', function(event){
                if(event.keyCode == 13){
                    getLocationFromAddress()
                }
            });

            $(document).on('change', '#city, #colony, #newcolony, #domicilio2, #newdomicilio2, #ext', function() {
                getLocationFromAddress();
            });

            function getLocationFromAddress(){
                var address = "",
                    countFields = 0;

                if($("#domicilio2 option:selected").text().length > 2 && $("#domicilio2").val() != "" && $("#domicilio2").val() != 0){
                    address = address + $("#domicilio2 option:selected").text();
                    countFields++;
                }else if ($("#newdomicilio2").val() != "")
                {
                    address = address +" "+ $("#newdomicilio2").val();
                    countFields++;
                }

                if($("#ext").val() != ""){
                    address = address +" "+ $("#ext").val();
                    countFields++;
                }

                if($("#colony option:selected").text().length > 2 && $("#colony").val() != "" && $("#colony option:selected").text() != "BAHIA DE BANDERAS" && $("#city option:selected").text() !="Seleccione una Colonia" ){
                    address = address + ", "+ $("#colony option:selected").text();
                    countFields++;
                }else if ($("#newcolony").val() != "")
                {
                    address = address +" "+ $("#newcolony").val();
                    countFields++;
                } 

                if($("#city option:selected").text().length > 2 && $("#city").val() != "" && $("#city option:selected").text() != "BAHIA DE BANDERAS" && $("#city option:selected").text() != "Seleccione una Colonia"){
                    address = address + ", "+ $("#city option:selected").text();
                    countFields++;
                }

                if(address != "" && countFields >= 4){
                    var address = address + ", Bahía de banderas";
                    console.log('Geo: '+address);

                    var geocoder = new google.maps.Geocoder();
                    var geocoderRequest = { address: address, componentRestrictions: {
                            country: "MX",
                            } };
                    geocoder.geocode(geocoderRequest, function(results, status){
                        window.geome = results;
                        if(results.length > 0){
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            };
                            
                            theMarker = L.marker({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}).addTo(map);
                            $("#este").val(theMarker.getLatLng().utm().x);
                            $("#norte").val(theMarker.getLatLng().utm().y);

                            map.setView({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}, 16);
                            
                        }else
                        {
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            }; 

                            $("#este").val(0);
                            $("#norte").val(0);

                            map.setView([20.74689, -105.39425], 12);
                        }
                    });
                }
            }

            $(document).on('change', '#service_type', function() {
                var service = $(this).val();

                if (service == 'otro')
                {
                    $('#other_service_type').removeAttr('disabled');
                    $('#other_service_type').addClass('required');
                }else
                {
                    $('#other_service_type').attr('disabled', true);
                    $('#other_service_type').removeClass('required');
                }
            });

            $('#modal-update-requirement').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    requirementId = button.data('requirementid'),
                    requirementName = button.data('requirement'),
                    modal = $(this);

                modal.find('[name="requirementId"]').val(requirementId);
                modal.find('#requirementName').val(requirementName);
            });

            $('.js-data-no-expediente-ajax').select2({
                minimumInputLength: 1,
                placeholder: "No. Expediente",
                allowClear: true,
                theme: "bootstrap",
                language: "es",
                ajax: {
                    url: "{{ route('pc.ajax.expedients') }}",
                    type: 'post',
                    dataType: 'json',
                    delay: 400,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _token: "{{ csrf_token() }}"
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data.data.expedients
                        };
                    }
                }
            });

            $('.js-data-no-expediente-ajax').on('select2:select', function (e) {
                readEpedientDetail();
            });
        });
    </script>
@endsection