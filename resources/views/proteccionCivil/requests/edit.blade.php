@extends('layouts.index')

@section('title') Solicitud: {{ $request->requestNumberText() }} ({{ $request->status() }})@endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
<div class="panel-top">
    <div class="d-flex justify-content-between">
        <h3>Solicitud: {{ $request->requestNumberText() }}</h3>
        <h3>Estatus: <span class="badge badge-secondary ml-3">{{ $request->status() }}</span></h3>
    </div>
</div>
<div class="panel-body with-buttons">
    <div class="margin-fix panel-row-fluid">
        @if(session()->has('alert'))
            <div class="alert alert-primary" role="alert">
                {{ session("alert") }}
            </div>
        @endif
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link btn-link active" id="period-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Ubicación</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="data-tab" data-toggle="tab" href="#data" role="tab" aria-controls="data" aria-selected="true">Solicitud</a>
            </li>
            <li class="nav-item">
                <a class="nav-link btn-link " id="requirements-tab" data-toggle="tab" href="#requirements" role="tab" aria-controls="requirements" aria-selected="true">Requisitos</a>
            </li>
        </ul>
        <form id="form-data" action="{{ route('pc.requests.update') }}" method="POST" class="tab-content">
            @csrf
            @method('PUT')
            <input type="hidden" name="requestId" value="{{ $request->id }}">
            <input type="hidden" name="request_action" value="update">
            <input type="hidden" name="redirect" value="{{ !empty($_GET['redirect'])? $_GET['redirect'] : '' }}">
            <div class="tab-pane fade show active" id="location" role="tabpanel" aria-labelledby="location-tab">
                <div class="form-group mt-1 mb-1 row margin-10 ">
                    <label for="city" class="col-sm-2 col-form-label">Poblacion <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <select name="city" id="city" class="required select2 form-control {{ $errors->has('city') ? ' is-invalid' : '' }}">
                            <option value="">Seleccione una Población</option>
                            @foreach ($poblaciones as $poblacion)
                                <option value="{{ $poblacion->IdPoblacion }}" data-city="{{ $poblacion->NombrePoblacion }}" {{ $request->id_poblacion == $poblacion->IdPoblacion? 'selected' : '' }}>{{ $poblacion->NombrePoblacion }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('city'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10 ">
                    <label for="colony" class="col-sm-2 col-form-label">Colonia <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="colony" data-class="required" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir otro"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            <select name="colony" id="colony" data-required="true" class="required select2 form-control {{ $errors->has('colony') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Colonia</option>
                            </select>
                            @if ($errors->has('colony'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('colony') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="domicilio2" class="col-sm-2 col-form-label">Calle <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="domicilio2" data-class="required" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir otro"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            <select name="domicilio2" id="domicilio2" data-required="true" class="required select2 form-control {{ $errors->has('domicilio2') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Calle</option>
                            </select>
                            @if ($errors->has('domicilio2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('domicilio2') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                
                <div class="form-group mt-1 mb-1 row margin-10">
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="calle1_u" class="col-sm-2 col-form-label">Entre Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="calle1_u" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir otro"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            <select name="calle1_u" id="calle1_u" class="select2 form-control {{ $errors->has('calle1_u') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle1_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle1_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="ext" class="col-sm-2 col-form-label">Exterior <span class="text-danger">*</span></label>
                    <div class="col-sm-2">
                        <input type="text" class="required form-control {{ $errors->has('ext') ? ' is-invalid' : '' }}" id="ext" name="ext" value="{{ $request->no_exterior }}">
                        @if ($errors->has('ext'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('ext') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="calle2_u" class="col-sm-2 col-form-label">Y Calle</label>
                    <div class="col-sm-6">
                        <div class="input-group ">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-primary btn-other" data-name="calle2_u" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir otro"><i class="fas fa-plus-circle"></i></button>
                            </div>
                            <select name="calle2_u" id="calle2_u" class="select2 form-control {{ $errors->has('calle2_u') ? ' is-invalid' : '' }}">
                                <option value="">Seleccione una Entre Calle</option>
                            </select>
                            @if ($errors->has('calle2_u'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('calle2_u') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <label for="int" class="col-sm-2 col-form-label">Interior</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control {{ $errors->has('int') ? ' is-invalid' : '' }}" id="int" name="int" value="{{ $request->no_interior }}">
                        @if ($errors->has('int'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('int') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <label for="este" class="col-sm-2 col-form-label">Coordenada Este</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="required form-control" id="este" name="este" value="{{ $request->coordenada_este }}">
                    </div>
                    <label for="norte" class="col-sm-2 col-form-label">Coordenada Norte</label>
                    <div class="col-sm-4">
                        <input readonly type="number" class="required form-control" id="norte" name="norte" value="{{ $request->coordenada_norte }}">
                    </div>
                </div>
                <div id="map" class="map" style="width:100%; height:500px;"></div>
                {{-- <div class="row margin-10 justify-content-end">
                    <button type="submit" class="btn btn-primary mr-2   btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
                </div> --}}
            </div>

            <div class="tab-pane fade " id="data" role="tabpanel" aria-labelledby="data-tab">
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Solicitud</legend>

                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="origen">Origen </label>
                            <input type="text" class="required form-control " id="origen" name="origen" value="{{ $request->origen }}" readonly>
                        </div>
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="fecha_solicitud">Fecha <span class="text-danger">*</span></label>
                            <div class="form-row">
                                <div class="col-3">
                                    <select name="fecha_solicitud_day" class="form-control required {{ $errors->has('fecha_solicitud_day') ? ' is-invalid' : '' }}" id="">
                                        <option value="">Seleccionar</option>
                                        @for ($i = 1; $i <= 31; $i++)
                                            <option value="{{ $i < 10? '0'.$i : $i }}">{{ $i < 10? '0'.$i : $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-3">
                                    <select name="fecha_solicitud_month" class="form-control required {{ $errors->has('fecha_solicitud_month') ? ' is-invalid' : '' }}" id="">
                                        <option value="">Seleccionar</option>
                                        <option value="01">Enero</option>
                                        <option value="02">Febrero</option>
                                        <option value="03">Marzo</option>
                                        <option value="04">Abril</option>
                                        <option value="05">Mayo</option>
                                        <option value="06">Junio</option>
                                        <option value="07">Julio</option>
                                        <option value="08">Agosto</option>
                                        <option value="09">Septiembre</option>
                                        <option value="10">Octubre</option>
                                        <option value="11">Noviembre</option>
                                        <option value="12">Diciembre</option>
                                    </select>
                                </div>
                                <div class="col-3">
                                    <select name="fecha_solicitud_year" class="form-control required {{ $errors->has('fecha_solicitud_year') ? ' is-invalid' : '' }}" id="">
                                        <option value="">Seleccionar</option>
                                        <option value="{{ now()->subYear()->year }}">{{ now()->subYear()->year }}</option>
                                        <option value="{{ now()->year }}" selected>{{ now()->year }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="estatus_licencia">Estatus de la Licencia <span class="text-danger">*</span></label>
                            <select name="estatus_licencia" class="form-control required {{ $errors->has('estatus_licencia') ? ' is-invalid' : '' }}" id="">
                                <option value="Nueva Licencia">Nueva Licencia</option>
                                <option value="Refrendo">Refrendo</option>
                                <option value="N/A">N/A</option>
                            </select>
                        </div>
                        <div class="form-group mt-1 mb-1 col-4">
                            <label for="no_licencia">No. de Licencia Municipal</label>
                            <input type="text" maxlength="50" class="form-control {{ $errors->has('no_licencia') ? ' is-invalid' : '' }}" id="no_licencia" name="no_licencia" value="{{ old('no_licencia', $request->no_licencia) }}" >
                            @if ($errors->has('no_licencia'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('no_licencia') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="nombre">Nombre o Razón Social <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('nombre') ? ' is-invalid' : '' }}" id="nombre" name="nombre" value="{{ old('nombre', $request->nombre) }}" >
                            @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="rfc">R.F.C.</label>
                            <input maxlength="50" type="text" class="form-control {{ $errors->has('rfc') ? ' is-invalid' : '' }}" id="rfc" name="rfc" value="{{ old('rfc', $request->rfc) }}">
                            @if ($errors->has('rfc'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('rfc') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="curp">CURP <span class="text-danger">*</span></label>
                            <input maxlength="50" type="text" class="required form-control {{ $errors->has('curp') ? ' is-invalid' : '' }}" id="curp" name="curp" value="{{ old('curp', $request->curp) }}">
                            @if ($errors->has('curp'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('curp') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="nombre_comercial">Nombre comercial <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('nombre_comercial') ? ' is-invalid' : '' }}" id="nombre_comercial" name="nombre_comercial" value="{{ old('nombre_comercial', $request->nombre_comercial) }}" >
                            @if ($errors->has('nombre_comercial'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre_comercial') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="no_empleados">No. de Empleados que laboran en el Inmueble <span class="text-danger">*</span></label>
                            <input type="number" class="required form-control {{ $errors->has('no_empleados') ? ' is-invalid' : '' }}" id="no_empleados" name="no_empleados" value="{{ old('no_empleados', $request->no_empleados) }}" >
                            @if ($errors->has('no_empleados'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('no_empleados') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="no_bombas_gasolinera">No. de Bombas en Gasolinera</label>
                            <input type="number" class="form-control {{ $errors->has('no_bombas_gasolinera') ? ' is-invalid' : '' }}" id="no_bombas_gasolinera" name="no_bombas_gasolinera" value="{{ old('no_bombas_gasolinera', $request->no_bombas_gasolinera) }}" min="0">
                            @if ($errors->has('no_bombas_gasolinera'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('no_bombas_gasolinera') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="horario_laboral">Horario y días laborales <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('horario_laboral') ? ' is-invalid' : '' }}" id="horario_laboral" name="horario_laboral" value="{{ old('horario_laboral', $request->horario_laboral) }}">
                            @if ($errors->has('horario_laboral'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('horario_laboral') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="nombre_solicitante">Nombre del Solicitante <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('nombre_solicitante') ? ' is-invalid' : '' }}" id="nombre_solicitante" name="nombre_solicitante" value="{{ old('nombre_solicitante', $request->nombre_solicitante) }}" >
                            @if ($errors->has('nombre_solicitante'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre_solicitante') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="caracter_juridico_solicitante">Caracter Juridico del Solicitante <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('caracter_juridico_solicitante') ? ' is-invalid' : '' }}" id="caracter_juridico_solicitante" name="caracter_juridico_solicitante" value="{{ old('caracter_juridico_solicitante', $request->caracter_juridico_solicitante) }}">
                            @if ($errors->has('caracter_juridico_solicitante'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('caracter_juridico_solicitante') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <label for="telefono_celular">Telefono celular <span class="text-danger">*</span></label>
                            <input type="text" class="required form-control {{ $errors->has('telefono_celular') ? ' is-invalid' : '' }}" id="telefono_celular" name="telefono_celular" value="{{ old('telefono_celular', $request->telefono_celular) }}">
                            @if ($errors->has('telefono_celular'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono_celular') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="telefono_fijo">Telefono fijo</label>
                            <input maxlength="50" type="text" class="form-control {{ $errors->has('telefono_fijo') ? ' is-invalid' : '' }}" id="telefono_fijo" name="telefono_fijo" value="{{ old('telefono_fijo', $request->telefono_fijo) }}">
                            @if ($errors->has('telefono_fijo'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('telefono_fijo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-1 mb-1 col">
                            <label for="email">Email</label>
                            <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email', $request->email) }}">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <br>
                </fieldset>
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Giros</legend>
                    <div class="form-row">
                        <div class="form-group mt-1 mb-1 col-4">
                            <select id="cuenta_serach" name="giro" class="js-data-turns-ajax form-control"></select>
                            @if ($errors->has('giro'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('giro') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="col-md-8 d-flex align-items-end">
                            <button type="button" class="btn btn-success btn-sm mb-1 add-turn"><i class="fas fa-plus"></i></button>
                        </div>
                    </div>
                    <table class="table table-sm turns-table">
                        <tbody>
                            @foreach ($request->turns as $turn)
                            <tr class="turn-row">
                                <td>
                                    <input type="hidden" name="turns[]" value="{{ $turn->id_giro }}">
                                    {{ $turn->giro }} - #{{ $turn->id_giro }}
                                    <a href="javascript:;" class="text-danger ml-2 btn-remove-turn"><i class="fas fa-trash-alt"></i></a>
                                </td>    
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                </fieldset>
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Tramites</legend>
                    <div class="alert alert-danger alert-formalities" style="display: none;">Por favor seleccione al menos un tramite para continuar!</div>
                    <div class="form-row">
                        @foreach ($formalities as $formality)
                            <div class="form-group mt-1 mb-1 col-md-6">
                                <input type="checkbox" class="" id="formality_{{ $formality->id }}" name="formalities[]" value="{{ $formality->id }}" required>
                                <label for="formality_{{ $formality->id }}">{{ $formality->tramite }}</label>
                            </div>
                        @endforeach
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group mt-1 mb-1 col">
                            <input type="checkbox" class="" id="other_formality" name="formalities[]" value="otro" {{ !empty($request->otro_tramite)? 'checked' : '' }}>
                            <label for="other_formality">Otro tramite</label>
                            <input type="text" class="form-control {{ $errors->has('other_formality') ? ' is-invalid' : '' }}" name="other_formality" value="{{ old('other_formality', $request->otro_tramite) }}" {{ !empty($request->otro_tramite)? '' : 'disabled' }}>
                        </div>
                    </div> --}}
                    <br>
                </fieldset>
            </div>

            <div class="tab-pane fade " id="requirements" role="tabpanel" aria-labelledby="requirements-tab">
                <fieldset class="col-xl-12 col-sm-12 fieldset">
                    <legend>Requisitos</legend>

                    <table class="table table-striped table-sm">
                        <thead>
                            <tr>
                                <th width="80%">Requisito</th>
                                <th width="10%" class="text-center">Estatus</th>
                                <th width="10%" class="text-center">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($requirements as $requirement)
                                <tr>
                                    <td>{{ $requirement->requisito }}</td>
                                    <td class="text-center text-white {{ $request->requirementStatus($requirement->id) == 'Pendiente' ? 'bg-warning' : 'bg-success' }}">{{ $request->requirementStatus($requirement->id) }}</td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary btn-sms" data-toggle="modal" data-target="#modal-update-requirement" data-requirementid="{{ $requirement->id }}" data-requirement="{{ $requirement->requisito }}"><i class="fas fa-edit"></i></button>
                                        @if ($request->requirementStatus($requirement->id) != config('system.pc.requirements.statuses.pending'))
                                            <a href="{{ asset('storage/'.$request->requirements()->where('id_requisito', $requirement->id)->first()->documento) }}" target="_blank" class="btn btn-secondary"><i class="fas fa-download"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                </fieldset>
            </div>
        </form>
    </div>
</div>
<div class="row justify-content-end panel-buttoms">
    <a target="_blank" href="{{ route('pc.requests.print', $request->id) }}" id="" class="btn btn-info mr-3"><i class="fa fa-print"></i> Formato de Solicitud</a>
    <button type="submit" class="btn btn-primary mr-3" id="btn-save-changes" style="display:none;"><i class="fas fa-save mr-2"></i> Guardar Cambios</button>
    <button type="button" class="btn btn-danger mr-3" id="btn-cancel-request" style="display:none;"><i class="fas fa-times-circle mr-2"></i> Cancelar Solicitud</button>
    <button type="button" class="btn btn-primary mr-3" id="btn-apply-request" style="display:none;"><i class="fas fa-paper-plane"></i> Aplicar tramite</button>
    <button type="button" class="btn btn-primary mr-3 btn-next " id="btn-previous" style="display:none;" data-target="#data">Anterior <i class="fas fa-arrow-left ml-2"></i></button>
    <button type="button" class="btn btn-primary mr-3 btn-next" id="btn-next" data-target="#data">Siguiente <i class="fas fa-arrow-right ml-2"></i></button>
</div>
@endsection

@section('modals')
    @include('partials.modals.proteccionCivil.modal_update_requirement')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ config('system.google.maps.apiKey') }}&libraries=places&v=weekly"
        defer
    ></script>

    <script>
        window.addEventListener('load', function () {
            OnChangeCity("{{ $request->id_colonia }}");
            OnChangeColony("{{ $request->id_calle }}", "{{ $request->id_cruce1 }}", "{{ $request->id_cruce2 }}");

            var request_date_day = "{{ explode('-', $request->fecha_solicitud)[2] }}",
                request_date_month = "{{ explode('-', $request->fecha_solicitud)[1] }}",
                request_date_year = "{{ explode('-', $request->fecha_solicitud)[0] }}",
                status_licencia = "{{ $request->estatus_licencia }}";

            $('[name="fecha_solicitud_day"]').val(request_date_day);
            $('[name="fecha_solicitud_month"]').val(request_date_month);
            $('[name="fecha_solicitud_year"]').val(request_date_year);
            $('[name="estatus_licencia"]').val(status_licencia);

            @foreach ($request->formalities as $formality)
                var formalityId = "{{ $formality->id_tramite }}";

                $('[name="formalities[]"][value="'+formalityId+'"]').attr('checked', true);
            @endforeach
        });

        function OnChangeCity(colony = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.colonies.by.city') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#colony").empty();
                $("#colony").append("<option value='0'>Seleccione una Colonia</option>");
                result.forEach(element => {
                    $("#colony").append($('<option>', {
                        value: element.IdColonia,
                        text: element.NombreColonia
                    }));
                });
                if(colony)
                {
                    $("#colony").val(colony).trigger('change.select2');
                }
            });
        }

        function OnChangeColony(calle = null, calle1 = null, calle2 = null)
        {
            var id = $("#city").val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.calles.by.colony') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                },
            }).done(function(result){
                $("#domicilio2").empty();
                $("#domicilio2").append("<option value='0'>Seleccione una Calle</option>");

                $("#calle1_u").empty();
                $("#calle1_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                $("#calle2_u").empty();
                $("#calle2_u").append("<option value='0'>Seleccione una Entre Calle</option>");

                result.forEach(element => {
                    $("#domicilio2").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle1_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));

                    $("#calle2_u").append($('<option>', {
                        value: element.IdCalle,
                        text: element.NombreCalle
                    }));
                });
                if(calle || calle1 || calle2)
                {
                    $("#domicilio2").val(calle).trigger('change.select2');
                    if (calle1 != '')
                    {
                        $("#calle1_u").val(calle1).trigger('change.select2');
                    }

                    if (calle2 != '')
                    {
                        $("#calle2_u").val(calle2).trigger('change.select2');
                    }
                }
            });
        }

        $(function(){
            $("#menu_proteccion_civil").addClass('active');

            @if (session()->has('requirement_uploaded'))
                $('#myTab li a#requirements-tab').tab('show');
            @endif

            // $('#update_persona').bootstrapToggle('on')
            var utm = "+proj=utm +zone=13";
            var wgs84 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
            var east = parseFloat($("#este").val().replace(",", "."));
            var nort = parseFloat($("#norte").val().replace(",", "."));

            var coordinates = proj4(utm,wgs84,[ east,nort]);
            var lat_and_long = {lat: coordinates[1], lng: coordinates[0]};
            var created = false;
            var map = L.map('map',{scrollWheelZoom:true}).setView([coordinates[1], coordinates[0]], 16);
            // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            //     attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap Saul Moncivais</a> contributors'
            // }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:Vuelo_Bajo_Ortofoto@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 12,
                maxZoom: 23,
                attribution: '&copy; IMPLAN - Ortofoto Bahia de Banderas 2019'
            }).addTo(map);
            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:vialidad_BB@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);

            L.tileLayer('http://187.188.190.48:8080/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:lim_loc_final@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 18,
                maxZoom: 23,
            }).addTo(map);
            var theMarker = {};
            theMarker = L.marker(lat_and_long).addTo(map);

            map.on('click',function(e){
                if (theMarker != undefined) {
                    map.removeLayer(theMarker);
                };
                theMarker = L.marker(e.latlng).addTo(map);
                $("#este").val(theMarker.getLatLng().utm().x)
                $("#norte").val(theMarker.getLatLng().utm().y)
            });
            
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
                var target = e.target.hash,
                    flag = true,
                    formalities = 0;

                switch (e.relatedTarget.hash) {
                    case "#requirements":
                        break;
                    case "#data":
                        $("#data").find(".required").each(function() {
                            if(!$(this).val()){
                                flag = false;
                                $(this).addClass("is-invalid");
                                $(this).parent().find(".select2").addClass("is-invalid");
                            }else{
                                if($(this).hasClass("is-invalid")){
                                    $(this).removeClass("is-invalid");
                                    $(this).parent().find(".select2").removeClass("is-invalid");
                                }
                            }
                        });

                        $('#data').find('[name="formalities[]"]').each(function() {
                            var value = $(this).val();

                            if ($(this).is(':checked'))
                            {
                                if (value == 'otro')
                                {
                                    var otherFormalityValue = $('[name="other_formality"]').val();
                                    if (otherFormalityValue != '')
                                    {
                                        formalities++;
                                    }
                                }else
                                {
                                    formalities++;
                                }
                            }
                        });

                        if(!flag)
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor complete los datos del solicitante para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }else if (formalities == 0)
                        {
                            flag = false;
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione al menos un tramite para continuar!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });

                            $('.alert-formalities').slideDown(function() {
                                setTimeout(() => {
                                    $('.alert-formalities').slideUp();
                                }, 8000);   
                            });
                        }

                        break;
                
                    case "#location":
                        $("#location").find(".required").each(function() {
                            if(!$(this).val()){
                                flag = false;
                                $(this).addClass("is-invalid");
                                $(this).parent().find(".select2").addClass("is-invalid");
                            }else{
                                if($(this).hasClass("is-invalid")){
                                    $(this).removeClass("is-invalid");
                                    $(this).parent().find(".select2").removeClass("is-invalid");
                                }
                            }
                        });
                        
                        if(!flag)
                        {
                            e.preventDefault();
                            $.alert({
                                title: 'Alerta!',
                                content: 'Por favor seleccione la ubicación para continuar y/o Verifique que la dirección sea correcta.!',
                                type: "red",
                                buttons: {
                                    Aceptar:{
                                        text: "Aceptar",
                                    }
                                }
                            });
                        }
                        
                        break;
                }

                if(flag)
                {
                    switch (e.target.hash) {
                        case "#location":
                            $("#btn-previous").fadeOut("fast");
                            
                            $("#btn-next").fadeIn("fast", function(){
                                $("#btn-next").data("target", "#data");
                            });

                            $("#btn-save-changes").fadeOut("fast");
                            $("#btn-apply-request").fadeOut("fast");
                            $("#btn-cancel-request").fadeOut("fast");
                            break;
                        case "#data":
                            $("#btn-previous").fadeIn("fast", function(){
                                $("#btn-previous").data("target", "#location")
                            });
                            $("#btn-next").fadeIn("fast", function(){
                                $("#btn-next").data("target", "#requirements");

                                $("#btn-save-changes").fadeOut("fast");
                                $("#btn-apply-request").fadeOut("fast");
                                $("#btn-cancel-request").fadeOut("fast");
                            });

                            break;
                        case "#requirements":
                            $("#btn-previous").data("target", "#data");

                            $("#btn-next").fadeOut("fast", function() {
                                $("#btn-save-changes").fadeIn("fast");
                                $("#btn-apply-request").fadeIn("fast");
                                $("#btn-cancel-request").fadeIn("fast");
                            });

                            break;
                    }
                }
            });

            $(document).on("click", "#btn-save-changes", function(){
                let flag = true,
                    formalities = 0;

                $("#data").find(".required").each(function() {
                    if(!$(this).val() || $(this).val().length == 0){
                        flag = false;
                        $(this).addClass("is-invalid");
                        $(this).parent().find(".select2").addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                            $(this).parent().find(".select2").removeClass("is-invalid");
                        }
                    }
                });

                $('#data').find('[name="formalities[]"]').each(function() {
                    var value = $(this).val();

                    if ($(this).is(':checked'))
                    {
                        if (value == 'otro')
                        {
                            var otherFormalityValue = $('[name="other_formality"]').val();
                            if (otherFormalityValue != '')
                            {
                                formalities++;
                            }
                        }else
                        {
                            formalities++;
                        }
                    }
                });
                
                if (formalities == 0)
                {
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor seleccione al menos un tramite para continuar!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });

                    $('.alert-formalities').slideDown(function() {
                        setTimeout(() => {
                            $('.alert-formalities').slideUp();
                        }, 8000);   
                    });
                }else if(flag){
                    $.confirm({
                        title: 'Guardar cambios!',
                        content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción solo actualizará la información de la solicitud.</small>',
                        type: "orange",
                        buttons: {
                            Aceptar: function () {
                                $('#form-data').find('[name="request_action"]').val('update');
                                $('#form-data').submit();
                            },
                            Cancelar: function () {
                            },
                        }
                    });
                }else{
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor Verifique que la información sea correcta.!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                }
            });

            $(document).on("click", "#btn-cancel-request", function(){
                $.confirm({
                    title: 'Cancelar tramite!',
                    content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción cancelará la solicitud.</small>',
                    type: "red",
                    buttons: {
                        Aceptar: function () {
                            $('#form-data').find('[name="request_action"]').val('cancel');
                            $('#form-data').submit();
                        },
                        Cancelar: function () {
                        },
                    }
                });
            });

            $(document).on("click", "#btn-apply-request", function(){
                let flag = true;
                $("#data").find(".required").each(function() {
                    if(!$(this).val() || $(this).val().length == 0){
                        flag = false;
                        $(this).addClass("is-invalid");
                        $(this).parent().find(".select2").addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                            $(this).parent().find(".select2").removeClass("is-invalid");
                        }
                    }
                });
                
                if(flag){
                    $.confirm({
                        title: 'Aplicar tramite!',
                        content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción enviará a inspección la solicitud.</small>',
                        type: "green",
                        buttons: {
                            Aceptar: function () {
                                $('#form-data').find('[name="request_action"]').val('apply');
                                $('#form-data').submit();
                            },
                            Cancelar: function () {
                            },
                        }
                    });
                }else{
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor Verifique que la información sea correcta.!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                }
            });

            $(document).on("click", ".btn-next", function(){
                $('#myTab a[href="'+$(this).data("target")+'"]').tab('show');
            });

            $(document).on("change", "#city", function(){
                var id = $("#city").val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.colonies.by.city') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $("#colony").empty();
                    $("#colony").append("<option value=''>Seleccione una Colonia</option>");
                    result.forEach(element => {
                        $("#colony").append($('<option>', {
                            value: element.IdColonia,
                            text: element.NombreColonia
                        }));
                    });
                });
            });
            $(document).on("change", "#colony", function(){
                var id = $("#city").val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.calles.by.colony') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    $("#domicilio2").empty();
                    $("#domicilio2").append("<option value=''>Seleccione una Calle</option>");

                    $("#calle1_u").empty();
                    $("#calle1_u").append("<option value=''>Seleccione una Entre Calle</option>");

                    $("#calle2_u").empty();
                    $("#calle2_u").append("<option value=''>Seleccione una Entre Calle</option>");

                    result.forEach(element => {
                        $("#domicilio2").append($('<option>', {
                            value: element.IdCalle,
                            text: element.NombreCalle
                        }));

                        $("#calle1_u").append($('<option>', {
                            value: element.IdCalle,
                            text: element.NombreCalle
                        }));

                        $("#calle2_u").append($('<option>', {
                            value: element.IdCalle,
                            text: element.NombreCalle
                        }));
                    });
                });
            });
            
            $(document).on("click", ".btn-other", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);

                parent.find("span.select2").fadeOut("slow", function(){
                    parent.append('<input type="text" class="form-control inputInserted '+_this.data("class")+'" id="new'+_this.data("name")+'" name="new'+_this.data("name")+'" placeholder="Escriba el nombre de la Calle"> ').show('slow');
                    _this.html('<i class="fas fa-minus-circle"></i>');
                    _this.removeClass("btn-other");
                    _this.addClass("remove-calle");
                    _this.attr("data-original-title", "Seleccionar").tooltip('show');
                });

                parent.find('select[data-required="true"]').removeClass('required');
            });

            $(document).on("click", ".remove-calle", function(){
                $('[data-toggle="tooltip"]').tooltip("hide");
                var parent = $(this).parents(".input-group");
                var _this = $(this);

                parent.find(".inputInserted").fadeOut("slow", function(){
                    $(this).remove();
                    parent.find("span.select2").fadeIn("slow")
                    _this.html('<i class="fas fa-plus-circle"></i>')
                    _this.removeClass("remove-calle");
                    _this.addClass("btn-other");
                    _this.attr("data-original-title", "Añadir otro").tooltip('show');
                });

                parent.find('select[data-required="true"]').addClass('required');
            });

            /*
                Validaciones para los campos de dirección
            */
            $(document).on('change', '#colony', function() {
                $('#newcolony').val("");
            });

            $(document).on('change', '#newcolony', function() {
                $('#colony').val("").trigger('change.select2');
            });

            $(document).on('change', '#domicilio2', function() {
                $('#newdomicilio2').val("");
            });

            $(document).on('change', '#newdomicilio2', function() {
                $('#domicilio2').val("").trigger('change.select2');
            });

            $(document).on('change', '#calle1_u', function() {
                $('#newcalle1_u').val("");
            });

            $(document).on('change', '#newcalle1_u', function() {
                $('#calle1_u').val("").trigger('change.select2');
            });

            $(document).on('change', '#calle2_u', function() {
                $('#newcalle2_u').val("");
            });

            $(document).on('change', '#newcalle2_u', function() {
                $('#calle2_u').val("").trigger('change.select2');
            });

            /*
                Actualizar marcador del mapa
            */
            $(document).on("keyup", '#ext', function(event){
                if(event.keyCode == 13){
                    getLocationFromAddress()
                }
            });

            $(document).on('change', '#city, #colony, #newcolony, #domicilio2, #newdomicilio2, #ext', function() {
                getLocationFromAddress();
            });

            function getLocationFromAddress(){
                var address = "",
                    countFields = 0;

                if($("#domicilio2 option:selected").text().length > 2 && $("#domicilio2").val() != "" && $("#domicilio2").val() != 0){
                    address = address + $("#domicilio2 option:selected").text();
                    countFields++;
                }else if ($("#newdomicilio2").val() != "")
                {
                    address = address +" "+ $("#newdomicilio2").val();
                    countFields++;
                }

                if($("#ext").val() != ""){
                    address = address +" "+ $("#ext").val();
                    countFields++;
                }

                if($("#colony option:selected").text().length > 2 && $("#colony").val() != "" && $("#colony option:selected").text() != "BAHIA DE BANDERAS" && $("#city option:selected").text() !="Seleccione una Colonia" ){
                    address = address + ", "+ $("#colony option:selected").text();
                    countFields++;
                }else if ($("#newcolony").val() != "")
                {
                    address = address +" "+ $("#newcolony").val();
                    countFields++;
                }

                if($("#city option:selected").text().length > 2 && $("#city").val() != "" && $("#city option:selected").text() != "BAHIA DE BANDERAS" && $("#city option:selected").text() != "Seleccione una Colonia"){
                    address = address + ", "+ $("#city option:selected").text();
                    countFields++;
                }

                if(address != "" && countFields >= 4){
                    var address = address + ", Bahía de banderas";
                    console.log('Geo: '+address);

                    var geocoder = new google.maps.Geocoder();
                    var geocoderRequest = { address: address, componentRestrictions: {
                            country: "MX",
                            } };
                    geocoder.geocode(geocoderRequest, function(results, status){
                        window.geome = results;
                        if(results.length > 0){
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            };
                            
                            theMarker = L.marker({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}).addTo(map);
                            $("#este").val(theMarker.getLatLng().utm().x);
                            $("#norte").val(theMarker.getLatLng().utm().y);

                            map.setView({lat: results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()}, 16);
                            
                        }else
                        {
                            if (theMarker != undefined) {
                                map.removeLayer(theMarker);
                            }; 

                            $("#este").val(0);
                            $("#norte").val(0);

                            map.setView([20.74689, -105.39425], 12);
                        }
                    });
                }
            }

            $(document).on('change', '#other_formality', function() {
                var otherFormality = $(this);

                if (otherFormality.is(':checked'))
                {
                    $('[name="other_formality"]').removeAttr('disabled');
                }else
                {
                    $('[name="other_formality"]').attr('disabled', true);
                }
            });

            $('#modal-update-requirement').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget),
                    requirementId = button.data('requirementid'),
                    requirementName = button.data('requirement'),
                    modal = $(this);

                modal.find('[name="requirementId"]').val(requirementId);
                modal.find('#requirementName').val(requirementName);
            });

            $('.js-data-turns-ajax').select2({
                minimumInputLength: 1,
                placeholder: "Giro",
                allowClear: true,
                theme: "bootstrap",
                language: "es",
                ajax: {
                    url: "{{ route('ajax.turns') }}",
                    type: 'post',
                    dataType: 'json',
                    delay: 400,
                    data: function (params) {
                        var query = {
                            search: params.term,
                            _token: "{{ csrf_token() }}"
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data.data.turns
                        };
                    }
                }
            });

            $(document).on('click', '.add-turn', function() {
                var turn = $('[name="giro"]').select2('data'),
                    turnName = turn[0].text,
                    turnId = turn[0].id,
                    turnRow = `<tr class="turn-row">
                                <td>
                                    <input type="hidden" name="turns[]" value="`+turnId+`">
                                    `+turnName+`
                                    <a href="javascript:;" class="text-danger ml-2 btn-remove-turn"><i class="fas fa-trash-alt"></i></a>
                                </td>    
                            </tr>`;

                // Debugear variable turns para saber que tiene
                // console.log(turn[0]);

                $('.turns-table').find('tbody').append(turnRow);
            });

            $(document).on('click', '.btn-remove-turn', function() {
                var turnRow = $(this).parents('.turn-row');
                turnRow.remove();
            });
        });
    </script>
@endsection