@extends('layouts.index')

@section('title') Nuevo Tramite @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Nuevo tramite</h3>
    </div>
</div>
<div class="panel-body @can('write_du_requests') with-buttons @endcan">
    <div class="margin-fix panel-row-fluid">
        <form id="form-data" action="{{ route('pc.formalities.store') }}" method="POST" class="tab-content">
            @csrf
            <fieldset class="col-xl-12 col-sm-12 fieldset">
                <legend>Tramite</legend>
                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col">
                        <label for="formality">Tramite <span class="text-danger">*</span></label>
                        <input maxlength="100" type="text" class="required form-control {{ $errors->has('formality') ? ' is-invalid' : '' }}" id="formality" name="formality" value="{{ old('formality') }}" >
                        @if ($errors->has('formality'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('formality') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <br>
            </fieldset>
            <fieldset class="col-xl-12 col-sm-12 fieldset">
                <legend>Costos por año</legend>
                <button class="btn btn-primary btn-add-costs" type="button"><i class="fas fa-plus-circle"></i> Agregar año</button>
                <br><br>
                <div class="form-row">
                    <table class="table table-striped table-costs">
                        <thead>
                            <tr>
                                <th>Año</th>
                                <th>Costo Nueva Licencia</th>
                                <th>Costo Refrendo</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="tr-cost" data-id="1">
                                <td><input type="text" name="years[]" class="form-control required" value="{{ now()->year }}" placeholder="{{ now()->year }}"></td>
                                <td>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">$</div>
                                        </div>
                                        <input type="text" name="costNewLicenses[]" class="form-control required" id="" value="465.00" placeholder="465.00">
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">$</div>
                                        </div>
                                        <input type="text" name="costRefrendos[]" class="form-control required" id="" value="275.00" placeholder="275.00">
                                    </div>
                                </td>
                                <td>
                                    <button class="btn btn-danger btn-remove-costs" type="button"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br>
            </fieldset>
        </form>
    </div>
</div>
@can('write_du_requests')
    <div class="row justify-content-end panel-buttoms">
        <button type="submit" class="btn btn-primary mr-3" id="btn-save-changes"><i class="fas fa-save mr-2"></i> Guardar Cambios</button>
    </div>
@endcan
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $("#menu_proteccion_civil").addClass('active');

            $(document).on('click', '.btn-add-costs', function() {
                var tr = '<tr class="tr-cost" data-id="1"> \
                                <td><input type="text" name="years[]"  class="form-control required" value="{{ now()->year }}" placeholder="{{ now()->year }}"></td> \
                                <td> \
                                    <div class="input-group mb-2"> \
                                        <div class="input-group-prepend"> \
                                            <div class="input-group-text">$</div> \
                                        </div> \
                                        <input type="text" name="costNewLicenses[]" class="form-control required" id="" value="465.00" placeholder="465.00"> \
                                    </div> \
                                </td> \
                                <td> \
                                    <div class="input-group mb-2"> \
                                        <div class="input-group-prepend"> \
                                            <div class="input-group-text">$</div> \
                                        </div> \
                                        <input type="text" name="costRefrendos[]" class="form-control required" id="" value="275.00" placeholder="275.00"> \
                                    </div> \
                                </td> \
                                <td> \
                                    <button class="btn btn-danger btn-remove-costs" type="button"><i class="fas fa-trash-alt"></i></button> \
                                </td> \
                            </tr>';

                $('.table-costs').find('tbody').append(tr);
            });

            $(document).on('click', '.btn-remove-costs', function() {
                $(this).parents('.tr-cost').remove();
            });
            
            $(document).on("click", "#btn-save-changes", function(){
                let flag = true;
                $("#form-data").find(".required").each(function() {
                    if(!$(this).val() || $(this).val().length == 0){
                        flag = false;
                        $(this).addClass("is-invalid");
                        $(this).parent().find(".select2").addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                            $(this).parent().find(".select2").removeClass("is-invalid");
                        }
                    }
                });
                
                if(flag){
                    $.confirm({
                        title: 'Confirmación!',
                        content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción genera el trámite.</small>',
                        type: "blue",
                        buttons: {
                            Aceptar: function () {
                                $('#form-data')[0].submit();
                            },
                            Cancelar: function () {
                            },
                        }
                    });
                }else{
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor Verifique que la información sea correcta.!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection