@extends('layouts.index')

@section('title') Tramites @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Tramites</h3>
        </div>
    </div>    
    {{-- @can('write_du_requests') --}}
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('pc.formalities.create') }}" class="btn btn-primary mr-3"><i class="fas fa-plus-circle mr-2"></i> Nuevo Tramite</a>
        </div>
    {{-- @endcan --}}
    <div class="panel-body with-buttons">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            
            <table style="width: 100%;" class="table dataTable table-sm table-hover">
                <thead>
                    <tr>
                        <th scope="col" style="width:10%;">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Costo</th>
                        <th scope="col">Costo Refrendo</th>
                        <th scope="col" style="width:5%;">Acciones</th>
                    </tr>
                </thead>
                <tbody class="small-font">
                    @foreach ($formalities as $formality)
                        <tr>
                            <td>{{ $formality->id }}</td>
                            <td>{{ $formality->tramite }}</td>
                            <td>${{ number_format($formality->currentCost(), 2) }}</td>
                            <td>${{ number_format($formality->currentCostRefrendo(), 2) }}</td>
                            <td>
                                <a href="{{ route('pc.formalities.edit', $formality->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        $(function(){
            $("#menu_proteccion_civil").addClass('active');
            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'pdfHtml5'
                ],
                "order": [[ 0, "asc" ]]
            });
        });
    </script>
@endsection