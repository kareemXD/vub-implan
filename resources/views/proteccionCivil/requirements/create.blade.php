@extends('layouts.index')

@section('title') Nuevo Requisito @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Nuevo Requisito</h3>
    </div>
</div>
<div class="panel-body @can('write_du_requests') with-buttons @endcan">
    <div class="margin-fix panel-row-fluid">
        <form id="form-data" action="{{ route('pc.requirements.store') }}" method="POST" class="tab-content">
            @csrf
            <fieldset class="col-xl-12 col-sm-12 fieldset">
                <legend>Requisito</legend>
                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col">
                        <label for="requirement">Requisito <span class="text-danger">*</span></label>
                        <input type="text" class="required form-control {{ $errors->has('requirement') ? ' is-invalid' : '' }}" id="requirement" name="requirement" value="{{ old('requirement') }}" >
                        @if ($errors->has('requirement'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('requirement') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <br>
            </fieldset>
        </form>
    </div>
</div>
@can('write_du_requests')
    <div class="row justify-content-end panel-buttoms">
        <button type="submit" class="btn btn-primary mr-3" id="btn-save-changes"><i class="fas fa-save mr-2"></i> Guardar Cambios</button>
    </div>
@endcan
@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $("#menu_proteccion_civil").addClass('active');

            $(document).on("click", "#btn-save-changes", function(){
                let flag = true;
                $("#form-data").find(".required").each(function() {
                    if(!$(this).val() || $(this).val().length == 0){
                        flag = false;
                        $(this).addClass("is-invalid");
                        $(this).parent().find(".select2").addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                            $(this).parent().find(".select2").removeClass("is-invalid");
                        }
                    }
                });
                
                if(flag){
                    $.confirm({
                        title: 'Confirmación!',
                        content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción generará el requisito.</small>',
                        type: "blue",
                        buttons: {
                            Aceptar: function () {
                                $('#form-data')[0].submit();
                            },
                            Cancelar: function () {
                            },
                        }
                    });
                }else{
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor Verifique que la información sea correcta.!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection