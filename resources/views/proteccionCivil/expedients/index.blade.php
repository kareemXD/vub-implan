@extends('layouts.index')

@section('title') Expedientes @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Expedientes</h3>
        </div>
    </div>    
    {{-- @can('write_du_requests') --}}
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('pc.expedients.create') }}" class="btn btn-primary mr-3"><i class="fas fa-plus-circle mr-2"></i> Nuevo expediente</a>
        </div>
    {{-- @endcan --}}
    <div class="panel-body with-buttons">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif

            <form id="searchForm" action="{{ route('pc.expedients') }}" method="GET" autocomplete="off">
            </form>
            
            <table style="width: 100%;" class="table dataTable table-sm table-hover">
                <thead>
                    <tr class="search-field">
                        <th scope="col" style="width:10%;"><input type="text" name="no_expediente" form="searchForm" style="font-size: 13px" value="{{ !empty(session('inputs')['no_expediente'])? session('inputs')['no_expediente'] : '' }}" placeholder="Expediente" class="form-control input-search" /></th>
                        <th scope="col"><input type="text" name="razon_social" form="searchForm" style="font-size: 13px" value="{{ !empty(session('inputs')['razon_social'])? session('inputs')['razon_social'] : '' }}" placeholder="Razon Social" class="form-control input-search" /></th>
                        <th scope="col"><input type="text" name="nombre_comercial" form="searchForm" style="font-size: 13px" value="{{ !empty(session('inputs')['nombre_comercial'])? session('inputs')['nombre_comercial'] : '' }}" placeholder="Nombre Comercial" class="form-control input-search" /></th>
                        <th scope="col">
                            <select name="grupo" class="form-control" form="searchForm" id="">
                                <option value="">Todos</option>
                                <option value="ORDINARIO">ORDINARIO</option>
                                <option value="DESARROLLO Y CONSTRUCCIÓN">DESARROLLO Y CONSTRUCCIÓN</option>
                                <option value="COMERCIO Y SERVICIOS">COMERCIO Y SERVICIOS</option>
                                <option value="EDUCACIONAL">EDUCACIONAL</option>
                                <option value="ALOJAMIENTO TEMPORAL">ALOJAMIENTO TEMPORAL</option>
                                <option value="LOCALIDAD">LOCALIDAD</option>
                                <option value="ATÍPICOS">ATÍPICOS</option>
                            </select>
                        </th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                        <th class="btn-search-table" style="width:5%;"><button type="submit" form="searchForm" class="btn btn-block btn-success"><i class="fas fa-search"></i></button></th>
                    </tr>
                    <tr>
                        <th scope="col" style="width:10%;">#</th>
                        <th scope="col">Razon Social</th>
                        <th scope="col">Nombre Comercial</th>
                        <th scope="col">Grupo</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Estado</th>
                        <th scope="col" style="width:5%;">Acciones</th>
                    </tr>
                </thead>
                <tbody class="small-font">
                    @foreach ($expedients as $expedient)
                        <tr>
                            <td>{{ $expedient->no_expediente }}</td>
                            <td>{{ $expedient->razon_social }}</td>
                            <td>{{ $expedient->nombre_comercial }}</td>
                            <td>{{ $expedient->grupo }}</td>
                            <td>{{ $expedient->telefono }}</td>
                            <td>{{ $expedient->estado }}</td>
                            <td>
                                <a href="{{ route('pc.expedients.edit', $expedient->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="table-footer">
                <div class="pagination">
                    <span>Mostrando {{ (($expedients->currentPage() * 15) - 14) }} a {{( $expedients->currentPage() * 15) }} de {{ $expedients->total() }} Filas</span>
                    {{ $expedients->appends(Request::only('no_expediente'))->links() }} 
                </div>
                {{-- <div class="button-exports">
                    <a href="{{ route('print.requests') }}" target="_BLANK" class="btn btn-success edit-taxpayer"><i class="fas fa-pdf"></i> Exportar a PDF</a>
                </div> --}}
            </div>
        </div>
    </div>
@endsection

@section('modals')
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        $(function(){
            $("#menu_proteccion_civil").addClass('active');

            var group = "{{ !empty(session('inputs')['grupo'])? session('inputs')['grupo'] : '' }}";
            console.log(group);
            $('[name="grupo"]').val(group);
        });
    </script>
@endsection