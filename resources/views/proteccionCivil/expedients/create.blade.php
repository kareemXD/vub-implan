@extends('layouts.index')

@section('title') Expedientes @endsection

@section('css')
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
    <link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
<div class="panel-top">
    <div class="d-flex justify-content-between">
        <h3>Nuevo Expediente</h3>
    </div>
</div>
<div class="panel-body with-buttons">
    <div class="margin-fix panel-row-fluid">
        @if(session()->has('alert'))
            <div class="alert alert-primary" role="alert">
                {{ session("alert") }}
            </div>
        @endif
        <form id="form-data" action="{{ route('pc.expedients.store') }}" method="POST" class="tab-content">
            @csrf
            <fieldset class="col-xl-12 col-sm-12 fieldset">
                <legend>Datos generales</legend>

                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col-4">
                        <label for="no_expediente">No. Expediente <span class="text-danger">*</span></label>
                        <input type="text" class="required form-control " id="no_expediente" name="no_expediente" value="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col-4">
                        <label for="grupo">Grupo <span class="text-danger">*</span></label>
                        <select name="grupo" class="form-control required {{ $errors->has('grupo') ? ' is-invalid' : '' }}" id="">
                            <option value="ORDINARIO">ORDINARIO</option>
                            <option value="DESARROLLO Y CONSTRUCCIÓN">DESARROLLO Y CONSTRUCCIÓN</option>
                            <option value="COMERCIO Y SERVICIOS">COMERCIO Y SERVICIOS</option>
                            <option value="EDUCACIONAL">EDUCACIONAL</option>
                            <option value="ALOJAMIENTO TEMPORAL">ALOJAMIENTO TEMPORAL</option>
                            <option value="LOCALIDAD">LOCALIDAD</option>
                            <option value="ATÍPICOS">ATÍPICOS</option>
                        </select>
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col-sm-4">
                        <label for="domicilio">Calle <span class="text-danger">*</span></label>
                        <input type="text" class="form-control {{ $errors->has('domicilio') ? ' is-invalid' : '' }}" id="domicilio" name="domicilio" value="{{ old('domicilio') }}">
                        @if ($errors->has('domicilio'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('domicilio') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group mt-1 mb-1 col">
                        <label for="numero">Número <span class="text-danger">*</span></label>
                        <input type="text" class="form-control {{ $errors->has('numero') ? ' is-invalid' : '' }}" id="numero" name="numero" value="{{ old('numero') }}">
                        @if ($errors->has('numero'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('numero') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group mt-1 mb-1 col">
                        <label for="colonia">Colonia <span class="text-danger">*</span></label>
                        <input type="text" class="required form-control {{ $errors->has('colonia') ? ' is-invalid' : '' }}" id="colonia" name="colonia" value="{{ old('colonia') }}" >
                        @if ($errors->has('colonia'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('colonia') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group mt-1 mb-1 col">
                        <label for="localidad">Localidad <span class="text-danger">*</span></label>
                        <input type="text" class="required form-control {{ $errors->has('localidad') ? ' is-invalid' : '' }}" id="localidad" name="localidad" value="{{ old('localidad') }}" >
                        @if ($errors->has('localidad'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('localidad') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <hr>
                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col-sm-4">
                        <label for="razon_social">Razón Social <span class="text-danger">*</span></label>
                        <input type="text" class="required form-control {{ $errors->has('razon_social') ? ' is-invalid' : '' }}" id="razon_social" name="razon_social" value="{{ old('razon_social') }}" >
                        @if ($errors->has('razon_social'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('razon_social') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group mt-1 mb-1 col">
                        <label for="rfc">RFC <span class="text-danger">*</span></label>
                        <input type="text" class="required form-control {{ $errors->has('rfc') ? ' is-invalid' : '' }}" id="rfc" name="rfc" value="{{ old('rfc') }}" >
                        @if ($errors->has('rfc'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('rfc') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group mt-1 mb-1 col">
                        <label for="nombre_comercial">Nombre Comercial <span class="text-danger">*</span></label>
                        <input type="text" class="required form-control {{ $errors->has('nombre_comercial') ? ' is-invalid' : '' }}" id="nombre_comercial" name="nombre_comercial" value="{{ old('nombre_comercial') }}" min="0">
                        @if ($errors->has('nombre_comercial'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('nombre_comercial') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col">
                        <label for="telefono">Teléfono <span class="text-danger">*</span></label>
                        <input type="text" class="required form-control {{ $errors->has('telefono') ? ' is-invalid' : '' }}" id="telefono" name="telefono" value="{{ old('telefono') }}" >
                        @if ($errors->has('telefono'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('telefono') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group mt-1 mb-1 col">
                        <label for="correo">Correo <span class="text-danger">*</span></label>
                        <input type="text" class="required form-control {{ $errors->has('correo') ? ' is-invalid' : '' }}" id="correo" name="correo" value="{{ old('correo') }}">
                        @if ($errors->has('correo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('correo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col">
                        <label for="estado">estado</label>
                        <input type="text" class="form-control {{ $errors->has('estado') ? ' is-invalid' : '' }}" id="estado" name="estado" value="{{ old('estado') }}">
                        @if ($errors->has('estado'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('estado') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group mt-1 mb-1 col">
                        <label for="observaciones">Observaciones</label>
                        <textarea class="form-control {{ $errors->has('observaciones') ? ' is-invalid' : '' }}" id="observaciones" name="observaciones" rows="5">{{ old('observaciones') }}</textarea>
                        @if ($errors->has('observaciones'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('observaciones') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <br>
            </fieldset>
        </form>
    </div>
</div>
<div class="row justify-content-end panel-buttoms">
    <button type="submit" class="btn btn-primary mr-3" id="btn-save-changes"><i class="fas fa-save mr-2"></i> Guardar Cambios</button>
</div>
@endsection

@section('modals')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script>
        $(function(){
            $("#menu_proteccion_civil").addClass('active');

            $(document).on("click", "#btn-save-changes", function(){
                let flag = true,
                    formalities = 0;

                $("#data").find(".required").each(function() {
                    if(!$(this).val() || $(this).val().length == 0){
                        flag = false;
                        $(this).addClass("is-invalid");
                        $(this).parent().find(".select2").addClass("is-invalid");
                    }else{
                        if($(this).hasClass("is-invalid")){
                            $(this).removeClass("is-invalid");
                            $(this).parent().find(".select2").removeClass("is-invalid");
                        }
                    }
                });

                if(flag){
                    $.confirm({
                        title: 'Guardar cambios!',
                        content: '<b>¿Estas seguro(a) de realizar esta acción?</b> <br/> <small>Esta acción solo actualizará la información del expediente.</small>',
                        type: "orange",
                        buttons: {
                            Aceptar: function () {
                                $('#form-data').submit();
                            },
                            Cancelar: function () {
                            },
                        }
                    });
                }else{
                    $.alert({
                        title: 'Alerta!',
                        content: 'Por favor Verifique que la información sea correcta.!',
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection