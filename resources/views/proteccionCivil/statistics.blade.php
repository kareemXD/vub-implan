@extends('layouts.index')

@section('title', 'Protección Civil - Estadísticas')

@section('css')
<style>
    #statistics-iframe iframe {
        height: 75.3vh;
        width: 100%;
        border: 0;
    }
    .loading {
        text-align: center;
        color: #B38E5D;
    }
</style>
@endsection

@section('content')
<div class="panel-top">
    <div class="text-center">
        <h3>Estadísticas</h3>
        <div class="loading">
            <i class="fas fa-spinner fa-pulse fa-3x"></i> <br>
            Cargando estadísticas...
        </div>
    </div>
</div>
<div class="panel-body">
    <div class="margin-fix panel-row-fluid">
        <div id="statistics-iframe"></div>
    </div>
</div>
@endsection

@section('js')
<script src="https://kit.fontawesome.com/e53f85d7ed.js" crossorigin="anonymous"></script>
<script>
    $(function() {
        var iframe = document.createElement("iframe");
        iframe.src = "http://187.188.190.48:3838/pc/";
        
        if (iframe.attachEvent){
            iframe.attachEvent("onload", function(){
                $('.loading').slideUp('slow');
            });
        } else {
            iframe.onload = function(){
                $('.loading').slideUp('slow');
            };
        }
        
        document.getElementById('statistics-iframe').appendChild(iframe);
    });
</script>
@endsection