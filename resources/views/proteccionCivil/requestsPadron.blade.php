@extends('layouts.index')

@section('title') Solicitudes @endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel-top">
        <div class="text-center">
            <h3>Solicitudes Padron & Licencias</h3>
        </div>
    </div>
    
    @can('write_requests')
        <div class="row justify-content-end panel-buttoms">
            <a href="{{ route('add.license') }}" class="btn btn-primary mr-3"><i class="fas fa-plus-circle mr-2"></i> Nueva Solicitud</a>
        </div>
    @endcan
    <div class="panel-body @can('write_requests') with-buttons @endcan">
        <div class="margin-fix panel-row-fluid">
            @if(session()->has('alert'))
                <div class="alert alert-primary" role="alert">
                    {{ session("alert") }}
                </div>
            @endif
            <div class="table-responsive">
                <table class="table table-sm table-hover laravel-pagination">
                    <thead>
                        <tr>
                            <form id="form-data" action="{{ route('requests') }}" method="POST" autocomplete="off">
                                @csrf
                                <th scope="col"><input type="text" name="cuenta" value="{{ (isset(session('inputs')['cuenta'])) ? session('inputs')['cuenta'] : "" }}" placeholder="Solicitud" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="id_contribuyente" value="{{ (isset(session('inputs')['id_contribuyente'])) ? session('inputs')['id_contribuyente'] : "" }}" placeholder="No. Contribuyente" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="contribuyente" value="{{ (isset(session('inputs')['contribuyente'])) ? session('inputs')['contribuyente'] : "" }}" placeholder="Nombre Contribuyente" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="nombre" value="{{ (isset(session('inputs')['nombre'])) ? session('inputs')['nombre'] : "" }}" placeholder="Nombre Negocio" class="form-control custom_input" /></th>
                                <th scope="col"><input type="text" name="fecha_alta" value="{{ (isset(session('inputs')['fecha_alta'])) ? session('inputs')['fecha_alta'] : "" }}" placeholder="Fecha de Alta" class="form-control custom_input" /></th>
                                <th scope="col"><select name="estatus" id="estatus" class="form-control custom_input">
                                                    @if (isset(session('inputs')['estatus']) && session('inputs')['estatus'] == "1")
                                                        <option value="t">Todos</option>
                                                        <option value="0">No Aplicado</option>
                                                        <option selected value="1">Aplicado</option>
                                                    @elseif(isset(session('inputs')['estatus']) && session('inputs')['estatus'] == "0")
                                                        <option value="t">Todos</option>
                                                        <option selected value="0">No Aplicado</option>
                                                        <option value="1">Aplicado</option>
                                                    @elseif(isset(session('inputs')['estatus']) && session('inputs')['estatus'] == "t")
                                                        <option selected value="t">Todos</option>
                                                        <option value="0">No Aplicado</option>
                                                        <option value="1">Aplicado</option>
                                                    @else 
                                                        <option selected value="0">No Aplicado</option>
                                                        <option value="1">Aplicado</option>
                                                    @endif
                                                </select></th>
                                <th class="btn-search-table"><button type="button" class="btn btn-success btn-block btn-submit"><i class="fas fa-search"></i></button></th>
                            </form>
                        </tr>
                        <tr>
                            <th scope="col" style="width:5%;">#</th>
                            <th scope="col">No. Contribuyente</th>
                            <th scope="col">Nombre Contribuyente</th>
                            <th scope="col">Nombre Negocio</th>
                            <th scope="col">Fecha Solicitud</th>
                            <th scope="col">Estatus</th>
                            <th scope="col" style="width:5%;">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="small-font">
                        @foreach ($requests as $request)
                            <tr>
                                <td>{{ $request->NumeroSolicitud }}</td>
                                <td>{{ $request->IdContribuyente }}</td>
                                <td>{{ $request->taxpayer->NombreCompleto }}</td>
                                <td>{{ $request->NombreNegocio }}</td>
                                <td>{{ $request->FechaSolicitud }}</td>
                                <td class="{{ ($request->Aplicado == 1)? 'bg-green' : 'bg-yellow' }}">{{ ($request->Aplicado == 1)? 'Aplicado' : 'No Aplicado' }}</td>
                                <td align="center"><a href="{{ route('update.request', $request->NumeroSolicitud) }}" class="btn btn-primary btn-action-table"><i class="fas fa-edit"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="table-footer">
                    <div class="pagination">
                        <span>Mostrando {{ (($requests->currentPage() * 15) - 14) }} a {{( $requests->currentPage() * 15) }} de {{ $requests->total() }} Filas</span>
                        {{ $requests->appends(Request::only('NumeroSolicitud'))->links() }} 
                    </div>
                    <div class="button-exports">
                        <a href="{{ route('print.requests') }}" target="_BLANK" class="btn btn-success edit-taxpayer"><i class="fas fa-pdf"></i> Exportar a PDF</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@section('modals')
    
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
    <script>
        $(function(){
            $("#padron").addClass('active');
            // $("[name='persona']").bootstrapSwitch();
            // $("[name='update_persona']").bootstrapSwitch();
            $('input[name="fecha_alta"]').daterangepicker({
                autoUpdateInput: false,
                opens: 'left',
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Aceptar",
                    "cancelLabel": "Limpiar",
                    "fromLabel": "De",
                    "toLabel": "A",
                    "customRangeLabel": "Personalizado",
                    "daysOfWeek": [
                        "Do",
                        "Lu",
                        "Ma",
                        "Mi",
                        "Ju",
                        "Vi",
                        "Sa"
                    ],
                    "monthNames": [
                        "Enero",
                        "Febrero",
                        "Marzo",
                        "Abril",
                        "Mayo",
                        "Junio",
                        "Julio",
                        "Agusto",
                        "Septiembre",
                        "Octubre",
                        "Noviembre",
                        "Diciembre"
                    ],
                    "firstDay": 1
                }
            });
            $('input[name="fecha_alta"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('input[name="fecha_alta"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            $(document).on("click", ".btn-submit", function(){
                $( ".custom_input" ).each(function( index, element ) {
                    $("#form-data").append($(this));
                });
                $("#form-data")[0].submit();
            });
            
            $(document).on("keypress", ".custom_input", function(e){
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    $( ".custom_input" ).each(function( index, element ) {
                        $("#form-data").append($(this));
                    });
                    $("#form-data")[0].submit();
                }
            });
        });
    </script>
@endsection