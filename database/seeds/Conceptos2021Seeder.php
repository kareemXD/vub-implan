<?php

use Illuminate\Database\Seeder;

class Conceptos2021Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   if(!is_null(DB::table('du_conceptos')->where("YearConcept", "2021")->first())){
            dd("Ya han sido insertados");
            return 0;
        }
        DB::table('du_conceptos')->insert([
            ['Articulo' => '42, I, A, 1', 'Concepto' => '0001', 'Uso' => 'Aprovechamiento de Recursos Naturales', 'Descripcion' => 'Aprovechamiento de Recursos Naturales' , 'Importe' => 1891.56, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 2.1', 'Concepto' => '0002', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'Hasta 500 metros cuadrados de terreno' , 'Importe' => 1513.25, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 2.2', 'Concepto' => '0003', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'De 501 a 1000 metros cuadrados de terreno' , 'Importe' => 2269.88, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 2.3', 'Concepto' => '0004', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'De 1001 a 5000 metros cuadrados de terreno' , 'Importe' => 3783.14, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 2.4', 'Concepto' => '0005', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'De 5,001 a 10,000 metros cuadrados de terreno' , 'Importe' => 5296.4, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 2.5', 'Concepto' => '0006', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'De 10,001 a 50,000 metros cuadrados de terreno, por cada 1,000 m2
            de terreno o parte proporcional.' , 'Importe' => 341.48, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 3.1', 'Concepto' => '0007', 'Uso' => 'Habitacional', 'Descripcion' => 'Hasta 500 metros cuadrados de terreno' , 'Importe' => 756.62, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 3.2', 'Concepto' => '0008', 'Uso' => 'Habitacional', 'Descripcion' => 'De 501 a 1000 metros cuadrados de terreno' , 'Importe' => 1134.94, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 3.3', 'Concepto' => '0009', 'Uso' => 'Habitacional', 'Descripcion' => 'De 1001 a 5000 metros cuadrados de terreno' , 'Importe' => 2269.88, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 3.4', 'Concepto' => '0010', 'Uso' => 'Habitacional', 'Descripcion' => 'De 5001 a 10,000 metros cuadrados de terreno' , 'Importe' => 3783.14, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 3.5', 'Concepto' => '0011', 'Uso' => 'Habitacional', 'Descripcion' => 'De 10,001 a 50,000 metros cuadrados de terreno, por cada 1,000 m2
            de terreno o parte proporcional.' , 'Importe' => 227.83, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 4', 'Concepto' => '0012', 'Uso' => 'Comercial', 'Descripcion' => 'Comercial' , 'Importe' => 3404.83, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 5', 'Concepto' => '0013', 'Uso' => 'Servicios', 'Descripcion' => 'Servicios' , 'Importe' => 2269.88, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 6', 'Concepto' => '0014', 'Uso' => 'Industrial', 'Descripcion' => 'Industrial' , 'Importe' => 2269.88, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 7', 'Concepto' => '0015', 'Uso' => 'Equipamiento', 'Descripcion' => 'Equipamiento' , 'Importe' => 756.63, 'YearConcept' => 2021],
            ['Articulo' => '42, I, A, 8', 'Concepto' => '0016', 'Uso' => 'Infraestructura ', 'Descripcion' => 'Infraestructura ' , 'Importe' => 756.63, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 1', 'Concepto' => '0017', 'Uso' => 'Aprovechamiento de Recursos Naturales', 'Descripcion' => 'Aprovechamiento de Recursos Naturales' , 'Importe' => 3783.14, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 2.1', 'Concepto' => '0018', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'Hasta 500 metros cuadrados de terreno' , 'Importe' => 7566.27, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 2.2', 'Concepto' => '0019', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'De 501 a 1000 metros cuadrados de terreno.' , 'Importe' => 11349.42, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 2.3', 'Concepto' => '0020', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'De 1001 a5000 metros cuadrados de terreno' , 'Importe' => 13240.5, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 2.4', 'Concepto' => '0021', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'De 5,001 a 10,000 metros cuadrados de terreno' , 'Importe' => 18915.7, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 2.5', 'Concepto' => '0022', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico Hotelero, incluyendo campos de golf
            Hotelero, incluyendo campos de golf', 'Descripcion' => 'De 10,001 metros cuadrados de terreno en adelante, por cada
            1,000 m2 de terreno o parte proporcional.' , 'Importe' => 1418.67, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 3.1', 'Concepto' => '0023', 'Uso' => 'Habitacional', 'Descripcion' => 'Hasta 500 metros cuadrados de terreno' , 'Importe' => 4539.76, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 3.2', 'Concepto' => '0024', 'Uso' => 'Habitacional', 'Descripcion' => 'De 501 a 1000 metros cuadrados de terreno' , 'Importe' => 6053.03, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 3.3', 'Concepto' => '0025', 'Uso' => 'Habitacional', 'Descripcion' => 'De 1001 a5000 metros cuadrados de terreno' , 'Importe' => 7566.28, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 3.4', 'Concepto' => '0026', 'Uso' => 'Habitacional', 'Descripcion' => 'De 5001 a 10,000 metros cuadrados de terreno' , 'Importe' => 15132.57, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 3.5', 'Concepto' => '0027', 'Uso' => 'Habitacional', 'Descripcion' => 'De10,001 metros cuadrados de terreno en adelante, por cada 1,000
            m2 de terreno o parte proporcional.' , 'Importe' => 1134.95, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 4', 'Concepto' => '0028', 'Uso' => 'Comercial por cada 2,000 metro cuadrados', 'Descripcion' => '4) Comercial por cada 2,000 metro cuadrados' , 'Importe' => 7566.28, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 5', 'Concepto' => '0029', 'Uso' => 'Servicios', 'Descripcion' => 'Servicios' , 'Importe' => 7566.28, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 6', 'Concepto' => '0030', 'Uso' => 'Industrial', 'Descripcion' => 'Industrial' , 'Importe' => 4539.76, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 7', 'Concepto' => '0031', 'Uso' => 'Equipamiento', 'Descripcion' => 'Equipamiento' , 'Importe' => 1513.25, 'YearConcept' => 2021],
            ['Articulo' => '42, B, 8', 'Concepto' => '0032', 'Uso' => 'Infraestructura', 'Descripcion' => 'Infraestructura' , 'Importe' => 1513.25, 'YearConcept' => 2021],
            ['Articulo' => '42, C, 1', 'Concepto' => '0033', 'Uso' => 'Hasta 5000 metros cuadrados', 'Descripcion' => 'Hasta 5000 metros cuadrados' , 'Importe' => 7566.28, 'YearConcept' => 2021],
            ['Articulo' => '42, C, 2', 'Concepto' => '0034', 'Uso' => 'De 5,001 a 10,000 metros cuadrados de terreno', 'Descripcion' => 'De 5,001 a 10,000 metros cuadrados de terreno' , 'Importe' => 11349.43, 'YearConcept' => 2021],
            ['Articulo' => '42, C, 3', 'Concepto' => '0035', 'Uso' => 'De 10,001 metros cuadrados de terreno en adelante, por cada 1,000 m2 de
            terreno o parte proporcional', 'Descripcion' => 'De 10,001 metros cuadrados de terreno en adelante, por cada 1,000 m2 de
            terreno o parte proporcional' , 'Importe' => 851.19, 'YearConcept' => 2021],
            ['Articulo' => '42, D, 1', 'Concepto' => '0036', 'Uso' => 'Habitacional residencial, turístico residencial, turístico condominal,
            turístico hotelero, por metro cuadrado de construcción.', 'Descripcion' => 'Habitacional residencial, turístico residencial, turístico condominal,
            turístico hotelero, por metro cuadrado de construcción.' , 'Importe' => 237.79, 'YearConcept' => 2021],
            ['Articulo' => '42, D, 2.1', 'Concepto' => '0037', 'Uso' => 'Habitacional', 'Descripcion' => 'Para el caso de unidades de vivienda de hasta 250 metros cuadrados
            de construcción se pagará por la emisión de la constancia de antigüedad.' , 'Importe' => 4502.4, 'YearConcept' => 2021],
            ['Articulo' => '42, D, 2.2', 'Concepto' => '0038', 'Uso' => ' Habitacional', 'Descripcion' => 'De 251 a 500 metros cuadrados se pagará por metro cuadrado de
            construcción' , 'Importe' => 20.86, 'YearConcept' => 2021],
            ['Articulo' => '42, D, 2.3', 'Concepto' => '0039', 'Uso' => 'Habitacional', 'Descripcion' => 'De más de 500 metros cuadrados de construcción se pagará por
            metro cuadrado de construcción.' , 'Importe' => 30.26, 'YearConcept' => 2021],
            ['Articulo' => '42, D, 3', 'Concepto' => '0040', 'Uso' => 'Comercial y de servicios por metro cuadrado de construcción', 'Descripcion' => 'Comercial y de servicios por metro cuadrado de construcción' , 'Importe' => 75.66, 'YearConcept' => 2021],
            ['Articulo' => '42, D, 4', 'Concepto' => '0041', 'Uso' => 'Industrial y Agroindustrial por metro cuadrado de construcción', 'Descripcion' => 'Industrial y Agroindustrial por metro cuadrado de construcción' , 'Importe' => 45.4, 'YearConcept' => 2021],
            ['Articulo' => '42, E, 1', 'Concepto' => '0042', 'Uso' => 'Habitacional', 'Descripcion' => 'Habitacional' , 'Importe' => 1.5, 'YearConcept' => 2021],
            ['Articulo' => '42, E, 2', 'Concepto' => '0043', 'Uso' => 'Turismo Condominal, Turismo, Hotelero y
            Residencial', 'Descripcion' => 'Turismo Condominal, Turismo, Hotelero y
            Residencial' , 'Importe' => 2.95, 'YearConcept' => 2021],
            ['Articulo' => '42, F, 1', 'Concepto' => '0044', 'Uso' => 'Por cada 10,000 M2 o parte proporcional', 'Descripcion' => 'Por cada 10,000 M2 o parte proporcional' , 'Importe' => 7566.28, 'YearConcept' => 2021],
            ['Articulo' => '42, G, 1', 'Concepto' => '0045', 'Uso' => 'Aprovechamiento de Recursos Naturales', 'Descripcion' => '1) Aprovechamiento de Recursos Naturales' , 'Importe' => 3.84, 'YearConcept' => 2021],
            ['Articulo' => '42, G, 2', 'Concepto' => '0046', 'Uso' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico
            Hotelero', 'Descripcion' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico
            Hotelero' , 'Importe' => 22.69, 'YearConcept' => 2021],
            ['Articulo' => '42, G, 3', 'Concepto' => '0047', 'Uso' => 'Habitacional Medio', 'Descripcion' => 'Habitacional Medio' , 'Importe' => 15.13, 'YearConcept' => 2021],
            ['Articulo' => '42, G, 4', 'Concepto' => '0048', 'Uso' => ' Habitacional Popular', 'Descripcion' => ' Habitacional Popular' , 'Importe' => 11.39, 'YearConcept' => 2021],
            ['Articulo' => '42, G, 5', 'Concepto' => '0049', 'Uso' => 'Comercial', 'Descripcion' => 'Comercial' , 'Importe' => 15.13, 'YearConcept' => 2021],
            ['Articulo' => '42, G, 6', 'Concepto' => '0050', 'Uso' => 'Servicios', 'Descripcion' => 'Servicios' , 'Importe' => 7.57, 'YearConcept' => 2021],
            ['Articulo' => '42, G, 7', 'Concepto' => '0051', 'Uso' => ' Industrial', 'Descripcion' => ' Industrial' , 'Importe' => 18.89, 'YearConcept' => 2021],
            ['Articulo' => '42, G, 8', 'Concepto' => '0052', 'Uso' => 'Equipamiento', 'Descripcion' => 'Equipamiento' , 'Importe' => 7.57, 'YearConcept' => 2021],
            ['Articulo' => '42, G, 9', 'Concepto' => '0053', 'Uso' => 'Infraestructura', 'Descripcion' => 'Infraestructura' , 'Importe' => 7.57, 'YearConcept' => 2021],
            ['Articulo' => '42, H, 1.1', 'Concepto' => '0054', 'Uso' => 'Por cada 1.00 Metro cúbico', 'Descripcion' => 'Habitacional' , 'Importe' => 6.05, 'YearConcept' => 2021],
            ['Articulo' => '42, H, 1.2', 'Concepto' => '0055', 'Uso' => 'Por cada 1.00 Metro cúbico', 'Descripcion' => ' Residencial' , 'Importe' => 12.11, 'YearConcept' => 2021],
            ['Articulo' => '42, i, 1', 'Concepto' => '0056', 'Uso' => 'Menor a 10 cm de diámetro fuste, por cada árbol.', 'Descripcion' => 'Menor a 10 cm de diámetro fuste, por cada árbol.' , 'Importe' => 37.83, 'YearConcept' => 2021],
            ['Articulo' => '42, i, 2', 'Concepto' => '0057', 'Uso' => 'De 11 cm a 30 cm de diámetro de fuste, por cada árbol.', 'Descripcion' => 'De 11 cm a 30 cm de diámetro de fuste, por cada árbol.' , 'Importe' => 113.49, 'YearConcept' => 2021],
            ['Articulo' => '42, i, 3', 'Concepto' => '0058', 'Uso' => 'De 31 cm a 75 cm de diámetro de fuste, por cada árbol.', 'Descripcion' => 'De 31 cm a 75 cm de diámetro de fuste, por cada árbol.' , 'Importe' => 264.82, 'YearConcept' => 2021],
            ['Articulo' => '42, i, 4', 'Concepto' => '0059', 'Uso' => 'De 76 cm de diámetro de fuste en adelante, por cada árbol', 'Descripcion' => 'De 76 cm de diámetro de fuste en adelante, por cada árbol' , 'Importe' => 1134.95, 'YearConcept' => 2021],
            ['Articulo' => '42, J, 1', 'Concepto' => '0060', 'Uso' => 'Por cada 1.00 Metro cuadrado', 'Descripcion' => 'Por cada 1.00 Metro cuadrado' , 'Importe' => 6.05, 'YearConcept' => 2021],
            ['Articulo' => '42, K, 1', 'Concepto' => '0061', 'Uso' => 'Por cada 1.00 Metro cuadrado', 'Descripcion' => 'Por cada 1.00 Metro cuadrado' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '42, I, 1', 'Concepto' => '0062', 'Uso' => 'Por cada 1.00 Metro cuadrado', 'Descripcion' => 'Por cada 1.00 Metro cuadrado' , 'Importe' => 7.57, 'YearConcept' => 2021],
            ['Articulo' => '42, II, 1', 'Concepto' => '0063', 'Uso' => 'Dictamen de autorización definitiva de fraccionamiento', 'Descripcion' => 'Dictamen de autorización definitiva de fraccionamiento' , 'Importe' => 22698.84, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 1', 'Concepto' => '0064', 'Uso' => 'Aprovechamiento de Recursos Naturales, por cada metro cuadrado de
            superficie del proyecto.', 'Descripcion' => 'Aprovechamiento de Recursos Naturales, por cada metro cuadrado de
            superficie del proyecto.' , 'Importe' => 37.83, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 2.1', 'Concepto' => '0065', 'Uso' => 'Residencial.', 'Descripcion' => 'Residencial, Turístico Residencial, Turístico Condominal, Turístico
            Hotelero incluyendo campos de golf, por cada metro cuadrado de
            superficie del proyecto.' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 3.1', 'Concepto' => '0066', 'Uso' => 'Habitacional, por unidad de vivienda', 'Descripcion' => 'Viviendas de hasta 50.00 metros cuadrados de construcción.' , 'Importe' => 907.96, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 3.2', 'Concepto' => '0067', 'Uso' => 'Habitacional, por unidad de vivienda', 'Descripcion' => 'Viviendas de 51.00 hasta 100.00 metros cuadrados de
            construcción.' , 'Importe' => 1134.95, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 3.3', 'Concepto' => '0068', 'Uso' => 'Habitacional, por unidad de vivienda', 'Descripcion' => 'Viviendas de 101.00 hasta 200.00 metros cuadrados de
            construcción.' , 'Importe' => 1365.21, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 3.4', 'Concepto' => '0069', 'Uso' => 'Habitacional, por unidad de vivienda', 'Descripcion' => 'Vivienda de 201 metros cuadrados de construcción en adelante' , 'Importe' => 1513.3, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 4', 'Concepto' => '0070', 'Uso' => 'Comercial y Servicios, obra nueva por cada M2
            de superficie del proyecto', 'Descripcion' => ' Comercial y Servicios, obra nueva por cada M2
            de superficie del proyecto' , 'Importe' => 60.55, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 5', 'Concepto' => '0071', 'Uso' => 'Comercial y Servicios, remodelación y/o adecuación, por cada M2 de
            superficie del proyecto', 'Descripcion' => 'Comercial y Servicios, remodelación y/o adecuación, por cada M2 de
            superficie del proyecto' , 'Importe' => 30.26, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 6', 'Concepto' => '0072', 'Uso' => 'Industrial, por cada M2 de superficie del proyecto', 'Descripcion' => 'Industrial, por cada M2 de superficie del proyecto' , 'Importe' => 15.13, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 7', 'Concepto' => '0073', 'Uso' => 'Equipamiento, por cada M2 de superficie del proyecto', 'Descripcion' => 'Equipamiento, por cada M2 de superficie del proyecto' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 8', 'Concepto' => '0074', 'Uso' => 'Infraestructura, por cada M2 de superficie del proyecto incluyendo
            vialidades', 'Descripcion' => 'Infraestructura, por cada M2 de superficie del proyecto incluyendo
            vialidades' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '42, III, A, 9', 'Concepto' => '0075', 'Uso' => 'Espacios verdes y abiertos, por cada M2 de superficie del proyecto.', 'Descripcion' => 'Espacios verdes y abiertos, por cada M2 de superficie del proyecto.' , 'Importe' => 37.88, 'YearConcept' => 2021],
            ['Articulo' => '42, III, B, 1', 'Concepto' => '0076', 'Uso' => 'Por cada 1.00 Metro cuadrado', 'Descripcion' => 'Por cada 1.00 Metro cuadrado' , 'Importe' => 7.56, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 1', 'Concepto' => '0077', 'Uso' => 'Habitacional Residencial', 'Descripcion' => 'Habitacional Residencial, Turístico Residencial, Turístico Condominal,
            Turístico Hotelero, por cada M2' , 'Importe' => 166.45, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 2, A', 'Concepto' => '0078', 'Uso' => 'Habitacional', 'Descripcion' => 'Autoconstrucción, para las obras que se ubiquen en colonias populares y
            hasta 70.00 M2, previa verificación del Ayuntamiento; solo obra nueva en
            planta baja.' , 'Importe' => 0, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 2, B', 'Concepto' => '0079', 'Uso' => 'Habitacional', 'Descripcion' => 'Habitacional, viviendas de hasta 50.00 M2 de construcción' , 'Importe' => 11.39, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 2, C', 'Concepto' => '0080', 'Uso' => 'Habitacional', 'Descripcion' => 'Habitacional, viviendas de 51.00 hasta 100.00 M2 de construcción' , 'Importe' => 15.13, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 2, D', 'Concepto' => '0081', 'Uso' => 'Habitacional', 'Descripcion' => 'Habitacional, viviendas de 101.00 hasta 200.00 M2 de construcción' , 'Importe' => 22.69, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 2, E', 'Concepto' => '0082', 'Uso' => 'Habitacional', 'Descripcion' => 'Habitacional, viviendas de 201.00 M2 de construcción en adelante' , 'Importe' => 52.98, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 3, A', 'Concepto' => '0083', 'Uso' => 'Habitacional para fraccionamientos nuevos o etapas nuevas de
            fraccionamientos previamente autorizados.', 'Descripcion' => 'Habitacional, viviendas de hasta 50.00 metros cuadrados de construcción' , 'Importe' => 30.26, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 3, B', 'Concepto' => '0084', 'Uso' => 'Habitacional para fraccionamientos nuevos o etapas nuevas de
            fraccionamientos previamente autorizados.', 'Descripcion' => 'Habitacional, viviendas de 51.00 y hasta 100.00 metros cuadrados de
            construcción:' , 'Importe' => 37.88, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 3, C', 'Concepto' => '0085', 'Uso' => 'Habitacional para fraccionamientos nuevos o etapas nuevas de
            fraccionamientos previamente autorizados.', 'Descripcion' => 'Habitacional, viviendas de 101.00 y hasta 200.00 metros cuadrados de
            construcción' , 'Importe' => 45.44, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 3, D', 'Concepto' => '0086', 'Uso' => 'Habitacional para fraccionamientos nuevos o etapas nuevas de
            fraccionamientos previamente autorizados.', 'Descripcion' => 'Habitacional, viviendas de 201.00 metros cuadrados de construcción en
            adelante' , 'Importe' => 113.53, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 4', 'Concepto' => '0087', 'Uso' => 'Comercial y Servicios', 'Descripcion' => 'Comercial y Servicios' , 'Importe' => 113.53, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 5', 'Concepto' => '0088', 'Uso' => 'Comercial y Servicios con cubierta ligera de estructura metálica o similar', 'Descripcion' => 'Comercial y Servicios con cubierta ligera de estructura metálica o similar' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 6', 'Concepto' => '0089', 'Uso' => 'Industrial y Agroindustrial', 'Descripcion' => ' Industrial y Agroindustrial' , 'Importe' => 56.74, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 7', 'Concepto' => '0090', 'Uso' => 'Industrial y Agroindustrial con cubierta ligera de estructura metálica o similar', 'Descripcion' => '7) Industrial y Agroindustrial con cubierta ligera de estructura metálica o similar' , 'Importe' => 37.88, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 8', 'Concepto' => '0091', 'Uso' => 'Equipamiento', 'Descripcion' => 'Equipamiento' , 'Importe' => 34.78, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 9', 'Concepto' => '0092', 'Uso' => 'Infraestructura', 'Descripcion' => 'Infraestructura' , 'Importe' => 34.78, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 10', 'Concepto' => '0093', 'Uso' => 'Construcción de Campos de Golf, por M2', 'Descripcion' => 'Construcción de Campos de Golf, por M2' , 'Importe' => 18.94, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 11', 'Concepto' => '0094', 'Uso' => 'Construcción de Albercas, fuentes, espejos y/o cortinas de agua por M3', 'Descripcion' => 'Construcción de Albercas, fuentes, espejos y/o cortinas de agua por M3' , 'Importe' => 189.19, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 12', 'Concepto' => '0095', 'Uso' => 'Construcción de palapas residenciales, hoteleras y/o turísticas por M2', 'Descripcion' => '12) Construcción de palapas residenciales, hoteleras y/o turísticas por M2' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '42, III, C, 13', 'Concepto' => '0096', 'Uso' => 'Construcción de palapas rústicas, por M2', 'Descripcion' => 'Construcción de palapas rústicas, por M2' , 'Importe' => 37.88, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 1, A', 'Concepto' => '0097', 'Uso' => 'Bardeo por metro lineal', 'Descripcion' => 'En Predio Rústico' , 'Importe' => 11.39, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 1, B', 'Concepto' => '0098', 'Uso' => 'Bardeo por metro lineal', 'Descripcion' => ' En Predio Urbano' , 'Importe' => 18.94, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 2, A', 'Concepto' => '0099', 'Uso' => 'Remodelación de fachada', 'Descripcion' => 'Para uso habitacional, por metro lineal' , 'Importe' => 34.79, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 2, B', 'Concepto' => '0100', 'Uso' => 'Remodelación de fachada', 'Descripcion' => 'Para uso comercial, por metro lineal' , 'Importe' => 60.54, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 3, 3.1', 'Concepto' => '0101', 'Uso' => 'Remodelación de Construcción por M2', 'Descripcion' => 'Remodelación en uso habitacional por M2' , 'Importe' => 52.98, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 4', 'Concepto' => '0102', 'Uso' => 'Remodelación en general para uso comercial o de servicios, por M2', 'Descripcion' => 'Remodelación en general para uso comercial o de servicios, por M2' , 'Importe' => 68.1, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 5', 'Concepto' => '0103', 'Uso' => 'Por techar sobre superficies abiertas y semi abiertas (patios, terrazas,
            pergolados, cocheras, tejabanes, remodelaciones de palapas, etc.)', 'Descripcion' => '5) Por techar sobre superficies abiertas y semi abiertas (patios, terrazas,
            pergolados, cocheras, tejabanes, remodelaciones de palapas, etc.)' , 'Importe' => 22.66, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 6', 'Concepto' => '0104', 'Uso' => 'Para la construcción de áreas deportivas privadas en general, por M2', 'Descripcion' => 'Para la construcción de áreas deportivas privadas en general, por M2' , 'Importe' => 22.66, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 7', 'Concepto' => '0105', 'Uso' => 'Para demoliciones en general, por M2', 'Descripcion' => 'Para demoliciones en general, por M2' , 'Importe' => 30.22, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 8', 'Concepto' => '0106', 'Uso' => 'Instalación de elevadores, escaleras eléctricas, montacargas y/o plumas por
            unidad', 'Descripcion' => ' Instalación de elevadores, escaleras eléctricas, montacargas y/o plumas por
            unidad' , 'Importe' => 3024.33, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 9', 'Concepto' => '0107', 'Uso' => 'Construcción de aljibes o cisternas por metro cúbico', 'Descripcion' => 'Construcción de aljibes o cisternas por metro cúbico' , 'Importe' => 37.88, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 10', 'Concepto' => '0108', 'Uso' => ' Por cambio de techo, terminación de obra, aplanados, pisos y similares por
            metro cuadrado.', 'Descripcion' => 'Por cambio de techo, terminación de obra, aplanados, pisos y similares por
            metro cuadrado.' , 'Importe' => 22.66, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 11', 'Concepto' => '0109', 'Uso' => 'Por la entrega – recepción de las obras de urbanización de
            fraccionamientos, por vivienda o lote', 'Descripcion' => 'Por la entrega – recepción de las obras de urbanización de
            fraccionamientos, por vivienda o lote' , 'Importe' => 151.31, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 12', 'Concepto' => '0110', 'Uso' => 'Por dictamen de Estudio de Impacto Vial (Tránsito), por trámite', 'Descripcion' => 'Por dictamen de Estudio de Impacto Vial (Tránsito), por trámite' , 'Importe' => 2269.84, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 13', 'Concepto' => '0111', 'Uso' => 'Permiso de Construcción de criptas y mausoleos, por unidad', 'Descripcion' => 'Permiso de Construcción de criptas y mausoleos, por unidad' , 'Importe' => 756.64, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 14', 'Concepto' => '0112', 'Uso' => 'Construcción de Sótanos (indistintamente de su uso), por metro cuadrado.', 'Descripcion' => 'Construcción de Sótanos (indistintamente de su uso), por metro cuadrado.' , 'Importe' => 151.31, 'YearConcept' => 2021],
            ['Articulo' => '42, III, D, 15', 'Concepto' => '0113', 'Uso' => 'Construcción de muros de contención, o construcción de cimentaciones
            por metro cúbico.', 'Descripcion' => 'Construcción de muros de contención, o construcción de cimentaciones
            por metro cúbico.' , 'Importe' => 37.88, 'YearConcept' => 2021],
            ['Articulo' => '42, III, E, 1', 'Concepto' => '0114', 'Uso' => 'Por cada 1.00 M2, o fracción', 'Descripcion' => 'Por cada 1.00 M2, o fracción' , 'Importe' => 7.56, 'YearConcept' => 2021],
            ['Articulo' => '42, III, E, 2', 'Concepto' => '0115', 'Uso' => 'Por cada 1.00 ML, o fracción', 'Descripcion' => 'Por cada 1.00 ML, o fracción' , 'Importe' => 3.84, 'YearConcept' => 2021],
            ['Articulo' => '42, VI, 1', 'Concepto' => '0116', 'Uso' => 'Por obras de hasta 75 metros cuadrados:', 'Descripcion' => 'Por obras de hasta 75 metros cuadrados:' , 'Importe' => 151.33, 'YearConcept' => 2021],
            ['Articulo' => '42, VI, 2', 'Concepto' => '0117', 'Uso' => 'Por obras mayores a 75 metros cuadrados', 'Descripcion' => 'Por obras mayores a 75 metros cuadrados' , 'Importe' => 453.98, 'YearConcept' => 2021],
            ['Articulo' => '45, 1, A', 'Concepto' => '0118', 'Uso' => 'Alineamiento, por metro lineal según el tipo de construcción y uso o
            destino de suelo', 'Descripcion' => 'Residencial, Turístico Residencial,
            Turístico Condominal, Turístico Hotelero' , 'Importe' => 302.65, 'YearConcept' => 2021],
            ['Articulo' => '45, 1, B', 'Concepto' => '0119', 'Uso' => 'Alineamiento, por metro lineal según el tipo de construcción y uso o
            destino de suelo', 'Descripcion' => 'Habitacional por autoconstrucción' , 'Importe' => 0, 'YearConcept' => 2021],
            ['Articulo' => '45, 1, C', 'Concepto' => '0120', 'Uso' => 'Alineamiento, por metro lineal según el tipo de construcción y uso o
            destino de suelo', 'Descripcion' => 'Habitacional en general' , 'Importe' => 22.69, 'YearConcept' => 2021],
            ['Articulo' => '45, 1, D', 'Concepto' => '0121', 'Uso' => 'Alineamiento, por metro lineal según el tipo de construcción y uso o
            destino de suelo', 'Descripcion' => 'Comercial' , 'Importe' => 113.53, 'YearConcept' => 2021],
            ['Articulo' => '45, 1, E', 'Concepto' => '0122', 'Uso' => 'Alineamiento, por metro lineal según el tipo de construcción y uso o
            destino de suelo', 'Descripcion' => 'Servicios' , 'Importe' => 113.53, 'YearConcept' => 2021],
            ['Articulo' => '45, 1, F', 'Concepto' => '0123', 'Uso' => 'Alineamiento, por metro lineal según el tipo de construcción y uso o
            destino de suelo', 'Descripcion' => 'Industrial y Agroindustrial' , 'Importe' => 113.53, 'YearConcept' => 2021],
            ['Articulo' => '45, 1, G', 'Concepto' => '0124', 'Uso' => 'Alineamiento, por metro lineal según el tipo de construcción y uso o
            destino de suelo', 'Descripcion' => ' Equipamiento' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '45, 1, H', 'Concepto' => '0125', 'Uso' => 'Alineamiento, por metro lineal según el tipo de construcción y uso o
            destino de suelo', 'Descripcion' => 'Infraestructura' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '45, 2, A', 'Concepto' => '0126', 'Uso' => 'Designación de números oficiales:', 'Descripcion' => 'Residencial, Turístico Residencial, Turístico
            Condominal, Turístico Hotelero' , 'Importe' => 1134.95, 'YearConcept' => 2021],
            ['Articulo' => '45, 2, B', 'Concepto' => '0127', 'Uso' => 'Designación de números oficiales:', 'Descripcion' => 'Habitacional por autoconstrucción' , 'Importe' => 0, 'YearConcept' => 2021],
            ['Articulo' => '45, 2, C', 'Concepto' => '0128', 'Uso' => 'Designación de números oficiales:', 'Descripcion' => 'Habitacional en general' , 'Importe' => 151.32, 'YearConcept' => 2021],
            ['Articulo' => '45, 2, D', 'Concepto' => '0129', 'Uso' => 'Designación de números oficiales:', 'Descripcion' => 'Comercial' , 'Importe' => 907.96, 'YearConcept' => 2021],
            ['Articulo' => '45, 2, E', 'Concepto' => '0130', 'Uso' => 'Designación de números oficiales:', 'Descripcion' => 'Servicios' , 'Importe' => 907.96, 'YearConcept' => 2021],
            ['Articulo' => '45, 2, F', 'Concepto' => '0131', 'Uso' => 'Designación de números oficiales:', 'Descripcion' => 'Industrial y Agroindustrial' , 'Importe' => 113.53, 'YearConcept' => 2021],
            ['Articulo' => '45, 2, G', 'Concepto' => '0132', 'Uso' => 'Designación de números oficiales:', 'Descripcion' => 'Equipamiento' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '45, 2, H', 'Concepto' => '0133', 'Uso' => 'Designación de números oficiales:', 'Descripcion' => ' InfraestructurA' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '45, 1', 'Concepto' => '0134', 'Uso' => 'Aprovechamiento de Recursos Naturales', 'Descripcion' => 'Aprovechamiento de Recursos Naturales' , 'Importe' => 2269.84, 'YearConcept' => 2021],
            ['Articulo' => '46, 2', 'Concepto' => '0135', 'Uso' => 'Turístico', 'Descripcion' => 'Turístico' , 'Importe' => 3404.88, 'YearConcept' => 2021],
            ['Articulo' => '46, 3', 'Concepto' => '0136', 'Uso' => 'Residencial', 'Descripcion' => 'Residencial' , 'Importe' => 3026.49, 'YearConcept' => 2021],
            ['Articulo' => '46, 4', 'Concepto' => '0137', 'Uso' => 'Habitacional', 'Descripcion' => 'Habitacional' , 'Importe' => 1664.6, 'YearConcept' => 2021],
            ['Articulo' => '46, 5', 'Concepto' => '0138', 'Uso' => 'Comercial', 'Descripcion' => 'Comercial' , 'Importe' => 2118.53, 'YearConcept' => 2021],
            ['Articulo' => '46, 6', 'Concepto' => '0139', 'Uso' => 'Servicios', 'Descripcion' => 'Servicios' , 'Importe' => 1664.6, 'YearConcept' => 2021],
            ['Articulo' => '46, 7', 'Concepto' => '0140', 'Uso' => 'Industrial y Agroindustrial', 'Descripcion' => 'Industrial y Agroindustrial' , 'Importe' => 1361.89, 'YearConcept' => 2021],
            ['Articulo' => '46, 8', 'Concepto' => '0141', 'Uso' => 'Equipamiento', 'Descripcion' => 'Equipamiento' , 'Importe' => 756.64, 'YearConcept' => 2021],
            ['Articulo' => '46, 9', 'Concepto' => '0142', 'Uso' => 'Infraestructura', 'Descripcion' => 'Infraestructura' , 'Importe' => 756.64, 'YearConcept' => 2021],
            ['Articulo' => '47, 1, A', 'Concepto' => '0143', 'Uso' => 'Por validación de dictamen de seguridad estructural por M2', 'Descripcion' => 'Turístico' , 'Importe' => 22.69, 'YearConcept' => 2021],
            ['Articulo' => '47, 1, B', 'Concepto' => '0144', 'Uso' => 'Por validación de dictamen de seguridad estructural por M3', 'Descripcion' => 'Residencial' , 'Importe' => 22.69, 'YearConcept' => 2021],
            ['Articulo' => '47, 1, C', 'Concepto' => '0145', 'Uso' => 'Por validación de dictamen de seguridad estructural por M4', 'Descripcion' => 'Habitacional' , 'Importe' => 7.56, 'YearConcept' => 2021],
            ['Articulo' => '47, 1, D', 'Concepto' => '0146', 'Uso' => 'Por validación de dictamen de seguridad estructural por M5', 'Descripcion' => 'Comercial' , 'Importe' => 15.11, 'YearConcept' => 2021],
            ['Articulo' => '47, 1, E', 'Concepto' => '0147', 'Uso' => 'Por validación de dictamen de seguridad estructural por M6', 'Descripcion' => 'Servicios' , 'Importe' => 15.11, 'YearConcept' => 2021],
            ['Articulo' => '47, 1, F', 'Concepto' => '0148', 'Uso' => 'Por validación de dictamen de seguridad estructural por M7', 'Descripcion' => 'Industrial y Agroindustrial' , 'Importe' => 22.69, 'YearConcept' => 2021],
            ['Articulo' => '47, 1, G', 'Concepto' => '0149', 'Uso' => 'Por validación de dictamen de seguridad estructural por M8', 'Descripcion' => 'Equipamiento' , 'Importe' => 7.56, 'YearConcept' => 2021],
            ['Articulo' => '47, 1, H', 'Concepto' => '0150', 'Uso' => 'Por validación de dictamen de seguridad estructural por M9', 'Descripcion' => 'Infraestructura' , 'Importe' => 0, 'YearConcept' => 2021],
            ['Articulo' => '47, 2', 'Concepto' => '0151', 'Uso' => 'Por dictamen de ocupación de terreno por
            construcción, por trámite', 'Descripcion' => 'Por dictamen de ocupación de terreno por
            construcción, por trámite' , 'Importe' => 907.96, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, A', 'Concepto' => '0152', 'Uso' => 'Residencial, Turístico Residencial,
            Turístico Condominal, Turístico
            Hotelero', 'Descripcion' => 'Residencial, Turístico Residencial,
            Turístico Condominal, Turístico
            Hotelero' , 'Importe' => 1513.25, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, B, B.1', 'Concepto' => '0153', 'Uso' => 'Habitacional', 'Descripcion' => 'Hasta 100.00 M2' , 'Importe' => 378.32, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, B, B.2', 'Concepto' => '0154', 'Uso' => 'Habitacional', 'Descripcion' => 'Hasta 200.00 M2' , 'Importe' => 605.3, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, B, B.3', 'Concepto' => '0155', 'Uso' => 'Habitacional', 'Descripcion' => 'Hasta 300.00 M2' , 'Importe' => 1134.95, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, B, B.4', 'Concepto' => '0156', 'Uso' => 'Habitacional', 'Descripcion' => 'Más de 300.00 M2' , 'Importe' => 1513.25, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, C', 'Concepto' => '0157', 'Uso' => 'Comercial', 'Descripcion' => 'Comercial' , 'Importe' => 1513.25, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, D', 'Concepto' => '0158', 'Uso' => 'Servicios', 'Descripcion' => 'Servicios' , 'Importe' => 1134.95, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, E', 'Concepto' => '0159', 'Uso' => 'Industrial y Agroindustrial', 'Descripcion' => 'Industrial y Agroindustrial' , 'Importe' => 605.3, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, F', 'Concepto' => '0160', 'Uso' => 'Equipamiento', 'Descripcion' => 'Equipamiento' , 'Importe' => 453.98, 'YearConcept' => 2021],
            ['Articulo' => '48, 1, G', 'Concepto' => '0161', 'Uso' => 'Cajón de estacionamiento y/o
            bodega', 'Descripcion' => 'Cajón de estacionamiento y/o
            bodega' , 'Importe' => 378.32, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, A', 'Concepto' => '0162', 'Uso' => 'Residencial, Turístico Residencial, Turístico
            Condominal, Turístico Hotelero', 'Descripcion' => 'Residencial, Turístico Residencial, Turístico
            Condominal, Turístico Hotelero' , 'Importe' => 226.99, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, B, B.1', 'Concepto' => '0163', 'Uso' => 'Habitacional', 'Descripcion' => 'Hasta 100.00 M2' , 'Importe' => 75.65, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, B, B.2', 'Concepto' => '0164', 'Uso' => 'Habitacional', 'Descripcion' => 'Hasta 200.00 M2' , 'Importe' => 90.76, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, B, B.3', 'Concepto' => '0165', 'Uso' => 'Habitacional', 'Descripcion' => 'Hasta 300.00 M2' , 'Importe' => 113.49, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, B, B.4', 'Concepto' => '0166', 'Uso' => 'Habitacional', 'Descripcion' => 'Más de 300.00 M2' , 'Importe' => 136.18, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, C', 'Concepto' => '0167', 'Uso' => 'Comercial', 'Descripcion' => 'Comercial' , 'Importe' => 302.67, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, D', 'Concepto' => '0168', 'Uso' => 'Servicios', 'Descripcion' => 'Servicios' , 'Importe' => 302.67, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, E', 'Concepto' => '0169', 'Uso' => 'Industrial y Agroindustrial', 'Descripcion' => 'Industrial y Agroindustrial' , 'Importe' => 302.67, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, F', 'Concepto' => '0170', 'Uso' => 'Equipamiento', 'Descripcion' => 'Equipamiento' , 'Importe' => 226.99, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, G, G.1', 'Concepto' => '0171', 'Uso' => 'Habitacional por cajón de estacionamiento o
            bodega en áreas comunes', 'Descripcion' => 'Hasta 100.00 M2' , 'Importe' => 83.21, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, G, G.2', 'Concepto' => '0172', 'Uso' => 'Habitacional por cajón de estacionamiento o
            bodega en áreas comunes', 'Descripcion' => 'Hasta 200.00 M2' , 'Importe' => 98.35, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, H, H.1', 'Concepto' => '0173', 'Uso' => 'Habitacional Residencial, Turístico Residencial,
            Turístico Condominal, Turístico Hotelero por cajón
            de estacionamiento o bodega en áreas comunes', 'Descripcion' => 'Hasta 100.00 M2' , 'Importe' => 121.04, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, H, H.2', 'Concepto' => '0174', 'Uso' => 'Habitacional Residencial, Turístico Residencial,
            Turístico Condominal, Turístico Hotelero por cajón
            de estacionamiento o bodega en áreas comunes', 'Descripcion' => 'Hasta 200.00 M2' , 'Importe' => 143.75, 'YearConcept' => 2021],
            ['Articulo' => '48, 2, 3, A', 'Concepto' => '0175', 'Uso' => 'Designación de área común', 'Descripcion' => 'Designación de área común' , 'Importe' => 167.36, 'YearConcept' => 2021],
            ['Articulo' => '48, 3, A', 'Concepto' => '0176', 'Uso' => 'Plano del Municipio de Bahía de Banderas, en diferentes escalas,
            formato hasta 90 centímetros por 60 centímetros, impresión a color,
            papel bond o similar.', 'Descripcion' => 'Plano del Municipio de Bahía de Banderas, en diferentes escalas,
            formato hasta 90 centímetros por 60 centímetros, impresión a color,
            papel bond o similar.' , 'Importe' => 98.37, 'YearConcept' => 2021],
            ['Articulo' => '48, 3, B', 'Concepto' => '0177', 'Uso' => 'Plano de zonificación primaria o secundaria, en diferentes escalas,
            formato hasta 90 centímetros por 60 centímetros, impresión a color,
            papel bond o similar.', 'Descripcion' => 'Plano de zonificación primaria o secundaria, en diferentes escalas,
            formato hasta 90 centímetros por 60 centímetros, impresión a color,
            papel bond o similar.' , 'Importe' => 98.37, 'YearConcept' => 2021],
            ['Articulo' => '49, 1, A', 'Concepto' => '0178', 'Uso' => 'Constancia de habitabilidad, uso Habitacional, por vivienda', 'Descripcion' => 'Habitacional popular en general' , 'Importe' => 756.62, 'YearConcept' => 2021],
            ['Articulo' => '49, 1, B', 'Concepto' => '0179', 'Uso' => 'Constancia de habitabilidad, uso Habitacional, por vivienda', 'Descripcion' => 'Habitacional Popular para Fraccionamientos' , 'Importe' => 1134.95, 'YearConcept' => 2021],
            ['Articulo' => '49, 1, C', 'Concepto' => '0180', 'Uso' => 'Constancia de habitabilidad, uso Habitacional, por vivienda', 'Descripcion' => 'Habitacional medio' , 'Importe' => 1513.25, 'YearConcept' => 2021],
            ['Articulo' => '49, 1, D', 'Concepto' => '0181', 'Uso' => 'Constancia de habitabilidad, uso Habitacional, por vivienda', 'Descripcion' => 'Habitacional residencial' , 'Importe' => 4539.74, 'YearConcept' => 2021],
            ['Articulo' => '49, 2, A', 'Concepto' => '0182', 'Uso' => 'Constancia de habitabilidad, uso hotelero y tiempo
            compartido por', 'Descripcion' => 'Cuarto de hotel y motel' , 'Importe' => 3783.14, 'YearConcept' => 2021],
            ['Articulo' => '49, 2, B', 'Concepto' => '0183', 'Uso' => 'Constancia de habitabilidad, uso hotelero y tiempo
            compartido por', 'Descripcion' => 'Cuarto.- suite o junior suite.' , 'Importe' => 4539.74, 'YearConcept' => 2021],
            ['Articulo' => '49, 2, C', 'Concepto' => '0184', 'Uso' => 'Constancia de habitabilidad, uso hotelero y tiempo
            compartido por', 'Descripcion' => 'Departamento, estudio, llave hotelera, villa, cabaña, bungalow
            o casa hotel' , 'Importe' => 5674.71, 'YearConcept' => 2021],
            ['Articulo' => '49, 2, D', 'Concepto' => '0185', 'Uso' => 'Constancia de habitabilidad, uso hotelero y tiempo
            compartido por', 'Descripcion' => 'Unidad de tiempo compartido' , 'Importe' => 5674.71, 'YearConcept' => 2021],
            ['Articulo' => '49, 3', 'Concepto' => '0186', 'Uso' => 'Constancia de habitabilidad, teatros, cines, centros
            comerciales, centros de espectáculos, templos, por metro
            cuadrado construido.', 'Descripcion' => 'Constancia de habitabilidad, teatros, cines, centros
            comerciales, centros de espectáculos, templos, por metro
            cuadrado construido.' , 'Importe' => 75.67, 'YearConcept' => 2021],
            ['Articulo' => '49, 4', 'Concepto' => '0187', 'Uso' => 'Constancia de habitabilidad para terrazas, albercas,
            áreas comunes o casa club por metro cuadrado en
            desarrollos hoteleros o turísticos.', 'Descripcion' => 'Constancia de habitabilidad para terrazas, albercas,
            áreas comunes o casa club por metro cuadrado en
            desarrollos hoteleros o turísticos.' , 'Importe' => 75.67, 'YearConcept' => 2021],
            ['Articulo' => '49, 5', 'Concepto' => '0188', 'Uso' => 'Constancia de habitabilidad de estacionamientos, aun como
            parte de un centro comercial, por metro cuadrado de
            estacionamiento.', 'Descripcion' => 'Constancia de habitabilidad de estacionamientos, aun como
            parte de un centro comercial, por metro cuadrado de
            estacionamiento.' , 'Importe' => 37.8, 'YearConcept' => 2021],
            ['Articulo' => '49, 6', 'Concepto' => '0189', 'Uso' => 'Visto bueno para negocios, por metro cuadrado
            construido.', 'Descripcion' => 'Visto bueno para negocios, por metro cuadrado
            construido.' , 'Importe' => 15.13, 'YearConcept' => 2021],
            ['Articulo' => '49, 7', 'Concepto' => '0190', 'Uso' => 'Copia certificada y/o digital de documentos oficiales
            expedidos, por hoja:', 'Descripcion' => 'Copia certificada y/o digital de documentos oficiales
            expedidos, por hoja:' , 'Importe' => 30.26, 'YearConcept' => 2021],
            ['Articulo' => '50, 1', 'Concepto' => '0191', 'Uso' => 'Vivienda unifamiliar', 'Descripcion' => 'Vivienda unifamiliar' , 'Importe' => 328.45, 'YearConcept' => 2021],
            ['Articulo' => '50, 2', 'Concepto' => '0192', 'Uso' => 'Vivienda dúplex', 'Descripcion' => 'Vivienda dúplex' , 'Importe' => 547.42, 'YearConcept' => 2021],
            ['Articulo' => '50, 3', 'Concepto' => '0193', 'Uso' => 'Cotos o bloques de viviendas hasta por treinta casas', 'Descripcion' => 'Cotos o bloques de viviendas hasta por treinta casas' , 'Importe' => 3284.54, 'YearConcept' => 2021],
            ['Articulo' => '50, 4', 'Concepto' => '0194', 'Uso' => 'Cotos o bloques de viviendas hasta por sesenta casas', 'Descripcion' => 'Cotos o bloques de viviendas hasta por sesenta casas' , 'Importe' => 5474.24, 'YearConcept' => 2021],
            ['Articulo' => '50, 5', 'Concepto' => '0195', 'Uso' => 'Cotos o bloques de viviendas hasta por noventa casas', 'Descripcion' => 'Cotos o bloques de viviendas hasta por noventa casas' , 'Importe' => 7663.94, 'YearConcept' => 2021],
            ['Articulo' => '51, I', 'Concepto' => '0196', 'Uso' => 'Inscripción de Directores Responsables de Obra en la Dirección de Desarrollo
            Urbano y Ecología, por trámite', 'Descripcion' => 'Inscripción de Directores Responsables de Obra en la Dirección de Desarrollo
            Urbano y Ecología, por trámite' , 'Importe' => 3783.14, 'YearConcept' => 2021],
            ['Articulo' => '51, II', 'Concepto' => '0197', 'Uso' => 'Renovación del registro de Directores Responsables de Obra en la Dirección de
            Desarrollo Urbano y Ecología, por trámite', 'Descripcion' => 'Renovación del registro de Directores Responsables de Obra en la Dirección de
            Desarrollo Urbano y Ecología, por trámite' , 'Importe' => 1891.57, 'YearConcept' => 2021],
            ['Articulo' => '51, III, A', 'Concepto' => '0198', 'Uso' => 'El registro de proveedores o contratistas al padrón respectivo, deberá
            efectuarse ante el Organismo Operador Municipal de Agua Potable,
            Alcantarillado y Saneamiento de Bahía de Banderas, Nayarit, y se regirá
            conforme al siguiente tabulador', 'Descripcion' => 'Inscripción de proveedores de bienes o servicios' , 'Importe' => 766.39, 'YearConcept' => 2021],
            ['Articulo' => '51, III, B', 'Concepto' => '0199', 'Uso' => 'El registro de proveedores o contratistas al padrón respectivo, deberá
            efectuarse ante el Organismo Operador Municipal de Agua Potable,
            Alcantarillado y Saneamiento de Bahía de Banderas, Nayarit, y se regirá
            conforme al siguiente tabulador', 'Descripcion' => 'Inscripción de Contratistas de Obra Pública' , 'Importe' => 766.39, 'YearConcept' => 2021],
            ['Articulo' => '51, III, C', 'Concepto' => '0200', 'Uso' => 'El registro de proveedores o contratistas al padrón respectivo, deberá
            efectuarse ante el Organismo Operador Municipal de Agua Potable,
            Alcantarillado y Saneamiento de Bahía de Banderas, Nayarit, y se regirá
            conforme al siguiente tabulador', 'Descripcion' => 'Refrendo anual de Proveedores y/o Contratistas' , 'Importe' => 383.2, 'YearConcept' => 2021],
            ['Articulo' => '52, I', 'Concepto' => '0201', 'Uso' => 'Acreditación de directores y peritos responsables de obra', 'Descripcion' => 'Acreditación de directores y peritos responsables de obra' , 'Importe' => 2648.19, 'YearConcept' => 2021],
            ['Articulo' => '52, II', 'Concepto' => '0202', 'Uso' => 'Refrendo de acreditación de directores y peritos responsables
            de obra.', 'Descripcion' => 'Refrendo de acreditación de directores y peritos responsables
            de obra.' , 'Importe' => 1891.57, 'YearConcept' => 2021],
            ['Articulo' => '52, III', 'Concepto' => '0203', 'Uso' => 'Inscripción de proveedores de bienes o servicios.', 'Descripcion' => 'Inscripción de proveedores de bienes o servicios.' , 'Importe' => 1891.57, 'YearConcept' => 2021],
            ['Articulo' => '52, IV', 'Concepto' => '0204', 'Uso' => 'Inscripción de contratistas de obra pública', 'Descripcion' => 'Inscripción de contratistas de obra pública' , 'Importe' => 5455.3, 'YearConcept' => 2021],
            ['Articulo' => '52, V', 'Concepto' => '0205', 'Uso' => 'Derechos de Inscripción en el fundo Municipal', 'Descripcion' => 'Derechos de Inscripción en el fundo Municipal' , 'Importe' => 367.3, 'YearConcept' => 2021],
            ['Articulo' => '57, A', 'Concepto' => '0206', 'Uso' => 'Anuncio tipo cartelera de piso.', 'Descripcion' => 'Anuncio tipo cartelera de piso.' , 'Importe' => 779.33, 'YearConcept' => 2021],
            ['Articulo' => '57, B', 'Concepto' => '0207', 'Uso' => 'Anuncio tipo pantalla electrónica', 'Descripcion' => 'Anuncio tipo pantalla electrónica' , 'Importe' => 15586.54, 'YearConcept' => 2021],
            ['Articulo' => '57, C', 'Concepto' => '0208', 'Uso' => 'Anuncio tipo estela', 'Descripcion' => 'Anuncio tipo estela' , 'Importe' => 1168.99, 'YearConcept' => 2021],
            ['Articulo' => '57, D', 'Concepto' => '0209', 'Uso' => 'Anuncio tipo navaja.', 'Descripcion' => 'Anuncio tipo navaja.' , 'Importe' => 3896.63, 'YearConcept' => 2021],
            ['Articulo' => '57, E', 'Concepto' => '0210', 'Uso' => 'Anuncio tipo valla', 'Descripcion' => 'Anuncio tipo valla' , 'Importe' => 1168.99, 'YearConcept' => 2021],
            ['Articulo' => '57, F', 'Concepto' => '0211', 'Uso' => 'Anuncio tipo toldo', 'Descripcion' => 'Anuncio tipo toldo' , 'Importe' => 1168.99, 'YearConcept' => 2021],
            ['Articulo' => '57, G', 'Concepto' => '0212', 'Uso' => 'Anuncio tipo rotulado', 'Descripcion' => 'Anuncio tipo rotulado' , 'Importe' => 21.26, 'YearConcept' => 2021],
            ['Articulo' => '57, G, 1', 'Concepto' => '0213', 'Uso' => 'Anuncio rotulado turístico con Iluminación', 'Descripcion' => 'Anuncio rotulado turístico con Iluminación' , 'Importe' => 779.33, 'YearConcept' => 2021],
            ['Articulo' => '57, G, 2', 'Concepto' => '0214', 'Uso' => 'Anuncio rotulado turístico sin iluminación', 'Descripcion' => 'Anuncio rotulado turístico sin iluminación' , 'Importe' => 311.72, 'YearConcept' => 2021],
            ['Articulo' => '57, G, 3', 'Concepto' => '0215', 'Uso' => 'Anuncio rotulado popular con iluminación', 'Descripcion' => 'Anuncio rotulado popular con iluminación' , 'Importe' => 389.67, 'YearConcept' => 2021],
            ['Articulo' => '57, G, 4', 'Concepto' => '0216', 'Uso' => 'Anuncio rotulado popular sin iluminación', 'Descripcion' => 'Anuncio rotulado popular sin iluminación' , 'Importe' => 155.86, 'YearConcept' => 2021],
            ['Articulo' => '57, H', 'Concepto' => '0217', 'Uso' => 'Anuncio de tijera o caballete (hasta por 45 días)', 'Descripcion' => 'Anuncio de tijera o caballete (hasta por 45 días)' , 'Importe' => 233.8, 'YearConcept' => 2021],
            ['Articulo' => '57, I', 'Concepto' => '0218', 'Uso' => 'Colocación de pendones (hasta por 45 días)', 'Descripcion' => 'Colocación de pendones (hasta por 45 días)' , 'Importe' => 467.57, 'YearConcept' => 2021],
            ['Articulo' => '57, J', 'Concepto' => '0219', 'Uso' => 'Anuncio colgante como manta o lona (hasta por 45 días', 'Descripcion' => 'Anuncio colgante como manta o lona (hasta por 45 días' , 'Importe' => 2337.99, 'YearConcept' => 2021],
            ['Articulo' => '57, K', 'Concepto' => '0220', 'Uso' => 'anuncio inflable de piso (hasta por 15 días)', 'Descripcion' => 'anuncio inflable de piso (hasta por 15 días)' , 'Importe' => 233.8, 'YearConcept' => 2021],
            ['Articulo' => '57, L', 'Concepto' => '0221', 'Uso' => 'Anuncio tipo poster parabuses (por cara)', 'Descripcion' => 'Anuncio tipo poster parabuses (por cara)' , 'Importe' => 779.33, 'YearConcept' => 2021],
            ['Articulo' => '57, M', 'Concepto' => '0222', 'Uso' => 'Volanteo por millar (hasta por 15 días)', 'Descripcion' => 'Volanteo por millar (hasta por 15 días)' , 'Importe' => 226.55, 'YearConcept' => 2021],
            ['Articulo' => '57, N, 1', 'Concepto' => '0223', 'Uso' => 'Perifoneo
            (No se autoriza el perifoneo después de las 22:00 horas)', 'Descripcion' => 'Por día y por una unidad' , 'Importe' => 15.62, 'YearConcept' => 2021],
            ['Articulo' => '57, N, 2', 'Concepto' => '0224', 'Uso' => 'Perifoneo
            (No se autoriza el perifoneo después de las 22:00 horas)', 'Descripcion' => 'Unidad extra por día' , 'Importe' => 7.78, 'YearConcept' => 2021],
            ['Articulo' => '57, Ñ', 'Concepto' => '0225', 'Uso' => 'Garantía por publicidad de 1 a 100 pendones o carteles.', 'Descripcion' => 'Garantía por publicidad de 1 a 100 pendones o carteles.' , 'Importe' => 5000, 'YearConcept' => 2021],
        ]);
    }
}
