<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTablePcSolicitudesVistoBuenoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->string('umpc', 16)->nullable();
            $table->string('ldrr', 16)->nullable();
            $table->string('cc', 16)->nullable();
            $table->string('numero_expediente', 45)->nullable();
            $table->string('testigo_1', 255)->nullable();
            $table->string('testigo_2', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->dropColumn('umpc');
            $table->dropColumn('ldrr');
            $table->dropColumn('cc');
            $table->dropColumn('numero_expediente');
            $table->dropColumn('testigo_1');
            $table->dropColumn('testigo_2');
        });
    }
}
