<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTablePcExpedientes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pc_expedientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('no_expediente')->nullable();
            $table->string('domicilio')->nullable();
            $table->string('numero')->nullable();
            $table->string('colonia')->nullable();
            $table->string('localidad')->nullable();
            $table->string('razon_social')->nullable();
            $table->string('rfc')->nullable();
            $table->string('nombre_comercial')->nullable();
            $table->string('grupo')->nullable();
            $table->string('giro')->nullable();
            $table->string('campo_de_riesgo')->nullable();
            $table->string('telefono')->nullable();
            $table->string('correo')->nullable();
            $table->text('observaciones')->nullable();
            $table->text('referencias')->nullable();
            $table->string('estado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pc_expedientes');
    }
}
