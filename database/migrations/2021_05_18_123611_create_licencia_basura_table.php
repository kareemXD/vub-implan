<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicenciaBasuraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlsrv')->create('Licencia_Basura', function (Blueprint $table) {
            $table->integer('Id');
            $table->string('LicenciaId');
            $table->string('Year', 4);
            $table->string('Concepto', 15);
            $table->string('Descripcion', 255);
            $table->decimal('Importe', 11, 2);

            $table->primary('Id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sqlsrv')->dropIfExists('Licencia_Basura');
    }
}
