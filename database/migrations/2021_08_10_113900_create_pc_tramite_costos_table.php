<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePcTramiteCostosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pc_tramite_costos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_tramite');
            $table->integer('year');
            $table->decimal('costo', 11,2);
            $table->decimal('costo_refrendo', 11,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pc_tramite_costos');
    }
}
