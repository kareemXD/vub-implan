<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldNumeroExpedientePcSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->dropColumn('numero_expediente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->string('numero_expediente', 45)->nullable();
        });
    }
}
