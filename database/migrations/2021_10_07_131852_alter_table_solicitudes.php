<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableSolicitudes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlsrv')->table('SolicitudLicencia', function (Blueprint $table) {
            $table->date('FechaAlta')->nullable();
            $table->string('NumeroTarjetonAlcoholes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sqlsrv')->table('SolicitudLicencia', function (Blueprint $table) {
            $table->dropColumn('FechaAlta');
            $table->dropColumn('NumeroTarjetonAlcoholes');
        });
    }
}
