<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableGiroHasTrash extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('giro_has_trash', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('giro_id');
            $table->integer('year');
            $table->decimal('costo_descarga', 11, 2)->default(0.00);
            $table->decimal('costo_recoleccion', 11, 2)->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('giro_has_trash');
    }
}
