<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterPcSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->integer('pc_inmueble_sector_id')->nullable();
            $table->integer('pc_tipo_establecimiento_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->dropColumn('pc_inmueble_sector_id');
            $table->dropColumn('pc_tipo_establecimiento_id');
        });
    }
}
