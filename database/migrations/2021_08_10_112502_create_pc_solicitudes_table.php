<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePcSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pc_solicitudes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_solicitud', 15);
            $table->integer('id_poblacion');
            $table->integer('id_colonia');
            $table->integer('id_calle');
            $table->integer('id_cruce1')->nullable();
            $table->integer('id_cruce2')->nullable();
            $table->string('no_exterior', 45);
            $table->string('no_interior', 45)->nullable();
            $table->decimal('coordenada_este', 11, 10);
            $table->decimal('coordenada_norte', 11, 10);
            $table->date('fecha_solicitud');
            $table->string('nombre', 255);
            $table->string('giro', 255)->nullable();
            $table->string('rfc', 50)->nullable();
            $table->string('no_licencia', 50)->nullable();
            $table->string('estatus_licencia', 45);
            $table->string('nombre_comercial', 255)->nullable();
            $table->integer('no_empleados')->nullable();
            $table->string('horario_laboral')->nullable();
            $table->string('nombre_solicitante')->nullable();
            $table->string('caracter_juridico_solicitante')->nullable();
            $table->string('telefono_celular')->nullable();
            $table->string('telefono_fijo')->nullable();
            $table->string('email', 100)->nullable();
            $table->string('otro_tramite')->nullable();
            $table->text('resultado_dictaminacion')->nullable();
            $table->string('origen', 45)->nullable();
            $table->integer('estatus')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pc_solicitudes');
    }
}
