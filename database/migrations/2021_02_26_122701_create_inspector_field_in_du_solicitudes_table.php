<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInspectorFieldInDuSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('du_solicitudes', function (Blueprint $table) {
            $table->integer('id_inspector')->nullable();

            $table->foreign('id_inspector')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('du_solicitudes', function (Blueprint $table) {
            $table->dropColumn('id_inspector');
        });
    }
}
