<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DuConceptos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('du_conceptos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("Articulo");
            $table->string('Concepto', 4);
            $table->text('Uso');
            $table->text('Descripcion');
            $table->double('Importe', 12, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('du_conceptos');
    }
}
