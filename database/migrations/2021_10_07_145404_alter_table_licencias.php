<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableLicencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlsrv')->table('Licencias', function (Blueprint $table) {
            $table->string('NumeroTarjetonAlcoholes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sqlsrv')->table('Licencias', function (Blueprint $table) {
            $table->dropColumn('NumeroTarjetonAlcoholes');
        });
    }
}
