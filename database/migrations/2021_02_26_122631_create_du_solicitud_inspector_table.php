<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDuSolicitudInspectorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('du_solicitud_inspector', function (Blueprint $table) {
            $table->integer('id_solicitud');
            $table->integer('id_inspector');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('id_solicitud')->references('id')->on('du_solicitudes');
            $table->foreign('id_inspector')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('du_solicitud_inspector');
    }
}
