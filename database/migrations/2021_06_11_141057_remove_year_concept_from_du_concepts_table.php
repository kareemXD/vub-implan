<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveYearConceptFromDuConceptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('du_conceptos', function (Blueprint $table) {
            $table->dropColumn('YearConcept');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('du_conceptos', function (Blueprint $table) {
            $table->integer('YearConcept')->default('2021');
        });
    }
}
