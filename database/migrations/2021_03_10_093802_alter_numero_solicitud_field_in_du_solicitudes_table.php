<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterNumeroSolicitudFieldInDuSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('du_solicitudes', function (Blueprint $table) {
            $table->string('numero_solicitud', 15)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('du_solicitudes', function (Blueprint $table) {
            $table->string('numero_solicitud', 10)->change();
        });
    }
}
