<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDuSolicitudDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('du_solicitud_documentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file', 100)->nullable();
            $table->text('description')->nullable();
            $table->integer('id_solicitud');
            $table->timestamp('created_at')->useCurrent();

            $table->foreign('id_solicitud')->references('id')->on('du_solicitudes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('du_solicitud_documentos');
    }
}
