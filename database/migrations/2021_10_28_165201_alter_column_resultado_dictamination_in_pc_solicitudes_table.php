<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnResultadoDictaminationInPcSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->renameColumn('resultado_dictaminacion', 'resultado_verificacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->renameColumn('resultado_verificacion', 'resultado_dictaminacion');
        });
    }
}
