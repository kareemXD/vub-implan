<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableGiroDetalleLicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sqlsrv')->table('GiroDetalleLic', function (Blueprint $table) {
            $table->boolean('Refrendo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sqlsrv')->table('GiroDetalleLic', function (Blueprint $table) {
            $table->dropColumn('Refrendo');
        });
    }
}
