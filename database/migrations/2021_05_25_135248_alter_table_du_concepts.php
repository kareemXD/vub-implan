<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableDuConcepts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('du_conceptos', function (Blueprint $table) {
            $table->boolean('UAN')->default("0");
            $table->boolean('OverCost')->default("0");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('du_conceptos', function (Blueprint $table) {
            $table->dropColumn('UAN');
            $table->dropColumn('OverCost');
        });
    }
}
