<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDuSolicitudTramiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('du_solicitud_tramite', function (Blueprint $table) {
            $table->integer('solicitud_id');
            $table->integer('tramite_id');

            $table->foreign('solicitud_id')->references('id')->on('du_solicitudes');
            $table->foreign('tramite_id')->references('id')->on('du_tramites');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('du_solicitud_tramite');
    }
}
