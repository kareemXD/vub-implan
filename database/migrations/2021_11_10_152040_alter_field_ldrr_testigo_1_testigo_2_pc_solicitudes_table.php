<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFieldLdrrTestigo1Testigo2PcSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->string('ldrr', 255)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pc_solicitudes', function (Blueprint $table) {
            $table->string('expediente', 255)->nullable();
        });
    }
}
