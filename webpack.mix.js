const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.stylus('resources/assets/stylus/style.styl', 'public/assets/css/', { use: [require('nib')()] } ).version();
mix.stylus('resources/assets/stylus/login.styl', 'public/assets/css/', { use: [require('nib')()] } ).version();

//mix.js('resources/assets/js/app.js', 'public/js/app.js');
