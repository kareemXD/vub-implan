<?php

namespace App\Http\Controllers;

use App\Models\Padron\Debts;
use App\Models\ProteccionCivil\Request as ProteccionCivilRequest;
use App\Models\ProteccionCivil\RequestTurn;
use Illuminate\Http\Request;

//Plugins 
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Auth;
use Illuminate\Support\Facades\DB;

class SecretController extends Controller
{
    public function __construct()
    {
        set_time_limit(-1);
    }

    public function createRoles($password = null)
    {
        if($password != null)
        {
            if($password == "SistemasImplan18")
            {
                Role::create(['name' => 'Administrador']);
                Role::create(['name' => 'Cliente']);

                dd("Acción Terminada con Exíto");
            }else
            {
                dd('Acción Denegada');
            }
        }else
        {
            dd('Acción Denegada');
        }
    }

    public function assignAdministrator($password = null)
    {
        if($password != null)
        {
            if($password == "SistemasImplan18")
            {
                if(Auth::check())
                {
                    $user = Auth::user();
                    $user->assignRole('Administrador');
                    
                    dd("Acción Terminada con Exíto");
                }else {
                    dd('Acción Denegada1');
                }
            }else
            {
                dd('Acción Denegada2');
            }
        }else
        {
            dd('Acción Denegada3');
        }
    }

    public function assignCorrectDescriptionToDebts($year, $password = null)
    {
        if($password != null)
        {
            if($password == "SistemasImplan18")
            {
                /* 
                    Corregir posibles errores de descripción en los adeudos,
                    por lo que este script lo que hace es hacer un barrido de todos los adeudos,
                    buscar su concepto y colocar la descripción correcta.

                    Debido a que los adeudos de anuncios aparecen con descripción de licencia de funcionamiento, 
                    cosa que esta mal ya que debe de aparecer la descripción del anuncio tal cual.

                    La ventaja es que el adeudo si tiene el id de concepto bien, por lo que solo hay que buscar
                    en la tabla de conceptos la descripción correcta y colocarla en la descripción del adeudo.
                */

                // Palabras a buscar en las descripciones de los conceptos de los adeudos, que correspondan a adeudos.
                $wordsToSearch = ['ANUNC', 'PENDON', 'DIFUSION', 'PERIFONEO'];

                // Estadisticas:
                $statistics = [
                    'updates' => [
                        'debts' => [],
                        'total' => 0
                    ],
                    'totalDebts' => 0,
                    'time' => [
                        'start' => now(),
                        'end' => ""
                    ]
                ];

                // Obtener todos los adeudos
                $debts = DB::connection('sqlsrv')->table('Adeudos')
                            ->where(function($query) use($year) {
                                $query->where('Referencia', 'like', $year.'%')
                                        ->where('Departamento', '02');
                            })->get();

                $statistics['totalDebts'] = count($debts);

                // Iterar sobre todos los adeudos
                foreach ($debts as $debt)
                {
                    // Obtener el concepto correspondiente del adeudo
                    $concept = DB::connection('sqlsrv')->table('Conceptos')->where('Concepto', $debt->Concepto)->first();

                    // Validar que el concepto exista o que tenga una relación
                    if  (!empty($concept))
                    {
                        foreach ($wordsToSearch as $word)
                        {
                            // Buscar coincidencias con las palabras buscadas.
                            if ((strpos(strtoupper($concept->Descripcion), $word) !== false))
                            {
                                // Validar si la descripción del concepto no corresponde con la descripción de la descripción.
                                if ($debt->Descripcion != $concept->Descripcion)
                                {
                                    // Guardar el adeudo con la descripción correcta
                                    DB::connection('sqlsrv')->table('Adeudos')->where(function($query) use($debt) {
                                        $query->where('Departamento', '02')
                                                ->where('CuentaDepto', $debt->CuentaDepto)
                                                ->where('Concepto', $debt->Concepto)
                                                ->where('Referencia', $debt->Referencia);
                                    })
                                    ->update(['Descripcion' => $concept->Descripcion]);
                    
                                    // Adeudo no actualizado
                                    $statistics['updates']['debts'][] = 'lic:'.$debt->CuentaDepto.' ref:'.$debt->Referencia;
                                    $statistics['updates']['total'] += 1;
                                }
                            }
                        }
                    }
                }

                $statistics['time']['end'] = now();

                // Fin
                return response()->json([
                    'data' => [
                        'statistics' => $statistics
                    ]
                ]);
            }else
            {
                dd('Acción Denegada1');
            }
        }else
        {
            dd('Acción Denegada2');
        }
    }

    public function syncronizeTurnsForProteccionCivil($password = null)
    {
        if($password != null)
        {
            if($password == "SistemasImplan18")
            {
                $requests = ProteccionCivilRequest::all();

                // Estadisticas:
                $statistics = [
                    'updates' => 0,
                    'totalRequests' => count($requests),
                    'time' => [
                        'start' => now(),
                        'end' => ""
                    ]
                ];

                foreach ($requests as $request)
                {
                    $turn = $request->turn;

                    if (!empty($turn))
                    {
                        RequestTurn::where('id_solicitud', $request->id)->delete();

                        $requestTurn = new RequestTurn;
                        $requestTurn->id_solicitud = $request->id;
                        $requestTurn->id_giro = $turn->IdGiro;
                        $requestTurn->giro = $turn->Nombre;
                        $requestTurn->save();

                        $statistics['updates']++;
                    }
                }

                $statistics['time']['end'] = now();

                dd($statistics);
            }else
            {
                dd('Acción Denegada1');
            }
        }else
        {
            dd('Acción Denegada2');
        }
    }
}
