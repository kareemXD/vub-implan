<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InfoController extends Controller
{
    public function php()
    {
        return phpinfo(INFO_MODULES);
    }
}
