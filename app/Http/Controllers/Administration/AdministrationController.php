<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Repositories
use App\Repositories\AdministrationRepository;
use Carbon\Carbon;

class AdministrationController extends Controller
{
    private $administrationRepository;

    public function __construct(
        AdministrationRepository $administrationRepository
    ){
        $this->administrationRepository = $administrationRepository;

    }
    public function dashboard(){
        $data = $this->administrationRepository->getStaticticsForDashboard();
        $now = Carbon::now();

        return view('administration.dashboard', compact('data', 'now'));
    }
}
