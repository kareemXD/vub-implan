<?php

namespace App\Http\Controllers;

//Services
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;


//Repositories
use App\Repositories\HomeRepository;
use App\Repositories\BitacoraRepository;

class HomeController extends Controller
{
    private $homeRepository;
    private $bitacoraRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        HomeRepository $homeRepository,
        BitacoraRepository $bitacoraRepository
    ){
        //Middleware
        $this->middleware('auth');

        //Repositories
        $this->homeRepository = $homeRepository;
        $this->bitacoraRepository = $bitacoraRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function redirect()
    {
        if(Auth::check())
        {
            $user = Auth::user();
            // dd($user->profile);
            $menu = $user->getMenu();
            //Reportamos en la bitacora el inicio de Session
            $date = Carbon::now("America/Mexico_City");
            $action = [
                "user" => $user->id,
                'type' => config("implan.bitacora.login"),
                'title' => "El usuario ". $user->name . " inicio sesión el dia " .$date->format("d/m/Y"). " a las " .$date->format("H:i"),
                'action' => "El usuario ". $user->name . " inicio sesión el dia " .$date->format("d/m/Y"). " a las " .$date->format("H:i"),
                "direction" => $user->profile->position->direccion_id,
            ];
            $this->bitacoraRepository->insertAction($action);
            //Fin del reporte de inicio de sesión

            return redirect()->route($menu[0]["url"]);
        }else {
            dd("entro");
            return redirect()->back()->with("alert", "Por favor inicie sessión.");
        }
        dd("entro2");
    }

    public function editProfile()
    {
        return view("partials.edit_profile");
    }

    public function notifications()
    {
        $notifications = $this->homeRepository->getAllNotificationByAuhtUser();
        return view("partials.notifications", compact("notifications"));
    }

    public function updateProfile(Request $request)
    {
        if(!empty($request->password)){
            $this->validate($request, [
                'id' => 'required',
                'name' => 'required|string|max:255',
                'password' => 'required|string|min:6|confirmed',
            ],
            [
                "name.required" => "EL nombre de usuario es Requerido.",
                "password.required" => "Por favor ingresa una contraseña valida.",
                "password.confirmed" => "La confimacion de contraseña es invalida.",
                "password.min" => "La contraseña debe contener minimo 6 caracteres.",
            ]);

            $this->homeRepository->updateUser($request->id, $request->name, $request->password);
        }else
        {
            $this->validate($request, [
                'id' => 'required',
                'name' => 'required|string|max:255',
            ],
            [
                "name.required" => "EL nombre de usuario es Requerido.",
            ]);
            $this->homeRepository->updateUserName($request->id, $request->name);
        }
        return redirect()->back();
    }

    public function updateNotification(Request $request)
    {
        $notification = $this->homeRepository->updateNotification($request->id);
        return $notification;
    }
}
