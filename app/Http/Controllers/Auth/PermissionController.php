<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\PermissionRepository;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    private $permissionRepository;

    public function __construct(
        PermissionRepository $permissionRepository
    ){
        $this->permissionRepository = $permissionRepository;
    }

    public function index()
    {
        $permissions = $this->permissionRepository->getPermissions();
        
        return view('auth.permissions.index')->with(compact('permissions'));
    }

    public function create()
    {
        return view('auth.permissions.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'guard' => 'required'
        ]);

        $this->permissionRepository->store($request);

        return redirect()->route('auth.permissions');
    }

    public function edit($permissionId)
    {
        $permission = $this->permissionRepository->getPermissionById($permissionId);

        return view('auth.permissions.edit')->with(compact('permission'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'guard' => 'required',
            'permissionId' => 'required'
        ]);

        $this->permissionRepository->update($request);

        return redirect()->route('auth.permissions');
    }
}
