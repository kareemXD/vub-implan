<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\DirectionRepository;
use Illuminate\Http\Request;

class DirectionController extends Controller
{
    private $directionRepository;

    public function __construct(
        DirectionRepository $directionRepository
    )
    {
        $this->directionRepository = $directionRepository;
    }

    public function index()
    {
        $directions = $this->directionRepository->getDirections();

        return view('auth.directions.index')->with(compact('directions'));
    }

    public function create()
    {
        return view('auth.directions.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'abreviacion' => 'required'
        ]);

        $this->directionRepository->store($request);

        return redirect()->route('auth.directions');
    }

    public function edit($directionId)
    {
        $direction = $this->directionRepository->getDirectionById($directionId);

        return view('auth.directions.edit')->with(compact('direction'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'abreviacion' => 'required'
        ]);

        $this->directionRepository->update($request);

        return redirect()->route('auth.directions');
    }
}
