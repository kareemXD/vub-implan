<?php

namespace App\Http\Controllers\Navigation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Repositories
use App\Repositories\NavigationRepository;

class NavigationController extends Controller
{
    private $navigationRepository;

    public function __construct(
        NavigationRepository $navigationRepository
    ){
        $this->navigationRepository = $navigationRepository;
    }

    public function menus()
    {
        $menus = $this->navigationRepository->getAllMenus();
        return view('navigation.menus', compact("menus"));
    }

    public function newMenu()
    {
        return view('navigation.new_menu');
    }

    public function storeMenu(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'alias' => 'required|unique:menus',
            'url' => 'unique:menus',
            'position' => 'required',
            'icon' => 'required',
        ],
        [
            'name.required' => 'El nombre es requerido.',
            'alias.required' => 'El alias es requerido.',
            'alias.unique' => 'Este alias ya existe.',
            'url.unique' => 'Esta ruta ya existe.',
            'position.required' => 'La posición es requerida.',
            'icon.required' => 'El icono es requerido.',
        ]);
        $this->navigationRepository->storeMenu($request);

        return redirect()->route("menus")->with("alert", "Se creo correctamente el Menu");
    }

    public function editMenu($id)
    {
        $menu = $this->navigationRepository->getMenuById($id);
        return view("navigation.edit_menu", compact("menu"));
    }

    public function updateMenu(Request $request)
    {
        $this->navigationRepository->updateMenu($request);
        return redirect()->route("menus")->with("alert", "Se actualizo correctamente el Menu.");
    }
}
