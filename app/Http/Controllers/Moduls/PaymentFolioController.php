<?php

namespace App\Http\Controllers\Moduls;

use App\Http\Controllers\Controller;
use App\Repositories\PadronRepository;
use App\Repositories\PaymentFolioRepository;
use Illuminate\Http\Request;

class PaymentFolioController extends Controller
{
    private $paymentFolioRepository,
            $padronRepository;

    public function __construct(
        PaymentFolioRepository $paymentFolioRepository,
        PadronRepository $padronRepository
    ){
        $this->paymentFolioRepository = $paymentFolioRepository;
        $this->padronRepository = $padronRepository;
    }

    public function padron(Request $request)
    {
        $account = $request->account;
        $license = null;
        $debts = [];
        $concepts = $this->padronRepository->getAllConceptos();

        if (!empty($account))
        {
            $license = $this->padronRepository->getLicenseById($account);

            if (!empty($license))
            {
                $debts = $this->paymentFolioRepository->getDebtsByPadronAccount($account);
            }
        }

        return view('paymentFolios.padron')->with(compact('account', 'license', 'debts', 'concepts'));
    }

    public function debtUpdate(Request $request)
    {
        $this->paymentFolioRepository->debtUpdate($request);

        return redirect()->back();
    }

    public function debtDelete(Request $request)
    {
        $this->paymentFolioRepository->debtDelete($request);
        
        return redirect()->back();
    }
}
