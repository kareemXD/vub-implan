<?php

namespace App\Http\Controllers\Moduls\DU;

use App\Http\Controllers\Controller;
use App\Repositories\DUConceptRepository;
use Illuminate\Http\Request;

class ConceptController extends Controller
{
    private $duConceptRepository;

    public function __construct(
        DUConceptRepository $duConceptRepository
    ){
        $this->duConceptRepository = $duConceptRepository;
    }

    public function index()
    {
        $concepts = $this->duConceptRepository->getConcepts();

        // Clear concepts 
        // $this->duConceptRepository->clearConcepts();

        return view('du.concepts.index')->with(compact('concepts'));
    }

    public function create()
    {
        $ingresosConceptos = $this->duConceptRepository->getIngresosConceptos();

        return view('du.concepts.create')->with(compact('ingresosConceptos'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'articulo' => 'required',
            'uso' => 'required',
            'concepto' => 'required',
        ]);

        $this->duConceptRepository->store($request);

        return redirect()->route('du.concepts');
    }

    public function edit($conceptId)
    {
        $concept = $this->duConceptRepository->getConceptById($conceptId);
        $ingresosConceptos = $this->duConceptRepository->getIngresosConceptos();
    
        return view('du.concepts.edit')->with(compact('concept', 'ingresosConceptos'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'conceptId' => 'required',
            'articulo' => 'required',
            'uso' => 'required',
            'concepto' => 'required',
        ]);

        $this->duConceptRepository->update($request);

        return redirect()->route('du.concepts');
    }
}
