<?php

namespace App\Http\Controllers\Moduls\ProteccionCivil;

use App\Http\Controllers\Controller;
use App\Repositories\ProteccionCivil\ExpedientRepository;
use Illuminate\Http\Request;

class ExpedientController extends Controller
{
    private $expedientRepository;

    public function __construct(
        ExpedientRepository $expedientRepository
    ){
        $this->expedientRepository = $expedientRepository;
    }

    public function index(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }

        $expedients = $this->expedientRepository->getExpedients($inputs);
        session(["inputs" => $inputs]);

        return view('proteccionCivil.expedients.index')->with(compact('expedients'));
    }

    public function create()
    {
        return view('proteccionCivil.expedients.create');
    }

    public function store(Request $request)
    {
        $expedient = $this->expedientRepository->store($request);

        return redirect()->route('pc.expedients.edit', $expedient->id)->with('alert', 'Expediente creado con éxito');
    }

    public function edit($expedietId)
    {
        $expedient = $this->expedientRepository->getExpedientById($expedietId);

        return view('proteccionCivil.expedients.edit')->with(compact('expedient'));
    }

    public function update(Request $request)
    {
        $expedient = $this->expedientRepository->update($request);

        return redirect()->route('pc.expedients.edit', $expedient->id)->with('alert', 'Expediente actualizado con éxito');
    }

    public function ajaxUpdate(Request $request)
    {
        $expedient = $this->expedientRepository->update($request);

        return response()->json([
            'data' => [
                'success' => true,
                'expedient' => $expedient
            ]
        ], 200);
    }
}
