<?php

namespace App\Http\Controllers\Moduls\ProteccionCivil;

use App\Http\Controllers\Controller;
use App\Repositories\ProteccionCivil\RequirementRepository;
use Illuminate\Http\Request;

class RequirementController extends Controller
{
    private $requirementRepository;

    public function __construct(
        RequirementRepository $requirementRepository
    ){
        $this->requirementRepository = $requirementRepository;
    }

    public function index()
    {
        $requirements = $this->requirementRepository->getRequirements();
        
        return view('proteccionCivil.requirements.index')->with(compact('requirements'));
    }

    public function create()
    {
        return view('proteccionCivil.requirements.create');
    }

    public function store(Request $request)
    {
        $this->requirementRepository->store($request);

        return redirect()->route('pc.requirements')->with('alert', 'Requisito creado con éxito');
    }

    public function edit($requirementId)
    {
        $requirement = $this->requirementRepository->getRequirementById($requirementId);

        return view('proteccionCivil.requirements.edit')->with(compact('requirement'));
    }

    public function update(Request $request)
    {
        $this->requirementRepository->update($request);

        return redirect()->route('pc.requirements')->with('alert', 'Requisito actualizado con éxito');
    }
}
