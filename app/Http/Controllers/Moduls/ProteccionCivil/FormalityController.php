<?php

namespace App\Http\Controllers\Moduls\ProteccionCivil;

use App\Http\Controllers\Controller;
use App\Repositories\PadronRepository;
use App\Repositories\ProteccionCivil\FormalityRepository;
use App\Repositories\ProteccionCivil\ServiceTypeRepository;
use App\Repositories\ProteccionCivil\UnitRepository;
use App\Repositories\ProteccionCivil\VisitTypeRepository;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Switch_;

class FormalityController extends Controller
{
    private $formalityRepository;

    public function __construct(
        FormalityRepository $formalityRepository
    ){
        $this->formalityRepository = $formalityRepository;
    }

    public function index()
    {
        $formalities = $this->formalityRepository->getFormalities();

        return view('proteccionCivil.formalities.index')->with(compact('formalities'));
    }

    public function create()
    {
        return view('proteccionCivil.formalities.create');
    }

    public function store(Request $request)
    {
        $this->formalityRepository->store($request);

        return redirect()->route('pc.formalities')->with('alert', 'Tramite creado con éxito');
    }

    public function edit($formalityId)
    {
        $formality = $this->formalityRepository->getFormalityById($formalityId);
       
        return view('proteccionCivil.formalities.edit')->with(compact('formality'));
    }

    public function update(Request $request)
    {
        $this->formalityRepository->update($request);

        return redirect()->route('pc.formalities')->with('alert', 'Tramite actualizado con éxito');
    }
}
