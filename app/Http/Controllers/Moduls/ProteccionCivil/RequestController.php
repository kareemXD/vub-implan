<?php

namespace App\Http\Controllers\Moduls\ProteccionCivil;

use App\Http\Controllers\Controller;
use App\Repositories\PadronRepository;
use App\Repositories\ProteccionCivil\ExpedientRepository;
use App\Repositories\ProteccionCivil\FormalityRepository;
use App\Repositories\ProteccionCivil\RequestRepository;
use App\Repositories\ProteccionCivil\RequirementRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class RequestController extends Controller
{
    private $padronRepository,
            $requestRepository,
            $formalityRepository,
            $requirementRepository,
            $expedientRepository;

    public function __construct(
        PadronRepository $padronRepository,
        RequestRepository $requestRepository,
        FormalityRepository $formalityRepository,
        RequirementRepository $requirementRepository,
        ExpedientRepository $expedientRepository
    ){
        $this->padronRepository = $padronRepository;
        $this->requestRepository = $requestRepository;
        $this->formalityRepository = $formalityRepository;
        $this->requirementRepository = $requirementRepository;
        $this->expedientRepository = $expedientRepository;
    }

    public function index(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }
        $requests = $this->requestRepository->getRequests($inputs);
        session(["inputs" => $inputs]);

        // dd($request->all());

        return view('proteccionCivil.requests.index')->with(compact('requests'));
    }

    public function create()
    {
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $formalities = $this->formalityRepository->getFormalities();

        return view('proteccionCivil.requests.create')->with(compact('poblaciones', 'formalities'));
    }

    public function store(Request $request)
    {
        $solicitud = $this->requestRepository->store($request);

        return redirect()->route('pc.requests.edit', $solicitud->id)->with('alert', 'Solicitud creada con éxito');
    }

    public function edit($requestId)
    {
        $request = $this->requestRepository->getRequestById($requestId);
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $formalities = $this->formalityRepository->getFormalities();
        $requirements = $this->requirementRepository->getRequirements();

        $isInspector = auth()->user()->hasRole(['Administrador', 'Inspector Proteccion Civil', 'Admin Proteccion Civil']);

        if ($request->estatus == 1)
        {
            return view('proteccionCivil.requests.edit')->with(compact('request', 'poblaciones', 'formalities', 'requirements'));
        }else if ($request->estatus >= 2 && $request->estatus <= 4 )
        {
            if ($isInspector)
            {
                return redirect()->route('pc.requests.dictamination', $request->id);
            }else
            {
                return redirect()->route('pc.requests.detail', $request->id);
            }
        }else if ($request->estatus >= 5)
        {
            return redirect()->route('pc.requests.detail', $request->id);
        }else
        {
            return redirect()->route('pc.requests');
        }
    }

    public function update(Request $data)
    {
        switch ($data->request_action) {
            case 'update':
                $request = $this->requestRepository->update($data);
                return redirect()->back()->with('alert', 'Información actualizada');
                break;
            case 'update_dictamination': 
                $request = $this->requestRepository->updateDictamination($data);
                return redirect()->back()->with('alert', 'Información actualizada');
                break;
            case 'apply':
                $request = $this->requestRepository->update($data);
                $request = $this->requestRepository->applyRequest($data);

                // Check redireccionamiento
                if ($data->redirect == 'padron_request')
                {
                    // Solicitud padron
                    $padronSolicitud = $request->padronSolicitud();
                    if (!empty($padronSolicitud))
                    {
                        return redirect()->route('update.request', $padronSolicitud->NumeroSolicitud)->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.')->with('formality_applied', true);
                    }

                    return redirect()->route('requests')->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.');
                }else if ($data->redirect == 'padron_license')
                {
                    // Licencia padron
                    $padronLicense = $request->padronLicencia();
                    if (!empty($padronLicense))
                    {
                        return redirect()->route('update.license', $padronLicense->NumeroLicencia)->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.')->with('show_formalities', true);
                    }

                    return redirect()->route('licenses')->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.');
                }else if ($data->redirect == 'du_update')
                {
                    // Solicitud update Desarrollo Urbano
                    $duSolicitud = $request->duSolicitud();
                    if (!empty($duSolicitud))
                    {
                        return redirect()->route('du.update.request', $duSolicitud->id)->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.')->with('show_formalities', true);
                    }

                    return redirect()->route('du.requests')->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.');
                }else if ($data->redirect == 'du_verify')
                {
                    $duSolicitud = $request->duSolicitud();
                    if (!empty($duSolicitud))
                    {
                        return redirect()->route('du.verify.request', $duSolicitud->id)->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.')->with('show_formalities', true);
                    }

                    return redirect()->route('du.requests')->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.');
                }else if ($data->redirect == 'du_inspect')
                {
                    $duSolicitud = $request->duSolicitud();
                    if (!empty($duSolicitud))
                    {
                        return redirect()->route('du.inspect.request', $duSolicitud->id)->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.')->with('show_formalities', true);
                    }

                    return redirect()->route('du.requests')->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.');
                }else if ($data->redirect == 'du_dictamination')
                {
                    $duSolicitud = $request->duSolicitud();
                    if (!empty($duSolicitud))
                    {
                        return redirect()->route('du.dictamination.request', $duSolicitud->id)->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.')->with('show_formalities', true);
                    }

                    return redirect()->route('du.requests')->with('alert', 'El tramite de protección civil se ha enviado para su dictaminación.');
                }else
                {
                    return redirect()->route('pc.requests.edit', $request->id)->with('alert', 'Solicitud '.$request->requestNumberText().' - APLICADO');
                }

                break;
            case 'cancel':
                $request = $this->requestRepository->update($data);
                $request = $this->requestRepository->cancelRequest($data);
                
                if ($data->redirect == 'padron_request')
                {
                    $padronSolicitud = $request->padronSolicitud();
                    if (!empty($padronSolicitud))
                    {
                        return redirect()->route('update.request', $padronSolicitud->NumeroSolicitud)->with('alert', 'El tramite de protección civil se ha cancelado.')->with('formality_applied', true);
                    }
                    
                    return redirect()->route('requests')->with('alert', 'El tramite de protección civil se ha cancelado.');
                }else if ($data->redirect == 'padron_license')
                {
                    $padronLicencia = $request->padronLicencia();
                    if (!empty($padronLicencia))
                    {
                        return redirect()->route('update.license', $padronLicencia->NumeroLicencia)->with('alert', 'El tramite de protección civil se ha cancelado.')->with('show_formalities', true);
                    }

                    return redirect()->route('licenses')->with('alert', 'El tramite de protección civil se ha cancelado.');
                }else if ($data->redirect == 'du_update')
                {
                    $duSolicitud = $request->duSolicitud();
                    if (!empty($duSolicitud))
                    {
                        return redirect()->route('du.update.request', $duSolicitud->id)->with('alert', 'El tramite de protección civil se ha cancelado.')->with('show_formalities', true);
                    }

                    return redirect()->route('du.requests')->with('alert', 'El tramite de protección civil se ha cancelado.');
                }else if ($data->redirect == 'du_verify')
                {
                    $duSolicitud = $request->duSolicitud();
                    if (!empty($duSolicitud))
                    {
                        return redirect()->route('du.verify.request', $duSolicitud->id)->with('alert', 'El tramite de protección civil se ha cancelado.')->with('show_formalities', true);
                    }

                    return redirect()->route('du.requests')->with('alert', 'El tramite de protección civil se ha cancelado.');
                }else if ($data->redirect == 'du_inspect')
                {
                    $duSolicitud = $request->duSolicitud();
                    if (!empty($duSolicitud))
                    {
                        return redirect()->route('du.inspect.request', $duSolicitud->id)->with('alert', 'El tramite de protección civil se ha cancelado.')->with('show_formalities', true);
                    }

                    return redirect()->route('du.requests')->with('alert', 'El tramite de protección civil se ha cancelado.');
                }else if ($data->redirect == 'du_dictamination')
                {
                    $duSolicitud = $request->duSolicitud();
                    if (!empty($duSolicitud))
                    {
                        return redirect()->route('du.dictamination.request', $duSolicitud->id)->with('alert', 'El tramite de protección civil se ha cancelado.')->with('show_formalities', true);
                    }

                    return redirect()->route('du.requests')->with('alert', 'El tramite de protección civil se ha cancelado.');
                }else
                {
                    return redirect()->route('pc.requests')->with('alert', 'Solicitud '.$request->requestNumberText().' - CANCELADO');
                }
                
                break;
            case 'reject':
                $request = $this->requestRepository->rejectRequest($data);
                return redirect()->route('pc.requests')->with('alert', 'Solicitud '.$request->requestNumberText().' - RECHAZADO');
                break;
            case 'verified':
                $request = $this->requestRepository->verifiedRequest($data);
                return redirect()->route('pc.requests.edit', $request->id)->with('alert', 'Solicitud '.$request->requestNumberText().' - VERIFICADO');
                break;

            case 'approve':
                $request = $this->requestRepository->approveRequest($data);
                return redirect()->route('pc.requests.edit', $request->id)->with('alert', 'Solicitud '.$request->requestNumberText().' - APROBADO');
                break;

            case 'set_high_risk_turn': 
                $request = $this->requestRepository->updateDictamination($data);
                $request = $this->requestRepository->setHighRiskTurn($data);
                return redirect()->back()->with('alert', 'Solicitud '.$request->requestNumberText().' - GIRO DE ALTO RIESGO');
                break;

            case 'set_ordinary_turn': 
                $request = $this->requestRepository->updateDictamination($data);
                $request = $this->requestRepository->setOrdinaryTurn($data);
                return redirect()->back()->with('alert', 'Solicitud '.$request->requestNumberText().' - GIRO ORDINARIO');
                break;
            
            default:
                # code...
                break;
        }

        return redirect()->route('pc.requests');
    }

    public function dictamination($requestId)
    {
        $request = $this->requestRepository->getRequestById($requestId);
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $formalities = $this->formalityRepository->getFormalities();
        $requirements = $this->requirementRepository->getRequirements();
        $inmuebleSectores = $this->requestRepository->getInmuebleSectores();
        $tipoEstablecimientos = $this->requestRepository->getTipoEstablecimientos();

        $isInspector = auth()->user()->hasRole(['Administrador', 'Inspector Proteccion Civil', 'Admin Proteccion Civil']);

        if ($request->estatus >= 2 && $request->estatus <= 4)
        {
            if ($isInspector)
            {
                return view('proteccionCivil.requests.dictamination')->with(compact('request', 'poblaciones', 'formalities', 'requirements', 'inmuebleSectores', 'tipoEstablecimientos'));
            }else
            {
                return redirect()->route('pc.requests.detail', $request->id);
            }
        }else if ($request->estatus == 1)
        {
            return redirect()->route('pc.requests.edit', $request->id);
        }else if ($request->estatus >= 5)
        {
            return redirect()->route('pc.requests.detail', $request->id);
        }else
        {
            return redirect()->route('pc.requests');
        }
    }

    public function detail($requestId)
    {
        $request = $this->requestRepository->getRequestById($requestId);
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $formalities = $this->formalityRepository->getFormalities();
        $requirements = $this->requirementRepository->getRequirements();
        $inmuebleSectores = $this->requestRepository->getInmuebleSectores();
        $tipoEstablecimientos = $this->requestRepository->getTipoEstablecimientos();

        return view('proteccionCivil.requests.detail')->with(compact('request', 'poblaciones', 'formalities', 'requirements', 'inmuebleSectores', 'tipoEstablecimientos'));
    }

    public function requirementUpload(Request $request)
    {
        $this->requestRepository->requirementUpload($request);

        return redirect()->back()->with(['alert' => 'Requisito subido con éxito', 'requirement_uploaded' => true]);
    }

    public function documentUpload(Request $request)
    {
        $this->requestRepository->documentUpload($request);

        return redirect()->back()->with(['alert' => 'Documento subido con éxito', 'document_uploaded' => true]);
    }

    public function vistoBueno(Request $request)
    {
        $solicitud = $this->requestRepository->getRequestById($request->requestId);
        
        if (!empty($solicitud))
        {
            $solicitud->umpc = $request->umpc;
            $solicitud->ldrr = $request->ldrr;
            $solicitud->save();

            $this->requestRepository->generateOfficeVistoBueno($request->requestId);
        }
    }

    public function cartaCompromiso(Request $request)
    {
        $solicitud = $this->requestRepository->getRequestById($request->requestId);
        
        if (!empty($solicitud))
        {
            $solicitud->cc = $request->cc;
            $solicitud->testigo_1 = $request->testigo_1;
            $solicitud->testigo_2 = $request->testigo_2;
            $solicitud->save();

            $this->requestRepository->generateOfficeCartaCompromiso($request->requestId);
        }
    }

    /*
        Reports
    */
    public function printRequest($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $request = $this->requestRepository->getRequestById($id);
        $formalities = '';

        foreach ($request->formalities as $key => $formality)
        {
            if ($key == 0)
            {
                $formalities .= strtoupper($formality->formality->tramite);
            }else
            {
                $formalities .= ', '.strtoupper($formality->formality->tramite);
            }
        }

        return view('partials.pdf.pc.request')->with(compact('request', 'formalities'));
    }

    /*
        Ajax
    */
    public function ajaxExpedientes(Request $request)
    {
        $expedients = $this->requestRepository->searchExpedientes($request->search);

        $arrayExpedients = [];
        foreach ($expedients as $expedient)
        {
            $dataExpedient = [
                'id' => $expedient->id,
                'text' => $expedient->no_expediente.' - '.$expedient->razon_social.' - '.$expedient->nombre_comercial
            ];

            $arrayExpedients[] = $dataExpedient;
        }

        return response()->json([
            'data' => [
                'success' => true,
                'expedients' => $arrayExpedients
            ]
        ], 200);
    }

    public function ajaxExpedienteFind(Request $request)
    {
        $expedient = $this->expedientRepository->getExpedientById($request->expedientId);

        return response()->json([
            'data' => [
                'success' => true,
                'expedient' => $expedient
            ]
        ], 200);
    }
}
