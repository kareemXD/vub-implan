<?php

namespace App\Http\Controllers\Moduls;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
//Repositories
use App\Repositories\PadronRepository;
use App\Repositories\BitacoraRepository;
use App\Repositories\ProteccionCivil\RequirementRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class PadronController extends Controller
{
    private $padronRepository,
            $bitacoraRepository,
            $proteccionCivilRequirementRepository;

    public function __construct(
        PadronRepository $padronRepository,
        BitacoraRepository $bitacoraRepository,
        RequirementRepository $proteccionCivilRequirementRepository


    ){
        $this->padronRepository = $padronRepository;
        $this->bitacoraRepository = $bitacoraRepository;
        $this->proteccionCivilRequirementRepository = $proteccionCivilRequirementRepository;
    }
    /*****************************************************************************
    * *
    * * Functions used for administration of Taxpayers
    * *
    *****************************************************************************/

    public function taxpayers(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }
        $taxpayers = $this->padronRepository->getAllTaxpayers($inputs);
        // dd($taxpayers);
        session(["inputs" => $inputs]);
        return view("padron.taxpayers", compact("taxpayers", "inputs"));
    }

    public function storeTaypayer(Request $request)
    {
        $validator = $this->validate($request, [
            "operation" => "required",
            "nombre" => "max:100|required",
            "apellido_pat" => "max:50",
            "apellido_mat" => "max:50",
            "rfc" => "max:15",
            "sexo" => "required",
            "persona" => "nullable",
            "email" => "max:50",
            "telefono_casa" => "max:15",
            "telefono_trab" => "max:15",
            "domicilio" => "max:60",
            "calle1" => "max:60",
            "calle2" => "max:60",
            "exterior" => "max:50",
            "interior" => "max:50",
            "cp" => "max:5",
            "localidad" => "max:40",
            "colonia" => "max:40",
        ],[
            "telefono_casa.max" => 'El campo no puede tener mas de 15 caracteres.',
            "telefono_trab.max" => 'El campo no puede tener mas de 15 caracteres.',
            "cp.max" => 'El campo no puede tener mas de 5 caracteres.',
            "rfc.required" => "EL campo es requerido",
            "nombre.required" => "EL campo es requerido",
            "cp.required" => "EL campo es requerido",
            "localidad.required" => "EL campo es requerido",
            "colonia.required" => "EL campo es requerido",
            "domicilio.required" => "EL campo es requerido",
        ]);
        $taxpayers = $this->padronRepository->storeTaypayer($request);
        return redirect()->back()->with("alert", "Se registro correctamente el contribuyente");
    }
    
    public function getTaxpayerData(Request $request)
    {
        return $this->padronRepository->getTaxpayerData($request->id);
    }
    
    public function storeUpdateTaypayer(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            "operation" => "required",
            "update_nombre" => "max:100|required",
            "update_apellido_pat" => "max:50",
            "update_apellido_mat" => "max:50",
            "update_rfc" => "max:15|required",
            "update_sexo" => "required",
            "update_persona" => "nullable",
            "update_email" => "max:50",
            "update_telefono_casa" => "max:15|nullable",
            "update_telefono_trab" => "max:15|nullable",
            "update_domicilio" => "max:60|nullable",
            "update_calle1" => "max:60|nullable",
            "update_calle2" => "max:60|nullable",
            "update_exterior" => "max:50|nullable",
            "update_interior" => "max:50|nullable",
            "update_cp" => "max:5|nullable",
            "update_localidad" => "max:40|nullable",
            "update_colonia" => "max:40|nullable",
        ],[
            "update_telefono_casa.max" => 'El campo no puede tener mas de 15 caracteres.',
            "update_telefono_trab.max" => 'El campo no puede tener mas de 15 caracteres.',
            "update_cp.max" => 'El campo no puede tener mas de 5 caracteres.',
            "update_rfc.required" => "EL campo es requerido",
            "update_nombre.required" => "EL campo es requerido",
            "update_cp.required" => "EL campo es requerido",
            "update_localidad.required" => "EL campo es requerido",
            "update_colonia.required" => "EL campo es requerido",
            "update_domicilio.required" => "EL campo es requerido",
        ]);
        $taxpayers = $this->padronRepository->storeUpdateTaypayer($request);
        return redirect()->back()->with("alert", "Se registro correctamente el contribuyente");
    }

    public function ajaxTaxpayers(Request $request)
    {
        $search = $request->search;
        $taxpayers = [];
        if (!empty($search))
        {
            $taxpayers = $this->padronRepository->getTaxpayersBySearch($search);
            foreach ($taxpayers as $taxpayer)
            {
                $taxpayer->text .= ' - #'.$taxpayer->id;
            }
        }

        return response()->json([
            'data' => [
                'success' => true,
                'taxpayers' => $taxpayers
            ]
        ]);
    }

    /*****************************************************************************
    * *
    * * Functions used for administration of turns
    * *
    *****************************************************************************/

    public function turns(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }
        $turns = $this->padronRepository->getAllTurns($inputs);
        session(["inputs" => $inputs]);
        // dd(session("inputs"));
        return view("padron.turns", compact("turns"));
    }
    
    public function getTurnData(Request $request)
    {
        return $this->padronRepository->getTurnById($request->id);
    }

    public function addTurn()
    {
        $documents = $this->padronRepository->getAllDocuments();
        $tiposGiro = $this->padronRepository->getAllTiposGiro();
        $subTiposGiro = $this->padronRepository->getAllSubTiposGiro();
        $riesgosGiros = $this->padronRepository->getAllRiesgosGiros();
        $clasesGiros = $this->padronRepository->getAllClasesGiros();
        $conceptos = $this->padronRepository->getAllConceptos();
        return view("padron.turn_add", compact("documents", "tiposGiro", "subTiposGiro", "riesgosGiros", "clasesGiros", "conceptos"));
    }
    
    public function storeTurn(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            "name" => "required",
            "cuota_fija" => "required",
            "cuota_minima" => "required",
            "tipo" => "required",
            "subtipo" => "required",
            "riesgo" => "required",
            "clase" => "required",
            "concepto" => "required",
            "claveHacienda" => "nullable"
        ]);
        $this->padronRepository->storeTurn($request);
        // dd($request);
        return redirect()->route("turns")->with("alert", "Se registro correctamente el Giro");
    }

    public function updateTurn($id)
    {
        $turn = $this->padronRepository->getTurnById($id);
        $documents = $this->padronRepository->getAllDocuments();
        $tiposGiro = $this->padronRepository->getAllTiposGiro();
        $subTiposGiro = $this->padronRepository->getAllSubTiposGiro();
        $riesgosGiros = $this->padronRepository->getAllRiesgosGiros();
        $clasesGiros = $this->padronRepository->getAllClasesGiros();
        $conceptos = $this->padronRepository->getAllConceptos();
        $turnConfig = $this->padronRepository->getTurnConfig($turn);

        // dd($turn);
        return view("padron.turn_update", compact("documents", "tiposGiro", "subTiposGiro", "riesgosGiros", "clasesGiros", "conceptos", "turn", "turnConfig"));
    }

    public function storeUpdateTurn(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "cuota_fija" => "required",
            "cuota_minima" => "required",
            "tipo" => "required",
            "subtipo" => "required",
            "riesgo" => "required",
            "clase" => "required",
            "concepto" => "required",
            "claveHacienda" => "nullable"
        ]);
        // dd($request);
        $this->padronRepository->storeUpdateTurn($request);
        return redirect()->route("turns")->with("alert", "Se actualizo correctamente el Giro");
    }

    public function updatePeriod(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "id" => "required",
            "cuota_fija" => "required",
            "cuota_minima" => "required",
            // "cuota_maxima" => "required"
        ]);
        if ($validator->fails()) {
            return "false";
        }
        // dd($request);
        $this->padronRepository->updatePeriod($request);
        return "true";
    }

    public function ajaxTurns(Request $request)
    {
        $search = $request->search;
        $turns = [];
        if (!empty($search))
        {
            $turns = $this->padronRepository->getTurnsBySearch($search);
            foreach ($turns as $turn)
            {
                $turn->text .= ' - #'.$turn->id;
            }

        }

        return response()->json([
            'data' => [
                'success' => true,
                'turns' => $turns
            ]
        ]);
    }

    /*****************************************************************************
    * *
    * * Functions used for administration of Requests
    * *
    *****************************************************************************/

    public function requests(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }
        $requests = $this->padronRepository->getAllRequests($inputs);
        session(["inputs" => $inputs]);
        return view("padron.requests", compact("requests"));
    }

    public function getLicenseCost(Request $request)
    {
        $cost = $this->padronRepository->getLicenseCost($request);
        return $cost;
    }
    
    public function getLicenseCostDetails(Request $request)
    {
        $cost = $this->padronRepository->getLicenseCostDetails($request);
        return $cost;
    }

    public function updateRequest($id, $operation = null)
    {
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $zonas = $this->padronRepository->getAllZonas();
        $request = $this->padronRepository->getRequestById($id);
        $trimester = $this->padronRepository->getActualTrimester();
        $requirements = $this->proteccionCivilRequirementRepository->getRequirements();
        $legends = $this->padronRepository->getAllLegends();

        if(!$operation){
            $operation = "store";
        }

        return view("padron.request_update", compact("request", 'poblaciones', 'zonas', 'trimester', 'operation', 'requirements', 'legends'));
    }

    public function storeUpdateRequest(Request $request)
    {
        // dd($request);
        if($request->operation == "update"){
            $licenses = $this->padronRepository->storeUpdateRequest($request);
            return redirect()->route("update.request", ["id" =>$licenses->NumeroSolicitud, "operation" => "update"])->with("alert", "Solicitud actualizada con Éxito");
        }elseif ($request->operation == "apply_changes") {
            if($license = $this->padronRepository->makeLicense($request))
            {
                $this->bitacoraRepository->insertActionSRV([
                    'NumeroLicencia' => $license->NumeroLicencia,
                    'DescripcionCorta' => 'Se genero el numero de licencia',
                    'DescripcionCompleta' => 'Se genero el numero de licencia',
                    'Usuario' => Auth::user()->name,
                    'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
                ]);
                return redirect()->route("update.license", $license->NumeroLicencia)->with("alert", "Licencia Creada con Éxito");
            }else {
                return redirect()->route("update.request", $request->id)->with("alert", "Por favor complete el tramite para generar la licencia.");
            }
        }
    }
    
    public function getDocumentByRequest(Request $request)
    {
        $data = $this->padronRepository->getDocumentByRequest($request->solicitud, $request->requisito);
        return $data;
    }
    
    public function showDocumentByRequest($requisito, $solicitud)
    {
        $data = $this->padronRepository->getDocumentByRequest($solicitud, $requisito);
        // dd($data);
        return Storage::disk("public")->download('documentacion_solicitudes/'.$solicitud.'/'.$data['pivot_Archivo'], $data['pivot_Archivo']);
    }
    
    public function updateDocumentRequest(Request $request)
    {
        if($request->operation == "store"){
            $data = $this->padronRepository->updateDocumentRequest($request);
            return redirect()->route("update.request", $data->NumeroSolicitud)->with("alert", "Solicitud actualizada con Éxito")->with("document_updated", true);
        }elseif ($request->operation == "update") {
            return $this->padronRepository->updateDocumentRequest($request);
        }
    }

    public function updatePcRequirementRequest(Request $data)
    {
        $request = $this->padronRepository->updatePcRequirementRequest($data);

        return redirect()->route('update.request', $request->NumeroSolicitud)->with('alert', 'Requisito de protección civil actualizado')->with('pc_requirement_updated', true);
    }

    public function requestPcFormalityStore(Request $data)
    {
        $this->padronRepository->requestPcFormalityStore($data);

        return redirect()->back()->with('alert', 'Se creo la solicitud para protección civil, por favor completa la información.')->with('pc_formality_created', true);
    }

    /*****************************************************************************
    * *
    * * Functions used for administration of Licenses
    * *
    *****************************************************************************/
    public function licenses(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }
        $licenses = $this->padronRepository->getAllLicenses($inputs);
        session(["inputs" => $inputs]);
        return view("padron.licenses", compact("licenses"));
    }
    
    public function addLicense()
    {
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $zonas = $this->padronRepository->getAllZonas();
        $trimester = $this->padronRepository->getActualTrimester();
        $legends = $this->padronRepository->getAllLegends();

        return view("padron.license_add", compact('poblaciones', 'zonas', 'trimester', 'legends'));
    }

    public function storeUpdateLicense(Request $request)
    {
        switch ($request->operation) {
            case 'update_location':
                $licenses = $this->padronRepository->storeUpdateLicenseLocation($request);
                // dd($licenses);
                return redirect()->route("update.license", $licenses->NumeroLicencia)->with("alert", "Ubicación actualizada con Éxito");
                break;
            case 'A': //ampliacion de giro
                    $licenses = $this->padronRepository->addTurnRequest($request);
                    return redirect()->route("update.license", $licenses->NumeroLicencia)->with("alert", "Giro agregado con Éxito");
                    break;
            case 'change_contributor': //cambiar datos del contribuyente sin crear nueva solicitud
                    $licenses =  $this->padronRepository->updateLicenseTaxpayer($request);
                    return redirect()->route("update.license", $licenses->NumeroLicencia)->with("alert", "Contribuyente actualizado correctamente.");
                    break;
            case 'update_turn':
                    $licenses =  $this->padronRepository->storeUpdateLicenseTurn($request);
                    return redirect()->route("update.license", $licenses->NumeroLicencia)->with("alert", "Actualizados los datos del giro.");
                    break;
            case 'G': //cambio de giro(completado)
                $licenses = $this->padronRepository->changeTurnRequest($request);
                return redirect()->route("update.license", $licenses->NumeroLicencia)->with("alert", "Giro Cambiado con éxito");
                break;
            case 'update_turn_data':
                    $licenses =  $this->padronRepository->storeUpdateLicenseTurn($request);
                    return redirect()->route("update.license", $licenses->NumeroLicencia)->with("alert", "Datos del negocio actualizados correctamente.");
                break;
            default:
                # code...
                break;
        }
    }
    
    public function getColoniesByCity(Request $request)
    {
        return $this->padronRepository->getColoniesByCity($request->id);
    }
    
    public function getCallesByColony(Request $request)
    {
        return $this->padronRepository->getCallesByColony($request->id);
    }

    public function storeLicense(Request $request)
    {
        // dd($request);
        $licenses = $this->padronRepository->storeLicense($request);
        // return redirect()->route("licenses")->with("alert", "Solicitud Creada con Éxito");
        return redirect()->route("update.request", $licenses->NumeroSolicitud)->with("alert", "Solicitud Creada con Éxito");
    }

    public function updateLicense($id)
    {
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $zonas = $this->padronRepository->getAllZonas();
        $license = $this->padronRepository->getLicenseById($id);
        $trimester = $this->padronRepository->getActualTrimester();
        $legends = $this->padronRepository->getAllLegends();
        $requirements = $this->proteccionCivilRequirementRepository->getRequirements();

        return view("padron.license_update", compact("license", 'poblaciones', 'zonas', 'trimester', 'legends', 'requirements'));
    }
    
    public function addRequestToLicense($license, $request)
    {
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $zonas = $this->padronRepository->getAllZonas();
        $license = $this->padronRepository->getLicenseById($license);
        $trimester = $this->padronRepository->getActualTrimester();
        $process = $this->padronRepository->getProcessById($request);
        $legends = $this->padronRepository->getAllLegends();
        
        if(is_null($license->requests())){
            return redirect()->back()->with("alert", "La licencia debe tener vinculada una solicitud.");
        }


        switch ($request) {
            case 'G':
                return view("padron.license_change_turn", compact("license", 'poblaciones', 'zonas', 'trimester', 'process', 'legends'));
                break;
            
            default:
                return view("padron.license_add_request", compact("license", 'poblaciones', 'zonas', 'trimester', 'process', 'legends'));
                break;
        }
        // dd($license, $request);
        return view("padron.license_add_request", compact("license", 'poblaciones', 'zonas', 'trimester', 'process', 'legends'));
    }

    public function loadTaxpayerLicences(Request $request)
    {
        $licenses = $this->padronRepository->getTaxpayerLicenses($request);

        return view('partials.content.licenses-by-taxpayer')->with(compact('licenses'));
    }

    public function updatePcRequirementLicense(Request $data)
    {
        $license = $this->padronRepository->updatePcRequirementLicense($data);

        return redirect()->route('update.license', $license->NumeroLicencia)->with('alert', 'Requisito de protección civil actualizado')->with('show_formalities', true);
    }

    public function licensePcFormalityStore(Request $data)
    {
        $this->padronRepository->licensePcFormalityStore($data);

        return redirect()->back()->with('alert', 'Se creo la solicitud para protección civil, por favor completa la información.')->with('show_formalities', true);
    }

    /*****************************************************************************
    * *
    * * Functions for Process
    * *
    *****************************************************************************/

    public function updateAnualTurns()
    {
        return view("padron.process.turns");
    }

    public function aplayAnualTurns(Request $request)
    {
        // dd($request);
        $this->padronRepository->aplayAnualTurns($request);
        return view("padron.process.turns")->with("alert", "Proceso Finalizado con Exito.");
    }
    
    /*****************************************************************************
    * *
    * * Functions for Finess
    * *
    *****************************************************************************/

    public function fines()
    {
        $fines = $this->padronRepository->getAllFines();
        return view("padron.process.turns");
    }
    
    /*****************************************************************************
    * *
    * * Functions for Alerts
    * *
    *****************************************************************************/

    public function alerts()
    {
        
        $alerts = $this->padronRepository->getAlertsWithLicense();
        return view("padron.alerts", compact('alerts'));
    }
    
    public function storeAlerts(Request $request)
    {
        if($request->operation == 'store')
        {
            $this->padronRepository->storeAlert($request);

            $this->bitacoraRepository->insertActionSRV([
                'NumeroLicencia' => $request->license,
                'DescripcionCorta' => 'Se Añadio una Alerta',
                'DescripcionCompleta' => 'Se Añadio una Alerta',
                'Usuario' => Auth::user()->name,
                'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
            ]);

            return redirect()->back()->with('alert', 'Alerta añadida con éxito');
        }elseif($request->operation == 'update')
        {
            $this->padronRepository->updateAlert($request);
            $this->bitacoraRepository->insertActionSRV([
                'NumeroLicencia' => $request->license,
                'DescripcionCorta' => 'Se actualizado una Alerta',
                'DescripcionCompleta' => 'Se actualizado una Alerta',
                'Usuario' => Auth::user()->name,
                'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
            ]);

            return redirect()->back()->with('alert', 'Alerta actualizada con éxito');
        }elseif($request->operation == 'delete')
        {
            $alert = $this->padronRepository->getAlert($request);

            if (!empty($alert))
            {
                $this->bitacoraRepository->insertActionSRV([
                    'NumeroLicencia' => $request->license,
                    'DescripcionCorta' => 'Se elimino una Alerta',
                    'DescripcionCompleta' => 'Se elimino una Alerta: '.$alert->Alerta,
                    'Usuario' => Auth::user()->name,
                    'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
                ]);

                $alert->delete();

                return redirect()->back()->with('alert', 'Alerta elimiada con éxito');
            }
            
            return redirect()->back()->with('alert', 'No se pudo eliminar la alerta');
        }

        return redirect()->back()->with('alert', 'Error con la alerta, vuelve a intentarlo.');
    }
    
    /*****************************************************************************
    * *
    * * Functions for Trash
    * *
    *****************************************************************************/

    public function trashPayment(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }
        $turns = $this->padronRepository->getAllTurns($inputs);
        session(["inputs" => $inputs]);

        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $conceptos = $this->padronRepository->getAllConceptos();

        return view("padron.process.trash.index", compact('turns', 'poblaciones', 'conceptos'));
    }

    public function trashPaymentEdit($turnId)
    {
        $conceptos = $this->padronRepository->getAllConceptos();
        $turn = $this->padronRepository->getTurnById($turnId);

        return view('padron.process.trash.edit')->with(compact('turn', 'conceptos'));
    }

    function updateTrash(Request $request){
        $this->padronRepository->updateTrash($request);
        if($request->type == 'turn'){
            return redirect()->back()->with("alert", "Giro actualizado con éxito");
        }
    }

    public function trashYearAdd(Request $request)
    {
        $this->padronRepository->trashYearAdd($request);

        return redirect()->back()->with('aler', 'Se creo correctamente el costo por año');
    }

    //dar de baja una licencia
    public function bajaLicencia($NumeroLicencia, Request $request)
    {
        $license = $this->padronRepository->getLicenseById($NumeroLicencia);

        if(is_null($license)) {
            return redirect()->back()->with("alert", "No se encontró la licencia");
        }
        
        $bajaLicencia = $this->padronRepository->storeBajaLicencia($license->NumeroLicencia, $request);

        return redirect()->route("licencia.baja.show", $bajaLicencia->NumeroLicencia)->with("alert", "Licencia dada de baja correctamente");
    }

    //mostrar detalles de la licencia dada de baja
    public function verBajaLicencia($id)
    {
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $zonas = $this->padronRepository->getAllZonas();
        $bajaLicencia = $this->padronRepository->getBajaLicenseById($id);
        $trimester = $this->padronRepository->getActualTrimester();
        $legends = $this->padronRepository->getAllLegends();

        // dd($license);
        return view("padron.license_baja_show", compact("bajaLicencia", 'poblaciones', 'zonas', 'trimester', 'legends'));
    }


    public function licenciasBajas(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }
        $bajasLicenses = $this->padronRepository->getCanceledLicenses($inputs);
        session(["inputs" => $inputs]);
        return view("padron.bajas-licenses", compact("bajasLicenses"));
    }

    public function addPeriod($idGiro, Request $request)
    {
        $giro = $this->padronRepository->getTurnById($idGiro);
        $validator = Validator::make($request->all(), [
            'periodo_cuota_fija' => 'required|numeric',
            'periodo_ano' => [
                'required',
                Rule::notIn(DB::connection("sqlsrv")->table("GiroCuotas")->where("IdGiro", $idGiro)->pluck("Ano")->toArray())
            ]
            ], [
                'periodo_cuota_fija.required' => 'La cuota fija es requerida',
                'periodo_cuota_fija.numeric' => 'La cuota fija debe ser un valor numérico',
                'periodo_ano.required' => 'Favor de colocar un año válido',
                'periodo_ano.not_in' =>  'El año ya se encuentra registrado'
            ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)
            ->withInput($request->all());
        }

        $turn = $this->padronRepository->addPeriodToTurn($idGiro, $request);

        return redirect()->back()->with("alert", "Giro actualizado correctamente");
    }

    //catalogo de localidades y calles
    public function getLocations(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }

        $locations = $this->padronRepository->getAllLocations($inputs);
        session(["inputs" => $inputs]);

        return view("padron.locations", compact("locations"));
    }

    public function editLocation($id, Request $request)
    {
        if($request->ajax()){
            $location = $this->padronRepository->getLocationById($id);

            return response()->json($location, 200);
        }

        return redirect()->back()->with("alert", "No cuenta con accesos");
    }

    public function updateLocation($id, Request $request){
        $validator = Validator::make($request->all(), [
            "actualizar_nombre_poblacion" => "required|min:2|max:100",
            "actualizar_nombre_corto" => "required|min:2|max:100",
        ]);
        if($validator->fails()){
            return response()->json("alert", "error de validacion");
        }

        $location = $this->padronRepository->storeUpdateLocation($id, $request);
        $url_colony = route('colony.list',$location->IdPoblacion);
        $url_street = route('street.list', $location->IdPoblacion);
        return response()->json(["alert" => "success","url_street" => $url_street, "url_colony" => $url_colony, "data" => $location], 200);
    }

    public function storeLocation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "crear_nombre_poblacion" => "required|min:2|max:100",
            "crear_nombre_corto" => "required|min:2|max:100",
        ]);
        if($validator->fails()){
            return response()->json("alert", "error de validacion");
        }
        $location = $this->padronRepository->storeLocation($request);
        $url_update = route('location.update', $location->IdPoblacion);
        $url_colony = route('colony.list', $location->IdPoblacion);
        $url_street = route('street.list', $location->IdPoblacion);
        return response()->json(["alert" => "success","url_update" => $url_update,"url_street" => $url_street , "url_colony" => $url_colony, "data" => $location], 200);

    }

    public function getColonies($idPoblacion = null, Request $request)
    {
        $locations = $this->padronRepository->getAllLocationsList();
        if(!is_null($idPoblacion)){
            $inputs = $request->all();
            if(array_key_exists("page", $inputs))
            {
                $inputs = session("inputs");
            }
    
            $colonies = $this->padronRepository->getAllColoniesByCity($idPoblacion,$inputs);
            session(["inputs" => $inputs]);
    
            return view("padron.colonies", compact("colonies", "idPoblacion", "locations"));

        }else{
            $inputs = $request->all();
            if(array_key_exists("page", $inputs))
            {
                $inputs = session("inputs");
            }
            $colonies = $this->padronRepository->getAllColonies($inputs);
            session(["inputs" => $inputs]);
    
            return view("padron.colonies", compact("colonies", "idPoblacion", "locations"));  
        }

    }

    /**agregar poblacion */
    public function addColony(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "crear_nombre_colonia" => "required|min:2|max:100",
            "crear_nombre_comun" => "required|min:2|max:100",
            "crear_id_poblacion" => "required",
        ]);
        if($validator->fails()){
            return response()->json("alert", "error de validacion");
        }
        $colony = $this->padronRepository->storeColony($request);
        $colony->location = $colony->location;
        $url_update = route('colony.update', $colony->IdColonia);
        $url_street = route('street.list', $colony->IdColonia);
        return response()->json(["alert" => "success","url_update" => $url_update, "url_street" => $url_street, "data" => $colony], 200);
    }

    public function editColony($idColonia, Request $request)
    {
        if($request->ajax()){
            $colony = $this->padronRepository->getColonyById($idColonia);

            return response()->json($colony, 200);
        }

        return redirect()->back()->with("alert", "No cuenta con accesos");
    }

    public function updateColony($id, Request $request){
        $validator = Validator::make($request->all(), [
            "actualizar_nombre_colonia" => "required|min:2|max:100",
            "actualizar_nombre_comun" => "required|min:2|max:100",
            "actualizar_id_poblacion" => "required",
        ]);
        if($validator->fails()){
            return response()->json("alert", "error de validación");
        }

        $colony = $this->padronRepository->storeUpdateColony($id, $request);
        $colony->location = $colony->location;
        $url_update = route('colony.update', $colony->IdColonia);
        $url_street = route('street.list', $colony->IdColonia);

        return response()->json(["alert" => "success", "url_update" =>$url_update, "url_street" => $url_street, "data" => $colony], 200);
    }

    public function getStreets($idPoblacion= null, Request $request)
    {
        $locations = $this->padronRepository->getAllLocationsList();
        if(!is_null($idPoblacion)){
            $inputs = $request->all();
            if(array_key_exists("page", $inputs))
            {
                $inputs = session("inputs");
            }
    
            $streets = $this->padronRepository->getAllStreetsByLocation($idPoblacion,$inputs);
            session(["inputs" => $inputs]);
    
            return view("padron.streets", compact("streets", "idPoblacion", "locations"));

        }else{
            $inputs = $request->all();
            if(array_key_exists("page", $inputs))
            {
                $inputs = session("inputs");
            }
    
            $streets = $this->padronRepository->getAllStreets($inputs);
            session(["inputs" => $inputs]);
    
            return view("padron.streets", compact("streets", "idPoblacion", "locations"));
        }
    }

    public function addStreet(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "crear_nombre_calle" => "required|min:2|max:100",
            "crear_alias_calle" => "required|min:2|max:100",
            "crear_nombre_oficial" => "required|min:2|max:100",
            "crear_id_poblacion" => "required",
        ]);
        if($validator->fails()){
            return response()->json("alert", "error de validacion");
        }
        $street = $this->padronRepository->storeStreet($request);
        $street->location = $street->location;
        $url_update = route('street.update', $street->IdCalle);
        return response()->json(["alert" => "success","url_update" => $url_update, "data" => $street], 200);
    }

    public function editStreet($idCalle, Request $request)
    {
        if($request->ajax()){
            $street = $this->padronRepository->getStreetById($idCalle);

            return response()->json($street, 200);
        }

        return redirect()->back()->with("alert", "No cuenta con accesos");
    }

    public function updateStreet($id, Request $request){
        $validator = Validator::make($request->all(), [
            "actualizar_nombre_calle" => "required|min:2|max:100",
            "actualizar_alias_calle" => "required|min:2|max:100",
            "actualizar_nombre_oficial" => "required|min:2|max:100",
            "actualizar_id_poblacion" => "required",
        ]);
        if($validator->fails()){
            return response()->json("alert", "error de validación");
        }

        $street = $this->padronRepository->storeUpdateStreet($id, $request);
        $street->location = $street->location;
        $url_update = route('street.update', $street->IdCalle);

        return response()->json(["alert" => "success", "url_update" =>$url_update, "data" => $street], 200);
    }

    public function getStreetById($id=null)
    {
        if(is_null($id)){
            return false;
        }
        return response()->json($this->padronRepository->getCalleById($id));
    }    

    //buscar toda la informacion de una licencia
    public function searchLicenseDetails(Request $request)
    {
        $license = $this->padronRepository->getLicenseById($request->input('search-license-details'));

        if(is_null($license)){
            return response()->json(false);
        }

        return view("partials.content.search-license-content", compact("license"));
    }
    //fin buscar detalles de una licencia

    public function statistics()
    {
        return view('padron.statistics');
    }
}
