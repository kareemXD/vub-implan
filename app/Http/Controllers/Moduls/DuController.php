<?php

namespace App\Http\Controllers\Moduls;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;

//Repositories
use App\Repositories\DuRepository;
use App\Repositories\PadronRepository;
use App\Repositories\ProteccionCivil\RequirementRepository;
use Illuminate\Support\Facades\Auth;

class DuController extends Controller
{
    private $duRepository,
            $padronRepository,
            $requirementRepository;

    function __construct(
        DuRepository $duRepository,
        PadronRepository $padronRepository,
        RequirementRepository $requirementRepository
    ){
        $this->duRepository = $duRepository;
        $this->padronRepository = $padronRepository;
        $this->requirementRepository = $requirementRepository;
    }

    function precedures(){
        $procedures = $this->duRepository->getAllProcedures();
        return view('du.procedures', compact('procedures'));
    }

    function addProcess(){
        $requirements = $this->duRepository->getAllRequirements();
        return view('du.process_add', compact('requirements'));
    }

    function storeProcess(Request $request){
        $this->validate($request,[
            'name' => 'required',
        ]);

        $this->duRepository->storeProcess($request);
        return redirect()->route('du.formalities')->with("alert", "Tramite Registrado con éxito.");
    }

    function updateProcess($id){
        $process = $this->duRepository->getProcessById($id);
        $requirements = $this->duRepository->getAllRequirements();
        return view('du.process_update', compact('process', 'requirements'));
    }

    function storeUpdateProcess(Request $request){
        $this->validate($request,[
            'name' => 'required',
        ]);
        $this->duRepository->storeUpdateProcess($request);
        return redirect()->back()->with("alert", "Tramite actualizado con éxito.");
    }

    function requests(Request $request)
    {
        $isDictaminator = Auth::user()->hasRole(config('system.du.roles.dictaminator'));
        $dictaminatorId = null;
        if ($isDictaminator)
        {
            $dictaminatorId = Auth::user()->id;
        }

        $isInspector = Auth::user()->hasRole(config('system.du.roles.inspector'));
        $inspectorId = null;
        if ($isInspector)
        {
            $inspectorId = Auth::user()->id;
        }

        $search = [
            'searching' => 0,
            'section' => '',
            'cuenta' => '',
            'contribuyente' => '',
            'dictaminator' => $dictaminatorId,
            'inspector' => $inspectorId,
        ];

        if (!empty($request->searching))
        {
            $search['searching'] = 1;
            $search['section'] = $request->section;
            $search['cuenta'] = $request->cuenta;
            $search['contribuyente'] = $request->contribuyente;
        }
        
        $requests = $this->duRepository->getDuRequestsBySearch($search);

        return view('du.requests', compact('requests', 'search'));
    }

    public function proceduresRequest(Request $request)
    {
        $request = $this->duRepository->getRequestById($request->requestId);

        return view('partials.modals.du.modal_request_procedures')->with(compact('request'));
    }
    
    function addRequests()
    {
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $procedures = $this->duRepository->getAllProcedures();

        return view('du.request_add', compact('poblaciones', 'procedures'));
    }
    
    function storeRequest(Request $request)
    {
        $data = $this->duRepository->storeRequest($request);

        return redirect()->route('du.update.request', $data->id);
    }
    
    function updateRequest($id)
    {
        $request = $this->duRepository->getRequestById($id);
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $procedures = $this->duRepository->getAvailableProceduresByRequest($request->id);
        $requirements = $this->requirementRepository->getRequirements();

        return view('du.request_update', compact('request', 'poblaciones', 'procedures', 'requirements'));
    }

    public function storeUpdateRequest(Request $request)
    {
        if($request->operation == "update")
        {
            $this->duRepository->updateRequest($request);
            $this->duRepository->changeRequestToCreated($request->solicitud);

            return redirect()->route("du.update.request", $request->solicitud)->with("alert", "Solicitud actualizada con Éxito");
        }else if ($request->operation == "apply_changes_window") 
        {
            $this->duRepository->changeRequestToVerify($request->solicitud);

            return redirect()->route("du.requests")->with("alert", "Expediente creado con Exito.");
        }else if ($request->operation == 'paid')
        {
            if ($this->duRepository->checkForPayment($request->solicitud))
            {
                $this->duRepository->changeRequestToPaid($request->solicitud);
                
                return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a solicitudes con trámite pagados.");
            }

            return redirect()->route("du.detail.request", $request->solicitud)->with("error", "El expediente aun no ha sido pagado.");
        }
        
        return redirect()->route("du.update.request", $request->solicitud)->with("alert", "Por favor complete el tramite para generar la licencia.");
    }

    function getDocumentByRequest(Request $request)
    {
        $data = $this->duRepository->getDocumentByRequest($request->solicitud, $request->requisito);
        return $data;
    }
    
    function updateDocumentRequest(Request $request)
    {
        $this->validate($request, [
            'document' => 'file|max:2000'
        ], [
            'document.max' => 'El tamaño del documento no debe exceder los 2 mb de peso.'
        ]);

        $data = $this->duRepository->updateDocumentRequest($request);
        return redirect()->route("du.update.request", $data->id)->with("alert", "Solicitud actualizada con Éxito")->with("document_updated", true);
    }

    public function updatePcRequirementRequest(Request $data)
    {
        $this->duRepository->updatePcRequirementRequest($data);

        return redirect()->back()->with('alert', 'Requisito de protección civil actualizado')->with('show_formalities', true);
    }

    public function requestPcFormalityStore(Request $data)
    {
        $this->duRepository->requestPcFormalityStore($data);

        return redirect()->back()->with('alert', 'Se creo la solicitud para protección civil, por favor completa la información.')->with('show_formalities', true);
    }
    
    function showDocumentByRequest($requisito, $solicitud){
        $request = $this->duRepository->getRequestById($solicitud);
        $data = $this->duRepository->getDocumentByRequest($solicitud, $requisito);
        return Storage::disk("public")->download('documentacion_solicitudes_du/'.$request->id.'/'.$data['pivot_archivo'], $data['pivot_archivo']);
    }

    function verifyRequest($requestId)
    {
        $request = $this->duRepository->getRequestById($requestId);
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $procedures = $this->duRepository->getAllProcedures();
        $users = $this->duRepository->getDuUsers();
        $dictaminators = $this->duRepository->getDictaminators();
        $inspectors = $this->duRepository->getInspectors();
        $requirements = $this->requirementRepository->getRequirements();

        return view('du.request_verify', compact('request', 'poblaciones', 'procedures', 'users', 'dictaminators', 'inspectors', 'requirements'));
    }

    function storeVerifyRequest(Request $request)
    {
        switch ($request->operation) 
        {
            case 'observation':
                $this->duRepository->storeRequestObservation($request);

                return redirect()->route("du.verify.request", $request->solicitud)->with("comment_added", true);
                break;

            case 'inspect':
                $this->duRepository->changeRequestToInspect($request->solicitud, $request->inspector);

                return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a Inspección satisfactoriamente.");
            break;
            
            case 'dictamination':
                $this->duRepository->changeRequestToDictamination($request->solicitud, $request->dictaminator);

                return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a Dictaminación satisfactoriamente.");
                break;

            case 'back_dictamination':
                $this->duRepository->changeRequestToBackDictamination($request->solicitud);

                return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a Dictaminación satisfactoriamente.");
                break;

            case 'sign':
                $this->duRepository->changeRequestToPayValidation($request->solicitud);

                return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a validación de pago.");
                break;

            case 'accept':
                $this->duRepository->changeRequestToAccept($request->solicitud);
                return redirect()->route("du.requests")->with("alert", "El expediente fue aceptado correctamente.");
                break;

            case 'reject':
                $this->duRepository->changeRequestToReject($request->solicitud);
                return redirect()->route("du.requests")->with("alert", "El expediente fue rechazado.");
                break;
        }
    }

    function inspectRequest($id)
    {
        $request = $this->duRepository->getRequestById($id);
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $procedures = $this->duRepository->getAllProcedures();
        // $users = $this->duRepository->getDuUsers();
        $requirements = $this->requirementRepository->getRequirements();

        return view('du.request_inspect', compact('request', 'poblaciones', 'procedures', 'requirements'));
    }
    
    function storeInspectRequest(Request $request)
    {
        switch ($request->operation) {
            case 'observation':
                $this->duRepository->storeRequestObservation($request);
                return redirect()->route("du.inspect.request", $request->solicitud)->with("comment_added", true);
                break;

            case 'verify':
                $this->duRepository->changeRequestToVerify($request->solicitud);
                return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a Verificación satisfactoriamente.");
            break;
        }
    }

    public function getAvailableInspectors(Request $request)
    {
        $request = $this->duRepository->getRequestById($request->requestId);
        $inspectors = $this->duRepository->getInspectors();

        return view('partials.modals.du.modal_request_asign_inspector')->with(compact('request', 'inspectors'));
    }

    public function setInspectorToRequest(Request $request)
    {
        $this->duRepository->changeRequestToInspect($request->requestId, $request->inspector);

        return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a Inspección satisfactoriamente.");
    }
    
    function dictaminationRequest($id)
    {
        $request = $this->duRepository->getRequestById($id);
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $procedures = $this->duRepository->getAllProcedures();
        $conceptos = $this->duRepository->getAllDebts();
        $users = $this->duRepository->getDuUsers();
        $requirements = $this->requirementRepository->getRequirements();

        return view('du.request_dictamination', compact('request', 'poblaciones', 'procedures', 'conceptos', 'users', 'requirements'));
    }
    
    function storeDictaminationRequest(Request $request)
    {
        switch ($request->operation) {
            case 'observation':
                $this->duRepository->storeRequestObservation($request);
                return redirect()->route("du.dictamination.request", $request->solicitud)->with("comment_added", true);
                break;

            case 'verify':
                $this->duRepository->changeRequestToConfirmation($request->solicitud);
                return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a Verificación satisfactoriamente.");
            break;

            case 'save-payment':
                $this->duRepository->storeRequestPayment($request);
                return redirect()->route("du.dictamination.request", $request->solicitud)->with("payment_added", true);
            break;

            case 'reject':
                $this->duRepository->changeRequestToReject($request->solicitud);
                return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a Rechazados satisfactoriamente.");
            break;

            case 'formats':
                $this->duRepository->changeRequestToFormats($request->solicitud);
                return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a solicitudes con Formatos satisfactoriamente.");
            break;
        }
    }

    public function getAvailableDictaminators(Request $request)
    {
        $request = $this->duRepository->getRequestById($request->requestId);
        $dictaminators = $this->duRepository->getDictaminators();

        return view('partials.modals.du.modal_request_asign_dictaminator')->with(compact('request', 'dictaminators'));
    }

    public function setDictaminatorToRequest(Request $request)
    {
        $this->duRepository->changeRequestToDictamination($request->requestId, $request->dictaminator);

        return redirect()->route("du.requests")->with("alert", "El expediente fue transferido a Dictaminación satisfactoriamente.");
    }

    public function detailRequest($id)
    {
        $request = $this->duRepository->getRequestById($id);
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $procedures = $this->duRepository->getAllProcedures();
        $conceptos = $this->duRepository->getAllDebts();
        //select distinct Concepto, Descripcion from Adeudos
        // dd($conceptos[0]);
        $users = $this->duRepository->getDuUsers();

        return view('du.request_detail', compact('request', 'poblaciones', 'procedures', 'conceptos', 'users'));
    }

    public function storeFinalDocument(Request $request)
    {
        $solicitud = $this->duRepository->storeFinalDocument($request);

        return redirect()->route("du.detail.request", $solicitud->id)->with("alert", "Documento final subido con éxito")->with("final_document_updated", true);
    }

    public function deleteFinalDocument(Request $request)
    {
        $solicitud = $this->duRepository->deleteFinalDocument($request);

        return redirect()->route("du.detail.request", $solicitud->id)->with("alert", "Documento final se elimino con éxito")->with("final_document_updated", true);
    }

    public function removePaymentConcept(Request $request)
    {
        $this->validate($request, [
            'paymentId' => 'required'
        ]);

        $this->duRepository->removePaymentConcept($request);

        return redirect()->back()->with("payment_removed", true);
    }
}
