<?php

namespace App\Http\Controllers\Moduls;

use App\Http\Controllers\Controller;
use App\Repositories\DuRepository;
use App\Repositories\PadronRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProteccionCivilController extends Controller
{
    private $padronRepository,
            $duRepository;

    public function __construct(
        PadronRepository $padronRepository,
        DuRepository $duRepository

    ){
        $this->padronRepository = $padronRepository;
        $this->duRepository = $duRepository;
    }

    public function requestsPadron(Request $request)
    {
        $inputs = $request->all();
        if(array_key_exists("page", $inputs))
        {
            $inputs = session("inputs");
        }

        $requests = $this->padronRepository->getAllRequests($inputs);
        session(["inputs" => $inputs]);

        return view('proteccionCivil.requestsPadron')->with(compact('requests'));
    }

    public function requestsDU(Request $request)
    {
        $isDictaminator = Auth::user()->hasRole(config('system.du.roles.dictaminator'));
        $dictaminatorId = null;
        if ($isDictaminator)
        {
            $dictaminatorId = Auth::user()->id;
        }

        $isInspector = Auth::user()->hasRole(config('system.du.roles.inspector'));
        $inspectorId = null;
        if ($isInspector)
        {
            $inspectorId = Auth::user()->id;
        }

        $search = [
            'searching' => 0,
            'section' => '',
            'cuenta' => '',
            'contribuyente' => '',
            'dictaminator' => $dictaminatorId,
            'inspector' => $inspectorId,
        ];

        if (!empty($request->searching))
        {
            $search['searching'] = 1;
            $search['section'] = $request->section;
            $search['cuenta'] = $request->cuenta;
            $search['contribuyente'] = $request->contribuyente;
        }
        
        $requests = $this->duRepository->getDuRequestsBySearch($search);

        return view('proteccionCivil.requestsDU', compact('requests', 'search'));
    }

    public function statistics()
    {
        return view('proteccionCivil.statistics');
    }
}
