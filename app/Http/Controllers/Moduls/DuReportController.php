<?php

namespace App\Http\Controllers\Moduls;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use PDF;
use DOMPDF;
use Auth;
//Repositories
use App\Repositories\DuRepository;
use Illuminate\Support\Facades\File;

set_time_limit(3600);

class DuReportController extends Controller
{
    public function __construct(
        DuRepository $duRepository
    ){
        $this->duRepository = $duRepository;
    }

    public function printWindowRequest($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $inputs = session("inputs");
        $duRequest = $this->duRepository->getRequestById($id);
        $procedures = '';

        foreach ($duRequest->procedures as $key => $process)
        {
            if ($key == 0)
            {
                $procedures .= strtoupper($process->nombre);
            }else
            {
                $procedures .= ', '.strtoupper($process->nombre);
            }
        }

        return view('partials.pdf.du.ventanilla')->with(compact('duRequest', 'procedures'));
    }

    public function printPaymentFormatRequest($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $request = $this->duRepository->getRequestById($id);

        File::copy(public_path('assets/images/hoja_membretada_morena.jpg'), storage_path('app/public/du/hoja_membretada_morena.jpg'));
        
        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('Desarollo Urbano');
        PDF::SetSubject('Desarollo Urbano');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/du/hoja_membretada_morena.jpg');
            $pdf->Image($imagen, 0,0, 216, 268, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha: ". $date_now->format("d / M / Y"), 0, 0, 'R');
            $pdf->SetFont('', 'B', 10);
            $pdf->SetY(45);
            $pdf->Cell(0, 0, "FORMATO DE PAGO", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(50);
            $pdf->Cell(0, 0, "DESARROLLO URBANO", 0, 0, 'C');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(200, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 57, PDF_MARGIN_RIGHT);
        PDF::AddPage('H', 'LETTER');
        $html = "";
        $html = view('partials.pdf.du.paymentFormat')->with(compact('request'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Formato de Pago");
        PDF::Output("Formato de Pago");
    }
}
