<?php

namespace App\Http\Controllers\Moduls;

use App\Exports\LicensingBudgetReportExport;
use App\Exports\PadronReportLicencesRefrendosExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use PDF;
use Auth;
use Maatwebsite\Excel\Facades\Excel;

//Repositories
use App\Repositories\PadronRepository;
use App\Repositories\AdministrationRepository;
use App\Repositories\BitacoraRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

set_time_limit(3600);
setlocale(LC_ALL, 'es_MX');

class PadronReportController extends Controller
{
    private $padronRepository,
            $administrationRepository;
    private $bitacoraRepository;

    public function __construct(
        PadronRepository $padronRepository,
        AdministrationRepository $administrationRepository,
        BitacoraRepository $bitacoraRepository
    ){
        $this->padronRepository = $padronRepository;
        $this->administrationRepository = $administrationRepository;
        $this->bitacoraRepository = $bitacoraRepository;
        Carbon::setLocale('es-MX');

    }

    public function printTaxpayers(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $inputs = session("inputs");
        $taxpayers = $this->padronRepository->getAllTaxpayers($inputs, true);
        // $chunk = $data->chunk(500);
        
        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor_morena.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Reporte de Contribuyentes", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". $date_now->format("d / M / Y"), 0, 0, 'R');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(215, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = "";
        // foreach ($chunk as $key => $taxpayers) {
        //     // dd($taxpayer);
        //     # code...
            $html .= view('partials.pdf.padron.taxpayers')->with(compact('taxpayers'))->render();
        // }
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Reporte de Contribuyentes ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Reporte de Contribuyentes ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function printLicenses()
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $inputs = session("inputs");
        $licenses = $this->padronRepository->getAllLicenses($inputs, true);
        // $chunk = $data->chunk(500);
        
        // prevenir que se borra la imagen de la licencia
        File::copy(public_path('assets/images/membrete_morena.jpg'), storage_path('app/public/padron/licenses/membrete_morena.jpg'));
        
        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/padron/licenses/membrete_morena.jpg');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(45);
            $pdf->Cell(0, 0, "Reporte de Licencias", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". $date_now->format("d / M / Y"), 0, 0, 'R');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(215, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 52, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = "";
        $html = view('partials.pdf.padron.licenses')->with(compact('licenses'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Reporte de Licensias ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Reporte de Licensias ".$date_now->format('d-M-Y').".pdf");
    }
    public function printTurns(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $inputs = session("inputs");
        $turns = $this->padronRepository->getAllTurns($inputs, true);
        // $chunk = $data->chunk(500);

        // prevenir que se borra la imagen de la licencia
        File::copy(public_path('assets/images/membrete_morena.jpg'), storage_path('app/public/padron/licenses/membrete_morena.jpg'));
        
        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/padron/licenses/membrete_morena.jpg');
            $pdf->Image($imagen, 0,0, 279, 210, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(38);
            $pdf->Cell(0, 0, "Reporte de Giros", 0, 0, 'C');
            
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha: ". $date_now->format("d / M / Y"), 0, 0, 'R');
            $pdf->SetY(43);
            $pdf->Cell(0, 0, "DIRECCIÓN DE PADRON Y LICENCIAS", 0, 0, 'C');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(200, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 52, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = "";
        $html = view('partials.pdf.padron.turns')->with(compact('turns'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Reporte de Giros ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Reporte de Giros ".$date_now->format('d-M-Y').".pdf");
    }

    public function reportMonthlyLicences()
    {
        $date_now = Carbon::now('America/Mexico_City');
        $data = $this->administrationRepository->getStaticticsForDashboard();
        // dd($data);
        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('Padron y Licencias');
        PDF::SetSubject('Padron y Licencias');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor_morena.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Reporte de Licencias por Mes", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". $date_now->format("d / M / Y"), 0, 0, 'R');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(215, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = "";
        $html = view('partials.pdf.padron.report_licences')->with(compact('data'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Reporte de Licencias por Mes ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Reporte de Licencias por Mes ".$date_now->format('d-M-Y').".pdf");
    }

    public function printLicense($license = null)
    {
        // Validar que si no tiene permisos para imprimir licencias, no puedan imprimirla.
        if (!auth()->user()->can('padron_licencias_print_license'))
        {
            return redirect()->route('licenses');
        }

        $data = $this->padronRepository->getLicenseById($license);
        $date_now = Carbon::now('America/Mexico_City');
        // $date_now = Carbon::parse($data->FechaAlta);

        // prevenir que se borra la imagen de la licencia
        // File::copy(public_path('assets/images/licencia_morena.png'), storage_path('app/public/padron/licenses/licencia_morena.png'));

        // dd($data);
        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('Padron y Licencias');
        PDF::SetSubject('Padron y Licencias');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 10);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            // $imagen = storage_path('app/public/padron/licenses/licencia_morena.png');
            // $pdf->Image($imagen, 0, 0, 218, 279, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status 
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
        });
       
        $pixeles = 17; //tiene que ser 15
        PDF::SetMargins(8, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('H', 'LETTER');
        PDF::SetFont('', 'B', 20);
        PDF::SetTextColor(255,0,0);
        PDF::SetY(172-$pixeles);
        PDF::Cell(203, 0, $data->NumeroLicencia, 0, 0, 'R');
        PDF::SetY(120 - $pixeles);
        PDF::SetFont('', 'B', 14);
        PDF::SetTextColor(0,0,0);
        if($data->FechaAlta >= date("Y")){
            PDF::Cell(0, 0, "", 0, 0, 'R');
        }else{
            PDF::Cell(203, 0, "REFRENDO", 0, 0, 'R');
        }

        if (!empty($data->NumeroTarjetonAlcoholes))
        {
            PDF::SetY(95 - $pixeles);
            PDF::SetFont('', '', 12);
            PDF::Cell(203, 0, "Tarjetón Alcoholes:", 0, 0, 'R');
            PDF::SetY(100 - $pixeles);
            PDF::SetFont('', 'B', 12);
            PDF::Cell(203, 0, $data->NumeroTarjetonAlcoholes, 0, 0, 'R');
        }

        PDF::SetFont('', '', 12);
        PDF::SetTextColor(0,0,0);
        PDF::SetMargins(16, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetY(106-$pixeles);
        PDF::Cell(0, 0, $data->taxpayer->Nombre." ".$data->taxpayer->ApellidoPaterno." ".$data->taxpayer->ApellidoMaterno, 0, 0, 'L');
        PDF::SetY(132 - $pixeles);
        PDF::Cell(0, 0, $data->NombreNegocio, 0, 0, 'L');
        

        PDF::SetFont('', '', 12);
        PDF::SetTextColor(0,0,0);
        PDF::SetY(152 - $pixeles);
        $marginaddress = 152 - $pixeles;
        if(!is_null($data->street())){
            PDF::cell(0,0, $data->street()->NombreCalle . ' #' . $data->Exterior." ".$data->Interior, 0, 0, 'L');
            $marginaddress += 5;
        }
        PDF::SetY($marginaddress);
        if(!is_null($data->colony())){
            PDF::Cell(0, 0, $data->colony()->NombreColonia . ' / ' .$data->city()->NombrePoblacion, 0, 0, 'L');

        }else{
            PDF::Cell(0, 0, $data->city()->NombrePoblacion, 0, 0, 'L');
        }
        
        $giros = '';
        
        PDF::SetY(173- $pixeles);
        $girosArrayNames = [];
        $girosData = [];
        foreach ($data->turns as $key => $turn) {
            if(!in_array($turn->Nombre, $girosArrayNames)){
                $girosData[] = $turn;
                $girosArrayNames[] = $turn->Nombre;
            }
        }
        $margin= 173-$pixeles;
        $margis = [];
        if(count($girosData) > 3){
            PDF::SetFont('', '', 11);
            $margin= 160-$pixeles;
            PDF::SetMargins(38, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            foreach ($girosData as $key => $turn) {
                PDF::SetY($margin);
                PDF::Cell(0, 0, $turn->Nombre, 0, 0, 'L');
                $margin+=5;
            }
        }else{
            foreach ($girosData as $key => $turn) {
                PDF::SetY($margin);
                PDF::Cell(0, 0, $turn->Nombre, 0, 0, 'L');
                $margin+=5;

            }
        }
        PDF::SetFont('', '', 12);
        
        PDF::SetMargins(38, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // PDF::SetY(190- $pixeles);
        //PDF::Cell(20, 0, $data->Observaciones, 0, 0, 'L'); se quitan las observaciones en la licencia solo debe ir en la orden de pago
        $legends = '';
        foreach ($data->legends as $key => $legend) {
            $legends .= $legend->NombreLeyenda.", ";
        }
        $legends = substr($legends, 0, -2);
        PDF::SetMargins(33, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetY(203 - $pixeles);
        PDF::Cell(0, 0, $legends, 0, 50, 'L');
        PDF::SetFont('', '', 12);
        PDF::SetMargins(30, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetY(218- $pixeles);
        Carbon::setLocale('es_MX');
        
        $fecha = Carbon::now("America/Mexico_city")->formatLocalized(" %d de %B de %Y") ;
        // $fecha = Carbon::parse($data->FechaAlta)->formatLocalized(" %d de %B de %Y") ;
        $fecha = utf8_encode($fecha);
        PDF::Cell(0, 0, "Valle de Banderas, Nay. a ".$fecha, 0, 50, 'C');

        PDF::SetTitle("Licencia ".$data->NumeroLicencia.' '.$date_now->format('d-M-Y').".pdf");
        PDF::Output("Licencia ".$data->NumeroLicencia.' '.$date_now->format('d-M-Y').".pdf");

        $this->bitacoraRepository->insertActionSRV([
            'NumeroLicencia' => $data->NumeroLicencia,
            'DescripcionCorta' => 'Se generó el formato de licencia',
            'DescripcionCompleta' => 'Se generó el formato de licencia',
            'Usuario' => Auth::user()->name,
            'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
        ]);
    }

    public function printCostLicense($id = null)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $license = $this->padronRepository->getLicenseById($id);
        // $date_now = Carbon::parse($license->FechaAlta);
        // dd($data);
        
        // prevenir que se borra la imagen de la licencia
        File::copy(public_path('assets/images/hoja_membretada_morena.jpg'), storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg'));

        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('Padron y Licencias');
        PDF::SetSubject('Padron y Licencias');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg');
            
            $pdf->Image($imagen, 0,0, 216, 268, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(45);
            $pdf->Cell(0, 0, "Orden de Pago", 0, 0, 'C');
            
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha: ". $date_now->format("d / M / Y"), 0, 0, 'R');
            $pdf->SetY(50);
            $pdf->Cell(0, 0, "DIRECCIÓN DE PADRON Y LICENCIAS", 0, 0, 'C');
            // $logo = public_path('assets/images/logo-bahia.jpg');
            // $pdf->Image($logo, 15,8, 0, 0, 'JPG', '', '', true, 300, '', false, false, 0);
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(200, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 57, PDF_MARGIN_RIGHT);
        PDF::AddPage('H', 'LETTER');
        $html = "";
        $html = view('partials.pdf.padron.payment_order')->with(compact('license'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        $this->bitacoraRepository->insertActionSRV([
            'NumeroLicencia' => $license->NumeroLicencia,
            'DescripcionCorta' => 'Se generó la orden de pago',
            'DescripcionCompleta' => 'Se generó la orden de pago',
            'Usuario' => Auth::user()->name,
            'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
        ]);

        PDF::SetTitle("Orden de pago ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Orden de pago ".$date_now->format('d-M-Y').".pdf");
    }

    function reportLicences(){
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $turns = $this->padronRepository->getAllTurnsReport();
        return view("padron.reports.licenses", compact('poblaciones', 'turns'));
    }
    
    function printReportLicences(Request $request){
        $date_now = Carbon::now('America/Mexico_City');
        $licenses = $this->padronRepository->getLicenseForReport($request);
        // dd($licenses);
        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('Padron y Licencias');
        PDF::SetSubject('Padron y Licencias');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/Hoja_Membretada.png');
            $pdf->Image($imagen, 0,0, 216, 279, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 14);
            $pdf->SetY(14);
            $pdf->Cell(0, 0, "DIRECCIÓN DE PADRON Y LICENCIAS", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha: ". $date_now->format("d / M / Y"), 0, 0, 'R');
            $pdf->SetY(20);
            $pdf->Cell(0, 0, "Orden de Pago", 0, 0, 'C');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(200, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('H', 'LETTER');
        $html = "";
        $html = view('partials.pdf.padron.report_licenses')->with(compact('licenses'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Reporte de Licencias por Mes ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Reporte de Licencias por Mes ".$date_now->format('d-M-Y').".pdf");
    }

    function reportLicencesRefrendos()
    {
        ini_set('max_execution_time', 900);
        
        $firstDate = $this->padronRepository->getFirstLicenseDate();

        return view("padron.reports.licenses_refrendos")->with(compact('firstDate'));
    }
    
    function detailReportLicencesRefrendos(Request $request)
    {
        ini_set('max_execution_time', 900);

        $date_now = Carbon::now('America/Mexico_City');
        $licenses = $this->padronRepository->getLicenseRefrendosForReport($request);
        $dateStart = $request->start;
        $dateEnd = $request->end;

        if ($request->type == 'export')
        {
            // return Excel::download(new PadronReportLicencesRefrendosExport($licenses), 'Reporte Licencias y Refrendos.xlsx');
            return view('partials.pdf.padron.report_licenses_refrendos')->with(compact('licenses', 'dateStart', 'dateEnd'));
        }else
        {
            return view('partials.pdf.padron.report_licenses_refrendos')->with(compact('licenses', 'dateStart', 'dateEnd'));
        }
    }

    public function printAlerts()
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $inputs = session("inputs");
        $alerts = $this->padronRepository->getAlertsWithLicense($inputs, true);
        // $chunk = $data->chunk(500);

        // prevenir que se borra la imagen de la licencia
        File::copy(public_path('assets/images/membrete_morena.jpg'), storage_path('app/public/padron/licenses/membrete_morena.jpg'));
        
        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/padron/licenses/membrete_morena.jpg');
            $pdf->Image($imagen, 0,0, 279, 210, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(38);
            $pdf->Cell(0, 0, "Reporte de Alertas", 0, 0, 'C');
            
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha: ". $date_now->format("d / M / Y"), 0, 0, 'R');
            $pdf->SetY(43);
            $pdf->Cell(0, 0, "DIRECCIÓN DE PADRON Y LICENCIAS", 0, 0, 'C');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(215, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 52, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = "";
        $html = view('partials.pdf.padron.alerts')->with(compact('alerts'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Reporte de Alertas ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Reporte de Alertas ".$date_now->format('d-M-Y').".pdf");
    }

    public function printBaja($id)
    {
        setlocale(LC_TIME, 'es_MX.utf8');
        Carbon::setLocale('es_MX.utf8');
        $date_now = Carbon::now('America/Mexico_City');
        $license = $this->padronRepository->getBajaLicenseById($id);
        $request = $license->requests()->latest("NumeroSolicitud")->first();
        // dd($data);
        
        // prevenir que se borra la imagen de la licencia
        File::copy(public_path('assets/images/hoja_membretada_morena.jpg'), storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg'));

        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('Padron y Licencias');
        PDF::SetSubject('Padron y Licencias');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg');
            $pdf->Image($imagen, 0,0, 216, 268, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(45);
            $pdf->Cell(0, 0, "Baja de Licencia", 0, 0, 'C');
            
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha: ".$date_now->day.' / '.config('system.months.'.$date_now->format('n')).' / '.$date_now->year, 0, 0, 'R');
            $pdf->SetY(50);
            $pdf->Cell(0, 0, "DIRECCIÓN DE PADRON Y LICENCIAS", 0, 0, 'C');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(200, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 57, PDF_MARGIN_RIGHT);
        PDF::AddPage('H', 'LETTER');
        $html = "";
        $html = view('partials.pdf.padron.baja-request')->with(compact('license', 'request'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        $this->bitacoraRepository->insertActionSRV([
            'NumeroLicencia' => $license->NumeroLicencia,
            'DescripcionCorta' => 'Se generó la Baja de Licencia',
            'DescripcionCompleta' => 'Se generó la Baja de Licencia',
            'Usuario' => Auth::user()->name,
            'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
        ]);

        PDF::SetTitle("Baja de Licencia ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Baja de Licencia".$date_now->format('d-M-Y').".pdf");
    }

    /**imprimir estado de cuenta */
    public function printAccountLicense($id =null)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $license = $this->padronRepository->getLicenseById($id);
        // dd($data);
        
        // prevenir que se borra la imagen de la licencia
        File::copy(public_path('assets/images/hoja_membretada_morena.jpg'), storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg'));

        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('Padron y Licencias');
        PDF::SetSubject('Padron y Licencias');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg');
            $pdf->Image($imagen, 0,0, 216, 268, '', '', '', false, 300, '', false, false, 0);
            //logo
            // $logo = public_path('assets/images/logo-bahia.jpg');
            // $pdf->Image($logo, 15,8, 0, 0, 'JPG', '', '', true, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(45);
            $pdf->Cell(0, 0, "Estado de Cuenta", 0, 0, 'C');
            
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha: ". $date_now->format("d / M / Y"), 0, 0, 'R');
            $pdf->SetY(50);
            $pdf->Cell(0, 0, "DIRECCIÓN DE PADRON Y LICENCIAS", 0, 0, 'C');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(200, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 57, PDF_MARGIN_RIGHT);
        PDF::AddPage('H', 'LETTER');
        $html = "";
        $html = view('partials.pdf.padron.estado-cuenta')->with(compact('license'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        $this->bitacoraRepository->insertActionSRV([
            'NumeroLicencia' => $license->NumeroLicencia,
            'DescripcionCorta' => 'Se consultó el estado de cuenta',
            'DescripcionCompleta' => 'Se consultó el estado de cuenta',
            'Usuario' => Auth::user()->name,
            'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
        ]);

        PDF::SetTitle("Estado de Cuenta");
        PDF::Output("Estado de Cuenta");
    }

    public function reporteLicenciasConAdeudo()
    {
        $poblaciones = $this->padronRepository->getAllPoblaciones();
        $total = $this->padronRepository->getTotalDebts();

        return view('padron.reports.licenses_debts')->with(compact('poblaciones', 'total'));
    }

    public function reporteLicenciasConAdeudoDetalle(Request $request)
    {
        $licenses = $this->padronRepository->licensesWithDebts($request);
        $date_now = now();

        // prevenir que se borra la imagen de la licencia
        File::copy(public_path('assets/images/membrete_morena.jpg'), storage_path('app/public/padron/licenses/membrete_morena.jpg'));
        
        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now, $licenses){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/padron/licenses/membrete_morena.jpg');
            $pdf->Image($imagen, 0,0, 279, 210, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(38);
            $pdf->Cell(0, 0, "Reporte de Licencias con Adeudo", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha: ". $date_now->format("d / M / Y"), 0, 0, 'R');
            $pdf->SetY(43);
            $pdf->Cell(0, 0, "DIRECCIÓN DE PADRON Y LICENCIAS", 0, 0, 'C');
            $pdf->SetFont('', '', 10);
            $pdf->SetY(50);
            $pdf->Cell(0, 0, "Población: ".$licenses['poblacion'].' | Licencias: '.number_format($licenses['totalLicencias'], 0).' | Deuda total: $'.number_format($licenses['total'], 2), 0, 0, 'C');
        });
       
        PDF::setFooterCallback(function($pdf){
            $pdf->SetY(-8);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(200, 0, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 55, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = "";
        $html = view('partials.pdf.padron.report_licenses_debts')->with(compact('licenses'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Reporte de Licencias con Adeudos (".$licenses['poblacion'].") - ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Reporte de Licencias con Adeudos (".$licenses['poblacion'].") - ".$date_now->format('d-M-Y').".pdf");
    }

    public function licensingBudgetReport()
    {
        return view('padron.reports.licensing_budget');
    }

    public function licensingBudgetReportDetail(Request $request)
    {
        if ($request->type == 'licenses')
        {
            // Licenses
            $taxpayer = null;
            $licenseIds = explode(',', $request->licenses);
            $licenses = [];
            foreach ($licenseIds as $licenseId)
            {
                $licenseId = trim($licenseId);
                $license = $this->padronRepository->getLicenseById($licenseId);

                if (!empty($license))
                {
                    $licenses[] = $license;
                }
            }
        }else if ($request->type == 'taxpayer')
        {
            // Taxpayer
            $taxpayer = $this->padronRepository->getTaxpayerData($request->idtaxpayer);
            $licenses = $taxpayer->licenses;
        }

        $totals = [
            'import' => 0,
            'uan' => 0,
            'trash' => 0,
            'total' => 0
        ];
        $rows = [];
        foreach ($licenses as $license)
        {
            $turns = $license->turns;

            foreach ($turns as $turn)
            {
                $row = [
                    'turn' => $turn->Nombre,
                    'license' => $license->NumeroLicencia,
                    'import' => 0,
                    'uan' => 0,
                    'trash' => 0,
                    'total' => 0,
                    'taxpayer' => $license->taxpayer->NombreCompleto,
                    'address' => $license->getAddress(),
                ];

                $debts = $license->debts();
                $import = 0;
                $uan = 0;
                $trash = 0;
                $total = 0;
                foreach ($debts as $debt)
                {
                    $import += (float)$debt->Importe;
                    $reference = substr($debt->Referencia, 0, 4);

                    if((strpos(strtoupper($debt->Descripcion), "BASURA") !== false) || strpos(strtoupper($debt->Descripcion), "RECOLECCION Y TRASLADO") !== false)
                    {
                        if ((int)$reference >= 2022)
                        {
                            $trash += (float)$debt->Importe * 0.15;
                        }else
                        {
                            $trash += (float)$debt->Importe * 0.12;
                        }
                    }else
                    {
                        $turn = DB::connection("sqlsrv")->table("Giros")->where("Nombre", $debt->Descripcion)->first();
                        $concept = DB::connection('sqlsrv')->table('Conceptos')->where('Concepto', $debt->Concepto)->first();

                        $addUAN = false;
                        $wordsToSearch = ['ANUNC', 'PENDON', 'DIFUSION', 'PERIFONEO', 'VOLANTEO'];
                        if (!empty($concept))
                        {
                            foreach ($wordsToSearch as $word)
                            {
                                if ((strpos(strtoupper($concept->Descripcion), $word) !== false))
                                {
                                    $addUAN = true;
                                }
                            }
                        }

                        if($debt->Referencia2 == "Alcohol" || @$turn->Alcohol==true || $addUAN === true)
                        {
                            // Cobrar UAN a todo los giros que tengan alcoholes
                            if ((int)$reference >= 2022)
                            {
                                $uan = $debt->Importe * 0.15;
                            }else
                            {
                                $uan = $debt->Importe * 0.12;
                            }
                        }
                    }
                }

                $total = $import + $uan + $trash;

                $row['import'] = $import;
                $row['uan'] = $uan;
                $row['trash'] = $trash;
                $row['total'] = $total;

                $totals['import'] += $import;
                $totals['uan'] += $uan;
                $totals['trash'] += $trash;
                $totals['total'] += $total;

                $rows[] = $row;
            }
        }

        return Excel::download(new LicensingBudgetReportExport($rows, $totals), 'PRESUPUESTO-DE-LICENCIAS.xlsx');
    }
}