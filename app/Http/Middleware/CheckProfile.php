<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check())
        {
            $user = Auth::user();
            $menus = $user->getMenu();
            $route = $request->route()->getAction()["as"];
            $bandera = 0;
            foreach ($menus as $key => $menu) {
                if($route == $menu["url"])
                {
                    $bandera = 1;
                }
                if(!empty($menu["submenu"]))
                {
                    foreach ($menu["submenu"] as $key => $submenu) {
                        if($route == $submenu["url"])
                        {
                            $bandera = 1;
                        }
                    }
                }
            }
            if ($bandera == 1) {
                return $next($request);
            }
            else {
                return redirect()->back()->with("alert", "Seccion Invalida.");
            }
        }else {
            return redirect()->back()->with("alert", "Por favor inicie sessión.");
        }
    }
}
