<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class RiesgoGiro extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Riesgo";
    protected $primaryKey = 'Riesgo';
    public $timestamps = false;
    public $incrementing = false;
}
