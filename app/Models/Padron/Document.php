<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Requisitos";
    protected $primaryKey = 'Requisito';
    public $timestamps = false;
}
