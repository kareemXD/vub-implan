<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Pagos";
    // protected $primaryKey = "NumeroSolicitud";
    public $timestamps = false;
    public $incrementing = false;

    public function license()
    {
        return $this->belongsTo('App\Models\Padron\License', 'CuentaDepto', 'NumeroLicencia');
    }
}
