<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class LicensePcRequirement extends Model
{
    protected $table = 'licencia_pc_requisitos';
    protected $connection = 'pgsql';
    protected $fillable = ['id_licencia', 'id_requisito', 'documento'];
}
