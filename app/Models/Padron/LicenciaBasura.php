<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class LicenciaBasura extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Licencia_Basura";
    public $timestamps = false;
    public $incrementing = false;

    public function licencia()
    {
        return $this->belongsTo('App\Models\Padron\Licencia', 'NumeroLicencia', 'LicenciaId');
    }
}
