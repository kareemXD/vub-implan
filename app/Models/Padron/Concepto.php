<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Conceptos";
    protected $primaryKey = 'Concepto';
    public $timestamps = false;
    public $incrementing = false;
}
