<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class HistoricoGiro extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "GiroCuotas";
    protected $primaryKey = ['IdGiro', 'Ano'];
    public $timestamps = false;
    public $incrementing = false;
}
