<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;
//Repositories
use App\Repositories\PadronRepository;

class BajasLicencia extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "BajasLicencias";
    protected $primaryKey = "NumeroLicencia";
    public $timestamps = false;
    public $incrementing = false;
    
    public function taxpayer()
    {
        return $this->belongsTo('App\Models\Padron\Taxpayer', 'IdContribuyente', 'IdContribuyente');
    }

    public function turns()
    {
        return $this->belongsToMany('App\Models\Padron\Turn', 'GiroDetalleLic', 'NumeroLicencia', 'IdGiro')->withPivot('Secuencia', 'Fecha', 'Descripcion', 'Observacion', 'Cuota', 'Cantidad', 'LicenciaAnexo');
    }

    public function legends()
    {
        return $this->belongsToMany('App\Models\Padron\Legend', 'LeyendasLicencias', 'NumeroLicencia', 'IdLeyenda')->withPivot('FolioLicencia');
    }

    public function getAddress(){
        return $this->city()->NombrePoblacion . ', ' .$this->colony()->NombreColonia. ', ' .$this->street()->NombreCalle . ' #' . $this->Exterior;
    }

    public function city()
    {
        return DB::connection("sqlsrv")->table("Poblaciones")->where("IdPoblacion", $this->IdPoblacion)->first();
    }

    public function colony()
    {
        return DB::connection("sqlsrv")->table("Colonias")->where("IdColonia", $this->IdColonia)->first();
    }

    public function street()
    {
        return DB::connection("sqlsrv")->table("Calles")->where("IdCalle", $this->IdCalle)->first();
    }
    
    public function alerts()
    {
        return DB::connection("sqlsrv")->table("Alertas")->where("NumeroLicencia", $this->NumeroLicencia)->get();
    }
    
    public function bitacora()
    {
        return DB::connection("sqlsrv")->table("BitacoraLicencia")->where("NumeroLicencia", $this->NumeroLicencia)->get();
    }

    public function getTurnsDetails()
    {
        $padronRepository = New PadronRepository;
        $info = [];
        foreach ($this->turns as $key => $turn) {
            $year = Carbon::parse($turn->pivot->Fecha)->format("Y");
            $now = Carbon::now("America/Mexico_City")->format("Y");
            
            $trimester = $padronRepository->getActualTrimester($turn->pivot->Fecha);
            $cost = 0;
            $basura = false;
            $location = DB::connection("sqlsrv")->table("Poblaciones")->where("IdPoblacion", $this->IdPoblacion)->first();
            if($location->Basura == 1){
                if($turn->Basura == 1){
                    $basura = true;
                }
            }
            $info = [
                'turn' => $turn,
                'alcohol' => ($turn->Alcohol == 1)? true : false,
                'trash' => $basura,
                'data' => []
            ];
            $descarga = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $turn->ConceptoDescarga)->first();
            $recoleccion = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $turn->ConceptoRecoleccion)->first();
            if($year == $now){
                $cost += $turn->CuotaFija * ($trimester["percentage"] / 100);
                $info['data'][] = [
                    "type" => 1,
                    "id" => $turn->IdGiro,
                    "nombre" => $turn->Nombre,
                    "date" => $turn->pivot->Fecha,
                    "observation" => "Costo Apertura ". $year,
                    "quantity" => $turn->pivot->Cantidad,
                    "cost" => $cost,
                ];
                if($basura){
                    $info['data'][] = [
                        "type" => 3, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                        "id" => $turn->IdGiro,
                        "nombre" => $turn->Nombre,
                        "date" => $turn->pivot->Fecha,
                        "observation" => $descarga->Descripcion,
                        "quantity" => $turn->pivot->Cantidad,
                        "cost" => $turn->CostoDescarga * ($trimester["percentage"] / 100),
                    ];
    
                    $info['data'][] = [
                        "type" => 4, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                        "id" => $turn->IdGiro,
                        "nombre" => $turn->Nombre,
                        "date" => $turn->pivot->Fecha,
                        "observation" => $descarga->Descripcion,
                        "quantity" => $turn->pivot->Cantidad,
                        "cost" => $turn->CostoRecoleccion * ($trimester["percentage"] / 100),
                    ];
                }
            }else{
                foreach ($turn->periods() as $key => $period) {
                    if($period->Ano == $year){
                        $cost += $period->CuotaFija * ($trimester["percentage"] / 100);
                        $info['data'][] = [
                            "type" => 1,
                            "id" => $turn->IdGiro,
                            "nombre" => $turn->Nombre,
                            "date" => $turn->pivot->Fecha,
                            "observation" => "Costo Apertura ". $period->Ano,
                            "quantity" => $turn->pivot->Cantidad,
                            "cost" => $cost,
                        ];
                        if($basura){
                            $info['data'][] = [
                                "type" => 3, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                                "id" => $turn->IdGiro,
                                "nombre" => $turn->Nombre,
                                "date" => $turn->pivot->Fecha,
                                "observation" => $descarga->Descripcion,
                                "quantity" => $turn->pivot->Cantidad,
                                "cost" => $turn->CostoDescarga * ($trimester["percentage"] / 100),
                            ];
            
                            $info['data'][] = [
                                "type" => 4, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                                "id" => $turn->IdGiro,
                                "nombre" => $turn->Nombre,
                                "date" => $turn->pivot->Fecha,
                                "observation" => $descarga->Descripcion,
                                "quantity" => $turn->pivot->Cantidad,
                                "cost" => $turn->CostoRecoleccion * ($trimester["percentage"] / 100),
                            ];
                        }
                    }elseif($period->Ano > $year && $period->Ano <= $now){
                        $cost += $period->CuotaFija;
                        $info['data'][] = [
                            "type" => 2,
                            "id" => $turn->IdGiro,
                            "nombre" => $turn->Nombre,
                            "date" => $period->Ano."-01-01",
                            "observation" => "Costo Refrendo ". $period->Ano,
                            "quantity" => $turn->pivot->Cantidad,
                            "cost" => $period->CuotaFija,
                        ];
                        if($basura){
                            $info['data'][] = [
                                "type" => 3, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                                "id" => $turn->IdGiro,
                                "nombre" => $turn->Nombre,
                                "date" => $period->Ano."-01-01",
                                "observation" => $descarga->Descripcion,
                                "quantity" => $turn->pivot->Cantidad,
                                "cost" => $turn->CostoDescarga ,
                            ];
            
                            $info['data'][] = [
                                "type" => 4, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                                "id" => $turn->IdGiro,
                                "nombre" => $turn->Nombre,
                                "date" => $period->Ano."-01-01",
                                "observation" => $recoleccion->Descripcion,
                                "quantity" => $turn->pivot->Cantidad,
                                "cost" => $turn->CostoRecoleccion ,
                            ];
                        }
                    }
                }
            }
        }
        
        return $info;
    }

    public function process()
    {
        return $this->belongsToMany('App\Models\Padron\ProcessType', 'SolicitudTramites', 'NumeroLicencia', 'TipoTramite');
    }

    public function documents()
    {
        return $this->belongsToMany('App\Models\Padron\Document', 'Resultados', 'NumeroLicencia', 'Requisito')->withPivot('Fecha', 'Oficio', 'FolioInspeccion', 'DictamenFavorable', 'Prorroga', 'Comentario', 'Archivo', "Estatus");
    }
    
    public function requests()
    {
        return $this->hasMany('App\Models\Padron\Solicitud', 'NumeroLicencia', 'NumeroLicencia');
    }
    
    public function debts()
    {
        return DB::connection("sqlsrv")->table("Adeudos")
        ->where("NumeroLicencia", $this->NumeroLicencia)->orderBy("Referencia", "ASC")->get();
    }

    public function debtsN()
    {
        return DB::connection("sqlsrv")->table("Adeudos")
        ->leftJoin("Pagos",function($join){
            $join->on("Pagos.CuentaDepto", "=", "Adeudos.CuentaDepto")
            ->where("Adeudos.Referencia",   "Pagos.Referencia");
        })->whereNull("Pagos.Caja")
        ->where("Adeudos.NumeroLicencia", $this->NumeroLicencia)
        ->select("Adeudos.*")
        ->orderBy("Referencia", "ASC")->get();
    }

}
