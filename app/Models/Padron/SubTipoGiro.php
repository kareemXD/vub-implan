<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class SubTipoGiro extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "SubTipoGiro";
    protected $primaryKey = 'SubTipoGiro';
    public $timestamps = false;
    public $incrementing = false;
}
