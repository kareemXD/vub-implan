<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class GiroConcepto extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "GiroConceptos";
    public $timestamps = false;
    public $incrementing = false;
}
