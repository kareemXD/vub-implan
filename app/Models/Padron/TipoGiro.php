<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class TipoGiro extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "TipoGiro";
    protected $primaryKey = 'TipoGiro';
    public $timestamps = false;
    public $incrementing = false;
}
