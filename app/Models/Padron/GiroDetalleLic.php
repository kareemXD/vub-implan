<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class GiroDetalleLic extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "GiroDetalleLic";
    public $timestamps = false;
    public $incrementing = false;
}
