<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class ClaseGiro extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Clase";
    protected $primaryKey = 'IdClase';
    public $timestamps = false;
    public $incrementing = false;
}
