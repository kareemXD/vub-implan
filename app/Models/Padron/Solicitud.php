<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

//Repositories
use App\Repositories\PadronRepository;

use Carbon\Carbon;
use DB;

class Solicitud extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "SolicitudLicencia";
    protected $primaryKey = "NumeroSolicitud";
    public $timestamps = false;
    public $incrementing = false;

    public function taxpayer()
    {
        return $this->belongsTo('App\Models\Padron\Taxpayer', 'IdContribuyente', 'IdContribuyente');
    }

    public function turns()
    {
        return $this->belongsToMany('App\Models\Padron\Turn', 'GiroDetalleSol', 'NumeroSolicitud', 'IdGiro')->withPivot('Secuencia', 'Fecha', 'Descripcion', 'Observacion', 'Cuota', 'Cantidad', 'LicenciaAnexo', 'Refrendo');
    }

    public function giroDetalles()
    {
        return $this->hasMany(GiroDetalleSol::class, "NumeroSolicitud", "NumeroSolicitud");
    }

    public function pcRequirements()
    {
        return $this->hasMany('App\Models\Padron\SolicitudPcRequirement', 'id_solicitud', 'NumeroSolicitud');
    }

    public function pcRequests()
    {
        return $this->hasMany('App\Models\Padron\SolicitudPcRequest', 'id_solicitud', 'NumeroSolicitud');
    }

    public function getTurnsDetails()
    {
        $info = [];
        $info['data'] = [];

        foreach ($this->turns()->orderBy('Fecha', 'ASC')->get() as $key => $turn) 
        {
            $date = Carbon::parse($turn->pivot->Fecha);
            $basura = false;

            $info = [
                'turn' => $turn,
                'alcohol' => ($turn->Alcohol == 1)? true : false,
                'trash' => $basura,
                'data' => $info['data']
            ];

            if ($turn->pivot->Refrendo == 1)
            {
                $info['data'][] = [
                    "type" => 2, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                    "id" => $turn->IdGiro,
                    "nombre" => $turn->Nombre,
                    "date" => $date->format('Y-m-d'),
                    "observation" => $turn->pivot->Observacion,
                    "cost" => str_replace(',', '', number_format($turn->CuotaFija, 2)),
                    "quantity" => str_replace(',', '', number_format($turn->pivot->Cantidad, 2)),
                    "subtotal" => str_replace(',', '', number_format((float)($turn->pivot->Cuota), 2)),
                ];
            }else
            {
                $padronRepository = new PadronRepository;
                $trimester = $padronRepository->getActualTrimester($turn->pivot->Fecha);
                $period = $turn->getPeriod($date->year);
    
                $cost = 0;
                if (!empty($period))
                {
                    $cost += $period->CuotaFija * ($trimester["percentage"] / 100);
                }else
                {
                    $cost += $turn->CuotaFija * ($trimester["percentage"] / 100);
                }

                $info['data'][] = [
                    "type" => 1, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                    "id" => $turn->IdGiro,
                    "nombre" => $turn->Nombre,
                    "date" => $date->format('Y-m-d'),
                    "observation" => $turn->pivot->Observacion,
                    "cost" => str_replace(',', '', number_format($cost, 2)),
                    "quantity" => str_replace(',', '', number_format($turn->pivot->Cantidad, 2)),
                    "subtotal" => str_replace(',', '', number_format((float)($cost), 2)),
                ];
            }
        }
        
        if($info == []){
            $info['data'] = [];
        }

        return $info ;
    }

    public function process()
    {
        return $this->belongsToMany('App\Models\Padron\ProcessType', 'SolicitudTramites', 'NumeroSolicitud', 'TipoTramite');
    }

    public function documents()
    {
        return $this->belongsToMany('App\Models\Padron\Document', 'Resultados', 'NumeroSolicitud', 'Requisito')->withPivot('Fecha', 'Oficio', 'FolioInspeccion', 'DictamenFavorable', 'Prorroga', 'Comentario', 'Archivo', "Estatus", "Verificacion");
    }
    
    public function legends()
    {
        return $this->belongsToMany('App\Models\Padron\Legend', 'LeyendasSolicitudes', 'NumeroSolicitud', 'IdLeyenda')->withPivot('NumeroLicencia');
    }

    public function garbages()
    {
        return $this->hasMany('App\Models\Padron\SolicitudLicenciaBasura', 'SolicitudId', 'NumeroSolicitud');
    }

    public function documentsFinished()
    {
        $flag = 0;
        foreach ($this->documents as $key => $document) {
            if($document->pivot->Estatus == 0)
            {
                $flag = 1;
            }
        }
        if($flag == 0)
        { return true;}
        else
        {return false;}
    }
    
    public function documentsVerifid()
    {
        $flag = 0;
        if(count($this->documents) == 0){
            return 'true';
        }
        foreach ($this->documents as $key => $document) {
            if($document->pivot->Verificacion == 0)
            {
                $flag = 1;
            }
        }
        
        if($flag == 0)
        { return 'true';}
        else
        {return 'false';}
    }

    public function pcRequirementStatus($pcRequirementId)
    {
        $request = $this;
        $status = 'Pendiente';

        $pcRequirement = $request->pcRequirements()->where('id_requisito', $pcRequirementId)->first();
        if (!empty($pcRequirement))
        {
            $status = 'Subido';
        }

        return $status;
    }

    public function pcRequirementStatusClass($pcRequirementId)
    {
        $request = $this;
        $class = 'text-white bg-warning';

        $pcRequirement = $request->pcRequirements()->where('id_requisito', $pcRequirementId)->first();
        if (!empty($pcRequirement))
        {
            $class = 'text-white bg-success';
        }

        return $class;
    }
}
