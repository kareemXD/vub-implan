<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class ProcessType extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "TiposTramite";
    protected $primaryKey = "TipoTramite";
    public $timestamps = false;
    public $incrementing = false;
}
