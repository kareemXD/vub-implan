<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class Legend extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Leyendas";
    protected $primaryKey = "IdLeyenda";
    public $timestamps = false;
    public $incrementing = false;
}
