<?php

namespace App\Models\Padron;

use App\Repositories\PadronRepository;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Poblaciones";
    protected $primaryKey = "IdPoblacion";
    public $timestamps = false;
    public $incrementing = false;

    public function padronDebts()
    {
        $poblacion = $this;

        $padronRepository = new PadronRepository;
        return $padronRepository->getDebtsByLocation($poblacion->IdPoblacion);
    }

    public function padronLicencesWithDebts()
    {
        $poblacion = $this;

        $padronRepository = new PadronRepository;
        return $padronRepository->getNumberOfLicensesWithDebts($poblacion->IdPoblacion);
    }
}
