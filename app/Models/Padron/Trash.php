<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class Trash extends Model
{
    protected $connection = "pgsql";
    protected $table = "giro_has_trash";

    public function turn()
    {
        return $this->belongsTo('App\Models\Padron\Turn', 'giro_id', 'IdGiro');
    }
}
