<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class GiroDetalleSol extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "GiroDetalleSol";
    public $timestamps = false;
    public $incrementing = false;
}
