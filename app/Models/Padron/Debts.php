<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class Debts extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Adeudos";
    // protected $primaryKey = "NumeroSolicitud";
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = ['Departamento', 'Concepto', 'CuentaDepto', 'Referencia', 'FechaMovto', 
    'NumeroLicencia', 'FechaVence', 'Descripcion', 'Importe', 'Estado', 'Cargo', 'Ejecutor', 'Folio', 'Requerimiento', 'Referencia2', 'Referencia3'];

    public function concept()
    {
        return $this->belongsTo('App\Models\Padron\Concepto', 'Concepto', 'Concepto');
    }
}
