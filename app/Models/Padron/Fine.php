<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class Fine extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Licencias";
    protected $primaryKey = "NumeroLicencia";
    public $timestamps = false;
    public $incrementing = false;
}
