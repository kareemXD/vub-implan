<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class LicensePcRequest extends Model
{
    protected $table = 'licencia_pc_solicitud';
    protected $connection = 'pgsql';
    protected $fillable = ['id_licencia', 'id_pc_solicitud'];

    public function pcRequest()
    {
        return $this->belongsTo('App\Models\ProteccionCivil\Request', 'id_pc_solicitud', 'id');
    }

    public function padronLicencia()
    {
        return $this->belongsTo('App\Models\Padron\License', 'id_licencia', 'NumeroLicencia');
    }
}
