<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Alertas";
    protected $primaryKey = "IdAlerta";
    public $timestamps = false;
    public $incrementing = false;
}
