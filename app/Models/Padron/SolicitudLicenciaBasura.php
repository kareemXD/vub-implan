<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class SolicitudLicenciaBasura extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "SolicitudLicencia_Basura";
    public $timestamps = false;
    public $incrementing = false;

    public function solicitud()
    {
        return $this->belongsTo('App\Models\Padron\Solicitud', 'NumeroSolicitud', 'SolicitudId');
    }
}
