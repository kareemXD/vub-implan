<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;
//Repositories
use App\Repositories\PadronRepository;
use Illuminate\Support\Facades\DB;

class License extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Licencias";
    protected $primaryKey = "NumeroLicencia";
    public $timestamps = false;
    public $incrementing = false;
    
    public function taxpayer()
    {
        return $this->belongsTo('App\Models\Padron\Taxpayer', 'IdContribuyente', 'IdContribuyente');
    }

    public function turns()
    {
        return $this->belongsToMany('App\Models\Padron\Turn', 'GiroDetalleLic', 'NumeroLicencia', 'IdGiro')->withPivot('Secuencia', 'Fecha', 'Descripcion', 'Observacion', 'Cuota', 'Cantidad', 'LicenciaAnexo', 'Refrendo');
    }

    public function giroDetalles()
    {
        return $this->hasMany(GiroDetalleLic::class, "NumeroLicencia", "NumeroLicencia");
    }
    public function legends()
    {
        return $this->belongsToMany('App\Models\Padron\Legend', 'LeyendasLicencias', 'NumeroLicencia', 'IdLeyenda')->withPivot('FolioLicencia');
    }

    public function getAddress(){
        return $this->city()->NombrePoblacion . ', ' .$this->colony()->NombreColonia. ', ' .$this->street()->NombreCalle . ' #' . $this->Exterior.' '.$this->Interior;
    }

    public function city()
    {
        return DB::connection("sqlsrv")->table("Poblaciones")->where("IdPoblacion", $this->IdPoblacion)->first();
    }

    public function location()
    {
        return DB::connection("sqlsrv")->table("Poblaciones")->where("IdPoblacion", $this->IdPoblacion)->first();
    }

    public function colony()
    {
        return DB::connection("sqlsrv")->table("Colonias")->where("IdColonia", $this->IdColonia)->first();
    }

    public function street()
    {
        return DB::connection("sqlsrv")->table("Calles")->where("IdCalle", $this->IdCalle)->first();
    }

    public function street1()
    {
        return DB::connection("sqlsrv")->table("Calles")->where("IdCalle", $this->IdCruce1)->first();
    }

    public function street2()
    {
        return DB::connection("sqlsrv")->table("Calles")->where("IdCalle", $this->IdCruce2)->first();
    }
    
    public function alerts()
    {
        return DB::connection("sqlsrv")->table("Alertas")->where("NumeroLicencia", $this->NumeroLicencia)->get();
    }
    
    public function bitacora()
    {
        return DB::connection("sqlsrv")->table("BitacoraLicencia")->where("NumeroLicencia", $this->NumeroLicencia)->orderBy('FechaAlta', 'asc')->get();
    }

    public function garbages()
    {
        return $this->hasMany('App\Models\Padron\LicenciaBasura', 'LicenciaId', 'NumeroLicencia');
    }

    public function getTurnsDetails()
    {
        $info = [];
        $info["data"] = [];
        
        foreach ($this->turns()->orderBy('Fecha', 'ASC')->get() as $key => $turn) 
        {
            $date = Carbon::parse($turn->pivot->Fecha);
            $basura = false;

            $info = [
                'turn' => $turn,
                'alcohol' => ($turn->Alcohol == 1)? true : false,
                'trash' => $basura,
                'data' => $info["data"],
            ];

            if ($turn->pivot->Refrendo == 1)
            {
                $info['data'][] = [
                    "type" => 2, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                    "id" => $turn->IdGiro,
                    "nombre" => $turn->Nombre,
                    "date" => $date->format('Y-m-d'),
                    "observation" => $turn->pivot->Observacion,
                    "cost" => str_replace(',', '', number_format($turn->CuotaFija, 2)),
                    "quantity" => str_replace(',', '', number_format($turn->pivot->Cantidad, 2)),
                    "subtotal" => str_replace(',', '', number_format((float)($turn->pivot->Cuota), 2)),
                ];
            }else
            {
                $padronRepository = new PadronRepository;
                $trimester = $padronRepository->getActualTrimester($turn->pivot->Fecha);
                $period = $turn->getPeriod($date->year);
    
                $cost = 0;
                if (!empty($period))
                {
                    $cost += $period->CuotaFija * ($trimester["percentage"] / 100);
                }else
                {
                    $cost += $turn->CuotaFija * ($trimester["percentage"] / 100);
                }
    
                $info['data'][] = [
                    "type" => 1, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                    "id" => $turn->IdGiro,
                    "nombre" => $turn->Nombre,
                    "date" => $date->format('Y-m-d'),
                    "observation" => $turn->pivot->Observacion,
                    "cost" => str_replace(',', '', number_format($cost, 2)),
                    "quantity" => str_replace(',', '', number_format($turn->pivot->Cantidad, 2)),
                    "subtotal" => str_replace(',', '', number_format((float)($cost), 2)),
                ];
            }
        }

        //end
        if($info == []){
            $info['data'] = [];
        }
        return $info;
    }

    public function process()
    {
        return $this->belongsToMany('App\Models\Padron\ProcessType', 'SolicitudTramites', 'NumeroLicencia', 'TipoTramite');
    }

    public function processLicense()
    {
        return $this->belongsToMany('App\Models\Padron\ProcessType', 'LicenciasTramites', 'NumeroLicencia', 'TipoTramite');
    }

    public function documents()
    {
        return $this->belongsToMany('App\Models\Padron\Document', 'Resultados', 'NumeroLicencia', 'Requisito')->withPivot('Fecha', 'Oficio', 'FolioInspeccion', 'DictamenFavorable', 'Prorroga', 'Comentario', 'Archivo', "Estatus");
    }
    
    public function requests()
    {
        return $this->hasMany('App\Models\Padron\Solicitud', 'NumeroLicencia', 'NumeroLicencia');
    }
    
    public function debts()
    {
        return DB::connection("sqlsrv")->table("Adeudos")
        ->where("NumeroLicencia", $this->NumeroLicencia)->orderBy("Referencia", "ASC")->get();
    }

    public function debtsN()
    {
        return DB::connection("sqlsrv")->table("Adeudos")
        ->leftJoin("Pagos",function($join){
            $join->on("Pagos.CuentaDepto", "=", "Adeudos.CuentaDepto")
            ->where("Adeudos.Referencia", "Pagos.Referencia");
        })->whereNull("Pagos.Caja")
        ->where("Adeudos.NumeroLicencia", $this->NumeroLicencia)
        ->select("Adeudos.*")
        ->orderBy("Adeudos.Referencia", "ASC")->get(); 
    }

    public function debtsArray()
    {
        return DB::connection("sqlsrv")->table("Adeudos")
        ->leftJoin("Pagos",function($join){
            $join->on("Pagos.CuentaDepto", "=", "Adeudos.CuentaDepto")
            ->where("Adeudos.Referencia", "Pagos.Referencia");
        })->whereNull("Pagos.Caja")
        ->where("Adeudos.NumeroLicencia", $this->NumeroLicencia)
        ->select(DB::raw("year(Adeudos.FechaVence) as Year"))
       ->pluck("Year")->toArray(); 
       /* $data =  DB::connection("sqlsrv")->table("Adeudos")->where(DB::raw("(select count(*) from Pagos where Pagos.CuentaDepto=Adeudos.CuentaDepto and Adeudos.Referencia=Pagos.Referencia)"), 0)
                ->where("Adeudos.NumeroLicencia", $this->NumeroLicencia)
                ->select("Adeudos.*")->pluck("Referencia")->toArray();
        return $data;*/
    }

    public function debtsArrayReference()
    {
        return DB::connection("sqlsrv")->table("Adeudos")
        ->leftJoin("Pagos",function($join){
            $join->on("Pagos.CuentaDepto", "=", "Adeudos.CuentaDepto")
            ->where("Adeudos.Referencia", "Pagos.Referencia");
        })->whereNull("Pagos.Caja")
        ->where("Adeudos.NumeroLicencia", $this->NumeroLicencia)
        ->select("Adeudos.*")
       ->pluck("Adeudos.Referencia")->toArray(); 
       /* $data =  DB::connection("sqlsrv")->table("Adeudos")->where(DB::raw("(select count(*) from Pagos where Pagos.CuentaDepto=Adeudos.CuentaDepto and Adeudos.Referencia=Pagos.Referencia)"), 0)
                ->where("Adeudos.NumeroLicencia", $this->NumeroLicencia)
                ->select("Adeudos.*")->pluck("Referencia")->toArray();
        return $data;*/
    }

    public function getTotalAdeudo()
    {
        $importes = 0;
        $totalBasura = 0;
        $totalAlcohol = 0;
        $total = 0;
        foreach ($this->debtsN() as $debt)
        {
            $basura = 0;
            $alcohol = 0;
            $reference = substr($debt->Referencia, 0, 4);
            
            if((strpos(strtoupper($debt->Descripcion), "BASURA") !== false) || strpos(strtoupper($debt->Descripcion), "RECOLECCION Y TRASLADO") !== false)
            {
                if ((int)$reference >= 2022)
                {
                    $basura = $debt->Importe * 0.15;
                }else
                {
                    $basura = $debt->Importe * 0.12;
                }
            }else
            {
                $turn = DB::connection("sqlsrv")->table("Giros")->where("Nombre", $debt->Descripcion)->first();
                $concept = DB::connection('sqlsrv')->table('Conceptos')->where('Concepto', $debt->Concepto)->first();

                $addUAN = false;
                $wordsToSearch = ['ANUNC', 'PENDON', 'DIFUSION', 'PERIFONEO', 'VOLANTEO'];
                if (!empty($concept))
                {
                    foreach ($wordsToSearch as $word)
                    {
                        if ((strpos(strtoupper($concept->Descripcion), $word) !== false))
                        {
                            $addUAN = true;
                        }
                    }
                }

                if($debt->Referencia2 == "Alcohol" || @$turn->Alcohol==true || $addUAN === true)
                {
                    // Cobrar UAN a todo los giros que tengan alcoholes
                    if ((int)$reference >= 2022)
                    {
                        $alcohol = $debt->Importe * 0.15;
                    }else
                    {
                        $alcohol = $debt->Importe * 0.12;
                    }
                }
            }

            $importes += $debt->Importe;
            $totalBasura += $basura;
            $totalAlcohol += $alcohol;
            $total += $debt->Importe + $alcohol + $basura;
        }

        return $total;
    }

    //adeudos de la licencia
    public function adeudos()
    {
        return $this->hasMany(Debts::class, "CuentaDepto");
    }

    //pagos realizados a la licencia
    public function pagos()
    {
        return $this->hasMany(Payment::class, "CuentaDepto");
    }

    public function getyearsDebt()
    {
        $years_array =  DB::connection("sqlsrv")->table("Adeudos")
        ->leftJoin("Pagos",function($join){
            $join->on("Pagos.CuentaDepto", "=", "Adeudos.CuentaDepto")
            ->where("Adeudos.Referencia", "Pagos.Referencia");
        })->whereNull("Pagos.Caja")
        ->where("Adeudos.NumeroLicencia", $this->NumeroLicencia)
        ->select(DB::raw("year(Adeudos.FechaVence) as FechaVence"))
        ->distinct()
        ->pluck("FechaVence")->toArray();
        
        return implode(",", $years_array);
    }

    public function pcRequirements()
    {
        return $this->hasMany('App\Models\Padron\LicensePcRequirement', 'id_licencia', 'NumeroLicencia');
    }

    public function pcRequests()
    {
        return $this->hasMany('App\Models\Padron\LicensePcRequest', 'id_licencia', 'NumeroLicencia');
    }

    public function pcRequirementStatus($pcRequirementId)
    {
        $license = $this;
        $status = 'Pendiente';

        $pcRequirement = $license->pcRequirements()->where('id_requisito', $pcRequirementId)->first();
        if (!empty($pcRequirement))
        {
            $status = 'Subido';
        }

        return $status;
    }

    public function pcRequirementStatusClass($pcRequirementId)
    {
        $license = $this;
        $class = 'text-white bg-warning';

        $pcRequirement = $license->pcRequirements()->where('id_requisito', $pcRequirementId)->first();
        if (!empty($pcRequirement))
        {
            $class = 'text-white bg-success';
        }

        return $class;
    }
}
