<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

use App\Models\Padron\HistoricoGiro;

use Carbon\Carbon;

//Repositories
use App\Repositories\PadronRepository;

class Turn extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Giros";
    protected $primaryKey = "IdGiro";
    public $timestamps = false;

    public function documents()
    {
        return $this->belongsToMany('App\Models\Padron\Document', 'GiroReq', 'IdGiro', 'Requisito')->withPivot('Alta', 'CambioDom', 'CambioGiroAnun', 'Traspaso', 'ReposicionLicencia', 'Baja', "Dependencia");
    }

    public function concepts()
    {
        return $this->belongsToMany('App\Models\Padron\ProcessType', 'GiroConceptos', 'IdGiro', 'TipoTramite')->withPivot('Concepto');
    }

    public function trashYears()
    {
        return $this->hasMany('App\Models\Padron\Trash', 'giro_id', 'IdGiro')->orderBy('year', 'asc');
    }

    public function periods()
    {
        $periods = HistoricoGiro::where("IdGiro", $this->IdGiro)->get();
        return $periods;
    }
    
    public function getCost()
    {
        $padronRepository = new PadronRepository;
        $turn = $this;
        // dd($this->pivot->Fecha);
        $year = Carbon::parse($this->pivot->Fecha)->format("Y");
        $now = Carbon::now("America/Mexico_City")->format("Y");
        $trimester = $padronRepository->getActualTrimester($this->pivot->Fecha);
        $cost = 0;
        // dd($turn);
        if($year == $now){
            $cost += $turn->CuotaFija * ($trimester["percentage"] / 100);
        }else{
            foreach ($turn->periods() as $key => $period) {
                if($period->Ano == $year){
                    $cost += $period->CuotaFija * ($trimester["percentage"] / 100);
                }elseif($period->Ano > $year && $period->Ano <= $now){
                    $cost += $period->CuotaFija;
                }
            }
            $cost += $turn->CuotaFija;
        }
        return number_format($cost, 2, ".", "");
    }

    public function getPeriod($year)
    {
        $turn = $this;
        
        $period = HistoricoGiro::where('IdGiro', $turn->IdGiro)->where('Ano', $year)->first();
        if (empty($period))
        {
            $period = HistoricoGiro::where('IdGiro', $turn->IdGiro)->orderBy('Ano', 'desc')->first();
        }

        return $period;
    }
}
