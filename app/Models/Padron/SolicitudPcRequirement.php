<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class SolicitudPcRequirement extends Model
{
    protected $table = 'solicitud_pc_requisitos';
    protected $connection = 'pgsql';
    protected $fillable = ['id_solicitud', 'id_requisito', 'documento'];
}
