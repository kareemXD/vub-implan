<?php

namespace App\Models\Padron\CW;

use Illuminate\Database\Eloquent\Model;

class PLPoints extends Model
{
    protected $connection = 'cartoweb';
    protected $table = 'pl_licencias';
    public $timestamps = false;

    protected $fillable = ["licencia", "nombre", "fecha_alta", "superficie", "empleados", "poblacion", "colonia", 
    "domicilio", "calle1", "calle2", "numero_int", "numero_ext", "contribuyente_sexo", "ubicacion", "adeudo", "giro"];
}
