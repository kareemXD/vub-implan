<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class SolicitudPcRequest extends Model
{
    protected $table = 'solicitud_pc_solicitud';
    protected $connection = 'pgsql';
    protected $fillable = ['id_solicitud', 'id_pc_solicitud'];

    public function pcRequest()
    {
        return $this->belongsTo('App\Models\ProteccionCivil\Request', 'id_pc_solicitud', 'id');
    }

    public function padronSolicitud()
    {
        return $this->belongsTo('App\Models\Padron\Solicitud', 'id_solicitud', 'NumeroSolicitud');
    }
}
