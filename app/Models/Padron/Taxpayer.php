<?php

namespace App\Models\Padron;

use Illuminate\Database\Eloquent\Model;

class Taxpayer extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "PadronContribuyentes";
    protected $primaryKey = 'IdContribuyente';
    public $timestamps = false;

    public function licenses()
    {
        return $this->hasMany('App\Models\Padron\License', 'IdContribuyente', 'IdContribuyente');
    }
}
