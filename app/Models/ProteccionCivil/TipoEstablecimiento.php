<?php

namespace App\Models\ProteccionCivil;

use Illuminate\Database\Eloquent\Model;

class TipoEstablecimiento extends Model
{
    protected $table = 'pc_tipo_establecimientos';
}
