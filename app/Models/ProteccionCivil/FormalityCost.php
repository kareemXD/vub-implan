<?php

namespace App\Models\ProteccionCivil;

use Illuminate\Database\Eloquent\Model;

class FormalityCost extends Model
{
    protected $table = 'pc_tramite_costos';
    protected $fillable = ['year', 'costo', 'costo_refrendo'];
}
