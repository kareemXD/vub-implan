<?php

namespace App\Models\ProteccionCivil;

use Illuminate\Database\Eloquent\Model;

class RequestFormality extends Model
{
    protected $table = 'pc_solicitud_tramites';
    protected $fillable = ['id_tramite', 'costo', 'year'];

    public function formality()
    {
        return $this->belongsTo('App\Models\ProteccionCivil\Formality', 'id_tramite', 'id');
    }
}
