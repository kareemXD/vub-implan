<?php

namespace App\Models\ProteccionCivil;

use Illuminate\Database\Eloquent\Model;

class RequestRequirement extends Model
{
    protected $table = 'pc_solicitud_requisitos';
    protected $fillable = ['id_requisito', 'documento'];

    public function requirement()
    {
        return $this->belongsTo('App\Models\ProteccionCivil\Requirement', 'id_requisito', 'id');
    }
}
