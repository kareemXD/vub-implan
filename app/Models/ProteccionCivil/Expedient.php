<?php

namespace App\Models\ProteccionCivil;

use Illuminate\Database\Eloquent\Model;

class Expedient extends Model
{
    protected $table = 'pc_expedientes';
}
