<?php

namespace App\Models\ProteccionCivil;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Formality extends Model
{
    protected $table = 'pc_tramites';

    public function costs()
    {
        return $this->hasMany('App\Models\ProteccionCivil\FormalityCost', 'id_tramite', 'id');
    }

    public function currentCost()
    {
        $formality = $this;
        $now = now();

        $cost = 0;
        $currentCost = $formality->costs()->where('year', $now->year)->first();
        if (!empty($currentCost))
        {
            $cost = $currentCost->costo;
        }else
        {
            $lastCost = $formality->costs()->orderBy('year', 'desc')->first();
            if (!empty($lastCost))
            {
                $cost = $lastCost->costo;
            }
        }

        return $cost;
    }

    public function costByYear($year)
    {
        $formality = $this;

        $cost = $formality->costs()->where('year', $year)->first();
        if (empty($currentCost))
        {
            $lastCost = $formality->costs()->orderBy('year', 'desc')->first();
            if (!empty($lastCost))
            {
                $cost = $lastCost;
            }
        }

        return $cost;
    }

    public function currentCostRefrendo()
    {
        $formality = $this;
        $now = now();

        $cost = 0;
        $currentCost = $formality->costs()->where('year', $now->year)->first();
        if (!empty($currentCost))
        {
            $cost = $currentCost->costo_refrendo;
        }else
        {
            $lastCost = $formality->costs()->orderBy('year', 'desc')->first();
            if (!empty($lastCost))
            {
                $cost = $lastCost->costo_refrendo;
            }
        }

        return $cost;
    }
}
