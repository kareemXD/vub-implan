<?php

namespace App\Models\ProteccionCivil;

use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    protected $table = 'pc_requisitos';
}
