<?php

namespace App\Models\ProteccionCivil;

use App\Models\Colony;
use App\Models\Du\Population;
use App\Models\Street;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\ProteccionCivil\RequestRepository;

class Request extends Model
{
    protected $table = 'pc_solicitudes';

    public function requirements()
    {
        return $this->hasMany('App\Models\ProteccionCivil\RequestRequirement', 'id_solicitud', 'id');
    }

    public function formalities()
    {
        return $this->hasMany('App\Models\ProteccionCivil\RequestFormality', 'id_solicitud', 'id');
    }

    public function documents()
    {
        return $this->hasMany('App\Models\ProteccionCivil\RequestDocument', 'id_solicitud', 'id');
    }

    public function colony()
    {
        return $this->belongsTo(Colony::class, "id_colonia");
    }

    public function street()
    {
        return $this->belongsTo(Street::class, "id_calle");
    }

    public function cruce1(){
        return $this->belongsTo(Street::class, "id_cruce1");

    }

    public function cruce2(){
        return $this->belongsTo(Street::class, "id_cruce2");

    }

    public function population()
    {
        return $this->belongsTo(Population::class, "id_poblacion");
    }

    public function padronSolicitudes()
    {
        return $this->hasMany('App\Models\Padron\SolicitudPcRequest', 'id_pc_solicitud', 'id');
    }

    public function padronLicencias()
    {
        return $this->hasMany('App\Models\Padron\LicensePcRequest', 'id_pc_solicitud', 'id');
    }

    public function duSolicitudes()
    {
        return $this->hasMany('App\Models\Du\SolicitudPcRequest', 'id_pc_solicitud', 'id');
    }

    public function turn()
    {
        return $this->belongsTo('App\Models\Padron\Turn', 'id_giro', 'IdGiro');
    }

    public function turns()
    {
        return $this->hasMany('App\Models\ProteccionCivil\RequestTurn', 'id_solicitud', 'id');
    }

    public function expedient()
    {
        return $this->belongsTo('App\Models\ProteccionCivil\Expedient', 'pc_expediente_id', 'id');
    }

    public function turnsText()
    {
        $request = $this;
        $turnText = "";

        foreach ($request->turns as $key => $turn)
        {
            if ($key == 0)
            {
                $turnText .= strtoupper($turn->giro);
            }else
            {
                $turnText .= ', '.strtoupper($turn->giro);
            }
        }

        return $turnText;
    }

    public function padronSolicitud()
    {
        $padronSolicitud = $this->padronSolicitudes()->first();
        
        if (!empty($padronSolicitud))
        {
            return $padronSolicitud->padronSolicitud;
        }

        return null;
    }

    public function padronLicencia()
    {
        $padronLicencia = $this->padronLicencias()->first();
        
        if (!empty($padronLicencia))
        {
            return $padronLicencia->padronLicencia;
        }

        return null;
    }

    public function duSolicitud()
    {
        $duSolicitud = $this->duSolicitudes()->first();
        
        if (!empty($duSolicitud))
        {
            return $duSolicitud->duSolicitud;
        }

        return null;
    }

    public function fullName()
    {
        $formality = $this;
        $fullName = $formality->nombre;

        return $fullName;
    }

    public function status()
    {
        $request = $this;
        $status = config('system.pc.requests.statuses_labels.'.$request->estatus);

        return $status;
    }

    public function statusClass()
    {
        $request = $this;
        switch ($request->estatus) {
            case '2':
                $estatus = 'text-white bg-warning';
                break;

            case '3':
                $estatus = 'text-white bg-secondary';
                break;
            
            case '4':
                $estatus = 'text-white bg-info';
                break;

            case '5':
                $estatus = 'text-white bg-success';
                break;

            case '6':
                $estatus = 'text-white bg-danger';
                break;

            case '7':
                $estatus = 'text-white bg-danger';
                break;
            
            default:
                $estatus = 'text-dark bg-light';
                break;
        }

        return $estatus;
    }

    public function requirementStatus($requirementId)
    {
        $request = $this;

        $requestRepository = new RequestRepository;
        return $requestRepository->requirementStatus($request->id, $requirementId);
    }

    public function fullAddress()
    {
        $request = $this;

        $requestRepository = new RequestRepository;
        return $requestRepository->getFullAddressByRequest($request->id);
    }

    public function requestNumberText()
    {
        $formality = $this;
        $formalityNumberText = $formality->numero_solicitud;

        return $formalityNumberText;
    }
}
