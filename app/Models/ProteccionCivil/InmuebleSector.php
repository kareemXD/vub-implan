<?php

namespace App\Models\ProteccionCivil;

use Illuminate\Database\Eloquent\Model;

class InmuebleSector extends Model
{
    protected $table = 'pc_inmueble_sectores';
}
