<?php

namespace App\Models\ProteccionCivil;

use Illuminate\Database\Eloquent\Model;

class RequestDocument extends Model
{
    protected $table = 'pc_solicitud_documentos';
    protected $fillable = ['descripcion', 'documento'];
}
