<?php

namespace App\Models\ProteccionCivil;

use Illuminate\Database\Eloquent\Model;

class RequestTurn extends Model
{
    protected $table = 'pc_solicitud_giros';

    public function request()
    {
        return $this->belongsTo('App\Models\ProteccionCivil\Request', 'id_solicitud', 'id');
    }
}
