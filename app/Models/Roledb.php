<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//Models

//Plugins
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Roledb extends Model
{
    protected $table = "roles";

    public function menus()
    {
        return $this->belongsToMany('App\Models\Menu', 'roles_has_menus', 'rol_id', 'menu_id');
    }

    public function permissions()
    {
        return $this->belongsToMany('Spatie\Permission\Models\Permission', 'role_has_permissions', 'role_id', 'permission_id');
    }

    public function customPermissions()
    {
        $permissions = $this->permissions()->where('custom', 1)->get();
        return $permissions;
    }
}
