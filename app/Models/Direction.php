<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Direction extends Model
{
    protected $table = "direcciones";

    public function actionLines()
    {
        return $this->belongsToMany('App\Models\PmActionLine', 'direcciones_has_pm_linea_accion', 'direccion_id', 'pm_linea_accion_id')->withPivot('status');
    }
    
    public function infIndicators()
    {
        return $this->belongsToMany('App\Models\InfIndicator', 'inf_indicador_has_direcciones', 'direccion_id', 'inf_indicador_id')->withPivot('status');
    }
    
    public function scheMeta()
    {
        return $this->belongsToMany('App\Models\AgeMeta', 'age_meta_has_direccion', 'direcciones_id', 'age_metas_id')->withPivot('estatus');
    }
    
    public function mirIndicators()
    {
        return $this->belongsToMany('App\Models\MirIndicator', 'direcciones_has_mir_indicadores', 'direcciones_id', 'mir_indicadores_id')->withPivot('status');
    }

    public function positions()
    {
        return $this->hasMany('App\Models\Position', 'direccion_id');
    }
    
    public function programs()
    {
        return $this->hasMany('App\Models\Program', 'direccion_id');
    }

    public function getMetadata()
    {
        return $this->hasMany('App\Models\DirectionMetadata', 'direccion_id');
    }

    public function director()
    {
        return $this->belongsTo('App\Models\Position', 'puestos_id');
    }
    
    public function activities()
    {
        return $this->hasMany('App\Models\Tracing\MonthlyActivity', 'direccion_id');
    }
    
    public function subdirections()
    {
        return $this->hasMany('App\Models\Direction', 'padre', "id");
    }
    public function hasDad()
    {
        if(empty($this->padre))
        {
            return false;
        }else {
            return true;
        }
    }

    public function dad()
    {
        return $this->belongsTo('App\Models\Direction', 'padre', "id");
    }
}
