<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class Ingresos_Concepto extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Conceptos";
    protected $primaryKey = "Concepto";
    public $timestamps = false;
    public $incrementing = false;
}
