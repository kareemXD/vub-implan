<?php

namespace App\Models\Du;

use App\Models\Colony;
use App\Models\Street;
use App\Repositories\DuRepository;
use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $table = "du_solicitudes";

    public function user()
    {
        return $this->belongsTo('App\User', 'id_user', 'id');
    }

    public function taxpayer()
    {
        return $this->belongsTo('App\Models\Du\Taxpayer', 'id_contribuyente');
    }
    
    public function documents()
    {
        return $this->belongsToMany('App\Models\Du\Requirement', 'du_solicitud_requerimientos', 'id_solicitud', 'id_requerimiento')->withPivot('comentario', 'archivo', "estatus", 'updated_at');
    }

    public function procedures()
    {
        return $this->belongsToMany('App\Models\Du\Process', 'du_solicitud_tramite', 'solicitud_id', 'tramite_id');
    }
    
    public function process()
    {
        return $this->belongsTo('App\Models\Du\Process', 'id_tramite');
    }
    
    public function comments()
    {
        return $this->hasMany('App\Models\Du\Comment', 'id_solicitud');
    }
    public function payments()
    {
        return $this->hasMany('App\Models\Du\Payments', 'id_solicitud');
    }

    public function colony()
    {
        return $this->belongsTo(Colony::class, "id_colonia");
    }

    public function street()
    {
        return $this->belongsTo(Street::class, "id_calle");
    }

    public function cruce1()
    {
        return $this->belongsTo(Street::class, "id_cruce1");
    }

    public function cruce2()
    {
        return $this->belongsTo(Street::class, "id_cruce2");
    }

    public function population()
    {
        return $this->belongsTo(Population::class, "id_poblacion");
    }

    public function dictaminators()
    {
        return $this->belongsToMany('App\User', 'du_solicitud_dictaminador', 'id_solicitud', 'id_dictaminador')->withPivot('created_at');
    }

    public function inspectors()
    {
        return $this->belongsToMany('App\User', 'du_solicitud_inspector', 'id_solicitud', 'id_inspector')->withPivot('created_at');
    }

    public function finalDocuments()
    {
        return $this->hasMany('App\Models\Du\SolicitudDocumento', 'id_solicitud', 'id');
    }
    
    public function documentsFinished()
    {
        $flag = 0;
        foreach ($this->documents as $key => $document) {
            if(!$document->pivot->estatus)
            {
                $flag = 1;
            }
        }
        if($flag == 0)
        { return true;}
        else
        {return false;}
    }

    public function fullAddress()
    {
        $request = $this;

        $duRepository = new DuRepository;
        return $duRepository->getFullAddressByRequest($request->id);
    }

    function currentInspector()
    {
        $inspectors = $this->inspectors()->orderBy('pivot_created_at', 'desc')->get();

        if (!empty($inspectors))
        {
            return $inspectors->first();
        }else
        {
            return null;
        }
    }

    function currentDictaminator()
    {
        $dictaminators = $this->dictaminators()->orderBy('pivot_created_at', 'desc')->get();

        if (!empty($dictaminators))
        {
            return $dictaminators->first();
        }else
        {
            return null;
        }
    }

    public function pcRequirements()
    {
        return $this->hasMany('App\Models\Du\SolicitudPcRequirement', 'id_solicitud', 'id');
    }

    public function pcRequests()
    {
        return $this->hasMany('App\Models\Du\SolicitudPcRequest', 'id_solicitud', 'id');
    }

    public function pcRequirementStatus($pcRequirementId)
    {
        $request = $this;
        $status = 'Pendiente';

        $pcRequirement = $request->pcRequirements()->where('id_requisito', $pcRequirementId)->first();
        
        if (!empty($pcRequirement))
        {
            $status = 'Subido';
        }

        return $status;
    }

    public function pcRequirementStatusClass($pcRequirementId)
    {
        $request = $this;
        $class = 'text-white bg-warning';

        $pcRequirement = $request->pcRequirements()->where('id_requisito', $pcRequirementId)->first();
        if (!empty($pcRequirement))
        {
            $class = 'text-white bg-success';
        }

        return $class;
    }
}
