<?php

namespace App\Models\Du\CW;

use Illuminate\Database\Eloquent\Model;

class DuPoints extends Model
{
    protected $connection = 'cartoweb';
    protected $table = 'du_requests';
    public $timestamps = false;

    protected $fillable = [];
}
