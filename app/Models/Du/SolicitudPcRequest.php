<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class SolicitudPcRequest extends Model
{
    protected $table = 'du_solicitud_pc_solicitud';
    protected $connection = 'pgsql';
    protected $fillable = ['id_solicitud', 'id_pc_solicitud'];

    public function pcRequest()
    {
        return $this->belongsTo('App\Models\ProteccionCivil\Request', 'id_pc_solicitud', 'id');
    }

    public function duSolicitud()
    {
        return $this->belongsTo('App\Models\Du\Solicitud', 'id_solicitud', 'id');
    }
}
