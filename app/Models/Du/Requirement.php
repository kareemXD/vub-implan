<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    protected $table = "du_requerimientos";
}
