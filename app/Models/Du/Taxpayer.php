<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class Taxpayer extends Model
{
    protected $table = "du_contribuyentes";

    function nombreCompleto(){
        return $this->nombre.' '.$this->apellido_pat.' '.$this->apellido_mat;
    }
}
