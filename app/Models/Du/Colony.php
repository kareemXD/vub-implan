<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class Colony extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Colonias";
    protected $primaryKey = "IdColonia";
    public $timestamps = false;
    public $incrementing = false;
}
