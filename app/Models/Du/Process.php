<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $table = "du_tramites";
    protected $primaryKey = "id";

    public function requests()
    {
        return $this->belongsToMany('App\Models\Du\Solicitud', 'du_solicitud_tramite', 'tramite_id', 'solicitud_id');
    }

    public function requirements()
    {
        return $this->belongsToMany('App\Models\Du\Requirement', 'du_requerimientos_tramite', 'id_tramite', 'id_requerimiento');
    }
}
