<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class Concepto extends Model
{
    protected $table = "du_conceptos";

    public function ingresosConcepto()
    {
        return $this->belongsTo('App\Models\Du\Ingresos_Concepto', 'Concepto', 'Concepto');
    }
    
}
