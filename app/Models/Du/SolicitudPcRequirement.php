<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class SolicitudPcRequirement extends Model
{
    protected $table = 'du_solicitud_pc_requisitos';
    protected $connection = 'pgsql';
    protected $fillable = ['id_solicitud', 'id_requisito', 'documento'];
}
