<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "du_comentarios";

    public function user()
    {
        return $this->belongsTo('App\User', 'id_usuario', 'id');
    }
}
