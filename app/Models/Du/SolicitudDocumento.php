<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class SolicitudDocumento extends Model
{
    protected $table = 'du_solicitud_documentos';
    public $timestamps = false;
}
