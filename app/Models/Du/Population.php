<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;

class Population extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Poblaciones";
    protected $primaryKey = "IdPoblacion";
    public $timestamps = false;
    public $incrementing = false;

}
