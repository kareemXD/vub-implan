<?php

namespace App\Models\Du;

use Illuminate\Database\Eloquent\Model;
use App\Models\Du\Concepto;

class Payments extends Model
{
    protected $table = "du_payments";

    public function concept()
    {
        return $this->belongsTo('App\Models\Du\Concepto', 'concepto_id', 'id');
    }

    public function ingresosConcepto()
    {
        return $this->belongsTo('App\Models\Du\Ingresos_Concepto', 'concepto', 'Concepto');
    }
}
