<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = "menus";

    public function submenus()
    {
        return $this->hasMany('App\Models\Menu', 'padre', "id");
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Roledb', 'roles_has_menus', 'menu_id', 'rol_id');
    }
}
