<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Colony extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Colonias";
    protected $primaryKey = "IdColonia";
    public $timestamps = false;
    public $incrementing = false;

    public function location()
    {
        return $this->belongsTo('App\Models\Padron\Location', "IdPoblacion", "IdPoblacion");
    }
}
