<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "Calles";
    protected $primaryKey = "IdCalle";
    public $timestamps = false;
    public $incrementing = false;

    public function location()
    {
        return $this->belongsTo('App\Models\Padron\Location', "IdPoblacion", "IdPoblacion");
    }
}
