<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Repositories\PadronRepository;
use App\Models\Padron\CW\PLPoints;
use Illuminate\Support\Facades\DB;

class SyncronizePlData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pl:getData';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'obtener datos para actualizar el cartoweb de la tabla de padrón y licencias';

    /**
     * Create a new command instance.
     *
     * @return void
     */

     //variables globales
    private $padronRepository;
    
    public function __construct(PadronRepository $padronRepository)
    {
        $this->padronRepository = $padronRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $countUpdates = 0;
        $counCreate = 0;
        $licenses = $this->padronRepository->getLicensesWithPoints();
        if(count($licenses) > 0)
        {
            foreach($licenses as $index => $license)
            {
                $plpoints = PLPoints::where("licencia", $license->NumeroLicencia)->first();
                $adeudoTotal = $license->getTotalAdeudo();
                $debt_years = $license->getyearsDebt();

                if(!is_null($plpoints))
                {
                    $plpoints->superficie = ($license->Superficie) ? $license->Superficie : 0;
                    $plpoints->empleados = ($license->EmpleosCreados) ? $license->EmpleosCreados : 0;
                    $plpoints->poblacion = is_null($license->location())? '': $license->location()->NombrePoblacion;
                    $plpoints->colonia = is_null($license->colony())? '' : $license->colony()->NombreColonia;
                    $plpoints->domicilio = is_null($license->street())? '' : $license->street()->NombreCalle ;
                    $plpoints->calle1 = is_null($license->street1())? '' : $license->street1()->NombreCalle ;
                    $plpoints->calle2 = is_null($license->street2())? '' : $license->street2()->NombreCalle ;
                    $plpoints->numero_int = intval(($license->Interior) ? $license->Interior : 0);
                    $plpoints->numero_ext = intval(($license->Exterior) ? $license->Exterior : 0);
                    $plpoints->contribuyente_sexo = ($license->taxpayer->Sexo)? $license->taxpayer->Sexo : "O";
                    $plpoints->adeudo = $adeudoTotal;
                    $plpoints->giro = implode(", ",$license->turns()->pluck("Nombre")->toArray());
                    $plpoints->ubicacion = DB::raw("ST_GeomFromText('POINT(".$license->CoordenadaEste." ".$license->CoordenadaNorte.")', 32613)");
                    $plpoints->years_debt =  $debt_years;
                    $plpoints->save();
                    $countUpdates++;
                }else{
                    PLPoints::create([
                        'licencia' => $license->NumeroLicencia,
                        'nombre' => $license->NombreNegocio,
                        'fecha_alta' => $license->FechaAlta,
                        'superficie' => ($license->Superficie) ? $license->Superficie : 0,
                        'empleados' => ($license->EmpleosCreados) ? $license->EmpleosCreados : 0,
                        'poblacion' => is_null($license->location())? '': $license->location()->NombrePoblacion,
                        'colonia' => is_null($license->colony())? '' : $license->colony()->NombreColonia,
                        'domicilio' => is_null($license->street())? '' : $license->street()->NombreCalle,
                        'calle1' => is_null($license->street1())? '' : $license->street1()->NombreCalle,
                        'calle2' => is_null($license->street2())? '' : $license->street2()->NombreCalle,
                        'numero_int' => intval(($license->Interior) ? $license->Interior : 0),
                        'numero_ext' => intval(($license->Exterior) ? $license->Exterior : 0),
                        'contribuyente_sexo' => ($license->taxpayer->Sexo)? $license->taxpayer->Sexo : "O",
                        'adeudo' => $adeudoTotal,
                        'giro' => implode(", ",$license->turns()->pluck("Nombre")->toArray()),
                        'years_debt' => $debt_years,
                        'ubicacion' => DB::raw("ST_GeomFromText('POINT(".$license->CoordenadaEste." ".$license->CoordenadaNorte.")', 32613)"),
                    ]);
                    $counCreate++;
                }
            }    
        }

        $textReturn = "Listo.";
        if($counCreate > 0){
            $textReturn .= " Se registraron ".$counCreate." Licencias";

        }
        if($countUpdates > 0){
            $textReturn .= " Se actualizaron ".$countUpdates." Licencias";

        }
        dd($textReturn);
        
        
    }

}
