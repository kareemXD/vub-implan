<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use Auth;
use App\Models\Notifications;
use App\Models\Menu;
use App\Models\Roledb;
use App\Repositories\DuRepository;
use Spatie\Permission\Models\Role;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNewsNotifications()
    {
        return Notifications::where("user_id", Auth::user()->id)->where("estatus", 0)->get();
    }
    
    public function getAllNotifications()
    {
        return Notifications::where("user_id", Auth::user()->id)->get();
    }

    public function profile()
    {
        return $this->belongsTo('App\Models\Profile', 'perfil_id');
    }

    public function requestsAsDictaminator()
    {
        return $this->belongsToMany('App\Models\Du\Solicitud', 'du_solicitud_dictaminador', 'id_dictaminador', 'id_solicitud')->withPivot('created_at');
    }

    public function requestsAsInspector()
    {
        return $this->belongsToMany('App\Models\Du\Solicitud', 'du_solicitud_inspector', 'id_inspector', 'id_solicitud')->withPivot('created_at');
    }

    public function getMenu()
    {
        $user = Auth::user();
        $role_lv = Role::findByName($user->getRoleNames()[0]);
        $role = Roledb::find($role_lv->id);
        $navs = [];
        $arr_menu = [];

        foreach($role->menus()->orderBy("posicion")->get() as $menu)
        {
            $arr_menu[] = $menu->id;
        }

        $menus = Menu::whereNull('padre')->orderBy("posicion")->get();
        foreach ($menus as $key => $menu)
        {
            $data_nav = [
                'id' => $menu->id,
                'name' => $menu->nombre,
                'url' => $menu->url,
                'icon' => $menu->icono,
                'position' => $menu->posicion,
                'alias' => $menu->alias,
                'submenu' => []
            ];

            if(count($menu->submenus) > 0)
            {
                $hasPermissionMenu = false;

                foreach ($menu->submenus()->orderBy("posicion")->get() as $key => $submenu) 
                {
                    $sub_nav = [
                        'id' => $submenu->id,
                        'name' => $submenu->nombre,
                        'url' => $submenu->url,
                        'icon' => $submenu->icono,
                        'position' => $submenu->posicion,
                        'alias' => $submenu->alias,
                        'submenu' => []
                    ];

                    if(count($submenu->submenus) > 0)
                    {
                        $hasPermissionSubmenu = false;
                        foreach ($submenu->submenus()->orderBy("posicion")->get() as $key => $submenu2) 
                        {
                            if(in_array($submenu2->id, $arr_menu))
                            {
                                $sub2_nav = [
                                    'id' => $submenu2->id,
                                    'name' => $submenu2->nombre,
                                    'url' => $submenu2->url,
                                    'icon' => $submenu2->icono,
                                    'position' => $submenu2->posicion,
                                    'alias' => $submenu2->alias,
                                    'submenu' => []
                                ];

                                $hasPermissionSubmenu = true;
                                $sub_nav["submenu"][] = $sub2_nav;
                            }
                        }

                        if ($hasPermissionSubmenu)
                        {
                            $data_nav["submenu"][] = $sub_nav;
                        }
                    }else
                    {
                        if (in_array($submenu->id, $arr_menu))
                        {
                            $hasPermissionMenu = true;
                            $data_nav["submenu"][] = $sub_nav;
                        }
                    }
                }

                if ($hasPermissionMenu)
                {
                    $navs[] = $data_nav;
                }
            }else
            {
                if (in_array($menu->id, $arr_menu))
                {
                    $navs[] = $data_nav;
                }
            }
        }

        return $navs;
    }
}
