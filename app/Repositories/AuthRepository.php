<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;

//Models
use App\User;
use App\Models\Menu;
use App\Models\Roledb;
use App\Models\Position;
use App\Models\Profile;
use App\Models\Direction;

//Services
use Illuminate\Support\Facades\Hash;

//Plugins 
use Spatie\Permission\Models\Role;

class AuthRepository
{
	/******************************* Functions for Users Section ******************************/
	public function getAllUsers()
	{
		return User::all();
	}

	public function getAllDirections()
	{
		return Direction::all();
	}
	
	public function getActiveUsers()
	{
		return User::where('estatus', 1)->get();
	}

	public function getAllRoles()
	{
		return Role::all();
	}

	public function getAllPositions()
	{
		return Position::all();
	}

	public function deactivateUser($id)
	{
		$user = User::find($id);
		$user->estatus = 0;
		$user->save();
	}

	public function newUser($data)
	{
		if($data->puesto_name)
		{
			$position = new Position;
			$position->nombre = $data->puesto_name;
			$position->descripcion = $data->puesto_desc;
			$position->direccion_id = $data->puesto_direccion;
			$position->save();
			$position = $position->id;
		}else {
			$position = $data->position;
		}
		$profile = new Profile;
		$profile->nombre = $data->name;
		$profile->apellido_pat = $data->ape_pat;
		$profile->apellido_mat = $data->ape_mat;
		$profile->puesto_id = $position;
		$profile->save();

		$user = new User;
		$user->name = $data->name." ".$data->ape_pat;
		$user->email = $data->email;
		$user->perfil_id = $profile->id;
		$user->password = Hash::make($data->password);
		$user->save();

		$user->assignRole($data->role);
	}

	public function getUserById($id)
	{
		return User::find($id);
	}

	public function updateUserName($data)
	{
		
		$user = User::find($data->id);
		$profile = Profile::find($user->perfil_id);
		$profile->nombre = $data->name;
		$profile->apellido_pat = $data->ape_pat;
		$profile->apellido_mat = $data->ape_mat;
		$profile->puesto_id = $data->position;
		$profile->save();
		
		$user->name = $data->name." ".$data->ape_pat;
		$user->save();
		$user->removeRole($user->getRoleNames()[0]);
		$user->assignRole($data->role);
	}
	
	public function updateUser($data)
	{
		$user = User::find($data->id);
		$profile = Profile::find($user->perfil_id);
		$profile->nombre = $data->name;
		$profile->apellido_pat = $data->ape_pat;
		$profile->apellido_mat = $data->ape_mat;
		$profile->puesto_id = $data->position;
		$profile->save();

		$user->name = $data->name." ".$data->ape_pat;
		$user->password = Hash::make($data->password);
		$user->save();
		// $user->removeRole($user->getRoleNames()[0]);
		$user->assignRole($data->role);
	}
	
	public function updateProfileClient($data)
	{
		$user = User::find($data->id);
		$user->password = Hash::make($data->password);
		$user->save();
	}

	/******************************* Functions for Role Section ******************************/

	public function getAllMenu()
	{
		return Menu::whereNull('padre')->orderBy("posicion")->get();
	}

	public function storeRole($data)
	{
		// $role = Role::findByName($data->name);
		// $role->delete();
		// dd("Eliminado");
		$role = Role::create(['name' => $data->name]);
		$roledb = Roledb::find($role->id);

		foreach($data->sections as $key_section => $section)
		{
			$sectiondb = Menu::find($section);
			$roledb->menus()->attach($sectiondb->id);
			if($data->permissions[$key_section] == "1")
			{
				$role->givePermissionTo('read_'.$sectiondb->alias);

			}else if ($data->permissions[$key_section] == "2")
			{
				$role->givePermissionTo('write_'.$sectiondb->alias);
			}
		}
	}

	public function getRolById($id)
	{
		return Roledb::find($id);
	}

	public function getRolLavByName($name)
	{
		return Role::findByName($name);
	}

	public function updateRole($data)
	{
		$role = Role::find($data->role_id);
		$role->syncPermissions();
		$role->name = $data->name;
		$role->save();
		$roledb = Roledb::find($role->id);

		foreach($roledb->menus as $menu)
		{
			$roledb->menus()->detach($menu->id);
		}

		foreach($data->sections as $key_section => $section)
		{
			$sectiondb = Menu::find($section);
			$roledb->menus()->attach($sectiondb->id);
			if($data->permissions[$key_section] == "1")
			{
				$role->givePermissionTo('read_'.$sectiondb->alias);

			}else if ($data->permissions[$key_section] == "2")
			{
				$role->givePermissionTo('write_'.$sectiondb->alias);
			}
		}

		foreach ($roledb->customPermissions() as $permission)
		{
			$roledb->permissions()->detach($permission->id);
		}

		if (!empty($data->customPermissions))
		{
			foreach ($data->customPermissions as $customPermission)
			{
				$roledb->permissions()->attach($customPermission);
			}
		}
	}
}