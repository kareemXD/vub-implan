<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Storage;
//Models
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\User;
use App\Models\Padron\Taxpayer;
use App\Models\Padron\Turn;
use App\Models\Padron\TipoGiro;
use App\Models\Padron\SubTipoGiro;
use App\Models\Padron\RiesgoGiro;
use App\Models\Padron\ClaseGiro;
use App\Models\Padron\Concepto;
use App\Models\Padron\HistoricoGiro;
use App\Models\Padron\Document;
use App\Models\Padron\License;
use App\Models\Padron\Solicitud;
use App\Models\Street;
use App\Models\Colony;
use App\Models\Padron\Location;
use App\Models\Padron\Legend;
use App\Models\Padron\CW\PLPoints;
use App\Models\Padron\ProcessType;
use App\Models\Padron\Debts;
use App\Models\Padron\Alert;
use App\Models\Padron\BajasLicencia;
use App\Models\Padron\LicenciaBasura;
use App\Models\Padron\LicensePcRequest;
use App\Models\Padron\Payment;
use App\Models\Padron\SolicitudLicenciaBasura;
use App\Models\Padron\Trash;
use App\Models\ProteccionCivil\Request as PcRequest;
use App\Repositories\ProteccionCivil\FormalityRepository;

class PadronRepository
{
    /*****************************************************************************
    * *
    * * Functions used for administration of Taxpayers
    * *
    *****************************************************************************/

    public function getAllTaxpayers($data = null, $pdf = null)
    {
        $taxpayers = Taxpayer::orderBy("IdContribuyente","DESC");
        if(array_key_exists("cuenta", $data) && $data["cuenta"]){
            $taxpayers->where("IdContribuyente", "like", $data["cuenta"]."%");
        }
        if(array_key_exists("nombre", $data) && $data["nombre"]){
            $taxpayers->where("Nombre", "like", "%".$data["nombre"]."%");
        }
        if(array_key_exists("ape_paterno", $data) && $data["ape_paterno"]){
            $taxpayers->where("ApellidoPaterno", "like", "%".$data["ape_paterno"]."%");
        }
        if(array_key_exists("ape_materno", $data) && $data["ape_materno"]){
            $taxpayers->where("ApellidoMaterno", "like", "%".$data["ape_materno"]."%");
        }
        if(array_key_exists("rfc", $data) && $data["rfc"]){
            $taxpayers->where("RFC", "like", "%".$data["rfc"]."%");
        }
        if(array_key_exists("colonia", $data) && $data["colonia"]){
            $taxpayers->where("Colonia", "like", "%".$data["colonia"]."%");
        }
        if(array_key_exists("fecha_alta", $data) && $data["fecha_alta"] ){
            $date = explode(" - ",$data["fecha_alta"]);
            $initial = str_replace(" ", "T", Carbon::create($date[0]." 00:00:00")->toDateTimeString());
            $final = str_replace(" ", "T", Carbon::create($date[1]." 23:59:59")->toDateTimeString());
            // dd($date, $initial, $final);
            $taxpayers->whereBetween ("FechaAlta", [$initial, $final]);
        }
        if($pdf)
        {
            return $taxpayers->get();
        }else {
            return $taxpayers->paginate(15);
        }
    }

    public function getTaxpayersBySearch($search)
    {
        $taxpayers = DB::connection('sqlsrv')
                        ->table('PadronContribuyentes')
                        ->where(function($query) use($search) {
                            $query->where('NombreCompleto', 'like', "%$search%")
                                    ->orWhere('IdContribuyente', 'like', "%$search%");
                        })
                        ->distinct()
                        ->select('IdContribuyente as id', 'NombreCompleto as text')
                        ->limit(15)
                        ->get();

        return $taxpayers;
    }

    public function storeTaypayer($data)
    {
        $now = Carbon::now("America/Mexico_City")->format("Y-m-d H:m:s");
        $now = str_replace(" ", "T", $now);
        // dd($now, $data);
        $old = Taxpayer::orderBy("IdContribuyente","DESC")->first();
        $taxpayer = new Taxpayer;
        $taxpayer->IdContribuyente = $old->IdContribuyente + 1;
        $taxpayer->Nombre = $data->nombre;
        $taxpayer->ApellidoPaterno = is_null($data->apellido_pat)? "" : $data->apellido_pat;
        $taxpayer->ApellidoMaterno = is_null($data->apellido_mat)? "" : $data->apellido_mat;
        $taxpayer->Sexo = $data->sexo;
        $taxpayer->PersonaMoral = (isset($data->persona)? 1 : 0);
        $taxpayer->RFC = is_null($data->rfc)? "XXXX000000XXX": $data->rfc;
        $taxpayer->Domicilio = $data->domicilio;
        $taxpayer->Exterior = is_null($data->exterior)? "": $data->exterior;
        $taxpayer->Interior = $data->interior;
        $taxpayer->Colonia = $data->colonia;
        $taxpayer->Cruce1 = $data->calle1;
        $taxpayer->Cruce2 = $data->calle2;
        $taxpayer->Ciudad = $data->localidad;
        $taxpayer->CP = $data->cp;
        $taxpayer->Municipio = "BAHIA DE BANDERAS";
        $taxpayer->Estado = "NAYARIT";
        $taxpayer->Pais = "MÉXICO";
        $taxpayer->TelefonoCasa = $data->telefono_casa;
        $taxpayer->TelefonoTrabajo = $data->telefono_trab;
        $taxpayer->Email = is_null($data->email)? "" : $data->email;
        $taxpayer->FechaAlta = $now;
        $taxpayer->ClaveUsuario = Auth::user()->id;
        $taxpayer->IdSistema = 7;
        $taxpayer->FechaModificacion = $now;
        $taxpayer->save();
    }

    function getAllDebts(){
        return Debts::all();
    }
    
    public function storeUpdateTaypayer($data)
    {
        $now = Carbon::now("America/Mexico_City")->format("Y-M-d H:m:s");
        // $now = str_replace(" ", "T", $now);
        $taxpayer = Taxpayer::find($data->id);
        $taxpayer->Nombre = $data->update_nombre;
        $taxpayer->ApellidoPaterno = is_null($data->update_apellido_pat)? "" : $data->update_apellido_pat;
        $taxpayer->ApellidoMaterno = is_null($data->update_apellido_mat)? "" : $data->update_apellido_mat;
        $taxpayer->Sexo = $data->update_sexo;
        $taxpayer->PersonaMoral = (isset($data->update_persona)? 1 : 0);
        $taxpayer->RFC = $data->update_rfc;
        $taxpayer->Domicilio = $data->update_domicilio;
        $taxpayer->Exterior = is_null($data->update_exterior)? "" : $data->update_exterior;
        $taxpayer->Interior = $data->update_interior;
        $taxpayer->Colonia = $data->update_colonia;
        $taxpayer->Cruce1 = $data->update_calle1;
        $taxpayer->Cruce2 = $data->update_calle2;
        $taxpayer->Ciudad = $data->update_localidad;
        $taxpayer->CP = $data->update_cp;
        $taxpayer->TelefonoCasa = $data->update_telefono_casa;
        $taxpayer->TelefonoTrabajo = $data->update_telefono_trab;
        $taxpayer->Email = $data->update_email;
        $taxpayer->FechaModificacion = $now;
        $taxpayer->save();
    }

    public function getTaxpayerData($id)
    {
        return Taxpayer::find($id);
    }

    public function getTaxpayerLicenses($data)
    {
        $licenses = License::Select("Licencias.*")->join("PadronContribuyentes", "PadronContribuyentes.IdContribuyente", "=", "Licencias.IdContribuyente")->where("Licencias.IdContribuyente", $data->taxpayerId)->has("requests")->get();

        return $licenses;
    }

    /*****************************************************************************
    * *
    * * Functions used for administration of turns
    * *
    *****************************************************************************/
    public function getAllTiposGiro()
    {
        return TipoGiro::all();
    }
    
    public function getAllSubTiposGiro()
    {
        return SubTipoGiro::all();
    }

    public function getAllRiesgosGiros()
    {
        return RiesgoGiro::all();
    }

    public function getAllClasesGiros()
    {
        return ClaseGiro::all();
    }

    public function getAllConceptos()
    {
        return Concepto::orderBy('Concepto', 'ASC')->get();
    }

    public function getAllTurns($data = null, $pdf = null)
    {
        $taxpayers = Turn::orderBy("IdGiro","DESC");
        if(array_key_exists("cuenta", $data) && $data["cuenta"]){
            $taxpayers->where("IdGiro", "like", $data["cuenta"]."%");
        }
        if(array_key_exists("nombre", $data) && $data["nombre"]){
            $taxpayers->where("Nombre", "like", "%".$data["nombre"]."%");
        }
        if(array_key_exists("clave", $data) && $data["clave"]){
            $taxpayers->where("CveHacienda", "=", $data["clave"]);
        }
        if(array_key_exists("concepto", $data) && $data["concepto"]){
            $taxpayers->where("Concepto", "=", $data["concepto"]);
        }

        if($pdf)
        {
            return $taxpayers->get();
        }else {
            return $taxpayers->paginate(15);
        }
    }
    
    public function getAllTurnsReport()
    {
        return Turn::orderBy("IdGiro","DESC")->get();
    }

    public function getTurnById($id)
    {
        return Turn::find($id);
    }

    public function getTurnsBySearch($search)
    {
        $turns = DB::connection('sqlsrv')
                        ->table('Giros')
                        ->where(function($query) use($search) {
                            $query->where('Nombre', 'like', "%$search%")
                                    ->orWhere('IdGiro', 'like', "%$search%");
                        })
                        ->distinct()
                        ->select('IdGiro as id', 'Nombre as text')
                        ->limit(15)
                        ->get();

        return $turns;
    }

    public function getAllDocuments()
    {
        return Document::all();
    }
    
    public function getTurnConfig($turn)
    {
        $data = [];
        foreach ($turn->documents as $key => $document) {
            
            if($document->pivot->Alta)
            {$data["check_".$document->Requisito."_al"] = true;}
            if($document->pivot->CambioDom)
            {$data["check_".$document->Requisito."_cd"] = true;}
            if($document->pivot->CambioGiroAnun)
            {$data["check_".$document->Requisito."_cg"] = true;}
            if($document->pivot->Traspaso)
            {$data["check_".$document->Requisito."_tr"] = true;}
            if($document->pivot->ReposicionLicencia)
            {$data["check_".$document->Requisito."_rl"] = true;}
            if($document->pivot->Baja)
            {$data["check_".$document->Requisito."_ba"] = true;}
        }
        return $data;
    }

    public function storeTurn($data)
    {
        // dd($data);
        $now = Carbon::now("America/Mexico_City")->format("Y");
        $old = Turn::orderBy("IdGiro", "DESC")->first();

        $turn = new Turn;
        $turn->IdGiro = $old->IdGiro + 1;
        $turn->Nombre = $data->name;
        $turn->Giro = $old->IdGiro + 1;
        $turn->CuotaFija = $data->cuota_fija;
        $turn->SubTipoGiro = $data->subtipo;
        $turn->TipoGiro = $data->tipo;
        $turn->Riesgo = $data->riesgo;
        $turn->IdClase = $data->clase;
        $turn->CveHacienda = $data->claveHacienda;
        $turn->Concepto = $data->concepto;
        $turn->Alcohol = $data->alcohol;
        $turn->save();

        $concepto_refrendo = $turn->concepts()->where("TiposTramite.TipoTramite", "R")->first();
        if (empty($concepto_refrendo))
        {
            $turn->concepts()->attach('R', [
                'Concepto' => $turn->Concepto
            ]);
        }

        $cuota = new HistoricoGiro;
        $cuota->IdGiro = $old->IdGiro + 1;
        $cuota->Ano = $now;
        $cuota->CuotaFija = $data->cuota_fija;
        $cuota->CuotaMinima = $data->cuota_minima;
        // $cuota->CuotaMaxima = $data->cuota_maxima;
        $cuota->Valorsindescuento = 0.00;
        $cuota->save();

        $turn = $this->getTurnById($old->IdGiro + 1);

        foreach ($this->getAllDocuments() as $key => $document) {
            if( $data["check_".$document->Requisito."_al"] || 
                $data["check_".$document->Requisito."_cd"] || 
                $data["check_".$document->Requisito."_cg"] || 
                $data["check_".$document->Requisito."_tr"] || 
                $data["check_".$document->Requisito."_rl"] || 
                $data["check_".$document->Requisito."_ba"])
            {
                // dd($data, $document);
                $turn->documents()->attach($document->Requisito, [  'Alta' => ($data["check_".$document->Requisito."_al"])? 1 : 0, 
                                                                    'CambioDom' => ($data["check_".$document->Requisito."_cd"])? 1 : 0, 
                                                                    'CambioGiroAnun' => ($data["check_".$document->Requisito."_cg"])? 1 : 0, 
                                                                    'Traspaso' => ($data["check_".$document->Requisito."_tr"])? 1 : 0,
                                                                    'ReposicionLicencia' => ($data["check_".$document->Requisito."_rl"])? 1 : 0,
                                                                    'Baja' => ($data["check_".$document->Requisito."_ba"])? 1 : 0,
                                                                    'Dependencia' => 0
                                                            ]);
            }
        }
    }
    
    public function storeUpdateTurn($data)
    {
        $turn = $this->getTurnById($data->id);
        $turn->Nombre = $data->name;
        $turn->CuotaFija = $data->cuota_fija;
        $turn->SubTipoGiro = $data->subtipo;
        $turn->TipoGiro = $data->tipo;
        $turn->Riesgo = $data->riesgo;
        $turn->IdClase = $data->clase;
        $turn->CveHacienda = $data->claveHacienda;
        $turn->Concepto = $data->concepto;
        $turn->Alcohol = ($data->alcohol == "1")? true: false;
        $turn->save();

        $now = Carbon::now("America/Mexico_City")->format("Y");
        $cuota = HistoricoGiro::where("IdGiro", $data->id)->where("Ano", $now)->update([
            "CuotaFija" => $data->cuota_fija,
            "CuotaMinima" => $data->cuota_minima,
            // "CuotaMaxima" => $data->cuota_maxima,
        ]);

        $turn->documents()->detach();
        foreach ($this->getAllDocuments() as $key => $document) {
            if( $data["check_".$document->Requisito."_al"] || 
                $data["check_".$document->Requisito."_cd"] || 
                $data["check_".$document->Requisito."_cg"] || 
                $data["check_".$document->Requisito."_tr"] || 
                $data["check_".$document->Requisito."_rl"] || 
                $data["check_".$document->Requisito."_ba"])
            {
                // dd($data, $document);
                $turn->documents()->attach($document->Requisito, [  'Alta' => ($data["check_".$document->Requisito."_al"])? 1 : 0, 
                                                                    'CambioDom' => ($data["check_".$document->Requisito."_cd"])? 1 : 0, 
                                                                    'CambioGiroAnun' => ($data["check_".$document->Requisito."_cg"])? 1 : 0, 
                                                                    'Traspaso' => ($data["check_".$document->Requisito."_tr"])? 1 : 0,
                                                                    'ReposicionLicencia' => ($data["check_".$document->Requisito."_rl"])? 1 : 0,
                                                                    'Baja' => ($data["check_".$document->Requisito."_ba"])? 1 : 0,
                                                                    'Dependencia' => 0
                                                            ]);
            }
        }

        $concepto_refrendo = $turn->concepts()->where("TiposTramite.TipoTramite", "R")->first();
        if (empty($concepto_refrendo))
        {
            $turn->concepts()->attach('R', [
                'Concepto' => $turn->Concepto
            ]);
        }
    }

    public function updatePeriod($data)
    {
        $turn = $this->getTurnById($data->id);
        $turn->CuotaFija = $data->cuota_fija;
        $turn->save();

        $cuota = HistoricoGiro::where("IdGiro", $data->id)->where("Ano", $data->period)->update([
            "CuotaFija" => $data->cuota_fija,
            "CuotaMinima" => $data->cuota_minima,
            // "CuotaMaxima" => $data->cuota_maxima,
        ]);
    }

    /*****************************************************************************
    * *
    * * Functions used for administration of Requests
    * *
    *****************************************************************************/

    public function getAllRequests($data = null, $pdf = null)
    {
        // dd($data);
        $requests = Solicitud::Select("SolicitudLicencia.*")->join("PadronContribuyentes", "PadronContribuyentes.IdContribuyente", "=", "SolicitudLicencia.IdContribuyente");
        if(empty($data))
        {
            $requests->where("Aplicado", 0);
        }
        if(array_key_exists("estatus", $data)  && $data["estatus"] != "t"){
            if($data["estatus"] == 1 || $data["estatus"] == 0)
            $requests->where("Aplicado", $data["estatus"]);
        }
        if(array_key_exists("cuenta", $data) && $data["cuenta"]){
            $requests->where("SolicitudLicencia.NumeroSolicitud", "=", $data["cuenta"]);
        }
        if(array_key_exists("id_contribuyente", $data) && $data["id_contribuyente"]){
            $requests->where("SolicitudLicencia.IdContribuyente", $data["id_contribuyente"]);
        }
        if(array_key_exists("contribuyente", $data) && $data["contribuyente"]){
            $requests->where("PadronContribuyentes.NombreCompleto", "like", "%".$data["contribuyente"]."%");
        }
        if(array_key_exists("nombre", $data) && $data["nombre"]){
            $requests->where("SolicitudLicencia.NombreNegocio", "like", "%".$data["nombre"]."%");
        }
        if(array_key_exists("fecha_alta", $data) && $data["fecha_alta"] ){
            $date = explode(" - ",$data["fecha_alta"]);
            $initial = str_replace(" ", "T", Carbon::create($date[0]." 00:00:00")->toDateTimeString());
            $final = str_replace(" ", "T", Carbon::create($date[1]." 23:59:59")->toDateTimeString());
            // dd($date, $initial, $final);
            $requests->whereBetween ("SolicitudLicencia.FechaSolicitud", [$initial, $final]);
        }
        $requests->orderBy("NumeroSolicitud","DESC");
        if($pdf)
        {
            return $requests->get();
        }else {
            return $requests->paginate(15);
        }
    }

    public function getLicenseCost($data)
    {
        $turn = $this->getTurnById($data->id);
        $year = Carbon::parse($data->date)->format("Y");
        $now = Carbon::now("America/Mexico_City")->format("Y");
        $trimester = $this->getActualTrimester($data->date);
        $cost = 0;
        // dd($turn);
        if($year == $now){
            $cost += $turn->CuotaFija * ($trimester["percentage"] / 100);
        }else{
            $cost += $turn->CuotaFija * ($trimester["percentage"] / 100);
            /*
            foreach ($turn->periods() as $key => $period) {
                if($period->Ano == $year){
                    $cost += $period->CuotaFija * ($trimester["percentage"] / 100);
                }elseif($period->Ano > $year && $period->Ano <= $now){
                    $cost += $period->CuotaFija;
                }
            }
            $cost += $turn->CuotaFija;
            */
        }
        return $cost;
    }

    public function getLicenseCostDetails($data)
    {
        $turn = $this->getTurnById($data->id);
        $date = Carbon::parse($data->date);
        $year = Carbon::parse($data->date)->format("Y");
        $trimester = $this->getActualTrimester($data->date);
        $cost = 0;
        $basura = false;
        $refrendo = $data->refrendo;
        $quantity = $data->quantity;
        if (empty($quantity))
        {
            $quantity = 1;
        }

        $info = [
            'turn' => $turn,
            'alcohol' => ($turn->Alcohol == 1)? true : false,
            'trash' => $basura,
            'data' => []
        ];
        if (!empty($refrendo))
        {
            $date->day = 1;
            $date->month = 1;

            // Refrendo
            $period = $turn->getPeriod($year);
            
            if (!empty($period))
            {
                $cost += $period->CuotaMinima;
            }else
            {
                $cost += $turn->CuotaFija;
            }

            $info['data'][] = [
                "type" => 2, // 1 = Apertura, 2 = Refrendo, 3 = Descarge Basura, 4 Recoleccion Basura
                "id" => $turn->IdGiro,
                "nombre" => $turn->Nombre,
                "date" => $date->format('Y-m-d'),
                "observation" => "Costo Refrendo ". $year,
                "cost" => str_replace(',','', number_format($cost, 2)),
                "quantity" => str_replace(',','', number_format($quantity, 2)),
                "subtotal" => str_replace(',','', number_format((float)($cost * $quantity), 2)),
                "refrendo" => 1,
            ];
        }else
        {
            $period = $turn->getPeriod($year);

            if (!empty($period))
            {
                $cost += $period->CuotaFija * ($trimester["percentage"] / 100);
            }else
            {
                $cost += $turn->CuotaFija * ($trimester["percentage"] / 100);
            }

            $info['data'][] = [
                "type" => 1, // 1 = Apertura, 2 = Refrendo
                "id" => $turn->IdGiro,
                "nombre" => $turn->Nombre,
                "date" => $date->format('Y-m-d'),
                "observation" => "Costo Apertura ". $year,
                "cost" => str_replace(',','', number_format($cost, 2)),
                "quantity" => str_replace(',','', number_format($quantity, 2)),
                "subtotal" => str_replace(',','', number_format((float)($cost * $quantity), 2)),
                "refrendo" => 0,
            ];
        }
        
        return $info;
    }

    public function getRequestById($id)
    {
        return Solicitud::find($id);
    }

    public function getDocumentByRequest($solicitud, $requisito)
    {
        $request = Solicitud::find($solicitud);
        $document = $request->documents()->where(DB::raw("Requisitos.Requisito"), $requisito)->first();
        return $document->getOriginal();
    }

    public function updateDocumentRequest($data)
    {
        $request = Solicitud::find($data->solicitud_id);
        $document = Document::find($data->requisito_id);

        if($data->operation == "store"){
            if(isset($data->document))
            {
                $file = $data->document;
                $ex = $file->getClientOriginalExtension();
                $name = $this->limpiarCaracteresEspeciales($document->Nombre);
                $name = strtolower($name);
                $name = str_replace(" ", "_", $name);
                if(Storage::disk('public')->putFileAs('documentacion_solicitudes/'.$request->NumeroSolicitud, $file, $name.".".$ex))
                {
                    $request->documents()->updateExistingPivot($document->Requisito, ['Comentario' => $data->comments, 'Archivo' => $name.".".$ex, 'Estatus' => 1]);
                }
            }else {
                $request->documents()->updateExistingPivot($document->Requisito, ['Comentario' => $data->comments]);
            }
            return $request;
        }elseif ($data->operation == "update") {
            $request->documents()->updateExistingPivot($document->Requisito, ['Verificacion' => $data->value]);
            return $request->documentsVerifid();
        }
            
    }

    public function updatePcRequirementRequest($data)
    {   
        $request = $this->getRequestById($data->solicitud_id);

        if (!empty($data->document))
        {
            // Revisar si ya cuenta con el mismo requisito para actualizarlo.
            $requestRequirement = $request->pcRequirements()->where('id_requisito', $data->requisito_id)->first();

            // Subir el documento nuevo y obtener la url.
            $fileName = $request->NumeroSolicitud.'-'.Str::random(10).'.'.$data->file('document')->extension();
            $data->file('document')->storeAs('public/padron/requests/'.$request->NumeroSolicitud.'/requirements', $fileName);
            $fileUrl = 'padron/requests/'.$request->NumeroSolicitud.'/requirements/'.$fileName;

            // Validar que haya un requisito anterior para eliminarlo.
            if (!empty($requestRequirement))
            {
                // Validar que exista el archivo antes de quererlo eliminar.
                if (Storage::disk('public')->exists($requestRequirement->documento))
                {
                    Storage::disk('public')->delete($requestRequirement->documento);
                }

                // Solo actualizar la url del documento.
                $requestRequirement->documento = $fileUrl;
                $requestRequirement->save();
            }else
            {
                // Generar el nuevo registro del requisito.
                $request->pcRequirements()->create([
                    'id_requisito' => $data->requisito_id,
                    'documento' => $fileUrl,
                ]);
            }
        }

        return $request;
    }

    public function requestPcFormalityStore($data)
    {
        $request = $this->getRequestById($data->solicitud_id);

        $lastPCRequest = PcRequest::orderBy('id', 'desc')->first();
        $numeroSolicitud = config('system.pc.requests.initialFolio'); // Contador Inicial
        if (!empty($lastPCRequest))
        {
            $numeroSolicitud = (int)$lastPCRequest->numero_solicitud;
            $numeroSolicitud = str_pad(($numeroSolicitud + 1), 4, '0', STR_PAD_LEFT);
        }

        $taxPayer = $request->taxpayer;

        $turnText = "";
        foreach ($request->turns as $key => $turn)
        {
            if ($key == 0)
            {
                $turnText .= $turn->Nombre;
            }else
            {
                $turnText .= ', '.$turn->Nombre;
            }
        }
        
        $PCRequest = new PcRequest;
        $PCRequest->numero_solicitud = $numeroSolicitud;
        $PCRequest->id_poblacion = $request->IdPoblacion;
        $PCRequest->id_colonia = $request->IdColonia;
        $PCRequest->id_calle = $request->IdCalle;
        $PCRequest->id_cruce1 = $request->IdCruce1;
        $PCRequest->id_cruce2 = $request->IdCruce2;
        $PCRequest->no_exterior = $request->Exterior;
        $PCRequest->no_interior = $request->Interior;
        $PCRequest->coordenada_este = $request->CoordenadaEste;
        $PCRequest->coordenada_norte = $request->CoordenadaNorte;
        $PCRequest->fecha_solicitud = now()->format('Y-m-d');
        $PCRequest->nombre = $taxPayer->NombreCompleto;
        $PCRequest->giro = $turnText;
        $PCRequest->rfc = null;
        $PCRequest->no_licencia = null;
        $PCRequest->estatus_licencia = 'Nueva Licencia';
        $PCRequest->nombre_comercial = $request->NombreNegocio;
        $PCRequest->no_empleados = $request->EmpleosCreados;
        $PCRequest->horario_laboral = null;
        $PCRequest->nombre_solicitante = $taxPayer->NombreCompleto;
        $PCRequest->caracter_juridico_solicitante = null;
        $PCRequest->telefono_celular = null;
        $PCRequest->telefono_fijo = null;
        $PCRequest->email = null;
        $PCRequest->otro_tramite = null;
        $PCRequest->origen = 'Padrón y Licencias';
        $PCRequest->estatus = config('system.pc.requests.statuses.pending'); // Pendiente
        $PCRequest->save();

        // Agregar tramite de visto bueno a la solicitud
        $formalityId = 1;
        $formalityRepository = new FormalityRepository; 
        $formality = $formalityRepository->getFormalityById($formalityId);
        if (!empty($formality))
        {
            $formalityCost = $formality->costByYear(now()->year);
            $PCRequest->formalities()->create([
                'id_tramite' => $formalityId,
                'costo' => $formalityCost->costo,
                'year' => $formalityCost->year
            ]);
        }

        // Asociar requisitos a la solicitud
        if (!empty($request->pcRequirements))
        {
            foreach ($request->pcRequirements as $pcRequirement)
            {
                $PCRequest->requirements()->create([
                    'id_requisito' => $pcRequirement->id_requisito,
                    'documento' => $pcRequirement->documento,
                ]);
            }
        }

        $request->pcRequests()->create([
            'id_pc_solicitud' => $PCRequest->id
        ]);

        return $PCRequest;
    }

    public function updatePcRequirementLicense($data)
    {   
        $license = $this->getLicenseById($data->licencia_id);

        if (!empty($data->document))
        {
            // Revisar si ya cuenta con el mismo requisito para actualizarlo.
            $licensePcRequirement = $license->pcRequirements()->where('id_requisito', $data->requisito_id)->first();

            // Subir el documento nuevo y obtener la url.
            $fileName = $license->NumeroLicencia.'-'.Str::random(10).'.'.$data->file('document')->extension();
            $data->file('document')->storeAs('public/padron/licenses/'.$license->NumeroLicencia.'/requirements', $fileName);
            $fileUrl = 'padron/licenses/'.$license->NumeroLicencia.'/requirements/'.$fileName;

            // Validar que haya un requisito anterior para eliminarlo.
            if (!empty($licensePcRequirement))
            {
                // Validar que exista el archivo antes de quererlo eliminar.
                if (Storage::disk('public')->exists($licensePcRequirement->documento))
                {
                    Storage::disk('public')->delete($licensePcRequirement->documento);
                }

                // Solo actualizar la url del documento.
                $licensePcRequirement->documento = $fileUrl;
                $licensePcRequirement->save();
            }else
            {
                // Generar el nuevo registro del requisito.
                $license->pcRequirements()->create([
                    'id_requisito' => $data->requisito_id,
                    'documento' => $fileUrl,
                ]);
            }
        }

        return $license;
    }

    public function licensePcFormalityStore($data)
    {
        $license = $this->getLicenseById($data->licencia_id);

        $lastPCRequest = PcRequest::orderBy('id', 'desc')->first();
        $numeroSolicitud = config('system.pc.requests.initialFolio'); // Contador Inicial
        if (!empty($lastPCRequest))
        {
            $numeroSolicitud = (int)$lastPCRequest->numero_solicitud;
            $numeroSolicitud = str_pad(($numeroSolicitud + 1), 4, '0', STR_PAD_LEFT);
        }

        $taxPayer = $license->taxpayer;

        $turnText = "";
        foreach ($license->turns as $key => $turn)
        {
            if ($key == 0)
            {
                $turnText .= $turn->Nombre;
            }else
            {
                $turnText .= ', '.$turn->Nombre;
            }
        }
        
        $PCRequest = new PcRequest;
        $PCRequest->numero_solicitud = $numeroSolicitud;
        $PCRequest->id_poblacion = $license->IdPoblacion;
        $PCRequest->id_colonia = $license->IdColonia;
        $PCRequest->id_calle = $license->IdCalle;
        $PCRequest->id_cruce1 = $license->IdCruce1;
        $PCRequest->id_cruce2 = $license->IdCruce2;
        $PCRequest->no_exterior = $license->Exterior;
        $PCRequest->no_interior = $license->Interior;
        $PCRequest->coordenada_este = $license->CoordenadaEste;
        $PCRequest->coordenada_norte = $license->CoordenadaNorte;
        $PCRequest->fecha_solicitud = now()->format('Y-m-d');
        $PCRequest->nombre = $taxPayer->NombreCompleto;
        $PCRequest->giro = $turnText;
        $PCRequest->rfc = null;
        $PCRequest->no_licencia = $license->NumeroLicencia;
        $PCRequest->estatus_licencia = 'Refrendo';
        $PCRequest->nombre_comercial = $license->NombreNegocio;
        $PCRequest->no_empleados = $license->EmpleosCreados;
        $PCRequest->horario_laboral = null;
        $PCRequest->nombre_solicitante = $taxPayer->NombreCompleto;
        $PCRequest->caracter_juridico_solicitante = null;
        $PCRequest->telefono_celular = null;
        $PCRequest->telefono_fijo = null;
        $PCRequest->email = null;
        $PCRequest->otro_tramite = null;
        $PCRequest->origen = 'Padrón y Licencias';
        $PCRequest->estatus = config('system.pc.requests.statuses.pending'); // Pendiente
        $PCRequest->save();

        // Agregar tramite de visto bueno a la solicitud
        $formalityId = 1;
        $formalityRepository = new FormalityRepository; 
        $formality = $formalityRepository->getFormalityById($formalityId);
        if (!empty($formality))
        {
            $formalityCost = $formality->costByYear(now()->year);
            $PCRequest->formalities()->create([
                'id_tramite' => $formalityId,
                'costo' => $formalityCost->costo,
                'year' => $formalityCost->year
            ]);
        }

        // Asociar requisitos a la solicitud
        if (!empty($license->pcRequirements))
        {
            foreach ($license->pcRequirements as $pcRequirement)
            {
                $PCRequest->requirements()->create([
                    'id_requisito' => $pcRequirement->id_requisito,
                    'documento' => $pcRequirement->documento,
                ]);
            }
        }

        $license->pcRequests()->create([
            'id_pc_solicitud' => $PCRequest->id
        ]);

        return $PCRequest;
    }
    
    function limpiarCaracteresEspeciales($cadena){
		
		//Reemplazamos la A y a
		$cadena = str_replace(
		array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
		array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
		$cadena
		);
 
		//Reemplazamos la E y e
		$cadena = str_replace(
		array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
		array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
		$cadena );
 
		//Reemplazamos la I y i
		$cadena = str_replace(
		array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
		array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
		$cadena );
 
		//Reemplazamos la O y o
		$cadena = str_replace(
		array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
		array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
		$cadena );
 
		//Reemplazamos la U y u
		$cadena = str_replace(
		array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
		array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
		$cadena );
 
		//Reemplazamos la N, n, C y c
		$cadena = str_replace(
		array('Ñ', 'ñ', 'Ç', 'ç'),
		array('N', 'n', 'C', 'c'),
		$cadena
		);
		
		return $cadena;
    }
    
    public function getActualTrimester($date = null)
    {
        if($date){
            $now = Carbon::parse($date);
        }else{
            $now = Carbon::now("America/Mexico_City");
        }
        $last = Carbon::create($now->format("Y"), 1, 0, 0, 0, 0, 'America/Mexico_City');
        $tri_1 = Carbon::create($now->format("Y"), 4, 30, 23, 59, 59, 'America/Mexico_City');
        $tri_2 = Carbon::create($now->format("Y"), 8, 31, 23, 59, 59, 'America/Mexico_City');
        $tri_3 = Carbon::create($now->format("Y"), 12, 31, 23, 59, 59, 'America/Mexico_City');

        if ($now > $last && $now <= $tri_1) {
            $trimester = [
                'number' => 1,
                'label' => "Primer Cuatrimestre del ". $now->format("Y"),
                'percentage' => 100
            ];
        }else if ($now > $tri_1 && $now <= $tri_2) {
            $trimester = [
                'number' => 2,
                'label' => "Segundo Cuatrimestre del ". $now->format("Y"),
                'percentage' => 70
            ];
        }else if ($now > $tri_2 && $now <= $tri_3) {
            $trimester = [
                'number' => 3,
                'label' => "Tercer Cuatrimestre del ". $now->format("Y"),
                'percentage' => 35
            ];
        }

        return $trimester;
    }

    /*****************************************************************************
    * *
    * * Functions used for administration of Licenses
    * *
    *****************************************************************************/

    public function getAllLicenses($data = null, $pdf = null)
    {
        $license = License::Select("Licencias.*")->join("PadronContribuyentes", "PadronContribuyentes.IdContribuyente", "=", "Licencias.IdContribuyente")
        ->orderBy(DB::raw("Cast(Licencias.NumeroLicencia as INT)"),"DESC");
        if(array_key_exists("cuenta", $data) && $data["cuenta"]){
            $license->where("Licencias.NumeroLicencia", "=", $data["cuenta"]);
        }
        if(array_key_exists("id_contribuyente", $data) && $data["id_contribuyente"]){
            $license->where("Licencias.IdContribuyente", $data["id_contribuyente"]);
        }
        if(array_key_exists("contribuyente", $data) && $data["contribuyente"]){
            $license->where("PadronContribuyentes.NombreCompleto", "like", "%".$data["contribuyente"]."%");
        }
        if(array_key_exists("nombre", $data) && $data["nombre"]){
            $license->where("Licencias.NombreNegocio", "like", "%".$data["nombre"]."%");
        }
        if(array_key_exists("fecha_alta", $data) && $data["fecha_alta"] ){
            $date = explode(" - ",$data["fecha_alta"]);
            $initial = str_replace(" ", "T", Carbon::create($date[0]." 00:00:00")->toDateTimeString());
            $final = str_replace(" ", "T", Carbon::create($date[1]." 23:59:59")->toDateTimeString());
            // dd($date, $initial, $final);
            $license->whereBetween ("Licencias.FechaAlta", [$initial, $final]);
        }

        //calles 
        if(array_key_exists("calle", $data) && $data["calle"]){
            $license->whereRaw("Licencias.IdCalle in  (select IdCalle from Calles where NombreCalle like '%".$data["calle"]."%')");
        }
        
        if(array_key_exists("exterior", $data) && $data["exterior"]){
            $license->where("Licencias.Exterior", $data["exterior"]);
        }

        if(array_key_exists("interior", $data) && $data["interior"]){
            $license->where("Licencias.Interior", $data["interior"]);
        }

        if(array_key_exists("colonia", $data) && $data["colonia"]){
            $license->whereRaw("Licencias.IdColonia in  (select IdColonia from Colonias where NombreColonia like '%".$data["colonia"]."%')");
        }

        if(array_key_exists("poblacion", $data) && $data["poblacion"]){
            $license->whereRaw("Licencias.IdPoblacion in  (select IdPoblacion from Poblaciones where NombrePoblacion like '%".$data["poblacion"]."%')");
        }

        if(array_key_exists("giros_list", $data) && $data["giros_list"]){
            $license->whereRaw("Licencias.NumeroLicencia in  (select GiroDetalleLic.NumeroLicencia from GiroDetalleLic where GiroDetalleLic.IdGiro in (select IdGiro from Giros where Giros.Nombre like '%".$data["giros_list"]."%')  )");
        }

        if($pdf){
            return $license->limit(500)->get();
        }else {
            return $license->paginate(15);
        }
    }

    public function getCanceledLicenses($data = null, $pdf = null){
        //agregar FolioAnterior
        $license = BajasLicencia::Select(
            "BajasLicencias.NumeroLicencia",
            "BajasLicencias.IdContribuyente","BajasLicencias.FechaAlta", "BajasLicencias.FechaSolicitud", "BajasLicencias.FolioLicencia",DB::raw("'0000000000' as FolioAnterior"), "BajasLicencias.NombreNegocio",
            "BajasLicencias.Linderos", "BajasLicencias.Observaciones", "BajasLicencias.IdColonia", "BajasLicencias.IdCalle", "BajasLicencias.Exterior", "BajasLicencias.Interior", "BajasLicencias.IdCruce1", "BajasLicencias.IdCruce2",
            "BajasLicencias.IdZona", "BajasLicencias.IdPoblacion", "BajasLicencias.Superficie", "BajasLicencias.EmpleosCreados", "BajasLicencias.FechaImpresion", "BajasLicencias.FechaTramite", "BajasLicencias.ClaveUsuario",
             "BajasLicencias.CoordenadaEste", "BajasLicencias.CoordenadaNorte", "BajasLicencias.PermisoAlcohol", "BajasLicencias.CausaBaja", "BajasLicencias.FechaBaja", "BajasLicencias.Comentario"
         )->join("PadronContribuyentes", "PadronContribuyentes.IdContribuyente", "=", "BajasLicencias.IdContribuyente")
         ->orderBy(DB::raw("Cast(BajasLicencias.NumeroLicencia as INT)"),"DESC");
        if(array_key_exists("cuenta", $data) && $data["cuenta"]){
            
            $license->where("BajasLicencias.NumeroLicencia", "=", $data["cuenta"]);
        }
        if(array_key_exists("id_contribuyente", $data) && $data["id_contribuyente"]){
            $license->where("BajasLicencias.IdContribuyente", $data["id_contribuyente"]);
        }
        if(array_key_exists("contribuyente", $data) && $data["contribuyente"]){
            $license->where("PadronContribuyentes.NombreCompleto", "like", "%".$data["contribuyente"]."%");
        }
        if(array_key_exists("nombre", $data) && $data["nombre"]){
            $license->where("BajasLicencias.NombreNegocio", "like", "%".$data["nombre"]."%");
        }
        if(array_key_exists("fecha_alta", $data) && $data["fecha_alta"] ){
            $date = explode(" - ",$data["fecha_alta"]);
            $initial = str_replace(" ", "T", Carbon::create($date[0]." 00:00:00")->toDateTimeString());
            $final = str_replace(" ", "T", Carbon::create($date[1]." 23:59:59")->toDateTimeString());
            // dd($date, $initial, $final);
            $license->whereBetween ("BajasLicencias.FechaAlta", [$initial, $final]);
        }

        if($pdf){
            return $license->has("requests")->get();
        }else {
            return $license->has("requests")->paginate(15);
        }
    }

    public function getAllPoblaciones()
    {
        return Location::orderBy('NombrePoblacion', 'ASC')->get();
    }
    
    public function getAllZonas()
    {
        return DB::connection("sqlsrv")->table("Zonas")->orderBy("IdZona", "ASC")->get();
    }
    
    public function getColoniesByCity($id)
    {
        return DB::connection("sqlsrv")->table("Colonias")->where("IdPoblacion", $id)->orderBy("IdColonia", "ASC")->get();
    }
    
    public function getCallesByColony($id)
    {
        return DB::connection("sqlsrv")->table("Calles")->where("IdPoblacion", $id)->orderBy("IdCalle", "ASC")->get();
    }

    public function makeLicense($data)
    {
        DB::beginTransaction();
        //try {
            $request = Solicitud::find($data->id);
            if($request->documentsVerifid() == "true")
            {
                $now = str_replace(" ", "T", Carbon::now("America/Mexico_city")->toDateTimeString());
                $old = License::select(DB::raw("Cast(NumeroLicencia as INT) as Licencia"))->orderBy("Licencia","DESC")->first();
                $NumeroLicencia = str_pad($old->Licencia + 1, 10, "0", STR_PAD_LEFT);

                $license = new License;
                $license->NumeroLicencia = $NumeroLicencia;
                $license->IdContribuyente = $request->IdContribuyente;
                $license->FechaAlta = $request->FechaAlta;
                $license->FechaSolicitud = str_replace(" ", "T", $request->FechaSolicitud);
                $license->FolioLicencia = is_null($request->FolioLicencia)? 1 : $request->FolioLicencia;
                $license->FolioAnterior = $request->FolioAnterior;
                $license->NombreNegocio = $request->NombreNegocio;
                $license->Linderos = $request->Linderos;
                $license->Observaciones = $request->Observaciones;
                $license->IdColonia = $request->IdColonia;
                $license->IdCalle = $request->IdCalle;
                $license->Exterior = is_null($request->Exterior)? "": $request->Exterior;
                $license->Interior = $request->Interior;
                $license->IdCruce1 = $request->IdCruce1;
                $license->IdCruce2 = $request->IdCruce2;
                $license->IdZona = $request->IdZona;
                $license->IdPoblacion = $request->IdPoblacion;
                $license->Superficie = $request->Superficie;
                $license->EmpleosCreados = $request->EmpleosCreados;
                $license->FechaImpresion = str_replace(" ", "T", $request->FechaImpresion);
                $license->FechaTramite = str_replace(" ", "T", $request->FechaTramite);
                $license->CoordenadaEste = $request->CoordenadaEste;
                $license->CoordenadaNorte = $request->CoordenadaNorte;
                $license->ClaveUsuario = "015";
                $license->PermisoAlcohol = $request->PermisoAlcohol;
                $license->NumeroTarjetonAlcoholes = $request->NumeroTarjetonAlcoholes;
                $license->save();
                
                foreach ($request->legends as $key => $legend) {
                    $license->legends()->attach($legend,['FolioLicencia' => 1]);
                }

                if(!is_null($data->cuenta_serach))
                {
                    foreach ($data->cuenta_serach as $key => $turn_id) 
                    {
                        $i = $key + 1;
                        $turn = $this->getTurnById($turn_id);
                        $obs = $data->observations_search[$key];
                        $quantityTurn = (float)$data->quantity_search[$key];
                        $adeudo = (float)$data->cost_search[$key] * $quantityTurn;

                        $license->turns()->attach($turn->IdGiro, ['Secuencia' => $i, 
                                                                    'Fecha' => str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()), 
                                                                    'Descripcion' => $turn->Nombre, 
                                                                    'Observacion' => $obs, 
                                                                    'Cantidad' => $quantityTurn, 
                                                                    'Cuota' => $adeudo,
                                                                    'Refrendo' => $data->type[$key] == 2? 1 : 0, 
                                                                    'LicenciaAnexo' => 0]);
            
                        $year = Carbon::parse(str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()))->format("Y");
                        $conpto = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $turn->Concepto)->first();
                        // $description = substr($conpto->Descripcion.' ('.$turn->Nombre.')', 0, 99);
                        $description = $conpto->Descripcion;
        
                        Debts::create([
                            "Departamento" => "02",
                            "Concepto" => strval($turn->Concepto),
                            "CuentaDepto" => strval($license->NumeroLicencia),
                            "Referencia" => $year.'-'.$turn->IdGiro,
                            "FechaMovto" => now()->format('Y-m-d').' 00:00:00',
                            "NumeroLicencia" => strval($license->NumeroLicencia),
                            "FechaVence" => now()->format('Y-m-d').' 00:00:00',
                            "Descripcion" => $description,
                            "Importe" => $adeudo,
                            "Estado" => "A",
                            "Referencia2" => ($turn->Alcohol == 1) ? 'Alcohol' : '',
                            "Cargo" => 1,
                        ]);
                    }
                }
                
                // Add Basura
                $request->garbages()->delete();
                Debts::where(function($query) use($license) {
                    $query->where('CuentaDepto', strval($license->NumeroLicencia))
                            ->where('Departamento', '02')
                            ->where('Referencia2', 'Basura');
                })
                ->delete();
                
                if(!empty($data->garbage_year))
                {
                    foreach ($data->garbage_year as $key => $garbageYear)
                    {
                        $reference = $key + 1;
                        $lastRecord = LicenciaBasura::orderBy('Id', 'desc')->first();
                        $id = 1;
                        if (!empty($lastRecord))
                        {
                            $id = $lastRecord->Id + 1;
                        }

                        $licenciaBasura = new LicenciaBasura;
                        $licenciaBasura->Id = $id;
                        $licenciaBasura->Year = $garbageYear;
                        $licenciaBasura->Concepto = $data->garbage_concept[$key];
                        $licenciaBasura->Descripcion = config('system.padron.garbage.concepts.'.$data->garbage_concept[$key]);
                        $licenciaBasura->Importe = (float)$data->garbage_amount[$key];
                        $licenciaBasura->LicenciaId = $license->NumeroLicencia;
                        $licenciaBasura->save();

                        Debts::create([
                            "Departamento" => "02",
                            "Concepto" => strval($licenciaBasura->Concepto),
                            "CuentaDepto" => strval($NumeroLicencia),
                            "Referencia" => $licenciaBasura->Year.'-'.$reference,
                            "FechaMovto" => now()->format('Y-m-d').' 00:00:00',
                            "NumeroLicencia" => strval($NumeroLicencia),
                            "FechaVence" => now()->format('Y-m-d').' 00:00:00',
                            "Descripcion" => $licenciaBasura->Descripcion,
                            "Importe" => $licenciaBasura->Importe,
                            "Estado" => "A",
                            "Referencia2" => 'Basura',
                            "Cargo" => 1,
                        ]);
                    }
                }
        
                $request->Aplicado = 1;
                $request->NumeroLicencia = $NumeroLicencia;
                $request->save();

                /*
                    Proteccion Civil
                */
                // Asociar las solicitudes de proteccion civil a la nueva licencia para el historial
                if (!empty($request->pcRequests))
                {
                    foreach ($request->pcRequests as $pcRequest)
                    {
                        $license->pcRequests()->create([
                            'id_pc_solicitud' => $pcRequest->id_pc_solicitud,
                        ]);
                    }
                }

                // Asociar los requisitos de la solicitud a la licencia
                if (!empty($request->pcRequirements))
                {
                    foreach ($request->pcRequirements as $pcRequirement)
                    {
                        $license->pcRequirements()->create([
                            'id_requisito' => $pcRequirement->id_requisito,
                            'documento' => $pcRequirement->documento
                        ]);
                    }
                }

                DB::commit();
                return $license;
            }
            return false;
       // } catch (\Illuminate\Database\QueryException $ex) {
            // dd($ex);
           // DB::rollback();
            //return false;
        //}
    }

    public function getAllLegends(){
        return Legend::orderBy("IdLeyenda","DESC")->get();
    }

    public function storeLicense($data){
        //funciones para registrar Calles y Colinias nuevas en caso de no Existir
        
        $street = $data->domicilio2;
        $street1 = $data->calle1_u;
        $street2 = $data->calle2_u;
        $colony1 = $data->colony;
        // dd($data);
        if(isset($data->newcolony) && !empty($data->newcolony)){
            //verificamos que la colonia no haya sido registrada anteriormente
            $checkIfColonyExists = Colony::where(DB::raw("upper(NombreColonia)"),strtoupper($data->newcolony))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfColonyExists))
            {
                $old = Colony::orderBy("IdColonia","DESC")->first();
                $colony = new Colony;
                $colony->IdColonia = $old->IdColonia + 1;
                $colony->NombreColonia = strtoupper($data->newcolony);
                $colony->NombreComun = strtoupper($data->newcolony);
                $colony->IdMunicipio = 1;
                $colony->IdPoblacion = $data->city;
                $colony->Colonos = "N";
                $colony->PorcentajeRefrendo = 0;
                $colony->Controlado = 0;
                $colony->save();
            }else{
                $colony = $checkIfColonyExists;
            }
            $colony1 = intval($colony->IdColonia);
        }
        
        if(isset($data->newdomicilio2) && !empty($data->newdomicilio2)){
            //verificamos que el domicilio no se ha registrado
            $checkIfStreetExist = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newdomicilio2))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfStreetExist))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle = new Street;
                $calle->IdCalle = $old->IdCalle + 1;
                $calle->NombreCalle = strtoupper($data->newdomicilio2);
                $calle->AliasCalle = strtoupper($data->newdomicilio2);
                $calle->NombreOficial = strtoupper($data->newdomicilio2);
                $calle->IdMunicipio = 1;
                $calle->IdPoblacion = $data->city;
                $calle->save();
            }else{
                $calle = $checkIfStreetExist;
            }
            $street = intval($calle->IdCalle);

        }

        if(isset($data->newcalle1_u) && !empty($data->newcalle1_u)){

            $checkIfNewCalle1Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle1_u))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfNewCalle1Exists))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle1 = new Street;
                $calle1->IdCalle = $old->IdCalle + 1;
                $calle1->NombreCalle = strtoupper($data->newcalle1_u);
                $calle1->AliasCalle = strtoupper($data->newcalle1_u);
                $calle1->NombreOficial = strtoupper($data->newcalle1_u);
                $calle1->IdMunicipio = 1;
                $calle1->IdPoblacion = $data->city;
                $calle1->save();
            }else{
                $calle1 = $checkIfNewCalle1Exists;
            }
            
            $street1 = intval($calle1->IdCalle);

        }

        if(isset($data->newcalle2_u) && !empty($data->newcalle2_u)){
            $checkIfNewCalle2Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle2_u))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfNewCalle2Exists))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle2 = new Street;
                $calle2->IdCalle = $old->IdCalle + 1;
                $calle2->NombreCalle = strtoupper($data->newcalle2_u);
                $calle2->AliasCalle = strtoupper($data->newcalle2_u);
                $calle2->NombreOficial = strtoupper($data->newcalle2_u);
                $calle2->IdMunicipio = 1;
                $calle2->IdPoblacion = $data->city;
                $calle2->save();
            }else{
                $calle2 = $checkIfNewCalle2Exists;
            }

            $street2 = intval($calle2->IdCalle);
        }

        // Seccion en la que se realiza el registro de la Solicitud
        $now = str_replace(" ", "T", Carbon::now("America/Mexico_city")->toDateTimeString());
        $old = Solicitud::orderBy("NumeroSolicitud","DESC")->first();
        $license = new Solicitud;
        $license->NumeroSolicitud = $old->NumeroSolicitud + 1;
        $license->IdContribuyente = $data->idtaxpayer;
        $license->FechaSolicitud = $now;
        $license->FolioLicencia = 1;
        $license->FolioAnterior = 1;
        $license->NombreNegocio = $data->name_b;
        $license->Linderos = $data->linderos;
        $license->Observaciones = $data->observations;
        $license->IdColonia = $colony1;
        $license->IdCalle = $street;
        $license->Exterior = is_null($data->ext)? "": $data->ext;
        $license->Interior = $data->int;
        $license->IdCruce1 = $street1;
        $license->IdCruce2 = $street2;
        $license->IdZona = $data->zone;
        $license->IdPoblacion = $data->city;
        $license->Superficie = $data->surface;
        $license->EmpleosCreados = intval($data->employees);
        $license->FechaImpresion = $now;
        $license->ClaveUsuario = "015";
        $license->Estatus = "C";
        $license->Aplicado = "0";
        $license->CoordenadaEste = $data->este;
        $license->CoordenadaNorte = $data->norte;
        $license->PermisoAlcohol = $data->alcohol;
        $license->FechaAlta = $data->alta;
        $license->NumeroTarjetonAlcoholes = $data->tarjeton_alcohol;
        $license->save();
        // Agregamos el tipo de tramite
        // En una licensia nueva siempe sera "N"
        $license->process()->attach("N");

        // Agregamos las leyendas a la Solicitud
        if(!empty($data->legends))
        {
            foreach ($data->legends as $key => $legend) {
                $license->legends()->attach($legend);
            }
        }

        //eliminar repetidos
        // $checkRepeat = [];
        // $data_cuenta_serach = $data->cuenta_serach;
        // $data_observations_search = $data->observations_search;
        // $data_quantity_search = $data->quantity_search;
        // $data_date_search = $data->date_search;
        // $turnRepeatKey[] = 0;
        // foreach ($data_cuenta_serach as $key => $giro){
        //     if(in_array($giro, $checkRepeat)){
        //         unset($data_cuenta_serach[$key]);
        //         unset($data_observations_search[$key]);
        //         unset($data_quantity_search[$key]);
        //         if($data_date_search[$key]  < $data_date_search[$turnRepeatKey[$giro]]){
        //             $data_date_search[$turnRepeatKey[$giro]] = $data_date_search[$key];
        //         }
        //         unset($data_date_search[$key]);
        //     }else{
        //         $checkRepeat[] = $giro;
        //         $turnRepeatKey[$giro] = $key;
        //     }
        // }

        // $data->cuenta_serach= $data_cuenta_serach;
        // $data->observations_search = $data_observations_search;
        // $data->quantity_search = $data_quantity_search;
        // $data->date_search = $data_date_search;
        //fin


        //Agregamos los giros a la Solicitud y obtenemos los documentos
        $documents = [];
        foreach ($data->cuenta_serach as $key => $giro) {
            $i = $key + 1;
            $turn = Turn::find($giro);
            $adeudo = (float)$data->cost_search[$key] * (float)$data->quantity_search[$key];

            $license->turns()->attach($giro, ['Secuencia' => $i, 
                                                'Fecha' => str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()), 
                                                'Descripcion' => $turn->Nombre, 
                                                'Observacion' => $data->observations_search[$key], 
                                                'Cuota' => $adeudo, 
                                                'Cantidad' => $data->quantity_search[$key], 
                                                'Refrendo' => $data->type[$key] == 2? 1 : 0,
                                                'LicenciaAnexo' => 0]);

            foreach ($turn->documents()->wherePivot("Alta", 1)->get() as $key => $document) {
                $documents[$document->Requisito] = $document->Requisito;
            }
        }

        //Agregamos los documentos
        foreach ($documents as $key => $document) {
            $license->documents()->attach($document);
        }

        // Basura
        $license->garbages()->delete();
        if(!empty($data->garbage_year))
        {
            foreach ($data->garbage_year as $key => $garbageYear)
            {
                $lastRecord = SolicitudLicenciaBasura::orderBy('Id', 'desc')->first();
                $id = 1;
                if (!empty($lastRecord))
                {
                    $id = $lastRecord->Id + 1;
                }

                $solicitudLicenciaBasura = new SolicitudLicenciaBasura;
                $solicitudLicenciaBasura->Id = $id;
                $solicitudLicenciaBasura->Year = $garbageYear;
                $solicitudLicenciaBasura->Concepto = $data->garbage_concept[$key];
                $solicitudLicenciaBasura->Descripcion = config('system.padron.garbage.concepts.'.$data->garbage_concept[$key]);
                $solicitudLicenciaBasura->Importe = (float)$data->garbage_amount[$key];
                $solicitudLicenciaBasura->SolicitudId = $license->NumeroSolicitud;
                $solicitudLicenciaBasura->save();
            }
        }

        //Retornamos la solicitud para realizar el redireccionamoento.
        return $license;
    }
    
    public function storeUpdateRequest($data)
    {
        $now = str_replace(" ", "T", Carbon::now("America/Mexico_city")->toDateTimeString());

        //actualizar o agregar calles
        $street = $data->domicilio2;
        $street1 = $data->calle1_u;
        $street2 = $data->calle2_u;
        $colony1 = $data->colony;
        // dd($data);
        if(isset($data->newcolony) && !empty($data->newcolony)){
            //verificamos que la colonia no haya sido registrada anteriormente
            $checkIfColonyExists = Colony::where(DB::raw("upper(NombreColonia)"),strtoupper($data->newcolony))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfColonyExists))
            {
                $old = Colony::orderBy("IdColonia","DESC")->first();
                $colony = new Colony;
                $colony->IdColonia = $old->IdColonia + 1;
                $colony->NombreColonia = strtoupper($data->newcolony);
                $colony->NombreComun = strtoupper($data->newcolony);
                $colony->IdMunicipio = 1;
                $colony->IdPoblacion = $data->city;
                $colony->Colonos = "N";
                $colony->PorcentajeRefrendo = 0;
                $colony->Controlado = 0;
                $colony->save();
            }else{
                $colony = $checkIfColonyExists;
            }
            $colony1 = intval($colony->IdColonia);
        }
        
        if(isset($data->newdomicilio2) && !empty($data->newdomicilio2)){
            //verificamos que el domicilio no se ha registrado
            $checkIfStreetExist = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newdomicilio2))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfStreetExist))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle = new Street;
                $calle->IdCalle = $old->IdCalle + 1;
                $calle->NombreCalle = strtoupper($data->newdomicilio2);
                $calle->AliasCalle = strtoupper($data->newdomicilio2);
                $calle->NombreOficial = strtoupper($data->newdomicilio2);
                $calle->IdMunicipio = 1;
                $calle->IdPoblacion = $data->city;
                $calle->save();
            }else{
                $calle = $checkIfStreetExist;
            }
            $street = intval($calle->IdCalle);

        }

        if(isset($data->newcalle1_u) && !empty($data->newcalle1_u)){

            $checkIfNewCalle1Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle1_u))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfNewCalle1Exists))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle1 = new Street;
                $calle1->IdCalle = $old->IdCalle + 1;
                $calle1->NombreCalle = strtoupper($data->newcalle1_u);
                $calle1->AliasCalle = strtoupper($data->newcalle1_u);
                $calle1->NombreOficial = strtoupper($data->newcalle1_u);
                $calle1->IdMunicipio = 1;
                $calle1->IdPoblacion = $data->city;
                $calle1->save();
            }else{
                $calle1 = $checkIfNewCalle1Exists;
            }
            
            $street1 = intval($calle1->IdCalle);

        }

        if(isset($data->newcalle2_u) && !empty($data->newcalle2_u)){
            $checkIfNewCalle2Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle2_u))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfNewCalle2Exists))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle2 = new Street;
                $calle2->IdCalle = $old->IdCalle + 1;
                $calle2->NombreCalle = strtoupper($data->newcalle2_u);
                $calle2->AliasCalle = strtoupper($data->newcalle2_u);
                $calle2->NombreOficial = strtoupper($data->newcalle2_u);
                $calle2->IdMunicipio = 1;
                $calle2->IdPoblacion = $data->city;
                $calle2->save();
            }else{
                $calle2 = $checkIfNewCalle2Exists;
            }

            $street2 = intval($calle2->IdCalle);
        }

        $request = Solicitud::find($data->id);
        $request->IdContribuyente = $data->idtaxpayer;
        $request->NombreNegocio = $data->name_b;
        $request->Linderos = $data->linderos;
        $request->Observaciones = $data->observations;
        $request->IdColonia = $colony1;
        $request->IdCalle = $street;
        $request->Exterior = is_null($data->ext)? "" : $data->ext;
        $request->Interior = $data->int;
        $request->IdCruce1 = $street1;
        $request->IdCruce2 = $street2;
        $request->IdZona = $data->zone;
        $request->IdPoblacion = $data->city;
        $request->Superficie = $data->surface;
        $request->EmpleosCreados = intval($data->employees);
        $request->CoordenadaEste = $data->este;
        $request->CoordenadaNorte = $data->norte;
        $request->FechaAlta = $data->alta;
        $request->NumeroTarjetonAlcoholes = $data->tarjeton_alcohol;
        $request->save();

        $request->turns()->detach();
        $documents = [];
        //eliminar repetidos
        // $checkRepeat = [];
        // $data_cuenta_serach = $data->cuenta_serach;
        // $data_observations_search = $data->observations_search;
        // $data_quantity_search = $data->quantity_search;
        // $data_date_search = $data->date_search;
        // $turnRepeatKey[] = 0;
        // foreach ($data_cuenta_serach as $key => $giro){
        //     if(in_array($giro, $checkRepeat)){
        //         unset($data_cuenta_serach[$key]);
        //         unset($data_observations_search[$key]);
        //         unset($data_quantity_search[$key]);
        //         if($data_date_search[$key]  < $data_date_search[$turnRepeatKey[$giro]]){
        //             $data_date_search[$turnRepeatKey[$giro]] = $data_date_search[$key];
        //         }
        //         unset($data_date_search[$key]);
        //     }else{
        //         $checkRepeat[] = $giro;
        //         $turnRepeatKey[$giro] = $key;
        //     }
        // }

        // dd($data->all());

        // $data->cuenta_serach= $data_cuenta_serach;
        // $data->observations_search = $data_observations_search;
        // $data->quantity_search = $data_quantity_search;
        // $data->date_search = $data_date_search;
        //fin
        foreach ($data->cuenta_serach as $key => $giro) {
            $i = $key + 1;
            $turn = Turn::find($giro);
            $adeudo = (float)$data->cost_search[$key] * (float)$data->quantity_search[$key];
            
            $request->turns()->attach($giro, ['Secuencia' => $i, 
                                              'Fecha' => str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()), 
                                              'Descripcion' => $turn->Nombre, 
                                              'Observacion' => $data->observations_search[$key], 
                                              'Cuota' => $adeudo,
                                              'Cantidad' => $data->quantity_search[$key], 
                                              'Refrendo' => $data->type[$key] == 2? 1 : 0, 
                                              'LicenciaAnexo' => 0]);

            foreach ($turn->documents()->wherePivot("Alta", 1)->get() as $key => $document) {
                $documents[$document->Requisito] = $document->Requisito;
            }
        }
        
        foreach ($documents as $key => $document) {
            if($request->documents()->where("Requisitos.Requisito", $document)->first() == null){
                $request->documents()->attach($document);
            }
        }

        // Basura
        $request->garbages()->delete();
        if(!empty($data->garbage_year))
        {
            foreach ($data->garbage_year as $key => $garbageYear)
            {
                $lastRecord = SolicitudLicenciaBasura::orderBy('Id', 'desc')->first();
                $id = 1;
                if (!empty($lastRecord))
                {
                    $id = $lastRecord->Id + 1;
                }

                $solicitudLicenciaBasura = new SolicitudLicenciaBasura;
                $solicitudLicenciaBasura->Id = $id;
                $solicitudLicenciaBasura->Year = $garbageYear;
                $solicitudLicenciaBasura->Concepto = $data->garbage_concept[$key];
                $solicitudLicenciaBasura->Descripcion = config('system.padron.garbage.concepts.'.$data->garbage_concept[$key]);
                $solicitudLicenciaBasura->Importe = (float)$data->garbage_amount[$key];
                $solicitudLicenciaBasura->SolicitudId = $request->NumeroSolicitud;
                $solicitudLicenciaBasura->save();
            }   
        }

        return $request;
    }

    public function getLicenseById($id)
    {
        return License::find($id);
    }

    public function storeUpdateLicenseLocation($data)
    {
          //actualizar o agregar calles
          $street = $data->domicilio2;
          $street1 = $data->calle1_u;
          $street2 = $data->calle2_u;
          $colony1 = $data->colony;
          // dd($data);
          if(isset($data->newcolony) && !empty($data->newcolony)){
              //verificamos que la colonia no haya sido registrada anteriormente
              $checkIfColonyExists = Colony::where(DB::raw("upper(NombreColonia)"),strtoupper($data->newcolony))->where("IdPoblacion", $data->city)->first();
              if(is_null($checkIfColonyExists))
              {
                  $old = Colony::orderBy("IdColonia","DESC")->first();
                  $colony = new Colony;
                  $colony->IdColonia = $old->IdColonia + 1;
                  $colony->NombreColonia = strtoupper($data->newcolony);
                  $colony->NombreComun = strtoupper($data->newcolony);
                  $colony->IdMunicipio = 1;
                  $colony->IdPoblacion = $data->city;
                  $colony->Colonos = "N";
                  $colony->PorcentajeRefrendo = 0;
                  $colony->Controlado = 0;
                  $colony->save();
              }else{
                  $colony = $checkIfColonyExists;
              }
              $colony1 = intval($colony->IdColonia);
          }
          
          if(isset($data->newdomicilio2) && !empty($data->newdomicilio2)){
              //verificamos que el domicilio no se ha registrado
              $checkIfStreetExist = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newdomicilio2))->where("IdPoblacion", $data->city)->first();
              if(is_null($checkIfStreetExist))
              {
                  $old = Street::orderBy("IdCalle","DESC")->first();
                  $calle = new Street;
                  $calle->IdCalle = $old->IdCalle + 1;
                  $calle->NombreCalle = strtoupper($data->newdomicilio2);
                  $calle->AliasCalle = strtoupper($data->newdomicilio2);
                  $calle->NombreOficial = strtoupper($data->newdomicilio2);
                  $calle->IdMunicipio = 1;
                  $calle->IdPoblacion = $data->city;
                  $calle->save();
              }else{
                  $calle = $checkIfStreetExist;
              }
              $street = intval($calle->IdCalle);
  
          }
  
          if(isset($data->newcalle1_u) && !empty($data->newcalle1_u)){
  
              $checkIfNewCalle1Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle1_u))->where("IdPoblacion", $data->city)->first();
              if(is_null($checkIfNewCalle1Exists))
              {
                  $old = Street::orderBy("IdCalle","DESC")->first();
                  $calle1 = new Street;
                  $calle1->IdCalle = $old->IdCalle + 1;
                  $calle1->NombreCalle = strtoupper($data->newcalle1_u);
                  $calle1->AliasCalle = strtoupper($data->newcalle1_u);
                  $calle1->NombreOficial = strtoupper($data->newcalle1_u);
                  $calle1->IdMunicipio = 1;
                  $calle1->IdPoblacion = $data->city;
                  $calle1->save();
              }else{
                  $calle1 = $checkIfNewCalle1Exists;
              }
              
              $street1 = intval($calle1->IdCalle);
  
          }
  
          if(isset($data->newcalle2_u) && !empty($data->newcalle2_u)){
              $checkIfNewCalle2Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle2_u))->where("IdPoblacion", $data->city)->first();
              if(is_null($checkIfNewCalle2Exists))
              {
                  $old = Street::orderBy("IdCalle","DESC")->first();
                  $calle2 = new Street;
                  $calle2->IdCalle = $old->IdCalle + 1;
                  $calle2->NombreCalle = strtoupper($data->newcalle2_u);
                  $calle2->AliasCalle = strtoupper($data->newcalle2_u);
                  $calle2->NombreOficial = strtoupper($data->newcalle2_u);
                  $calle2->IdMunicipio = 1;
                  $calle2->IdPoblacion = $data->city;
                  $calle2->save();
              }else{
                  $calle2 = $checkIfNewCalle2Exists;
              }
  
              $street2 = intval($calle2->IdCalle);
          }
  
          /*
          $street = $data->domicilio2;
          $street1 = $data->calle1_u;
          $street2 = $data->calle2_u;
          $colony1 = $data->colony;
          */

        $license = License::find($data->id);
        if(!is_null($colony1)){
            $license->IdColonia = $colony1;
        }
        if(!is_null($street) || $street!=0){
            $license->IdCalle = $street;
        }
        $license->Exterior = is_null($data->ext)? "" : $data->ext;
        $license->Interior = $data->int;
        $license->IdCruce1 = $street1;
        $license->IdCruce2 = $street2;
        if(!is_null($data->zone)){
            $license->IdZona = $data->zone;
        }
        if(!is_null($data->city)){
            $license->IdPoblacion = $data->city;
        }
        $license->CoordenadaEste = $data->este;
        $license->CoordenadaNorte = $data->norte;
        $license->save();
        
        
        return $license;
    }

    public function addTurnRequest($data)
    {
        //ampliación de giro
        $now = str_replace(" ", "T", Carbon::now("America/Mexico_city")->toDateTimeString());
        $license = License::find($data->id);

        //actualizar solicitud
        $request = $license->requests()->first();
        $request->Linderos = $data->linderos;
        $request->Observaciones = $data->observations;
        $request->Superficie = $data->surface;
        $request->EmpleosCreados = intval($data->employees);
        $request->save();

        $license->Linderos = $request->Linderos;
        $license->Observaciones = $request->Observaciones;
        $license->Superficie = $request->Superficie;
        $license->EmpleosCreados = $request->EmpleosCreados;
        $license->save();
        $license->legends()->detach();

        // Agregamos las leyendas a la Solicitud
        if(!is_null($data->legends))
        {
            foreach ($data->legends as $key => $legend) {
                $license->legends()->attach($legend, ['FolioLicencia' => 1]);
            }
        }

        //se eliminan las diferencias
        if(!is_null($request->turns())){
            $cuenta_search_array = array_diff($data->cuenta_serach,$request->turns()->pluck("Giros.IdGiro")->toArray());

        }else{
            $cuenta_search_array = $data->cuenta_serach;
        }

        if(count($cuenta_search_array) == 0){
            $cuenta_search_array = $data->cuenta_serach;
        }
        
        $documents = [];
        foreach ($cuenta_search_array as $key => $giro) 
        {
            $i = $key + 1;
            $turn = Turn::find($giro);
            $giroDetalleSol  = $request->giroDetalles()->where("IdGiro", $giro)->first();

            if(is_null($giroDetalleSol))
            {
                $request->turns()->attach($giro, ['Secuencia' => $i, 
                'Fecha' => str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()), 
                'Descripcion' => $turn->Nombre, 
                'Observacion' => $data->observations_search[$key], 
                'Cuota' => $turn->CuotaFija, 
                'Cantidad' => $data->quantity_search[$key], 
                'LicenciaAnexo' => 0]);
            }
            
            foreach ($turn->documents()->wherePivot("Alta", 1)->get() as $key => $document) {
                $documents[$document->Requisito] = $document->Requisito;
            }
        }
        
        foreach ($documents as $key => $document) 
        {
            if($request->documents()->where("Requisitos.Requisito", $document)->first() == null){
                $request->documents()->attach($document);
            }
        }

        $now = Carbon::now("America/Mexico_City")->format("Y");
        $adeudo = 0;
        $cost = 0;
        $reference_add = 1;

        if(count($request->turns) == 0){
            return $license;
        }
        
        //eliminar los cargos antiguos de las licencias
        // DB::connection("sqlsrv")->table("Adeudos")->where("NumeroLicencia", $license->NumeroLicencia)->delete();

        // dd($request->turns, $data);

        // if(!empty($license->turns))
        // {
        //     $license->turns()->detach();
        // }

        //eliminar repetidos
        $checkRepeat = [];
        $data_cuenta_serach = $data->cuenta_serach;
        $data_observations_search = $data->observations_search;
        $data_quantity_search = $data->quantity_search;
        $data_date_search = $data->date_search;
        $turnRepeatKey[] = 0;
        if(!is_null($data->cuenta_serach)){
            foreach ($data_cuenta_serach as $key => $giro){
                if(in_array($giro, $checkRepeat)){
                    unset($data_cuenta_serach[$key]);
                    unset($data_observations_search[$key]);
                    unset($data_quantity_search[$key]);
                    if($data_date_search[$key]  < $data_date_search[$turnRepeatKey[$giro]]){
                        $data_date_search[$turnRepeatKey[$giro]] = $data_date_search[$key];
                    }
                    unset($data_date_search[$key]);
                }else{
                    $checkRepeat[] = $giro;
                    $turnRepeatKey[$giro] = $key;
                }
            }
        }


        $data->cuenta_serach= $data_cuenta_serach;
        $data->observations_search = $data_observations_search;
        $data->quantity_search = $data_quantity_search;
        $data->date_search = $data_date_search;

        if(!is_null($data->cuenta_serach))
        {
            foreach ($data->cuenta_serach as $key => $turn_id) 
            {
                $i = $key + 1;
                $turn = $this->getTurnById($turn_id);
                $obs = $data->observations_search[$key];
                $quantityTurn = floatval($data->quantity_search[$key]);
                $adeudo = $turn->CuotaFija * $quantityTurn;

                $license->turns()->attach($turn->IdGiro, ['Secuencia' => $i, 
                                                            'Fecha' => str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()), 
                                                            'Descripcion' => $turn->Nombre, 
                                                            'Observacion' => $obs, 
                                                            'Cuota' => $turn->CuotaFija, 
                                                            'Cantidad' => $quantityTurn, 
                                                            'LicenciaAnexo' => 0]);
    
                                              
                $conpto = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $turn->Concepto)->first();
                $conpto_ref = $turn->concepts()->where("TiposTramite.TipoTramite", "R")->first();
                $conpto_ref = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $conpto_ref->pivot->Concepto)->first();
                $year = Carbon::parse(str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()))->format("Y");
                $trimester = $this->getActualTrimester(str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()));
    
                if($year == $now){
                    $cost += $adeudo * ($trimester["percentage"] / 100);
                    //en caso de que el adeudo ya esté cargado no se vuelve a ingresar
                    $adeudoC = Debts::where("Departamento", "02")->where("Concepto", strval($turn->Concepto))->where("CuentaDepto", strval($license->NumeroLicencia))->where("Referencia", $year."-".$reference_add)->first();
                    if(!is_null($adeudoC)){
                        $reference_add++;
                    }
                    Debts::create([ 
                        "Departamento" => "02",
                        "Concepto" => strval($turn->Concepto),
                        "CuentaDepto" => strval($license->NumeroLicencia),
                        "Referencia" => $year."-".$reference_add,
                        "FechaMovto" => $now,
                        "NumeroLicencia" => strval($license->NumeroLicencia),
                        "FechaVence" => $now,
                        "Descripcion" => $conpto->Descripcion,
                        "Importe" => $adeudo * ($trimester["percentage"] / 100),
                        "Estado" => "A",
                        "Referencia2" => ($turn->Alcohol == 1) ? 'Alcohol' : '',
                        "Cargo" => 1,
                    ]);
                }else{
                    foreach ($turn->periods() as $key => $period) {
                        $adeudo = $period->CuotaFija * $quantityTurn;
                        if($period->Ano == $year){
                            $cost += $adeudo * ($trimester["percentage"] / 100);
    
                            //verificamos que no se haya cargado el adeudo de la licencia con el giro nuevo
                            $adeudoC = Debts::where("Departamento", "02")->where("Concepto", strval($turn->Concepto))
                            ->where("CuentaDepto", strval($license->NumeroLicencia))->where("Referencia", $period->Ano."-".$reference_add)->first();
                            

                            if(!is_null($adeudoC)){
                                $reference_add++;
                            }
                            
                            Debts::create([
                                "Departamento" => "02",
                                "Concepto" => strval($turn->Concepto),
                                "CuentaDepto" => strval($license->NumeroLicencia),
                                "Referencia" => $period->Ano."-".$reference_add,
                                "FechaMovto" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                "NumeroLicencia" => strval($license->NumeroLicencia),
                                "FechaVence" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                "Descripcion" => $conpto->Descripcion,
                                "Importe" => $adeudo * ($trimester["percentage"] / 100),
                                "Estado" => "A",
                                "Referencia2" => ($turn->Alcohol == 1) ? 'Alcohol' : '',
                                "Cargo" => 1,
                            ]);
                        }elseif($period->Ano > $year && $period->Ano <= $now){
                            $cost += is_numeric($period->CuotaMinima)? ($period->CuotaMinima * $quantityTurn) : $adeudo;
    
                            //verificamos que no se haya cargado el adeudo de la licencia con el giro nuevo
                            $adeudoC = Debts::where("Departamento", "02")->where("Concepto", strval($conpto_ref->Concepto))
                            ->where("CuentaDepto", strval($license->NumeroLicencia))->where("Referencia", $period->Ano."-".$reference_add)->first();
    
                            if(!is_null($adeudoC)){
                                $reference_add++;
                            }

                            Debts::create([
                                "Departamento" => "02",
                                "Concepto" => strval($conpto_ref->Concepto),
                                "CuentaDepto" => strval($license->NumeroLicencia),
                                "Referencia" => $period->Ano."-".$reference_add,
                                "FechaMovto" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                "NumeroLicencia" => strval($license->NumeroLicencia),
                                "FechaVence" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                "Descripcion" => $conpto_ref->Descripcion,
                                "Importe" => is_numeric($period->CuotaMinima)? ($period->CuotaMinima * $quantityTurn) : $adeudo,
                                "Estado" => "A",
                                "Referencia2" => ($turn->Alcohol == 1) ? 'Alcohol' : '',
                                "Cargo" => 1,
                            ]);
                        }
                    }
                }
            }
        }
        // en Ampliacion de giro se pone A
        try {
            $request->process()->attach("A");
        } catch (\Throwable $th) {
            //throw $th;
        }
        
        return $license;
    }

    public function getProcessById($id)
    {
        return ProcessType::find($id);
    }
    
    /*****************************************************************************
    * *
    * * Functions for Process
    * *
    *****************************************************************************/
    
    public function aplayAnualTurns($data)
    {
        $turns = Turn::all();
        foreach ($turns as $key => $turn) {

            $historico = new HistoricoGiro;
            $historico->IdGiro = $turn->IdGiro;
            $historico->Ano = $data->period;
            $historico->CuotaMinima = $turn->CuotaFija * ((100 + $data->percentage) /  100);
            // $historico->CuotaMaxima = $turn->CuotaFija * ((100 + $data->percentage) /  100);
            $historico->CuotaFija = $turn->CuotaFija * ((100 + $data->percentage) /  100);
            $historico->Valorsindescuento = 0;
            $historico->save();

            $turn->CuotaFija = $turn->CuotaFija * ((100 + $data->percentage) /  100);
            $turn->save();
        }
    }
    
    /*****************************************************************************
    * *
    * * Functions for Fines
    * *
    *****************************************************************************/
    
    public function getAllFines()
    {
        
    }
    
    /*****************************************************************************
    * *
    * * Functions for Reports
    * *
    *****************************************************************************/
    
    public function getLicenseForReport($data)
    {
        // dd($data);
        $initial = str_replace(" ", "T", Carbon::create($data->start." 00:00:00")->toDateTimeString());
        $final = str_replace(" ", "T", Carbon::create($data->end." 23:59:59")->toDateTimeString());
        $licenses = License::whereBetween("FechaAlta", [$initial, $final]);

        if($data->locations[0] != null){
            $licenses->whereIn("IdPoblacion", $data->locations);
        }

        $licenses = $licenses->get();
        
        if($data->turns[0] != null){
            foreach ($licenses as $key => $license) {
                $flag = 0;
                foreach ($license->turns as $keyTurn => $turn) {
                    if(in_array($turn->IdGiro, $data->turns)){
                        $flag = 1;
                    }
                }

                if($flag == 0){
                    $licenses->forget($key);
                }
            }
        }
        return $licenses;
    }

    public function getFirstLicenseDate()
    {
        $firstRecord = License::orderBy('FechaAlta', 'asc')->first();

        $date = Carbon::parse($firstRecord->FechaAlta);

        return $date->format('Y-m-d');
    }

    public function getLicenseRefrendosForReport($data)
    {
        $date_start = Carbon::createFromFormat('Y-m-d H:i:s', $data->start.' 00:00:00');
        $date_end = Carbon::createFromFormat('Y-m-d H:i:s', $data->end.' 23:59:59');
        // dd($data->all(), $date_start, $date_end);
        $pagos = Payment::join('Licencias', 'Pagos.CuentaDepto', '=', 'Licencias.NumeroLicencia')
                        ->join('GiroDetalleLic', 'Licencias.NumeroLicencia', '=', 'GiroDetalleLic.NumeroLicencia')
                        ->join('Giros', 'GiroDetalleLic.IdGiro', '=', 'Giros.IdGiro')
                        ->where(function($query) use($date_start, $date_end) {
                            $query->whereBetween('Pagos.FechaPago', [$date_start->toDateTimeString(), $date_end->toDateTimeString()])
                                    ->where('Pagos.Departamento', '02')
                                    ->where('Giros.Nombre', 'not like', '%ANUNCIO%')
                                    ->where('Giros.Nombre', 'not like', '%SARE%');
                        })
                        ->select('Pagos.CuentaDepto', 'Pagos.Importe', 'Pagos.FechaPago')
                        ->distinct()
                        ->get();

        $licensesArray = [
            'licenses' => [],
            'refrendos' => []
        ];
        foreach ($pagos as $pago)
        {
            $license = $pago->license;
            $fechaAlta = Carbon::parse($license->FechaAlta);

            if ($date_start <= $fechaAlta && $date_end >= $fechaAlta)
            {
                // Nueva licencia
                if(!key_exists($pago->CuentaDepto, $licensesArray['licenses']))
                {
                    $licensesArray['licenses'][$pago->CuentaDepto] = [
                        'license' => $pago->license,
                        'importe' => (float)$pago->Importe,
                        'fechaPago' => $pago->FechaPago
                    ];
                }else 
                {
                    $licensesArray['licenses'][$pago->CuentaDepto]['importe'] += (float)$pago->Importe;
                }
            }else
            {
                // Refrendos
                if(!key_exists($pago->CuentaDepto, $licensesArray['refrendos']))
                {
                    $licensesArray['refrendos'][$pago->CuentaDepto] = [
                        'license' => $pago->license,
                        'importe' => (float)$pago->Importe,
                        'fechaPago' => $pago->FechaPago
                    ];
                }else 
                {
                    $licensesArray['refrendos'][$pago->CuentaDepto]['importe'] += (float)$pago->Importe;
                }
            }
        }

        return $licensesArray;
    }
    
    /*****************************************************************************
    * *
    * * Functions for Trash
    * *
    *****************************************************************************/
    
    public function updateTrash($data)
    {
        if($data->type == 'turn'){
            Turn::where('IdGiro', $data->turn)->update([
                "Basura" => (isset($data->trash)) ? $data->trash : 0,
                "ConceptoDescarga" => $data->condescarga,
                "CostoDescarga" => $data->cuotadescarga,
                "ConceptoRecoleccion" => $data->conrecoleccion,
                "CostoRecoleccion" => $data->cuotarecoleccion,
            ]);

            // Generar padron de costos 15 años atras
            $turn = $this->getTurnById($data->turn);
            if (!empty($turn))
            {
                $currentYear = now()->year;
                $stopYear = 2000;
                $currentCuotaDescarga = $data->cuotadescarga;
                $currentCuotaRecoleccion = $data->cuotarecoleccion;
    
                for ($year = $currentYear; $year >= $stopYear; $year--)
                {
                    $trashYear = $turn->trashYears()->where('year', $year)->first();

                    if (empty($trashYear))
                    {
                        $trashYear = new Trash;
                        $trashYear->year = $year;
                        $trashYear->giro_id = $turn->IdGiro;
                        $trashYear->save();
                    }
    
                    $trashYear->costo_descarga = $currentCuotaDescarga;
                    $trashYear->costo_recoleccion = $currentCuotaRecoleccion;
                    $trashYear->save();

                    $currentCuotaDescarga = $currentCuotaDescarga * 100 / 103; // 3% de diferencia con año
                    $currentCuotaRecoleccion = $currentCuotaRecoleccion * 100 / 103; // 3% de diferencia con año
                }
            }
        }elseif ($data->type == 'location') {
            DB::connection("sqlsrv")->table("Poblaciones")->where('IdPoblacion', $data->id)->update(["Basura" => $data->value]);
        }
        return true;
    }

    public function trashYearAdd($request)
    {
        $turn = $this->getTurnById($request->turn);

        if (!empty($turn))
        {
            $trashYear = $turn->trashYears()->where('year', $request->year)->first();

            if (empty($trashYear))
            {
                $trashYear = new Trash;
                $trashYear->year = $request->year;
                $trashYear->giro_id = $turn->IdGiro;
                $trashYear->save();
            }

            $trashYear->costo_descarga = $request->cuotadescarga;
            $trashYear->costo_recoleccion = $request->cuotarecoleccion;
            $trashYear->save();
        }
    }
    
    /*****************************************************************************
    * *
    * * Functions for Alerts
    * *
    *****************************************************************************/
    
    public function getAllAlerts()
    {
        return DB::connection("sqlsrv")->table("Alertas")->get();
    }
    
    public function storeAlert($data)
    {
        $license = $this->getLicenseById($data->license);
        $old =DB::connection("sqlsrv")->table("Alertas")->select(DB::raw("Cast(IdAlerta as INT) as Alerta"))->orderBy("Alerta","DESC")->first();
        $alerta = str_pad($old->Alerta + 1, 10, "0", STR_PAD_LEFT);
        $giros = '';
        foreach ($license->turns as $key => $turn) {
            $giros .= $turn->Nombre.", ";
        }

        $giros = substr($giros, 0, -2);
        // dd($old, $alerta);

        DB::connection("sqlsrv")->table("Alertas")->insert([
            'IdAlerta' => $alerta,
            'IdCalle' => $license->IdCalle,
            'IdPoblacion' => $license->IdPoblacion,
            'IdColonia' => $license->IdColonia,
            'Exterior' => $license->Exterior,
            'Interior' => $license->Interior,
            'Alerta' => substr($data->description, 0, 250), 
            'TipoGiro' => substr($giros, 0, 50),
            'De' => substr($data->from, 0, 50),
            'Para' => substr($license->NombreNegocio, 0, 50),
            'FechaAlta' => str_replace(" ", "T", Carbon::parse($data->date)->toDateTimeString()),
            'PermitirTramites' => (isset($data->tramites))? $data->tramites : 0,
            'NumeroLicencia' => $license->NumeroLicencia,
        ]);
    }
    
    public function updateAlert($data)
    {
        DB::connection("sqlsrv")->table("Alertas")->where('IdAlerta', $data->alert)->update([
            'Alerta' => substr($data->description, 0, 250),
            'De' => substr($data->from, 0, 50),
            'FechaAlta' => str_replace(" ", "T", Carbon::parse($data->date)->toDateTimeString()),
            'PermitirTramites' => (isset($data->tramites))? $data->tramites : 0,
        ]);
    }

    public function getAlert($data)
    {
        $alert = Alert::find($data->alert);
        return $alert;
    }

    public function getAlertsWithLicense()
    {
        return Alert::whereNotNull("NumeroLicencia")->paginate(15);
    }

    public function updateLicenseTaxpayer($data){
        $now = str_replace(" ", "T", Carbon::now("America/Mexico_city")->toDateTimeString());;
        $license = License::find($data->id);
        //actualizar solicitud
        $request = @$license->requests()->first();
        $license->IdContribuyente = $data->idtaxpayer;
        $license->FechaSolicitud = $now;
        $license->save();

        try {
            $request = $license->requests()->first();
            $request->IdContribuyente = $data->idtaxpayer;
            $request->FechaSolicitud = $now;
            $request->save();
        } catch (\Throwable $th) {
            //throw $th;
        }
        
        return $license;

    }

    public function storeBajaLicencia($NumeroLicencia, $data)
    {
        $licencia = $this->getLicenseById($NumeroLicencia);
        $bajaLicencia = $this->getBajaLicenseById($NumeroLicencia);

        //try {
            if(is_null($bajaLicencia)) {
                //se registra uno nuevo
                $bajaLicencia = new BajasLicencia();
            }else{

            }
            $bajaLicencia->NumeroLicencia = $licencia->NumeroLicencia;
            $bajaLicencia->IdContribuyente = $licencia->IdContribuyente;
            $bajaLicencia->FechaAlta = str_replace(" ", "T", $licencia->FechaAlta);
            $bajaLicencia->FechaSolicitud = str_replace(" ", "T", $licencia->FechaSolicitud);
            $bajaLicencia->FolioLicencia = $licencia->FolioLicencia;
            $bajaLicencia->NombreNegocio = $licencia->NombreNegocio;
            $bajaLicencia->Linderos = $licencia->Linderos;
            $bajaLicencia->Observaciones = $licencia->Observaciones;
            $bajaLicencia->IdColonia = $licencia->IdColonia;
            $bajaLicencia->IdCalle = $licencia->IdCalle;
            $bajaLicencia->Exterior = $licencia->Exterior;
            $bajaLicencia->Interior = $licencia->Interior;
            $bajaLicencia->IdCruce1 = $licencia->IdCruce1;
            $bajaLicencia->IdCruce2 = $licencia->IdCruce2;
            $bajaLicencia->IdZona = $licencia->IdZona;
            $bajaLicencia->IdPoblacion = $licencia->IdPoblacion;
            $bajaLicencia->Superficie = $licencia->Superficie;
            $bajaLicencia->EmpleosCreados = $licencia->EmpleosCreados;
            $bajaLicencia->FechaImpresion = str_replace(" ", "T", $licencia->FechaImpresion);
            $bajaLicencia->FechaTramite = str_replace(" ", "T", $licencia->FechaTramite);
            $bajaLicencia->CausaBaja = 1; //siempre el el identificador 1
            $bajaLicencia->FechaBaja = str_replace(" ", "T", Carbon::now("America/Mexico_city")->toDateTimeString());//fecha actual del servidor 
            $bajaLicencia->Comentario = is_null($data->get("baja-comments"))? "" : $data->get("baja-comments") ;  //si no hay datos se pone comentarios vacios
            $bajaLicencia->ClaveUsuario = $licencia->ClaveUsuario;
            $bajaLicencia->CoordenadaEste = $licencia->CoordenadaEste;
            $bajaLicencia->CoordenadaNorte = $licencia->CoordenadaNorte;
            $bajaLicencia->PermisoAlcohol = $licencia->PermisoAlcohol;

            $bajaLicencia->save();
            //en caso de haber tramites de licencia, el sistema los debe eliminar
            
            $this->deleteLicenseById($licencia->NumeroLicencia);
        //} catch (\Throwable $th) {
            //dd("no");
        //}
        
        return $bajaLicencia;
        
    }

    public function getBajaLicenseById($id)
    {
        return BajasLicencia::find($id);
    }

    public function getLicenciaTramites($NumeroLicencia)
    {
        return DB::connection("sqlsrv")->table("LicenciasTramites")->where("NumeroLicencia", $NumeroLicencia)->get();
    }

    //metodo para eliminar licencias
    public function deleteLicenseById($NumeroLicencia)
    {
        //eliminar de Permisos
        DB::connection("sqlsrv")->table("Permisos")->where("NumeroLicencia", $NumeroLicencia)->delete();
        //eliminar de LicenciasTramites
        DB::connection("sqlsrv")->table("LicenciasTramites")->where("NumeroLicencia", $NumeroLicencia)->delete();
        //eliminar referencia de historicolicencia
        $historicosLicenciasId = DB::connection("sqlsrv")->table("HistoricoLicencias")->where("NumeroLicencia", $NumeroLicencia)->pluck("IdHistorico")->toArray();
        if(count($historicosLicenciasId) > 0)
        {
            DB::connection("sqlsrv")->table("GiroDetalleHis")->whereIn("IdHistorico", $historicosLicenciasId)->delete();
        }
        //eliminar relacion de tramiteslicencia historial
        DB::connection("sqlsrv")->table("TramitesLicenciasHis")->where("NumeroLicencia", $NumeroLicencia)->delete();
        //eliminar de HistoricoLicencias
        DB::connection("sqlsrv")->table("HistoricoLicencias")->where("NumeroLicencia", $NumeroLicencia)->delete();
        //eliminar de LeyendasLicencias
        DB::connection("sqlsrv")->table("LeyendasLicencias")->where("NumeroLicencia", $NumeroLicencia)->delete();
        //eliminar de GiroDetalleLic
        DB::connection("sqlsrv")->table("GiroDetalleLic")->where("NumeroLicencia", $NumeroLicencia)->delete();
        //eliminar de LicenciasInspeccionar
        DB::connection("sqlsrv")->table("LicenciasInspeccionar")->where("NumeroLicencia", $NumeroLicencia)->delete();
        //eliminar de Requerimientos
        DB::connection("sqlsrv")->table("Requerimientos")->where("NumeroLicencia", $NumeroLicencia)->delete();
        
        //cambiar estatus de pagos
        DB::connection("sqlsrv")->table("Pagos")->where("NumeroLicencia", $NumeroLicencia)->delete();
        //eliminar de Adeudos

        DB::connection("sqlsrv")->table("Adeudos")->where("NumeroLicencia", $NumeroLicencia)->delete();

        License::find($NumeroLicencia)->delete();
    }

    public function addPeriodToTurn($idGiro, $data)
    {
        $historicoGiro = new HistoricoGiro();
        $historicoGiro->IdGiro = $idGiro;
        $historicoGiro->Ano = $data->periodo_ano;
        $historicoGiro->CuotaMinima = is_numeric($data->periodo_cuota_minima) ? floatval($data->periodo_cuota_minima) : 0;
        // $historicoGiro->CuotaMaxima = is_numeric($data->periodo_cuota_maxima) ? floatval($data->periodo_cuota_maxima) : 0;
        $historicoGiro->CuotaFija = is_numeric($data->periodo_cuota_fija) ? floatval($data->periodo_cuota_fija) : 0;
        $historicoGiro->Valorsindescuento = 0;
        $historicoGiro->save();
    }

    public function getAllLocations($data = null, $pdf = null)
    {
        $locations = Location::orderBy("IdPoblacion","DESC");
        if(array_key_exists("id_poblacion", $data) && $data["id_poblacion"]){
            $locations->where("IdPoblacion", "=", $data["IdPoblacion"]);
        }
        if(array_key_exists("nombre_poblacion", $data) && $data["nombre_poblacion"]){
            $locations->where("NombrePoblacion", "like", "%".strtoupper($data["nombre_poblacion"])."%");
        }
        if(array_key_exists("nombre_corto", $data) && $data["nombre_corto"]){
            $locations->where("NombreCorto", "like", "%".$data["nombre_corto"]."%");
        }
        if($pdf)
        {
            return $locations->get();
        }else {
            return $locations->paginate(25);
        }
    }

    public function getLocationById($id)
    {
        return Location::find($id);
    }

    public function storeLocation($data)
    {
        $oldLocation = Location::orderBy("IdPoblacion", "desc")->first();

        $location = new Location;
        $location->IdPoblacion = intval($oldLocation->IdPoblacion) + 1;
        $location->NombrePoblacion = strtoupper($data->crear_nombre_poblacion);
        $location->NombreCorto = strtoupper($data->crear_nombre_corto);
        $location->IdMunicipio = 1;
        $location->save();
        return $location;
    }

    public function storeUpdateLocation($id, $data)
    {
        $location = $this->getLocationById($id);
        $location->NombrePoblacion = strtoupper($data->actualizar_nombre_poblacion);
        $location->NombreCorto = strtoupper($data->actualizar_nombre_corto);
        $location->save();
        return $location;
    }

    //colonias por ciudad
    public function getAllColoniesByCity($idPoblacion, $data = null, $pdf=null){
        $colonies = Colony::where("IdPoblacion", $idPoblacion)->orderBy("IdColonia","DESC");

        if(array_key_exists("id_colonia", $data) && $data["id_colonia"]){
            $colonies->where("IdColonia", "=", $data["id_colonia"]);
        }
        if(array_key_exists("nombre_colonia", $data) && $data["nombre_colonia"]){
            $colonies->where("NombreColonia", "like", "%".strtoupper($data["nombre_colonia"])."%");
        }
        if(array_key_exists("nombre_comun", $data) && $data["nombre_comun"]){
            $colonies->where("NombreComun", "like", "%".$data["nombre_comun"]."%");
        }
        if($pdf)
        {
            return $colonies->get();
        }else {
            return $colonies->paginate(25);
        }
    }

    //todas las colonias
    public function getAllColonies($data = null, $pdf=null)
    {
        $colonies = Colony::orderBy("IdColonia","DESC");

        if(array_key_exists("id_colonia", $data) && $data["id_colonia"]){
            $colonies->where("IdColonia", "=", $data["id_colonia"]);
        }
        if(array_key_exists("nombre_colonia", $data) && $data["nombre_colonia"]){
            $colonies->where("NombreColonia", "like", "%".strtoupper($data["nombre_colonia"])."%");
        }
        if(array_key_exists("nombre_comun", $data) && $data["nombre_comun"]){
            $colonies->where("NombreComun", "like", "%".$data["nombre_comun"]."%");
        }
        if($pdf)
        {
            return $colonies->get();
        }else {
            return $colonies->paginate(25);
        }
    }
    
    public function getAllLocationsList()
    {
        return Location::orderBy("IdPoblacion", "Desc")->get();
    }

    public function storeColony($data)
    {
        $oldColony = Colony::orderBy("IdColonia", "desc")->first();
        $colony = new Colony();
        $colony->IdColonia = intval($oldColony->IdColonia) + 1;
        $colony->IdMunicipio = 1;
        $colony->IdPoblacion = $data->crear_id_poblacion;
        $colony->NombreColonia = strtoupper($data->crear_nombre_colonia);
        $colony->NombreComun = strtoupper($data->crear_nombre_comun);
        $colony->Colonos = "N";
        $colony->PorcentajeRefrendo = 0;
        $colony->Controlado = 0;
        $colony->save();
        return $colony;
    }

    public function storeUpdateColony($id, $data)
    {
        $colony = $this->getColonyById($id);
        $colony->IdPoblacion = $data->actualizar_id_poblacion;
        $colony->NombreColonia = strtoupper($data->actualizar_nombre_colonia);
        $colony->NombreComun = strtoupper($data->actualizar_nombre_comun);
        $colony->save();
        return $colony;
    }

    public function getColonyById($id)
    {
        return Colony::find($id);
    }

    public function getAllStreets($data = null)
    {
        $streets = Street::orderBy("IdCalle","DESC");

        if(array_key_exists("id_calle", $data) && $data["id_calle"]){
            $streets->where("IdCalle", "=", $data["id_calle"]);
        }
        if(array_key_exists("nombre_calle", $data) && $data["nombre_calle"]){
            $streets->where("NombreCalle", "like", "%".strtoupper($data["nombre_calle"])."%");
        }
        if(array_key_exists("alias_calle", $data) && $data["alias_calle"]){
            $streets->where("AliasCalle", "like", "%".$data["alias_calle"]."%");
        }
        if(array_key_exists("nombre_oficial", $data) && $data["nombre_oficial"]){
            $streets->where("NombreOficial", "like", "%".$data["nombre_oficial"]."%");
        }

        return $streets->paginate(25);

    }

    public function getAllStreetsByLocation($idPoblacion, $data)
    {
        $streets = Street::where("IdPoblacion", $idPoblacion)->orderBy("IdCalle","DESC");

        if(array_key_exists("id_calle", $data) && $data["id_calle"]){
            $streets->where("IdCalle", "=", $data["id_calle"]);
        }
        if(array_key_exists("nombre_calle", $data) && $data["nombre_calle"]){
            $streets->where("NombreCalle", "like", "%".strtoupper($data["nombre_calle"])."%");
        }
        if(array_key_exists("alias_calle", $data) && $data["alias_calle"]){
            $streets->where("AliasCalle", "like", "%".$data["alias_calle"]."%");
        }
        if(array_key_exists("nombre_oficial", $data) && $data["nombre_oficial"]){
            $streets->where("NombreOficial", "like", "%".$data["nombre_oficial"]."%");
        }

        return $streets->paginate(25);
    }

    public function storeStreet($data)
    {
        $oldStreet = Street::orderBy("IdCalle", "desc")->first();
        $street = new Street();
        $street->IdCalle = intval($oldStreet->IdCalle) + 1;
        $street->NombreCalle = strtoupper($data->crear_nombre_calle);
        $street->AliasCalle = strtoupper($data->crear_alias_calle);
        $street->NombreOficial = strtoupper($data->crear_nombre_oficial);
        $street->IdMunicipio = 1;
        $street->IdPoblacion = $data->crear_id_poblacion;
        $street->IdTipoCalle = "S/T";
        $street->save();
        return $street;
    }

    public function getStreetById($id)
    {
        return Street::find($id);
    }

    public function storeUpdateStreet($id, $data)
    {
        $street = $this->getStreetById($id);
        $street->NombreCalle = strtoupper($data->actualizar_nombre_calle);
        $street->AliasCalle = strtoupper($data->actualizar_alias_calle);
        $street->NombreOficial = strtoupper($data->actualizar_nombre_oficial);
        $street->IdPoblacion = $data->actualizar_id_poblacion;
        $street->save();
        return $street;
    }

    //actualizar datos del negocio
    public function storeUpdateLicenseTurn($data)
    {
        $now = str_replace(" ", "T", Carbon::now("America/Mexico_city")->toDateTimeString());
        $license = License::find($data->id);

        $license->Linderos = $data->linderos;
        $license->Observaciones = is_null($data->observations)? '': $data->observations ;
        $license->Superficie = is_null($data->Superficie)? $license->Superficie : ($data->Superficie);
        $license->EmpleosCreados = is_null($data->EmpleosCreados)? $license->EmpleosCreados: $data->EmpleosCreados ;
        $license->NombreNegocio = $data->name_b;
        $license->FechaAlta = $data->alta;
        $license->NumeroTarjetonAlcoholes = $data->tarjeton_alcohol;
        $license->save();

        $license->legends()->detach();
        // Agregamos las leyendas a la Solicitud
        if(!is_null($data->legends))
        {
            foreach ($data->legends as $key => $legend) {
                $license->legends()->attach($legend, ['FolioLicencia' => 1]);
            }
        }
        
        if($data->operation == "update_turn_data"){
            return $license;
        }
        $now = Carbon::now("America/Mexico_City")->format("Y");
        $adeudo = 0;
        //eliminar los cargos antiguos de las licencias que no tengan adeudo
        DB::connection("sqlsrv")->table("Adeudos")->where("CuentaDepto", $license->NumeroLicencia)->delete();

        $license->turns()->detach();
        if(!empty($data->cuenta_serach))
        {
            foreach ($data->cuenta_serach as $key => $turn_id) 
            {
                $i = $key + 1;
                $turn = $this->getTurnById($turn_id);
                $obs = $data->observations_search[$key];
                $quantityTurn = (float)$data->quantity_search[$key];
                $adeudo = (float)$data->cost_search[$key] * $quantityTurn;

                $license->turns()->attach($turn->IdGiro, ['Secuencia' => $i, 
                                                            'Fecha' => str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()), 
                                                            'Descripcion' => $turn->Nombre, 
                                                            'Observacion' => $obs, 
                                                            'Cantidad' => $quantityTurn, 
                                                            'Cuota' => $adeudo,
                                                            'Refrendo' => $data->type[$key] == 2? 1 : 0, 
                                                            'LicenciaAnexo' => 0]);
                
                $year = Carbon::parse(str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()))->format("Y");
                $conpto = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $turn->Concepto)->first();
                // $description = substr($conpto->Descripcion.' ('.$turn->Nombre.')', 0, 99);
                $description = $conpto->Descripcion;

                Debts::create([
                    "Departamento" => "02",
                    "Concepto" => strval($turn->Concepto),
                    "CuentaDepto" => strval($license->NumeroLicencia),
                    "Referencia" => $year.'-'.$turn->IdGiro,
                    "FechaMovto" => now()->format('Y-m-d').' 00:00:00',
                    "NumeroLicencia" => strval($license->NumeroLicencia),
                    "FechaVence" => now()->format('Y-m-d').' 00:00:00',
                    "Descripcion" => $description,
                    "Importe" => $adeudo,
                    "Estado" => "A",
                    "Referencia2" => ($turn->Alcohol == 1) ? 'Alcohol' : '',
                    "Cargo" => 1,
                ]);
            }
        }

        // Basura
        $license->garbages()->delete();
        Debts::where(function($query) use($license) {
            $query->where('CuentaDepto', strval($license->NumeroLicencia))
                    ->where('Departamento', '02')
                    ->where('Referencia2', 'Basura');
        })
        ->delete();

        if(!empty($data->garbage_year))
        {
            foreach ($data->garbage_year as $key => $garbageYear)
            {
                $reference = $key + 1;
                $lastRecord = LicenciaBasura::orderBy('Id', 'desc')->first();
                $id = 1;
                if (!empty($lastRecord))
                {
                    $id = $lastRecord->Id + 1;
                }

                $licenciaBasura = new LicenciaBasura;
                $licenciaBasura->Id = $id;
                $licenciaBasura->Year = $garbageYear;
                $licenciaBasura->Concepto = $data->garbage_concept[$key];
                $licenciaBasura->Descripcion = config('system.padron.garbage.concepts.'.$data->garbage_concept[$key]);
                $licenciaBasura->Importe = (float)$data->garbage_amount[$key];
                $licenciaBasura->LicenciaId = $license->NumeroLicencia;
                $licenciaBasura->save();

                Debts::create([
                    "Departamento" => "02",
                    "Concepto" => strval($licenciaBasura->Concepto),
                    "CuentaDepto" => strval($license->NumeroLicencia),
                    "Referencia" => $licenciaBasura->Year.'-'.$reference,
                    "FechaMovto" => now()->format('Y-m-d').' 00:00:00',
                    "NumeroLicencia" => strval($license->NumeroLicencia),
                    "FechaVence" => now()->format('Y-m-d').' 00:00:00',
                    "Descripcion" => $licenciaBasura->Descripcion,
                    "Importe" => $licenciaBasura->Importe,
                    "Estado" => "A",
                    "Referencia2" => 'Basura',
                    "Cargo" => 1,
                ]);
            }
        }

        //
        return $license;
    }

    //obtener callle por id
    public function getCalleById($id){
        return Street::find($id);
    }

    public function changeTurnRequest($data)
    {
        $license = $this->getLicenseById($data->id);
        //**primero agregamos la solicitud*** */
        $now = str_replace(" ", "T", Carbon::now("America/Mexico_city")->toDateTimeString());
        $old = Solicitud::orderBy("NumeroSolicitud","DESC")->first();
        $request = new Solicitud();
        $request->NumeroSolicitud = $old->NumeroSolicitud + 1;
        $request->IdContribuyente = $license->IdContribuyente;
        $request->NombreNegocio = $data->name_b;
        $request->Linderos = $data->linderos;
        $request->Observaciones = $data->observations;
        $request->IdColonia = $license->IdColonia;
        $request->IdCalle = $license->IdCalle;
        $request->Exterior = $license->Exterior;
        $request->Interior = $license->Interior;
        $request->IdCruce1 = $license->IdCruce1;
        $request->IdCruce2 = $license->IdCruce2;
        $request->IdZona = $license->IdZona;
        $request->IdPoblacion = $license->IdPoblacion;
        $request->Superficie = $data->surface;
        $request->FechaImpresion = $now;
        $request->FechaSolicitud = $now;
        $request->ClaveUsuario = "015";
        $request->Estatus = "C";
        $request->Aplicado = "1";
        $request->EmpleosCreados = intval($data->employees);
        $request->CoordenadaEste = $license->CoordenadaEste;
        $request->CoordenadaNorte = $license->CoordenadaNorte;
        $request->save();

        $request->turns()->detach();
        //eliminar repetidos
        $checkRepeat = [];
        $data_cuenta_serach = $data->cuenta_serach;
        $data_observations_search = $data->observations_search;
        $data_quantity_search = $data->quantity_search;
        $data_date_search = $data->date_search;
        $turnRepeatKey[] = 0;
        foreach ($data_cuenta_serach as $key => $giro){
            if(in_array($giro, $checkRepeat)){
                unset($data_cuenta_serach[$key]);
                unset($data_observations_search[$key]);
                unset($data_quantity_search[$key]);
                if($data_date_search[$key]  < $data_date_search[$turnRepeatKey[$giro]]){
                    $data_date_search[$turnRepeatKey[$giro]] = $data_date_search[$key];
                }
                unset($data_date_search[$key]);
            }else{
                $checkRepeat[] = $giro;
                $turnRepeatKey[$giro] = $key;
            }
        }

        $data->cuenta_serach= $data_cuenta_serach;
        $data->observations_search = $data_observations_search;
        $data->quantity_search = $data_quantity_search;
        $data->date_search = $data_date_search;
        //fin
        $documents = [];
        if(!is_null($data->cuenta_serach))
        {
            foreach ($data->cuenta_serach as $key => $giro) {
                $i = $key + 1;
                $turn = Turn::find($giro);
                $request->turns()->attach($giro, ['Secuencia' => $i, 
                                                  'Fecha' => str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()), 
                                                  'Descripcion' => $turn->Nombre, 
                                                  'Observacion' => $data->observations_search[$key], 
                                                  'Cuota' => $turn->CuotaFija, 
                                                  'Cantidad' => $data->quantity_search[$key], 
                                                  'LicenciaAnexo' => 0]);
    
                foreach ($turn->documents()->wherePivot("Alta", 1)->get() as $key => $document) {
                    $documents[$document->Requisito] = $document->Requisito;
                }
            }
        }

        
        foreach ($documents as $key => $document) {
            if($request->documents()->where("Requisitos.Requisito", $document)->first() == null){
                $request->documents()->attach($document);
            }
        }
        $license = $this->processChangeTurn($data);
        $request->NumeroLicencia = $license->NumeroLicencia;
        $request->save();
        $request->process()->attach("G");
        return $license;
        //***se termina la solicitud y despues se cambia el rol */
    }
    //hacer proceso para cambio o ampliacion de giro
    public function processChangeTurn($data)
    {
        $now = str_replace(" ", "T", Carbon::now("America/Mexico_city")->toDateTimeString());
        $license = License::find($data->id);

        
        $license->Linderos = $data->linderos;
        $license->Observaciones = $data->observations;
        $license->Superficie = is_null($data->Superficie)? $license->Superficie : ($data->Superficie);
        $license->EmpleosCreados = is_null($data->EmpleosCreados)? $license->EmpleosCreados: $data->EmpleosCreados ;
        $license->NombreNegocio = $data->name_b;
        $license->save();
        $now = Carbon::now("America/Mexico_City")->format("Y");
        $adeudo = 0;
        $cost = 0;
        //eliminar los cargos antiguos de las licencias que no tengan adeudo
        $idAdeudos = $license->debtsArrayReference();
        DB::connection("sqlsrv")->table("Adeudos")
        ->whereIn("Referencia", $idAdeudos)
        ->where("NumeroLicencia", $license->NumeroLicencia)->delete();
        
        $license->turns()->detach();

        if(is_null($data->cuenta_serach))
        {
            foreach ($data->cuenta_serach as $key => $turn_id) {
                $i = $key + 1;
                $turn = $this->getTurnById($turn_id);
                $obs = $data->observations_search[$key];
                $adeudo = $turn->CuotaFija * floatval($data->quantity_search[$key]);
                $quantityTurn = floatval($data->quantity_search[$key]);
                $license->turns()->attach($turn->IdGiro, ['Secuencia' => $i, 
                                                            'Fecha' => str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()), 
                                                            'Descripcion' => $turn->Nombre, 
                                                            'Observacion' => $obs, 
                                                            'Cuota' => $turn->CuotaFija, 
                                                            'Cantidad' => floatval($data->quantity_search[$key]), 
                                                            'LicenciaAnexo' => 0]);
    
                                            
                $conpto = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $turn->Concepto)->first();
                $conpto_ref = $turn->concepts()->where("TiposTramite.TipoTramite", "R")->first();
                $conpto_ref = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $conpto_ref->pivot->Concepto)->first();
                $descarga = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $turn->ConceptoDescarga)->first();
                $recoleccion = DB::connection("sqlsrv")->table("Conceptos")->where("Concepto", "=", $turn->ConceptoRecoleccion)->first();
                // dd($conpto_ref, $conpto);
                $year = Carbon::parse(str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()))->format("Y");
                $trimester = $this->getActualTrimester(str_replace(" ", "T", Carbon::parse($data->date_search[$key])->toDateTimeString()));
                $basura = false;
                $location = DB::connection("sqlsrv")->table("Poblaciones")->where("IdPoblacion", $license->IdPoblacion)->first();
                if($location->Basura == 1){
                    if($turn->Basura == 1){
                        $basura = true;
                    }
                }
    
                if($year == $now){
                    $cost += $adeudo * ($trimester["percentage"] / 100);
                    //en caso de que el adeudo ya esté cargado no se vuelve a ingresar
                    $adeudoC = Debts::where("Departamento", "02")->where("Concepto", strval($turn->Concepto))->where("CuentaDepto", strval($license->NumeroLicencia))->where("Referencia", $year)->first();
                    if(is_null($adeudoC)){
                        Debts::create([
                            "Departamento" => "02",
                            "Concepto" => strval($turn->Concepto),
                            "CuentaDepto" => strval($license->NumeroLicencia),
                            "Referencia" => $year,
                            "FechaMovto" => $now,
                            "NumeroLicencia" => strval($license->NumeroLicencia),
                            "FechaVence" => $now,
                            "Descripcion" => $conpto->Descripcion,
                            "Importe" => $adeudo * ($trimester["percentage"] / 100),
                            "Estado" => "A",
                            "Referencia2" => ($turn->Alcohol == 1) ? 'Alcohol' : '',
                            "Cargo" => 1,
                        ]);
                    }
    
    
                    if($basura){
                        $adeudosBasuraDescarga = Debts::where("Departamento", "02")
                            ->where("Concepto", strval($descarga->Concepto))
                            ->where("CuentaDepto", strval($license->NumeroLicencia))
                            ->where("Descripcion", $descarga->Descripcion)->where("Referencia", $year)->first();
                        if(is_null($adeudosBasuraDescarga)){
                            Debts::create([
                                "Departamento" => "02",
                                "Concepto" => strval($descarga->Concepto),
                                "CuentaDepto" => strval($license->NumeroLicencia),
                                "Referencia" => $year,
                                "FechaMovto" => $now,
                                "NumeroLicencia" => strval($license->NumeroLicencia),
                                "FechaVence" => $now,
                                "Descripcion" => $descarga->Descripcion,
                                "Importe" => ($turn->CostoDescarga * ($trimester["percentage"] / 100)),
                                "Estado" => "A",
                                "Referencia2" => 'Basura',
                                "Cargo" => 1,
                            ]);
                        }
                        
                        $adeudosBasuraRecoleccion= Debts::where("Departamento", "02")
                            ->where("Concepto", strval($recoleccion->Concepto))
                            ->where("CuentaDepto", strval($license->NumeroLicencia))
                            ->where("Descripcion", $recoleccion->Descripcion)->where("Referencia", $year)->first();
                        if(is_null($adeudosBasuraDescarga))
                        {
                            Debts::create([
                                "Departamento" => "02",
                                "Concepto" => strval($recoleccion->Concepto),
                                "CuentaDepto" => strval($license->NumeroLicencia),
                                "Referencia" => $year,
                                "FechaMovto" => $now,
                                "NumeroLicencia" => strval($license->NumeroLicencia),
                                "FechaVence" => $now,
                                "Descripcion" => $recoleccion->Descripcion,
                                "Importe" => ($turn->CostoRecoleccion * ($trimester["percentage"] / 100)),
                                "Estado" => "A",
                                "Referencia2" => 'Basura',
                                "Cargo" => 1,
                            ]);
                        }
                        
                    }
                }else{
                    foreach ($turn->periods() as $key => $period) {
                        $adeudo = $period->CuotaFija * $quantityTurn;
                        if($period->Ano == $year){
                            $cost += $adeudo * ($trimester["percentage"] / 100);
    
                            //verificamos que no se haya cargado el adeudo de la licencia con el giro nuevo
                            $adeudoC = Debts::where("Departamento", "02")->where("Concepto", strval($turn->Concepto))
                            ->where("CuentaDepto", strval($license->NumeroLicencia))->where("Referencia", $period->Ano)->first();
                            if(is_null($adeudoC)){
                                Debts::create([
                                    "Departamento" => "02",
                                    "Concepto" => strval($turn->Concepto),
                                    "CuentaDepto" => strval($license->NumeroLicencia),
                                    "Referencia" => $period->Ano,
                                    "FechaMovto" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                    "NumeroLicencia" => strval($license->NumeroLicencia),
                                    "FechaVence" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                    "Descripcion" => $conpto->Descripcion,
                                    "Importe" => $adeudo * ($trimester["percentage"] / 100),
                                    "Estado" => "A",
                                    "Referencia2" => ($turn->Alcohol == 1) ? 'Alcohol' : '',
                                    "Cargo" => 1,
                                ]);
                            }
                            
                            if($basura){
                                $adeudosBasuraDescarga = Debts::where("Departamento", "02")
                                    ->where("Concepto", strval($descarga->Concepto))
                                    ->where("CuentaDepto", strval($license->NumeroLicencia))
                                    ->where("Descripcion", $descarga->Descripcion)->where("Referencia", $period->Ano)->first();
                                if(is_null($adeudosBasuraDescarga))
                                {
                                    Debts::create([
                                        "Departamento" => "02",
                                        "Concepto" => strval($descarga->Concepto),
                                        "CuentaDepto" => strval($license->NumeroLicencia),
                                        "Referencia" => $period->Ano,
                                        "FechaMovto" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                        "NumeroLicencia" => strval($license->NumeroLicencia),
                                        "FechaVence" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                        "Descripcion" => $descarga->Descripcion,
                                        "Importe" => ($turn->CostoDescarga * ($trimester["percentage"] / 100)),
                                        "Estado" => "A",
                                        "Referencia2" => 'Basura',
                                        "Cargo" => 1,
                                    ]);
                                }
                                
    
                                $adeudosBasuraRecoleccion= Debts::where("Departamento", "02")
                                ->where("Concepto", strval($recoleccion->Concepto))
                                ->where("CuentaDepto", strval($license->NumeroLicencia))
                                ->where("Descripcion", $recoleccion->Descripcion)->where("Referencia", $period->Ano)->first();
    
                                if(is_null($adeudosBasuraRecoleccion))
                                {
                                    Debts::create([
                                        "Departamento" => "02",
                                        "Concepto" => strval($recoleccion->Concepto),
                                        "CuentaDepto" => strval($license->NumeroLicencia),
                                        "Referencia" => $period->Ano,
                                        "FechaMovto" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                        "NumeroLicencia" => strval($license->NumeroLicencia),
                                        "FechaVence" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                        "Descripcion" => $recoleccion->Descripcion,
                                        "Importe" => ($turn->CostoRecoleccion * ($trimester["percentage"] / 100)),
                                        "Estado" => "A",
                                        "Referencia2" => 'Basura',
                                        "Cargo" => 1,
                                    ]);
                                }
                                
                            }
                        }elseif($period->Ano > $year && $period->Ano <= $now){
                            $cost += is_numeric($period->CuotaMinima)? ($period->CuotaMinima * $quantityTurn) : $adeudo;
    
                            //verificamos que no se haya cargado el adeudo de la licencia con el giro nuevo
                            $adeudoC = Debts::where("Departamento", "02")->where("Concepto", strval($conpto_ref->Concepto))
                            ->where("CuentaDepto", strval($license->NumeroLicencia))->where("Referencia", $period->Ano)->first();
    
                            if(is_null($adeudoC))
                            {
                                Debts::create([
                                    "Departamento" => "02",
                                    "Concepto" => strval($conpto_ref->Concepto),
                                    "CuentaDepto" => strval($license->NumeroLicencia),
                                    "Referencia" => $period->Ano,
                                    "FechaMovto" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                    "NumeroLicencia" => strval($license->NumeroLicencia),
                                    "FechaVence" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                    "Descripcion" => $conpto_ref->Descripcion,
                                    "Importe" => is_numeric($period->CuotaMinima)? ($period->CuotaMinima * $quantityTurn) : $adeudo,
                                    "Estado" => "A",
                                    "Referencia2" => ($turn->Alcohol == 1) ? 'Alcohol' : '',
                                    "Cargo" => 1,
                                ]);
                            }
                            
                            if($basura){
                                $adeudosBasuraDescarga = Debts::where("Departamento", "02")
                                    ->where("Concepto", strval($descarga->Concepto))
                                    ->where("CuentaDepto", strval($license->NumeroLicencia))
                                    ->where("Descripcion", $descarga->Descripcion)->where("Referencia", $period->Ano)->first();
    
                                if(is_null($adeudosBasuraDescarga)){
                                    Debts::create([
                                        "Departamento" => "02",
                                        "Concepto" => strval($descarga->Concepto),
                                        "CuentaDepto" => strval($license->NumeroLicencia),
                                        "Referencia" => $period->Ano,
                                        "FechaMovto" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                        "NumeroLicencia" => strval($license->NumeroLicencia),
                                        "FechaVence" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                        "Descripcion" => $descarga->Descripcion,
                                        "Importe" => $turn->CostoDescarga,
                                        "Estado" => "A",
                                        "Referencia2" => 'Basura',
                                        "Cargo" => 1,
                                    ]);
                                }
    
                                $adeudosBasuraRecoleccion= Debts::where("Departamento", "02")
                                ->where("Concepto", strval($recoleccion->Concepto))
                                ->where("CuentaDepto", strval($license->NumeroLicencia))
                                ->where("Descripcion", $recoleccion->Descripcion)->where("Referencia", $period->Ano)->first();
                                if(is_null($adeudosBasuraRecoleccion))
                                {
                                    Debts::create([
                                        "Departamento" => "02",
                                        "Concepto" => strval($recoleccion->Concepto),
                                        "CuentaDepto" => strval($license->NumeroLicencia),
                                        "Referencia" => $period->Ano,
                                        "FechaMovto" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                        "NumeroLicencia" => strval($license->NumeroLicencia),
                                        "FechaVence" => Carbon::parse($period->Ano."-01-01")->toDateString(),
                                        "Descripcion" => $recoleccion->Descripcion,
                                        "Importe" => $turn->CostoRecoleccion,
                                        "Estado" => "A",
                                        "Referencia2" => 'Basura',
                                        "Cargo" => 1,
                                    ]);
                                }
                                
                            }
                        }
                    }
                }
                
            }
        }

        //
        return $license;
    }

    public function getTurnAnounce()
    {
        $anounces = DB::connection("sqlsrv")->table("Giros")->where("Nombre", "like", "%Anuncio%")->pluck("IdGiro")->toArray();
        return DB::connection("sqlsrv")->table("GiroDetalleLic")->whereIn("IdGiro", $anounces)->distinct()->pluck("NumeroLicencia")->ToArray();
    }

    //obtener licencias que si tienen georeferenciacion
    public function getLicensesWithPoints()
    {
        return License::where("CoordenadaEste", "!=", "0")->where("CoordenadaNorte", "!=", "0")->has("requests")->get();
    }

    public function getAvailableYearsDebts()
    {
        $years = DB::connection('sqlsrv')->select(DB::raw('SELECT YEAR(FechaMovto) as Year FROM Adeudos GROUP BY YEAR(FechaMovto) ORDER BY YEAR(FechaMovto) DESC'));
        return $years;
    }

    public function licensesWithDebts($request)
    {
        $poblacion = DB::connection('sqlsrv')->table('Poblaciones')->where('IdPoblacion', $request->poblacion)->first();
        $licensesArray = [
            'licencias' => [],
            'totalLicencias' => 0,
            'total' => 0,
            'poblacion' => 'Sin Poblacion'
        ];

        if (!empty($poblacion))
        {
            $licensesArray['poblacion'] = $poblacion->NombrePoblacion;

            $total = DB::connection('sqlsrv')
                    ->select(DB::raw("SELECT SUM(Adeudos.Importe) AS Importe
                    FROM Adeudos 
                    INNER JOIN Licencias ON Adeudos.CuentaDepto = Licencias.NumeroLicencia
                    WHERE Licencias.IdPoblacion = CAST($request->poblacion AS INT)
                    "));

            $licensesArray['total'] = (float)$total[0]->Importe;
    
            $licenses = DB::connection('sqlsrv')
                    ->select(DB::raw("SELECT Licencias.NumeroLicencia, Licencias.NombreNegocio, Licencias.Exterior, Licencias.Interior, Licencias.FechaAlta, PadronContribuyentes.NombreCompleto as Contribuyente, SUM(Adeudos.Importe) AS Importe
                    FROM Licencias
                    INNER JOIN Adeudos ON Licencias.NumeroLicencia = Adeudos.CuentaDepto
                    INNER JOIN Poblaciones ON Licencias.IdPoblacion = Poblaciones.IdPoblacion
                    INNER JOIN PadronContribuyentes ON Licencias.IdContribuyente = PadronContribuyentes.IdContribuyente
                    WHERE Licencias.IdPoblacion = CAST($request->poblacion AS INT)
                    GROUP BY Licencias.NumeroLicencia, PadronContribuyentes.NombreCompleto, Licencias.NombreNegocio, Licencias.Exterior, Licencias.Interior, Licencias.FechaAlta
                    ORDER BY Importe DESC
                    "));
    

            $totalLicencias = 0;
            foreach ($licenses as $license)
            {
                $licenseModel = License::where('NumeroLicencia', $license->NumeroLicencia)->first();
    
                if (!empty($licenseModel))
                {
                    // Giros
                    $girosText = '';
                    foreach ($licenseModel->turns as $key => $turn)
                    {
                        if ($key == 0)
                        {
                            $girosText .= (string)$turn->Nombre;
                        }else
                        {
                            $girosText .= ', '.(string)$turn->Nombre;
                        }
                    }
    
                    // Licencia
                    $licenseData = [
                        'NumeroLicencia' => $license->NumeroLicencia,
                        'Contribuyente' => $license->Contribuyente,
                        'NombreNegocio' => $license->NombreNegocio,
                        'Calle' => is_null($licenseModel->street())? "": $licenseModel->street()->NombreCalle,
                        'Exterior' => $license->Exterior,
                        'Interior' => $license->Interior,
                        'Colonia' => is_null($licenseModel->colony())? "": $licenseModel->colony()->NombreColonia,
                        'Poblacion' => is_null($licenseModel->location())? "": $licenseModel->location()->NombrePoblacion,
                        'Giro' => $girosText,
                        'Importe' => $license->Importe,
                        'FechaAlta' => substr($license->FechaAlta, 0, 10),
                    ];
    
                    $licensesArray['licencias'][] = $licenseData;
                    $totalLicencias++;
                }
            }

            $licensesArray['totalLicencias'] = $totalLicencias;
        }

        return $licensesArray;
    }

    public function getDebtsByLocation($IdPoblacion)
    {
        $total = DB::connection('sqlsrv')
                    ->select(DB::raw("SELECT SUM(Adeudos.Importe) AS Importe
                    FROM Adeudos 
                    INNER JOIN Licencias ON Adeudos.CuentaDepto = Licencias.NumeroLicencia
                    WHERE Licencias.IdPoblacion = CAST($IdPoblacion AS INT)
                    "));

        return (float)$total[0]->Importe;
    }

    public function getNumberOfLicensesWithDebts($IdPoblacion)
    {
        $total = DB::connection('sqlsrv')
                    ->select(DB::raw("SELECT Licencias.NumeroLicencia
                    FROM Licencias 
                    INNER JOIN Adeudos ON Licencias.NumeroLicencia = Adeudos.CuentaDepto
                    WHERE Licencias.IdPoblacion = CAST($IdPoblacion AS INT)
                    GROUP BY Licencias.NumeroLicencia
                    "));

        return count($total);
    }

    public function getTotalDebts()
    {
        $total = DB::connection('sqlsrv')
                    ->select(DB::raw("SELECT SUM(Adeudos.Importe) AS Importe
                    FROM Adeudos 
                    INNER JOIN Licencias ON Adeudos.NumeroLicencia = Licencias.NumeroLicencia
                    "));

        return (float)$total[0]->Importe;
    }
}