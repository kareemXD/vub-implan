<?php
namespace App\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
//Models
use App\User;

use App\Models\Du\Process;
use App\Models\Du\Requirement;
use App\Models\Du\Solicitud;
use App\Models\Du\Taxpayer;
use App\Models\Du\Comment;
use App\Models\Du\Payments;
use App\Models\Du\SolicitudDocumento;
use App\Models\Du\Concepto;
use App\Models\Du\Ingresos_Concepto;
use App\Models\Padron\Debts;
use App\Models\Padron\Payment;
use App\Models\ProteccionCivil\Request as ProteccionCivilRequest;
use App\Repositories\ProteccionCivil\FormalityRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DuRepository
{
   function getAllProcedures(){
      return Process::all();
   }

   public function getAvailableProceduresByRequest($requestId)
   {
      $procedures = Process::whereDoesntHave('requests', function(Builder $query) use($requestId) {
                                 $query->where('id', $requestId);
                              })->get();

      return $procedures;
   }

   function getAllRequirements(){
      return Requirement::all();
   }

   function getProcessById($id){
      return Process::find($id);
   }

   function storeProcess($data)
   {
      $process = new Process;
      $process->nombre = $data->name;
      $process->save();

      if (!empty($data->validate_requirement))
      {
         foreach ($data->validate_requirement as $key => $requirement) {
            $process->requirements()->attach($requirement);
         }
      }

      if(!empty($data->requirement))
      {
         foreach ($data->requirement as $key => $requirement) 
         {
            $requ = new Requirement;
            $requ->nombre = $requirement;
            $requ->save();
   
            $process->requirements()->attach($requ->id);
         }
      }
   }
   
   function storeUpdateProcess($data)
   {
      $process = Process::find($data->id);
      $process->nombre = $data->name;
      $process->save();

      $process->requirements()->detach();
      if (!empty($data->validate_requirement))
      {
         foreach ($data->validate_requirement as $key => $requirement) 
         {
            $process->requirements()->attach($requirement);
         }
      }

      if(!empty($data->requirement))
      {
         foreach ($data->requirement as $key => $requirement) 
         {
            if (!empty($requirement))
            {
               $requ = new Requirement;
               $requ->nombre = $requirement;
               $requ->save();
      
               $process->requirements()->attach($requ->id);
            }
         }
      }
   }

   function getDuRequests(){
      return Solicitud::all();
   }

   public function getDuRequestsBySearch($search)
   {
      $account = $search['cuenta'];
      $contribuyente = $search['contribuyente'];
      $dictaminator = $search['dictaminator'];
      $inspector = $search['inspector'];

      $requests = DB::table('du_solicitudes')->join('du_contribuyentes', 'du_solicitudes.id_contribuyente', '=', 'du_contribuyentes.id');

      if (!empty($dictaminator))
      {
         $requests = $requests->where('du_solicitudes.id_dictaminator', $dictaminator);
      }

      if (!empty($inspector))
      {
         $requests = $requests->where('du_solicitudes.id_inspector', $inspector);
      }

      if (!empty($account))
      {
         $requests = $requests->orWhere('du_solicitudes.numero_solicitud', 'like', '%'.$account.'%');
      }

      if (!empty($contribuyente))
      {
         $taxpayersWithoutFullname = Taxpayer::whereNull('fullname')->get();
         foreach ($taxpayersWithoutFullname as $taxpayer)
         {
            $taxpayer->fullname = $taxpayer->nombre.' '.$taxpayer->apellido_pat.' '.$taxpayer->apellido_mat;
            $taxpayer->save();
         }

         $requests = $requests->orWhere('du_contribuyentes.fullname', 'like', '%'.$contribuyente.'%');
      }

      $requestSearching = $requests->pluck('du_solicitudes.id')->toArray();

      $requests = Solicitud::whereIn('id', $requestSearching)->get();

      return $requests;
   }
   
   public function getRequestById($id)
   {
      return Solicitud::find($id);
   }

   function getRequirementsById($id){
      $process = Process::find($id);
      return $process->requirements;
   }

   function storeRequest($data)
   {
      $taxpayer = new Taxpayer;
      $taxpayer->nombre = $data->nombre; 
      $taxpayer->apellido_pat = $data->apellido_pat;
      $taxpayer->apellido_mat = $data->apellido_mat;
      $taxpayer->fullname = $data->nombre.' '.$data->apellido_pat.' '.$data->apellido_mat;
      $taxpayer->correo = $data->email;
      $taxpayer->telefono = $data->telefono;
      $taxpayer->edad = $data->edad;
      $taxpayer->sexo = $data->sexo;
      $taxpayer->rfc = $data->rfc;
      $taxpayer->persona = (isset($data->persona))? 1 : 0;
      $taxpayer->razon_social = $data->razon;
      $taxpayer->telefono_trab = $data->telefono_trab;
      $taxpayer->rfc_trab = $data->rfc_trab;
      $taxpayer->save();

      // Validate new Colony
      $colonyId = $data->colony;
      if (!empty($data->newcolony))
      {
         $colonyRepository = new ColonyRepository;
         $colony = $colonyRepository->storeOrUpdateColony($data->newcolony, $data->city);

         if (!empty($colony))
         {
            $colonyId = $colony->IdColonia;
         }
      }

      // Validate newdomicilio2
      $streetId = $data->domicilio2;
      if (!empty($data->newdomicilio2))
      {
         $streetRepository = new StreetRepository;
         $street = $streetRepository->storeOrUpdateStreet($data->newdomicilio2, $data->city);

         if (!empty($street))
         {
            $streetId = $street->IdCalle;
         }
      }

      // Validate newcalle1_u
      $streetReference1Id = $data->calle1_u;
      if (!empty($data->newcalle1_u))
      {
         $streetRepository = new StreetRepository;
         $street = $streetRepository->storeOrUpdateStreet($data->newcalle1_u, $data->city);

         if (!empty($street))
         {
            $streetReference1Id = $street->IdCalle;
         }
      }

      // Validate newcalle2_u
      $streetReference2Id = $data->calle2_u;
      if (!empty($data->newcalle2_u))
      {
         $streetRepository = new StreetRepository;
         $street = $streetRepository->storeOrUpdateStreet($data->newcalle2_u, $data->city);

         if (!empty($street))
         {
            $streetReference2Id = $street->IdCalle;
         }
      }

      $lastRequest = Solicitud::select(DB::raw("Cast(LEFT(numero_solicitud, 10) as INT) as numero_solicitud"))->orderBy("numero_solicitud","DESC")->first();
      if(!empty($lastRequest) && (int)$lastRequest->numero_solicitud >= config('system.du.licenses.initialFolio'))
      {
         $requestId = (int)$lastRequest->numero_solicitud + 1;
      }else
      {
         $requestId = config('system.du.licenses.initialFolio');
      }

      $request = new Solicitud;
      $request->numero_solicitud = $requestId;
      $request->id_contribuyente = $taxpayer->id;
      $request->id_poblacion = $data->city;
      $request->id_colonia = $colonyId;
      $request->id_calle = $streetId;
      $request->id_cruce1 = $streetReference1Id;
      $request->id_cruce2 = $streetReference2Id;
      $request->no_exterior = $data->ext;
      $request->no_interior = $data->int;
      $request->estatus = config('system.du.requests.statuses.created');
      $request->coordenada_este = $data->este;
      $request->coordenada_norte = $data->norte;
      $request->observaciones = $data->observations;
      $request->clave_catastral = $data->clave;
      $request->gestor = $data->gestor;
      $request->id_user = auth()->user()->id;
      $request->save();

      $request->procedures()->sync($data->process);

      $requirements = Requirement::join('du_requerimientos_tramite', 'du_requerimientos.id', '=', 'du_requerimientos_tramite.id_requerimiento')
                                    ->where(function($query) use($data) {
                                       $query->whereIn('du_requerimientos_tramite.id_tramite', $data->process);
                                    })
                                    ->select('du_requerimientos.id')
                                    ->distinct()
                                    ->get();

      foreach ($requirements as $requirement)
      {
         $request->documents()->attach($requirement->id, ['estatus' => 0]);
      }

      return $request;
   }
  
   function updateRequest($data){
      $taxpayer = Taxpayer::find($data->contribuyente);
      $taxpayer->nombre = $data->nombre; 
      $taxpayer->apellido_pat = $data->apellido_pat;
      $taxpayer->apellido_mat = $data->apellido_mat;
      $taxpayer->fullname = $data->nombre.' '.$data->apellido_pat.' '.$data->apellido_mat;
      $taxpayer->correo = $data->email;
      $taxpayer->telefono = $data->telefono;
      $taxpayer->edad = $data->edad;
      $taxpayer->sexo = $data->sexo;
      $taxpayer->rfc = $data->rfc;
      $taxpayer->persona = (isset($data->persona))? 1 : 0;
      $taxpayer->razon_social = $data->razon;
      $taxpayer->telefono_trab = $data->telefono_trab;
      $taxpayer->rfc_trab = $data->rfc_trab;
      $taxpayer->save();

      // Validate new Colony
      $colonyId = $data->colony;
      if (!empty($data->newcolony))
      {
         $colonyRepository = new ColonyRepository;
         $colony = $colonyRepository->storeOrUpdateColony($data->newcolony, $data->city);

         if (!empty($colony))
         {
            $colonyId = $colony->IdColonia;
         }
      }

      // Validate newdomicilio2
      $streetId = $data->domicilio2;
      if (!empty($data->newdomicilio2))
      {
         $streetRepository = new StreetRepository;
         $street = $streetRepository->storeOrUpdateStreet($data->newdomicilio2, $data->city);

         if (!empty($street))
         {
            $streetId = $street->IdCalle;
         }
      }

      // Validate newcalle1_u
      $streetReference1Id = $data->calle1_u;
      if (!empty($data->newcalle1_u))
      {
         $streetRepository = new StreetRepository;
         $street = $streetRepository->storeOrUpdateStreet($data->newcalle1_u, $data->city);

         if (!empty($street))
         {
            $streetReference1Id = $street->IdCalle;
         }
      }

      // Validate newcalle2_u
      $streetReference2Id = $data->calle2_u;
      if (!empty($data->newcalle2_u))
      {
         $streetRepository = new StreetRepository;
         $street = $streetRepository->storeOrUpdateStreet($data->newcalle2_u, $data->city);

         if (!empty($street))
         {
            $streetReference2Id = $street->IdCalle;
         }
      }
      
      $request = Solicitud::findOrFail($data->solicitud);
      $request->id_poblacion = $data->city;
      $request->id_colonia = $colonyId;
      $request->id_calle = $streetId;
      $request->id_cruce1 = $streetReference1Id;
      $request->id_cruce2 = $streetReference2Id;
      $request->no_exterior = $data->ext;
      $request->no_interior = $data->int;
      $request->coordenada_este = $data->este;
      $request->coordenada_norte = $data->norte;
      $request->observaciones = $data->observations;
      $request->clave_catastral = $data->clave;
      $request->save();

      $request->procedures()->sync($data->process);

      return $request;
   }

   public function getFullAddressByRequest($requestId)
   {
      $request = Solicitud::find($requestId);

      $address = $request->street->NombreCalle." #".$request->no_exterior;

      if (!empty($request->no_interior))
      {
         $address .= " int:".$request->no_interior;
      }

      $address .= ", ".$request->colony->NombreColonia.", ".$request->population->NombrePoblacion.", Bahía de Banderas, Nayarit.";

      return $address;
   }

   function getDocumentByRequest($solicitud, $requisito){
      // dd($solicitud, $requisito);
      $request = Solicitud::find($solicitud);
      $document = $request->documents()->where(DB::raw("du_requerimientos.id"), $requisito)->first();
      return $document->getOriginal();
   }

   public function updateDocumentRequest($data)
   {
        $request = Solicitud::find($data->solicitud_id);
        $document = Requirement::find($data->requisito_id);

        if(isset($data->document))
        {
            $file = $data->document;
            $ex = $file->getClientOriginalExtension();
            $name = $this->limpiarCaracteresEspeciales($document->nombre);
            $name = strtolower($name);
            $name = str_replace(" ", "_", $name);

            if(Storage::disk('public')->putFileAs('documentacion_solicitudes_du/'.$request->id, $file, $name.".".$ex))
            {
                $request->documents()->updateExistingPivot($document->id, ['comentario' => $data->comments, 'archivo' => $name.".".$ex, 'estatus' => 1]);
            }
        }else {
            $request->documents()->updateExistingPivot($document->id, ['comentario' => $data->comments]);
        }

        return $request;
   }

   public function updatePcRequirementRequest($data)
   {
      $request = $this->getRequestById($data->solicitud_id);

      if (!empty($data->document))
      {
         // dd($request->pcRequirements);
         // Revisar si ya cuenta con el mismo requisito para actualizarlo.
         $requestRequirement = $request->pcRequirements()->where('id_requisito', $data->requisito_id)->first();

         // Subir el documento nuevo y obtener la url.
         $fileName = $request->id.'-'.Str::random(10).'.'.$data->file('document')->extension();
         $data->file('document')->storeAs('public/du/requests/'.$request->id.'/requirements', $fileName);
         $fileUrl = 'du/requests/'.$request->id.'/requirements/'.$fileName;

         // Validar que haya un requisito anterior para eliminarlo.
         if (!empty($requestRequirement))
         {
               // Validar que exista el archivo antes de quererlo eliminar.
               if (Storage::disk('public')->exists($requestRequirement->documento))
               {
                  Storage::disk('public')->delete($requestRequirement->documento);
               }

               // Solo actualizar la url del documento.
               $requestRequirement->documento = $fileUrl;
               $requestRequirement->save();
         }else
         {
            // Generar el nuevo registro del requisito.
            $request->pcRequirements()->create([
               'id_requisito' => $data->requisito_id,
               'documento' => $fileUrl,
            ]);
         }
      }

      return $request;
   }

   public function requestPcFormalityStore($data)
   {
      $request = $this->getRequestById($data->solicitud_id);
      
      $lastPCRequest = ProteccionCivilRequest::orderBy('id', 'desc')->first();
      $numeroSolicitud = config('system.pc.requests.initialFolio'); // Contador Inicial

      if (!empty($lastPCRequest))
      {
         $numeroSolicitud = (int)$lastPCRequest->numero_solicitud > $numeroSolicitud ? (int)$lastPCRequest->numero_solicitud : $numeroSolicitud;
      }
      $numeroSolicitud = str_pad(($numeroSolicitud + 1), 4, '0', STR_PAD_LEFT);
      $taxPayer = $request->taxpayer;
      
      $PCRequest = new ProteccionCivilRequest;
      $PCRequest->numero_solicitud = $numeroSolicitud;
      $PCRequest->id_poblacion = $request->id_poblacion;
      $PCRequest->id_colonia = $request->id_colonia;
      $PCRequest->id_calle = $request->id_calle;
      $PCRequest->id_cruce1 = $request->id_cruce1;
      $PCRequest->id_cruce2 = $request->id_cruce2;
      $PCRequest->no_exterior = $request->no_exterior;
      $PCRequest->no_interior = $request->no_interior;
      $PCRequest->coordenada_este = $request->coordenada_este;
      $PCRequest->coordenada_norte = $request->coordenada_norte;
      $PCRequest->fecha_solicitud = now()->format('Y-m-d');
      $PCRequest->nombre = $taxPayer->fullname;
      $PCRequest->giro = null;
      $PCRequest->rfc = $taxPayer->rfc;
      $PCRequest->no_licencia = null;
      $PCRequest->estatus_licencia = 'N/A';
      $PCRequest->nombre_comercial = null;
      $PCRequest->no_empleados = null;
      $PCRequest->horario_laboral = null;
      $PCRequest->nombre_solicitante = !empty($taxPayer->fullname)? $taxPayer->fullname : $request->gestor;
      $PCRequest->caracter_juridico_solicitante = null;
      $PCRequest->telefono_celular = $taxPayer->telefono;
      $PCRequest->telefono_fijo = $taxPayer->telefono_trab;
      $PCRequest->email = $taxPayer->correo;
      $PCRequest->otro_tramite = null;
      $PCRequest->origen = 'Desarrollo Urbano';
      $PCRequest->estatus = config('system.pc.requests.statuses.pending'); // Pendiente
      $PCRequest->save();

      // Agregar tramite de visto bueno a la solicitud
      $formalityId = 1;
      $formalityRepository = new FormalityRepository; 
      $formality = $formalityRepository->getFormalityById($formalityId);
      if (!empty($formality))
      {
         $formalityCost = $formality->costByYear(now()->year);
         $PCRequest->formalities()->create([
               'id_tramite' => $formalityId,
               'costo' => $formalityCost->costo,
               'year' => $formalityCost->year
         ]);
      }

      // Asociar requisitos a la solicitud
      if (!empty($request->pcRequirements))
      {
         foreach ($request->pcRequirements as $pcRequirement)
         {
               $PCRequest->requirements()->create([
                  'id_requisito' => $pcRequirement->id_requisito,
                  'documento' => $pcRequirement->documento,
               ]);
         }
      }

      $request->pcRequests()->create([
         'id_pc_solicitud' => $PCRequest->id
      ]);

      return $PCRequest;
   }

   function getDuUsers(){
      return User::permission(['write_du_requests', 'read_du_requests'])->get();
   }

   function storeRequestObservation($data){
      $request = Solicitud::find($data->solicitud);

      $comment = new Comment();
      $comment->id_solicitud = $request->id;
      $comment->id_usuario = Auth::user()->id;
      $comment->observaciones = $data->comment;
      $comment->estatus = $request->estatus;
      $comment->save();
   }

   function storeRequestPayment($data)
   {
      $request = Solicitud::find($data->solicitud);
      $concept = Concepto::findOrFail($data->concepto);
      $importe = (float)$concept->Importe;

      if($importe == 0)
      {
         return false;
      }else
      {
         $total = $importe;

         //calcula el con
         if($concept->UAN == "1")
         {
            $total += ($importe * 0.12);
         }

         if($concept->OverCost == "1")
         {
            $total += ($importe * 0.005);
         }

         $payment = new Payments();
         $payment->id_solicitud = $request->id;
         $payment->concepto_id = $concept->id;
         $payment->concepto = $concept->Concepto;
         $payment->subtotal = $importe;
         $payment->uan = $concept->UAN;
         $payment->sobre_costo = $concept->OverCost;
         $payment->total = $total;
         $payment->save();
      }
   }

   public function removePaymentConcept($data)
   {
      $payment = Payments::findOrFail($data->paymentId);
      $payment->delete();
   }
   
   function changeRequestToCreated($id)
   {
      $request = Solicitud::find($id);
      $request->estatus = config('system.du.requests.statuses.created');
      $request->save();
   }

   function changeRequestToVerify($id)
   {
      $request = Solicitud::find($id);
      $request->estatus = config('system.du.requests.statuses.verification');
      $request->save();
   }

   function changeRequestToInspect($id, $inspector)
   {
      $request = Solicitud::find($id);
      $request->id_inspector = $inspector;
      $request->estatus = config('system.du.requests.statuses.inspection');
      $request->save();

      $request->inspectors()->attach($inspector);
   }
   
   function changeRequestToDictamination($id, $dictaminator)
   {
      $request = Solicitud::find($id);
      $request->id_dictaminator = $dictaminator;
      $request->estatus = config('system.du.requests.statuses.dictamination');
      $request->save();

      $request->dictaminators()->attach($dictaminator);
   }

   function changeRequestToBackDictamination($id)
   {
      $request = Solicitud::find($id);
      $request->estatus = config('system.du.requests.statuses.dictamination');
      $request->save();
   }
   
   function changeRequestToConfirmation($id)
   {
      $request = Solicitud::find($id);
      $request->estatus = config('system.du.requests.statuses.confirmation');
      $request->save();
   }

   function changeRequestToPayValidation($id)
   {
      $request = Solicitud::find($id);
      $request->estatus = config('system.du.requests.statuses.payValidation');
      $request->save();

      $license = 'UAM-'.$request->numero_solicitud.'/'.Carbon::parse($request->created_at)->format('y');
      Debts::where('CuentaDepto', $license)->delete();

      // Generar Adeudo
      if(count($request->payments) > 0)
      {
         foreach ($request->payments as $paymentKey => $payment) 
         {
            if (!empty($payment->concept))
            {
               $duConcepto = $payment->concept;

               if (!empty($duConcepto->ingresosConcepto))
               {
                  $ingresosConcepto = $duConcepto->ingresosConcepto;
                  $date = now()->format('Y-m-d').' 00:00:00';

                  Debts::create([
                     "Departamento" => config('system.departments.desarrollorUrbano'), // Desarrollo urbano
                     "Concepto" => strval($ingresosConcepto->Concepto),
                     "CuentaDepto" => strval($license),
                     "Referencia" => strval($paymentKey),
                     "FechaMovto" => $date,
                     "NumeroLicencia" => substr($request->numero_solicitud, 0, 10),
                     "FechaVence" => $date,
                     "Descripcion" => $ingresosConcepto->Descripcion,
                     "Importe" => (string)$ingresosConcepto->Importe,
                     "Estado" => "A",
                     "Cargo" => 1,
                  ]);

                  if ($payment->uan == 1)
                  {
                     // Cobrar UAN concepto 99437
                     $UANIngresosConcepto = Ingresos_Concepto::find('99437');
                     if (!empty($UANIngresosConcepto))
                     {
                        $UANImporte = (float)$ingresosConcepto->Importe * 0.12;

                        Debts::create([
                           "Departamento" => config('system.departments.desarrollorUrbano'), // Desarrollo urbano
                           "Concepto" => strval($UANIngresosConcepto->Concepto),
                           "CuentaDepto" => strval($license),
                           "Referencia" => strval($paymentKey),
                           "FechaMovto" => $date,
                           "NumeroLicencia" => substr($request->numero_solicitud, 0, 10),
                           "FechaVence" => $date,
                           "Descripcion" => $UANIngresosConcepto->Descripcion,
                           "Importe" => $UANImporte,
                           "Estado" => "A",
                           "Cargo" => 1,
                        ]);
                     }
                  }

                  if ($payment->sobre_costo == 1)
                  {
                     // Cobrar OverCost concepto 99162
                     $overCostIngresosConcepto = Ingresos_Concepto::find('99162');
                     if (!empty($overCostIngresosConcepto))
                     {
                        $overCostImporte = (float)$ingresosConcepto->Importe * 0.005;

                        Debts::create([
                           "Departamento" => config('system.departments.desarrollorUrbano'), // Desarrollo urbano
                           "Concepto" => strval($overCostIngresosConcepto->Concepto),
                           "CuentaDepto" => strval($license),
                           "Referencia" => strval($paymentKey),
                           "FechaMovto" => $date,
                           "NumeroLicencia" => substr($request->numero_solicitud, 0, 10),
                           "FechaVence" => $date,
                           "Descripcion" => $overCostIngresosConcepto->Descripcion,
                           "Importe" => $overCostImporte,
                           "Estado" => "A",
                           "Cargo" => 1,
                        ]);
                     }
                  }
               }
            }
         }
      }
   }

   public function checkForPayment($id)
   {
      $request = Solicitud::find($id);

      // Por pagar
      $totalAdeudo = 0;
      foreach ($request->payments as $payment)
      {
         $totalAdeudo += (float)$payment->total;
      }

      // Pagado
      $totalPagos = 0;
      $license = 'UAM-'.$request->numero_solicitud.'/'.Carbon::parse($request->created_at)->format('y');
      $payments = Payment::where('CuentaDepto', $license)->get();
      foreach ($payments as $payment)
      {
         $totalPagos += (float)$payment->Importe;
      }

      $total = $totalAdeudo - $totalPagos;
      if ($total <= 0)
      {
         return true;
      }

      return false;
   }

   function changeRequestToPaid($id)
   {
      $request = Solicitud::find($id);
      $request->estatus = config('system.du.requests.statuses.paid');
      $request->save();
   }

   function changeRequestToFormats($id)
   {
      $request = Solicitud::find($id);
      $request->estatus = config('system.du.requests.statuses.formats');
      $request->save();
   }

   function changeRequestToAccept($id)
   {
      $request = Solicitud::find($id);
      $request->estatus = config('system.du.requests.statuses.accepted');
      $request->save();
   }

   function changeRequestToReject($id)
   {
      $request = Solicitud::find($id);
      $request->fecha_confirmacion = \Carbon\Carbon::now();
      $request->estatus = config('system.du.requests.statuses.rejected');
      $request->save();
   }

   function limpiarCaracteresEspeciales($cadena){
		
        //Reemplazamos la A y a
        $cadena = str_replace(
        array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
        array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
        $cadena
        );

        //Reemplazamos la E y e
        $cadena = str_replace(
        array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
        array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
        $cadena );

        //Reemplazamos la I y i
        $cadena = str_replace(
        array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
        array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
        $cadena );

        //Reemplazamos la O y o
        $cadena = str_replace(
        array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
        array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
        $cadena );

        //Reemplazamos la U y u
        $cadena = str_replace(
        array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
        array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
        $cadena );

        //Reemplazamos la N, n, C y c
        $cadena = str_replace(
        array('Ñ', 'ñ', 'Ç', 'ç'),
        array('N', 'n', 'C', 'c'),
        $cadena
        );
        
        return $cadena;
   }

   function getAllDebts()
   {
      return Concepto::where(function($query) {
                        $query->where('Status', 1);
                     })
                     ->orderBy('Articulo', 'asc')
                     ->get();
   }

   public function getDictaminators()
   {
      $dictaminators = User::join('model_has_roles', 'users.id', '=', 'model_id')
                     ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                     ->where(function($query) {
                        $query->where('roles.name', config('system.du.roles.dictaminator'))
                                 ->where('model_has_roles.model_type', 'App\User');
                     })
                     ->select('users.*')
                     ->distinct()
                     ->get();

      foreach ($dictaminators as $dictaminator)
      {
         $activeRequests = $this->getRequestsInDictaminationByUser($dictaminator->id);
         $dictaminator->activeRequests = $activeRequests;
         $dictaminator->countActiveRequests = count($activeRequests);
      }

      return $dictaminators;
   }

   public function getInspectors()
   {
      $inspectors = User::join('model_has_roles', 'users.id', '=', 'model_id')
                     ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                     ->where(function($query) {
                        $query->where('roles.name', config('system.du.roles.inspector'))
                                 ->where('model_has_roles.model_type', 'App\User');
                     })
                     ->select('users.*')
                     ->distinct()
                     ->get();

      foreach ($inspectors as $inspector)
      {
         $activeRequests = $this->getRequestsInInspectionByUser($inspector->id);
         $inspector->activeRequests = $activeRequests;
         $inspector->countActiveRequests = count($activeRequests);
      }

      return $inspectors;
   }

   public function getRequestsInDictaminationByUser($userId)
   {
      $requests = Solicitud::where(function($query) use($userId) {
                                 $query->where('id_dictaminator', $userId)
                                          ->whereIn('estatus', [3,5,6]);
                              })->get();
                           
      return $requests;
   }

   public function getRequestsInInspectionByUser($userId)
   {
      $requests = Solicitud::where(function($query) use($userId) {
                                 $query->where('id_inspector', $userId)
                                          ->where('estatus', 2);
                              })->get();
         
      return $requests;
   }

   public function storeFinalDocument($data)
   {
      $request = Solicitud::findOrFail($data->requestId);

      if(!empty($data->document))
      {
         $solicitudDocumento = new SolicitudDocumento;
         $solicitudDocumento->id_solicitud = $request->id;
         $solicitudDocumento->description = $data->description;
         $solicitudDocumento->save();

         $file = $data->document;
         $extension = $file->getClientOriginalExtension();
         $name = 'final_document_'.$solicitudDocumento->id.'_'.now()->valueOf();
         $name = $name.".".$extension;

         if(Storage::disk('public')->putFileAs('final_documents_solicitudes_du/'.$request->id, $file, $name))
         {
            $solicitudDocumento->file = $name;
            $solicitudDocumento->save();
         }
      }

      return $request;
   }

   public function deleteFinalDocument($data)
   {
      $request = Solicitud::findOrFail($data->requestId);
      $solicitudDocumento = SolicitudDocumento::findOrFail($data->finalDocumentId);

      if (Storage::disk('public')->exists('final_documents_solicitudes_du/'.$request->id.'/'.$solicitudDocumento->file)) 
      {
         if (Storage::disk('public')->delete('final_documents_solicitudes_du/'.$request->id.'/'.$solicitudDocumento->file))
         {
            $solicitudDocumento->delete();
         }
      }

      return $request;
   }
}