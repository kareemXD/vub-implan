<?php
namespace App\Repositories;

use Auth;

//Models
use App\User;
use App\Models\Menu;
use App\Models\Permissiondb;
use App\Models\Roledb;

//Services
use Spatie\Permission\Models\Permission;

class NavigationRepository
{
	public function getAllMenus()
	{
		return Menu::whereNull('padre')->orderBy("posicion")->get();
	}

	public function storeMenu($data)
	{
		$menu = new Menu;
		$menu->nombre = $data->name;
		$menu->url = $data->url;
		$menu->alias = $data->alias;
		$menu->icono = $data->icon;
		$menu->posicion = $data->position;
		$menu->save();

		if(!empty($data->url))
		{
			Permission::create(['name' => 'read_'.$menu->alias]);
			Permission::create(['name' => 'write_'.$menu->alias]);
		}

		if(!empty($data->subname))
		{
			$count = count($data->subname);
			for ($i=0; $i < $count; $i++) { 
				$submenu = new Menu;
				$submenu->nombre = $data->subname[$i];
				$submenu->url = $data->suburl[$i];
				$submenu->alias = $data->subalias[$i];
				$submenu->posicion = $data->subposition[$i];
				$submenu->padre = $menu->id;
				$submenu->save();

				Permission::create(['name' => 'read_'.$submenu->alias]);
				Permission::create(['name' => 'write_'.$submenu->alias]);
			}
		}
	}

	public function getMenuById($id)
	{
		return Menu::find($id);
	}

	public function updateMenu($data)
	{
		$menu = $this->getMenuById($data->id);

		if(empty($data->url))
		{
			if(!empty($menu->url))
			{
				$permission_read = Permission::where('name', "read_".$menu->alias)->first();

				if (!empty($permission_read))
				{
					$permission_read->delete();
				}

				$permission_write = Permission::where('name', "write_".$menu->alias)->first();

				if (!empty($permission_write))
				{
					$permission_write->delete();
				}
			}
		}

		$menu->nombre = $data->name;
		$menu->url = $data->url;
		$menu->icono = $data->icon;
		$menu->posicion = $data->position;
		$menu->save();
		$roles = Roledb::all();
		if(empty($data->submenus_id))
		{
			$data->submenus_id = [];
		}
		foreach($menu->submenus as $submenudb)
		{
			if(!in_array($submenudb->id, $data->submenus_id))
			{
				$permission_read = Permission::where('name', "read_".$submenudb->alias)->first();

				if (!empty($permission_read))
				{
					$permission_read->delete();
				}

				$permission_write = Permission::where('name', "write_".$submenudb->alias)->first();

				if (!empty($permission_write))
				{
					$permission_write->delete();
				}

				foreach ($roles as $rol) 
				{
					$rol->menus()->detach($submenudb->id);
				}

				$submenudb->delete();
			}
		}

		if(!empty($data->subname))
		{
			$count = count($data->subname);
			$count_ids = count($data->submenus_id);
			for ($i=0; $i < $count; $i++) {
				if ($i < $count_ids) {
					$submenu = Menu::find($data->submenus_id[$i]);
				}
				if(!empty($submenu))
				{
					$submenu->nombre = $data->subname[$i];
					$submenu->url = $data->suburl[$i];
					$submenu->posicion = $data->subposition[$i];
					$submenu->save();
					$submenu = "";
				}else {
					$submenu = new Menu;
					$submenu->nombre = $data->subname[$i];
					$submenu->alias = $data->subalias[$i];
					$submenu->url = $data->suburl[$i];
					$submenu->posicion = $data->subposition[$i];
					$submenu->padre = $menu->id;
					$submenu->save();
					Permission::create(['name' => 'read_'.$submenu->alias]);
					Permission::create(['name' => 'write_'.$submenu->alias]);
					$submenu = "";
				}
				

			}
		}
	}
}