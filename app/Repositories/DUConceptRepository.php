<?php

namespace App\Repositories;

use App\Models\Du\Concepto;
use App\Models\Du\Ingresos_Concepto;

class DUConceptRepository
{
    public function getConcepts()
    {
        $concepts = Concepto::all();
        return $concepts;
    }

    public function getConceptById($conceptId)
    {
        $concept = Concepto::findOrFail($conceptId);
        return $concept;
    }

    public function store($request)
    {
        $ingresoConcepto = Ingresos_Concepto::findOrFail($request->concepto);

        $concept = new Concepto;
        $concept->Articulo = $request->articulo;
        $concept->Concepto = $ingresoConcepto->Concepto;
        $concept->Uso = $request->uso;
        $concept->Descripcion = $request->descripcion;
        $concept->Importe = $ingresoConcepto->Importe;
        $concept->UAN = !empty($request->uan)? 1 : 0;
        $concept->OverCost = !empty($request->over_cost)? 1 : 0;
        $concept->Status = !empty($request->status)? 1 : 0;
        $concept->save();
    }

    public function update($request)
    {
        $ingresoConcepto = Ingresos_Concepto::findOrFail($request->concepto);

        $concept = $this->getConceptById($request->conceptId);
        $concept->Articulo = $request->articulo;
        $concept->Concepto = $ingresoConcepto->Concepto;
        $concept->Uso = $request->uso;
        $concept->Descripcion = $request->descripcion;
        $concept->Importe = $ingresoConcepto->Importe;
        $concept->UAN = !empty($request->uan)? 1 : 0;
        $concept->OverCost = !empty($request->over_cost)? 1 : 0;
        $concept->Status = !empty($request->status)? 1 : 0;
        $concept->save();
    }

    public function getIngresosConceptos()
    {
        $conceptos = Ingresos_Concepto::all();

        return $conceptos;
    }

    public function clearConcepts()
    {
        // Clear concepts
        Concepto::where('YearConcept', '!=', '2021')->delete();
        Concepto::where('id', '>', 0)->update(['UAN' => '1', 'OverCost' => '0']);
    }
}