<?php

namespace App\Repositories;

use App\Models\Padron\Concepto;
use App\Models\Padron\Debts;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentFolioRepository
{
    public function getDebtsByPadronAccount($padronAccount)
    {
        $debts = Debts::where('CuentaDepto', $padronAccount)->orderBy('FechaMovto', 'ASC')->get();
        return $debts;
    }

    public function debtUpdate($request)
    {
        $concept = Concepto::where('Concepto', $request->concept)->first();

        if (!empty($concept))
        {
            $description = substr($concept->Descripcion, 0, 100);

            DB::connection('sqlsrv')
                ->table('Adeudos')
                ->where('Concepto', $request->originalConcept)
                ->where('CuentaDepto', $request->cuentaDepto)
                ->where('Referencia', $request->reference)
                ->where('Departamento', $request->department)
                ->update([
                    'Concepto' => $request->concept, 
                    'Descripcion' => $description,
                    'Importe' => (float)$request->amount]);


            $bitacoraShortDescription = strtoupper(substr('Se actualizo adeudo con referencia '.$request->reference, 0, 100));
            $bitacoraLongDescription = strtoupper('Se actualizo adeudo con referencia '.$request->reference.' por motivo: '.$request->reason.', concepto: '.$request->concept.', descripción: '.$description.', importe: $'.$request->amount);

            $bitacoraRepository = new BitacoraRepository;
            $bitacoraRepository->insertActionSRV([
                'NumeroLicencia' => $request->cuentaDepto,
                'DescripcionCorta' => $bitacoraShortDescription,
                'DescripcionCompleta' => $bitacoraLongDescription,
                'Usuario' => Auth::user()->name,
                'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
            ]);
        }
    }

    public function debtDelete($request)
    {
        $description = substr($request->description, 0, 100);

        DB::connection('sqlsrv')
            ->table('Adeudos')
            ->where('Concepto', $request->originalConcept)
            ->where('CuentaDepto', $request->cuentaDepto)
            ->where('Referencia', $request->reference)
            ->where('Departamento', $request->department)
            ->delete();

        $bitacoraShortDescription = strtoupper(substr('Se elimino adeudo con referencia '.$request->reference, 0, 100));
        $bitacoraLongDescription = strtoupper('Se elimino adeudo con referencia '.$request->reference.' por motivo: '.$request->reason.', concepto: '.$request->concept.', descripción: '.$description.', importe: $'.$request->amount);

        $bitacoraRepository = new BitacoraRepository;
        $bitacoraRepository->insertActionSRV([
            'NumeroLicencia' => $request->cuentaDepto,
            'DescripcionCorta' => $bitacoraShortDescription,
            'DescripcionCompleta' => $bitacoraLongDescription,
            'Usuario' => Auth::user()->name,
            'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
        ]);
    }
}