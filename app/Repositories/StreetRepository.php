<?php

namespace App\Repositories;

use App\Models\Street;

class StreetRepository
{
    public function storeOrUpdateStreet($streetName, $cityId)
    {
        $street = Street::where(function($query) use ($streetName, $cityId) {
                            $query->where('NombreCalle', $streetName)
                                    ->where('IdPoblacion', $cityId);
                            })->first();

        if (empty($street))
        {
            $last = Street::orderBy('IdCalle', 'desc')->first();

            $streetId = 1;
            if (!empty($last))
            {
                $streetId = (int)$last->IdCalle + 1;
            }

            $street = new Street;
            $street->IdCalle = $streetId;
            $street->NombreCalle = $streetName;
            $street->AliasCalle = $streetName;
            $street->NombreOficial = $streetName;
            $street->IdMunicipio = config('system.settings.municipality');
            $street->IdPoblacion = $cityId;
            $street->IdTipoCalle = null;
            $street->IdCalleGuiaRoji = null;
            $street->IdColonia = null;
            $street->save();
        }

        return $street;
    }
}