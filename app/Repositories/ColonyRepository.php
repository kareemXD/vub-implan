<?php

namespace App\Repositories;

use App\Models\Colony;

class ColonyRepository
{
    public function storeOrUpdateColony($colonyName, $cityId)
    {
        $colony = Colony::where(function($query) use($colonyName, $cityId) {
                                $query->where('NombreColonia', $colonyName)
                                        ->where('IdPoblacion', $cityId);
                            })->first();

        if (empty($colony))
        {
            $last = Colony::orderBy("IdColonia", "desc")->first();
            
            $colonyId = 1;
            if(!empty($last)){
                $colonyId = (int)$last->IdColonia + 1;
            }

            $colony = new Colony;
            $colony->IdColonia = $colonyId;
            $colony->TipoGiro = null;
            $colony->SubTipoGiro = null;
            $colony->IdMunicipio = config('system.settings.municipality');
            $colony->IdPoblacion = $cityId;
            $colony->NombreColonia = $colonyName;
            $colony->NombreComun = $colonyName;
            $colony->Colonos = 'N';
            $colony->PorcentajeRefrendo = 0;
            $colony->Controlado = 0;
            $colony->save();
        }

        return $colony;
    }
}