<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use DB;
//Models
use App\User;
use App\Models\Bitacora;

class BitacoraRepository
{
	public function insertAction($data)
	{
		$report = new Bitacora;

		$report->titulo = $data["title"];
		$report->accion = $data["action"];
		$report->tipo = $data["type"];
		$report->user_id = $data["user"];
		$report->direccion_id = $data["direction"];

		$report->save();
	}
	
	public function insertActionSRV($data)
	{
		$old =DB::connection("sqlsrv")->table("BitacoraLicencia")->orderBy("IdBitacoraLicencia","DESC")->first();
		if($old){
			$id = $old->IdBitacoraLicencia + 1;
		}else{
			$id = 1;
		}
		// dd($data);
		DB::connection("sqlsrv")->table("BitacoraLicencia")->insert([
            'IdBitacoraLicencia' => $id,
			'NumeroLicencia' => $data['NumeroLicencia'],
			'DescripcionCorta' => $data['DescripcionCorta'],
			'DescripcionCompleta' => $data['DescripcionCompleta'],
			'Usuario' => $data['Usuario'],
			'FechaAlta' => str_replace(" ", "T", Carbon::parse($data['FechaAlta'])->toDateTimeString()),
        ]);
	}
}