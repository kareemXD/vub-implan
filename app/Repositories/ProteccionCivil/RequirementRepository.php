<?php

namespace App\Repositories\ProteccionCivil;

use App\Models\ProteccionCivil\Requirement;

class RequirementRepository
{
    public function getRequirements()
    {
        $requirements = Requirement::orderBy('id', 'asc')->get();
        return $requirements;
    }

    public function getRequirementById($requirementId)
    {
        $requirement = Requirement::findOrFail($requirementId);
        return $requirement;
    }

    public function store($request)
    {
        $requirement = new Requirement;
        $requirement->requisito = $request->requirement;
        $requirement->save();
    }

    public function update($request)
    {
        $requirement = $this->getRequirementById($request->requirementId);
        $requirement->requisito = $request->requirement;
        $requirement->save();
    }
}