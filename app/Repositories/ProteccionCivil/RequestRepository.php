<?php

namespace App\Repositories\ProteccionCivil;

use App\Models\Colony;
use App\Models\ProteccionCivil\InmuebleSector;
use App\Models\ProteccionCivil\Request;
use App\Models\ProteccionCivil\RequestTurn;
use App\Models\ProteccionCivil\TipoEstablecimiento;
use App\Models\Street;
use App\Repositories\PadronRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use PDF;
use Illuminate\Support\Facades\File;

class RequestRepository
{
    public function getRequests($data = null)
    {
        if (empty($data))
        {
            $pcOriginRequests = Request::orderBy('numero_solicitud', 'desc')->where('origen', 'Protección Civil')->get();
            $padronDuOriginRequests = Request::orderBy('numero_solicitud', 'desc')->where('origen', '!=','Protección Civil')->whereIn('estatus', [2, 3, 5])->get();

            $requests = $padronDuOriginRequests->merge($pcOriginRequests);
            $validIds = $requests->pluck('id')->toArray();

            $requests = Request::whereIn('id', $validIds)->orderBy('id', 'desc');

            return $requests->paginate(15);
        }else
        {
            $pcOriginRequests = Request::orderBy('numero_solicitud', 'desc')->where('origen', 'Protección Civil')->get();
            $padronDuOriginRequests = Request::orderBy('numero_solicitud', 'desc')->where('origen', '!=','Protección Civil')->whereIn('estatus', [2, 3, 5])->get();

            $requests = $padronDuOriginRequests->merge($pcOriginRequests);
            $validIds = $requests->pluck('id')->toArray();

            $requests = Request::leftJoin('pc_expedientes', 'pc_expedientes.id', '=', 'pc_solicitudes.pc_expediente_id')->whereIn('pc_solicitudes.id', $validIds)->orderBy('pc_solicitudes.id', 'desc');

            if(!empty($data['numero_solicitud'])){
                $requests->where("pc_solicitudes.numero_solicitud", 'like', $data['numero_solicitud']);
            }

            if(!empty($data['no_expediente'])){
                $requests->where("pc_expedientes.no_expediente", 'like', $data['no_expediente']);
            }

            if(!empty($data['nombre_comercial'])){
                $requests->where("pc_solicitudes.nombre_comercial", 'like', '%'.$data['nombre_comercial'].'%');
            }

            if(!empty($data['nombre_solicitante'])){
                $requests->where("pc_solicitudes.nombre_solicitante", 'like', '%'.$data['nombre_solicitante'].'%');
            }

            if(!empty($data['origen'])){
                $requests->where("pc_solicitudes.origen", 'like', '%'.$data['origen'].'%');
            }
            
            if(!empty($data['estatus'])){
                $status = 1;
                switch (strtoupper($data['estatus'])) {
                    case 'EN PROCESO':
                        $status = 2;
                        break;
                    case 'CANCELADO':
                        $status = 4;
                        break;
                    case 'RECHAZADO':
                        $status = 5;
                        break;
                    case 'APROBADO':
                        $status = 3;
                        break;
                }

                $requests->where("pc_solicitudes.estatus", $status);
            }

            return $requests->select('pc_solicitudes.*')->paginate(15);
        }
    }

    public function getRequestById($requestId)
    {
        $request = Request::findOrFail($requestId);
        return $request;
    }

    public function store($data)
    {
        //actualizar o agregar calles
        $street = $data->domicilio2;
        $street1 = $data->calle1_u;
        $street2 = $data->calle2_u;
        $colony1 = $data->colony;
        // dd($data);
        if(isset($data->newcolony) && !empty($data->newcolony)){
            //verificamos que la colonia no haya sido registrada anteriormente
            $checkIfColonyExists = Colony::where(DB::raw("upper(NombreColonia)"),strtoupper($data->newcolony))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfColonyExists))
            {
                $old = Colony::orderBy("IdColonia","DESC")->first();
                $colony = new Colony;
                $colony->IdColonia = $old->IdColonia + 1;
                $colony->NombreColonia = strtoupper($data->newcolony);
                $colony->NombreComun = strtoupper($data->newcolony);
                $colony->IdMunicipio = 1;
                $colony->IdPoblacion = $data->city;
                $colony->Colonos = "N";
                $colony->PorcentajeRefrendo = 0;
                $colony->Controlado = 0;
                $colony->save();
            }else{
                $colony = $checkIfColonyExists;
            }
            $colony1 = intval($colony->IdColonia);
        }
        
        if(isset($data->newdomicilio2) && !empty($data->newdomicilio2)){
            //verificamos que el domicilio no se ha registrado
            $checkIfStreetExist = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newdomicilio2))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfStreetExist))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle = new Street;
                $calle->IdCalle = $old->IdCalle + 1;
                $calle->NombreCalle = strtoupper($data->newdomicilio2);
                $calle->AliasCalle = strtoupper($data->newdomicilio2);
                $calle->NombreOficial = strtoupper($data->newdomicilio2);
                $calle->IdMunicipio = 1;
                $calle->IdPoblacion = $data->city;
                $calle->save();
            }else{
                $calle = $checkIfStreetExist;
            }
            $street = intval($calle->IdCalle);

        }

        if(isset($data->newcalle1_u) && !empty($data->newcalle1_u)){

            $checkIfNewCalle1Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle1_u))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfNewCalle1Exists))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle1 = new Street;
                $calle1->IdCalle = $old->IdCalle + 1;
                $calle1->NombreCalle = strtoupper($data->newcalle1_u);
                $calle1->AliasCalle = strtoupper($data->newcalle1_u);
                $calle1->NombreOficial = strtoupper($data->newcalle1_u);
                $calle1->IdMunicipio = 1;
                $calle1->IdPoblacion = $data->city;
                $calle1->save();
            }else{
                $calle1 = $checkIfNewCalle1Exists;
            }
            
            $street1 = intval($calle1->IdCalle);

        }

        if(isset($data->newcalle2_u) && !empty($data->newcalle2_u)){
            $checkIfNewCalle2Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle2_u))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfNewCalle2Exists))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle2 = new Street;
                $calle2->IdCalle = $old->IdCalle + 1;
                $calle2->NombreCalle = strtoupper($data->newcalle2_u);
                $calle2->AliasCalle = strtoupper($data->newcalle2_u);
                $calle2->NombreOficial = strtoupper($data->newcalle2_u);
                $calle2->IdMunicipio = 1;
                $calle2->IdPoblacion = $data->city;
                $calle2->save();
            }else{
                $calle2 = $checkIfNewCalle2Exists;
            }

            $street2 = intval($calle2->IdCalle);
        }

        $lastRequest = Request::orderBy('id', 'desc')->first();
        $numeroSolicitud = config('system.pc.requests.initialFolio'); // Contador Inicial
        if (!empty($lastRequest))
        {
            $numeroSolicitud = (int)$lastRequest->numero_solicitud > $numeroSolicitud ? (int)$lastRequest->numero_solicitud : $numeroSolicitud;
        }
        $numeroSolicitud = str_pad(($numeroSolicitud + 1), 4, '0', STR_PAD_LEFT);

        $request = new Request;
        $request->numero_solicitud = $numeroSolicitud;
        $request->id_poblacion = $data->city;
        $request->id_colonia = $colony1;
        $request->id_calle = $street;
        $request->id_cruce1 = $street1;
        $request->id_cruce2 = $street2;
        $request->no_exterior = $data->ext;
        $request->no_interior = $data->int;
        $request->coordenada_este = $data->este;
        $request->coordenada_norte = $data->norte;
        $request->fecha_solicitud = $data->fecha_solicitud_year.'-'.$data->fecha_solicitud_month.'-'.$data->fecha_solicitud_day;
        $request->nombre = $data->nombre;
        // $request->giro = $data->giro;
        $request->rfc = $data->rfc;
        $request->curp = $data->curp;
        $request->no_licencia = $data->no_licencia;
        $request->estatus_licencia = $data->estatus_licencia;
        $request->nombre_comercial = $data->nombre_comercial;
        $request->no_empleados = $data->no_empleados;
        $request->horario_laboral = $data->horario_laboral;
        $request->nombre_solicitante = $data->nombre_solicitante;
        $request->caracter_juridico_solicitante = $data->caracter_juridico_solicitante;
        $request->telefono_celular = $data->telefono_celular;
        $request->telefono_fijo = $data->telefono_fijo;
        $request->email = $data->email;
        $request->otro_tramite = $data->other_formality;
        $request->origen = $data->origen;
        $request->no_bombas_gasolinera = $data->no_bombas_gasolinera;
        // $request->id_giro = $data->giro;
        $request->estatus = config('system.pc.requests.statuses.pending'); // Pendiente
        $request->save();

        // Agregar los tramites de la solicitud
        if (!empty($data->formalities))
        {
            $formalityRepository = new FormalityRepository;

            foreach ($data->formalities as $formalityId)
            {
                if ($formalityId != 'otro')
                {
                    $formality = $formalityRepository->getFormalityById($formalityId);
    
                    if (!empty($formality))
                    {
                        $formalityCost = $formality->costByYear($data->fecha_solicitud_year);
                        $request->formalities()->create([
                            'id_tramite' => $formalityId,
                            'costo' => $formalityCost->costo,
                            'year' => $formalityCost->year
                        ]);
                    }
                }
            }
        }

        // Giros
        if (!empty($data->turns))
        {
            foreach ($data->turns as $turnId)
            {
                $padronRepository = new PadronRepository;
                $turn = $padronRepository->getTurnById($turnId);

                if (!empty($turn))
                {
                    $requestTurn = new RequestTurn;
                    $requestTurn->id_solicitud = $request->id;
                    $requestTurn->id_giro = $turnId;
                    $requestTurn->giro = $turn->Nombre;
                    $requestTurn->save();
                }
            }
        }

        return $request;
    }

    public function update($data)
    {
        //actualizar o agregar calles
        $street = $data->domicilio2;
        $street1 = $data->calle1_u;
        $street2 = $data->calle2_u;
        $colony1 = $data->colony;
        // dd($data);
        if(isset($data->newcolony) && !empty($data->newcolony)){
            //verificamos que la colonia no haya sido registrada anteriormente
            $checkIfColonyExists = Colony::where(DB::raw("upper(NombreColonia)"),strtoupper($data->newcolony))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfColonyExists))
            {
                $old = Colony::orderBy("IdColonia","DESC")->first();
                $colony = new Colony;
                $colony->IdColonia = $old->IdColonia + 1;
                $colony->NombreColonia = strtoupper($data->newcolony);
                $colony->NombreComun = strtoupper($data->newcolony);
                $colony->IdMunicipio = 1;
                $colony->IdPoblacion = $data->city;
                $colony->Colonos = "N";
                $colony->PorcentajeRefrendo = 0;
                $colony->Controlado = 0;
                $colony->save();
            }else{
                $colony = $checkIfColonyExists;
            }
            $colony1 = intval($colony->IdColonia);
        }
        
        if(isset($data->newdomicilio2) && !empty($data->newdomicilio2)){
            //verificamos que el domicilio no se ha registrado
            $checkIfStreetExist = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newdomicilio2))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfStreetExist))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle = new Street;
                $calle->IdCalle = $old->IdCalle + 1;
                $calle->NombreCalle = strtoupper($data->newdomicilio2);
                $calle->AliasCalle = strtoupper($data->newdomicilio2);
                $calle->NombreOficial = strtoupper($data->newdomicilio2);
                $calle->IdMunicipio = 1;
                $calle->IdPoblacion = $data->city;
                $calle->save();
            }else{
                $calle = $checkIfStreetExist;
            }
            $street = intval($calle->IdCalle);

        }

        if(isset($data->newcalle1_u) && !empty($data->newcalle1_u)){

            $checkIfNewCalle1Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle1_u))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfNewCalle1Exists))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle1 = new Street;
                $calle1->IdCalle = $old->IdCalle + 1;
                $calle1->NombreCalle = strtoupper($data->newcalle1_u);
                $calle1->AliasCalle = strtoupper($data->newcalle1_u);
                $calle1->NombreOficial = strtoupper($data->newcalle1_u);
                $calle1->IdMunicipio = 1;
                $calle1->IdPoblacion = $data->city;
                $calle1->save();
            }else{
                $calle1 = $checkIfNewCalle1Exists;
            }
            
            $street1 = intval($calle1->IdCalle);

        }

        if(isset($data->newcalle2_u) && !empty($data->newcalle2_u)){
            $checkIfNewCalle2Exists = Street::where(DB::raw("upper(NombreCalle)"),strtoupper($data->newcalle2_u))->where("IdPoblacion", $data->city)->first();
            if(is_null($checkIfNewCalle2Exists))
            {
                $old = Street::orderBy("IdCalle","DESC")->first();
                $calle2 = new Street;
                $calle2->IdCalle = $old->IdCalle + 1;
                $calle2->NombreCalle = strtoupper($data->newcalle2_u);
                $calle2->AliasCalle = strtoupper($data->newcalle2_u);
                $calle2->NombreOficial = strtoupper($data->newcalle2_u);
                $calle2->IdMunicipio = 1;
                $calle2->IdPoblacion = $data->city;
                $calle2->save();
            }else{
                $calle2 = $checkIfNewCalle2Exists;
            }

            $street2 = intval($calle2->IdCalle);
        }
        
        $request = $this->getRequestById($data->requestId);
        $request->id_poblacion = $data->city;
        $request->id_colonia = $colony1;
        $request->id_calle = $street;
        $request->id_cruce1 = $street1;
        $request->id_cruce2 = $street2;
        $request->no_exterior = $data->ext;
        $request->no_interior = $data->int;
        $request->coordenada_este = $data->este;
        $request->coordenada_norte = $data->norte;
        $request->fecha_solicitud = $data->fecha_solicitud_year.'-'.$data->fecha_solicitud_month.'-'.$data->fecha_solicitud_day;
        $request->nombre = $data->nombre;
        $request->giro = $data->giro;
        $request->rfc = $data->rfc;
        $request->curp = $data->curp;
        $request->no_licencia = $data->no_licencia;
        $request->estatus_licencia = $data->estatus_licencia;
        $request->nombre_comercial = $data->nombre_comercial;
        $request->no_empleados = $data->no_empleados;
        $request->horario_laboral = $data->horario_laboral;
        $request->nombre_solicitante = $data->nombre_solicitante;
        $request->caracter_juridico_solicitante = $data->caracter_juridico_solicitante;
        $request->telefono_celular = $data->telefono_celular;
        $request->telefono_fijo = $data->telefono_fijo;
        $request->email = $data->email;
        $request->otro_tramite = $data->other_formality;
        $request->no_bombas_gasolinera = $data->no_bombas_gasolinera;
        $request->id_giro = $data->giro;
        $request->save();

        // Agregar los tramites de la solicitud
        $request->formalities()->delete();
        if (!empty($data->formalities))
        {
            $formalityRepository = new FormalityRepository;

            foreach ($data->formalities as $formalityId)
            {
                if ($formalityId != 'otro')
                {
                    $formality = $formalityRepository->getFormalityById($formalityId);
    
                    if (!empty($formality))
                    {
                        $formalityCost = $formality->costByYear($data->fecha_solicitud_year);
                        $request->formalities()->create([
                            'id_tramite' => $formalityId,
                            'costo' => $formalityCost->costo,
                            'year' => $formalityCost->year
                        ]);
                    }
                }
            }
        }

        RequestTurn::where('id_solicitud', $request->id)->delete();
        // Giros
        if (!empty($data->turns))
        {
            foreach ($data->turns as $turnId)
            {
                $padronRepository = new PadronRepository;
                $turn = $padronRepository->getTurnById($turnId);

                if (!empty($turn))
                {
                    $requestTurn = new RequestTurn;
                    $requestTurn->id_solicitud = $request->id;
                    $requestTurn->id_giro = $turnId;
                    $requestTurn->giro = $turn->Nombre;
                    $requestTurn->save();
                }
            }
        }

        return $request;
    }

    public function updateDictamination($data)
    {
        $request = $this->getRequestById($data->requestId);
        $request->pc_expediente_id = $data->no_expediente;
        $request->pc_inmueble_sector_id = $data->inmueble_sector;
        $request->pc_tipo_establecimiento_id = $data->tipo_establecimiento;
        $request->save();

        return $request;
    }
    
    public function verifiedRequest($data)
    {
        $request = $this->getRequestById($data->requestId);
        $request->estatus = config('system.pc.requests.statuses.verified');
        $request->resultado_verificacion = $data->resultado_verificacion;
        $request->save();

        return $request;
    }

    public function applyRequest($data)
    {
        $request = $this->getRequestById($data->requestId);
        $request->estatus = config('system.pc.requests.statuses.process');
        $request->save();

        return $request;
    }

    public function cancelRequest($data)
    {
        $request = $this->getRequestById($data->requestId);
        $request->estatus = config('system.pc.requests.statuses.cancelled');
        $request->save();

        return $request;
    }

    public function rejectRequest($data)
    {
        $request = $this->getRequestById($data->requestId);
        $request->estatus = config('system.pc.requests.statuses.rejected');
        $request->save();

        return $request;
    }

    public function approveRequest($data)
    {
        $request = $this->getRequestById($data->requestId);
        $request->estatus = config('system.pc.requests.statuses.approved');
        $request->save();

        return $request;
    }

    public function setHighRiskTurn($data)
    {
        $request = $this->getRequestById($data->requestId);
        $request->tipo_giro = config('system.pc.requests.turnTypes.highRisk');
        $request->estatus = config('system.pc.requests.statuses.verification');
        $request->save();

        return $request;
    }

    public function setOrdinaryTurn($data)
    {
        $request = $this->getRequestById($data->requestId);
        $request->tipo_giro = config('system.pc.requests.turnTypes.ordinary');
        $request->estatus = config('system.pc.requests.statuses.verified');
        $request->save();

        return $request;
    }

    public function generateOfficeVistoBueno($requestId)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $request = $this->getRequestById($requestId);

        File::copy(public_path('assets/images/hoja_membretada_morena.jpg'), storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg'));

        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle(' CARTA DE CONFORMIDAD DE VISTO BUENO - PROTECCIÓN CIVIL');
        PDF::SetSubject(' CARTA DE CONFORMIDAD DE VISTO BUENO - PROTECCIÓN CIVIL');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use($date_now){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg');
            // Image($file, $x='', $y='', $w=0, $h=0, $type='', $link='', $align='', $resize=false, $dpi=300, $palign='', $ismask=false, $imgmask=false, $border=0, $fitbox=false, $hidden=false, $fitonpage=false)
            $pdf->Image($imagen, 0, -10, 216, 279, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
        });
       
        PDF::setFooterCallback(function($pdf) use($request) {
            // Set font
            $pdf->SetFont('', '', 8);
            // Page number
            $pdf->SetY(-38);
            $pdf->Cell(80, 0, 'C.c.p Archivo', 0, false, 'L', 0, '', 0, false, 'T', 'M');
            $pdf->SetY(-34);
            $pdf->Cell(80, 0, 'C.c.p Minuta', 0, false, 'L', 0, '', 0, false, 'T', 'M');
            $pdf->SetY(-30);
            $pdf->Cell(80, 0, 'VMMS/'.$request->ldrr, 0, false, 'L', 0, '', 0, false, 'T', 'M');
            
            $pdf->SetY(-34);
            $pdf->SetFont('', 'B', 8);
            $pdf->Cell(200, 0, 'Página '.$pdf->getAliasNumPage().' de '.$pdf->getAliasNbPages().' del Oficio', 0, false, 'R', 0, '', 0, false, 'T', 'M');
            $pdf->SetY(-30);
            $pdf->SetFont('', '', 8);
            $pdf->Cell(184, 0, 'UMPC/02/'.$request->umpc.'/'.$request->created_at->format('Y'), 0, false, 'R', 0, '', 0, false, 'T', 'M');

            $pdf->SetY(-34);
            $pdf->SetFont('', 'B', 8);
            $pdf->Cell(170, 0, 'Presidencia Municipal', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $pdf->SetY(-30);
            $pdf->SetFont('', '', 8);
            $pdf->Cell(170, 0, 'Calle morelos No. 12 Valle de Banderas, Bahía de Banderas; Nayarit. (329) 291 1870', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 39, PDF_MARGIN_RIGHT);
        PDF::AddPage('H', 'LETTER');
        $html = "";
        $html = view('partials.pdf.pc.vistoBueno')->with(compact('request'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        // $this->bitacoraRepository->insertActionSRV([
        //     'NumeroLicencia' => $license->NumeroLicencia,
        //     'DescripcionCorta' => 'Se generó la orden de pago',
        //     'DescripcionCompleta' => 'Se generó la orden de pago',
        //     'Usuario' => Auth::user()->name,
        //     'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
        // ]);

        $fileName = "CARTA DE CONFORMIDAD DE VISTO BUENO ".'UMPC/02/'.$request->umpc.'/'.$request->created_at->format('Y')." - ".$date_now->format('d-M-Y').".pdf";
        PDF::SetTitle($fileName);
        PDF::Output($fileName);
    }

    public function generateOfficeCartaCompromiso($requestId)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $request = $this->getRequestById($requestId);

        File::copy(public_path('assets/images/hoja_membretada_morena.jpg'), storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg'));

        PDF::SetCreator("XI. Ayuntamiento de Bahia de Banderas");
        PDF::SetAuthor('VUB');
        PDF::SetTitle('CARTA COMPROMISO - PROTECCIÓN CIVIL');
        PDF::SetSubject('CARTA COMPROMISO - PROTECCIÓN CIVIL');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = storage_path('app/public/padron/licenses/hoja_membretada_morena.jpg');
            
            $pdf->Image($imagen, 0,-10, 216, 279, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(40);
            $pdf->Cell(0, 0, "UNIDAD MUNICIPAL DE PROTECCIÓN CIVIL", 0, 0, 'C');
            $pdf->SetY(45);
            $pdf->Cell(0, 0, "BAHÍA DE BANDERAS, NAYARIT", 0, 0, 'C');
            // $logo = public_path('assets/images/logo-bahia.jpg');
            // $pdf->Image($logo, 15,8, 0, 0, 'JPG', '', '', true, 300, '', false, false, 0);
        });
       
        PDF::setFooterCallback(function($pdf) use($request) {
            // Set font
            $pdf->SetFont('', '', 8);
            // Page number
            $pdf->SetY(-34);
            $pdf->SetFont('', 'B', 8);
            $pdf->Cell(200, 0, 'Página '.$pdf->getAliasNumPage().' de '.$pdf->getAliasNbPages().' del Oficio', 0, false, 'R', 0, '', 0, false, 'T', 'M');
            $pdf->SetY(-30);
            $pdf->SetFont('', '', 8);
            $pdf->Cell(184, 0, 'CC/'.$request->cc.'/'.$request->created_at->format('Y'), 0, false, 'R', 0, '', 0, false, 'T', 'M');
            
            $pdf->SetY(-34);
            $pdf->SetFont('', 'B', 8);
            $pdf->Cell(170, 0, 'Presidencia Municipal', 0, false, 'C', 0, '', 0, false, 'T', 'M');
            $pdf->SetY(-30);
            $pdf->SetFont('', '', 8);
            $pdf->Cell(170, 0, 'Calle morelos No. 12 Valle de Banderas, Bahía de Banderas; Nayarit. (329) 291 1870', 0, false, 'C', 0, '', 0, false, 'T', 'M');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, 50, PDF_MARGIN_RIGHT);
        PDF::AddPage('H', 'LETTER');
        $html = "";
        $html = view('partials.pdf.pc.cartaCompromiso')->with(compact('request'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        // $this->bitacoraRepository->insertActionSRV([
        //     'NumeroLicencia' => $license->NumeroLicencia,
        //     'DescripcionCorta' => 'Se generó la orden de pago',
        //     'DescripcionCompleta' => 'Se generó la orden de pago',
        //     'Usuario' => Auth::user()->name,
        //     'FechaAlta' => Carbon::now("America/Mexico_City")->toDateString()
        // ]);

        $fileName = "CARTA COMPROMISO ".'CC/'.$request->cc.'/'.$request->created_at->format('Y')." - ".$date_now->format('d-M-Y').".pdf";
        PDF::SetTitle($fileName);
        PDF::Output($fileName);
    }

    public function requirementStatus($requestId, $requirementId)
    {
        $request = $this->getRequestById($requestId);
        $status = config('system.pc.requirements.statuses.pending');

        $document = $request->requirements()->where('id_requisito', $requirementId)->first();
        if (!empty($document))
        {
            $status = config('system.pc.requirements.statuses.uploaded');
        }

        return $status;
    }

    public function getFullAddressByRequest($requestId)
    {
        $request = Request::find($requestId);

        $address = $request->street->NombreCalle." #".$request->no_exterior;

        if (!empty($request->no_interior))
        {
            $address .= " int:".$request->no_interior;
        }

        $address .= ", ".$request->colony->NombreColonia.", ".$request->population->NombrePoblacion.", Bahía de Banderas, Nayarit.";

        return $address;
    }

    public function requirementUpload($data)
    {
        $request = $this->getRequestById($data->requestId);

        if (!empty($data->document))
        {
            // Revisar si ya cuenta con el mismo requisito para actualizarlo.
            $requestRequirement = $request->requirements()->where('id_requisito', $data->requirementId)->first();

            // Subir el documento nuevo y obtener la url.
            $fileName = $request->id.'-'.Str::random(10).'.'.$data->file('document')->extension();
            $data->file('document')->storeAs('public/requests/'.$request->id.'/requirements', $fileName);
            $fileUrl = 'requests/'.$request->id.'/requirements/'.$fileName;

            // Validar que haya un requisito anterior para eliminarlo.
            if (!empty($requestRequirement))
            {
                // Validar que exista el archivo antes de quererlo eliminar.
                if (Storage::disk('public')->exists($requestRequirement->documento))
                {
                    Storage::disk('public')->delete($requestRequirement->documento);
                }

                // Solo actualizar la url del documento.
                $requestRequirement->documento = $fileUrl;
                $requestRequirement->save();
            }else
            {
                // Generar el nuevo registro del requisito.
                $request->requirements()->create([
                    'id_requisito' => $data->requirementId,
                    'documento' => $fileUrl,
                ]);
            }
        }

        return $request;
    }

    public function documentUpload($data)
    {
        $request = $this->getRequestById($data->requestId);

        if (!empty($data->document))
        {
            // Subir el documento nuevo y obtener la url.
            $fileName = $request->id.'-'.Str::random(10).'.'.$data->file('document')->extension();
            $data->file('document')->storeAs('public/requests/'.$request->id.'/documents', $fileName);
            $fileUrl = 'requests/'.$request->id.'/documents/'.$fileName;

            // Generar el nuevo registro del requisito.
            $request->documents()->create([
                'descripcion' => $data->document_name,
                'documento' => $fileUrl,
            ]);
        }

        return $request;
    }

    public function searchExpedientes($search)
    {
        $expedients = DB::connection('pgsql')
                        ->table('pc_expedientes')
                        ->where(function($query) use($search) {
                            $query->where('no_expediente', 'ilike', "%$search%")
                                    ->orWhere('razon_social', 'ilike', "%$search%")
                                    ->orWhere('nombre_comercial', 'ilike', "%$search%");
                        })
                        ->distinct()
                        ->select('id', 'no_expediente', 'razon_social', 'nombre_comercial')
                        ->limit(15)
                        ->get();

        return $expedients;
    }

    public function getInmuebleSectores()
    {
        $inmuebleSectores = InmuebleSector::all();
        return $inmuebleSectores;
    }

    public function getTipoEstablecimientos()
    {
        $tipoEstablecimientos = TipoEstablecimiento::all();
        return $tipoEstablecimientos;
    }
}