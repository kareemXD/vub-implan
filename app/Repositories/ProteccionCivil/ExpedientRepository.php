<?php

namespace App\Repositories\ProteccionCivil;

use App\Models\ProteccionCivil\Expedient;

class ExpedientRepository 
{
    public function getExpedients($inputs = null)
    {
        $expedients = Expedient::orderByRaw('length(no_expediente), no_expediente');

        if (!empty($inputs))
        {
            if(!empty($inputs['no_expediente'])){
                $expedients->where("no_expediente", 'ilike', $inputs['no_expediente']);
            }
    
            if(!empty($inputs['razon_social'])){
                $expedients->where("razon_social", 'ilike', '%'.$inputs['razon_social'].'%');
            }
    
            if(!empty($inputs['nombre_comercial'])){
                $expedients->where("nombre_comercial", 'ilike', '%'.$inputs['nombre_comercial'].'%');
            }
            
            if(!empty($inputs['grupo'])){
                $expedients->where("grupo", 'ilike', '%'.$inputs['grupo'].'%');
            }
        }

        return $expedients->paginate(15);
    }

    public function getExpedientById($expedientId)
    {
        $expedient = Expedient::findOrFail($expedientId);
        return $expedient;
    }

    public function getGroups()
    {
        $groups = Expedient::select('grupo')->distinct()->pluck('grupo')->toArray();
        return $groups;
    }

    public function store($request)
    {
        $expedient = new Expedient;
        $expedient->no_expediente = $request->no_expediente;
        $expedient->domicilio = $request->domicilio;
        $expedient->numero = $request->numero;
        $expedient->colonia = $request->colonia;
        $expedient->localidad = $request->localidad;
        $expedient->razon_social = $request->razon_social;
        $expedient->rfc = $request->rfc;
        $expedient->nombre_comercial = $request->nombre_comercial;
        $expedient->grupo = $request->grupo;
        // $expedient->giro = $request->giro;
        // $expedient->campo_de_riesgo = $request->campo_de_riesgo;
        $expedient->telefono = $request->telefono;
        $expedient->correo = $request->correo;
        $expedient->observaciones = $request->observaciones;
        $expedient->referencias = $request->referencias;
        $expedient->estado = $request->estado;
        $expedient->save();

        return $expedient;
    }

    public function update($request)
    {
        $expedient = $this->getExpedientById($request->expedientId);
        // $expedient->no_expediente = $request->no_expediente;
        $expedient->domicilio = $request->domicilio;
        $expedient->numero = $request->numero;
        $expedient->colonia = $request->colonia;
        $expedient->localidad = $request->localidad;
        $expedient->razon_social = $request->razon_social;
        $expedient->rfc = $request->rfc;
        $expedient->nombre_comercial = $request->nombre_comercial;
        $expedient->grupo = $request->grupo;
        // $expedient->giro = $request->giro;
        // $expedient->campo_de_riesgo = $request->campo_de_riesgo;
        $expedient->telefono = $request->telefono;
        $expedient->correo = $request->correo;
        $expedient->observaciones = $request->observaciones;
        $expedient->referencias = $request->referencias;
        $expedient->estado = $request->estado;
        $expedient->save();

        return $expedient;
    }
}