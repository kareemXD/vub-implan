<?php

namespace App\Repositories\ProteccionCivil;

use App\Models\ProteccionCivil\Formality;

class FormalityRepository
{
    public function getFormalities()
    {
        $formalities = Formality::all();
        return $formalities;
    }

    public function getFormalityById($formalityId)
    {
        $formality = Formality::findOrFail($formalityId);
        return $formality;
    }

    public function store($request)
    {
        $formality = new Formality;
        $formality->tramite = $request->formality;
        $formality->save();

        if (!empty($request->years))
        {
            foreach ($request->years as $key => $year)
            {
                $formality->costs()->create([
                    'year' => $year,
                    'costo' => str_replace(',', '', $request->{'costNewLicenses'}[$key]),
                    'costo_refrendo' => str_replace(',', '', $request->{'costRefrendos'}[$key])
                ]);
            }
        }

        return $formality;
    }

    public function update($request)
    {
        $formality = $this->getFormalityById($request->formalityId);
        $formality->tramite = $request->formality;
        $formality->save();

        if (!empty($request->years))
        {
            $formality->costs()->delete();

            foreach ($request->years as $key => $year)
            {
                $formality->costs()->create([
                    'year' => $year,
                    'costo' => str_replace(',', '', $request->{'costNewLicenses'}[$key]),
                    'costo_refrendo' => str_replace(',', '', $request->{'costRefrendos'}[$key])
                ]);
            }
        }

        return $formality;
    }
}