<?php

namespace App\Repositories;

use App\Models\Direction;

class DirectionRepository
{
    public function getDirections()
    {
        $directions = Direction::all();
        return $directions;
    }

    public function getDirectionById($directionId)
    {
        $direction = Direction::findOrFail($directionId);
        return $direction;
    }

    public function store($request)
    {
        $direction = new Direction;
        $direction->nombre = $request->nombre;
        $direction->abreviacion = $request->abreviacion;
        $direction->save();
    }

    public function update($request)
    {
        $direction = $this->getDirectionById($request->directionId);
        $direction->nombre = $request->nombre;
        $direction->abreviacion = $request->abreviacion;
        $direction->save();
    }
}