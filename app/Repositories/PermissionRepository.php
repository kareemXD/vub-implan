<?php

namespace App\Repositories;

use Spatie\Permission\Models\Permission;

class PermissionRepository 
{
    public function getPermissionById($permissionId)
    {
        $permission = Permission::where('id', $permissionId)->firstOrFail();
        return $permission;
    }

    public function getCustomPermissionById($permissionId)
    {
        $permission = Permission::where('custom', 1)->where('id', $permissionId)->firstOrFail();
        return $permission;
    }

    public function getPermissions()
    {
        $permissions = Permission::all();
        return $permissions;
    }

    public function getCustomPermissions()
    {
        $permissions = Permission::where('custom', 1)->get();
        return $permissions;
    }

    public function store($request)
    {
        $permission = new Permission;
        $permission->name = $request->name;
        $permission->guard_name = $request->guard;
        $permission->custom = !empty($request->custom)? 1 : 0;
        $permission->label = $request->label;
        $permission->save();
    }

    public function update($request)
    {
        $permission = $this->getPermissionById($request->permissionId);
        $permission->name = $request->name;
        $permission->guard_name = $request->guard;
        $permission->custom = !empty($request->custom)? 1 : 0;
        $permission->label = $request->label;
        $permission->save();
    }
}