<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\OfficeNotification;
//Models
use App\User;
use App\Models\Ticket;
use App\Models\Bitacora;
use App\Models\Notifications;
use App\Models\Offices;
use App\Models\Position;
use App\Models\Priority;

use App\Models\Padron\License;

//Services
use Illuminate\Support\Facades\Hash;

class AdministrationRepository
{
	public function getActiveTickets()
	{
		return Ticket::whereIn('estatus_id', [1,2])->get();
	}
	
	public function getAllTickets()
	{
		return Ticket::all();
	}
	
	public function getTicketById($id)
	{
		$ticket =Ticket::find($id);
		$ticket->prioridad_txt = $ticket->priority->nombre;
		$ticket->prioridad_color = $ticket->priority->color;
		$ticket->estatus_txt = $ticket->status->nombre;
		$ticket->estatus_color = $ticket->status->color;
		$ticket->solicitante = $ticket->user->name;

		return $ticket;
	}
	
	public function changeTicketStatus($id, $status)
	{
		$ticket =Ticket::find($id);
		$ticket->estatus_id = $status;
		$ticket->save();

		$bitacora = new Bitacora;
		$bitacora->action = "El usuario ". Auth::user()->name . " cambio el estatus del ticket con folio " . $ticket->folio . ', de "Pendiente" a "En Revisión"';
		$bitacora->users_id = Auth::user()->id;
		$bitacora->save();

		$notification = new Notifications;
		$notification->user_id = $ticket->users_id;
		$notification->contenido = "El usuario ". Auth::user()->name . " cambio el estatus del ticket con folio " . $ticket->folio . ', de "Pendiente" a "En Revisión"';
		$notification->estatus = 0;
		$notification->save();

		return $ticket;
	}
	
	public function finishTicket($id, $comment)
	{
		$ticket =Ticket::find($id);
		$ticket->estatus_id = 3;
		$ticket->comentario = $comment;
		$ticket->fecha_termino = Carbon::now("America/Mexico_City");
		$ticket->save();

		$bitacora = new Bitacora;
		$bitacora->action = "El usuario ". Auth::user()->name . " cambio el estatus del ticket con folio " . $ticket->folio . ', de "En Revisión" a "Terminado"';
		$bitacora->user_id = Auth::user()->id;
		$bitacora->direccion_id = Auth::user()->profile->position->direction->id;
		$bitacora->save();

		$notification = new Notifications;
		$notification->user_id = $ticket->users_id;
		$notification->contenido = "El usuario ". Auth::user()->name . " cambio el estatus del ticket con folio " . $ticket->folio . ', de "En Revisión" a "Terminado"';
		$notification->estatus = 0;
		$notification->save();

		return $ticket;
	}

	public function storeTicket($data)
	{
		
		$old_ticket = Ticket::orderBy('id', 'DESC')->first();
		if(empty($old_ticket))
		{
			$folio = "00001";
		}else
		{
			$arr = intval(explode("SOP-", $old_ticket->folio)[1]);
			$folio = str_pad($arr+1, 5, "0", STR_PAD_LEFT);
		}
		$ticket = new Ticket;
		$ticket->folio = "SOP-".$folio;
		$ticket->descripcion = $data->description;
		$ticket->prioridad_id = $data->priority;
		$ticket->estatus_id = 1;
		$ticket->users_id = Auth::user()->id;
		$ticket->save();

		$bitacora = new Bitacora;
		$bitacora->action = "El usuario ". Auth::user()->name . " registro un ticket con folio " . $ticket->folio . ', el dia '. $ticket->created_at;
		$bitacora->user_id = Auth::user()->id;
		$bitacora->direccion_id = Auth::user()->profile->position->direction->id;
		$bitacora->save();

		$users = User::all();
		foreach ($users as $key => $user) {
			if($user->hasRole("Administrador"))
			{
				$notification = new Notifications;
				$notification->user_id = $user->id;
				$notification->contenido = "El usuario ". Auth::user()->name . " registro un ticket con folio " . $ticket->folio . ', el dia '. $ticket->created_at;
				$notification->estatus = 0;
				$notification->save();
			}
		}
		return "Se registro correctamente un ticket con folio ". $ticket->folio;
	}

	public function getAllUsers()
	{
		return User::all();
	}

	public function newUser($data)
	{
		$user = new User;
		$user->name = $data->name;
		$user->email = $data->email;
		$user->password = Hash::make($data->password);
		$user->save();

		$user->assignRole("Cliente");
	}

	public function getUserById($id)
	{
		return User::find($id);
	}
	
	//Offices Functions
	public function getAllOffices()
	{
		return Offices::orderByDesc('fecha_recepcion')->get();
	}

	public function storeOffice($data)
	{ 	
		if($data->tipo == 0)
		{
			DB::beginTransaction();

			try {
				// Validate, then create if valid
				$office = new Offices;

				if($data->respuesta != null)
				{
					$office->respuesta = 1;
				}
				else
				{
					$office->respuesta = 0;
				}

				$office->estatus = 1;		
				$office->emisor = $data->emisor;
				$office->no_oficio = $data->no_oficio;
				$office->fecha_emision = $data->fecha_emision;
				$office->asunto = $data->asunto;
				$office->fecha_recepcion = $data->fecha_recepcion;
				$office->fecha_respuesta = $data->fecha_respuesta;
				$office->dependencia = $data->dependencia;			
				$office->type = $data->tipo;
				$office->save();
				$data->id = $office->id;
				$originalName = $data->oficio->getClientOriginalName();
				$ex = $data->oficio->getClientOriginalExtension();
				$no_oficio = str_replace('/', '-', $data->no_oficio);
				Storage::disk('public')->putFileAs('oficios', $data->oficio, $no_oficio.'-'.$data->id.'.'.$ex);
				$data->cc = 0;
				if(!empty($data->responsable))
				{
					$puesto = Position::find($data->responsable);
					$correo = $puesto->profile[0]->user->email;			
					Mail::to($correo)->send(new OfficeNotification($data));
				}
				
				if (Mail::failures()) {
					
				}else {
					
				}
				// dd($data->userAttach);
				if(!empty($data->userAttach))
				{
					foreach ($data->userAttach as $value)
					{
						$office->responsable()->attach($value, ['type' => 1]);
						$data->cc = 1;
						$puesto = Position::find($value);
						$correo = $puesto->profile[0]->user->email;
						Mail::to($correo)->send(new OfficeNotification($data));     	
					}						
				}	
				
				$office->responsable()->attach($data->responsable, ['type' => 0]);
				
			}catch(ValidationException $e){
				// Rollback and then redirect
				// back to form with errors
				DB::rollback();
			}
			DB::commit();
		}
		else if($data->tipo == 1)
		{
			DB::beginTransaction();
			try {
				$oficio = Offices::where('no_oficio', '=', $data->no_oficio )->count();
				if($oficio > 0)
				{
					$ex = $data->oficio->getClientOriginalExtension();
					$data->no_oficio = str_replace('/', '-', $data->no_oficio);
					Storage::disk('public')->putFileAs('oficios', $data->oficio, $data->no_oficio.'-'.$data->id.'.'.$ex);
					$office = Offices::find($data->id);
					$office->emisor = $data->emisor;
					$office->estatus = 3;
					$office->fecha_recepcion = $data->fecha_recepcion;
					$office->save();
				}
				else 
				{
					$office = new Offices;
					$office->emisor = $data->emisor;
					$no_office = DB::table('oficios')->where('type', '=', 1)->orWhere('type', '=', 2)->orderBy('id', 'desc')->first();			
					$now = Carbon::now();
					if($no_office != null)
					{
						$numero = substr($no_office->no_oficio, 4, -5);
						$id = (int)$numero;
						$id = $id+1;
						$no_oficio = 'IMP/'.$id.'/'.$now->year;
					}
					else 
					{
						$no_oficio = 'IMP/465/'.$now->year;
					}
					$office->no_oficio = $no_oficio;
					$office->fecha_emision = $data->fecha_emision;
					$office->asunto = $data->asunto;
					$office->fecha_recepcion = $data->fecha_recepcion;
					$office->dependencia = $data->dependencia;			
					$office->type = $data->tipo;
					$office->estatus = 0;
					$office->save();

					if(!empty($data->userAttach))
					{
						foreach ($data->userAttach as $value)
						{
							$office->responsable()->attach($value, ['type' => 1]);
							$data->cc = 1;
							$puesto = Position::find($value);
							$correo = $puesto->profile[0]->user->email;
							Mail::to($correo)->send(new OfficeNotification($data));     	
						}						
					}	

					$office->responsable()->attach(Auth::user()->profile->position->id, ['type' => 0]);
				}
			} catch (ValidationException $e) {
				DB::rollback();
			}
			DB::commit();
		}
		else if($data->tipo == 2)
		{
			DB::beginTransaction();
			try {
				$oficio = Offices::where('no_oficio', '=', $data->no_oficio )->count();
				if($oficio > 0)
				{
					$ex = $data->oficio->getClientOriginalExtension();
					$data->no_oficio = str_replace('/', '-', $data->no_oficio);
					Storage::disk('public')->putFileAs('oficios', $data->oficio, $data->no_oficio.'-'.$data->id.'.'.$ex);


					$office = Offices::find($data->id);
					$office->fecha_recepcion = $data->fecha_recepcion;
					$office->estatus = 3;
					$office->save();

					
					$officeFather = Offices::find($office->dad->id);
					$officeFather->estatus = 3;
					$officeFather->save();
				}
				else 
				{
					$office = new Offices;
					$office->emisor = $data->emisor;
					$no_office = DB::table('oficios')->where('type', '=', 1)->orWhere('type', '=', 2)->orderBy('id', 'desc')->first();			
					$now = Carbon::now();
					if($no_office != null)
					{
						$numero = substr($no_office->no_oficio, 4, -5);
						$id = (int)$numero;
						$id = $id+1;
						$no_oficio = 'IMP/'.$id.'/'.$now->year;
					}
					else 
					{
						$no_oficio = 'IMP/465/'.$now->year;
						$id = "465";
					}
					$office->no_oficio = $no_oficio;
					$office->fecha_emision = $data->fecha_emision;
					$office->asunto = $data->asunto;
					$office->fecha_recepcion = $data->fecha_recepcion;
					$office->dependencia = $data->dependencia;			
					$office->type = $data->tipo;
					$office->id_padre = $data->id_padre;
					$office->estatus = 0;
					$office->save();
					if(!empty($data->userAttach))
					{
						foreach ($data->userAttach as $value)
						{
							$office->responsable()->attach($value, ['type' => 1]);
							$data->cc = 1;
							$puesto = Position::find($value);
							$correo = $puesto->profile[0]->user->email;
							Mail::to($correo)->send(new OfficeNotification($data));     	
						}						
					}	
					$office->responsable()->attach(Auth::user()->profile->position->id, ['type' => 0]);
				}
			} catch (ValidationException $e) {
				DB::rollback();
			}
			DB::commit();
		}
	}

	public function getOfficeById($id)
	{
		return Offices::find($id);
	}

	public function changeStatusByid($data)
	{
		$office = Offices::find($data->id);
		$office->estatus = $data->estatus;
		$office->save();
	}

	public function getAllPriorities()
	{
		return Priority::all();
	}

	public function sendNotification($data)
	{
		$office = Offices::find($data->id);
		$data->no_oficio = $office->no_oficio;
		$puesto = Position::find($data->encargado);
		$data->asunto = "alerta";
		$correo = $puesto->profile[0]->user->email;
		Mail::to($correo)->send(new OfficeNotification($data));     	
	}

	public function statusDelete($data)
	{
		$office = Offices::find($data->id);
		$office->estatus = 4;
		$office->save();
	}
	public function statusRestore($data)
	{
		$office = Offices::find($data->id);
		
		if($office->type == 0 && $office->respuesta == false)
		{
			$office->estatus = 1;
		}
		else if($office->type == 0 && $office->respuesta != false)
		{
			$office->estatus = 2;
		}
		else if($office->type == 1 || $office->type == 2)
		{
			$office->estatus = 0;
		}
		$office->save();
	}

	public function getStaticticsForDashboard()
	{
		$meses = [
			'Enero' => [
				'nombre' => 'Enero',
				'numero' => '01',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Febrero' => [
				'nombre' => 'Febrero',
				'numero' => '02',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Marzo' => [
				'nombre' => 'Marzo',
				'numero' => '03',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Abril' => [
				'nombre' => 'Abril',
				'numero' => '04',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Mayo' => [
				'nombre' => 'Mayo',
				'numero' => '05',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Junio' => [
				'nombre' => 'Junio',
				'numero' => '06',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Julio' => [
				'nombre' => 'Julio',
				'numero' => '07',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Agosto' => [
				'nombre' => 'Agosto',
				'numero' => '08',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Septiembre' => [
				'nombre' => 'Septiembre',
				'numero' => '09',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Octubre' => [
				'nombre' => 'Octubre',
				'numero' => '10',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Noviembre' => [
				'nombre' => 'Noviembre',
				'numero' => '11',
				'licensias' => '',
				'recaudacion' => '',
			],
			'Diciembre' => [
				'nombre' => 'Diciembre',
				'numero' => '12',
				'licensias' => '',
				'recaudacion' => '',
			]
		];
		
		foreach ($meses as $key_mes => $mes) {
			$query = License::Select("Licencias.*")->join("PadronContribuyentes", "PadronContribuyentes.IdContribuyente", "=", "Licencias.IdContribuyente")->orderBy(DB::raw("Cast(Licencias.NumeroLicencia as INT)"),"DESC");
			$now = Carbon::now("America/Mexico_City");
			$date = Carbon::create($now->format("Y"), $mes['numero'], 06, 12, 0, 0);
			$initial = str_replace(" ", "T", $date->firstOfMonth()->toDateTimeString());
            $final = str_replace(" ", "T", $date->lastOfMonth()->endOfDay()->toDateTimeString());
			// dd($date, $initial, $final);
			$query->whereBetween ("Licencias.FechaAlta", [$initial, $final]);
			$liceses = $query->get();
			$meses[$key_mes]["licensias"] = $query->count();
			$meses[$key_mes]["data"] = $query->get();
			$total = 0;
			if(count($liceses) > 0)
			{
				foreach ($liceses as $key => $licese) {
					foreach ($licese->turns as $key => $turn) {
						$total += $turn->getCost();
					}
				}
				$meses[$key_mes]["recaudacion"] = $total;
			}
		}
		return $meses;
	}
}