<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PadronNewLicencesSheet implements FromView
{
    private $licenses;


    public function __construct($licenses)
    {
        $this->licenses = $licenses;
    }

    public function view(): View
    {
        return view('padron.exports.licenses', [
            'licenses' => $this->licenses
        ]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Nuevas Licencias';
    }
}
