<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class LicensingBudgetReportExport implements FromView
{
    protected $rows,
            $totals;

    public function __construct($rows, $totals)
    {
        $this->rows = $rows;
        $this->totals = $totals;
    }

    public function view(): View
    {
        return view('padron.exports.licensingBudgetReport', [
            'rows' => $this->rows,
            'totals' => $this->totals,
        ]);
    }
}
