<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class PadronReportLicencesRefrendosExport implements WithMultipleSheets
{
    private $licenses;

    public function __construct($licenses)
    {
        $this->licenses = $licenses;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        return [
            'Nuevas Licencias' => new PadronNewLicencesSheet($this->licenses['licenses']),
            'Refrendos' => new PadronRefrendosSheet($this->licenses['refrendos']),
        ];
    }
}
