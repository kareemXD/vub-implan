<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*********************************** Auth and Others Routes ***********************************/
Route::get('/', function () {
    if (auth()->check())
    {
        return redirect()->route('dashboard');
    }else
    {
        return view('auth.login');
    }
});

Auth::routes();

Route::get('/home', 'HomeController@redirect')->middleware('checkstatus')->name('redirect');
Route::get('/redirect', 'HomeController@redirect')->middleware('checkstatus')->name('redirect');

Route::get('/editar/perfil', 'HomeController@editProfile')->middleware('auth')->name("edit.profile");


Route::get('/notificaciones', 'HomeController@notifications')->middleware('auth')->name("notifications");
Route::post('/actualizar/notificacion', 'HomeController@updateNotification')->middleware('auth')->name("update.notification");
/*********************************** End Auth and Others Routes ******************************/

/*********************************** All Routes ***********************************/
Route::middleware('auth')->group(function () {

    Route::namespace('Administration')->group(function () {
        //Dashboard
        Route::get('/panel-control', 'AdministrationController@dashboard')->middleware('checkProfile')->name('dashboard');
    });

    // Authentication
    Route::namespace('Auth')->prefix('autenticacion')->group(function () {
        // Authentication -> Users (User Administration)
        Route::get('/usuarios', 'AuthController@users')->middleware('checkProfile')->name('users');
        Route::post('/obtener/datos/usuario', 'AuthController@getUserById')->name('get.user.data');
        Route::post('/nuevo/usuario', 'AuthController@newUser')->name('new.register');
        Route::post('/actualizar/perfil', 'AuthController@updateProfile')->middleware('auth')->name("update.profile");
        Route::post('/actualizar/perfil/usuario', 'AuthController@updateProfileClient')->middleware('auth')->name("update.profile.client");
        Route::post('/desactivar/usuario', 'AuthController@deactivateUser')->middleware('isAdmin')->name("deactivate.user");

        // Authentication -> Roles (Roles Administration)
        Route::get('/roles', 'AuthController@roles')->middleware('checkProfile')->name('roles');
        Route::get('/nuevo/rol', 'AuthController@newRole')->name('new.role');
        Route::post('/guardar/rol', 'AuthController@storeRole')->name('store.role');
        Route::get('/editar/rol/{id}', 'AuthController@editRole')->name('edit.role');
        Route::post('/actualizar/rol', 'AuthController@updateRole')->name('update.role');

        Route::prefix('permisos')->group(function() {
            Route::get('/', 'PermissionController@index')->name('auth.permissions');
            Route::get('crear', 'PermissionController@create')->name('auth.permissions.create');
            Route::post('registrar', 'PermissionController@store')->name('auth.permissions.store');
            Route::get('{permissionId}/editar', 'PermissionController@edit')->name('auth.permissions.edit');
            Route::put('actualizar', 'PermissionController@update')->name('auth.permissions.update');
        });

        Route::prefix('direcciones')->group(function() {
            Route::get('/', 'DirectionController@index')->name('auth.directions');
            Route::get('crear', 'DirectionController@create')->name('auth.directions.create');
            Route::post('registrar', 'DirectionController@store')->name('auth.directions.store');
            Route::get('{permissionId}/editar', 'DirectionController@edit')->name('auth.directions.edit');
            Route::put('actualizar', 'DirectionController@update')->name('auth.directions.update');
        });
    });

    //Navigation
    Route::namespace('Navigation')->prefix('navegacion')->group(function () {
        //Navigation -> Menus (Administration of Menus and Submenus)
        Route::get('/menus', 'NavigationController@menus')->middleware('checkProfile')->name('menus');
        Route::get('/nuevo/menu', 'NavigationController@newMenu')->name('new.menu');
        Route::post('/guardar/menu', 'NavigationController@storeMenu')->name('store.menu');
        Route::get('/editar/menu/{id}', 'NavigationController@editMenu')->name('edit.menu');
        Route::post('/actualizar/menu', 'NavigationController@updateMenu')->name('update.menu');
    });
    
    //Padron y Licencias
    Route::namespace('Moduls')->prefix('padron')->group(function () {
        //Administration of taxpayers
        Route::any('/contribuyentes', 'PadronController@taxpayers')->middleware('checkProfile')->name('contribuyentes');
        Route::post('/guardar/contribuyentes', 'PadronController@storeTaypayer')->name('store.taxpayer');
        Route::post('/actualizar/contribuyentes', 'PadronController@storeUpdateTaypayer')->name('store.update.taxpayer');
        Route::post('/obtener/contribuyentes', 'PadronController@getTaxpayerData')->name('get.taxpayer.data');
        Route::any('/imprimir/contribuyentes/{id}', 'PadronController@printTaxpayerDetail')->name('print.taxpayer.detail');
        Route::any('/imprimir/contribuyentes', 'PadronReportController@printTaxpayers')->name('print.taxpayers');
        Route::post('ajax/contribuyentes', 'PadronController@ajaxTaxpayers')->name('ajax.taxpayers');
        
        //Administration of turns
        Route::any('/giros', 'PadronController@turns')->middleware('checkProfile')->name('turns');
        Route::any('/obtener/giro', 'PadronController@getTurnData')->name('get.turn.data');
        Route::any('/imprimir/giros', 'PadronReportController@printTurns')->name('print.turns');
        Route::get('/nuevo/giros', 'PadronController@addTurn')->name('add.turn');
        Route::post('/guardar/giros', 'PadronController@storeTurn')->name('store.turn');
        Route::get('/actualizar/giros/{id}', 'PadronController@updateTurn')->name('update.turn');
        Route::post('/guardar/actualizacion/giro', 'PadronController@storeUpdateTurn')->name('store.update.turn');
        Route::post('/guardar/actualizacion/periodo', 'PadronController@updatePeriod')->name('update.period');
        Route::post('/guardar/periodo/{idGiro}', 'PadronController@addPeriod')->name('add.period');
        Route::post('ajax/giros', 'PadronController@ajaxTurns')->name('ajax.turns');

        //Administration of Requests
        Route::any('/solicitudes', 'PadronController@requests')->middleware('checkProfile')->name('requests');
        Route::get('/nueva/solicitud', 'PadronController@addRequest')->name('add.request');
        // Route::any('/imprimir/solicitudes', 'PadronReportController@printRequests')->name('print.requests');
        Route::post('/obtener/costos', 'PadronController@getLicenseCost')->name('get.license.cost');
        Route::post('/obtener/costos/detalles', 'PadronController@getLicenseCostDetails')->name('get.license.cost.details');
        Route::get('/actualizar/solicitud/{id}/{operation?}', 'PadronController@updateRequest')->name('update.request');
        Route::post('/guardar/actualizacion/solicitud', 'PadronController@storeUpdateRequest')->name('store.update.request');
        Route::post('/actualizar/requisito', 'PadronController@updateDocumentRequest')->name('update.request.document');
        Route::post('/solicitud/actualizar/pc/requisito', 'PadronController@updatePcRequirementRequest')->name('update.request.pcRequirements.update');
        Route::post('/solicitud/pc/nuevo/tramite', 'PadronController@requestPcFormalityStore')->name('update.request.formalities.store');
        Route::post('/obtener/requisito', 'PadronController@getDocumentByRequest')->name('get.document.by.request');
        Route::get('/mostrar/documento/{requisito}/{solicitud}', 'PadronController@showDocumentByRequest')->name('show.document.by.request');
        Route::post('load/taxpayer/licences', 'PadronController@loadTaxpayerLicences')->name('load.taxpayer.licences');
        
        //Administration of Licenses
        Route::any('/licencias', 'PadronController@licenses')->middleware('checkProfile')->name('licenses');
        Route::get('/nueva/licencia', 'PadronController@addLicense')->name('add.license');
        Route::post('/guardar/licencia', 'PadronController@storeLicense')->name('store.license');
        Route::post('/guardar/actualizacion/licencia', 'PadronController@storeUpdateLicense')->name('store.update.license');
        Route::get('/nuevo/tramite/{licencia}/{tramite}', 'PadronController@addRequestToLicense')->name('add.request.license');
        Route::any('/search-license-details', 'PadronController@searchLicenseDetails')->name('search.license.details');
        
        Route::any('/imprimir/licencias', 'PadronReportController@printLicenses')->name('print.licenses');
        Route::get('/actualizar/licencia/{id}', 'PadronController@updateLicense')->name('update.license');
        Route::any('/imprimir/licencia/{licencia?}', 'PadronReportController@printLicense')->name('print.license');
        Route::any('/imprimir/costos/{licencia}', 'PadronReportController@printCostLicense')->name('print.cost.license');
        Route::any('/imprimir/account/{licencia}', 'PadronReportController@printAccountLicense')->name('print.account.license');
        Route::post('/licencia/actualizar/pc/requisito', 'PadronController@updatePcRequirementLicense')->name('update.license.pcRequirements.update');
        Route::post('/licencia/pc/nuevo/tramite', 'PadronController@licensePcFormalityStore')->name('update.license.formalities.store');
        
        //licencias dadas de baja
        Route::any('/licencias-baja', 'PadronController@licenciasBajas')->middleware('checkProfile')->name('licenses.canceled');
        Route::post('baja/licencia/{NumeroLicencia}', 'PadronController@bajaLicencia')->name('store.license.baja');
        Route::get('licencia-baja/show/{id}', 'PadronController@verBajaLicencia')->name('licencia.baja.show');
        Route::any('imprimir/baja/licencia/{NumeroLicencia}', 'PadronReportController@printBaja')->name('print.license.baja');

        Route::post('/obtener/colonias', 'PadronController@getColoniesByCity')->name('get.colonies.by.city');
        Route::post('/obtener/calles', 'PadronController@getCallesByColony')->name('get.calles.by.colony');
        
        //Process of Padron and Licences
        Route::any('/actualizacion/anual/giros', 'PadronController@updateAnualTurns')->middleware('checkProfile')->name('pyl.update.turns');
        Route::any('/realizar/anual/giros', 'PadronController@aplayAnualTurns')->name('aplay.turns.process');
        Route::any('/refrendos/anuales/licencias', 'PadronController@updateAnualLicences')->middleware('checkProfile')->name('pyl.update.licences');
        
        Route::any('/pago/basura', 'PadronController@trashPayment')->middleware('checkProfile')->name('pyl.trash');
        Route::get('/pago/basura/{turnId}/editar', 'PadronController@trashPaymentEdit')->name('pyl.edit.trash');
        Route::post('/actualizar/pago/basura', 'PadronController@updateTrash')->name('pyl.update.trash');
        Route::post('/agregar/ano/basura', 'PadronController@trashYearAdd')->name('pyl.trash.years.add');
        
        //Alerts
        Route::any('/alertas', 'PadronController@alerts')->middleware('checkProfile')->name('pyl.alerts');
        Route::any('/guardar/alerta', 'PadronController@storeAlerts')->name('pyl.store.alert');
        Route::any('/imprimir/alertas', 'PadronReportController@printAlerts')->name('print.alerts');


        //Reports
        Route::any('/reporte/licencias/mes', 'PadronReportController@reportMonthlyLicences')->middleware('checkProfile')->name('report.monthly.licences');
        Route::any('/reporte/licencias', 'PadronReportController@reportLicences')->middleware('checkProfile')->name('report.licences');
        Route::any('/imprimir/reporte/licencias', 'PadronReportController@printReportLicences')->name('print.report.licenses');
        Route::get('/reporte/licencias-refrendos', 'PadronReportController@reportLicencesRefrendos')->middleware('checkProfile')->name('report.licencesRefrendos');
        Route::get('/reporte/licencias-refrendos/detalle', 'PadronReportController@detailReportLicencesRefrendos')->name('detail.report.licensesRefrendos');
        Route::get('/reporte/licencias/adeudos', 'PadronReportController@reporteLicenciasConAdeudo')->name('report.licenses.debts');
        Route::get('/reporte/licencias/adeudos/detalle', 'PadronReportController@reporteLicenciasConAdeudoDetalle')->name('report.licenses.debts.details');
        Route::get('/reporte/presupuestos-licencias', 'PadronReportController@licensingBudgetReport')->name('report.licensingBudgetReport');
        Route::get('/reporte/presupuestos-licencias/detalle', 'PadronReportController@licensingBudgetReportDetail')->name('report.licensingBudgetReport.detail');
    
        Route::any("/poblaciones", "PadronController@getLocations")->middleware('checkProfile')->name("locations.list");
        Route::get("/guardar/poblacion", "PadronController@addLocation")->name("location.add");
        Route::post("/guardar/poblacion", "PadronController@storeLocation");
        Route::get("/actualizar/poblacion/{id}", "PadronController@editLocation")->name("location.update");
        Route::post("/actualizar/poblacion/{id}", "PadronController@updateLocation");
        
        //colonias
        Route::any("/colony/list/{idPoblacion?}", "PadronController@getColonies")->name("colony.list");
        Route::post("/guardar/colonia", "PadronController@addColony")->name("colony.add");
        Route::get("/guardar/actualizar/colonia/{idColonia}", "PadronController@editColony")->name("colony.update");
        Route::post("/guardar/actualizar/colonia/{idColonia}", "PadronController@updateColony");

        //calles
        Route::any("/calle/list/{idPoblacion?}", "PadronController@getStreets")->name("street.list");
        Route::post("/guardar/calle", "PadronController@addStreet")->name("street.add");
        Route::get("/guardar/actualizar/calle/{idPoblacion?}", "PadronController@editStreet")->name("street.update");
        Route::post("/guardar/actualizar/calle/{idPoblacion?}", "PadronController@updateStreet");
        Route::get("/getinfoaddress", "PadronController@getInfoAddress")->name("api.get-info");
        Route::any("/obtener-calle-por-id/{id?}", "PadronController@getStreetById")->name("api.get-street-by-id");

        Route::get('estadisticas', 'PadronController@statistics')->name('padron.statistics');
    });
    
    //Desarrollo Urbano
    Route::namespace('Moduls')->prefix('du')->group(function () {
        //Administration of Firmalities
        Route::get('/tramites', 'DuController@precedures')->middleware('checkProfile')->name('du.formalities');
        Route::get('/nuevo/tramite', 'DuController@addProcess')->name('du.add.process');
        Route::post('/guardar/nuevo/tramite', 'DuController@storeProcess')->name('du.store.process');
        Route::get('/editar/tramite/{id}', 'DuController@updateProcess')->name('du.update.process');
        Route::post('/guardar/editcion/tramite', 'DuController@storeUpdateProcess')->name('du.store.update.process');

        Route::prefix('conceptos')->namespace('DU')->group(function() {
            Route::get('/', 'ConceptController@index')->name('du.concepts');
            Route::get('crear', 'ConceptController@create')->name('du.concepts.create');
            Route::post('guardar', 'ConceptController@store')->name('du.concepts.store');
            Route::get('{conceptId}/editar', 'ConceptController@edit')->name('du.concepts.edit');
            Route::put('actualizar', 'ConceptController@update')->name('du.concepts.update');
        });

        //Administration of Requests
        Route::get('/solicitudes', 'DuController@requests')->middleware('checkProfile')->name('du.requests');
        Route::get('/nueva/solicitud', 'DuController@addRequests')->name('du.add.request');
        Route::post('/guardar/nueva/solicitud', 'DuController@storeRequest')->name('du.store.request');
        Route::get('/actualizar/solicitud/{id}', 'DuController@updateRequest')->name('du.update.request');
        Route::post('/guardar/actualizacion/solicitud', 'DuController@storeUpdateRequest')->name('du.store.update.request');
        Route::post('/obtener/requisitos', 'DuController@getRequirementsById')->name('get.requeriments.by.process');
        Route::post('/actualizar/requisito', 'DuController@updateDocumentRequest')->name('update.du.request.document');
        Route::post('/pc/actualizar/requisito', 'DuController@updatePcRequirementRequest')->name('update.du.request.pcRequirement');
        Route::post('/pc/nuevo/tramite', 'DuController@requestPcFormalityStore')->name('update.du.request.formalities.store');
        Route::post('/obtener/requisito', 'DuController@getDocumentByRequest')->name('get.document.du.by.request');
        Route::get('/mostrar/documento/{requisito}/{solicitud}', 'DuController@showDocumentByRequest')->name('show.document.by.request');

        Route::get('/verificacion/solicitud/{id}', 'DuController@verifyRequest')->name('du.verify.request');
        Route::post('/guardar/verificacion/solicitud', 'DuController@storeVerifyRequest')->name('du.store.verify.request');
        
        Route::get('/inspeccionar/solicitud/{id}', 'DuController@inspectRequest')->name('du.inspect.request');
        Route::post('/guardar/inspeccion/solicitud', 'DuController@storeInspectRequest')->name('du.store.inspect.request');
        Route::post('solicitud/inspectores', 'DuController@getAvailableInspectors')->name('du.request.asign.inspectors');
        Route::put('solicitud/establecer/inspector', 'DuController@setInspectorToRequest')->name('du.request.asign.inspectors.update');
        
        Route::get('/dictaminacion/solicitud/{id}', 'DuController@dictaminationRequest')->name('du.dictamination.request');
        Route::post('/guardar/dictaminacion/solicitud', 'DuController@storeDictaminationRequest')->name('du.store.dictamination.request');
        Route::post('solicitud/dictaminadores', 'DuController@getAvailableDictaminators')->name('du.request.asign.dictaminators');
        Route::put('solicitud/establecer/dictaminador', 'DuController@setDictaminatorToRequest')->name('du.request.asign.dictaminators.update');
        
        Route::post('payments/concepts/remove', 'DuController@removePaymentConcept')->name('du.payments.concepts.remove');
        
        Route::get('/detalle/solicitud/{id}', 'DuController@detailRequest')->name('du.detail.request');
        Route::post('/subir/documento', 'DuController@storeFinalDocument')->name('du.finalDocuments.store');
        Route::delete('/eliminar/documento', 'DuController@deleteFinalDocument')->name('du.finalDocuments.delete');
       
        //impresion formato ventanilla
        Route::any('/imprimir/ventanilla/{id}', 'DuReportController@printWindowRequest')->name('du.print.caratula.request');
        // Impresion formato de pago
        Route::any('/imprimir/formatopago/{id}', 'DuReportController@printPaymentFormatRequest')->name('du.print.paymentFormat.request');


        //Administration of Licenses
        Route::get('/licencias', 'DuController@licenses')->middleware('checkProfile')->name('du.licenses');
        Route::get('/licencias', 'DuController@licenses')->middleware('checkProfile')->name('new.licencia');

        
        // Requests procedures
        Route::post('tramites/solicitud', 'DuController@proceduresRequest')->name('du.requests.procedures');

    });

    Route::namespace('Moduls')->prefix('pc')->group(function() {
        // Route::get('solicitudes/padron', 'ProteccionCivilController@requestsPadron')->name('pc.requests.padron');
        // Route::get('solicitudes/du', 'ProteccionCivilController@requestsDu')->name('pc.requests.du');

        Route::prefix('solicitudes')->namespace('ProteccionCivil')->group(function() {
            Route::get('/', 'RequestController@index')->name('pc.requests');
            Route::get('crear', 'RequestController@create')->name('pc.requests.create');
            Route::post('guardar', 'RequestController@store')->name('pc.requests.store');
            Route::get('{requestId}/editar', 'RequestController@edit')->name('pc.requests.edit');
            Route::put('actualizar', 'RequestController@update')->name('pc.requests.update');
            Route::get('{requestId}/dictaminacion', 'RequestController@dictamination')->name('pc.requests.dictamination');
            Route::get('{requestId}/detalle', 'RequestController@detail')->name('pc.requests.detail');
            Route::post('requisito/subir', 'RequestController@requirementUpload')->name('pc.requests.requirements.upload');
            Route::post('documento/subir', 'RequestController@documentUpload')->name('pc.requests.documents.upload');

            Route::get('{requestId}/imprimir', 'RequestController@printRequest')->name('pc.requests.print');
            Route::get('{requestId}/vistobueno', 'RequestController@vistoBueno')->name('pc.requests.vistoBueno');
            Route::get('{requestId}/cartacompromiso', 'RequestController@cartaCompromiso')->name('pc.requests.cartaCompromiso');

            Route::post('ajax/expedientes', 'RequestController@ajaxExpedientes')->name('pc.ajax.expedients');
            Route::post('ajax/expediente/buscar', 'RequestController@ajaxExpedienteFind')->name('pc.ajax.expedient.find');
        });

        Route::prefix('tramites')->namespace('ProteccionCivil')->group(function() {
            Route::get('/', 'FormalityController@index')->name('pc.formalities');
            Route::get('nuevo', 'FormalityController@create')->name('pc.formalities.create');
            Route::post('guardar', 'FormalityController@store')->name('pc.formalities.store');
            Route::get('{tramiteId}/editar', 'FormalityController@edit')->name('pc.formalities.edit');
            Route::put('actualizar', 'FormalityController@update')->name('pc.formalities.update');
        });

        Route::prefix('requisitos')->namespace('ProteccionCivil')->group(function() {
            Route::get('/', 'RequirementController@index')->name('pc.requirements');
            Route::get('nuevo', 'RequirementController@create')->name('pc.requirements.create');
            Route::post('guardar', 'RequirementController@store')->name('pc.requirements.store');
            Route::get('{unitId}/editar', 'RequirementController@edit')->name('pc.requirements.edit');
            Route::put('actualizar', 'RequirementController@update')->name('pc.requirements.update');
        });

        Route::prefix('expedientes')->namespace('ProteccionCivil')->group(function() {
            Route::get('/', 'ExpedientController@index')->name('pc.expedients');
            Route::get('crear', 'ExpedientController@create')->name('pc.expedients.create');
            Route::post('guardar', 'ExpedientController@store')->name('pc.expedients.store');
            Route::get('{expedientId}/editar', 'ExpedientController@edit')->name('pc.expedients.edit');
            Route::put('actualizar', 'ExpedientController@update')->name('pc.expedients.update');

            Route::post('ajax/update', 'ExpedientController@ajaxUpdate')->name('pc.ajax.expedients.update');
        });

        Route::get('estadisticas', 'ProteccionCivilController@statistics')->name('pc.statistics');
    });

    Route::namespace('Moduls')->prefix('oromapas')->group(function() {
        Route::get('estadisticas', 'OromapasController@statistics')->name('oromapas.statistics');
    });

    Route::namespace('Moduls')->prefix('paymentFolio')->group(function() {
        Route::get('padron', 'PaymentFolioController@padron')->name('paymentFolios.padron');
        Route::put('debt/update', 'PaymentFolioController@debtUpdate')->name('paymentFolios.debts.update');
        Route::delete('debt/delete', 'PaymentFolioController@debtDelete')->name('paymentFolios.debts.delete');
    });
});
/*********************************** End All Routes *******************************/

/*********************************** Secret Routes ***********************************/
Route::prefix('secret')->group(function () {
    Route::get('/crear-roles/{password?}', 'SecretController@createRoles');
    Route::get('/asignar-administrador/{password?}', 'SecretController@assignAdministrator');
    Route::get('/asignar-descripcion-correcta-a-los-adeudos/{year}/{password?}', 'SecretController@assignCorrectDescriptionToDebts');
    Route::get('/sincronizar-giros-para-solicitudes-proteccion-civil/{password?}', 'SecretController@syncronizeTurnsForProteccionCivil');
});
/*********************************** End Secret Routes *******************************/
// Route::get('/Registrar', 'OromapasController@index')->name('oromapas');

Route::prefix('info')->group(function() {
    Route::get('php', 'InfoController@php');
});